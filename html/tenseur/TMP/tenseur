% vim: set filetype=tex:

\documentclass[matemat.tex]{subfiles}
\begin{document}

\chapter{Tenseurs}

\label{chap:tenseur}

\section*{Dépendances}

\begin{itemize}
\item Chapitre~\ref{chap:vecteur} : Les vecteurs
\item Chapitre~\ref{chap:lineaire} : Les fonctions linéaires
\item Chapitre~\ref{chap:forme} : Les formes
\item Chapitre~\ref{chap:ps} : Les produits scalaires
\end{itemize}

\section{Introduction}

Nous présentons deux variantes de la définition des tenseurs. La première, classique, est basée sur les formes linéaires. La seconde, basée sur les vecteurs et la généralisation des produits scalaires, a l'avantage de mettre en relief la structure particulière des tenseurs, ainsi que la multitude de fonctions que l'on peut leur associer.

\section{Produit tensoriel de formes linéaires}

Soit deux espaces vectoriels $E$ et $F$ sur $\corps$, ainsi que les fonctions $\varphi \in \lineaire(E,S)$ et $\psi \in \lineaire(F,S)$. On définit le produit tensoriel de ces deux fonctions, noté $\varphi \otimes \psi$, par~:

$$(\varphi \otimes \psi)(a,b) = \varphi(a) \cdot \psi(b)$$

pour tout $a \in E$ et $b \in F$. La fonction $\varphi \otimes \psi$ est donc une forme bilinéaire vérifiant~:

$$\biforme{a}{\varphi \otimes \psi}{b} = \forme{\varphi}{a} \cdot \forme{\psi}{b}$$

\subsection*{Forme associée}

On peut réécrire la définition comme~:

$$(\varphi \otimes \psi)(a,b) = \forme{\psi(b) \cdot \varphi}{a} = (\psi(b) \cdot \varphi)(a)$$

Nous pouvons donc associer à chaque $b \in F$ une forme linéaire $\psi(b) \cdot \varphi$ que nous notons~:

$$\forme{\varphi \otimes \psi}{b} = \varphi \cdot \psi(b)$$

\subsection*{Dualité}

On voit en échangeant $\varphi$ et $\psi$ que~:

$$(\psi \otimes \varphi)(b,a) = \psi(b) \cdot \varphi(a) = (\varphi \otimes \psi)(a,b)$$

\subsection*{Associativité}

Le produit tensoriel étant associatif, on note~:

$$\varphi \otimes \psi \otimes \omega = \varphi \otimes (\psi \otimes \omega) = (\varphi \otimes \psi) \otimes \omega$$

pour toutes formes linéaires $\varphi,\psi,\omega$ définies sur les espaces vectoriels $E,F,G$.

\subsection*{Notation}

On convient également de la notation~:

$$\big(\forme{\psi \otimes \varphi}{u}\big)(x) = \psi(x) \cdot \forme{\varphi}{u}$$

pour tout $x \in F$. On a donc~:

$$\forme{\psi \otimes \varphi}{u} = \psi \cdot \forme{\varphi}{u}$$

\section{Tenseurs de formes linéaires}

Soit les espaces vectoriels $E_1,E_2,...,E_n$ sur $\corps$. On nomme tenseur d'ordre $n$ les formes $n$-linéaires de $E_1 \times E_2 \times ... \times E_n$ vers $\corps$.

\section{Produit tensoriel de deux vecteurs}

Soit les espaces vectoriels $E$ et $F$ sur $\corps$. Choisissons $a \in F$ et $b \in E$. Le produit tensoriel $a \otimes b$ est l'application linéaire de $E$ vers $F$ définie par~:

$$(a \otimes b)(c) = a \cdot \scalaire{b}{c}$$

pour tout $c \in E$. On vérifie aisément que ce produit est bilinéaire.

\subsection*{Associativité mixte}

Il est clair d'après les définitions des opérations que~:

\begin{Eqts}
(\alpha \cdot a) \otimes b = \alpha \cdot (a \otimes b) \\
a \otimes (b \cdot \alpha) = (a \otimes b) \cdot \alpha
\end{Eqts}

pour tout $\alpha \in \corps$.

\subsection*{Distributivité}

On a~:

\begin{Eqts}
(a + b) \otimes c = a \otimes c + b \otimes c \\
a \otimes (c + d) = a \otimes c + a \otimes d
\end{Eqts}

pour tout $a,b \in F$ et $c,d \in E$.

\section{Contractions}

Soit les espaces vectoriels $E,F,G$ sur $\corps$. On étend la notion de «~produit scalaire~» par les relations~:

\begin{Eqts}
\scalaire{a \otimes b}{c} = a \cdot \scalaire{b}{c} \\
\scalaire{b}{c \otimes d} = \scalaire{b}{c} \cdot d \\
\scalaire{a \otimes b}{c \otimes d} = \scalaire{b}{c} \cdot (a \otimes d)
\end{Eqts}

valables pour tout $(a,b,c,d) \in G \times F \times F \times E$.

\section{Contractions doubles}

Soit $b \in F$ et $c \in E$. Afin de rester consistant avec le produit tensoriel des formes, nous définissons la forme associée à $b \otimes c$ par~:

$$\varphi(a,d) = \scalaire{a}{(b \otimes c)(d)} = \scalaire{a}{b} \cdot \scalaire{c}{d}$$

pour tout $d \in E$ et $a \in F$. On note en général cette forme au moyen de la double contraction~:

$$\braket{a}{b \otimes c}{d} = \scalaire{a}{b} \cdot \scalaire{c}{d}$$

\section{Dualité}

Soit le scalaire $\alpha \in \setC$ et les vecteurs $a,u \in E_1$ et $b,v \in E_2$. On a~:

$$\scalaire{u}{\alpha \cdot (a \otimes b)(v)} = \alpha \cdot \scalaire{u}{a} \cdot \scalaire{b}{v}$$

et~:

\begin{eqnarray*}
\scalaire{\conjaccent{\alpha} \cdot (b \otimes a)(u)}{v} &=& \alpha \cdot \conjugue(\scalaire{a}{u}) \cdot \scalaire{b}{v} \\
&=& \alpha \cdot \scalaire{u}{a} \cdot \scalaire{b}{v}
\end{eqnarray*}

On en déduit que~:

$$(\alpha \cdot a \otimes b)^\dual =  \conjaccent{\alpha} \cdot b \otimes a$$

\section{Combinaison linéaire}

Soit des vecteurs $a_i,b_i,c_i,d_i$ et des scalaires $\theta_{ij},\upsilon_{ij}$. On étend la définition des contractions à des combinaisons linéaires de la forme~:

\begin{Eqts}
T = \sum_{i,j} \theta_{ij} \cdot a_i \otimes b_j \\
U = \sum_{i,j} \upsilon_{ij} \cdot c_i \otimes d_j
\end{Eqts}

en imposant simplement la linéarité. On a donc par exemple~:

$$\scalaire{T}{U} = \sum_{i,j,k,l} \theta_{ij} \cdot \upsilon_{kl} \cdot \scalaire{b_j}{c_k} \cdot (a_i \otimes d_l)$$

\section{Tenseurs d'ordre deux}

Soit $a \in F$ et $b \in E$. Un objet de la forme~:

$$T = a \otimes b$$

est un cas particulier de «~tenseur d'ordre deux~». Il est formellement défini comme une application linéaire de $E$ vers $F$, mais il est en fait bien plus riche puisqu'on peut associer différents types de fonctions et de formes à chaque contraction possible impliquant ce tenseur. Les notions de tenseur et de contraction sont en fait étroitement liées.

On note $\tenseur_2(F,E)$ l'espace vectoriel généré par ce type de tenseurs d'ordre deux.

\section{Tenseurs d'ordre un et zéro}

La possibilité d'associer une forme linéaire à chaque vecteur $a \in E$ par la contraction avec un autre vecteur, qui est dans ce cas un simple produit scalaire~:

$$\varphi_a(b) = \scalaire{a}{b}$$

nous incite à considérer les vecteurs comme des «~tenseurs d'ordre un~». Nommant $\tenseur_1(E)$ l'ensemble des tenseurs d'ordre un, on a simplement $\tenseur_1(E) = E$. Quant aux scalaires, ils seront considérés comme des «~tenseurs d'ordre zéro~». On note $\tenseur_0 = \corps$ l'ensemble des tenseurs d'ordre zéro.

\section{Associativité}

Soit $a \in G$, $b \in F$ et $c \in E$. Comme $\lineaire(E,F)$ est également un espace vectoriel, on a~:

$$\Big[ a \otimes  \big[ (b \otimes c)(u) \big] \Big](v) = \big[ a \otimes b \scalaire{c}{u} \big](v) = a \scalaire{c}{u} \scalaire{b}{v}$$

valable pour tout $u \in E$ et tout $v \in F$. Mais $\lineaire(F,G)$ est aussi un espace vectoriel, et l'on a aussi~:

$$\Big[ \big[ (a \otimes b)(v) \big] \otimes c \Big](u) = \Big[ \scalaire{b}{v} a \otimes c \Big](u) = \scalaire{b}{v} \scalaire{c}{u} a$$

Le résultat étant le même, le produit tensoriel est associatif, et nous notons~:

$$a \otimes b \otimes c = a \otimes (b \otimes c) = (a \otimes b) \otimes c$$

l'application linéaire définie par~:

$$(a \otimes b \otimes c)(u,v) = \scalaire{c}{u} \scalaire{b}{v} a$$

\section{Tenseur d'ordre $n$}

Considérons les espaces vectoriels $E_1,...,E_n$, les séries de vecteurs $a_k^1,a_k^2,...a_k^{N_k} \in E_k$ et les scalaires $\theta_{ij...r} \in \corps$. Un tenseur d'ordre $n$ est une combinaison linéaire de la forme~:

$$T = \sum_{i,j,...,r} \theta_{ij...r} \cdot a_1^i \otimes a_2^j \otimes ... \otimes a_n^r$$

On note $\tenseur_n(E_1,E_2,...,E_n)$ l'espace des tenseurs d'ordre $n$.

Lorsque tous les espaces vectoriels sont égaux, soit $E = E_1 = E_2 = ... = E_n$, on note $\tenseur_n(E) = \tenseur_n(E,E,...,E)$.

\subsection*{Attention}

Ne pas confondre les {\em indices supérieurs} (le $i$ des vecteurs $a_j^i$ par exemple), très utilisés en calcul tensoriel, avec les puissances ! Dans le contexte des tenseurs, une éventuelle puissance d'un scalaire $\theta_i^j$ serait notée au besoin par~:

$$\big( \theta_j^i \big)^m = \theta_j^i \cdot ... \cdot \theta_j^i$$

\section{Dualité}

Soit les séries de vecteurs $a_k^i \in E_k$, les scalaires $\theta_{ij...rs} \in \corps$, et le tenseur associé~:

$$A = \sum_{i,j,...,r,s} \theta_{ij...rs} \cdot a_1^i \otimes a_2^j \otimes ... \otimes a_{n - 1}^r \otimes a_n^s$$

On vérifie que le dual s'obtient en inversant l'ordre des vecteurs et en conjuguant les coordonnées~:

$$A^\dual =  \sum_{i,j,...,r,s} \conjaccent{\theta}_{ij...rs} \cdot a_n^s \otimes a_{n - 1}^r \otimes ... \otimes a_2^j \otimes a_1^i$$

\section{Réduction}

Soit les séries de vecteurs $a_k^i \in E_k$, les scalaires $\theta_{ij...rs} \in \corps$, et le tenseur associé~:

$$A = \sum_{i,j,...,r,s} \theta_{ij...rs} \cdot a_1^i \otimes a_2^j \otimes ... \otimes a_{n - 1}^r \otimes a_n^s$$

Les opérations de réduction consistent à construire des tenseurs d'ordre $n - 1$, notés $A_-(s)$ et $A^-(i)$, en retirant les vecteurs $a_n^s$ (réduction à droite) ou les vecteurs $a_1^i$ (réduction à gauche)~:

\begin{Eqts}
A_-(s) =  \sum_{i,j,...,r} \theta_{ij...rs} \cdot a_1^i \otimes a_2^j \otimes ... \otimes a_{n - 1}^r \\
A^-(i) =  \sum_{j,...,r,s} \theta_{ij...rs} \cdot a_2^j \otimes ... \otimes a_{n - 1}^r \otimes a_n^s
\end{Eqts}

\section{Contraction généralisée}

Soit les séries de vecteurs $a_k^i \in E_k$ et $b_k^j \in F_k$, les scalaires $\eta_{i...r}, \theta_{j...s} \in \corps$, et les tenseurs~:

\begin{Eqts}
A = \sum_{i,...,r} \eta_{i...r} \cdot a_1^i \otimes ... \otimes a_m^r \\
B = \sum_{j,...,s} \theta_{j...s} \cdot b_1^j \otimes ... \otimes b_n^s
\end{Eqts}

La contraction d'ordre $0$ consiste simplement à juxtaposer les deux tenseurs au moyen du produit tensoriel~:

$$\contraction{A}{0}{B} = \sum_{i,...,r,j,...,s} \eta_{i...r} \cdot \theta_{j...s} \cdot a_1^i \otimes ... \otimes a_m^r \otimes b_1^j \otimes ... \otimes b_n^s$$

Soit $p \in \setN$ tel que $0 \le p \le \min \{m,n\}$. Si~:

\begin{Eqts}
E_m = F_1 \\
E_{m - 1} = F_2 \\
\vdots \\
E_{m - p + 1} = F_p \\
\end{Eqts}

on peut définir la contraction $\contraction{}{p}{}$  d'ordre $p$ de deux tenseurs par récurrence~:

$$\contraction{A}{p}{B} = \sum_{r,j} \scalaire{a_m^r}{b_1^j} \cdot \contraction{A_-(r)}{p - 1}{B^-(j)}$$

\subsection*{Notation}

Soit $A$ un tenseur d'ordre $m$ et $B$ un tenseur d'ordre $n$. Le produit tensoriel est bien évidemment identique à la contraction d'ordre $0$. On le note~:

$$A \otimes B = \contraction{A}{0}{B}$$

La contraction d'ordre $1$ est notée comme un produit scalaire~:

$$\scalaire{A}{B} = \contraction{A}{1}{B}$$

ou comme un produit lorsqu'il n'y a pas de confusion possible~:

$$A \cdot B = \scalaire{A}{B}$$

Enfin, la contraction maximale d'ordre $M = \min \{m,n\}$ est notée par~:

$$A : B = \contraction{A}{M}{B}$$

\subsection*{Exemple}

Considérons le cas~:

\begin{Eqts}
A = a_1 \otimes ... \otimes a_m \\
B = b_1 \otimes ... \otimes b_n
\end{Eqts}

En utilisant $p$ fois la récurrence, on obtient~:

$$\contraction{A}{p}{B} = \scalaire{a_m}{b_1} \cdot ... \cdot \scalaire{a_{m-p+1}}{b_p} \cdot a_1 \otimes ... \otimes a_{m-p} \otimes b_{p+1} \otimes ... \otimes b_n$$

soit un tenseur d'ordre $m - p + n - p$.

\section{Contraction double généralisée}

On définit également la contraction double utilisant trois facteurs par~:

$$\dblecont{Y}{m}{T}{n}{X} = \contraction{ Y }{m}{ \contraction{T}{n}{X} } = \contraction{  \contraction{Y}{m}{T} }{n}{ X }$$

\subsection*{Notation}

On note~:

\begin{Eqts}
\braket{y}{A}{x} = \dblecont{y}{1}{A}{1}{x} \\
y \cdot A \cdot x = \braket{y}{A}{x}
\end{Eqts}

\section{Coordonnées}

Si chaque $E_k$ dispose d'une base de vecteurs $(e_k^1,e_k^2,...e_k^{N_k})$, tout tenseur $T \in \tenseur_n(E_1,E_2,...,E_n)$ peut être écrit sous la forme~:

$$T = \sum_{i,...,r} \theta_{i...r} \cdot e_1^i \otimes ... \otimes e_n^r$$

On dit que les $\theta_{i...r}$ sont les coordonnées de $T$. L'indépendance linéaire des $e_k^i$ nous garantit que ces coordonnées sont uniques.

Il y a donc~:

$$N = \prod_i N_i$$

coordonnées déterminant un tenseur.

\subsection*{Bases orthonormées}

Si les bases sont orthonormées~:

$$\scalaire{e_k^i}{e_k^j} = \indicatrice_{ij}$$

les coordonnées s'obtiennent aisément au moyen des contractions. On évalue~:

\begin{Eqts}
\contraction{e_n^s \otimes ... \otimes e_1^j}{n}{T} = \sum_{i,...,r} \theta_{i...r} \cdot \scalaire{e_1^j}{e_1^i} \cdot ... \cdot \scalaire{e_n^s}{e_n^r} \\
\contraction{e_n^s \otimes ... \otimes e_1^j}{n}{T} = \sum_{i,...,s} \theta_{i...r} \cdot \indicatrice_{ji} \cdot ... \cdot \indicatrice_{sr}
\end{Eqts}

Le produit des deltas de Kronecker s'annulant partout sauf aux indices $(i,...,r) = (j,...,s)$, on a finalement~:

$$\contraction{e_n^s \otimes ... \otimes e_1^j}{n}{T} = \theta_{j...s}$$

ce qui nous donne les valeurs des $\theta_{j...s}$.

\section{Norme}

La norme d'un tenseur $A$ est analogue aux normes dérivées d'un produit scalaire. On utilise ici la contraction maximale et le tenseur duel de $A$, afin que les espaces des vecteurs soient compatibles~:

$$\norme{A} = \sqrt{A^\dual : A}$$

\subsection*{Bases orthonormées}

Supposons que $A$ soit représenté par rapport aux bases orthonormées $(e_k^1,e_k^2,...e_k^{N_k})$~:

$$A = \sum_{i,...,r} \theta_{i...r} \cdot e_1^i \otimes ... \otimes e_n^r$$

pour certains $\theta_{i...r} \in \corps$. La norme nous donne dans ce cas~:

$$\norme{A} = \sqrt{A^\dual : A} = \sqrt{\sum_{i,...,r} \abs{\theta_{i...r}}^2$$

\section{Tenseur identité}

Le tenseur identité $\tenseuridentite \in \tenseur_2(E)$ est défini par~:

$$\tenseuridentite \cdot u = \contraction{\tenseuridentite}{1}{u} = u$$

pour tout $u \in E$.

\subsection*{Base orthonormée}

Soit $(e_1,...,e_n)$ une base orthonormée de $E$. On a alors~:

$$\tenseuridentite \cdot e_i = e_i$$

et~:

$$\contraction{\tenseuridentite}{2}{e_j \otimes e_i} = \scalaire{\tenseuridentite \cdot e_i}{e_j} = \indicatrice_{ij}$$

On en conclut que~:

$$\tenseuridentite = \sum_{i,j} \indicatrice_{ij} \cdot e_i \otimes e_j = \sum_i e_i \otimes e_i$$

\section{Inverse}

Soit un tenseur $T \in \tenseur_2(E)$. Si il existe un tenseur $T^{-1} \in \tenseur_2(E)$ dont les contractions d'ordre $1$ avec $T$ donnent le tenseur identité~:

$$T \cdot T^{-1} = \contraction{T}{1}{T^{-1}} = T^{-1} \cdot T = \contraction{T^{-1}}{1}{T} = \tenseuridentite$$

on dit que $T^{-1}$ est l'inverse de $T$.

\section{Trace}

Soit $(e_1,...,e_n)$ est une base orthonormée de $E$ et le tenseur $T \in \tenseur_2(E)$~:

$$T = \sum_{i,j} \theta_{ij} \cdot e_i \otimes e_j$$

On définit sa trace par~:

$$\trace T = T : \tenseuridentite = \sum_{i,j} \theta_{ij} \cdot \indicatrice_{ij} = \sum_i \theta_{ii}$$

\section{Cadre}

On dit que les vecteurs $e_i \in E$ forment un cadre de $E$ si~:

$$\sum_i e_i \otimes e_i = \tenseuridentite$$

On a alors, pour tout vecteur $u$ de $E$~:

$$\sum_i \scalaire{e_i \otimes e_i}{u} = u$$

c'est-à-dire~:

$$u = \sum_i \scalaire{e_i}{u} e_i$$

par définition de la contraction. Prenant le produit scalaire avec un autre vecteur $v \in F$, on obtient~:

$$\scalaire{v}{u} = \sum_i \scalaire{v}{e_i}\scalaire{e_i}{u}$$

\section{Représentation matricielle}

Soit les vecteurs $u \in F = \matrice(K,m,1) \equiv \corps^m$ et $v,w \in E = \matrice(K,n,1) \equiv \corps^n$. La matrice~:

\begin{Eqts}
A = u \cdot v^\dual =
\begin{Matrix}{cccc}
u_1 \cdot \bar{v}_1 & u_1 \cdot \bar{v}_2 & \ldots & u_1 \cdot \bar{v}_n \\
u_21 \cdot \bar{v}_1 & u_2 \cdot \bar{v}_2 & \ldots & u_2 \cdot \bar{v}_n \\
\vdots &        & \ddots &  \vdots \\
u_m \cdot \bar{v}_1 & u_m \cdot \bar{v}_2 & \ldots & u_m \cdot \bar{v}_n
\end{Matrix}
\end{Eqts}

représente une application linéaire de $\corps^n$ vers $\corps^m$. Comme le produit matriciel est associatif, cette fonction possède la propriété~:

$$A \cdot w = (u \cdot v^\dual) \cdot w = u \cdot (v^\dual \cdot w) = u \cdot \scalaire{v}{w}$$

Ce résultat étant identique à la définition d'un tenseur d'ordre deux, on voit que les matrices sont équivalentes à des tenseurs d'ordre deux~: $\matrice(K,m,n) \equiv \tenseur_2(\corps^m,\corps^n)$. On parle donc de tenseurs matriciels pour désigner cette représentation. On définit par équivalence le produit tensoriel de deux vecteurs matriciels par~:

$$u \otimes v = u \cdot v^\dual$$

\section{Contraction d'ordre $1$}

Soit les espaces vectoriels $E_1,E_2,E_3$ sur $\corps$ et les suites de vecteurs $(e_k^1,e_k^2,...e_k^{N_k})$ formant des bases orthonormées des $E_k$. Soit les tenseurs~:

\begin{Eqts}
\mathcal{A} = \sum_{i,k} a_{ik} \cdot e_1^i \otimes e_2^k \\
\mathcal{B} = \sum_{l,j} b_{lj} \cdot e_2^l \otimes e_3^j
\end{Eqts}

où $a_{ij},b_{ij} \in \corps$. Calculons leur contraction d'ordre $1$~:

\begin{Eqts}
\mathcal{C} = \contraction{ \mathcal{A} }{1}{ \mathcal{B} } = \sum_{i,j,k,l} a_{ik} \cdot b_{lj} \cdot \scalaire{e_2^k}{e_2^l} \cdot e_1^i \otimes e_3^j \\
\mathcal{C} = \sum_{i,j,k,l} a_{ik} \cdot b_{lj} \cdot \indicatrice_{kl} \cdot e_1^i \otimes e_3^j \\
\mathcal{C} = \sum_{i,j,k} a_{ik} \cdot b_{kj} \cdot e_1^i \otimes e_3^j
\end{Eqts}

On voit que les coordonnées obtenues~:

$$c_{ij} = \sum_k a_{ik} \cdot b_{kj}$$

correspondent à une matrice $C = ( c_{ij} )_{i,j}$ telle que~:

$$C = A \cdot B$$

où les matrices $A$ et $B$ sont données par~:

\begin{Eqts}
A = ( a_{ij} )_{i,j} \\
B = ( b_{ij} )_{i,j}
\end{Eqts}

La contraction d'ordre $1$ correspond donc au produit matriciel, qui correspond lui-même à la composition d'application linéaires.

\section{Contraction maximale}

Soit les espaces vectoriels $E_1,E_2,E_3$ sur $\corps$ et les suites de vecteurs $(e_k^1,e_k^2,...e_k^{N_k})$ formant des bases orthonormées des $E_k$. Soit les tenseurs~:

\begin{Eqts}
\mathcal{A} = \sum_{i,k} a_{ik} \cdot e_1^i \otimes e_2^k \\
\mathcal{B} = \sum_{l,j} b_{lj} \cdot e_2^l \otimes e_3^j
\end{Eqts}

où $a_{ij},b_{ij} \in \corps$. Calculons leur contraction maximale~:

\begin{Eqts}
\alpha = \mathcal{A} : \mathcal{B} = \contraction{ \mathcal{A} }{2}{ \mathcal{B} } = \sum_{i,j,k,l} a_{ik} \cdot b_{lj} \cdot \indicatrice_{kl} \cdot \indicatrice_{ij} \\
\alpha = \sum_{i,k} a_{ik} \cdot b_{ki}
\end{Eqts}

Ce résultat nous incite à définir l'opération équivalente sur les matrices associées $A = ( a_{ij} )_{i,j}$ et $B = ( b_{ij} )_{i,j}$~:

$$A : B = \sum_{i,j} a_{ij} \cdot b_{ji}$$

\section{Base canonique}

Soit $\canonique_{m,i}$ les vecteurs de la base canonique de $\corps^m$ et $\canonique_{n,i}$ les vecteurs de la base canonique de $\corps^n$. Si $A = (a_{ij})_{i,j} \in \matrice(\corps,m,n)$, on a clairement~:

$$A = \sum_{i = 1}^m \sum_{j = 1}^n a_{ij} \cdot \canonique_{m,i} \otimes \canonique_{n,j} = \sum_{i = 1}^m \sum_{j = 1}^n a_{ij} \cdot \canonique_{m,i} \cdot \canonique_{n,j}^\dual$$

\subsection*{Lignes}

On a~:

$$\canonique_{m,k}^\dual \cdot A = \sum_{k,i,j} a_{ij} \cdot \indicatrice_{ik} \cdot \canonique_{n,j}^\dual = \sum_j a_{kj} \cdot \canonique_{n,j}^\dual$$

La matrice de taille $(1,n)$ obtenue est donc la $k^{eme}$ ligne de $A$~:

$$\canonique_{m,k}^\dual \cdot A = \ligne_k A$$

\subsection*{Colonnes}

On a~:

$$A \cdot \canonique_{n,k} = \sum_{k,i,j} a_{ij} \cdot \canonique_{m,i} \cdot \indicatrice_{jk} = \sum_i a_{ik} \cdot \canonique_{m,i}$$

La matrice de taille $(m,1)$ obtenue est donc la $k^{eme}$ colonne de $A$~:

$$A \cdot \canonique_{n,k} = \colonne_k A$$

\subsection*{Identité}

Dans le cas où $m = n$, on a en particulier~:

$$\sum_{i = 1}^n \canonique_{n,i} \otimes \canonique_{n,i} = \sum_{i = 1}^n \canonique_{n,i} \cdot \canonique_{n,i}^\dual = I$$

\section{Normes matricielles}

La norme de Frobenius d'une matrice $A = ( a_{ij} )_{i,j}$ est la norme du tenseur associé. On a donc~:

$$\norme{A}_F = \sqrt{A^\dual : A} = \sqrt{\sum_{i,j} \bar{a}_{ij} \cdot a_{ij} } = \sqrt{ \sum_{i,j} \abs{a_{ij}}^2 }$$

\subsection*{Trace}

La trace d'une matrice $A = (a_{ij})_{i,j} \in \matrice(K,m,n)$ est la trace du tenseur sous-jacent, et donc~:

$$\trace(A) = \sum_{i \in I} a_{ii}$$

où $I = \{1, 2, ..., \min \{ m , n \} \}$.

\end{document}
