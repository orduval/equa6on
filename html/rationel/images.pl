# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \frac{a}{b} \cdot \frac{c}{d} \cdot \frac{d}{c} \cdot \frac{b}{a}...
...t \frac{b}{a} = \frac{a}{b} \cdot \frac{b}{a} = \frac{a \cdot b}{b \cdot a} = 1$">|; 

$key = q/(.,.);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$(.,.)$">|; 

$key = q/(a,1)=aslash1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$(a,1) = a/1$">|; 

$key = q/(a,b)+(c,d);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$(a,b) + (c,d)$">|; 

$key = q/(a,b)insetZ^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$(a,b) \in \setZ^2$">|; 

$key = q/(setQ,+,cdot);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img191.svg"
 ALT="$(\setQ,+,\cdot)$">|; 

$key = q/-aslashb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$-a/b$">|; 

$key = q/1=1slash1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$1 = 1 / 1$">|; 

$key = q/F(a,b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$F(a,b)$">|; 

$key = q/a,b,c,d,e,finsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img21.svg"
 ALT="$a,b,c,d,e,f \in \setZ$">|; 

$key = q/a,b,c,dinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img18.svg"
 ALT="$a,b,c,d \in \setZ$">|; 

$key = q/a,b,c,dinsetZsetminus{0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$a,b,c,d \in \setZ \setminus \{ 0 \}$">|; 

$key = q/a,b,ninsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img54.svg"
 ALT="$a,b,n \in \setZ$">|; 

$key = q/a,binsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$a,b \in \setZ$">|; 

$key = q/a,bne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img45.svg"
 ALT="$a,b \ne 0$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$a$">|; 

$key = q/ainsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img39.svg"
 ALT="$a \in \setZ$">|; 

$key = q/aslashb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$a / b$">|; 

$key = q/b,c,dne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img147.svg"
 ALT="$b,c,d \ne 0$">|; 

$key = q/b,d,fne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$b,d,f \ne 0$">|; 

$key = q/b,d,fstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img110.svg"
 ALT="$b,d,f \strictsuperieur 0$">|; 

$key = q/b,dge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img87.svg"
 ALT="$b,d \ge 0$">|; 

$key = q/b,dle0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img96.svg"
 ALT="$b,d \le 0$">|; 

$key = q/b,dne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img19.svg"
 ALT="$b,d \ne 0$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$b$">|; 

$key = q/b=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$b = 0$">|; 

$key = q/b=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$b = 1$">|; 

$key = q/bcdotd=(-b)cdot(-d)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img97.svg"
 ALT="$b \cdot d = (-b) \cdot (-d) \strictsuperieur 0$">|; 

$key = q/bcdotdstrictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img92.svg"
 ALT="$b \cdot d \strictinferieur 0$">|; 

$key = q/bcdotdstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img88.svg"
 ALT="$b \cdot d \strictsuperieur 0$">|; 

$key = q/bge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img94.svg"
 ALT="$b \ge 0$">|; 

$key = q/ble0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img90.svg"
 ALT="$b \le 0$">|; 

$key = q/bne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img34.svg"
 ALT="$b \ne 0$">|; 

$key = q/bslasha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$b / a$">|; 

$key = q/bslashb=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$b / b = 1$">|; 

$key = q/bstrictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img81.svg"
 ALT="$b \strictinferieur 0$">|; 

$key = q/bstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img76.svg"
 ALT="$b \strictsuperieur 0$">|; 

$key = q/delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img197.svg"
 ALT="$\delta$">|; 

$key = q/deltastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img200.svg"
 ALT="$\delta \strictsuperieur 0$">|; 

$key = q/dge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img91.svg"
 ALT="$d \ge 0$">|; 

$key = q/displaystyle(-m)diventiere(-n)=mdiventieren;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle (-m) \diventiere (-n) = m \diventiere n$">|; 

$key = q/displaystyle(-m)diventieren=-(mdiventieren);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle (-m) \diventiere n = -(m \diventiere n)$">|; 

$key = q/displaystyle(-u)+(-v)ge(-u)+(-v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\displaystyle (-u)+(-v) \ge (-u)+(-v)$">|; 

$key = q/displaystyle(a,b)cdot(c,d)=(acdotc,bcdotd);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle (a,b) \cdot (c,d) = (a \cdot c, b \cdot d)$">|; 

$key = q/displaystyle(acdotf+ecdotb)cdotdcdothle(ccdoth+gcdotd)cdotbcdotf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\displaystyle (a \cdot f + e \cdot b) \cdot d \cdot h \le (c \cdot h + g \cdot d) \cdot b \cdot f$">|; 

$key = q/displaystyle(i_0,...,i_m)in{0,1,2,3,4,5,6,7,8,9}^{m+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\displaystyle (i_0,...,i_m) \in \{ 0,1,2,3,4,5,6,7,8,9 \}^{m + 1}$">|; 

$key = q/displaystyle(j_1,...,j_n)in{0,1,2,3,4,5,6,7,8,9}^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle (j_1,...,j_n) \in \{ 0,1,2,3,4,5,6,7,8,9 \}^n$">|; 

$key = q/displaystyle(mdiventieren)cdot(idiventierej)=(mcdoti)diventiere(ncdotj);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle (m \diventiere n) \cdot (i \diventiere j) = (m \cdot i) \diventiere (n \cdot j)$">|; 

$key = q/displaystyle(x+y)^2=(x+y)cdot(x+y)=xcdot(x+y)+ycdot(x+y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img185.svg"
 ALT="$\displaystyle (x + y)^2 = (x + y) \cdot (x + y) = x \cdot (x + y) + y \cdot (x + y)$">|; 

$key = q/displaystyle(x+y)^2=x^2+2cdotxcdoty+y^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img188.svg"
 ALT="$\displaystyle (x + y)^2 = x^2 + 2 \cdot x \cdot y + y^2$">|; 

$key = q/displaystyle(x+y)^2=x^2+xcdoty+xcdoty+y^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img187.svg"
 ALT="$\displaystyle (x + y)^2 = x^2 + x \cdot y + x \cdot y + y^2$">|; 

$key = q/displaystyle(x+y)^2=xcdotx+xcdoty+ycdotx+ycdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img186.svg"
 ALT="$\displaystyle (x + y)^2 = x \cdot x + x \cdot y + y \cdot x + y \cdot y$">|; 

$key = q/displaystyle(x-y)^2=(x+(-y))^2=x^2+2cdotxcdot(-y)+y^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img189.svg"
 ALT="$\displaystyle (x - y)^2 = (x + (-y))^2 = x^2 + 2 \cdot x \cdot (-y) + y^2$">|; 

$key = q/displaystyle(x-y)^2=x^2-2cdotxcdoty+y^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img190.svg"
 ALT="$\displaystyle (x - y)^2 = x^2 - 2 \cdot x \cdot y + y^2$">|; 

$key = q/displaystyle-7512,34=-parentheses{2+1cdot10+5cdot10^2+7cdot10^3+3cdotunsur{10}+4cdotunsur{10^2}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img145.svg"
 ALT="$\displaystyle -7512,34 = -\parentheses{2 + 1 \cdot 10 + 5 \cdot 10^2 + 7 \cdot 10^3 + 3 \cdot \unsur{10} + 4 \cdot \unsur{10^2}}$">|; 

$key = q/displaystyle-frac{a}{b}=frac{-a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.70ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle -\frac{a}{b} = \frac{-a}{b}$">|; 

$key = q/displaystyle-ule-v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle -u \le -v$">|; 

$key = q/displaystyle-vge-u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\displaystyle -v \ge -u$">|; 

$key = q/displaystyle0=frac{0}{1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle 0 = \frac{0}{1}$">|; 

$key = q/displaystyle0strictinferieurdeltastrictinferieurepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img198.svg"
 ALT="$\displaystyle 0 \strictinferieur \delta \strictinferieur \epsilon$">|; 

$key = q/displaystyleF(a,b)=left{frac{acdotn}{bcdotn}:ninsetZ,nne0right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.44ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle F(a,b) = \left\{ \frac{a \cdot n}{b \cdot n} : n \in \setZ,  n \ne 0 \right\}$">|; 

$key = q/displaystylea=frac{a}{1}=(a,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle a = \frac{a}{1} = (a,1)$">|; 

$key = q/displaystylea=i-j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle a = i - j$">|; 

$key = q/displaystyleabs{x}=max{x,-x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle \abs{x} = \max \{ x , -x \}$">|; 

$key = q/displaystyleacdot1ge0cdotb=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle a \cdot 1 \ge 0 \cdot b = 0$">|; 

$key = q/displaystyleacdot1le0cdotb=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle a \cdot 1 \le 0 \cdot b = 0$">|; 

$key = q/displaystyleacdotdcdotfcdoth+ecdothcdotbcdotdleccdotbcdotfcdoth+gcdotfcdotbcdotd;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\displaystyle a \cdot d \cdot f \cdot h + e \cdot h \cdot b \cdot d \le c \cdot b \cdot f \cdot h + g \cdot f \cdot b \cdot d$">|; 

$key = q/displaystyleacdotdcdotfcdothleccdotbcdotfcdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\displaystyle a \cdot d \cdot f \cdot h \le c \cdot b \cdot f \cdot h$">|; 

$key = q/displaystyleacdotdgeccdotb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle a \cdot d \ge c \cdot b$">|; 

$key = q/displaystyleacdotdleccdotb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle a \cdot d \le c \cdot b$">|; 

$key = q/displaystyleacdotecdotdcdotfleccdotecdotbcdotf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle a \cdot e \cdot d \cdot f \le c \cdot e \cdot b \cdot f$">|; 

$key = q/displaystyleadiventiereb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle a \diventiere b$">|; 

$key = q/displaystyleage0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle a \ge 0$">|; 

$key = q/displaystyleale0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle a \le 0$">|; 

$key = q/displaystyleaslashb=frac{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle a/b = \frac{a}{b}$">|; 

$key = q/displaystylecrochets{frac{a}{b}cdotfrac{c}{d}}cdotfrac{e}{f}=frac{acdotc}{bcdotd}cdotfrac{e}{f}=frac{acdotccdote}{bcdotdcdotf};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.89ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \crochets{\frac{a}{b} \cdot \frac{c}{d}} \cdot \frac{e}{f} = \fra...
...t c}{b \cdot d} \cdot \frac{e}{f} = \frac{a \cdot c \cdot e}{b \cdot d \cdot f}$">|; 

$key = q/displaystylecrochets{parentheses{frac{a}{b}}^{-1}}^{-1}=crochets{frac{b}{a}}^{-1}=frac{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.13ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \crochets{\parentheses{\frac{a}{b}}^{-1}}^{-1} = \crochets{\frac{b}{a}}^{-1} = \frac{a}{b}$">|; 

$key = q/displaystyledelta=frac{m}{2n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img199.svg"
 ALT="$\displaystyle \delta = \frac{m}{2  n}$">|; 

$key = q/displaystyledelta=frac{m}{2n}strictinferieurfrac{m}{n}=epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img204.svg"
 ALT="$\displaystyle \delta = \frac{m}{2  n} \strictinferieur \frac{m}{n} = \epsilon$">|; 

$key = q/displaystyleecdothcdotbcdotdlegcdotfcdotbcdotd;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\displaystyle e \cdot h \cdot b \cdot d \le g \cdot f \cdot b \cdot d$">|; 

$key = q/displaystyleecdothlegcdotf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle e \cdot h \le g \cdot f$">|; 

$key = q/displaystyleege0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle e \ge 0$">|; 

$key = q/displaystyleepsilon=frac{m}{n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img194.svg"
 ALT="$\displaystyle \epsilon = \frac{m}{n}$">|; 

$key = q/displaystyleepsilon=y-xstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img207.svg"
 ALT="$\displaystyle \epsilon = y - x \strictsuperieur 0$">|; 

$key = q/displaystylefrac{-a}{-b}=frac{(-1)cdot(-a)}{(-1)cdot(-b)}=frac{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle \frac{-a}{-b} = \frac{(-1) \cdot (-a)}{(-1) \cdot (-b)} = \frac{a}{b}$">|; 

$key = q/displaystylefrac{0}{n}=frac{0cdotn}{1cdotn}=frac{0}{1}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle \frac{0}{n} = \frac{0 \cdot n}{1 \cdot n} = \frac{0}{1} = 0$">|; 

$key = q/displaystylefrac{acdote}{bcdotf}lefrac{ccdote}{dcdotf};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.83ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\displaystyle \frac{a \cdot e}{b \cdot f} \le \frac{c \cdot e}{d \cdot f}$">|; 

$key = q/displaystylefrac{acdotf+ecdotb}{bcdotf}lefrac{ccdoth+gcdotd}{dcdoth};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.40ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\displaystyle \frac{a \cdot f + e \cdot b}{b \cdot f} \le \frac{c \cdot h + g \cdot d}{d \cdot h}$">|; 

$key = q/displaystylefrac{acdotn}{bcdotn}=frac{a}{b}cdotfrac{n}{n}=frac{a}{b}cdot1=frac{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.38ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \frac{a \cdot n}{b \cdot n} = \frac{a}{b} \cdot \frac{n}{n} = \frac{a}{b} \cdot 1 = \frac{a}{b}$">|; 

$key = q/displaystylefrac{a}{-b}=frac{(-1)cdota}{(-1)cdot(-b)}=frac{-a}{b}=-frac{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \frac{a}{-b} = \frac{(-1) \cdot a}{(-1) \cdot (-b)} = \frac{-a}{b} = -\frac{a}{b}$">|; 

$key = q/displaystylefrac{a}{b}+0=frac{a}{b}+frac{0}{1}=frac{acdot1+0cdotb}{bcdot1}=frac{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \frac{a}{b} + 0 = \frac{a}{b} + \frac{0}{1} = \frac{a \cdot 1 + 0 \cdot b}{b \cdot 1} = \frac{a}{b}$">|; 

$key = q/displaystylefrac{a}{b}+frac{-a}{b}=frac{a-a}{b}=frac{0}{b}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \frac{a}{b} + \frac{-a}{b} = \frac{a - a}{b} = \frac{0}{b} = 0$">|; 

$key = q/displaystylefrac{a}{b}+frac{c}{d}=frac{acdotd+ccdotb}{bcdotd};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle \frac{a}{b} + \frac{c}{d} = \frac{a \cdot d + c \cdot b}{b \cdot d}$">|; 

$key = q/displaystylefrac{a}{b}+frac{c}{d}=frac{acdotd}{bcdotd}+frac{ccdotb}{dcdotb};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle \frac{a}{b} + \frac{c}{d} = \frac{a \cdot d}{b \cdot d} + \frac{c \cdot b}{d \cdot b}$">|; 

$key = q/displaystylefrac{a}{b}+frac{e}{f}lefrac{c}{d}+frac{g}{h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.79ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\displaystyle \frac{a}{b} + \frac{e}{f} \le \frac{c}{d} + \frac{g}{h}$">|; 

$key = q/displaystylefrac{a}{b}-frac{c}{d}=frac{a}{b}+frac{-c}{d}=frac{acdotd-ccdotb}{bcdotd};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle \frac{a}{b} - \frac{c}{d} = \frac{a}{b} + \frac{-c}{d} = \frac{a \cdot d - c \cdot b}{b \cdot d}$">|; 

$key = q/displaystylefrac{a}{b}=(a,b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle \frac{a}{b} = (a,b)$">|; 

$key = q/displaystylefrac{a}{b}=F(a,b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \frac{a}{b} = F(a,b)$">|; 

$key = q/displaystylefrac{a}{b}cdot1=frac{a}{b}cdotfrac{1}{1}=frac{acdot1}{bcdot1}=frac{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \frac{a}{b} \cdot 1 = \frac{a}{b} \cdot \frac{1}{1} = \frac{a \cdot 1}{b \cdot 1} = \frac{a}{b}$">|; 

$key = q/displaystylefrac{a}{b}cdotcrochets{frac{c}{d}cdotfrac{e}{f}}=crochets{frac{a}{b}cdotfrac{c}{d}}cdotfrac{e}{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \frac{a}{b} \cdot \crochets{\frac{c}{d} \cdot \frac{e}{f}} = \crochets{\frac{a}{b} \cdot \frac{c}{d}} \cdot \frac{e}{f}$">|; 

$key = q/displaystylefrac{a}{b}cdotcrochets{frac{c}{d}cdotfrac{e}{f}}=frac{a}{b}cdotfrac{ccdote}{dcdotf}=frac{acdotccdote}{bcdotdcdotf};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \frac{a}{b} \cdot \crochets{\frac{c}{d} \cdot \frac{e}{f}} = \fra...
...\cdot \frac{c \cdot e}{d \cdot f} = \frac{a \cdot c \cdot e}{b \cdot d \cdot f}$">|; 

$key = q/displaystylefrac{a}{b}cdotfrac{b}{a}=frac{acdotb}{bcdota}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \frac{a}{b} \cdot \frac{b}{a} = \frac{a \cdot b}{b \cdot a} = 1$">|; 

$key = q/displaystylefrac{a}{b}cdotfrac{c}{d}=frac{acdotc}{bcdotd};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.38ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \frac{a}{b} \cdot \frac{c}{d} = \frac{a \cdot c}{b \cdot d}$">|; 

$key = q/displaystylefrac{a}{b}cdotfrac{c}{d}=frac{acdotc}{bcdotd}=frac{ccdota}{dcdotb}=frac{c}{d}cdotfrac{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.38ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \frac{a}{b} \cdot \frac{c}{d} = \frac{a \cdot c}{b \cdot d} = \frac{c \cdot a}{d \cdot b} = \frac{c}{d} \cdot \frac{a}{b}$">|; 

$key = q/displaystylefrac{a}{b}cdotfrac{c}{d}cdotfrac{e}{f}=frac{a}{b}cdotcrochets{frac{c}{d}cdotfrac{e}{f}}=crochets{frac{a}{b}cdotfrac{c}{d}}cdotfrac{e}{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \frac{a}{b} \cdot \frac{c}{d} \cdot \frac{e}{f} = \frac{a}{b} \cd...
...\cdot \frac{e}{f}} = \crochets{\frac{a}{b} \cdot \frac{c}{d}} \cdot \frac{e}{f}$">|; 

$key = q/displaystylefrac{a}{b}lefrac{c}{d};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle \frac{a}{b} \le \frac{c}{d}$">|; 

$key = q/displaystylefrac{a}{n}+frac{b}{n}=(a+b)cdotunsur{n}=frac{a+b}{n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \frac{a}{n} + \frac{b}{n} = (a + b) \cdot \unsur{n} = \frac{a + b}{n}$">|; 

$key = q/displaystylefrac{a}{n}+frac{b}{n}=frac{a}{1}cdotunsur{n}+frac{b}{1}cdotunsur{n}=acdotunsur{n}+bcdotunsur{n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle \frac{a}{n} + \frac{b}{n} = \frac{a}{1} \cdot \unsur{n} + \frac{b}{1} \cdot \unsur{n} = a \cdot \unsur{n} + b \cdot \unsur{n}$">|; 

$key = q/displaystylefrac{d}{c}cdotfrac{b}{a}=parentheses{frac{a}{b}cdotfrac{c}{d}}^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \frac{d}{c} \cdot \frac{b}{a} = \parentheses{\frac{a}{b} \cdot \frac{c}{d}}^{-1}$">|; 

$key = q/displaystylefrac{e}{f}lefrac{g}{h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.79ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\displaystyle \frac{e}{f} \le \frac{g}{h}$">|; 

$key = q/displaystylefrac{n}{n}=frac{1}{1}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \frac{n}{n} = \frac{1}{1} = 1$">|; 

$key = q/displaystylefrac{x}{y}=frac{aslashb}{cslashd}=frac{a}{b}cdotfrac{d}{c};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\displaystyle \frac{x}{y} = \frac{a/b}{c/d} = \frac{a}{b} \cdot \frac{d}{c}$">|; 

$key = q/displaystylefrac{x}{y}=q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.79ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img150.svg"
 ALT="$\displaystyle \frac{x}{y} = q$">|; 

$key = q/displaystylei_m...i_0,j_1...j_n=-parentheses{i_0+...+i_mcdot10^m+j_1cdotunsur{10}+...+j_ncdotunsur{10^n}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img144.svg"
 ALT="$\displaystyle i_m ... i_0, j_1 ... j_n = -\parentheses{i_0 + ... + i_m \cdot 10^m + j_1 \cdot \unsur{10} + ... + j_n \cdot \unsur{10^n}}$">|; 

$key = q/displaystylei_m...i_0,j_1...j_n=i_0+...+i_mcdot10^m+j_1cdotunsur{10}+...+j_ncdotunsur{10^n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle i_m ... i_0, j_1 ... j_n = i_0 + ... + i_m \cdot 10^m + j_1 \cdot \unsur{10} + ... + j_n \cdot \unsur{10^n}$">|; 

$key = q/displaystyleleft(frac{a}{b}right)^n=frac{a}{b}cdot...cdotfrac{a}{b}=frac{a^n}{b^n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.85ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle \left( \frac{a}{b} \right)^n = \frac{a}{b} \cdot ... \cdot \frac{a}{b} = \frac{a^n}{b^n}$">|; 

$key = q/displaystyleleft(frac{a}{b}right)^{-1}=frac{b}{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \left( \frac{a}{b} \right)^{-1} = \frac{b}{a}$">|; 

$key = q/displaystyleleft(frac{a}{b}right)^{-n}=left(frac{a^n}{b^n}right)^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.13ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img158.svg"
 ALT="$\displaystyle \left( \frac{a}{b} \right)^{-n} = \left( \frac{a^n}{b^n} \right)^{-1}$">|; 

$key = q/displaystyleleft(frac{a}{b}right)^{-n}=left(frac{b}{a}right)^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.80ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle \left( \frac{a}{b} \right)^{-n} = \left( \frac{b}{a} \right)^n$">|; 

$key = q/displaystylem=-a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle m = -a$">|; 

$key = q/displaystylem=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle m = a$">|; 

$key = q/displaystylemcdotnstrictinferieurmcdot2cdotn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img203.svg"
 ALT="$\displaystyle m \cdot n \strictinferieur m \cdot 2 \cdot n$">|; 

$key = q/displaystylemdiventiere(-n)=-(mdiventieren);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle m \diventiere (-n) = - (m \diventiere n)$">|; 

$key = q/displaystylemmodulon=imoduloj=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle m \modulo n = i \modulo j = 0$">|; 

$key = q/displaystylemstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img196.svg"
 ALT="$\displaystyle m \strictsuperieur 0$">|; 

$key = q/displaystylen=-b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle n = -b$">|; 

$key = q/displaystylen=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle n = b$">|; 

$key = q/displaystylendiventieren=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle n \diventiere n = 1$">|; 

$key = q/displaystylenstrictinferieur2n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img201.svg"
 ALT="$\displaystyle n \strictinferieur 2  n$">|; 

$key = q/displaystylep^2=pcdotpge0cdotp=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img169.svg"
 ALT="$\displaystyle p^2 = p \cdot p \ge 0 \cdot p = 0$">|; 

$key = q/displaystyleq=xcdoty^{-1}=frac{a}{b}cdotfrac{d}{c};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle q = x \cdot y^{-1} = \frac{a}{b} \cdot \frac{d}{c}$">|; 

$key = q/displaystylesetQ=left{frac{a}{b}:a,binsetZ,bne0right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.44ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \setQ = \left\{ \frac{a}{b} : a,b \in \setZ,  b \ne 0 \right\}$">|; 

$key = q/displaystylesetQ=left{frac{i-j}{k-l}:i,j,k,linsetN,knelright};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \setQ = \left\{ \frac{i - j}{k - l} : i,j,k,l \in \setN,  k \ne l \right\}$">|; 

$key = q/displaystylesetQ^+={xinsetQ:xge0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle \setQ^+ = \{ x \in \setQ : x \ge 0 \}$">|; 

$key = q/displaystylesetQ^-={xinsetQ:xle0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle \setQ^- = \{ x \in \setQ : x \le 0 \}$">|; 

$key = q/displaystylesigne(x)=cases{1&text{si}xge0-1&text{si}xstrictinferieur0cases{;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle \signe(x) =
\begin{cases}
1 &amp; \text{ si } x \ge 0 \\\\
-1 &amp; \text{ si } x \strictinferieur 0
\end{cases}$">|; 

$key = q/displaystyleu+(-u)+(-v)gev+(-u)+(-v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle u + (-u) + (-v) \ge v + (-u) + (-v)$">|; 

$key = q/displaystyleu=frac{e}{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.79ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\displaystyle u = \frac{e}{f}$">|; 

$key = q/displaystyleugev;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\displaystyle u \ge v$">|; 

$key = q/displaystyleulev;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\displaystyle u \le v$">|; 

$key = q/displaystylev=frac{g}{h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle v = \frac{g}{h}$">|; 

$key = q/displaystylex+(-u)ley+(-v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle x + (-u) \le y + (-v)$">|; 

$key = q/displaystylex+epsilon=x+y-x=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img211.svg"
 ALT="$\displaystyle x + \epsilon = x + y - x = y$">|; 

$key = q/displaystylex+uley+v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle x + u \le y + v$">|; 

$key = q/displaystylex,yge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img176.svg"
 ALT="$\displaystyle x,y \ge 0$">|; 

$key = q/displaystylex-uley-v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle x - u \le y - v$">|; 

$key = q/displaystylex=frac{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle x = \frac{a}{b}$">|; 

$key = q/displaystylex=frac{a}{b}=frac{-a}{-b}=frac{m}{n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.89ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle x = \frac{a}{b} = \frac{-a}{-b} = \frac{m}{n}$">|; 

$key = q/displaystylex=frac{a}{b}ge0=frac{0}{1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle x = \frac{a}{b} \ge 0 = \frac{0}{1}$">|; 

$key = q/displaystylex=frac{a}{b}le0=frac{0}{1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle x = \frac{a}{b} \le 0 = \frac{0}{1}$">|; 

$key = q/displaystylex=frac{m}{n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle x = \frac{m}{n}$">|; 

$key = q/displaystylex=qcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.67ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle x = q \cdot y$">|; 

$key = q/displaystylex^2=(-p)^2=(-p)cdot(-p)=pcdotp=p^2ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img174.svg"
 ALT="$\displaystyle x^2 = (-p)^2 = (-p) \cdot (-p) = p \cdot p = p^2 \ge 0$">|; 

$key = q/displaystylex^2=p^2ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img170.svg"
 ALT="$\displaystyle x^2 = p^2 \ge 0$">|; 

$key = q/displaystylex^2=xcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img165.svg"
 ALT="$\displaystyle x^2 = x \cdot x$">|; 

$key = q/displaystylex^2=xcdotxleycdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img178.svg"
 ALT="$\displaystyle x^2 = x \cdot x \le y \cdot x$">|; 

$key = q/displaystylex^2ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.46ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img175.svg"
 ALT="$\displaystyle x^2 \ge 0$">|; 

$key = q/displaystylex^2ley^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img182.svg"
 ALT="$\displaystyle x^2 \le y^2$">|; 

$key = q/displaystylex^2leycdotx=xcdotyley^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img181.svg"
 ALT="$\displaystyle x^2 \le y \cdot x = x \cdot y \le y^2$">|; 

$key = q/displaystylex^{-m+n}=x^{-m}cdotx^n=frac{x^n}{x^m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.85ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img161.svg"
 ALT="$\displaystyle x^{-m + n} = x^{-m} \cdot x^n = \frac{x^n}{x^m}$">|; 

$key = q/displaystylex^{-m-n}=x^{-m}cdotx^{-n}=unsur{x^mcdotx^n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img162.svg"
 ALT="$\displaystyle x^{-m - n} = x^{-m} \cdot x^{-n} = \unsur{x^m \cdot x^n}$">|; 

$key = q/displaystylex^{i+j}=x^icdotx^j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img163.svg"
 ALT="$\displaystyle x^{i + j} = x^i \cdot x^j$">|; 

$key = q/displaystylex^{m+n}=x^mcdotx^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.08ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img159.svg"
 ALT="$\displaystyle x^{m + n} = x^m \cdot x^n$">|; 

$key = q/displaystylex^{m-n}=x^mcdotx^{-n}=frac{x^m}{x^n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.85ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img160.svg"
 ALT="$\displaystyle x^{m - n} = x^m \cdot x^{-n} = \frac{x^m}{x^n}$">|; 

$key = q/displaystylexcdotyleycdoty=y^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img180.svg"
 ALT="$\displaystyle x \cdot y \le y \cdot y = y^2$">|; 

$key = q/displaystylexcdotzleycdotz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\displaystyle x \cdot z \le y \cdot z$">|; 

$key = q/displaystylexlexlex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img208.svg"
 ALT="$\displaystyle x \le x \le x$">|; 

$key = q/displaystylexley;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle x \le y$">|; 

$key = q/displaystylexstrictinferieurx+deltastrictinferieurx+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img209.svg"
 ALT="$\displaystyle x \strictinferieur x + \delta \strictinferieur x + \epsilon$">|; 

$key = q/displaystylexstrictinferieury;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img206.svg"
 ALT="$\displaystyle x \strictinferieur y$">|; 

$key = q/displaystylexstrictinferieurzstrictinferieury;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img212.svg"
 ALT="$\displaystyle x \strictinferieur z \strictinferieur y$">|; 

$key = q/displaystyley=frac{c}{d};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle y = \frac{c}{d}$">|; 

$key = q/displaystylez=frac{e}{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.79ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle z = \frac{e}{f}$">|; 

$key = q/displaystylez=x+delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img210.svg"
 ALT="$\displaystyle z = x + \delta$">|; 

$key = q/dle0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img95.svg"
 ALT="$d \le 0$">|; 

$key = q/dslashd=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$d / d = 1$">|; 

$key = q/ecdotf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img114.svg"
 ALT="$e \cdot f$">|; 

$key = q/epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img195.svg"
 ALT="$\epsilon$">|; 

$key = q/epsiloninsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img192.svg"
 ALT="$\epsilon \in \setQ$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img193.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/f:xmapstox^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img183.svg"
 ALT="$f : x \mapsto x^2$">|; 

$key = q/fcdothstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img124.svg"
 ALT="$f \cdot h \strictsuperieur 0$">|; 

$key = q/i,j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img40.svg"
 ALT="$i,j$">|; 

$key = q/i,jinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img164.svg"
 ALT="$i,j \in \setZ$">|; 

$key = q/m,n,i,jinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$m,n,i,j \in \setZ$">|; 

$key = q/m,ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img155.svg"
 ALT="$m,n \in \setN$">|; 

$key = q/m,ninsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img65.svg"
 ALT="$m, n \in \setZ$">|; 

$key = q/mstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img202.svg"
 ALT="$m \strictsuperieur 0$">|; 

$key = q/ninsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img27.svg"
 ALT="$n \in \setZ$">|; 

$key = q/nne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$n \ne 0$">|; 

$key = q/nstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img80.svg"
 ALT="$n \strictsuperieur 0$">|; 

$key = q/p=-x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img172.svg"
 ALT="$p = -x$">|; 

$key = q/p=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img167.svg"
 ALT="$p = x$">|; 

$key = q/pge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img168.svg"
 ALT="$p \ge 0$">|; 

$key = q/qinsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img148.svg"
 ALT="$q \in \setQ$">|; 

$key = q/setZsubseteqsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\setZ \subseteq \setQ$">|; 

$key = q/x,y,z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img107.svg"
 ALT="$x,y,z$">|; 

$key = q/x,y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img205.svg"
 ALT="$x,y$">|; 

$key = q/x,yinsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img146.svg"
 ALT="$x,y \in \setQ$">|; 

$key = q/x,yinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img184.svg"
 ALT="$x,y \in \setZ$">|; 

$key = q/x,z,u,v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img118.svg"
 ALT="$x,z,u,v$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img100.svg"
 ALT="$x$">|; 

$key = q/x=-p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img173.svg"
 ALT="$x = -p$">|; 

$key = q/xge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img166.svg"
 ALT="$x \ge 0$">|; 

$key = q/xinsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img74.svg"
 ALT="$x \in \setQ$">|; 

$key = q/xle0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img171.svg"
 ALT="$x \le 0$">|; 

$key = q/xley;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img177.svg"
 ALT="$x \le y$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img149.svg"
 ALT="$y$">|; 

$key = q/y^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img152.svg"
 ALT="$y^{-1}$">|; 

$key = q/yge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img179.svg"
 ALT="$y \ge 0$">|; 

$key = q/zge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img111.svg"
 ALT="$z \ge 0$">|; 

$key = q/{eqnarraystar}1&=&1slash10&=&0slash1-1&=&-1slash1{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img11.svg"
 ALT="\begin{eqnarray*}
1 &amp;=&amp; 1 / 1 \\\\
0 &amp;=&amp; 0 / 1 \\\\
-1 &amp;=&amp; -1 / 1
\end{eqnarray*}">|; 

1;

