# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 28.57ex; " SRC="|."$dir".q|img71.svg"
 ALT="\begin{eqnarray*}
\sum_{k = 0}^n \binome{n}{k} \cdot x^k &amp;=&amp; (1 + x) \sum_{k = 0...
...} \cdot x^k + \sum_{i = 1}^n \binome{n - 1}{i - 1} \cdot x^i \\\\
\end{eqnarray*}">|; 

$key = q/(x,y)incorps^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img86.svg"
 ALT="$(x,y) \in \corps^2$">|; 

$key = q/(x_k-x_0)^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$(x_k - x_0)^{-1}$">|; 

$key = q/1,x,...,x^{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img35.svg"
 ALT="$1,x,...,x^{n-1}$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img54.svg"
 ALT="$A$">|; 

$key = q/A=a_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img59.svg"
 ALT="$A = a_n$">|; 

$key = q/B_i^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.35ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img104.svg"
 ALT="$B_i^n$">|; 

$key = q/B_n(x,y)=B_n(y,x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img90.svg"
 ALT="$B_n(x,y) = B_n(y,x)$">|; 

$key = q/B_n:corpstimescorpsmapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img84.svg"
 ALT="$B_n : \corps \times \corps
\mapsto \corps$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img120.svg"
 ALT="$a$">|; 

$key = q/a_0,a_1,...a_nincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$a_0,a_1,...a_n \in \corps$">|; 

$key = q/a_0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img22.svg"
 ALT="$a_0 = 0$">|; 

$key = q/a_0incorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img19.svg"
 ALT="$a_0 \in \corps$">|; 

$key = q/a_{nk}incorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img66.svg"
 ALT="$a_{nk} \in \corps$">|; 

$key = q/ainmathcal{P}_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img115.svg"
 ALT="$a \in \mathcal{P}_n$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img121.svg"
 ALT="$b$">|; 

$key = q/b_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img68.svg"
 ALT="$b_0$">|; 

$key = q/b_n:corpstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img62.svg"
 ALT="$b_n : \corps \to \corps$">|; 

$key = q/b_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img82.svg"
 ALT="$b_n$">|; 

$key = q/b_{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img83.svg"
 ALT="$b_{n - 1}$">|; 

$key = q/binmathcal{P}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img116.svg"
 ALT="$b \in \mathcal{P}_m$">|; 

$key = q/cincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img112.svg"
 ALT="$c \in \corps$">|; 

$key = q/cinmathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img111.svg"
 ALT="$c \in \mathcal{F}$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\corps$">|; 

$key = q/displaystyle(q,r)=division(a,b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\displaystyle (q,r) = \division(a,b)$">|; 

$key = q/displaystyle(x+y)^2=x(x+y)+y(x+y)=x^2+2xy+y^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle (x + y)^2 = x  (x + y) + y  (x + y) = x^2 + 2  x  y + y^2$">|; 

$key = q/displaystyle0=p_n(x_k)=(x_k-x_0)cdotq_{n-1}(x_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle 0 = p_n(x_k) = (x_k - x_0) \cdot q_{n-1}(x_k)$">|; 

$key = q/displaystyleA=frac{p(x_0)}{prod_{i=1}^n(x_0-x_i)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.76ex; vertical-align: -2.37ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle A = \frac{p(x_0)}{\prod_{i = 1}^n (x_0 - x_i)}$">|; 

$key = q/displaystyleB_i^n(t)=binome{n}{i}cdott^icdot(1-t)^{n-i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle B_i^n(t) = \binome{n}{i} \cdot t^i \cdot (1 - t)^{n-i}$">|; 

$key = q/displaystyleB_n(x,y)=(x+y)^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle B_n(x,y) = (x + y)^n$">|; 

$key = q/displaystyleB_n(x,y)=sum_{k=0}^nbinome{n}{k}cdotx^kcdoty^{n-k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\displaystyle B_n(x,y) = \sum_{k = 0}^n \binome{n}{k} \cdot x^k \cdot y^{n - k}$">|; 

$key = q/displaystyleB_n(x,y)=y^ncdotleft(1+frac{x}{y}right)^n=y^ncdotb_nleft(frac{x}{y}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.80ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle B_n(x,y) = y^n \cdot \left( 1 + \frac{x}{y} \right)^n = y^n \cdot b_n\left( \frac{x}{y} \right)$">|; 

$key = q/displaystyleb_n(x)=(1+x)^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle b_n(x) = (1 + x)^n$">|; 

$key = q/displaystyleb_n(x)=(1+x)^n=sum_{k=0}^na_{nk}cdotx^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle b_n(x) = (1 + x)^n = \sum_{k = 0}^n a_{nk} \cdot x^k$">|; 

$key = q/displaystyleb_n(x)=(1+x)cdotb_{n-1}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle b_n(x) = (1 + x) \cdot b_{n-1}(x)$">|; 

$key = q/displaystylebinome{0}{0}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \binome{0}{0} = 1$">|; 

$key = q/displaystylebinome{n}{0}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \binome{n}{0} = 1$">|; 

$key = q/displaystylebinome{n}{0}=binome{n-1}{0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \binome{n}{0} = \binome{n - 1}{0}$">|; 

$key = q/displaystylebinome{n}{k}=a_{nk};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \binome{n}{k} = a_{nk}$">|; 

$key = q/displaystylebinome{n}{k}=binome{n-1}{k}+binome{n-1}{k-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle \binome{n}{k} = \binome{n - 1}{k} + \binome{n - 1}{k - 1}$">|; 

$key = q/displaystylebinome{n}{k}=binome{n}{n-k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle \binome{n}{k} = \binome{n}{n - k}$">|; 

$key = q/displaystylebinome{n}{n}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle \binome{n}{n} = 1$">|; 

$key = q/displaystylebinome{n}{n}=binome{n-1}{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle \binome{n}{n} = \binome{n - 1}{n - 1}$">|; 

$key = q/displaystylec(t)=c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle c(t) = c$">|; 

$key = q/displaystyleh=p_1-p_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle h = p_1 - p_2$">|; 

$key = q/displaystylemathcal{B}_n(c)(t)=sum_{i=0}^nccdotB_i^n(t)=csum_{i=0}^nbinome{n}{i}t^icdot(1-t)^{n-i}=c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle \mathcal{B}_n(c)(t) = \sum_{i = 0}^n c \cdot B_i^n(t) = c \sum_{i = 0}^n \binome{n}{i} t^i \cdot (1 - t)^{n-i} = c$">|; 

$key = q/displaystylemathcal{B}_n(f)(t)=sum_{i=0}^nf(islashn)cdotB_i^n(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle \mathcal{B}_n(f)(t) = \sum_{i = 0}^n f(i / n) \cdot B_i^n(t)$">|; 

$key = q/displaystylemathcal{P}_n=polynome(setR,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \mathcal{P}_n = \polynome(\setR,n)$">|; 

$key = q/displaystylemu_i(x)=x^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.75ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \mu_i(x) = x^i$">|; 

$key = q/displaystylep(r)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle p(r) = 0$">|; 

$key = q/displaystylep(x)=a_ncdotprod_{i=1}^n(x-x_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle p(x) = a_n \cdot \prod_{i=1}^n (x - x_i)$">|; 

$key = q/displaystylep(x)=sum_{i=0}^na_icdotx^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle p(x) = \sum_{i = 0}^n a_i \cdot x^i$">|; 

$key = q/displaystylep(x_0)=q(x_0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle p(x_0) = q(x_0)$">|; 

$key = q/displaystylep(x_i)=q(x_i)=0qquadi=1,2,...,n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle p(x_i) = q(x_i) = 0 \qquad i = 1,2,...,n$">|; 

$key = q/displaystylep_n(x)=(x-x_0)cdotq_{n-1}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle p_n(x) = (x - x_0) \cdot q_{n-1}(x)$">|; 

$key = q/displaystylep_n(x)=(x-x_0)cdotq_{n-1}(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle p_n(x) = (x - x_0) \cdot q_{n-1}(x) = 0$">|; 

$key = q/displaystylep_n(x)=p_n(x)-p_n(x_0)=sum_{i=1}^na_icdot(x^i-x_0^i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle p_n(x) = p_n(x) - p_n(x_0) = \sum_{i=1}^n a_i \cdot (x^i - x_0^i)$">|; 

$key = q/displaystylep_n(x)=sum_{i=0}^na_icdotx^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle p_n(x) = \sum_{i=0}^n a_i \cdot x^i$">|; 

$key = q/displaystylep_n(x_0)=p_n(x_1)=...=p_n(x_m)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle p_n(x_0) = p_n(x_1) = ... = p_n(x_m) = 0$">|; 

$key = q/displaystyleq(x)=Acdotprod_{i=1}^n(x-x_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle q(x) = A \cdot \prod_{i=1}^n (x - x_i)$">|; 

$key = q/displaystyleq_{n-1}(x)=sum_{i=1}^na_isum_{j=0}^{i-1}x^jcdotx_0^{i-1-j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.55ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle q_{n - 1}(x) = \sum_{i = 1}^n a_i \sum_{j = 0}^{i - 1} x^j \cdot x_0^{i - 1 - j}$">|; 

$key = q/displaystyleq_{n-1}(x_k)=frac{p_{n-1}(x_k)}{x_k-x_0}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle q_{n-1}(x_k) = \frac{p_{n-1}(x_k)}{x_k - x_0} = 0$">|; 

$key = q/displaystylesum_{k=0}^nbinome{n}{k}cdotx^kcdoty^{n-k}=sum_{i=0}^nbinome{n}{i}cdoty^icdotx^{n-i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \sum_{k = 0}^n \binome{n}{k} \cdot x^k \cdot y^{n - k} = \sum_{i = 0}^n \binome{n}{i} \cdot y^i \cdot x^{n - i}$">|; 

$key = q/displaystylesum_{k=0}^nbinome{n}{k}cdotx^kcdoty^{n-k}=sum_{k=0}^nbinome{n}{n-k}cdotx^kcdoty^{n-k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle \sum_{k = 0}^n \binome{n}{k} \cdot x^k \cdot y^{n - k} = \sum_{k = 0}^n \binome{n}{n - k} \cdot x^k \cdot y^{n - k}$">|; 

$key = q/displaystylesum_{k=0}^{n}binome{n}{k}=(1+1)^n=2^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle \sum_{k=0}^{n} \binome{n}{k} = (1 + 1)^n = 2^n$">|; 

$key = q/displaystylesum_{k=0}^{n}binome{n}{k}cdot(-1)^k=(-1+1)^n=0^n=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle \sum_{k=0}^{n} \binome{n}{k} \cdot (-1)^k = (-1 + 1)^n = 0^n = 0$">|; 

$key = q/displaystylesum_{k=0}^{n}binome{n}{k}cdotx^kcdot(1-x)^{n-k}=(x+1-x)^n=1^n=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle \sum_{k=0}^{n} \binome{n}{k} \cdot x^k \cdot (1-x)^{n-k} = (x + 1 - x)^n = 1^n = 1$">|; 

$key = q/displaystylex^i-x_0^i=(x-x_0)sum_{j=0}^{i-1}x^jcdotx_0^{i-1-j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.55ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle x^i - x_0^i = (x - x_0) \sum_{j = 0}^{i - 1} x^j \cdot x_0^{i - 1 -j}$">|; 

$key = q/displaystylex_0strictinferieurx_1strictinferieur...strictinferieurx_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.76ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle x_0 \strictinferieur x_1 \strictinferieur ... \strictinferieur x_m$">|; 

$key = q/finmathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img110.svg"
 ALT="$f \in \mathcal{F}$">|; 

$key = q/i,ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img102.svg"
 ALT="$i,n \in \setN$">|; 

$key = q/ilen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img103.svg"
 ALT="$i \le n$">|; 

$key = q/kin{1,...,n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img80.svg"
 ALT="$k \in \{1,...,n-1\}$">|; 

$key = q/m+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img14.svg"
 ALT="$m+1$">|; 

$key = q/m+1gen+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img45.svg"
 ALT="$m + 1 \ge n + 1$">|; 

$key = q/m-1gen-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img42.svg"
 ALT="$m-1 \ge n-1$">|; 

$key = q/mathcal{B}_n:mathcal{F}mapstomathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\mathcal{B}_n : \mathcal{F} \mapsto \mathcal{F}$">|; 

$key = q/mathcal{F}=fonction([0,1],corps);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\mathcal{F} = \fonction([0,1],\corps)$">|; 

$key = q/mgen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img15.svg"
 ALT="$m \ge n$">|; 

$key = q/mlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img117.svg"
 ALT="$m \le n$">|; 

$key = q/mu_i:corpsmapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\mu_i : \corps \mapsto \corps$">|; 

$key = q/n+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img56.svg"
 ALT="$n+1$">|; 

$key = q/n+1=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img20.svg"
 ALT="$n + 1 = 1$">|; 

$key = q/n-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img25.svg"
 ALT="$n - 1$">|; 

$key = q/n-i=k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img92.svg"
 ALT="$n - i = k$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$n$">|; 

$key = q/n=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$n = 0$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img61.svg"
 ALT="$n \in \setN$">|; 

$key = q/p:corpsmapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img2.svg"
 ALT="$p : \corps \mapsto \corps$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$p$">|; 

$key = q/p=q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img57.svg"
 ALT="$p = q$">|; 

$key = q/p_0(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$p_0(x) = 0$">|; 

$key = q/p_0(x)=a_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$p_0(x) = a_0$">|; 

$key = q/p_0(x_0)=a_0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$p_0(x_0) = a_0 = 0$">|; 

$key = q/p_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$p_0$">|; 

$key = q/p_0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$p_0 = 0$">|; 

$key = q/p_1,p_2inmathcal{P}_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$p_1,p_2 \in \mathcal{P}_n$">|; 

$key = q/p_1=p_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img47.svg"
 ALT="$p_1 = p_2$">|; 

$key = q/p_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img26.svg"
 ALT="$p_n$">|; 

$key = q/pinmathcal{P}_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img48.svg"
 ALT="$p\in\mathcal{P}_n$">|; 

$key = q/polynome(corps,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\polynome(\corps,n)$">|; 

$key = q/pstrictinferieurmlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img124.svg"
 ALT="$p \strictinferieur m \le n$">|; 

$key = q/q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img119.svg"
 ALT="$q$">|; 

$key = q/q_{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img33.svg"
 ALT="$q_{n - 1}$">|; 

$key = q/q_{n-1}inmathcal{P}_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img36.svg"
 ALT="$q_{n-1} \in \mathcal{P}_n$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$r$">|; 

$key = q/tin[0,1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img106.svg"
 ALT="$t \in [0,1]$">|; 

$key = q/x=-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img97.svg"
 ALT="$x = -1$">|; 

$key = q/x=x_1,x_2,...,x_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img37.svg"
 ALT="$x = x_1, x_2, ...,x_m$">|; 

$key = q/x=y=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img95.svg"
 ALT="$x = y = 1$">|; 

$key = q/x^0=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img73.svg"
 ALT="$x^0 = 1$">|; 

$key = q/x^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img79.svg"
 ALT="$x^k$">|; 

$key = q/x^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img76.svg"
 ALT="$x^n$">|; 

$key = q/x_0notin{x_1,...,x_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$x_0 \notin \{x_1,...,x_n\}$">|; 

$key = q/x_1,x_2,...,x_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img49.svg"
 ALT="$x_1,x_2,...,x_n$">|; 

$key = q/x_k-x_0ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img39.svg"
 ALT="$x_k - x_0 \ne 0$">|; 

$key = q/xincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img6.svg"
 ALT="$x \in \corps$">|; 

$key = q/y=1-x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img100.svg"
 ALT="$y = 1 - x$">|; 

$key = q/y=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img98.svg"
 ALT="$y = 1$">|; 

$key = q/{Eqts}a(x)=b(x)cdotq(x)+r(x)q(x)=sum_{i=0}^{n-m}q_ix^ir(x)=sum_{i=0}^{p}r_ix^i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.29ex; vertical-align: -8.60ex; " SRC="|."$dir".q|img123.svg"
 ALT="\begin{Eqts}
a(x) = b(x) \cdot q(x) + r(x) \\\\
q(x) = \sum_{i=0}^{n-m} q_i x^i \\\\
r(x) = \sum_{i=0}^{p} r_i x^i
\end{Eqts}">|; 

$key = q/{Eqts}a(x)=sum_{i=0}^na_icdotx^ib(x)=sum_{i=0}^mb_icdotx^i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.36ex; vertical-align: -6.63ex; " SRC="|."$dir".q|img118.svg"
 ALT="\begin{Eqts}
a(x) = \sum_{i = 0}^n a_i \cdot x^i \\\\
b(x) = \sum_{i = 0}^m b_i \cdot x^i
\end{Eqts}">|; 

$key = q/{Eqts}p(x)=a_ncdotx^n+sum_{i=0}^{n-1}a_icdotx^iq(x)=Acdotx^n+sum_{i=0}^{n-1}b_i(x_1,...,x_n)cdotx^i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.02ex; vertical-align: -6.96ex; " SRC="|."$dir".q|img58.svg"
 ALT="\begin{Eqts}
p(x) = a_n \cdot x^n + \sum_{i = 0}^{n - 1} a_i \cdot x^i \\\\
q(x) = A \cdot x^n + \sum_{i = 0}^{n - 1} b_i(x_1,...,x_n) \cdot x^i
\end{Eqts}">|; 

$key = q/{eqnarraystar}b_0(x)&=&(1+x)^0=1b_1(x)&=&(1+x)^1=1+xb_2(x)&=&(1+x)^2=(1+x)cdot(1+x)=1+2x+x^2{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img64.svg"
 ALT="\begin{eqnarray*}
b_0(x) &amp;=&amp; (1 + x)^0 = 1 \\\\
b_1(x) &amp;=&amp; (1 + x)^1 = 1 + x \\\\
b_2(x) &amp;=&amp; (1 + x)^2 = (1 + x) \cdot (1 + x) = 1 + 2  x + x^2
\end{eqnarray*}">|; 

1;

