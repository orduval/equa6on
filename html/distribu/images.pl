# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 23.13ex; " SRC="|."$dir".q|img47.svg"
 ALT="\begin{eqnarray*}
\int_{\setR} \hat{u}(-x)  v(x)  dx &amp;=&amp; \lim_{a \to +\infty}\...
...&amp; \lim_{a \to +\infty} \int^{-a}_a \hat{u}(\xi)  v(-\xi)  d\xi
\end{eqnarray*}">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$E$">|; 

$key = q/E=mathcal{F}subseteqlebesgue^2(setR,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.76ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$E = \mathcal{F} \subseteq \lebesgue^2(\setR,\setR)$">|; 

$key = q/K;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img12.svg"
 ALT="$K$">|; 

$key = q/OD{u}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\OD{u}{x}$">|; 

$key = q/a>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img38.svg"
 ALT="$a &gt; 0$">|; 

$key = q/convolution;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.73ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\convolution$">|; 

$key = q/correlation;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\correlation$">|; 

$key = q/d_a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img36.svg"
 ALT="$d_a$">|; 

$key = q/dirac(-x)=dirac(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\dirac(-x) = \dirac(x)$">|; 

$key = q/diracinF^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\dirac \in F^\dual$">|; 

$key = q/displaystyle(uconvolutionv)(t)=int_{-infty}^{+infty}u(t-s)v(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.92ex; vertical-align: -2.36ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle (u \convolution v)(t) = \int_{-\infty}^{+\infty} u(t-s)  v(s)  ds$">|; 

$key = q/displaystyle(ucorrelationv)(t)=int_{-infty}^{+infty}u(s+t)v(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.92ex; vertical-align: -2.36ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle (u \correlation v)(t) = \int_{-\infty}^{+\infty} u(s+t)  v(s)  ds$">|; 

$key = q/displaystyleOD{e_+}{x}=dirac;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \OD{e_+}{x} = \dirac$">|; 

$key = q/displaystylebiforme{u}{K}{v}=int_{AtimesB}u(x)cdotK(x,y)cdotv(y)dmu(x)dnu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -2.36ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \biforme{u}{K}{v} = \int_{A \times B} u(x) \cdot K(x,y) \cdot v(y)  d\mu(x)  d\nu(y)$">|; 

$key = q/displaystyled_a(u)(x)=u(acdotx);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle d_a(u)(x) = u(a \cdot x)$">|; 

$key = q/displaystylediracconvolutionu=u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle \dirac \convolution u = u$">|; 

$key = q/displaystyleforme{OD{e_+}{x}}{v}=-left[lim_{xto+infty}v(x)-v(0)right]=v(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \forme{\OD{e_+}{x}}{v} = - \left[\lim_{x \to +\infty} v(x) - v(0)\right] = v(0)$">|; 

$key = q/displaystyleforme{OD{u}{x}}{v}=-forme{u}{OD{v}{x}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \forme{\OD{u}{x}}{v} = - \forme{u}{\OD{v}{x}}$">|; 

$key = q/displaystyleforme{d_a(u)}{v}=unsur{a}forme{u}{d_{1slasha}(v)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \forme{d_a(u)}{v} = \unsur{a} \forme{u}{d_{1/a}(v)}$">|; 

$key = q/displaystyleforme{dirac}{u}=u(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \forme{\dirac}{u} = u(0)$">|; 

$key = q/displaystyleforme{hat{mu}}{u}=int_Au(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \forme{ \hat{\mu} }{u} = \int_A u(x)  d\mu(x)$">|; 

$key = q/displaystyleforme{r(u)}{v}=forme{u}{r(v)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \forme{r(u)}{v} = \forme{u}{r(v)}$">|; 

$key = q/displaystyleforme{t_a(u)}{v}=forme{u}{t_{-a}(v)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \forme{t_a(u)}{v} = \forme{u}{t_{-a}(v)}$">|; 

$key = q/displaystyleforme{varphi}{u}=int_Au(x)cdothat{varphi}(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \forme{\varphi}{u} = \int_A u(x) \cdot \hat{\varphi}(x)  dx$">|; 

$key = q/displaystyleint_Au(x)cdotvarphi(x)dx=forme{varphi}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \int_A u(x) \cdot \varphi(x)  dx = \forme{\varphi}{u}$">|; 

$key = q/displaystyleint_setRdirac(x)cdotu(x)dx=u(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \int_\setR \dirac(x) \cdot u(x)  dx = u(0)$">|; 

$key = q/displaystyleint_{A^2}dirac(xi-x)cdotK(xi,eta)cdotdirac(eta-y)dmu(xi)dnu(eta)=K(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \int_{A^2} \dirac(\xi - x) \cdot K(\xi,\eta) \cdot \dirac(\eta - y)  d\mu(\xi)  d\nu(\eta) = K(x,y)$">|; 

$key = q/displaystyleint_{AtimesB}u(x)cdotK(x,y)cdotv(y)dmu(x)dnu(y)=biforme{u}{K}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -2.36ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \int_{A \times B} u(x) \cdot K(x,y) \cdot v(y)  d\mu(x)  d\nu(y) = \biforme{u}{K}{v}$">|; 

$key = q/displaystyleint_{setR}OD{u}{x}(x)cdotv(x)dx=-int_{setR}u(x)cdotOD{v}{x}(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.46ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \int_{\setR} \OD{u}{x}(x) \cdot v(x)  dx = - \int_{\setR} u(x) \cdot \OD{v}{x}(x)  dx$">|; 

$key = q/displaystyleint_{setR}dirac(x-y)u(y)dy=u(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \int_{\setR} \dirac(x-y)  u(y)  dy = u(x)$">|; 

$key = q/displaystyleint_{setR}hat{u}(ax)v(x)dx=unsur{a}int_{setR}hat{u}(xi)vleft(xislasharight)dxi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle \int_{\setR} \hat{u}(a  x)  v(x)  dx = \unsur{a} \int_{\setR} \hat{u}(\xi)  v\left( \xi/a \right)  d\xi$">|; 

$key = q/displaystyleint_{setR}hat{u}(x-a)v(x)dx=int_{setR}hat{u}(xi)v(xi+a)dxi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \int_{\setR} \hat{u}(x-a) v(x) dx = \int_{\setR} \hat{u}(\xi) v(\xi+a) d\xi$">|; 

$key = q/displaystyleint_{setR}u(x)dirac(-x)dx=int_{setR}u(-x)dirac(x)dx=u(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle \int_{\setR} u(x)  \dirac(-x)  dx = \int_{\setR} u(-x)  \dirac(x)  dx = u(0)$">|; 

$key = q/displaystyleint_{setR}u(x)dirac(x-a)dx=u(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle \int_{\setR} u(x)  \dirac(x-a)  dx = u(a)$">|; 

$key = q/displaystylelim_{xto+infty}u(x)=lim_{xto-infty}u(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \lim_{x \to +\infty} u(x) = \lim_{x \to -\infty} u(x) = 0$">|; 

$key = q/displaystylemu(A)=forme{hat{mu}}{indicatrice_A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \mu(A) = \forme{ \hat{\mu} }{\indicatrice_A}$">|; 

$key = q/displaystyler(u)(x)=u(-x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle r(u)(x) = u(-x)$">|; 

$key = q/displaystylet_a(u)(x)=u(x-a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle t_a(u)(x) = u(x - a)$">|; 

$key = q/dxi=-dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$d\xi = -dx$">|; 

$key = q/dxi=adx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img40.svg"
 ALT="$d\xi = a  dx$">|; 

$key = q/dxi=dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img52.svg"
 ALT="$d\xi = dx$">|; 

$key = q/e_+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img31.svg"
 ALT="$e_+$">|; 

$key = q/hat{K}:AtimesBmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\hat{K} : A \times B \mapsto F$">|; 

$key = q/hat{mu};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\hat{\mu}$">|; 

$key = q/hat{varphi};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\hat{\varphi}$">|; 

$key = q/mu:sousens(setR)mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\mu : \sousens(\setR) \mapsto \setR$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\mu$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img43.svg"
 ALT="$r$">|; 

$key = q/t_a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.96ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img49.svg"
 ALT="$t_a$">|; 

$key = q/tinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img59.svg"
 ALT="$t \in \setR$">|; 

$key = q/u,v:AmapstoB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$u,v : A \mapsto B$">|; 

$key = q/u,v:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img56.svg"
 ALT="$u, v : \setR \mapsto \setR$">|; 

$key = q/u:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$u : A \mapsto \setR$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img28.svg"
 ALT="$u$">|; 

$key = q/uconvolutionv:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img57.svg"
 ALT="$u \convolution v : \setR \mapsto \setR$">|; 

$key = q/ucorrelationv:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img66.svg"
 ALT="$u \correlation v : \setR \mapsto \setR$">|; 

$key = q/uinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img22.svg"
 ALT="$u \in F$">|; 

$key = q/uinmathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img19.svg"
 ALT="$u \in \mathcal{F}$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\varphi$">|; 

$key = q/vinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img30.svg"
 ALT="$v\in F$">|; 

$key = q/xi=-x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\xi = -x$">|; 

$key = q/xi=acdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\xi = a \cdot x$">|; 

$key = q/xi=x-a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\xi = x - a$">|; 

$key = q/{Eqts}e_+(x)=indicatrice_{[0,+infty)}=cases{1&mbox{si}tge00&mbox{si}t<0cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img32.svg"
 ALT="\begin{Eqts}
e_+(x) = \indicatrice_{[0,+\infty)} =
\begin{cases}
1 &amp; \mbox{si } t \ge 0 \\\\
0 &amp; \mbox{si } t &lt; 0
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}int_{setR}OD{u}{x}(x)cdotv(x)dx=lim_{ato+infty}left[u(a)cdotv(a)-u(-a)cdotv(-a)right]-int_{setR}u(x)cdotOD{v}{x}(x)dx{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.46ex; vertical-align: -2.16ex; " SRC="|."$dir".q|img25.svg"
 ALT="\begin{Eqts}
\int_{\setR} \OD{u}{x}(x) \cdot v(x)  dx =
\lim_{a \to +\infty} \l...
...u(-a) \cdot v(-a) \right]
- \int_{\setR} u(x) \cdot \OD{v}{x}(x)  dx
\end{Eqts}">|; 

$key = q/{eqnarraystar}forme{OD{e_+}{x}}{v}&=&-forme{e_+}{OD{v}{x}}&=&-int_0^{+infty}OD{v}{x}(x)dx{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.90ex; " SRC="|."$dir".q|img33.svg"
 ALT="\begin{eqnarray*}
\forme{\OD{e_+}{x}}{v} &amp;=&amp; - \forme{e_+}{\OD{v}{x}} \\\\
&amp;=&amp; - \int_0^{+\infty} \OD{v}{x}(x) dx
\end{eqnarray*}">|; 

1;

