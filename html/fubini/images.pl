# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 23.05ex; " SRC="|."$dir".q|img104.svg"
 ALT="\begin{eqnarray*}
\lim_{n \to \infty} \int_X u_n(x)  d\nu(x) &amp;=&amp; \lim_{n \to \i...
...} \int_{C(n)} f(z)  d\sigma(z) \\\\
&amp;=&amp; \int_A f(z)  d\sigma(z)
\end{eqnarray*}">|; 

$key = q/A(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$A(x)$">|; 

$key = q/A(x)=P(x)capA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$A(x) = P(x) \cap A$">|; 

$key = q/A(x)capA(z)=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img144.svg"
 ALT="$A(x) \cap A(z) = \emptyset$">|; 

$key = q/A,BsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img27.svg"
 ALT="$A,B \subseteq \Omega$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$A$">|; 

$key = q/A=[a_1,b_1]times[a_2,b_2]...times[a_n,b_n]subseteqsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img134.svg"
 ALT="$A = [a_1,b_1] \times [a_2,b_2] ... \times [a_n,b_n] \subseteq \setR^n$">|; 

$key = q/A=[alpha,beta]times[gamma,delta]subseteqsetR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img129.svg"
 ALT="$A = [\alpha,\beta] \times [\gamma,\delta] \subseteq \setR^2$">|; 

$key = q/AcapB=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.06ex; vertical-align: -0.23ex; " SRC="|."$dir".q|img28.svg"
 ALT="$A \cap B = \emptyset$">|; 

$key = q/AsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img12.svg"
 ALT="$A \subseteq \Omega$">|; 

$key = q/C(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img90.svg"
 ALT="$C(\alpha)$">|; 

$key = q/C(m,x)subseteqC(n,x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img100.svg"
 ALT="$C(m,x) \subseteq C(n,x)$">|; 

$key = q/Delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img148.svg"
 ALT="$\Delta$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img66.svg"
 ALT="$E$">|; 

$key = q/F,G:XmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img57.svg"
 ALT="$F,G : X \mapsto \setR$">|; 

$key = q/FessinferieurG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img67.svg"
 ALT="$F \essinferieur G$">|; 

$key = q/FleG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img65.svg"
 ALT="$F \le G$">|; 

$key = q/I:XmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$I : X \mapsto \setR$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img50.svg"
 ALT="$N$">|; 

$key = q/N=NcapA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img48.svg"
 ALT="$N = N \cap A$">|; 

$key = q/NsubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img47.svg"
 ALT="$N \subseteq A$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\Omega$">|; 

$key = q/Psi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\Psi$">|; 

$key = q/S,I:[a,b]mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img141.svg"
 ALT="$S,I : [a,b] \mapsto \setR$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img53.svg"
 ALT="$X$">|; 

$key = q/XsubseteqPsi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img7.svg"
 ALT="$X \subseteq \Psi$">|; 

$key = q/Z(alpha)=AsetminusC(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img92.svg"
 ALT="$Z(\alpha) = A \setminus C(\alpha)$">|; 

$key = q/Z(alpha,x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img96.svg"
 ALT="$Z(\alpha,x)$">|; 

$key = q/[(x,c),(x,d)];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img114.svg"
 ALT="$[(x,c),(x,d)]$">|; 

$key = q/a,b,c,dinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img109.svg"
 ALT="$a,b,c,d \in \setR$">|; 

$key = q/a,b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img140.svg"
 ALT="$a,b$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img116.svg"
 ALT="$a$">|; 

$key = q/aleb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img110.svg"
 ALT="$a \le b$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img117.svg"
 ALT="$b$">|; 

$key = q/cled;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img111.svg"
 ALT="$c \le d$">|; 

$key = q/displaystyleA(x)={(x,y):I(x)leyleS(x)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle A(x) = \{ (x,y) : I(x) \le y \le S(x) \}$">|; 

$key = q/displaystyleA(x)capA(y)=P(x)capP(y)capA=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle A(x) \cap A(y) = P(x) \cap P(y) \cap A = \emptyset$">|; 

$key = q/displaystyleA=bigcup_{xin[a,b]}A(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.06ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\displaystyle A = \bigcup_{x \in [a,b]} A(x)$">|; 

$key = q/displaystyleA={(x,y)in[a,b]timessetR:I(x)leyleS(x)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle A = \{ (x,y) \in [a,b] \times \setR : I(x) \le y \le S(x) \}$">|; 

$key = q/displaystyleC(x)=A(x)setminusN(x)={yinA(x):f(y)leg(y)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle C(x) = A(x) \setminus N(x) = \{ y \in A(x) : f(y) \le g(y) \}$">|; 

$key = q/displaystyleDelta={(s,t)insetR^2:0les,tleT,quadsget};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img149.svg"
 ALT="$\displaystyle \Delta = \{ (s,t) \in \setR^2 : 0 \le s,t \le T, \quad s \ge t \}$">|; 

$key = q/displaystyleE=XsetminusZ={xinX:mu(N(x))=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle E = X \setminus Z = \{ x \in X : \mu(N(x)) = 0 \}$">|; 

$key = q/displaystyleF(x)=int_{A(x)}f(y)dmu(y)leint_{A(x)}g(y)dmu(y)=G(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle F(x) = \int_{A(x)} f(y)  d\mu(y) \le \int_{A(x)} g(y)  d\mu(y) = G(x)$">|; 

$key = q/displaystyleI(x)=int_{A(x)}f(y)dmu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle I(x) = \int_{A(x)} f(y)  d\mu(y)$">|; 

$key = q/displaystyleN={zinA:f(z)strictsuperieurg(z)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle N = \{ z \in A : f(z) \strictsuperieur g(z) \}$">|; 

$key = q/displaystyleP=[a,b]times[c,d];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle P = [a,b] \times [c,d]$">|; 

$key = q/displaystyleZ={xinX:mu(N(x))strictsuperieur0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle Z = \{ x \in X : \mu(N(x)) \strictsuperieur 0 \}$">|; 

$key = q/displaystyle[(a,y),(b,y)]={(x,y):xin[a,b]};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\displaystyle [(a,y),(b,y)] = \{ (x,y) : x \in [a,b] \}$">|; 

$key = q/displaystyle[(x,c),(x,d)]={(x,y):yin[c,d]};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\displaystyle [(x,c),(x,d)] = \{ (x,y) : y \in [c,d] \}$">|; 

$key = q/displaystylebigcup_{xinX}A(x)=Acapbigcup_{xinX}P(x)=AcapOmega=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \bigcup_{x \in X} A(x) = A \cap \bigcup_{x \in X} P(x) = A \cap \Omega = A$">|; 

$key = q/displaystyleint_0^Tdsint_0^su(t)dt=int_0^Tu(t)dtint_t^Tds=int_0^T(T-t)cdotu(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.88ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle \int_0^T  ds \int_0^s u(t)  dt = \int_0^T u(t)  dt \int_t^T  ds = \int_0^T (T - t) \cdot u(t)  dt$">|; 

$key = q/displaystyleint_Af(x)dsigma(x)-epsilonleint_Xdnu(x)int_{A(x)}f(y)dmu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle \int_A f(x)  d\sigma(x) - \epsilon \le \int_X  d\nu(x) \int_{A(x)} f(y)  d\mu(y)$">|; 

$key = q/displaystyleint_Af(x)dsigma(x)leint_Xdnu(x)int_{A(x)}f(y)dmu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle \int_A f(x)  d\sigma(x) \le \int_X  d\nu(x) \int_{A(x)} f(y)  d\mu(y)$">|; 

$key = q/displaystyleint_Af(x)dx=int_{a_1}^{b_1}dx_1...int_{a_n}^{b_n}dx_nf(x_1,...x_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.11ex; vertical-align: -2.41ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\displaystyle \int_A f(x)  dx = \int_{a_1}^{b_1} dx_1  ... \int_{a_n}^{b_n} dx_n  f(x_1,...x_n)$">|; 

$key = q/displaystyleint_Af(x,y)dsigma(x,y)=int_a^bdnu(x)int_{I(x)}^{S(x)}f(x,y)dmu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\displaystyle \int_A f(x,y)  d\sigma(x,y) = \int_a^b d\nu(x)  \int_{I(x)}^{S(x)} f(x,y)  d\mu(y)$">|; 

$key = q/displaystyleint_Af(x,y)dsigma(x,y)=int_alpha^betadxint_gamma^deltaf(x,y)dy=int_gamma^deltadyint_alpha^betaf(x,y)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.19ex; vertical-align: -2.50ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\displaystyle \int_A f(x,y)  d\sigma(x,y) = \int_\alpha^\beta dx \int_\gamma^\delta f(x,y)  dy = \int_\gamma^\delta dy \int_\alpha^\beta f(x,y)  dx$">|; 

$key = q/displaystyleint_Af(x,y)dxdy=int_alpha^betadxint_gamma^deltaf(x,y)dy=int_gamma^deltadyint_alpha^betaf(x,y)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.19ex; vertical-align: -2.50ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\displaystyle \int_A f(x,y)  dx  dy = \int_\alpha^\beta dx \int_\gamma^\delta f(x,y)  dy = \int_\gamma^\delta dy \int_\alpha^\beta f(x,y)  dx$">|; 

$key = q/displaystyleint_Af(z)dsigma(z)-epsilonleint_Aw(z)dsigma(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \int_A f(z)  d\sigma(z) - \epsilon \le \int_A w(z)  d\sigma(z)$">|; 

$key = q/displaystyleint_Af(z)dsigma(z)=int_Xdnu(x)int_{A(x)}f(y)dmu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle \int_A f(z)  d\sigma(z) = \int_X  d\nu(x) \int_{A(x)} f(y)  d\mu(y)$">|; 

$key = q/displaystyleint_Aw(z)dsigma(z)=int_Xdnu(x)int_{A(x)}w(y)dmu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \int_A w(z)  d\sigma(z) = \int_X  d\nu(x) \int_{A(x)} w(y)  d\mu(y)$">|; 

$key = q/displaystyleint_Deltaf(x)dx=int_0^Tdsint_0^sf(s,t)dt=int_0^Tdtint_t^Tf(s,t)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.88ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle \int_\Delta f(x)  dx = \int_0^T  ds \int_0^s f(s,t)  dt = \int_0^T  dt \int_t^T f(s,t)  ds$">|; 

$key = q/displaystyleint_XF(x)dnu(x)leint_XG(x)dnu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \int_X F(x)  d\nu(x) \le \int_X G(x)  d\nu(x)$">|; 

$key = q/displaystyleint_Xdnu(x)int_{A(x)}f(y)dmu(y)=int_XI(x)dnu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \int_X  d\nu(x) \int_{A(x)} f(y)  d\mu(y) = \int_X I(x)  d\nu(x)$">|; 

$key = q/displaystyleint_Xdnu(x)int_{A(x)}f(y)dmu(y)leint_Af(z)dsigma(z)+epsiloncdotsigma(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle \int_X  d\nu(x) \int_{A(x)} f(y)  d\mu(y) \le \int_A f(z)  d\sigma(z) + \epsilon \cdot \sigma(A)$">|; 

$key = q/displaystyleint_Xdnu(x)int_{A(x)}f(y)dmu(y)leint_Af(z)dsigma(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle \int_X  d\nu(x) \int_{A(x)} f(y)  d\mu(y) \le \int_A f(z)  d\sigma(z)$">|; 

$key = q/displaystyleint_Xdnu(x)int_{A(x)}f(y)dmu(y)leint_Xdnu(x)int_{A(x)}[w(y)+epsilon]dmu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle \int_X  d\nu(x) \int_{A(x)} f(y)  d\mu(y) \le \int_X  d\nu(x) \int_{A(x)} [w(y) + \epsilon]  d\mu(y)$">|; 

$key = q/displaystyleint_Xdnu(x)int_{A(x)}f(y)dmu(y)leint_Xdnu(x)int_{A(x)}g(y)dmu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \int_X  d\nu(x) \int_{A(x)} f(y)  d\mu(y) \le \int_X  d\nu(x) \int_{A(x)} g(y)  d\mu(y)$">|; 

$key = q/displaystyleint_Xdnu(x)int_{A(x)}w(y)dmu(y)=int_Aw(z)dsigma(z)leint_Af(z)dsigma(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle \int_X  d\nu(x) \int_{A(x)} w(y)  d\mu(y) = \int_A w(z)  d\sigma(z) \le \int_A f(z)  d\sigma(z)$">|; 

$key = q/displaystyleint_Xint_{A(x)}f(y)dmu(y)dnu(x)=int_XI(x)dnu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \int_X \int_{A(x)} f(y)  d\mu(y)  d\nu(x) = \int_X I(x)  d\nu(x)$">|; 

$key = q/displaystyleint_{AtimesB}f(x,y)dmu(x)dnu(y)=int_Admu(x)int_Bf(x,y)dnu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -2.36ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle \int_{A \times B} f(x,y)  d\mu(x)  d\nu(y) = \int_A  d\mu(x) \int_B f(x,y)  d\nu(y)$">|; 

$key = q/displaystyleint_{AtimesB}f(x,y)dmu(x)dnu(y)=int_Bdnu(y)int_Af(x,y)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -2.36ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle \int_{A \times B} f(x,y)  d\mu(x)  d\nu(y) = \int_B d\nu(y)  \int_A f(x,y)  d\mu(x)$">|; 

$key = q/displaystyleint_{C(alpha)}f(z)dsigma(z)=int_Xdnu(x)int_{C(alpha,x)}f(y)dmu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \int_{C(\alpha)} f(z)  d\sigma(z) = \int_X  d\nu(x) \int_{C(\alpha,x)} f(y)  d\mu(y)$">|; 

$key = q/displaystylelim_{alphato+infty}int_{C(alpha)}f(z)dsigma(z)=int_Af(z)dsigma(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle \lim_{\alpha \to +\infty} \int_{C(\alpha)} f(z)  d\sigma(z) = \int_A f(z)  d\sigma(z)$">|; 

$key = q/displaystylelim_{ntoinfty}int_{C(n)}f(z)dsigma(z)=int_Af(z)dsigma(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle \lim_{n \to \infty} \int_{C(n)} f(z)  d\sigma(z) = \int_A f(z)  d\sigma(z)$">|; 

$key = q/displaystylelim_{ntoinfty}int_{C(n,x)}f(y)dmu(y)=int_{A(x)}f(y)dmu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle \lim_{n \to \infty} \int_{C(n,x)} f(y)  d\mu(y) = \int_{A(x)} f(y)  d\mu(y)$">|; 

$key = q/displaystylelim_{ntoinfty}u_n(x)=int_{A(x)}f(y)dmu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle \lim_{n \to \infty} u_n(x) = \int_{A(x)} f(y)  d\mu(y)$">|; 

$key = q/displaystylemathcal{P}(A)={A(x):xinX}={P(x)capA:xinX};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \mathcal{P}(A) = \{ A(x) : x \in X \} = \{ P(x) \cap A : x \in X \}$">|; 

$key = q/displaystylemathcal{P}={[(x,c),(x,d)]:xin[a,b]};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\displaystyle \mathcal{P} = \{ [(x,c),(x,d)] : x \in [a,b] \}$">|; 

$key = q/displaystylemathcal{Q}={[(a,y),(b,y)]:yin[c,d]};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle \mathcal{Q} = \{ [(a,y),(b,y)] : y \in [c,d] \}$">|; 

$key = q/displaystylemu([(a,y),(b,y)])=b-a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\displaystyle \mu([(a,y),(b,y)]) = b - a$">|; 

$key = q/displaystylemu([(x,c),(x,d)])=d-c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle \mu([(x,c),(x,d)]) = d - c$">|; 

$key = q/displaystylesigma(A)=int_Xmu(A(x))dnu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \sigma(A) = \int_X \mu(A(x))  d\nu(x)$">|; 

$key = q/displaystylesigma(N)=int_Xmu(N(x))dnu(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \sigma(N) = \int_X \mu(N(x))  d\nu(x) = 0$">|; 

$key = q/displaystylesigma(P)=(muotimesmu)([a,b]times[c,d])=mu([a,b])cdotmu([c,d])=(b-a)cdot(d-c);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle \sigma(P) = (\mu \otimes \mu)([a,b] \times [c,d]) = \mu([a,b]) \cdot \mu([c,d]) = (b - a) \cdot (d - c)$">|; 

$key = q/displaystylesigma_x(A)=int_a^b(d-c)dx=(d-c)int_a^bdx=(d-c)cdot(b-a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\displaystyle \sigma_x(A) = \int_a^b (d - c) dx = (d - c) \int_a^b dx = (d - c) \cdot (b - a)$">|; 

$key = q/displaystylesigma_y(A)=int_c^d(b-a)dy=(b-a)int_c^ddy=(b-a)cdot(d-c);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\displaystyle \sigma_y(A) = \int_c^d (b - a) dy = (b - a) \int_c^d dy = (b - a) \cdot (d - c)$">|; 

$key = q/displaystylesupessentiel_{xinA}^sigma[f(x)-w(x)]leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.77ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle \supessentiel_{x \in A}^\sigma [f(x) - w(x)] \le \epsilon$">|; 

$key = q/displaystylesupessentiel_{xinA}^sigmaf(x)strictinferieur+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.77ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle \supessentiel_{x \in A}^\sigma f(x) \strictinferieur +\infty$">|; 

$key = q/displaystyleu_n(x)=int_{C(n,x)}f(y)dmu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle u_n(x) = \int_{C(n,x)} f(y)  d\mu(y)$">|; 

$key = q/displaystylew=sum_iw_icdotindicatrice_{A_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle w = \sum_i w_i \cdot \indicatrice_{A_i}$">|; 

$key = q/dsigma(x,y)=dxdy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$d\sigma(x,y) = dx  dy$">|; 

$key = q/dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img136.svg"
 ALT="$dx$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/f,g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$f,g$">|; 

$key = q/f:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img32.svg"
 ALT="$f : A \mapsto \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img73.svg"
 ALT="$f$">|; 

$key = q/f=f^+-f^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img105.svg"
 ALT="$f = f^+ - f^-$">|; 

$key = q/fessinferieurg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img45.svg"
 ALT="$f \essinferieur g$">|; 

$key = q/fessinferieurw+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img79.svg"
 ALT="$f \essinferieur w + \epsilon$">|; 

$key = q/int_A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.86ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\int_A$">|; 

$key = q/int_Xint_{A(x)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.25ex; vertical-align: -1.29ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\int_X \int_{A(x)}$">|; 

$key = q/mathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\mathcal{C}$">|; 

$key = q/mathcal{C}={P(x)insousens(Omega):xinX};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\mathcal{C} = \{ P(x) \in \sousens(\Omega) : x \in X \}$">|; 

$key = q/mathcal{M};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\mathcal{M}$">|; 

$key = q/mathcal{P}(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\mathcal{P}(A)$">|; 

$key = q/mathcal{T}subseteqsousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\mathcal{T} \subseteq \sousens(\Omega)$">|; 

$key = q/mathcal{U}subseteqsousens(Psi);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\mathcal{U} \subseteq \sousens(\Psi)$">|; 

$key = q/mlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img99.svg"
 ALT="$m \le n$">|; 

$key = q/mu(N(x))=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\mu(N(x)) = 0$">|; 

$key = q/mu:mathcal{T}mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\mu : \mathcal{T} \mapsto \setR$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\mu$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img133.svg"
 ALT="$n$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img94.svg"
 ALT="$n \in \setN$">|; 

$key = q/nu(Z)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\nu(Z) = 0$">|; 

$key = q/nu:mathcal{U}mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\nu : \mathcal{U} \mapsto \setR$">|; 

$key = q/nu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\nu$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\setR$">|; 

$key = q/setR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\setR^2$">|; 

$key = q/sigma:mathcal{M}mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\sigma : \mathcal{M} \mapsto \setR$">|; 

$key = q/sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\sigma$">|; 

$key = q/sigma=muotimes...otimesmu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\sigma = \mu \otimes ... \otimes \mu$">|; 

$key = q/sigma_x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\sigma_x$">|; 

$key = q/sigma_x=sigma_y=sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\sigma_x = \sigma_y = \sigma$">|; 

$key = q/sigma_y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\sigma_y$">|; 

$key = q/sigmage0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\sigma \ge 0$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img152.svg"
 ALT="$t$">|; 

$key = q/u_mleu_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.01ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img101.svg"
 ALT="$u_m \le u_n$">|; 

$key = q/w:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img38.svg"
 ALT="$w : A \mapsto \setR$">|; 

$key = q/w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img72.svg"
 ALT="$w$">|; 

$key = q/w_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img40.svg"
 ALT="$w_i$">|; 

$key = q/wessinferieurf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img63.svg"
 ALT="$w \essinferieur f$">|; 

$key = q/x,yinX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img16.svg"
 ALT="$x,y \in X$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img115.svg"
 ALT="$x$">|; 

$key = q/xinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img59.svg"
 ALT="$x \in E$">|; 

$key = q/xinX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img35.svg"
 ALT="$x \in X$">|; 

$key = q/xmapstomu(A(x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$x \mapsto \mu(A(x))$">|; 

$key = q/xmapstomu(N(x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$x \mapsto \mu(N(x))$">|; 

$key = q/xney;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$x \ne y$">|; 

$key = q/xnez;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img145.svg"
 ALT="$x \ne z$">|; 

$key = q/{A_1,...,A_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\{A_1,...,A_n\}$">|; 

$key = q/{Eqts}C(alpha)={xinA:f(x)lealpha}Z(alpha)={xinA:f(x)strictsuperieuralpha}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img88.svg"
 ALT="\begin{Eqts}
C(\alpha) = \{ x \in A : f(x) \le \alpha \} \\\\
Z(\alpha) = \{ x \in A : f(x) \strictsuperieur \alpha \}
\end{Eqts}">|; 

$key = q/{Eqts}C(alpha,x)=C(alpha)capA(x)={xinA(x):f(x)lealpha}Z(alpha,x)=A(x)setminusC(alpha)={xinA(x):f(x)strictsuperieuralpha}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img89.svg"
 ALT="\begin{Eqts}
C(\alpha,x) = C(\alpha) \cap A(x) = \{ x \in A(x) : f(x) \le \alpha...
...\setminus C(\alpha) = \{ x \in A(x) : f(x) \strictsuperieur \alpha \}
\end{Eqts}">|; 

$key = q/{Eqts}Delta={(s,t)insetR^2:0lesleT,quad0letles}Delta={(s,t)insetR^2:0letleT,quadtlesleT}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img150.svg"
 ALT="\begin{Eqts}
\Delta = \{ (s,t) \in \setR^2 : 0 \le s \le T, \quad 0 \le t \le s ...
...\Delta = \{ (s,t) \in \setR^2 : 0 \le t \le T, \quad t \le s \le T \}
\end{Eqts}">|; 

$key = q/{Eqts}F(x)=int_{A(x)}f(y)dmu(y)G(x)=int_{A(x)}g(y)dmu(y){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.78ex; vertical-align: -7.34ex; " SRC="|."$dir".q|img58.svg"
 ALT="\begin{Eqts}
F(x) = \int_{A(x)} f(y)  d\mu(y) \ \\\\
G(x) = \int_{A(x)} g(y)  d\mu(y)
\end{Eqts}">|; 

$key = q/{eqnarraystar}A(x)cupB(x)&=&(AcapP(x))cup(BcapP(x))&=&(AcupB)capP(x)&=&(AcupB)(x){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img29.svg"
 ALT="\begin{eqnarray*}
A(x) \cup B(x) &amp;=&amp; (A \cap P(x)) \cup (B \cap P(x)) \\\\
&amp;=&amp; (A \cup B) \cap P(x) \\\\
&amp;=&amp; (A \cup B)(x)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}N(x)=NcapP(x)&=&NcapAcapP(x)&=&NcapA(x)&=&{zinA(x):f(z)strictsuperieurg(z)}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img49.svg"
 ALT="\begin{eqnarray*}
N(x) = N \cap P(x) &amp;=&amp; N \cap A \cap P(x) \\\\
&amp;=&amp; N \cap A(x) \\\\
&amp;=&amp; \{ z \in A(x) : f(z) \strictsuperieur g(z) \}
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}int_Aw(x)dsigma(x)&=&int_Xdnu(x)int_{A(x)}w(y)dmu(y)&le&int_Xdnu(x)int_{A(x)}f(y)dmu(y){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 17.04ex; " SRC="|."$dir".q|img75.svg"
 ALT="\begin{eqnarray*}
\int_A w(x)  d\sigma(x) &amp;=&amp; \int_X  d\nu(x) \int_{A(x)} w(y)  d\mu(y) \\\\
&amp;\le&amp; \int_X  d\nu(x) \int_{A(x)} f(y)  d\mu(y)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}lim_{ntoinfty}int_Xu_n(x)dnu(x)&=&int_Xlim_{ntoinfty}u_n(x)dnu(x)&=&int_Xdnu(x)int_{A(x)}f(y)dmu(y){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.65ex; " SRC="|."$dir".q|img103.svg"
 ALT="\begin{eqnarray*}
\lim_{n \to \infty} \int_X u_n(x)  d\nu(x) &amp;=&amp; \int_X \lim_{n...
...x)  d\nu(x) \\\\
&amp;=&amp; \int_X  d\nu(x) \int_{A(x)} f(y)  d\mu(y)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}sigma(emptyset)&=&int_Xmu(emptysetcapP(x))dnu(x)&=&int_Xmu(emptyset)dnu(x)&=&int_X0dnu(x)&=&0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 24.54ex; " SRC="|."$dir".q|img26.svg"
 ALT="\begin{eqnarray*}
\sigma(\emptyset) &amp;=&amp; \int_X \mu(\emptyset \cap P(x))  d\nu(x...
..._X \mu(\emptyset)  d\nu(x) \\\\
&amp;=&amp; \int_X 0  d\nu(x) \\\\
&amp;=&amp; 0
\end{eqnarray*}">|; 

1;

