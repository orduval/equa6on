# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.34ex; " SRC="|."$dir".q|img38.svg"
 ALT="\begin{eqnarray*}
\indicatrice[A \cup B] &amp;=&amp; \indicatrice_A + \indicatrice_B - \...
...catrice_A + \indicatrice_B - \indicatrice_A \cdot \indicatrice_B
\end{eqnarray*}">|; 

$key = q/(x,y)inAtimesB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$(x,y) \in A \times B$">|; 

$key = q/A,BsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$A,B \subseteq \Omega$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$A$">|; 

$key = q/AcapB=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.06ex; vertical-align: -0.23ex; " SRC="|."$dir".q|img28.svg"
 ALT="$A \cap B = \emptyset$">|; 

$key = q/AsubseteqB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img10.svg"
 ALT="$A \subseteq B$">|; 

$key = q/AsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img2.svg"
 ALT="$A \subseteq \Omega$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img30.svg"
 ALT="$B$">|; 

$key = q/IsubsetOmega^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img41.svg"
 ALT="$I \subset \Omega^2$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\Omega$">|; 

$key = q/displaystyle(AsetminusB)cap(AcapB)=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle (A \setminus B) \cap (A \cap B) = \emptyset$">|; 

$key = q/displaystyle(AsetminusB)capB=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle (A \setminus B) \cap B = \emptyset$">|; 

$key = q/displaystyleA=(AsetminusB)cup(AcapB);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle A = (A \setminus B) \cup (A \cap B)$">|; 

$key = q/displaystyleAcupB=(AsetminusB)cupB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle A \cup B = (A \setminus B) \cup B$">|; 

$key = q/displaystyleI={(i,j)inOmega^2:i=j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle I = \{ (i,j) \in \Omega^2 : i = j \}$">|; 

$key = q/displaystyleindicatrice[A](x)=indicatrice_A(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \indicatrice[A](x) = \indicatrice_A(x)$">|; 

$key = q/displaystyleindicatrice[A]=indicatrice_A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \indicatrice[A] = \indicatrice_A$">|; 

$key = q/displaystyleindicatrice[AcapB]=indicatrice_Acdotindicatrice_B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \indicatrice[A \cap B] = \indicatrice_A \cdot \indicatrice_B$">|; 

$key = q/displaystyleindicatrice[AcupB]=indicatrice[AsetminusB]+indicatrice_B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \indicatrice[A \cup B] = \indicatrice[A \setminus B] + \indicatrice_B$">|; 

$key = q/displaystyleindicatrice[AcupB]=indicatrice_A+indicatrice_B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \indicatrice[A \cup B] = \indicatrice_A + \indicatrice_B$">|; 

$key = q/displaystyleindicatrice[AtimesB](x,y)=indicatrice_A(x)cdotindicatrice_B(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \indicatrice[A \times B](x,y) = \indicatrice_A(x) \cdot \indicatrice_B(y)$">|; 

$key = q/displaystyleindicatrice[emptyset]=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \indicatrice[\emptyset] = 0$">|; 

$key = q/displaystyleindicatrice^{ij}=indicatrice_i^j=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.96ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \indicatrice^{ij} = \indicatrice_i^j = \indicatrice_{ij}$">|; 

$key = q/displaystyleindicatrice_A(x)=cases{1&text{si}xinA0&text{si}xinOmegasetminusAcases{;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \indicatrice_A(x) =
\begin{cases}
1 &amp; \text{si }  x \in A \\\\
0 &amp; \text{si }  x \in \Omega \setminus A
\end{cases}$">|; 

$key = q/displaystyleindicatrice_A(x)leindicatrice_B(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \indicatrice_A(x) \le \indicatrice_B(x)$">|; 

$key = q/displaystyleindicatrice_A:Omegamapsto{0,1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \indicatrice_A : \Omega \mapsto \{ 0 , 1 \}$">|; 

$key = q/displaystyleindicatrice_A=indicatrice[AsetminusB]+indicatrice[AcapB];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \indicatrice_A = \indicatrice[A \setminus B] + \indicatrice[A \cap B]$">|; 

$key = q/displaystyleindicatrice_Aleindicatrice_B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \indicatrice_A \le \indicatrice_B$">|; 

$key = q/displaystyleindicatrice_{ij}=indicatrice_I(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \indicatrice_{ij} = \indicatrice_I(i,j)$">|; 

$key = q/indicatrice_A(x)=0leindicatrice_B(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\indicatrice_A(x) = 0 \le \indicatrice_B(x)$">|; 

$key = q/indicatrice_A(x)=indicatrice_B(x)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\indicatrice_A(x) = \indicatrice_B(x) = 1$">|; 

$key = q/indicatrice_A(x)cdotindicatrice_B(x)=0cdot0=0=indicatrice[AcapB](x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\indicatrice_A(x) \cdot \indicatrice_B(x) = 0 \cdot 0 = 0 = \indicatrice[A \cap B](x)$">|; 

$key = q/indicatrice_A(x)cdotindicatrice_B(x)=0cdot1=0=indicatrice[AcapB](x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\indicatrice_A(x) \cdot \indicatrice_B(x) = 0 \cdot 1 = 0 = \indicatrice[A \cap B](x)$">|; 

$key = q/indicatrice_A(x)cdotindicatrice_B(x)=1cdot0=0=indicatrice[AcapB](x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\indicatrice_A(x) \cdot \indicatrice_B(x) = 1 \cdot 0 = 0 = \indicatrice[A \cap B](x)$">|; 

$key = q/indicatrice_A(x)cdotindicatrice_B(x)=1cdot1=1=indicatrice[AcapB](x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\indicatrice_A(x) \cdot \indicatrice_B(x) = 1 \cdot 1 = 1 = \indicatrice[A \cap B](x)$">|; 

$key = q/xin(OmegasetminusA)capB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$x \in (\Omega \setminus A) \cap B$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img11.svg"
 ALT="$x \in A$">|; 

$key = q/xinAcap(OmegasetminusB);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$x \in A \cap (\Omega \setminus B)$">|; 

$key = q/xinAcapB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img19.svg"
 ALT="$x \in A \cap B$">|; 

$key = q/xinB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$x \in B$">|; 

$key = q/xinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img17.svg"
 ALT="$x \in \Omega$">|; 

$key = q/xinOmegasetminus(AcupB);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$x \in \Omega \setminus (A \cup B)$">|; 

$key = q/xinOmegasetminusA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$x \in \Omega \setminus A$">|; 

$key = q/{Eqts}indicatrice_{ij}=cases{1&text{si}i=j0&text{si}ineqjcases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img44.svg"
 ALT="\begin{Eqts}
\indicatrice_{ij} =
\begin{cases}
1 &amp; \text{si } i = j \\\\
0 &amp; \text{si } i \neq j
\end{cases}\end{Eqts}">|; 

$key = q/{eqnarraystar}indicatrice[AsetminusB]&=&indicatrice_A-indicatrice[AcapB]&=&indicatrice_A-indicatrice_Acdotindicatrice_B{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.34ex; " SRC="|."$dir".q|img34.svg"
 ALT="\begin{eqnarray*}
\indicatrice[A \setminus B] &amp;=&amp; \indicatrice_A - \indicatrice[...
... B] \\\\
&amp;=&amp; \indicatrice_A - \indicatrice_A \cdot \indicatrice_B
\end{eqnarray*}">|; 

1;

