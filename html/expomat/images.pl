# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 30.70ex; " SRC="|."$dir".q|img20.svg"
 ALT="\begin{eqnarray*}
\OD{}{t} \exp(A \cdot t) &amp;=&amp; \sum_{k=1}^{+\infty} \unsur{k!} \...
...{k!} \cdot A^k \cdot k \cdot t^k \\\\
&amp;=&amp; A \cdot \exp(A \cdot t)
\end{eqnarray*}">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img54.svg"
 ALT="$A$">|; 

$key = q/A=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$A = 0$">|; 

$key = q/A=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$A=1$">|; 

$key = q/A=A^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img55.svg"
 ALT="$A = A^\dual$">|; 

$key = q/Ainmathfrak{M}(setR,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$A\in\mathfrak{M}(\setR,n,n)$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img23.svg"
 ALT="$E$">|; 

$key = q/E_0(t)=E_0(0)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$E_0(t) = E_0(0) = I$">|; 

$key = q/E_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img9.svg"
 ALT="$E_0$">|; 

$key = q/L(t)=L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$L(t) = L$">|; 

$key = q/R(-s)=[R(s)]^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img104.svg"
 ALT="$R(-s) = [R(s)]^{-1}$">|; 

$key = q/R(0)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img91.svg"
 ALT="$R(0) = I$">|; 

$key = q/R(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img88.svg"
 ALT="$R(t)$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img82.svg"
 ALT="$R$">|; 

$key = q/U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img57.svg"
 ALT="$U$">|; 

$key = q/U^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img63.svg"
 ALT="$U^\dual$">|; 

$key = q/X(0)insetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img79.svg"
 ALT="$X(0) \in \setR^n$">|; 

$key = q/X:setRmapstosetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img52.svg"
 ALT="$X : \setR \mapsto \setR^n$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img72.svg"
 ALT="$X$">|; 

$key = q/Y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img71.svg"
 ALT="$Y$">|; 

$key = q/Y=(y_i)_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$Y = (y_i)_i$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img106.svg"
 ALT="$b$">|; 

$key = q/displaystyleA=UcdotLambdacdotU^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle A = U \cdot \Lambda \cdot U^\dual$">|; 

$key = q/displaystyleE(t)=exp(Acdott);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle E(t) = \exp(A \cdot t)$">|; 

$key = q/displaystyleE_0(t)-E_0(0)=int_0^t0dt=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle E_0(t) - E_0(0) = \int_0^t 0  dt = 0$">|; 

$key = q/displaystyleE_A:setRmapstoinmathfrak{M}(setR,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle E_A : \setR \mapsto \in\mathfrak{M}(\setR,n,n)$">|; 

$key = q/displaystyleLambda=(lambda_icdotdelta_{ij})_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \Lambda = (\lambda_i \cdot \delta_{ij})_{i,j}$">|; 

$key = q/displaystyleNOD{u}{t}{k}(0)=AcdotNOD{u}{t}{k-1}(0)=AcdotA^{k-1}=A^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.26ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \NOD{u}{t}{k}(0) = A \cdot \NOD{u}{t}{k - 1}(0) = A \cdot A^{k - 1} = A^k$">|; 

$key = q/displaystyleOD{E_0}{t}=0cdotE_0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \OD{E_0}{t} = 0 \cdot E_0 = 0$">|; 

$key = q/displaystyleOD{u}{t}(0)=Acdotu(0)=AcdotI=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \OD{u}{t}(0) = A \cdot u(0) = A \cdot I = A$">|; 

$key = q/displaystyleOD{u}{t}(t)=OD{L}{t}(t)cdotu(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle \OD{u}{t}(t) = \OD{L}{t}(t) \cdot u(t)$">|; 

$key = q/displaystyleR(s+t)=R(s)cdotR(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle R(s+t) = R(s) \cdot R(t)$">|; 

$key = q/displaystyleR(t)=expint_0^tL(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle R(t) = \exp\int_0^t L(s) ds$">|; 

$key = q/displaystyleR:setRmapstomathfrak{M}(setR,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle R : \setR\mapsto\mathfrak{M}(\setR,n,n)$">|; 

$key = q/displaystyleU^dual=U^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle U^\dual = U^{-1}$">|; 

$key = q/displaystyleUcdotdot{Y}=AcdotUcdotYdollar;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle U \cdot \dot{Y} = A \cdot U \cdot Y \ $">|; 

$key = q/displaystyleX(t)=UcdotY(t)=Ucdotexp(Lambdacdott)cdotU^dualcdotX(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle X(t) = U \cdot Y(t) = U \cdot \exp(\Lambda \cdot t) \cdot U^\dual \cdot X(0)$">|; 

$key = q/displaystyleX(t)=exp(Acdott)cdotX(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle X(t) = \exp(A \cdot t) \cdot X(0)$">|; 

$key = q/displaystyleX=UcdotYquadLeftrightarrowquadY=U^dualcdotX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle X = U \cdot Y \quad\Leftrightarrow\quad Y = U^\dual \cdot X$">|; 

$key = q/displaystyleY(0)=U^dualcdotX(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle Y(0) = U^\dual \cdot X(0)$">|; 

$key = q/displaystyleY(t)=exp(Lambdacdott)cdotU^dualcdotX(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle Y(t) = \exp(\Lambda \cdot t) \cdot U^\dual \cdot X(0)$">|; 

$key = q/displaystyleY(t)=exp(Lambdacdott)cdotY(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle Y(t) = \exp(\Lambda \cdot t) \cdot Y(0)$">|; 

$key = q/displaystyledot{X}(t)=AcdotX(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \dot{X}(t) = A \cdot X(t)$">|; 

$key = q/displaystyledot{Y}=U^dualcdotAcdotUcdotY=LambdacdotY;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle \dot{Y} = U^\dual \cdot A \cdot U \cdot Y = \Lambda \cdot Y$">|; 

$key = q/displaystyledot{u}=Lcdotu+b=LcdotRcdotx+b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle \dot{u} = L \cdot u + b = L \cdot R \cdot x + b$">|; 

$key = q/displaystyledot{u}=dot{R}cdotx+Rcdotdot{x}=LcdotRcdotx+Rcdotdot{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.49ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle \dot{u} = \dot{R} \cdot x + R \cdot \dot{x} = L \cdot R \cdot x + R \cdot \dot{x}$">|; 

$key = q/displaystyledot{x}(t)=Acdotexp(Acdott)cdotx_0=Acdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \dot{x}(t) = A \cdot \exp(A \cdot t) \cdot x_0 = A \cdot x$">|; 

$key = q/displaystyledot{y}_i=lambda_icdoty_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle \dot{y}_i = \lambda_i \cdot y_i$">|; 

$key = q/displaystyleexp(0)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \exp(0) = I$">|; 

$key = q/displaystyleexp(A)=E_A(1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \exp(A) = E_A(1)$">|; 

$key = q/displaystyleexp(A)=Ucdotexp(Lambda)cdotU^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle \exp(A) = U \cdot \exp(\Lambda) \cdot U^\dual$">|; 

$key = q/displaystyleexp(A)=sum_{k=0}^{+infty}frac{1}{k!}cdotA^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \exp(A) = \sum_{k=0}^{+\infty} \frac{1}{k!} \cdot A^k$">|; 

$key = q/displaystyleexp(A)^{-1}=exp(-A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \exp(A)^{-1} = \exp(- A)$">|; 

$key = q/displaystyleexp(A)cdotX(0)=Ucdotexp(Lambda)cdotU^dualcdotX(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle \exp(A) \cdot X(0) = U \cdot \exp(\Lambda) \cdot U^\dual \cdot X(0)$">|; 

$key = q/displaystyleexp(Acdot(t-t))=exp(Acdot0)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \exp(A \cdot (t - t)) = \exp(A \cdot 0) = I$">|; 

$key = q/displaystyleexp(Acdot(t-t))=exp(Acdott)cdotexp(Acdot(-t))=exp(Acdott)cdotexp(-Acdott);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \exp(A \cdot (t - t)) = \exp(A \cdot t) \cdot \exp(A \cdot (-t)) = \exp(A \cdot t) \cdot \exp(- A \cdot t)$">|; 

$key = q/displaystyleexp(Acdot0)=I+sum_{k=1}^{+infty}unsur{k!}cdotA^kcdot0^k=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \exp(A \cdot 0) = I + \sum_{k=1}^{+\infty} \unsur{k!} \cdot A^k \cdot 0^k = I$">|; 

$key = q/displaystyleexp(Acdott)=sum_{k=0}^{+infty}frac{1}{k!}cdotA^kcdott^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \exp(A \cdot t) = \sum_{k=0}^{+\infty} \frac{1}{k!} \cdot A^k \cdot t^k$">|; 

$key = q/displaystyleexp(Acdott)^{-1}=exp(-Acdott);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \exp(A \cdot t)^{-1} = \exp(- A \cdot t)$">|; 

$key = q/displaystyleexp(Acdott)cdotX(0)=Ucdotexp(Lambdacdott)cdotU^dualcdotX(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle \exp(A \cdot t) \cdot X(0) = U \cdot \exp(\Lambda \cdot t) \cdot U^\dual \cdot X(0)$">|; 

$key = q/displaystyleexp(Acdott)cdotexp(-Acdott)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \exp(A \cdot t) \cdot \exp(- A \cdot t) = I$">|; 

$key = q/displaystyleexp(Lambdacdott)=Big(exp(lambda_icdott)cdotindicatrice_{ij}Big)_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.76ex; vertical-align: -2.00ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \exp(\Lambda \cdot t) = \Big( \exp(\lambda_i \cdot t) \cdot \indicatrice_{ij} \Big)_{i,j}$">|; 

$key = q/displaystyleexp(t)=sum_{k=0}^{+infty}frac{t^k}{k!};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \exp(t) = \sum_{k=0}^{+\infty} \frac{t^k}{k!}$">|; 

$key = q/displaystyleexpBig(Acdot(s+t)Big)=exp(Acdots)cdotexp(Acdott);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \exp\Big(A \cdot (s + t) \Big) = \exp(A \cdot s) \cdot \exp(A \cdot t)$">|; 

$key = q/displaystylesum_kunsur{k!}Lambda^kcdott^k=Bigg(sum_kunsur{k!}lambda_i^kcdott^kcdotindicatrice_{ij}Bigg)_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.53ex; vertical-align: -3.40ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \sum_k \unsur{k !}  \Lambda^k \cdot t^k = \Bigg( \sum_k \unsur{k !}  \lambda_i^k \cdot t^k \cdot \indicatrice_{ij} \Bigg)_{i,j}$">|; 

$key = q/displaystyleu(0)=exp(Acdot0)=exp(0)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle u(0) = \exp(A \cdot 0) = \exp(0) = I$">|; 

$key = q/displaystyleu(t)=R(t)cdotu_0+int_0^tR(t)cdot[R(s)]^{-1}cdotb(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle u(t) = R(t) \cdot u_0 + \int_0^t R(t) \cdot [R(s)]^{-1} \cdot b(s) ds$">|; 

$key = q/displaystyleu(t)=R(t)cdotu_0+int_0^tR(t-s)cdotb(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle u(t) = R(t) \cdot u_0 + \int_0^t R(t-s) \cdot b(s) ds$">|; 

$key = q/displaystyleu:setRmapstomatrice(setR,n,n),tmapstoexp(Acdott);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle u : \setR \mapsto \matrice(\setR, n, n),  t \mapsto \exp(A \cdot t)$">|; 

$key = q/displaystyleu_x(x,0)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle u_x(x,0) = I$">|; 

$key = q/displaystyleu_x(x,t)=deriveepartielle{u}{x^T}(x,t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle u_x(x,t) = \deriveepartielle{u}{x^T}(x,t)$">|; 

$key = q/displaystyleu_x(x,t)=expint_0^tderiveepartielle{f}{u^T}(s,u(x,s))ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle u_x(x,t) = \exp\int_0^t \deriveepartielle{f}{u^T}(s,u(x,s)) ds$">|; 

$key = q/displaystylex(0)=exp(Acdot0)cdotx_0=Icdotx_0=x_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle x(0) = \exp(A \cdot 0) \cdot x_0 = I \cdot x_0 = x_0$">|; 

$key = q/displaystylex(t)=exp(Acdott)cdotx_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle x(t) = \exp(A \cdot t) \cdot x_0$">|; 

$key = q/displaystylex(t)=u_0+int_0^tleft[R(s)right]^{-1}cdotb(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle x(t) = u_0 + \int_0^t \left[ R(s) \right]^{-1} \cdot b(s) ds$">|; 

$key = q/displaystyley_i(t)=y_{i}(0)cdotexp(lambda_icdott);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle y_i(t) = y_{i}(0) \cdot \exp(\lambda_i \cdot t)$">|; 

$key = q/exp(Acdott);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\exp(A \cdot t)$">|; 

$key = q/f,g:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img31.svg"
 ALT="$f, g : \setR \mapsto \setR$">|; 

$key = q/lambda_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\lambda_i$">|; 

$key = q/mathbb{R};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\mathbb {R}$">|; 

$key = q/n=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img28.svg"
 ALT="$n=1$">|; 

$key = q/psi(s)=R(s)cdotR(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\psi(s) = R(s) \cdot R(t)$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\setR$">|; 

$key = q/sinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img33.svg"
 ALT="$s \in \setR$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img89.svg"
 ALT="$t$">|; 

$key = q/t=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$t=0$">|; 

$key = q/t=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$t = 1$">|; 

$key = q/tinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img11.svg"
 ALT="$t \in \setR$">|; 

$key = q/tmapstoexp(Acdott);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$t \mapsto \exp(A \cdot t)$">|; 

$key = q/u(x,t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$u(x,t)$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$u$">|; 

$key = q/u=Rcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img90.svg"
 ALT="$u = R \cdot x$">|; 

$key = q/u_0=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img107.svg"
 ALT="$u_0 = x$">|; 

$key = q/varphi(s)=R(s+t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\varphi(s) = R(s+t)$">|; 

$key = q/w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img102.svg"
 ALT="$w$">|; 

$key = q/x(0)=u_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img92.svg"
 ALT="$x(0) = u_0$">|; 

$key = q/x:setRmapstosetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img45.svg"
 ALT="$x : \setR \mapsto \setR^n$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img49.svg"
 ALT="$x$">|; 

$key = q/x_0insetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img44.svg"
 ALT="$x_0 \in \setR^n$">|; 

$key = q/{Eqts}L(t)mapstoderiveepartielle{f}{u^T}(t,u(x,t))R(t)mapstou_x(x,t){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.52ex; vertical-align: -3.70ex; " SRC="|."$dir".q|img113.svg"
 ALT="\begin{Eqts}
L(t) \mapsto \deriveepartielle{f}{u^T}(t,u(x,t)) \\\\
R(t) \mapsto u_x(x,t)
\end{Eqts}">|; 

$key = q/{Eqts}OD{E_A}{t}=AcdotE_AE_A(0)=I{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.90ex; vertical-align: -5.39ex; " SRC="|."$dir".q|img4.svg"
 ALT="\begin{Eqts}
\OD{E_A}{t} = A \cdot E_A \ \\\\
E_A(0) = I
\end{Eqts}">|; 

$key = q/{Eqts}OD{w}{s}(s)=Lcdotw(s)w(0)=R(t){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.90ex; vertical-align: -5.39ex; " SRC="|."$dir".q|img103.svg"
 ALT="\begin{Eqts}
\OD{w}{s}(s) = L \cdot w(s) \ \\\\
w(0) = R(t)
\end{Eqts}">|; 

$key = q/{Eqts}OD{x}{t}=Acdotxx(0)=x_0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.90ex; vertical-align: -5.39ex; " SRC="|."$dir".q|img50.svg"
 ALT="\begin{Eqts}
\OD{x}{t} = A \cdot x \ \\\\
x(0) = x_0
\end{Eqts}">|; 

$key = q/{Eqts}deriveepartielle{u}{t}(x,t)=f(t,u(x,t))u(x,0)=x{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.52ex; vertical-align: -3.70ex; " SRC="|."$dir".q|img109.svg"
 ALT="\begin{Eqts}
\deriveepartielle{u}{t}(x,t) = f(t,u(x,t)) \\\\
u(x,0) = x
\end{Eqts}">|; 

$key = q/{Eqts}dot{u}(t)=L(t)cdotu(t)+b(t)u(0)=u_0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img86.svg"
 ALT="\begin{Eqts}
\dot{u}(t) = L(t) \cdot u(t) + b(t) \ \\\\
u(0) = u_0
\end{Eqts}">|; 

$key = q/{Eqts}dot{x}=R^{-1}cdotbx(0)=u_0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img95.svg"
 ALT="\begin{Eqts}
\dot{x} = R^{-1} \cdot b \\\\
x(0) = u_0
\end{Eqts}">|; 

$key = q/{eqnarraystar}dot{R}(t)&=&L(t)cdotR(t)R(0)&=&I{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img84.svg"
 ALT="\begin{eqnarray*}
\dot{R}(t) &amp;=&amp; L(t) \cdot R(t) \\\\
R(0) &amp;=&amp; I
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}dot{u}&=&uu(0)&=&1{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img51.svg"
 ALT="\begin{eqnarray*}
\dot{u} &amp;=&amp; u \\\\
u(0) &amp;=&amp; 1
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}f&:&smapstoexpBig(Acdot(s+t)Big)g&:&smapstoexp(Acdots)cdotexp(Acdott){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.21ex; " SRC="|."$dir".q|img32.svg"
 ALT="\begin{eqnarray*}
f &amp;:&amp; s \mapsto \exp\Big(A \cdot (s + t) \Big) \\\\
g &amp;:&amp; s \mapsto \exp(A \cdot s) \cdot \exp(A \cdot t)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}partialf(s)&=&AcdotexpBig(Acdot(s+t)Big)=Acdotf(s)f(0)&=&expBig(Acdot(0+t)Big)=exp(Acdott){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.06ex; " SRC="|."$dir".q|img34.svg"
 ALT="\begin{eqnarray*}
\partial f(s) &amp;=&amp; A \cdot \exp\Big(A \cdot (s + t) \Big) = A \...
...s) \\\\
f(0) &amp;=&amp; \exp\Big(A \cdot (0 + t) \Big) = \exp(A \cdot t)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}partialg(s)&=&Acdotexp(Acdots)cdotexp(t)=Acdotg(s)g(0)&=&exp(Acdot0)cdotexp(Acdott)=Icdotexp(Acdott)=exp(Acdott){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img35.svg"
 ALT="\begin{eqnarray*}
\partial g(s) &amp;=&amp; A \cdot \exp(A \cdot s) \cdot \exp(t) = A \c...
...cdot \exp(A \cdot t) = I \cdot \exp(A \cdot t) = \exp(A \cdot t)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}partialu(s)&=&Acdotu(s)u(0)&=&exp(Acdott){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img36.svg"
 ALT="\begin{eqnarray*}
\partial u(s) &amp;=&amp; A \cdot u(s) \\\\
u(0) &amp;=&amp; \exp(A \cdot t)
\end{eqnarray*}">|; 

1;

