# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate labels original text with physical files.


$key = q/chap:expomat/;
$external_labels{$key} = "$URL/" . q|expomat.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:edo_sys_lin/;
$external_labels{$key} = "$URL/" . q|expomat.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2023 (Released January 1, 2023)
# labels from external_latex_labels array.


$key = q/chap:expomat/;
$external_latex_labels{$key} = q|1 Exponentielle matricielle|; 
$noresave{$key} = "$nosave";

$key = q/sec:edo_sys_lin/;
$external_latex_labels{$key} = q|1.11 Systèmes linéaires|; 
$noresave{$key} = "$nosave";

1;

