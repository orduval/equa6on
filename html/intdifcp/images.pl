# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.37ex; vertical-align: -5.62ex; " SRC="|."$dir".q|img47.svg"
 ALT="\begin{Eqts}
\oint_C \frac{dz}{(z-a)^k} =
\int_0^{2\pi} \frac{R \img \exp(\img\t...
...}{(z-a)^k} = \img R^{1-k} \int_0^{2\pi} \exp[\img(1-k)\theta] d\theta
\end{Eqts}">|; 

$key = q/(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$(x,y)$">|; 

$key = q/(z-a)^p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$(z-a)^p$">|; 

$key = q/(z-a)^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$(z-a)^{-1}$">|; 

$key = q/(z-a)^{-k-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$(z-a)^{-k-1}$">|; 

$key = q/2piimg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img78.svg"
 ALT="$2\pi\img$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$A$">|; 

$key = q/AsubseteqsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img4.svg"
 ALT="$A\subseteq\setC$">|; 

$key = q/C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$C$">|; 

$key = q/Deltax,DeltayinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\Delta x,\Delta y\in\setR$">|; 

$key = q/Deltax;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\Delta x$">|; 

$key = q/F:setCmapstosetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img30.svg"
 ALT="$F : \setC \mapsto \setC$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$R$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$a$">|; 

$key = q/a_{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img77.svg"
 ALT="$a_{-1}$">|; 

$key = q/ainsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img56.svg"
 ALT="$a\in\setC$">|; 

$key = q/anotinl;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img57.svg"
 ALT="$a\notin l$">|; 

$key = q/displaystyle(z-a)^k=left(Rexp(imgtheta)right)^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle (z - a)^k = \left( R \exp(\img\theta) \right)^k$">|; 

$key = q/displaystyleC={gamma(theta)=a+Rexp(imgtheta):thetain[0,2pi)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle C = \{ \gamma(\theta) = a + R \exp(\img\theta) : \theta \in [ 0 , 2 \pi ) \}$">|; 

$key = q/displaystyleOD{f}{z}(a)=frac{k!}{2piimg}oint_{partialS}frac{f(z)}{(z-a)^{k+1}}dz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \OD{f}{z}(a) = \frac{k !}{2 \pi \img} \oint_{\partial S} \frac{f(z)}{(z-a)^{k+1}} dz$">|; 

$key = q/displaystyleOD{f}{z}(z)=lim_{substack{hto0hinsetC}}frac{f(z+h)-f(z)}{h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.59ex; vertical-align: -3.21ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle \OD{f}{z}(z) = \lim_{ \substack{ h \to 0 \ h \in\setC } } \frac{f(z+h) - f(z)}{h}$">|; 

$key = q/displaystyleOD{gamma}{theta}(theta)=Rimgexp(imgtheta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \OD{\gamma}{\theta}(\theta) = R \img \exp(\img\theta)$">|; 

$key = q/displaystyleOD{z^n}{z}=nz^{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \OD{z^n}{z}=n z^{n-1}$">|; 

$key = q/displaystyleOD{}{t}(Fcircgamma)(t)=(OD{F}{z}circgamma)(t)OD{gamma}{t}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \OD{}{t}(F \circ \gamma)(t) = (\OD{F}{z} \circ \gamma)(t) \OD{\gamma}{t}(t)$">|; 

$key = q/displaystylealpha_k=frac{1}{2piimg}oint_lfrac{f(z)}{(z-a)^{k+1}}dz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \alpha_k = \frac{1}{2 \pi \img} \oint_l \frac{f(z)}{(z-a)^{k+1}} dz$">|; 

$key = q/displaystylef(a)=frac{1}{2piimg}oint_{partialS}frac{f(z)}{z-a}dz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.58ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle f(a) = \frac{1}{2 \pi \img} \oint_{\partial S} \frac{f(z)}{z-a} dz$">|; 

$key = q/displaystylef(a)oint_{partialS}frac{1}{z-a}dz=oint_{partialS}frac{f(z)}{z-a}dz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.58ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle f(a) \oint_{\partial S} \frac{1}{z-a} dz = \oint_{\partial S} \frac{f(z)}{z-a} dz$">|; 

$key = q/displaystylef(z)=sum_{n=-infty}^{+infty}alpha_n(z-a)^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.30ex; vertical-align: -3.16ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle f(z) = \sum_{n=-\infty}^{+\infty} \alpha_n (z-a)^n$">|; 

$key = q/displaystylef(z)=sum_{n=-p}^{+infty}alpha_n(z-a)^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.44ex; vertical-align: -3.30ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle f(z) = \sum_{n=-p}^{+\infty} \alpha_n (z-a)^n$">|; 

$key = q/displaystylefrac{d^{p-1}}{dz^{p-1}}[(z-a)^pf(z)]=(p-1)!left[a_{-1}+a_0(z-a)+a_1(z-a)^2+...right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \frac{d^{p-1}}{dz^{p-1}}[(z-a)^p f(z)] = (p-1) ! \left[a_{-1} + a_0 (z-a) + a_1 (z-a)^2 + ...\right]$">|; 

$key = q/displaystyleint_0^{2pi}exp[imgltheta]dtheta=2pidelta_{l,0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \int_0^{2\pi} \exp[\img l \theta] d\theta = 2 \pi \delta_{l,0}$">|; 

$key = q/displaystyleint_Afdmu=int_Audmu+imgint_Avdmu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \int_A f d\mu = \int_A u d\mu + \img \int_A v d\mu$">|; 

$key = q/displaystyleint_lOD{F}{z}(z)dz=F(gamma(b))-F(gamma(a));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.46ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \int_l \OD{F}{z}(z) dz = F(\gamma(b))-F(\gamma(a))$">|; 

$key = q/displaystyleint_lf(z)dz=int_a^b(fcircgamma)(t)OD{gamma}{t}(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \int_l f(z) dz = \int_a^b (f \circ \gamma)(t) \OD{\gamma}{t}(t) dt$">|; 

$key = q/displaystylel={gamma(t):tin[a,b]}subsetsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle l = \{ \gamma(t) : t \in [a,b] \} \subset \setC$">|; 

$key = q/displaystylelim_{ztoa}frac{d^{p-1}}{dz^{p-1}}[(z-a)^pf(z)]=(p-1)!a_{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.21ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle \lim_{z \to a} \frac{d^{p-1}}{dz^{p-1}}[(z-a)^p f(z)] = (p-1) ! a_{-1}$">|; 

$key = q/displaystyleoint_Cfrac{dz}{(z-a)^k}=2piimgdelta_{k,1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \oint_C \frac{dz}{(z-a)^k} = 2 \pi \img \delta_{k,1}$">|; 

$key = q/displaystyleoint_lf(z)(z-a)^{-k-1}dz=sum_{n=-infty}^{+infty}alpha_noint_l(z-a)^{n-k-1}dz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.30ex; vertical-align: -3.16ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \oint_l f(z)(z-a)^{-k-1} dz = \sum_{n=-\infty}^{+\infty} \alpha_n \oint_l (z-a)^{n-k-1} dz$">|; 

$key = q/displaystyleoint_lf(z)dz=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \oint_l f(z) dz = 0$">|; 

$key = q/displaystyleoint_lf(z)dz=int_{partialS}f(z)dz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \oint_l f(z) dz = \int_{\partial S} f(z) dz$">|; 

$key = q/displaystyleoint_lf(z)dz=oint_l(udx-vdy)+imgoint_l(udy+vdx);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \oint_l f(z) dz = \oint_l (u dx - v dy) + \img \oint_l (u dy + v dx)$">|; 

$key = q/displaystyleoint_lfrac{f(z)-f(a)}{z-a}dz=oint_lg(z)dz=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.58ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \oint_l \frac{f(z)-f(a)}{z-a} dz = \oint_l g(z) dz = 0$">|; 

$key = q/displaystyleoint_lg(z)dz=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle \oint_l g(z) dz = 0$">|; 

$key = q/displaystyleoint_{partialS}frac{dz}{(z-a)^k}=2piimgdelta_{k,1}delta_S(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \oint_{\partial S} \frac{dz}{(z-a)^k} = 2 \pi \img \delta_{k,1} \delta_S(a)$">|; 

$key = q/displaystyleu,v:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle u,v : A \mapsto \setR$">|; 

$key = q/f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$f(a)$">|; 

$key = q/f(z)=(z-a)^{-k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$f(z) = (z-a)^{-k}$">|; 

$key = q/f(z)=u(x,y)+imgv(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$f(z) = u(x,y) + \img v(x,y)$">|; 

$key = q/f:AmapstosetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img19.svg"
 ALT="$f : A \mapsto \setC$">|; 

$key = q/f:setCmapstosetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f : \setC \mapsto \setC$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$f$">|; 

$key = q/gamma:[a,b]mapstosetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\gamma : [a,b] \mapsto \setC$">|; 

$key = q/h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$h$">|; 

$key = q/imgDeltay;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\img \Delta y$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img62.svg"
 ALT="$k$">|; 

$key = q/kinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img46.svg"
 ALT="$k \in \setZ$">|; 

$key = q/kne1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$k \ne 1$">|; 

$key = q/l;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img26.svg"
 ALT="$l$">|; 

$key = q/l=partialS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$l = \partial S$">|; 

$key = q/linsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img49.svg"
 ALT="$l\in\setZ$">|; 

$key = q/p-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img74.svg"
 ALT="$p-1$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img72.svg"
 ALT="$p$">|; 

$key = q/pinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img71.svg"
 ALT="$p \in \setZ$">|; 

$key = q/setC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\setC$">|; 

$key = q/setR^2mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\setR^2 \mapsto \setR$">|; 

$key = q/u,v:setR^2mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$u,v : \setR^2 \mapsto \setR$">|; 

$key = q/u=Re(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$u = \Re(f)$">|; 

$key = q/v=Im(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$v = \Im(f)$">|; 

$key = q/z-a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img64.svg"
 ALT="$z-a$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$z$">|; 

$key = q/zinC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img45.svg"
 ALT="$z\in C$">|; 

$key = q/{Eqts}deriveepartielle{u}{x}=deriveepartielle{v}{y}deriveepartielle{u}{y}=-deriveepartielle{v}{x}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.39ex; vertical-align: -5.13ex; " SRC="|."$dir".q|img15.svg"
 ALT="\begin{Eqts}
\deriveepartielle{u}{x} = \deriveepartielle{v}{y} \\\\
\deriveepartielle{u}{y} = -\deriveepartielle{v}{x}
\end{Eqts}">|; 

$key = q/{Eqts}frac{partial^2u}{partialx^2}+frac{partial^2u}{partialy^2}=0frac{partial^2v}{partialx^2}+frac{partial^2v}{partialy^2}=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.84ex; vertical-align: -5.36ex; " SRC="|."$dir".q|img16.svg"
 ALT="\begin{Eqts}
\frac{\partial^2 u}{\partial x^2} + \frac{\partial^2 u}{\partial y^...
...c{\partial^2 v}{\partial x^2} + \frac{\partial^2 v}{\partial y^2} = 0
\end{Eqts}">|; 

$key = q/{Eqts}g(z)=cases{frac{f(z)-f(a)}{z-a}&mbox{si}zneaOD{f}{z}(a)&mbox{si}z=acases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img54.svg"
 ALT="\begin{Eqts}
g(z) =
\begin{cases}
\frac{f(z)-f(a)}{z-a} &amp; \mbox{si } z \ne a \\\\
\OD{f}{z}(a) &amp; \mbox{si } z = a
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}int_lf(z)dz=int_l(u+imgv)(dx+imgdy)int_lf(z)dz=int_l(udx-vdy)+imgint_l(udy+vdx){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.44ex; vertical-align: -5.16ex; " SRC="|."$dir".q|img28.svg"
 ALT="\begin{Eqts}
\int_l f(z) dz = \int_l (u + \img v)(dx + \img dy) \\\\
\int_l f(z) dz = \int_l (u dx - v dy) + \img \int_l (u dy + v dx)
\end{Eqts}">|; 

$key = q/{Eqts}oint_lf(z)dz=int_S(-deriveepartielle{v}{x}-deriveepartielle{u}{y})dS+imgint_S(deriveepartielle{u}{x}-deriveepartielle{v}{y})dS{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.46ex; vertical-align: -2.16ex; " SRC="|."$dir".q|img37.svg"
 ALT="\begin{Eqts}
\oint_l f(z) dz = \int_S (-\deriveepartielle{v}{x}-\deriveepartiell...
... \img \int_S (\deriveepartielle{u}{x}-\deriveepartielle{v}{y}) dS \\\\
\end{Eqts}">|; 

$key = q/{Eqts}u(x,y)=Releft(f(x+imgy)right)v(x,y)=Imleft(f(x+imgy)right){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img7.svg"
 ALT="\begin{Eqts}
u(x,y) = \Re\left(f(x + \img y) \right) \\\\
v(x,y) = \Im\left(f(x + \img y) \right)
\end{Eqts}">|; 

1;

