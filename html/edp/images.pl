# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/AsubseteqsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img25.svg"
 ALT="$A \subseteq \setR^n$">|; 

$key = q/Fsubseteqlebesgue^2(setR^n,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.76ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$F \subseteq \lebesgue^2(\setR^n,\setR)$">|; 

$key = q/G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img35.svg"
 ALT="$G$">|; 

$key = q/Gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\Gamma$">|; 

$key = q/L(u)=f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$L(u) = f$">|; 

$key = q/L:Fmapstolebesgue^2(setR^n,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.76ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$L : F \mapsto \lebesgue^2(\setR^n,\setR)$">|; 

$key = q/L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img37.svg"
 ALT="$L$">|; 

$key = q/OmegasubseteqsetR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.34ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\Omega\subseteq\setR^2$">|; 

$key = q/a,b,c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$a,b,c$">|; 

$key = q/delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\delta$">|; 

$key = q/displaystyleG(x,y)=v_x(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle G(x,y)=v_x(y)$">|; 

$key = q/displaystyleGamma={left(w_x(t),w_y(t)right):tinsetR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \Gamma = \{ \left(w_x(t),w_y(t)\right) : t \in\setR \}$">|; 

$key = q/displaystyleL(u)=f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle L(u) = f$">|; 

$key = q/displaystyleL(v_x)(y)=delta(y-x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle L(v_x)(y)=\delta(y-x)$">|; 

$key = q/displaystyleL:umapstolaplaceu=sum_idfdxdx{u}{x_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.50ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle L : u \mapsto \laplace u = \sum_i \dfdxdx{u}{x_i}$">|; 

$key = q/displaystyleOD{varphi}{t}(t)=f(t,u(t));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \OD{\varphi}{t}(t) = f(t,u(t))$">|; 

$key = q/displaystyleOD{varphi}{t}=u_xa+u_yb=c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \OD{\varphi}{t} = u_x  a + u_y  b = c$">|; 

$key = q/displaystylea(x,y,u)u_x(x,y)+b(x,y,u)u_y(x,y)=c(x,y,u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle a(x,y,u)  u_x(x,y) + b(x,y,u)  u_y(x,y) = c(x,y,u)$">|; 

$key = q/displaystylef:(t,u)mapstocleft(w_x(t),w_y(t),uright);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle f : (t,u) \mapsto c\left(w_x(t),w_y(t),u\right)$">|; 

$key = q/displaystyleforme{L(v_x)}{u}=forme{v_x}{L(u)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \forme{L(v_x)}{u} = \forme{v_x}{L(u)}$">|; 

$key = q/displaystyleforme{u}{L(v)}=forme{L(u)}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \forme{u}{L(v)} = \forme{L(u)}{v}$">|; 

$key = q/displaystyleint_Adelta(x-a)f(x)dx=f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \int_A \delta(x-a)  f(x)  dx = f(a)$">|; 

$key = q/displaystyleint_Adelta(y-x)u(y)dy=int_Av_x(y)f(y)dy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \int_A \delta(y-x)  u(y)  dy = \int_A v_x(y)  f(y)  dy$">|; 

$key = q/displaystyleint_Au(x)cdotv(x)dx=forme{u}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \int_A u(x) \cdot v(x)  dx = \forme{u}{v}$">|; 

$key = q/displaystyleu(x)=int_AG(x,y)f(y)dy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle u(x) = \int_A G(x,y)  f(y)  dy$">|; 

$key = q/displaystylevarphi(0)=u_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \varphi(0) = u_0$">|; 

$key = q/displaystylevarphi(t)=uleft(w_x(t),w_y(t)right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \varphi(t) = u\left(w_x(t),w_y(t)\right)$">|; 

$key = q/finF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$f \in F$">|; 

$key = q/forme{}{}:F^DtimesFmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\forme{}{} : F^D \times F \mapsto \setR$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$t$">|; 

$key = q/u,vinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$u,v\in F$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$u$">|; 

$key = q/u_x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img7.svg"
 ALT="$u_x$">|; 

$key = q/u_y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img8.svg"
 ALT="$u_y$">|; 

$key = q/uinF=continue^1(setR^2,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$u \in F = \continue^1(\setR^2,\setR)$">|; 

$key = q/v_x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img33.svg"
 ALT="$v_x$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\varphi$">|; 

$key = q/w_x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img11.svg"
 ALT="$w_x$">|; 

$key = q/w_y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img12.svg"
 ALT="$w_y$">|; 

$key = q/x,y,u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$x,y,u$">|; 

$key = q/{Eqts}OD{w_x}{t}(t)=a(w_x(t),w_y(t),u(w_x(t),w_y(t)))OD{w_y}{t}(t)=b(w_x(t),w_y(t),u(w_x(t),w_y(t))){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.49ex; vertical-align: -4.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="\begin{Eqts}
\OD{w_x}{t}(t) = a(w_x(t),w_y(t),u(w_x(t),w_y(t))) \\\\
\OD{w_y}{t}(t) = b(w_x(t),w_y(t),u(w_x(t),w_y(t)))
\end{Eqts}">|; 

$key = q/{Eqts}u_x(x,y)=deriveepartielle{u}{x}(x,y)u_y(x,y)=deriveepartielle{u}{y}(x,y){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.94ex; vertical-align: -4.91ex; " SRC="|."$dir".q|img4.svg"
 ALT="\begin{Eqts}
u_x(x,y) = \deriveepartielle{u}{x}(x,y) \\\\
u_y(x,y) = \deriveepartielle{u}{y}(x,y)
\end{Eqts}">|; 

1;

