# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.87ex; " SRC="|."$dir".q|img20.svg"
 ALT="\begin{eqnarray*}
\OD{y}{x} = \OD{y}{z} \cdot \OD{z}{x} &amp;=&amp; \unsur{n} \cdot z^{\...
...{\alpha - m} \cdot x^{m - 1} \\\\
&amp;=&amp; \alpha \cdot x^{\alpha - 1}
\end{eqnarray*}">|; 

$key = q/alpha=1slashn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\alpha = 1/n$">|; 

$key = q/alpha=mslashn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\alpha = m/n$">|; 

$key = q/alphainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\alpha \in \setR$">|; 

$key = q/displaystyleOD{x}{y}=alphacdoty^{alpha-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.40ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \OD{x}{y} = \alpha \cdot y^{\alpha-1}$">|; 

$key = q/displaystyleOD{y}{x}=ncdotx^{n-1}=ncdotycdoty^{-alpha};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \OD{y}{x} = n \cdot x^{n - 1} = n \cdot y \cdot y^{-\alpha}$">|; 

$key = q/displaystyleOD{}{x}left(unsur{x}right)=OD{y}{x}=-frac{y}{x}=-frac{1}{x^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \OD{}{x}\left( \unsur{x} \right) = \OD{y}{x} = -\frac{y}{x} = -\frac{1}{x^2}$">|; 

$key = q/displaystyleOD{}{x}left(x^alpharight)=alphacdotx^{alpha-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \OD{}{x}\left( x^\alpha \right) = \alpha \cdot x^{\alpha-1}$">|; 

$key = q/displaystylexcdotdy+ycdotdx=d(1)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle x \cdot dy + y \cdot dx = d(1) = 0$">|; 

$key = q/displaystyley=x^nqquadLeftrightarrowqquadx=y^{1slashn};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.76ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle y = x^n \qquad \Leftrightarrow\qquad x = y^{1/n}$">|; 

$key = q/displaystyley=x^{-n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.53ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle y = x^{-n}$">|; 

$key = q/displaystyley=x^{mslashn};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.76ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle y = x^{m/n}$">|; 

$key = q/displaystyley=z^{1slashn};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.76ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle y = z^{1/n}$">|; 

$key = q/displaystylez=x^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.81ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle z = x^m$">|; 

$key = q/f:xmapstox^alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f : x \mapsto x^\alpha$">|; 

$key = q/minsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img16.svg"
 ALT="$m\in\setZ$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$n\in\setN$">|; 

$key = q/x,alphainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img2.svg"
 ALT="$x,\alpha \in \setR$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$z$">|; 

$key = q/{Eqts}xcdoty=1y=unsur{x}=x^{-1}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.41ex; vertical-align: -3.64ex; " SRC="|."$dir".q|img3.svg"
 ALT="\begin{Eqts}
x \cdot y = 1 \\\\
y = \unsur{x} = x^{-1}
\end{Eqts}">|; 

$key = q/{Eqts}z=x^{-1}y=z^n{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img9.svg"
 ALT="\begin{Eqts}
z = x^{-1} \\\\
y = z^n
\end{Eqts}">|; 

1;

