# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img30.svg"
 ALT="\begin{eqnarray*}
\mathcal{E}(\gamma) &amp;=&amp; \scalaire{e - \delta \cdot x}{e - \del...
...} \\\\
&amp;=&amp; \scalaire{e}{e} + \abs{\delta}^2 \cdot \scalaire{x}{x}
\end{eqnarray*}">|; 

$key = q/(a_1,a_2,...,a_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img136.svg"
 ALT="$(a_1,a_2,...,a_n)$">|; 

$key = q/(m,m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img185.svg"
 ALT="$(m,m)$">|; 

$key = q/(n,m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img174.svg"
 ALT="$(n,m)$">|; 

$key = q/(n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img168.svg"
 ALT="$(n,n)$">|; 

$key = q/(u_1,...,u_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img163.svg"
 ALT="$(u_1,...,u_m)$">|; 

$key = q/(u_1,...,u_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$(u_1,...,u_n)$">|; 

$key = q/(u_1,...,u_p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img207.svg"
 ALT="$(u_1,...,u_p)$">|; 

$key = q/(u_1,...,u_p,u_{p+1},...,u_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img211.svg"
 ALT="$(u_1,...,u_p,u_{p + 1},...,u_n)$">|; 

$key = q/(u_1,u_2,...,u_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img149.svg"
 ALT="$(u_1,u_2,...,u_k)$">|; 

$key = q/(u_1,u_2,...,u_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img160.svg"
 ALT="$(u_1,u_2,...,u_n)$">|; 

$key = q/(u_1,u_2,...u_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img135.svg"
 ALT="$(u_1,u_2,...u_n)$">|; 

$key = q/(u_{p+1},...,u_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img212.svg"
 ALT="$(u_{p + 1},...,u_n)$">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img114.svg"
 ALT="$1$">|; 

$key = q/2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img115.svg"
 ALT="$2$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img194.svg"
 ALT="$A$">|; 

$key = q/A=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img213.svg"
 ALT="$A = I$">|; 

$key = q/Acdotx=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img218.svg"
 ALT="$A \cdot x = y$">|; 

$key = q/Ainmatrice(corps,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img195.svg"
 ALT="$A \in \matrice(\corps,n,n)$">|; 

$key = q/B=boule(0,r)={x:norme{x}ler};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$B = \boule(0,r) = \{ x : \norme{x} \le r \}$">|; 

$key = q/DeltainU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\Delta \in U$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$E$">|; 

$key = q/P=(p_{ij})_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img172.svg"
 ALT="$P = ( p_{ij} )_{i,j}$">|; 

$key = q/Q=I-P;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img187.svg"
 ALT="$Q = I - P$">|; 

$key = q/U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img79.svg"
 ALT="$U$">|; 

$key = q/U=U_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img206.svg"
 ALT="$U = U_n$">|; 

$key = q/U=combilin{u_1,...,u_m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img166.svg"
 ALT="$U = \combilin{u_1,...,u_m}$">|; 

$key = q/U^dual=U^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img192.svg"
 ALT="$U^\dual = U^{-1}$">|; 

$key = q/U^dualcdotU=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img184.svg"
 ALT="$U^\dual \cdot U = I$">|; 

$key = q/U_k=combilin{u_1,u_2,...,u_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img152.svg"
 ALT="$U_k = \combilin{u_1,u_2,...,u_k}$">|; 

$key = q/U_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img215.svg"
 ALT="$U_m$">|; 

$key = q/U_m^dualcdotU_m=I_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.35ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img216.svg"
 ALT="$U_m^\dual \cdot U_m = I_m$">|; 

$key = q/UsubseteqE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img76.svg"
 ALT="$U \subseteq E$">|; 

$key = q/a_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img197.svg"
 ALT="$a_1$">|; 

$key = q/a_1ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img137.svg"
 ALT="$a_1 \ne 0$">|; 

$key = q/a_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img140.svg"
 ALT="$a_2$">|; 

$key = q/a_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img162.svg"
 ALT="$a_k$">|; 

$key = q/a_{k+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img151.svg"
 ALT="$a_{k + 1}$">|; 

$key = q/abs{alpha}lerslashd;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\abs{\alpha} \le r / d$">|; 

$key = q/abs{delta}^2cdotscalaire{x}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\abs{\delta}^2 \cdot \scalaire{x}{x}$">|; 

$key = q/abs{delta}^2strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\abs{\delta}^2 \strictsuperieur 0$">|; 

$key = q/alpha=-rslashd;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\alpha = - r / d$">|; 

$key = q/alpha=rslashd;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\alpha = r / d$">|; 

$key = q/alphainsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\alpha \in \setC$">|; 

$key = q/b_{ik}=composante_{ik}U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.35ex; vertical-align: -0.65ex; " SRC="|."$dir".q|img180.svg"
 ALT="$b_{ik} = \composante_{ik} U$">|; 

$key = q/c_{kj}=composante_{kj}U^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.62ex; vertical-align: -0.95ex; " SRC="|."$dir".q|img181.svg"
 ALT="$c_{kj} = \composante_{kj} U^\dual$">|; 

$key = q/cne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img60.svg"
 ALT="$c \ne 0$">|; 

$key = q/combilin{u_1,...,u_m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\combilin{u_1,...,u_m}$">|; 

$key = q/combilin{u_1,u_2,...,u_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img200.svg"
 ALT="$\combilin{u_1,u_2,...,u_k}$">|; 

$key = q/combilin{u_{m+1},...,u_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\combilin{u_{m + 1},...,u_n}$">|; 

$key = q/combilin{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\combilin{x}$">|; 

$key = q/corps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img190.svg"
 ALT="$\corps^n$">|; 

$key = q/delta=gamma-lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\delta = \gamma - \lambda$">|; 

$key = q/deltane0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\delta \ne 0$">|; 

$key = q/displaystyle-2lambda_kcdotscalaire{u_k}{z}+2lambda_k=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle -2 \lambda_k \cdot \scalaire{u_k}{z} + 2 \lambda_k = 0$">|; 

$key = q/displaystyle-dcdotrle-norme{c}cdotnorme{x}lescalaire{c}{x}lenorme{c}cdotnorme{x}ledcdotr;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle - d \cdot r \le - \norme{c} \cdot \norme{x} \le \scalaire{c}{x} \le \norme{c} \cdot \norme{x} \le d \cdot r$">|; 

$key = q/displaystyleD=min_{gammainsetC}norme{y-gammacdotx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.91ex; vertical-align: -2.08ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle D = \min_{\gamma \in \setC} \norme{y - \gamma \cdot x}$">|; 

$key = q/displaystyleD=min{norme{z-v}:vinU};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle D = \min \{ \norme{z - v} : v \in U \}$">|; 

$key = q/displaystyleD^2=norme{y-lambdacdotx}^2=norme{e}^2=norme{y}^2-norme{p}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle D^2 = \norme{y - \lambda \cdot x}^2 = \norme{e}^2 = \norme{y}^2 - \norme{p}^2$">|; 

$key = q/displaystyleD^2=norme{y}^2-abs{lambda}^2cdotnorme{x}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle D^2 = \norme{y}^2 - \abs{\lambda}^2 \cdot \norme{x}^2$">|; 

$key = q/displaystyleD^2=norme{z}^2-norme{p}^2=norme{z}^2-sum_iabs{scalaire{u_i}{z}}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle D^2 = \norme{z}^2 - \norme{p}^2 = \norme{z}^2 - \sum_i \abs{\scalaire{u_i}{z}}^2$">|; 

$key = q/displaystyleP=UcdotU^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img182.svg"
 ALT="$\displaystyle P = U \cdot U^\dual$">|; 

$key = q/displaystyleP=sum_{k=1}^mu_kotimesu_k=sum_{k=1}^mu_kcdotu_k^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img167.svg"
 ALT="$\displaystyle P = \sum_{k = 1}^m u_k \otimes u_k = \sum_{k = 1}^m u_k \cdot u_k^\dual$">|; 

$key = q/displaystyleP^2=UcdotU^dualcdotUcdotU^dual=UcdotIcdotU^dual=UcdotU^dual=P;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img186.svg"
 ALT="$\displaystyle P^2 = U \cdot U^\dual \cdot U \cdot U^\dual = U \cdot I \cdot U^\dual = U \cdot U^\dual = P$">|; 

$key = q/displaystylePcdotQ=QcdotP=P-P^2=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img188.svg"
 ALT="$\displaystyle P \cdot Q = Q \cdot P = P - P^2 = 0$">|; 

$key = q/displaystylePcdotz=sum_{k=1}^mu_kcdotu_k^dualcdotz=sum_{k=1}^mu_kcdotscalaire{u_k}{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img170.svg"
 ALT="$\displaystyle P \cdot z = \sum_{k = 1}^m u_k \cdot u_k^\dual \cdot z = \sum_{k = 1}^m u_k \cdot \scalaire{u_k}{z}$">|; 

$key = q/displaystyleU=[u_1u_2hdotsu_m];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img177.svg"
 ALT="$\displaystyle U = [u_1  u_2  \hdots  u_m]$">|; 

$key = q/displaystyleU^{-1}=U^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img217.svg"
 ALT="$\displaystyle U^{-1} = U^\dual$">|; 

$key = q/displaystyleU_m=[u_1u_2...u_m];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img204.svg"
 ALT="$\displaystyle U_m = [u_1  u_2  ...  u_m]$">|; 

$key = q/displaystyleU_m^dualcdotAcdotU_m=I_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img205.svg"
 ALT="$\displaystyle U_m^\dual \cdot A \cdot U_m = I_m$">|; 

$key = q/displaystyleabs{scalaire{x}{y}}lenorme{x}cdotnorme{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \abs{ \scalaire{x}{y} } \le \norme{x} \cdot \norme{y}$">|; 

$key = q/displaystylecomposante_{ij}(U^dualcdotU)=u_i^dualcdotu_j=scalaire{u_i}{u_j}=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.32ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img183.svg"
 ALT="$\displaystyle \composante_{ij} (U^\dual \cdot U) = u_i^\dual \cdot u_j = \scalaire{u_i}{u_j} = \indicatrice_{ij}$">|; 

$key = q/displaystyleconjaccentlambdacdotlambdacdotscalaire{x}{x}lescalaire{y}{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.84ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \conjaccent \lambda \cdot \lambda \cdot \scalaire{x}{x} \le \scalaire{y}{y}$">|; 

$key = q/displaystyleconjaccent{lambda}=conjuguefrac{scalaire{x}{y}}{scalaire{x}{x}}=frac{scalaire{y}{x}}{scalaire{x}{x}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \conjaccent{\lambda} = \conjugue \frac{ \scalaire{x}{y} }{ \scalaire{x}{x} } = \frac{ \scalaire{y}{x} }{ \scalaire{x}{x} }$">|; 

$key = q/displaystyled=norme{c}=sqrt{scalaire{c}{c}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle d = \norme{c} = \sqrt{ \scalaire{c}{c} }$">|; 

$key = q/displaystylee=y-lambdacdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle e = y - \lambda \cdot x$">|; 

$key = q/displaystylee=z-p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle e = z - p$">|; 

$key = q/displaystylee_2=(tenseuridentite-mathcal{P}_1)cdota_2=a_2-u_1cdotscalaire{u_1}{a_2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle e_2 = (\tenseuridentite - \mathcal{P}_1) \cdot a_2 = a_2 - u_1 \cdot \scalaire{u_1}{a_2}$">|; 

$key = q/displaystylee_{k+1}=(tenseuridentite-mathcal{P}_k)cdota_{k+1}=a_{k+1}-sum_{i=1}^ku_icdotscalaire{u_i}{a_{k+1}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.30ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\displaystyle e_{k + 1} = (\tenseuridentite - \mathcal{P}_k) \cdot a_{k + 1} = a_{k + 1} - \sum_{i = 1}^k u_i \cdot \scalaire{u_i}{a_{k + 1} }$">|; 

$key = q/displaystyleeta=frac{r}{d}cdotcinargmax_{xinB}scalaire{c}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.52ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \eta = \frac{r}{d} \cdot c \in \arg\max_{x \in B} \scalaire{c}{x}$">|; 

$key = q/displaystylefrac{scalaire{y}{x}}{scalaire{x}{x}}cdotfrac{scalaire{x}{y}}{scalaire{x}{x}}cdotscalaire{x}{x}lescalaire{y}{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \frac{ \scalaire{y}{x} }{ \scalaire{x}{x} } \cdot \frac{ \scalaire{x}{y} }{ \scalaire{x}{x} } \cdot \scalaire{x}{x} \le \scalaire{y}{y}$">|; 

$key = q/displaystylelambda=(lambda_1,...,lambda_n)=(scalaire{u_1}{z},...,scalaire{u_n}{z});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \lambda = (\lambda_1,...,\lambda_n) = (\scalaire{u_1}{z},...,\scalaire{u_n}{z})$">|; 

$key = q/displaystylelambda=argmin_{gammainsetC}mathcal{E}(gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.91ex; vertical-align: -2.08ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \lambda = \arg\min_{\gamma \in \setC} \mathcal{E}(\gamma)$">|; 

$key = q/displaystylelambda=frac{scalaire{x}{y}}{scalaire{x}{x}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \lambda = \frac{\scalaire{x}{y}}{\scalaire{x}{x}}$">|; 

$key = q/displaystylemathcal{E}(gamma)=norme{y-gammacdotx}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \mathcal{E}(\gamma) = \norme{y - \gamma \cdot x}^2$">|; 

$key = q/displaystylemathcal{E}(gamma)=norme{z-sum_{i=1}^ngamma_icdotu_i}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.54ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle \mathcal{E}(\gamma) = \norme{z - \sum_{i = 1}^n \gamma_i \cdot u_i}^2$">|; 

$key = q/displaystylemathcal{E}(gamma)=scalaire{e}{e}+scalaire{Delta}{Delta}gescalaire{e}{e}=mathcal{E}(lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle \mathcal{E}(\gamma) = \scalaire{e}{e} + \scalaire{\Delta}{\Delta} \ge \scalaire{e}{e} = \mathcal{E}(\lambda)$">|; 

$key = q/displaystylemathcal{E}(gamma)=scalaire{y-gammacdotx}{y-gammacdotx}=scalaire{y}{y}-2cdotgammacdotscalaire{x}{y}+gamma^2cdotscalaire{x}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \mathcal{E}(\gamma) = \scalaire{y - \gamma \cdot x}{y - \gamma \c...
...e{y}{y} - 2 \cdot \gamma \cdot \scalaire{x}{y} + \gamma^2 \cdot \scalaire{x}{x}$">|; 

$key = q/displaystylemathcal{E}(gamma)gescalaire{e}{e}=mathcal{E}(lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \mathcal{E}(\gamma) \ge \scalaire{e}{e} = \mathcal{E}(\lambda)$">|; 

$key = q/displaystylemathcal{P}=sum_iu_iotimesu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\displaystyle \mathcal{P} = \sum_i u_i \otimes u_i$">|; 

$key = q/displaystylemathcal{P}=sum_{i=1}^mu_iotimesu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\displaystyle \mathcal{P} = \sum_{i = 1}^m u_i \otimes u_i$">|; 

$key = q/displaystylemathcal{P}_k=sum_{i=1}^ku_iotimesu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.30ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle \mathcal{P}_k = \sum_{i = 1}^k u_i \otimes u_i$">|; 

$key = q/displaystylemathcal{P}cdotmathcal{Q}=mathcal{P}cdotmathcal{P}-mathcal{P}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.32ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\displaystyle \mathcal{P} \cdot \mathcal{Q} = \mathcal{P} \cdot \mathcal{P} - \mathcal{P} = 0$">|; 

$key = q/displaystylemathcal{P}cdoty=sum_iu_icdotscalaire{u_i}{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle \mathcal{P} \cdot y = \sum_i u_i \cdot \scalaire{u_i}{y}$">|; 

$key = q/displaystylemathcal{P}cdotz=y=mathcal{P}cdoty=mathcal{P}cdot(mathcal{P}cdotz);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle \mathcal{P} \cdot z = y = \mathcal{P} \cdot y = \mathcal{P} \cdot (\mathcal{P} \cdot z)$">|; 

$key = q/displaystylemathcal{Q}=tenseuridentite-mathcal{P}=sum_{i=1}^nu_iotimesu_i-sum_{i=1}^mu_iotimesu_i=sum_{i=m+1}^nu_iotimesu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.05ex; vertical-align: -3.18ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\displaystyle \mathcal{Q} = \tenseuridentite - \mathcal{P} = \sum_{i = 1}^n u_i \otimes u_i - \sum_{i = 1}^m u_i \otimes u_i = \sum_{i = m + 1}^n u_i \otimes u_i$">|; 

$key = q/displaystylemathcal{Q}cdotmathcal{P}=(tenseuridentite-mathcal{P})cdotmathcal{P}=mathcal{P}-mathcal{P}cdotmathcal{P}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\displaystyle \mathcal{Q} \cdot \mathcal{P} = (\tenseuridentite - \mathcal{P}) \cdot \mathcal{P} = \mathcal{P} - \mathcal{P} \cdot \mathcal{P} = 0$">|; 

$key = q/displaystylemathcal{Q}cdotmathcal{Q}=mathcal{Q}-mathcal{Q}cdotmathcal{P}=mathcal{Q};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.32ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle \mathcal{Q} \cdot \mathcal{Q} = \mathcal{Q} - \mathcal{Q} \cdot \mathcal{P} = \mathcal{Q}$">|; 

$key = q/displaystylenorme{alphacdotc}=abs{alpha}cdotnorme{c}=abs{alpha}cdotdler;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \norme{\alpha \cdot c} = \abs{\alpha} \cdot \norme{c} = \abs{\alpha} \cdot d \le r$">|; 

$key = q/displaystylenorme{e}^2=scalaire{e}{e}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \norme{e}^2 = \scalaire{e}{e} \ge 0$">|; 

$key = q/displaystylenorme{p}^2=norme{y}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle \norme{p}^2 = \norme{y}^2$">|; 

$key = q/displaystylenorme{p}^2=sum_{i,j}conjugue(scalaire{u_i}{z})cdotscalaire{u_j}{z}cdotscalaire{u_i}{u_j}=sum_iabs{scalaire{u_i}{z}}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\displaystyle \norme{p}^2 = \sum_{i,j} \conjugue(\scalaire{u_i}{z}) \cdot \scalaire{u_j}{z} \cdot \scalaire{u_i}{u_j} = \sum_i \abs{\scalaire{u_i}{z}}^2$">|; 

$key = q/displaystylenorme{y}^2=norme{p+e}^2=norme{p}^2+norme{e}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \norme{y}^2 = \norme{p + e}^2 = \norme{p}^2 + \norme{e}^2$">|; 

$key = q/displaystylenorme{z}^2=norme{p}^2+norme{e}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle \norme{z}^2 = \norme{p}^2 + \norme{e}^2$">|; 

$key = q/displaystylep=lambdacdotx=frac{scalaire{x}{y}}{scalaire{x}{x}}cdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle p = \lambda \cdot x = \frac{ \scalaire{x}{y} }{ \scalaire{x}{x} } \cdot x$">|; 

$key = q/displaystylep=mathcal{P}cdotz=contraction{mathcal{P}}{1}{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\displaystyle p = \mathcal{P} \cdot z = \contraction{\mathcal{P} }{1}{z}$">|; 

$key = q/displaystylep=sum_iscalaire{u_i}{z}cdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle p = \sum_i \scalaire{u_i}{z} \cdot u_i$">|; 

$key = q/displaystylep=sum_iu_icdotscalaire{u_i}{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle p = \sum_i u_i \cdot \scalaire{u_i}{z}$">|; 

$key = q/displaystylep_{ij}=sum_{k=1}^mb_{ik}cdotc_{kj};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img179.svg"
 ALT="$\displaystyle p_{ij} = \sum_{k = 1}^m b_{ik} \cdot c_{kj}$">|; 

$key = q/displaystylep_{ij}=sum_{k=1}^mu_{ki}cdotconjaccent{u}_{kj};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img173.svg"
 ALT="$\displaystyle p_{ij} = \sum_{k = 1}^m u_{ki} \cdot \conjaccent{u}_{kj}$">|; 

$key = q/displaystylepartialmathcal{E}(lambda)=-2cdotscalaire{x}{y}+2cdotlambdacdotscalaire{x}{x}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \partial \mathcal{E}(\lambda) = - 2 \cdot \scalaire{x}{y} + 2 \cdot \lambda \cdot \scalaire{x}{x} = 0$">|; 

$key = q/displaystylepincombilin{x}={gammacdotx:gammainsetR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle p \in \combilin{x} = \{ \gamma \cdot x : \gamma \in \setR \}$">|; 

$key = q/displaystylescalaire{c}{x}=-frac{r}{d}cdotscalaire{c}{c}=-dcdotr;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \scalaire{c}{x} = - \frac{r}{d} \cdot \scalaire{c}{c} = - d \cdot r$">|; 

$key = q/displaystylescalaire{c}{x}=frac{r}{d}cdotscalaire{c}{c}=frac{r}{d}cdotd^2=dcdotr;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle \scalaire{c}{x} = \frac{r}{d} \cdot \scalaire{c}{c} = \frac{r}{d} \cdot d^2 = d \cdot r$">|; 

$key = q/displaystylescalaire{e}{x}=conjuguescalaire{x}{e}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \scalaire{e}{x} = \conjugue \scalaire{x}{e} = 0$">|; 

$key = q/displaystylescalaire{p}{e}=lambdacdotscalaire{x}{e}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \scalaire{p}{e} = \lambda \cdot \scalaire{x}{e} = 0$">|; 

$key = q/displaystylescalaire{p}{p}=norme{p}^2lenorme{y}^2=scalaire{y}{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \scalaire{p}{p} = \norme{p}^2 \le \norme{y}^2 = \scalaire{y}{y}$">|; 

$key = q/displaystylescalaire{u_1}{u_1}=frac{scalaire{a_1}{a_1}}{norme{a_1}^2}=frac{scalaire{a_1}{a_1}}{scalaire{a_1}{a_1}}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.94ex; vertical-align: -2.55ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle \scalaire{u_1}{u_1} = \frac{ \scalaire{a_1}{a_1} }{ \norme{a_1}^2 } = \frac{ \scalaire{a_1}{a_1} }{ \scalaire{a_1}{a_1} } = 1$">|; 

$key = q/displaystylescalaire{u_i}{u_j}=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img161.svg"
 ALT="$\displaystyle \scalaire{u_i}{u_j} = \indicatrice_{ij}$">|; 

$key = q/displaystylescalaire{u_i}{u_j}=u_i^dualcdotAcdotu_j=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img203.svg"
 ALT="$\displaystyle \scalaire{u_i}{u_j} = u_i^\dual \cdot A \cdot u_j = \indicatrice_{ij}$">|; 

$key = q/displaystylescalaire{x}{e}=scalaire{x}{y}-lambdacdotscalaire{x}{x}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \scalaire{x}{e} = \scalaire{x}{y} - \lambda \cdot \scalaire{x}{x} = 0$">|; 

$key = q/displaystylescalaire{x}{y}=x^dualcdotAcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img196.svg"
 ALT="$\displaystyle \scalaire{x}{y} = x^\dual \cdot A \cdot y$">|; 

$key = q/displaystylescalaire{y}{x}cdotscalaire{x}{y}lescalaire{x}{x}cdotscalaire{y}{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \scalaire{y}{x} \cdot \scalaire{x}{y} \le \scalaire{x}{x} \cdot \scalaire{y}{y}$">|; 

$key = q/displaystylesum_{i=1}^nu_iotimesu_i=P=UcdotU^{-1}=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img193.svg"
 ALT="$\displaystyle \sum_{i = 1}^n u_i \otimes u_i = P = U \cdot U^{-1} = I$">|; 

$key = q/displaystyletenseuridentite=sum_{i=1}^nu_iotimesu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\displaystyle \tenseuridentite = \sum_{i = 1}^n u_i \otimes u_i$">|; 

$key = q/displaystyletheta=-frac{r}{d}cdotcinargmin_{xinB}scalaire{c}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.52ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \theta = - \frac{r}{d} \cdot c \in \arg\min_{x \in B} \scalaire{c}{x}$">|; 

$key = q/displaystyleu_1=frac{a_1}{norme{a_1}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.92ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle u_1 = \frac{a_1}{ \norme{a_1} }$">|; 

$key = q/displaystyleu_1=frac{a_1}{sqrt{a_1^dualcdotAcdota_1}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.37ex; vertical-align: -2.72ex; " SRC="|."$dir".q|img198.svg"
 ALT="$\displaystyle u_1 = \frac{a_1}{ \sqrt{a_1^\dual \cdot A \cdot a_1} }$">|; 

$key = q/displaystyleu_i^dualcdotu_j=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img165.svg"
 ALT="$\displaystyle u_i^\dual \cdot u_j = \indicatrice_{ij}$">|; 

$key = q/displaystyleu_k^dualcdotAcdotx=sum_{i=1}^n(u_k^dualcdotAcdotu_i)cdotx_i=x_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img221.svg"
 ALT="$\displaystyle u_k^\dual \cdot A \cdot x = \sum_{i = 1}^n (u_k^\dual \cdot A \cdot u_i) \cdot x_i = x_k$">|; 

$key = q/displaystyleu_{k+1}=frac{e_{k+1}}{norme{e_{k+1}}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.92ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img158.svg"
 ALT="$\displaystyle u_{k + 1} = \frac{e_{k + 1} }{ \norme{ e_{k + 1} } }$">|; 

$key = q/displaystyleu_{k+1}=frac{e_{k+1}}{sqrt{e_{k+1}^dualcdotAcdote_{k+1}}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.37ex; vertical-align: -2.72ex; " SRC="|."$dir".q|img202.svg"
 ALT="$\displaystyle u_{k + 1} = \frac{e_{k + 1} }{ \sqrt{e_{k + 1}^\dual \cdot A \cdot e_{k + 1} } }$">|; 

$key = q/displaystylevarphi(x)=scalaire{c}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \varphi(x) = \scalaire{c}{x}$">|; 

$key = q/displaystylex=sum_ix_icdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle x = \sum_i x_i \cdot u_i$">|; 

$key = q/displaystylex=sum_{i=1}^nu_icdot(u_i^dualcdotAcdotx)=sum_{i=1}^nu_icdotu_i^dualcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img222.svg"
 ALT="$\displaystyle x = \sum_{i = 1}^n u_i \cdot (u_i^\dual \cdot A \cdot x) = \sum_{i = 1}^n u_i \cdot u_i^\dual \cdot y$">|; 

$key = q/displaystylex=sum_{i=1}^nx_icdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img220.svg"
 ALT="$\displaystyle x = \sum_{i = 1}^n x_i \cdot u_i$">|; 

$key = q/displaystyley-gammacdotx=y-lambdacdotx-deltacdotx=e-deltacdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle y - \gamma \cdot x = y - \lambda \cdot x - \delta \cdot x = e - \delta \cdot x$">|; 

$key = q/displaystyley=p+e;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle y = p + e$">|; 

$key = q/displaystyley=sum_iscalaire{u_i}{y}cdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\displaystyle y = \sum_i \scalaire{u_i}{y} \cdot u_i$">|; 

$key = q/e;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$e$">|; 

$key = q/e=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img57.svg"
 ALT="$e = 0$">|; 

$key = q/e=z-p=mathcal{Q}cdotz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img131.svg"
 ALT="$e = z - p = \mathcal{Q} \cdot z$">|; 

$key = q/e_2ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img145.svg"
 ALT="$e_2 \ne 0$">|; 

$key = q/e_{k+1}ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img157.svg"
 ALT="$e_{k + 1} \ne 0$">|; 

$key = q/gamma=(gamma_1,...,gamma_n)insetC^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\gamma = (\gamma_1,...,\gamma_n) \in \setC^n$">|; 

$key = q/gamma=(gamma_1,...,gamma_n)insetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\gamma = (\gamma_1,...,\gamma_n) \in \setR^n$">|; 

$key = q/gamma=lambda+delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\gamma = \lambda + \delta$">|; 

$key = q/gamma=lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\gamma = \lambda$">|; 

$key = q/gammainsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\gamma \in \setC$">|; 

$key = q/gammainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\gamma \in \setR$">|; 

$key = q/ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\ge 0$">|; 

$key = q/jin{1,2,...,k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img156.svg"
 ALT="$j \in \{1,2,...,k\}$">|; 

$key = q/k=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img210.svg"
 ALT="$k = n$">|; 

$key = q/k=p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img209.svg"
 ALT="$k = p$">|; 

$key = q/klen-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img150.svg"
 ALT="$k \le n - 1$">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\lambda$">|; 

$key = q/lambda=(lambda_1,...,lambda_n)insetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\lambda = (\lambda_1,...,\lambda_n) \in \setR^n$">|; 

$key = q/lambda_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\lambda_k$">|; 

$key = q/lambdainsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\lambda \in \setC$">|; 

$key = q/lambdainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\lambda \in \setR$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img175.svg"
 ALT="$m$">|; 

$key = q/m=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img191.svg"
 ALT="$m = n$">|; 

$key = q/mathcal{E}(gamma)strictsuperieurscalaire{e}{e};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\mathcal{E}(\gamma) \strictsuperieur \scalaire{e}{e}$">|; 

$key = q/mathcal{E}:setC^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\mathcal{E} : \setC^n \mapsto \setR$">|; 

$key = q/mathcal{E}:setCmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\mathcal{E} : \setC \mapsto \setR$">|; 

$key = q/mathcal{E}:setR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\mathcal{E} : \setR^n \mapsto \setR$">|; 

$key = q/mathcal{E}:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\mathcal{E} : \setR \mapsto \setR$">|; 

$key = q/mathcal{E};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\mathcal{E}$">|; 

$key = q/mathcal{P}=mathcal{P}cdotmathcal{P};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\mathcal{P} = \mathcal{P} \cdot \mathcal{P}$">|; 

$key = q/mathcal{P}_1=u_1otimesu_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\mathcal{P}_1 = u_1 \otimes u_1$">|; 

$key = q/mathcal{P}cdoty=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\mathcal{P} \cdot y = y$">|; 

$key = q/mlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img127.svg"
 ALT="$m \le n$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img89.svg"
 ALT="$n$">|; 

$key = q/norme{e}=sqrt{mathcal{E}(lambda)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.81ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\norme{e} = \sqrt{\mathcal{E}(\lambda)}$">|; 

$key = q/norme{e}=sqrt{scalaire{e}{e}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.81ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\norme{e} = \sqrt{ \scalaire{e}{e} }$">|; 

$key = q/norme{e}^2=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\norme{e}^2 = 0$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$p$">|; 

$key = q/p=lambdacdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img49.svg"
 ALT="$p = \lambda \cdot x$">|; 

$key = q/partial_kmathcal{E}(lambda_1,...,lambda_n)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\partial_k \mathcal{E}(\lambda_1,...,\lambda_n) = 0$">|; 

$key = q/pinU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img80.svg"
 ALT="$p \in U$">|; 

$key = q/plen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img208.svg"
 ALT="$p \le n$">|; 

$key = q/scalaire{p}{e}=scalaire{e}{p}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\scalaire{p}{e} = \scalaire{e}{p} = 0$">|; 

$key = q/scalaire{u_1}{e_2}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img144.svg"
 ALT="$\scalaire{u_1}{e_2} = 0$">|; 

$key = q/scalaire{u_1}{u_2}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img148.svg"
 ALT="$\scalaire{u_1}{u_2} = 0$">|; 

$key = q/scalaire{u_2}{u_2}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\scalaire{u_2}{u_2} = 1$">|; 

$key = q/scalaire{u_i}{u_j}=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\scalaire{u_i}{u_j} = \indicatrice_{ij}$">|; 

$key = q/scalaire{u_i}{u_j}=u_i^dualcdotAcdotu_j=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img199.svg"
 ALT="$\scalaire{u_i}{u_j} = u_i^\dual \cdot A \cdot u_j = \indicatrice_{ij}$">|; 

$key = q/scalaire{u_j}{e_{k+1}}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img155.svg"
 ALT="$\scalaire{u_j}{e_{k + 1} } = 0$">|; 

$key = q/scalaire{u_j}{u_{k+1}}=indicatrice_{j,k+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img159.svg"
 ALT="$\scalaire{u_j }{u_{k + 1} } = \indicatrice_{j, k + 1}$">|; 

$key = q/setC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\setC$">|; 

$key = q/setC^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\setC^n$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\setR$">|; 

$key = q/u_1,u_2,...,u_minmatrice(corps,n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img164.svg"
 ALT="$u_1,u_2,...,u_m \in \matrice(\corps,n,1)$">|; 

$key = q/u_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img141.svg"
 ALT="$u_1$">|; 

$key = q/u_2=e_2slashnorme{e_2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img146.svg"
 ALT="$u_2 = e_2 / \norme{e_2}$">|; 

$key = q/u_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img189.svg"
 ALT="$u_i$">|; 

$key = q/u_i^dualcdotu_j=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img214.svg"
 ALT="$u_i^\dual \cdot u_j = \indicatrice_{ij}$">|; 

$key = q/u_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img176.svg"
 ALT="$u_k$">|; 

$key = q/u_k=(u_{kj})_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img171.svg"
 ALT="$u_k = ( u_{kj} )_j$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\varphi$">|; 

$key = q/x,yinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$x,y \in E$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$x$">|; 

$key = q/x=alphacdotc;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img65.svg"
 ALT="$x = \alpha \cdot c$">|; 

$key = q/x_i=scalaire{u_i}{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$x_i = \scalaire{u_i}{z}$">|; 

$key = q/x_iincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img219.svg"
 ALT="$x_i \in \corps$">|; 

$key = q/xinU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img96.svg"
 ALT="$x \in U$">|; 

$key = q/xne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$x \ne 0$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$y$">|; 

$key = q/y=lambdacdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img58.svg"
 ALT="$y = \lambda \cdot x$">|; 

$key = q/y=mathcal{P}cdotzinU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img122.svg"
 ALT="$y = \mathcal{P} \cdot z \in U$">|; 

$key = q/yinU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img118.svg"
 ALT="$y \in U$">|; 

$key = q/z-g=z-p-Delta=e-Delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img102.svg"
 ALT="$z - g = z - p - \Delta = e - \Delta$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img81.svg"
 ALT="$z$">|; 

$key = q/z=p+e;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img107.svg"
 ALT="$z = p + e$">|; 

$key = q/zinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img78.svg"
 ALT="$z \in E$">|; 

$key = q/zinmatrice(corps,n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img169.svg"
 ALT="$z \in \matrice(\corps,n,1)$">|; 

$key = q/{Eqts}g=sum_igamma_icdotu_ip=sum_ilambda_icdotu_iDelta=sum_idelta_icdotu_i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 17.76ex; vertical-align: -8.33ex; " SRC="|."$dir".q|img101.svg"
 ALT="\begin{Eqts}
g = \sum_i \gamma_i \cdot u_i \\\\
p = \sum_i \lambda_i \cdot u_i \\\\
\Delta = \sum_i \delta_i \cdot u_i
\end{Eqts}">|; 

$key = q/{eqnarraystar}composante_{ik}U&=&u_{ki}composante_{kj}U^dual&=&conjuguecomposante_{jk}U=conjaccent{u}_{kj}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.64ex; " SRC="|."$dir".q|img178.svg"
 ALT="\begin{eqnarray*}
\composante_{ik} U &amp;=&amp; u_{ki} \\\\
\composante_{kj} U^\dual &amp;=&amp; \conjugue \composante_{jk} U = \conjaccent{u}_{kj}
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}e_{k+1}&=&a_{k+1}-sum_{i=1}^ku_icdotscalaire{u_i}{a_{k+1}}&=&a_{k+1}-sum_{i=1}^ku_icdotu_i^dualcdotAcdota_{k+1}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 20.00ex; " SRC="|."$dir".q|img201.svg"
 ALT="\begin{eqnarray*}
e_{k + 1} &amp;=&amp; a_{k + 1} - \sum_{i = 1}^k u_i \cdot \scalaire{u...
... 1} - \sum_{i = 1}^k u_i \cdot u_i^\dual \cdot A \cdot a_{k + 1}
\end{eqnarray*}">|; 

1;

