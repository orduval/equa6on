# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/AsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img7.svg"
 ALT="$A \subseteq \Omega$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\Omega$">|; 

$key = q/age^dualm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.07ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img27.svg"
 ALT="$a \ge^\dual m$">|; 

$key = q/ainA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img11.svg"
 ALT="$a \in A$">|; 

$key = q/alem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img26.svg"
 ALT="$a \le m$">|; 

$key = q/displaystyleAle^dualmle^dualmajor_{le^dual}A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.23ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle A \le^\dual m \le^\dual \major_{\le^\dual} A$">|; 

$key = q/displaystyleinf_{le^dual}A=sup_leA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.16ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \inf_{\le^\dual} A = \sup_\le A$">|; 

$key = q/displaystylem=sup_{le^dual}A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.13ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle m = \sup_{\le^\dual} A$">|; 

$key = q/displaystylemajor_{le^dual}A=minor_leA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.13ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \major_{\le^\dual} A = \minor_\le A$">|; 

$key = q/displaystylemax_{le^dual}A=min_leA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.68ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \max_{\le^\dual} A = \min_\le A$">|; 

$key = q/displaystylemaxim_{le^dual}A=accolades{max_{le^dual}A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \maxim_{\le^\dual} A = \accolades{\max_{\le^\dual} A}$">|; 

$key = q/displaystylemaxim_{le^dual}A=minim_leA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.68ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \maxim_{\le^\dual} A = \minim_\le A$">|; 

$key = q/displaystylemaxim_{le^dual}A=minim_leA=accolades{min_leA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \maxim_{\le^\dual} A = \minim_\le A = \accolades{\min_\le A}$">|; 

$key = q/displaystylemge^dualA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.18ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle m \ge^\dual A$">|; 

$key = q/displaystylemin_{le^dual}A=max_leA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.68ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \min_{\le^\dual} A = \max_\le A$">|; 

$key = q/displaystyleminim_{le^dual}A=maxim_leA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.68ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \minim_{\le^\dual} A = \maxim_\le A$">|; 

$key = q/displaystyleminmajor_{le^dual}A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.13ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle m \in \major_{\le^\dual} A$">|; 

$key = q/displaystyleminmaxim_{le^dual}A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.68ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle m \in \maxim_{\le^\dual} A$">|; 

$key = q/displaystyleminminim_leA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.68ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle m \in \minim_\le A$">|; 

$key = q/displaystyleminminor_leA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.68ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle m \in \minor_\le A$">|; 

$key = q/displaystyleminor_leA=major_{le^dual}AlemleA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.13ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \minor_\le A = \major_{\le^\dual} A \le m \le A$">|; 

$key = q/displaystyleminor_{le^dual}A=major_leA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.13ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \minor_{\le^\dual} A = \major_\le A$">|; 

$key = q/displaystylemle^dualA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.18ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle m \le^\dual A$">|; 

$key = q/displaystylesup_{le^dual}A=inf_leA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.16ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \sup_{\le^\dual} A = \inf_\le A$">|; 

$key = q/displaystylexle^dualy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.32ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle x \le^\dual y$">|; 

$key = q/displaystyleylex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle y \le x$">|; 

$key = q/le;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\le$">|; 

$key = q/le^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.07ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\le^\dual$">|; 

$key = q/m=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$m = a$">|; 

$key = q/mgeA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img14.svg"
 ALT="$m \ge A$">|; 

$key = q/mge^dualA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img19.svg"
 ALT="$m \ge^\dual A$">|; 

$key = q/mge^duala;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.07ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img12.svg"
 ALT="$m \ge^\dual a$">|; 

$key = q/mgea;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img15.svg"
 ALT="$m \ge a$">|; 

$key = q/minOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img8.svg"
 ALT="$m \in \Omega$">|; 

$key = q/mleA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img9.svg"
 ALT="$m \le A$">|; 

$key = q/mle^dualA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img21.svg"
 ALT="$m \le^\dual A$">|; 

$key = q/mle^duala;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.07ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img16.svg"
 ALT="$m \le^\dual a$">|; 

$key = q/mlea;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img10.svg"
 ALT="$m \le a$">|; 

$key = q/x,yinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$x,y \in \Omega$">|; 

1;

