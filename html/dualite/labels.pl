# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate labels original text with physical files.


$key = q/chap:dualite/;
$external_labels{$key} = "$URL/" . q|dualite.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2023 (Released January 1, 2023)
# labels from external_latex_labels array.


$key = q/chap:dualite/;
$external_latex_labels{$key} = q|1 Dualité|; 
$noresave{$key} = "$nosave";

1;

