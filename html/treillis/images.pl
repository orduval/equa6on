# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(mathcal{C},subseteq);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img144.svg"
 ALT="$(\mathcal{C},\subseteq)$">|; 

$key = q/(mathcal{T},le);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$(\mathcal{T},\le)$">|; 

$key = q/(mathcal{T},le)^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$(\mathcal{T},\le)^\dual$">|; 

$key = q/(mathcal{T},le^dual);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img118.svg"
 ALT="$(\mathcal{T},\le^\dual)$">|; 

$key = q/0lex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img28.svg"
 ALT="$0 \le x$">|; 

$key = q/1gex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img39.svg"
 ALT="$1 \ge x$">|; 

$key = q/A,Binmathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img143.svg"
 ALT="$A,B \in \mathcal{C}$">|; 

$key = q/A,BsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img2.svg"
 ALT="$A,B \subseteq \Omega$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img92.svg"
 ALT="$A$">|; 

$key = q/Asubseteqmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img91.svg"
 ALT="$A \subseteq \mathcal{T}$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\Omega$">|; 

$key = q/Z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img75.svg"
 ALT="$Z$">|; 

$key = q/a,b,cinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img61.svg"
 ALT="$a,b,c \in \mathcal{T}$">|; 

$key = q/a,binmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$a,b \in \mathcal{T}$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img130.svg"
 ALT="$a$">|; 

$key = q/a_1,a_2,...,a_ninmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img117.svg"
 ALT="$a_1, a_2, ..., a_n \in \mathcal{T}$">|; 

$key = q/ainmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img25.svg"
 ALT="$a \in \mathcal{T}$">|; 

$key = q/displaystyle(mathcal{T},le)^dual=(mathcal{T},le^dual);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle (\mathcal{T},\le)^\dual = (\mathcal{T},\le^\dual)$">|; 

$key = q/displaystyle0=min_lemathcal{T}=max_{le^dual}mathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.68ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle 0 = \min_\le \mathcal{T} = \max_{\le^\dual} \mathcal{T}$">|; 

$key = q/displaystyle0=minmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle 0 = \min \mathcal{T}$">|; 

$key = q/displaystyle0^dual=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle 0^\dual = 1$">|; 

$key = q/displaystyle0sqcapa=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle 0 \sqcap a = 0$">|; 

$key = q/displaystyle0sqcupa=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle 0 \sqcup a = a$">|; 

$key = q/displaystyle1=max_lemathcal{T}=min_{le^dual}mathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.68ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle 1 = \max_\le \mathcal{T} = \min_{\le^\dual} \mathcal{T}$">|; 

$key = q/displaystyle1=maxmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle 1 = \max \mathcal{T}$">|; 

$key = q/displaystyle1^dual=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle 1^\dual = 0$">|; 

$key = q/displaystyle1sqcapa=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle 1 \sqcap a = a$">|; 

$key = q/displaystyle1sqcupa=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle 1 \sqcup a = 1$">|; 

$key = q/displaystyleA={a_1,a_2,...,a_n}subseteqmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle A = \{ a_1, a_2, ..., a_n \} \subseteq \mathcal{T}$">|; 

$key = q/displaystyleAcapBinmathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle A \cap B \in \mathcal{C}$">|; 

$key = q/displaystyleAcupBinmathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\displaystyle A \cup B \in \mathcal{C}$">|; 

$key = q/displaystyleOmega=max_{subseteq}mathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.68ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\displaystyle \Omega = \max_{\subseteq} \mathcal{C}$">|; 

$key = q/displaystyleZ=Asetminus{x}={...,a_{i-1},a_{i+1},...};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle Z = A \setminus \{ x \} = \{ ..., a_{i - 1}, a_{i + 1}, ... \}$">|; 

$key = q/displaystylea=asqcap(asqcupb);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\displaystyle a = a \sqcap (a \sqcup b)$">|; 

$key = q/displaystylea=asqcap^dual(asqcup^dualb);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle a = a \sqcap^\dual (a \sqcup^\dual b)$">|; 

$key = q/displaystylea=asqcup(asqcapb);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle a = a \sqcup (a \sqcap b)$">|; 

$key = q/displaystylea=inf{a,sigma};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\displaystyle a = \inf \{a,\sigma\}$">|; 

$key = q/displaystylea=inf{a,sup{a,b}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle a = \inf \{a, \sup \{a, b\} \}$">|; 

$key = q/displaystylea=inf{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle a = \inf\{a\}$">|; 

$key = q/displaystylea=sup{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle a = \sup\{a\}$">|; 

$key = q/displaystylea_1sqcapa_2sqcap...sqcapa_n=inf{a_1,a_2,...,a_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\displaystyle a_1 \sqcap a_2 \sqcap ... \sqcap a_n = \inf \{ a_1, a_2, ..., a_n \}$">|; 

$key = q/displaystylea_1sqcupa_2sqcup...sqcupa_n=sup{a_1,a_2,...,a_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\displaystyle a_1 \sqcup a_2 \sqcup ... \sqcup a_n = \sup \{ a_1, a_2, ..., a_n \}$">|; 

$key = q/displaystyleale{a,sigma};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\displaystyle a \le \{a,\sigma\}$">|; 

$key = q/displaystyleasqcap(bsqcapc)=(asqcapb)sqcapc;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle a \sqcap (b \sqcap c) = (a \sqcap b) \sqcap c$">|; 

$key = q/displaystyleasqcap(bsqcupc)=(asqcapb)sqcup(asqcapc);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle a \sqcap (b \sqcup c) = (a \sqcap b) \sqcup (a \sqcap c)$">|; 

$key = q/displaystyleasqcap^dualb=inf_{le^dual}{a,b}=sup_le{a,b}=asqcupb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.29ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle a \sqcap^\dual b = \inf_{\le^\dual} \{a,b\} = \sup_\le \{a,b\} = a \sqcup b$">|; 

$key = q/displaystyleasqcapa=inf{a,a}=inf{a}=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle a \sqcap a = \inf\{a,a\} = \inf\{a\} = a$">|; 

$key = q/displaystyleasqcapb=bsqcapa;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle a \sqcap b = b \sqcap a$">|; 

$key = q/displaystyleasqcapb=inf{a,b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle a \sqcap b = \inf \{ a, b \}$">|; 

$key = q/displaystyleasqcapbsqcapc=asqcap(bsqcapc)=(asqcapb)sqcapc;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\displaystyle a \sqcap b \sqcap c = a \sqcap (b \sqcap c) = (a \sqcap b) \sqcap c$">|; 

$key = q/displaystyleasqcup(bsqcapc)=(asqcupb)sqcap(asqcupc);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle a \sqcup (b \sqcap c) = (a \sqcup b) \sqcap (a \sqcup c)$">|; 

$key = q/displaystyleasqcup(bsqcupc)=(asqcupb)sqcupc;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle a \sqcup (b \sqcup c) = (a \sqcup b) \sqcup c$">|; 

$key = q/displaystyleasqcup^dual(bsqcup^dualc)=(asqcup^dualb)sqcup^dualc;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\displaystyle a \sqcup^\dual (b \sqcup^\dual c) = (a \sqcup^\dual b) \sqcup^\dual c$">|; 

$key = q/displaystyleasqcup^dualb=sup_{le^dual}{a,b}=inf_le{a,b}=asqcapb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.29ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle a \sqcup^\dual b = \sup_{\le^\dual} \{a,b\} = \inf_\le \{a,b\} = a \sqcap b$">|; 

$key = q/displaystyleasqcupa=sup{a,a}=sup{a}=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle a \sqcup a = \sup\{a,a\} = \sup\{a\} = a$">|; 

$key = q/displaystyleasqcupb=bsqcupa;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\displaystyle a \sqcup b = b \sqcup a$">|; 

$key = q/displaystyleasqcupb=sup{a,b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle a \sqcup b = \sup \{ a, b \}$">|; 

$key = q/displaystyleasqcupbsqcupc=asqcup(bsqcupc)=(asqcupb)sqcupc;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle a \sqcup b \sqcup c = a \sqcup (b \sqcup c) = (a \sqcup b) \sqcup c$">|; 

$key = q/displaystyleemptyset,Omegainmathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle \emptyset,\Omega \in \mathcal{C}$">|; 

$key = q/displaystyleemptyset=min_{subseteq}mathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.84ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img145.svg"
 ALT="$\displaystyle \emptyset = \min_{\subseteq} \mathcal{C}$">|; 

$key = q/displaystylegamma=infZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle \gamma = \inf Z$">|; 

$key = q/displaystyleinfA=infBig{infbig(Asetminus{x}big),xBig};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle \inf A = \inf \Big\{ \inf\big(A \setminus \{ x \}\big) , x \Big\}$">|; 

$key = q/displaystyleinf_{le^dual}{a,b}=sup_le{a,b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.29ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \inf_{\le^\dual} \{a,b\} = \sup_\le \{a,b\}$">|; 

$key = q/displaystyleinf{0,a}=max{0}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \inf \{ 0, a \} = \max \{ 0 \} = 0$">|; 

$key = q/displaystyleinf{1,a}=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \inf \{ 1, a \} = a$">|; 

$key = q/displaystylelambda=inf{gamma,x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle \lambda = \inf \{ \gamma, x \}$">|; 

$key = q/displaystylelambda=maxminorA=infA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle \lambda = \max \minor A = \inf A$">|; 

$key = q/displaystylelambdainminorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle \lambda \in \minor A$">|; 

$key = q/displaystylelambdaleZcup{x}=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle \lambda \le Z \cup \{ x \} = A$">|; 

$key = q/displaystylelambdalea;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\displaystyle \lambda \le a$">|; 

$key = q/displaystylelambdalegammaleZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle \lambda \le \gamma \le Z$">|; 

$key = q/displaystylelambdale{a,sigma};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\displaystyle \lambda \le \{a,\sigma\}$">|; 

$key = q/displaystylelambdale{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle \lambda \le \{ x \}$">|; 

$key = q/displaystylemajor{1,a}={1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle \major \{ 1, a \} = \{ 1 \}$">|; 

$key = q/displaystylemathcal{C}subseteqsousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle \mathcal{C} \subseteq \sousens(\Omega)$">|; 

$key = q/displaystyleminor{0,a}={0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \minor \{ 0, a \} = \{ 0 \}$">|; 

$key = q/displaystyleminor{a,b}leinf{a,b}le{a,b}lesup{a,b}lemajor{a,b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \minor \{ a, b \} \le \inf \{ a, b \} \le \{ a, b \} \le \sup \{ a, b \} \le \major \{ a, b \}$">|; 

$key = q/displaystyleminor{a,sigma}leale{a,sigma};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\displaystyle \minor \{a,\sigma\} \le a \le \{a,\sigma\}$">|; 

$key = q/displaystyleminor{a}leale{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle \minor\{a\} \le a \le \{a\}$">|; 

$key = q/displaystylemu=supZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle \mu = \sup Z$">|; 

$key = q/displaystylesigma=minmajorA=supA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle \sigma = \min \major A = \sup A$">|; 

$key = q/displaystylesigma=sup{a,b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle \sigma = \sup\{a,b\}$">|; 

$key = q/displaystylesigma=sup{mu,x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle \sigma = \sup \{ \mu, x \}$">|; 

$key = q/displaystylesigmageZcup{x}=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle \sigma \ge Z \cup \{ x \} = A$">|; 

$key = q/displaystylesigmagemugeZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle \sigma \ge \mu \ge Z$">|; 

$key = q/displaystylesigmage{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle \sigma \ge \{ x \}$">|; 

$key = q/displaystylesigmainmajorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle \sigma \in \major A$">|; 

$key = q/displaystylesupA=supBig{supbig(Asetminus{x}big),xBig};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle \sup A = \sup \Big\{ \sup\big(A \setminus \{ x \}\big) , x \Big\}$">|; 

$key = q/displaystylesup_{le^dual}{a,b}=inf_le{a,b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.29ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \sup_{\le^\dual} \{a,b\} = \inf_\le \{a,b\}$">|; 

$key = q/displaystylesupbig{a,sup{b,c}big}=sup{a,b,c}=supbig{sup{a,b},cbig};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle \sup \big\{ a, \sup \{b, c\} \big\} = \sup \{a,b,c\} = \sup \big\{ \sup \{a, b\}, c \big\}$">|; 

$key = q/displaystylesup{0,a}=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \sup \{ 0, a \} = a$">|; 

$key = q/displaystylesup{1,a}=min{1}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \sup \{ 1, a \} = \min \{ 1 \} = 1$">|; 

$key = q/displaystylevarkappageZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle \varkappa \ge Z$">|; 

$key = q/displaystylevarkappagemu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle \varkappa \ge \mu$">|; 

$key = q/displaystylevarkappagesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle \varkappa \ge \sigma$">|; 

$key = q/displaystylevarkappagex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle \varkappa \ge x$">|; 

$key = q/displaystylevarkappage{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle \varkappa \ge \{ x \}$">|; 

$key = q/displaystylevarkappainmajorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle \varkappa \in \major A$">|; 

$key = q/displaystylevarkappainmajorZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle \varkappa \in \major Z$">|; 

$key = q/displaystylevarkappainmajor{mu,x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\displaystyle \varkappa \in \major \{ \mu, x \}$">|; 

$key = q/displaystylevarthetainminorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\displaystyle \vartheta \in \minor A$">|; 

$key = q/displaystylevarthetainminorZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle \vartheta \in \minor Z$">|; 

$key = q/displaystylevarthetainminor{gamma,x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle \vartheta \in \minor \{ \gamma, x \}$">|; 

$key = q/displaystylevarthetaleZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle \vartheta \le Z$">|; 

$key = q/displaystylevarthetalegamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle \vartheta \le \gamma$">|; 

$key = q/displaystylevarthetalelambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\displaystyle \vartheta \le \lambda$">|; 

$key = q/displaystylevarthetalex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle \vartheta \le x$">|; 

$key = q/displaystylevarthetale{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle \vartheta \le \{ x \}$">|; 

$key = q/displaystylex=a_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle x = a_i$">|; 

$key = q/displaystylexsqcap^dualx^ddagger=0^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle x \sqcap^\dual x^\ddagger = 0^\dual$">|; 

$key = q/displaystylexsqcapx^ddagger=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle x \sqcap x^\ddagger = 0$">|; 

$key = q/displaystylexsqcup^dualx^ddagger=1^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle x \sqcup^\dual x^\ddagger = 1^\dual$">|; 

$key = q/displaystylexsqcupx^ddagger=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle x \sqcup x^\ddagger = 1$">|; 

$key = q/displaystyle{0,a}lealemajor{0,a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \{ 0, a \} \le a \le \major \{ 0, a \}$">|; 

$key = q/displaystyle{1,a}geageminor{1,a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \{ 1, a \} \ge a \ge \minor \{ 1, a \}$">|; 

$key = q/displaystyle{a,b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \{ a,b \}$">|; 

$key = q/displaystyle{a}lealemajor{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \{a\} \le a \le \major\{a\}$">|; 

$key = q/iin{1,2,...,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img72.svg"
 ALT="$i \in \{ 1, 2, ..., n \}$">|; 

$key = q/lambdainmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\lambda \in \mathcal{T}$">|; 

$key = q/le;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\le$">|; 

$key = q/le^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.07ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\le^\dual$">|; 

$key = q/mathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\mathcal{C}$">|; 

$key = q/mathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\mathcal{T}$">|; 

$key = q/n-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img68.svg"
 ALT="$n - 1$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img69.svg"
 ALT="$n$">|; 

$key = q/nge2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img70.svg"
 ALT="$n \ge 2$">|; 

$key = q/sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\sigma$">|; 

$key = q/sigmagea;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\sigma \ge a$">|; 

$key = q/sqcap;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.48ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\sqcap$">|; 

$key = q/sqcap^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\sqcap^\dual$">|; 

$key = q/sqcup;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.48ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\sqcup$">|; 

$key = q/sqcup^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\sqcup^\dual$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img58.svg"
 ALT="$x$">|; 

$key = q/x=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$x = 0$">|; 

$key = q/x=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$x = 1$">|; 

$key = q/x^ddagger;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img57.svg"
 ALT="$x^\ddagger$">|; 

$key = q/x^ddaggerinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.20ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img51.svg"
 ALT="$x^\ddagger \in \mathcal{T}$">|; 

$key = q/xge1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img38.svg"
 ALT="$x \ge 1$">|; 

$key = q/xinmajor{1,a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$x \in \major \{ 1, a \}$">|; 

$key = q/xinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img52.svg"
 ALT="$x \in \mathcal{T}$">|; 

$key = q/xinminor{0,a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$x \in \minor \{ 0, a \}$">|; 

$key = q/xle0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img27.svg"
 ALT="$x \le 0$">|; 

$key = q/{A,B};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\{A,B\}$">|; 

$key = q/{Eqts}sup_subseteq{A,B}=AcupBinf_subseteq{A,B}=AcapB{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.52ex; vertical-align: -5.70ex; " SRC="|."$dir".q|img4.svg"
 ALT="\begin{Eqts}
\sup_\subseteq \{ A,B \} = A \cup B \ \\\\
\inf_\subseteq \{ A,B \} = A \cap B
\end{Eqts}">|; 

$key = q/{a,b}={b,a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\{a,b\} = \{b,a\}$">|; 

$key = q/{a,sigma};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\{a,\sigma\}$">|; 

1;

