# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$A$">|; 

$key = q/Gamma(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\Gamma(\alpha)$">|; 

$key = q/Gamma(alpha)subseteqGamma(lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\Gamma(\alpha) \subseteq \Gamma(\lambda)$">|; 

$key = q/IleS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img130.svg"
 ALT="$I \le S$">|; 

$key = q/L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img38.svg"
 ALT="$L$">|; 

$key = q/Lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\Lambda$">|; 

$key = q/LsetminusZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$L \setminus Z$">|; 

$key = q/LsetminusZsubseteqC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$L \setminus Z \subseteq C$">|; 

$key = q/LsubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img29.svg"
 ALT="$L \subseteq A$">|; 

$key = q/Psi(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\Psi(\alpha)$">|; 

$key = q/Psi(alpha)subseteqPsi(sigma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\Psi(\alpha) \subseteq \Psi(\sigma)$">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$S$">|; 

$key = q/SsetminusZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$S \setminus Z$">|; 

$key = q/SsetminusZsubseteqC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$S \setminus Z \subseteq C$">|; 

$key = q/SstrictinferieurI;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img114.svg"
 ALT="$S \strictinferieur I$">|; 

$key = q/SsubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img63.svg"
 ALT="$S \subseteq A$">|; 

$key = q/Theta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\Theta$">|; 

$key = q/ThetacapLambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\Theta \cap \Lambda$">|; 

$key = q/ThetacapLambdaneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\Theta \cap \Lambda \ne \emptyset$">|; 

$key = q/Xi(alpha)=AsetminusUpsilon(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\Xi(\alpha) = A \setminus \Upsilon(\alpha)$">|; 

$key = q/Xi(beta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\Xi(\beta)$">|; 

$key = q/XsubseteqY;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img57.svg"
 ALT="$X \subseteq Y$">|; 

$key = q/ZsubseteqL;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img39.svg"
 ALT="$Z \subseteq L$">|; 

$key = q/ZsubseteqS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img72.svg"
 ALT="$Z \subseteq S$">|; 

$key = q/abs{I-beta}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\abs{I - \beta} \le \epsilon$">|; 

$key = q/abs{alpha-S}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\abs{\alpha - S} \le \epsilon$">|; 

$key = q/alpha,betainThetacapLambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\alpha,\beta \in \Theta \cap \Lambda$">|; 

$key = q/alphagesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\alpha \ge \sigma$">|; 

$key = q/alphainLambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\alpha \in \Lambda$">|; 

$key = q/alphainTheta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\alpha \in \Theta$">|; 

$key = q/alphainThetacapLambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\alpha \in \Theta \cap \Lambda$">|; 

$key = q/alphainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\alpha \in \setR$">|; 

$key = q/alphaleS+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\alpha \le S + \epsilon$">|; 

$key = q/alphalelambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\alpha \le \lambda$">|; 

$key = q/alphanebeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\alpha \ne \beta$">|; 

$key = q/alphastrictinferieurbeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\alpha \strictinferieur \beta$">|; 

$key = q/beta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\beta$">|; 

$key = q/betageI-epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\beta \ge I - \epsilon$">|; 

$key = q/betainLambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\beta \in \Lambda$">|; 

$key = q/betainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\beta \in \setR$">|; 

$key = q/betanealpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\beta \ne \alpha$">|; 

$key = q/betastrictinferieurlambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\beta \strictinferieur \lambda$">|; 

$key = q/betastrictsuperieursigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\beta \strictsuperieur \sigma$">|; 

$key = q/delta=I-Sstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\delta = I - S \strictsuperieur 0$">|; 

$key = q/displaystyleA=Xi(alpha)cupUpsilon(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle A = \Xi(\alpha) \cup \Upsilon(\alpha)$">|; 

$key = q/displaystyleC={xinA:f(x)gelambda};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle C = \{ x \in A : f(x) \ge \lambda \}$">|; 

$key = q/displaystyleC={xinA:f(x)lesigma};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle C = \{ x \in A : f(x) \le \sigma \}$">|; 

$key = q/displaystyleCsubseteq{xinA:f(x)strictinferieurbeta}=Gamma(beta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle C \subseteq \{ x \in A : f(x) \strictinferieur \beta \} = \Gamma(\beta)$">|; 

$key = q/displaystyleCsubseteq{xinA:f(x)strictsuperieurbeta}=Psi(beta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle C \subseteq \{ x \in A : f(x) \strictsuperieur \beta \} = \Psi(\beta)$">|; 

$key = q/displaystyleGamma(lambda)={xinA:f(x)strictinferieurlambda};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \Gamma(\lambda) = \{ x \in A : f(x) \strictinferieur \lambda \}$">|; 

$key = q/displaystyleLambda={lambdainsetR:fesssuperieurlambda}={lambdainsetR:mu(Gamma(lambda))=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \Lambda = \{ \lambda \in \setR : f \esssuperieur \lambda \} = \{ \lambda \in \setR : \mu(\Gamma(\lambda)) = 0 \}$">|; 

$key = q/displaystyleLsetminusZ={xinL:f(x)gelambda};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle L \setminus Z = \{ x \in L : f(x) \ge \lambda \}$">|; 

$key = q/displaystylePsi(sigma)={xinA:f(x)strictsuperieursigma};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \Psi(\sigma) = \{ x \in A : f(x) \strictsuperieur \sigma \}$">|; 

$key = q/displaystyleSsetminusZ={xinS:f(x)lesigma};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle S \setminus Z = \{ x \in S : f(x) \le \sigma \}$">|; 

$key = q/displaystyleTheta={sigmainsetR:fessinferieursigma}={sigmainsetR:mu(Psi(sigma))=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \Theta = \{ \sigma \in \setR : f \essinferieur \sigma \} = \{ \sigma \in \setR : \mu(\Psi(\sigma)) = 0 \}$">|; 

$key = q/displaystyleThetacapLambda=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.06ex; vertical-align: -0.23ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle \Theta \cap \Lambda = \emptyset$">|; 

$key = q/displaystyleThetacapLambda={alpha};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\displaystyle \Theta \cap \Lambda = \{ \alpha \}$">|; 

$key = q/displaystyleThetasubseteq[lambda,+infty[;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle \Theta \subseteq [\lambda, +\infty[$">|; 

$key = q/displaystyleThetasubseteq]-infty,sigma];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle \Theta \subseteq  ]-\infty, \sigma]$">|; 

$key = q/displaystyleUpsilon(alpha)=Psi(alpha)cupGamma(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle \Upsilon(\alpha) = \Psi(\alpha) \cup \Gamma(\alpha)$">|; 

$key = q/displaystyleUpsilon(alpha)={xinA:f(x)nealpha};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle \Upsilon(\alpha) = \{ x \in A : f(x) \ne \alpha \}$">|; 

$key = q/displaystyleXi(alpha)={xinA:f(x)=alpha};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle \Xi(\alpha) = \{ x \in A : f(x) = \alpha \}$">|; 

$key = q/displaystyleXi(beta)={xinA:f(x)=beta}subseteq{xinA:f(x)nealpha};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle \Xi(\beta) = \{ x \in A : f(x) = \beta \} \subseteq \{ x \in A : f(x) \ne \alpha \}$">|; 

$key = q/displaystyle[alpha,+infty[subseteqTheta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\displaystyle [\alpha, +\infty[  \subseteq \Theta$">|; 

$key = q/displaystyle[alpha,beta]=]-infty,beta]cap[alpha,+infty[subseteqThetacapLambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\displaystyle [\alpha,\beta] =  ]-\infty, \beta] \cap [\alpha, +\infty[  \subseteq \Theta \cap \Lambda$">|; 

$key = q/displaystyle[sigma,+infty[subseteqTheta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle [\sigma, +\infty[  \subseteq \Theta$">|; 

$key = q/displaystyle[sigma,+infty[subseteqThetasubseteq[lambda,+infty[;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle [\sigma, +\infty[  \subseteq \Theta \subseteq [\lambda, +\infty[$">|; 

$key = q/displaystyle]-infty,beta]subseteqLambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle ]-\infty, \beta] \subseteq \Lambda$">|; 

$key = q/displaystyle]-infty,lambda]subseteqLambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle ]-\infty, \lambda] \subseteq \Lambda$">|; 

$key = q/displaystyle]-infty,lambda]subseteqThetasubseteq]-infty,sigma];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle ]-\infty, \lambda]  \subseteq \Theta \subseteq  ]-\infty, \sigma]$">|; 

$key = q/displaystylealphaleS+epsilonstrictinferieurI-epsilonlebeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\displaystyle \alpha \le S + \epsilon \strictinferieur I - \epsilon \le \beta$">|; 

$key = q/displaystylef+gessinferieursigma+tau;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle f + g \essinferieur \sigma + \tau$">|; 

$key = q/displaystylef+gesssuperieurlambda+gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle f + g \esssuperieur \lambda + \gamma$">|; 

$key = q/displaystylefessinferieurgessinferieursupessentiel_{xinA}g(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle f \essinferieur g \essinferieur \supessentiel_{x \in A} g(x)$">|; 

$key = q/displaystylefessinferieursigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle f \essinferieur \sigma$">|; 

$key = q/displaystylefesssuperieurlambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle f \esssuperieur \lambda$">|; 

$key = q/displaystyleinfessentiel_{xinA}[f(x)+g(x)]geinfessentiel_{xinA}f(x)+infessentiel_{xinA}g(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle \infessentiel_{x \in A} [f(x) + g(x)] \ge \infessentiel_{x \in A} f(x) + \infessentiel_{x \in A} g(x)$">|; 

$key = q/displaystyleinfessentiel_{xinA}f(x)=supLambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \infessentiel_{x \in A} f(x) = \sup \Lambda$">|; 

$key = q/displaystyleinfessentiel_{xinA}f(x)essinferieurfessinferieurg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle \infessentiel_{x \in A} f(x) \essinferieur f \essinferieur g$">|; 

$key = q/displaystyleinfessentiel_{xinA}f(x)leinfessentiel_{xinA}g(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle \infessentiel_{x \in A} f(x) \le \infessentiel_{x \in A} g(x)$">|; 

$key = q/displaystyleinfessentiel_{xinA}f(x)lesupessentiel_{xinA}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle \infessentiel_{x \in A} f(x) \le \supessentiel_{x \in A} f(x)$">|; 

$key = q/displaystylelambdaleinfessentiel_{xinA}f(x)lesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \lambda \le \infessentiel_{x \in A} f(x) \le \sigma$">|; 

$key = q/displaystylelambdalesupessentiel_{xinA}f(x)lesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \lambda \le \supessentiel_{x \in A} f(x) \le \sigma$">|; 

$key = q/displaystylemu(Upsilon(alpha))=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle \mu(\Upsilon(\alpha)) = 0$">|; 

$key = q/displaystylemu({xinA:f(x)strictinferieurlambda})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \mu(\{ x \in A : f(x) \strictinferieur \lambda \}) = 0$">|; 

$key = q/displaystylemu({xinA:f(x)strictsuperieursigma})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \mu(\{ x \in A : f(x) \strictsuperieur \sigma \}) = 0$">|; 

$key = q/displaystylesupessentiel_{xinA}[f(x)+g(x)]lesupessentiel_{xinA}f(x)+supessentiel_{xinA}g(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle \supessentiel_{x \in A} [f(x) + g(x)] \le \supessentiel_{x \in A} f(x) + \supessentiel_{x \in A} g(x)$">|; 

$key = q/displaystylesupessentiel_{xinA}f(x)=infTheta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \supessentiel_{x \in A} f(x) = \inf \Theta$">|; 

$key = q/displaystylesupessentiel_{xinA}f(x)lesupessentiel_{xinA}g(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\displaystyle \supessentiel_{x \in A} f(x) \le \supessentiel_{x \in A} g(x)$">|; 

$key = q/epsilon=deltaslash4;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\epsilon = \delta / 4$">|; 

$key = q/f(x)=beta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img104.svg"
 ALT="$f(x) = \beta$">|; 

$key = q/f(x)gelambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$f(x) \ge \lambda$">|; 

$key = q/f(x)lesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$f(x) \le \sigma$">|; 

$key = q/f(x)nealpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img105.svg"
 ALT="$f(x) \ne \alpha$">|; 

$key = q/f(x)strictinferieurbeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img80.svg"
 ALT="$f(x) \strictinferieur \beta$">|; 

$key = q/f(x)strictsuperieurbeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$f(x) \strictsuperieur \beta$">|; 

$key = q/f,g:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img132.svg"
 ALT="$f,g : A \mapsto \setR$">|; 

$key = q/f:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f : A \mapsto \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img2.svg"
 ALT="$f$">|; 

$key = q/fessinferieurg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img133.svg"
 ALT="$f \essinferieur g$">|; 

$key = q/fessinferieursigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img17.svg"
 ALT="$f \essinferieur \sigma$">|; 

$key = q/gammainLambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\gamma \in \Lambda$">|; 

$key = q/gammalesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\gamma \le \sigma$">|; 

$key = q/infXgeinfY;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\inf X \ge \inf Y$">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\lambda$">|; 

$key = q/lambda=inf[lambda,+infty[;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\lambda = \inf [\lambda, +\infty[$">|; 

$key = q/lambda=sup]-infty,lambda];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\lambda = \sup  ]-\infty, \lambda]$">|; 

$key = q/lambdainLambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\lambda \in \Lambda$">|; 

$key = q/lambdainLambdaneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\lambda \in \Lambda \ne \emptyset$">|; 

$key = q/lambdainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\lambda \in \setR$">|; 

$key = q/lambdaleTheta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\lambda \le \Theta$">|; 

$key = q/lambdale{S,I}lesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\lambda \le \{ S,I \} \le \sigma$">|; 

$key = q/mu(C)gemu(LsetminusZ)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\mu(C) \ge \mu(L \setminus Z) \strictsuperieur 0$">|; 

$key = q/mu(C)gemu(SsetminusZ)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\mu(C) \ge \mu(S \setminus Z) \strictsuperieur 0$">|; 

$key = q/mu(Gamma(beta))gemu(C)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\mu(\Gamma(\beta)) \ge \mu(C) \strictsuperieur 0$">|; 

$key = q/mu(Gamma(lambda))=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\mu(\Gamma(\lambda)) = 0$">|; 

$key = q/mu(LsetminusZ)=mu(L)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\mu(L \setminus Z) = \mu(L) \strictsuperieur 0$">|; 

$key = q/mu(Psi(alpha))=mu(Gamma(alpha))=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\mu(\Psi(\alpha)) = \mu(\Gamma(\alpha)) = 0$">|; 

$key = q/mu(Psi(beta))gemu(C)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\mu(\Psi(\beta)) \ge \mu(C) \strictsuperieur 0$">|; 

$key = q/mu(Psi(sigma))=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\mu(\Psi(\sigma)) = 0$">|; 

$key = q/mu(SsetminusZ)=mu(S)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\mu(S \setminus Z) = \mu(S) \strictsuperieur 0$">|; 

$key = q/mu(Xi(alpha))=mu(A)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\mu(\Xi(\alpha)) = \mu(A) \strictsuperieur 0$">|; 

$key = q/mu(Xi(alpha))strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\mu(\Xi(\alpha)) \strictsuperieur 0$">|; 

$key = q/mu(Xi(beta))=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\mu(\Xi(\beta)) = 0$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\mu$">|; 

$key = q/sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\sigma$">|; 

$key = q/sigma=inf[sigma,+infty[;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\sigma = \inf [\sigma, +\infty[$">|; 

$key = q/sigma=sup]-infty,sigma];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\sigma = \sup  ]-\infty, \sigma]$">|; 

$key = q/sigmageLambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\sigma \ge \Lambda$">|; 

$key = q/sigmainTheta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\sigma \in \Theta$">|; 

$key = q/sigmainThetaneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\sigma \in \Theta \ne \emptyset$">|; 

$key = q/sigmainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\sigma \in \setR$">|; 

$key = q/supXlesupY;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\sup X \le \sup Y$">|; 

$key = q/thetagelambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\theta \ge \lambda$">|; 

$key = q/thetainTheta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\theta \in \Theta$">|; 

$key = q/xinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img8.svg"
 ALT="$x \in S$">|; 

$key = q/{Eqts}S=supessentiel{f(x):xinA}I=infessentiel{f(x):xinA}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img112.svg"
 ALT="\begin{Eqts}
S = \supessentiel \{ f(x) : x \in A \} \\\\
I = \infessentiel \{ f(x) : x \in A \}
\end{Eqts}">|; 

$key = q/{Eqts}fessinferieursigma=supessentiel{f(x):xinA}gessinferieurtau=supessentiel{g(x):xinA}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img138.svg"
 ALT="\begin{Eqts}
f \essinferieur \sigma = \supessentiel \{ f(x) : x \in A \} \\\\
g \essinferieur \tau = \supessentiel \{ g(x) : x \in A \}
\end{Eqts}">|; 

$key = q/{Eqts}fesssuperieurlambda=infessentiel{f(x):xinA}gesssuperieurgamma=infessentiel{g(x):xinA}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img141.svg"
 ALT="\begin{Eqts}
f \esssuperieur \lambda = \infessentiel \{ f(x) : x \in A \} \\\\
g \esssuperieur \gamma = \infessentiel \{ g(x) : x \in A \}
\end{Eqts}">|; 

$key = q/{Eqts}supessentiel_{xinA}^muf(x)infessentiel_{xinA}^muf(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.03ex; vertical-align: -6.46ex; " SRC="|."$dir".q|img28.svg"
 ALT="\begin{Eqts}
\supessentiel_{x \in A}^\mu f(x) \ \\\\
\infessentiel_{x \in A}^\mu f(x)
\end{Eqts}">|; 

$key = q/{Eqts}supessentiel_{xinA}f(x)=inf{sigmainsetR:fessinferieursigma}infessentiel_{xinA}f(x)=sup{lambdainsetR:fesssuperieurlambda}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.24ex; vertical-align: -5.56ex; " SRC="|."$dir".q|img25.svg"
 ALT="\begin{Eqts}
\supessentiel_{x \in A} f(x) = \inf \{ \sigma \in \setR : f \essinf...
... \in A} f(x) = \sup \{ \lambda \in \setR : f \esssuperieur \lambda \}
\end{Eqts}">|; 

$key = q/{Eqts}supessentiel{f(x):xinA}=supessentiel_{xinA}f(x)infessentiel{f(x):xinA}=infessentiel_{xinA}f(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.87ex; vertical-align: -3.87ex; " SRC="|."$dir".q|img26.svg"
 ALT="\begin{Eqts}
\supessentiel \{f(x) : x \in A\} = \supessentiel_{x \in A} f(x) \\\\
\infessentiel \{f(x) : x \in A\} = \infessentiel_{x \in A} f(x)
\end{Eqts}">|; 

$key = q/{eqnarraystar}S+epsilon&=&S+frac{delta}{4}I-epsilon&=&S+delta-frac{delta}{4}=S+frac{3delta}{4}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.31ex; " SRC="|."$dir".q|img124.svg"
 ALT="\begin{eqnarray*}
S + \epsilon &amp;=&amp; S + \frac{\delta}{4} \\\\
I - \epsilon &amp;=&amp; S + \delta - \frac{\delta}{4} = S + \frac{3 \delta}{4}
\end{eqnarray*}">|; 

1;

