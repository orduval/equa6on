# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.85ex; " SRC="|."$dir".q|img64.svg"
 ALT="\begin{eqnarray*}
\OD{\sin}{\theta}(\theta)
&amp;=&amp; \lim_{\delta\to 0} \frac{\sin(\t...
...- 1) + \cos(\theta)  \sin(\delta)}{\delta} \\\\
&amp;=&amp; \cos(\theta)
\end{eqnarray*}">|; 

$key = q/(1,0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$(1,0)$">|; 

$key = q/(1,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$(1,1)$">|; 

$key = q/(2,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$(2,1)$">|; 

$key = q/(2,2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$(2,2)$">|; 

$key = q/(c_1,c_2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img166.svg"
 ALT="$(c_1,c_2)$">|; 

$key = q/(dx,dy);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$(dx,dy)$">|; 

$key = q/(epsilon,psi);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img114.svg"
 ALT="$(\epsilon,\psi)$">|; 

$key = q/(x,y)=(1,0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$(x,y) = (1,0)$">|; 

$key = q/-sin;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.92ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img150.svg"
 ALT="$-\sin$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$0$">|; 

$key = q/Q(0)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$Q(0)=I$">|; 

$key = q/Q(4psi)=Q(0)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img135.svg"
 ALT="$Q(4\psi)=Q(0)=I$">|; 

$key = q/Q(delta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$Q(\delta)$">|; 

$key = q/Q(theta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$Q(\theta)$">|; 

$key = q/^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$^{-1}$">|; 

$key = q/cos(0)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\cos(0) = 1$">|; 

$key = q/cos,sin:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\cos, \sin : \setR \mapsto \setR$">|; 

$key = q/cos;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\cos$">|; 

$key = q/delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\delta$">|; 

$key = q/deltato0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\delta\to 0$">|; 

$key = q/displaystyle(1,0)+(dx,dy)=(1,delta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle (1,0)+(dx,dy)=(1,\delta)$">|; 

$key = q/displaystyle(Q(theta)cdotx)^dualcdot(Q(theta)cdoty)=x^dualcdotQ(theta)^dualcdotQ(theta)cdoty=x^dualcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle (Q(\theta) \cdot x)^\dual \cdot (Q(\theta) \cdot y) = x^\dual \cdot Q(\theta)^\dual \cdot Q(\theta) \cdot y = x^\dual \cdot y$">|; 

$key = q/displaystyle-1lecos(theta)=frac{scalaire{x}{y}}{norme{x}norme{y}}le1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img159.svg"
 ALT="$\displaystyle -1 \le \cos(\theta) = \frac{ \scalaire{x}{y} }{ \norme{x} \norme{y} } \le 1$">|; 

$key = q/displaystyle-int_a^bsin(t)dt=cos(b)-cos(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle -\int_a^b \sin(t)  dt = \cos(b) - \cos(a)$">|; 

$key = q/displaystyle1-(t-epsilon)delta=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle 1 - (t-\epsilon)  \delta = 0$">|; 

$key = q/displaystyle1-u(t)strictinferieur1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle 1 - u(t) \strictinferieur 1$">|; 

$key = q/displaystyle2xdx+2ydy=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle 2  x  dx + 2  y  dy = 0$">|; 

$key = q/displaystyleOD{cos}{theta}(0)=-sin(0)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \OD{\cos}{\theta}(0) = -\sin(0) = 0$">|; 

$key = q/displaystyleOD{cos}{theta}(theta)=lim_{deltato0}frac{cos(theta+delta)-cos(theta)}{delta}dollar;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \OD{\cos}{\theta}(\theta) = \lim_{\delta\to 0} \frac{ \cos(\theta+\delta)-\cos(\theta) }{ \delta } \ $">|; 

$key = q/displaystyleOD{sin}{theta}(0)=cos(0)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \OD{\sin}{\theta}(0) = \cos(0) = 1$">|; 

$key = q/displaystyleOD{sin}{theta}(theta)=lim_{deltato0}frac{sin(theta+delta)-sin(theta)}{delta}dollar;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \OD{\sin}{\theta}(\theta) = \lim_{\delta\to 0} \frac{\sin(\theta+\delta)-\sin(\theta)}{\delta} \ $">|; 

$key = q/displaystyleOD{tan}{x}(x)=frac{cos(x)}{cos(x)}-frac{sin(x)cdotbig(-sin(x)big)}{cos(x)^2}=1+tan(x)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.04ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img155.svg"
 ALT="$\displaystyle \OD{\tan}{x}(x) = \frac{\cos(x)}{\cos(x)} - \frac{\sin(x) \cdot \big(-\sin(x)\big)}{\cos(x)^2} = 1 + \tan(x)^2$">|; 

$key = q/displaystyleOOD{cos}{theta}(theta)=OD{(-sin)}{theta}(theta)=-cos(theta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle \OOD{\cos}{\theta}(\theta) = \OD{(-\sin)}{\theta}(\theta) = -\cos(\theta)$">|; 

$key = q/displaystyleOOD{sin}{theta}(theta)=OD{cos}{theta}(theta)=-sin(theta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \OOD{\sin}{\theta}(\theta) = \OD{\cos}{\theta}(\theta) = -\sin(\theta)$">|; 

$key = q/displaystyleQ(-theta)=Q(theta)^{-1}=Q(theta)^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle Q(-\theta) = Q(\theta)^{-1} = Q(\theta)^\dual$">|; 

$key = q/displaystyleQ(0)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle Q(0) = I$">|; 

$key = q/displaystyleQ(t+4psi)=Q(t)Q(4psi)=Q(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle Q(t + 4\psi) = Q(t)  Q(4\psi) = Q(t)$">|; 

$key = q/displaystyleQ(theta)^dual=Q(theta)^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle Q(\theta)^\dual = Q(\theta)^{-1}$">|; 

$key = q/displaystyleQ(theta)^dualcdotQ(theta)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle Q(\theta)^\dual \cdot Q(\theta) = I$">|; 

$key = q/displaystyleQ(theta)cdotQ(-theta)=Q(theta-theta)=Q(0)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle Q(\theta) \cdot Q(-\theta) = Q(\theta - \theta) = Q(0) = I$">|; 

$key = q/displaystyleQ(theta_1+theta_2)=Q(theta_2)cdotQ(theta_1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle Q(\theta_1+\theta_2) = Q(\theta_2) \cdot Q(\theta_1)$">|; 

$key = q/displaystyleQ:setRmapstomatrice(setR,2,2),qquadthetamapstoQ(theta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle Q : \setR\mapsto \matrice(\setR, 2, 2), \qquad \theta \mapsto Q(\theta)$">|; 

$key = q/displaystyleY^dualcdotX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img165.svg"
 ALT="$\displaystyle Y^\dual \cdot X$">|; 

$key = q/displaystyleabs{u(t)-u(0)}=abs{u(t)-1}strictinferieur1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \abs{u(t)-u(0)} = \abs{u(t) - 1} \strictinferieur 1$">|; 

$key = q/displaystylecos(2x)=1-2sin(x)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\displaystyle \cos(2  x) = 1 - 2 \sin(x)^2$">|; 

$key = q/displaystylecos(2x)=2cos(x)^2-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img145.svg"
 ALT="$\displaystyle \cos(2  x) = 2 \cos(x)^2 - 1$">|; 

$key = q/displaystylecos(2x)=cos(x)^2-sin(x)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img144.svg"
 ALT="$\displaystyle \cos(2  x) = \cos(x)^2 - \sin(x)^2$">|; 

$key = q/displaystylecos(2x)=cos(x+x)=cos(x)cdotcos(x)-sin(x)cdotsin(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle \cos(2  x) = \cos(x + x) = \cos(x) \cdot \cos(x) - \sin(x) \cdot \sin(x)$">|; 

$key = q/displaystylecos(theta)^2+sin(theta)^2=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \cos(\theta)^2 + \sin(\theta)^2 = 1$">|; 

$key = q/displaystylecos(x)^2+sin(x)^2=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \cos(x)^2 + \sin(x)^2 = 1$">|; 

$key = q/displaystylecos(x)^2=frac{cos(2x)+1}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.08ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\displaystyle \cos(x)^2 = \frac{\cos(2  x) + 1}{2}$">|; 

$key = q/displaystyledelta=int_0^epsilonu(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle \delta = \int_0^\epsilon u(s) ds$">|; 

$key = q/displaystylederiveepartielle{c_i}{x_j}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.60ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img168.svg"
 ALT="$\displaystyle \deriveepartielle{c_i}{x_j} = 0$">|; 

$key = q/displaystyleint_a^bcos(t)dt=sin(b)-sin(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img149.svg"
 ALT="$\displaystyle \int_a^b \cos(t)  dt = \sin(b) - \sin(a)$">|; 

$key = q/displaystyleint_a^bsin(t)dt=cos(a)-cos(b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\displaystyle \int_a^b \sin(t)  dt = \cos(a) - \cos(b)$">|; 

$key = q/displaystylelim_{deltato0}frac{abs{E_{ij}(delta)}}{delta}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \lim_{\delta\to 0} \frac{\abs{E_{ij}(\delta)}}{\delta} = 0$">|; 

$key = q/displaystylelim_{deltato0}frac{cos(delta)-1}{delta}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \lim_{\delta\to 0} \frac{\cos(\delta) - 1}{\delta} = 0$">|; 

$key = q/displaystylelim_{deltato0}frac{sin(delta)-delta}{delta}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \lim_{\delta\to 0} \frac{\sin(\delta)-\delta}{\delta} = 0$">|; 

$key = q/displaystylelim_{deltato0}frac{sin(delta)}{delta}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \lim_{\delta\to 0} \frac{\sin(\delta)}{\delta} = 1$">|; 

$key = q/displaystylepi=2psi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle \pi = 2\psi$">|; 

$key = q/displaystylepsi=min{xge0:u(x)=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle \psi = \min\{ x \ge 0 : u(x) = 0 \}$">|; 

$key = q/displaystylepsileunsur{delta}+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle \psi \le \unsur{\delta} + \epsilon$">|; 

$key = q/displaystylescalaire{Q(theta)cdotx}{Q(theta)cdoty}=scalaire{x}{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \scalaire{Q(\theta) \cdot x}{Q(\theta) \cdot y} = \scalaire{x}{y}$">|; 

$key = q/displaystylescalaire{c_i}{c_j}=delta_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img167.svg"
 ALT="$\displaystyle \scalaire{c_i}{c_j} = \delta_{ij}$">|; 

$key = q/displaystylescalaire{x}{y}=x^dualcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \scalaire{x}{y} = x^\dual \cdot y$">|; 

$key = q/displaystylesin(x)^2=frac{1-cos(2x)}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.08ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img148.svg"
 ALT="$\displaystyle \sin(x)^2 = \frac{1 - \cos(2  x)}{2}$">|; 

$key = q/displaystylet=unsur{delta}+epsilon<+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle t = \unsur{\delta} + \epsilon &lt; +\infty$">|; 

$key = q/displaystyletan(0)=frac{sin(0)}{cos(0)}=frac{0}{1}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle \tan(0) = \frac{\sin(0)}{\cos(0)} = \frac{0}{1} = 0$">|; 

$key = q/displaystyletan(x)=frac{sin(x)}{cos(x)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\displaystyle \tan(x) = \frac{\sin(x)}{\cos(x)}$">|; 

$key = q/displaystyleu(t)=-int_psi^tv(s)dsle0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.07ex; vertical-align: -2.50ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\displaystyle u(t) = -\int_\psi^t v(s) ds \le 0$">|; 

$key = q/displaystyleu(t)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle u(t) \strictsuperieur 0$">|; 

$key = q/displaystyleu(t-2psi)=-u(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle u(t-2\psi) = -u(t)$">|; 

$key = q/displaystyleu(t-psi)=v(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\displaystyle u(t-\psi) = v(t)$">|; 

$key = q/displaystyleuleft(unsur{delta}+epsilonright)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle u\left(\unsur{\delta} + \epsilon\right) \le 0$">|; 

$key = q/displaystylev(2psi)=u(psi)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\displaystyle v(2\psi) = u(\psi) = 0$">|; 

$key = q/displaystylev(psi)=lim_{xtopsi}v(x)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.92ex; vertical-align: -2.09ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle v(\psi) = \lim_{x\to\psi} v(x) \ge 0$">|; 

$key = q/displaystylev(s)=u(t)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\displaystyle v(s) = u(t) \ge 0$">|; 

$key = q/displaystylev(t)=int_0^tu(s)ds>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle v(t) = \int_0^t u(s) ds &gt; 0$">|; 

$key = q/displaystylevarphi=inf{xinsetR:xge0,u(x)<0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle \varphi = \inf\{ x \in \setR : x \ge 0,  u(x) &lt; 0 \}$">|; 

$key = q/displaystylevarphileunsur{delta}+epsilon<+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle \varphi \le \unsur{\delta} + \epsilon &lt; +\infty$">|; 

$key = q/displaystylex^2+y^2=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle x^2 + y^2 = 1$">|; 

$key = q/epsiloninintervalleouvert{0}{varphi};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\epsilon\in \intervalleouvert{0}{\varphi}$">|; 

$key = q/f(t)=u(t-psi);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img118.svg"
 ALT="$f(t) = u(t-\psi)$">|; 

$key = q/intervalleouvert{0}{varphi};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\intervalleouvert{0}{\varphi}$">|; 

$key = q/kinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img142.svg"
 ALT="$k\in\setZ$">|; 

$key = q/pi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\pi$">|; 

$key = q/psi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\psi$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\setR$">|; 

$key = q/setR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\setR^2$">|; 

$key = q/sin(0)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\sin(0) = 0$">|; 

$key = q/sin;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.73ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\sin$">|; 

$key = q/sin[psi,2psi];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img124.svg"
 ALT="$s\in [\psi,2\psi]$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img101.svg"
 ALT="$t$">|; 

$key = q/t=2psi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img120.svg"
 ALT="$t = 2 \psi$">|; 

$key = q/t=s-psiin[0,psi];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img125.svg"
 ALT="$t = s-\psi \in [0,\psi]$">|; 

$key = q/tan;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\tan$">|; 

$key = q/theta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img160.svg"
 ALT="$\theta$">|; 

$key = q/theta_1+theta_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\theta_1+\theta_2$">|; 

$key = q/theta_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\theta_1$">|; 

$key = q/theta_1=theta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\theta_1 = \theta$">|; 

$key = q/theta_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\theta_2$">|; 

$key = q/theta_2=delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\theta_2 = \delta$">|; 

$key = q/thetain[0,pi];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img158.svg"
 ALT="$\theta\in [0,\pi]$">|; 

$key = q/thetainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\theta \in \setR$">|; 

$key = q/tinintervalleouvert{0}{epsilon};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img90.svg"
 ALT="$t\in \intervalleouvert{0}{\epsilon}$">|; 

$key = q/tinintervalleouvert{epsilon}{varphi};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img95.svg"
 ALT="$t\in \intervalleouvert{\epsilon}{\varphi}$">|; 

$key = q/tinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img71.svg"
 ALT="$t \in \setR$">|; 

$key = q/u(2psi);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img123.svg"
 ALT="$u(2\psi)$">|; 

$key = q/u(2psi)=-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img129.svg"
 ALT="$u(2\psi) = -1$">|; 

$key = q/u(2psi)^2=1-v(2psi)^2=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img122.svg"
 ALT="$u(2\psi)^2 = 1 - v(2\psi)^2 = 1$">|; 

$key = q/u(4psi)=u(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img133.svg"
 ALT="$u(4\psi) = u(0)$">|; 

$key = q/u(psi)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$u(\psi) = 0$">|; 

$key = q/u(t)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img102.svg"
 ALT="$u(t) \le 0$">|; 

$key = q/u,v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img86.svg"
 ALT="$u, v$">|; 

$key = q/u:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img69.svg"
 ALT="$u : \setR \mapsto \setR$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img78.svg"
 ALT="$u$">|; 

$key = q/u^2+v^2=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.20ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img110.svg"
 ALT="$u^2+v^2 = 1$">|; 

$key = q/uge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img96.svg"
 ALT="$u \ge 0$">|; 

$key = q/v(4psi)=v(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img134.svg"
 ALT="$v(4\psi) = v(0)$">|; 

$key = q/v(psi)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$v(\psi) = 1$">|; 

$key = q/v(psi)=pm1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img112.svg"
 ALT="$v(\psi)=\pm 1$">|; 

$key = q/v(psi)^2=1-u(psi)^2=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img111.svg"
 ALT="$v(\psi)^2 = 1 - u(\psi)^2 = 1$">|; 

$key = q/v:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img75.svg"
 ALT="$v : \setR \mapsto \setR$">|; 

$key = q/v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img80.svg"
 ALT="$v$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\varphi$">|; 

$key = q/varphi<+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\varphi &lt; +\infty$">|; 

$key = q/vgedelta>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img113.svg"
 ALT="$v \ge \delta &gt; 0$">|; 

$key = q/x,yinsetR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$x,y\in\setR^2$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img161.svg"
 ALT="$x$">|; 

$key = q/x=cos(theta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$x = \cos(\theta)$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img24.svg"
 ALT="$x \in \setR$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img162.svg"
 ALT="$y$">|; 

$key = q/y=(R,theta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img169.svg"
 ALT="$y = (R,\theta)$">|; 

$key = q/y=sin(theta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$y = \sin(\theta)$">|; 

$key = q/{Eqts}-1lecosle1-1lesinle1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img29.svg"
 ALT="\begin{Eqts}
-1 \le \cos \le 1 \\\\
-1 \le \sin \le 1
\end{Eqts}">|; 

$key = q/{Eqts}E_{11}=E_{22}E_{12}=-E_{21}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img45.svg"
 ALT="\begin{Eqts}
E_{11} = E_{22} \\\\
E_{12} = -E_{21}
\end{Eqts}">|; 

$key = q/{Eqts}Matrix{{cc}cos(theta)&-sin(theta)sin(theta)&cos(theta)Matrix{qquadmathrm{ou}qquadMatrix{{cc}cos(theta)&sin(theta)sin(theta)&-cos(theta)Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img16.svg"
 ALT="\begin{Eqts}
\begin{Matrix}{cc}
\cos(\theta) &amp; -\sin(\theta) \\\\
\sin(\theta) &amp; ...
...s(\theta) &amp; \sin(\theta) \\\\
\sin(\theta) &amp; -\cos(\theta)
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}OD{cos}{theta}(theta)=-sin(theta)OD{sin}{theta}(theta)=cos(theta){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.05ex; vertical-align: -6.47ex; " SRC="|."$dir".q|img65.svg"
 ALT="\begin{Eqts}
\OD{\cos}{\theta}(\theta) = -\sin(\theta) \ \\\\
\OD{\sin}{\theta}(\theta) = \cos(\theta)
\end{Eqts}">|; 

$key = q/{Eqts}OOD{v}{t}=-vv(psi)=1OD{v}{t}(psi)=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.28ex; vertical-align: -6.59ex; " SRC="|."$dir".q|img117.svg"
 ALT="\begin{Eqts}
\OOD{v}{t} = -v \\\\
v(\psi) = 1 \\\\
\OD{v}{t}(\psi) = 0
\end{Eqts}">|; 

$key = q/{Eqts}OOD{}{t}(-u)=-(-u)-u(0)=-1OD{}{t}(-u)(0)=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.28ex; vertical-align: -6.59ex; " SRC="|."$dir".q|img130.svg"
 ALT="\begin{Eqts}
\OOD{}{t}(-u) = -(-u) \\\\
-u(0) = -1 \\\\
\OD{}{t}(-u)(0) = 0
\end{Eqts}">|; 

$key = q/{Eqts}Q(delta)=Matrix{{cc}1&-deltadelta&1Matrix{+Matrix{{cc}E_{11}&E_{12}E_{21}&E_{22}Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img43.svg"
 ALT="\begin{Eqts}
Q(\delta) =
\begin{Matrix}{cc}
1 &amp; -\delta \\\\
\delta &amp; 1
\end{Matrix}+
\begin{Matrix}{cc}
E_{11} &amp; E_{12} \\\\
E_{21} &amp; E_{22}
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}Q(delta)approxMatrix{{cc}1&-deltadelta&1Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img41.svg"
 ALT="\begin{Eqts}
Q(\delta) \approx
\begin{Matrix}{cc}
1 &amp; -\delta \\\\
\delta &amp; 1
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}Q(delta)cdotMatrix{{c}10Matrix{approxMatrix{{c}1deltaMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img39.svg"
 ALT="\begin{Eqts}
Q(\delta) \cdot
\begin{Matrix}{c}
1 \\\\
0
\end{Matrix} \approx
\begin{Matrix}{c}
1 \\\\
\delta
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}Q(theta)=Matrix{{cc}cos(theta)&-sin(theta)sin(theta)&cos(theta)Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img22.svg"
 ALT="\begin{Eqts}
Q(\theta) =
\begin{Matrix}{cc}
\cos(\theta) &amp; -\sin(\theta) \\\\
\sin(\theta) &amp; \cos(\theta)
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}X^dualcdotX=I_kY^dualcdotY=I_l{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img164.svg"
 ALT="\begin{Eqts}
X^\dual \cdot X = I_k \\\\
Y^\dual \cdot Y = I_l
\end{Eqts}">|; 

$key = q/{Eqts}Xinmatrice(setR,k,n)Yinmatrice(setR,l,n){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img163.svg"
 ALT="\begin{Eqts}
X \in \matrice(\setR, k, n) \\\\
Y \in \matrice(\setR, l, n)
\end{Eqts}">|; 

$key = q/{Eqts}cos(0)=1sin(0)=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img23.svg"
 ALT="\begin{Eqts}
\cos(0) = 1 \\\\
\sin(0) = 0
\end{Eqts}">|; 

$key = q/{Eqts}cos(theta+2kpi)=...=cos(theta)sin(theta+2kpi)=...=sin(theta){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img141.svg"
 ALT="\begin{Eqts}
\cos(\theta+2 k\pi) = ... = \cos(\theta) \\\\
\sin(\theta+2 k\pi) = ... = \sin(\theta)
\end{Eqts}">|; 

$key = q/{Eqts}cos(theta+2pi)=cos(theta)sin(theta+2pi)=sin(theta){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img140.svg"
 ALT="\begin{Eqts}
\cos(\theta+2\pi) = \cos(\theta) \\\\
\sin(\theta+2\pi) = \sin(\theta)
\end{Eqts}">|; 

$key = q/{Eqts}cos(theta_1+theta_2)=cos(theta_1)cos(theta_2)-sin(theta_1)sin(theta_2)sin(theta_1+theta_2)=sin(theta_1)cos(theta_2)+cos(theta_1)sin(theta_2){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img57.svg"
 ALT="\begin{Eqts}
\cos(\theta_1+\theta_2) = \cos(\theta_1)  \cos(\theta_2)-\sin(\the...
..._2) = \sin(\theta_1)  \cos(\theta_2)+\cos(\theta_1)  \sin(\theta_2)
\end{Eqts}">|; 

$key = q/{Eqts}cos(x)^2ge0sin(x)^2ge0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img26.svg"
 ALT="\begin{Eqts}
\cos(x)^2 \ge 0 \\\\
\sin(x)^2 \ge 0
\end{Eqts}">|; 

$key = q/{Eqts}dx=0dy=delta{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img36.svg"
 ALT="\begin{Eqts}
dx = 0 \\\\
dy = \delta
\end{Eqts}">|; 

$key = q/{Eqts}e_R=deriveepartielle{r}{R}=cos(theta)cdotc_1+sin(theta)cdotc_2e_theta=deriveepartielle{r}{theta}=-Rcdotsin(theta)c_1+Rcdotcos(theta)cdotc_2{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.49ex; vertical-align: -4.68ex; " SRC="|."$dir".q|img172.svg"
 ALT="\begin{Eqts}
e_R = \deriveepartielle{r}{R} = \cos(\theta) \cdot c_1 + \sin(\thet...
...{\theta} = -R \cdot \sin(\theta) c_1 + R \cdot \cos(\theta) \cdot c_2
\end{Eqts}">|; 

$key = q/{Eqts}r=x_1cdotc_1+x_2cdotc_2dr=e_RdR+e_thetadtheta{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img171.svg"
 ALT="\begin{Eqts}
r = x_1 \cdot c_1 + x_2 \cdot c_2 \\\\
dr = e_R  dR + e_\theta  d\theta
\end{Eqts}">|; 

$key = q/{Eqts}u(0)=1partialu=-v{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img79.svg"
 ALT="\begin{Eqts}
u(0) = 1 \\\\
\partial u = -v
\end{Eqts}">|; 

$key = q/{Eqts}u(0)=1qquadv(0)=0u(psi)=0qquadv(psi)=1u(2psi)=-1qquadv(2psi)=0u(3psi)=0qquadv(3psi)=-1u(4psi)=1qquadv(4psi)=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.49ex; vertical-align: -7.70ex; " SRC="|."$dir".q|img132.svg"
 ALT="\begin{Eqts}
u(0) = 1 \qquad v(0) = 0 \\\\
u(\psi) = 0 \qquad v(\psi) = 1 \\\\
u(2...
...(3\psi) = 0 \qquad v(3\psi) = -1 \\\\
u(4\psi) = 1 \qquad v(4\psi) = 0
\end{Eqts}">|; 

$key = q/{Eqts}u(t)-1=-int_0^tv(s)dsv(t)-0=int_0^tu(s)ds{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.12ex; vertical-align: -5.50ex; " SRC="|."$dir".q|img87.svg"
 ALT="\begin{Eqts}
u(t) - 1 = - \int_0^t v(s) ds \\\\
v(t) - 0 = \int_0^t u(s) ds
\end{Eqts}">|; 

$key = q/{Eqts}u(t)-u(0)=int_0^tOD{u}{t}(s)dsv(t)-v(0)=int_0^tOD{v}{t}(s)ds{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.12ex; vertical-align: -5.50ex; " SRC="|."$dir".q|img85.svg"
 ALT="\begin{Eqts}
u(t) - u(0) = \int_0^t \OD{u}{t}(s)  ds \\\\
v(t) - v(0) = \int_0^t \OD{v}{t}(s)  ds
\end{Eqts}">|; 

$key = q/{Eqts}u(t)=1-int_0^tv(s)dsv(t)=int_0^tu(s)ds{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.12ex; vertical-align: -5.50ex; " SRC="|."$dir".q|img88.svg"
 ALT="\begin{Eqts}
u(t) = 1 - \int_0^t v(s) ds \\\\
v(t) = \int_0^t u(s) ds
\end{Eqts}">|; 

$key = q/{Eqts}u(t)=cos(t)v(t)=sin(t){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img77.svg"
 ALT="\begin{Eqts}
u(t) = \cos(t) \\\\
v(t) = \sin(t)
\end{Eqts}">|; 

$key = q/{Eqts}u(t+4psi)=u(t)v(t+4psi)=v(t){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img137.svg"
 ALT="\begin{Eqts}
u(t+4\psi) = u(t) \\\\
v(t+4\psi) = v(t)
\end{Eqts}">|; 

$key = q/{Eqts}v(0)=0partialv=u{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img81.svg"
 ALT="\begin{Eqts}
v(0) = 0 \\\\
\partial v = u
\end{Eqts}">|; 

$key = q/{Eqts}x_1=Rcdotcos(theta)x_2=Rcdotsin(theta){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img170.svg"
 ALT="\begin{Eqts}
x_1 = R \cdot \cos(\theta) \\\\
x_2 = R \cdot \sin(\theta)
\end{Eqts}">|; 

$key = q/{eqnarraystar}cos(x)^2=1-sin(x)^2le1sin(x)^2=1-cos(x)^2le1{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img28.svg"
 ALT="\begin{eqnarray*}
\cos(x)^2 = 1 - \sin(x)^2 \le 1 \\\\
\sin(x)^2 = 1 - \cos(x)^2 \le 1
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}partial^2u(t)&=&-u(t)u(0)&=&1partialu(0)&=&0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img70.svg"
 ALT="\begin{eqnarray*}
\partial^2 u(t) &amp;=&amp; -u(t) \\\\
u(0) &amp;=&amp; 1 \\\\
\partial u(0) &amp;=&amp; 0
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}partial^2v(t)&=&-v(t)v(0)&=&0partialv(0)&=&1{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img76.svg"
 ALT="\begin{eqnarray*}
\partial^2 v(t) &amp;=&amp; -v(t) \\\\
v(0) &amp;=&amp; 0 \\\\
\partial v(0) &amp;=&amp; 1
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}partialu(t)&=&1+u(t)^2u(0)&=&0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img157.svg"
 ALT="\begin{eqnarray*}
\partial u(t) &amp;=&amp; 1 + u(t)^2 \\\\
u(0) &amp;=&amp; 0
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}u(t)&=&1-int_0^tv(s)dsle1-int_epsilon^tv(s)ds&le&1-(t-epsilon)delta{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.68ex; " SRC="|."$dir".q|img100.svg"
 ALT="\begin{eqnarray*}
u(t) &amp;=&amp; 1 - \int_0^t v(s) ds \le 1 - \int_\epsilon^t v(s) ds \\\\
&amp;\le&amp; 1 - (t-\epsilon)  \delta
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}v(t)&=&int_0^tu(s)ds=int_0^epsilonu(s)ds+int_epsilon^tu(s)ds&ge&int_0^epsilonu(s)ds>0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.66ex; " SRC="|."$dir".q|img98.svg"
 ALT="\begin{eqnarray*}
v(t) &amp;=&amp; \int_0^t u(s) ds = \int_0^\epsilon u(s) ds + \int_\epsilon^t u(s) ds \\\\
&amp;\ge&amp; \int_0^\epsilon u(s) ds &gt; 0
\end{eqnarray*}">|; 

1;

