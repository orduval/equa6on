# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\Omega$">|; 

$key = q/Omega^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\Omega^n$">|; 

$key = q/autreaddition,autremultiplication;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.21ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\autreaddition, \autremultiplication$">|; 

$key = q/autreaddition;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.76ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\autreaddition$">|; 

$key = q/autremultiplication;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.73ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\autremultiplication$">|; 

$key = q/displaystyle(x_1,x_2,...,x_n),(y_1,y_2,...,y_n)inOmega^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle (x_1,x_2,...,x_n), (y_1,y_2,...,y_n) \in \Omega^n$">|; 

$key = q/displaystyle(x_1,x_2,...,x_n)opera(y_1,y_2,...,y_n)=(x_1operay_1,x_2operay_2,...,x_noperay_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle (x_1,x_2,...,x_n) \opera (y_1,y_2,...,y_n) = (x_1 \opera y_1, x_2 \opera y_2, ..., x_n \opera y_n)$">|; 

$key = q/displaystyle(xautreadditiony)autremultiplicationz=(xautremultiplicationz)autreaddition(yautremultiplicationz);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle (x \autreaddition y) \autremultiplication z = (x \autremultiplication z) \autreaddition (y \autremultiplication z)$">|; 

$key = q/displaystyle(xoperay)operaz=xopera(yoperaz);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle (x \opera y) \opera z = x \opera (y \opera z)$">|; 

$key = q/displaystylem=moperan=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.73ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle m = m \opera n = n$">|; 

$key = q/displaystylenoperax=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.73ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle n \opera x = x$">|; 

$key = q/displaystyleopera:OmegatimesOmegamapstoOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \opera : \Omega \times \Omega \mapsto \Omega$">|; 

$key = q/displaystylex=(x_1,x_2,...,x_n)inOmega^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle x = (x_1,x_2,...,x_n) \in \Omega^n$">|; 

$key = q/displaystylexoperan=noperax=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.73ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle x \opera n = n \opera x = x$">|; 

$key = q/displaystylexoperan=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.73ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle x \opera n = x$">|; 

$key = q/displaystylexoperax^inverse=x^inverseoperax=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle x \opera x^\inverse = x^\inverse \opera x = n$">|; 

$key = q/displaystylexoperay=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle x \opera y = n$">|; 

$key = q/displaystylexoperay=yoperax;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle x \opera y = y \opera x$">|; 

$key = q/displaystylexoperayoperaz=xopera(yoperaz)=(xoperay)operaz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle x \opera y \opera z = x \opera (y \opera z) = (x \opera y) \opera z$">|; 

$key = q/displaystyley=(y_1,y_2,...,y_n)inOmega^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle y = (y_1,y_2,...,y_n) \in \Omega^n$">|; 

$key = q/displaystyley=z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle y = z$">|; 

$key = q/displaystyleyoperax=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle y \opera x = n$">|; 

$key = q/displaystyleyoperaxoperaz=(yoperax)operaz=noperaz=z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle y \opera x \opera z = (y \opera x) \opera z = n \opera z = z$">|; 

$key = q/displaystyleyoperaxoperaz=yopera(xoperaz)=yoperan=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle y \opera x \opera z = y \opera (x \opera z) = y \opera n = y$">|; 

$key = q/displaystylez=(z_1,z_2,...,z_n)inOmega^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle z = (z_1,z_2,...,z_n) \in \Omega^n$">|; 

$key = q/displaystylez=xoperay;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle z = x \opera y$">|; 

$key = q/displaystylez_i=x_ioperay_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle z_i = x_i \opera y_i$">|; 

$key = q/displaystylezautremultiplication(xautreadditiony)=(zautremultiplicationx)autreaddition(zautremultiplicationy);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle z \autremultiplication (x \autreaddition y) = (z \autremultiplication x) \autreaddition (z \autremultiplication y)$">|; 

$key = q/iin{1,2,...,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$i \in \{1,2,...,n\}$">|; 

$key = q/m,ninOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$m,n \in \Omega$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img23.svg"
 ALT="$n$">|; 

$key = q/ninOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$n \in \Omega$">|; 

$key = q/opera;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.73ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\opera$">|; 

$key = q/x,y,zinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img20.svg"
 ALT="$x,y,z \in \Omega$">|; 

$key = q/x,yinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$x,y \in \Omega$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$x$">|; 

$key = q/x^inverseinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.20ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img17.svg"
 ALT="$x^\inverse \in \Omega$">|; 

$key = q/x_1,x_2,...,x_ninOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$x_1,x_2,...,x_n \in \Omega$">|; 

$key = q/xinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img9.svg"
 ALT="$x \in \Omega$">|; 

$key = q/y,zinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$y,z \in \Omega$">|; 

$key = q/yinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$y \in \Omega$">|; 

$key = q/zinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img4.svg"
 ALT="$z \in \Omega$">|; 

1;

