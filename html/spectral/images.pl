# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.22ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \norme{u_m - u_n} \le \frac{ \norme{A}^{n + 1} }{ \abs{\lambda}^{...
...da}^i } \right] \cdot \norme{x} = r^{n + 1} \cdot S_{m - n - 1} \cdot \norme{x}$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img36.svg"
 ALT="$0$">|; 

$key = q/A:HmapstoH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$A : H \mapsto H$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img30.svg"
 ALT="$A$">|; 

$key = q/A^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$A^k$">|; 

$key = q/A^nto0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$A^n \to 0$">|; 

$key = q/B=Aslashlambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$B = A / \lambda$">|; 

$key = q/H;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$H$">|; 

$key = q/L(x)inH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$L(x) \in H$">|; 

$key = q/L:HmapstoH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$L : H \mapsto H$">|; 

$key = q/S_{m-n-1}leS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img34.svg"
 ALT="$S_{m - n - 1} \le S$">|; 

$key = q/displaystyle(identite-A)^{-1}=sum_{k=0}^{+infty}A^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle (\identite - A)^{-1} = \sum_{k = 0}^{+\infty} A^k$">|; 

$key = q/displaystyle(identite-A)circsum_{k=0}^nA^k=identite-A^{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle (\identite - A) \circ \sum_{k = 0}^n A^k = \identite - A^{n + 1}$">|; 

$key = q/displaystyle(identite-A)circsum_{k=0}^{+infty}A^k=lim_{ntoinfty}(identite-A^{n+1})=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle (\identite - A) \circ \sum_{k = 0}^{+\infty} A^k = \lim_{n \to \infty} (\identite - A^{n + 1}) = \identite$">|; 

$key = q/displaystyle(lambdacdotidentite-A)^{-1}=unsur{lambda}cdotL;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle (\lambda \cdot \identite - A)^{-1} = \unsur{\lambda} \cdot L$">|; 

$key = q/displaystyleL=sum_{k=0}^{+infty}unsur{lambda^k}cdotA^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle L = \sum_{k = 0}^{+\infty} \unsur{\lambda^k} \cdot A^k$">|; 

$key = q/displaystyleS=sum_{k=0}^{+infty}frac{norme{A^k}}{abs{lambda}^k}=lim_{ntoinfty}S_n=sup_{ninsetN}S_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle S = \sum_{k = 0}^{+\infty} \frac{ \norme{A^k} }{ \abs{\lambda}^k } = \lim_{n \to \infty} S_n = \sup_{n \in \setN} S_n$">|; 

$key = q/displaystyleS_n=sum_{k=0}^nfrac{norme{A^k}}{abs{lambda}^k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle S_n = \sum_{k = 0}^n \frac{ \norme{A^k} }{ \abs{\lambda}^k }$">|; 

$key = q/displaystyleexp(A)(u)=sum_{k=0}^{+infty}unsur{k!}cdotA^k(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \exp(A)(u) = \sum_{k = 0}^{+\infty} \unsur{k !} \cdot A^k(u)$">|; 

$key = q/displaystyleexp(A)=sum_{k=0}^{+infty}unsur{k!}cdotA^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \exp(A) = \sum_{k = 0}^{+\infty} \unsur{k !} \cdot A^k$">|; 

$key = q/displaystylefrac{norme{A^k}}{abs{lambda}^k}lefrac{norme{A}^k}{abs{lambda}^k}=r^kstrictinferieur1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.48ex; vertical-align: -2.63ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \frac{ \norme{A^k} }{ \abs{\lambda}^k } \le \frac{ \norme{A}^k }{ \abs{\lambda}^k } = r^k \strictinferieur 1$">|; 

$key = q/displaystyleleft[sum_{k=0}^nA^kright]circ(identite-A)=identite-A^{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \left[ \sum_{k = 0}^n A^k \right] \circ (\identite - A) = \identite - A^{n + 1}$">|; 

$key = q/displaystyleleft[sum_{k=0}^{+infty}A^kright]circ(identite-A)=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \left[ \sum_{k = 0}^{+\infty} A^k \right] \circ (\identite - A) = \identite$">|; 

$key = q/displaystylenorme{A^k}lenorme{A}^kstrictinferieurabs{lambda}^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.20ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \norme{A^k} \le \norme{A}^k \strictinferieur \abs{\lambda}^k$">|; 

$key = q/displaystylenorme{L(x)}=lim_{ntoinfty}norme{u_n}lefrac{norme{x}}{1-r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.28ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \norme{L(x)} = \lim_{n \to \infty} \norme{u_n} \le \frac{\norme{x}}{1 - r}$">|; 

$key = q/displaystylenorme{L}leunsur{1-r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.03ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \norme{L} \le \unsur{1 - r}$">|; 

$key = q/displaystylenorme{u_m-u_n}=norme{sum_{k=n+1}^munsur{lambda^k}cdotA^k(x)}lesum_{k=n+1}^mfrac{norme{A^k(x)}}{abs{lambda}^k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.37ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \norme{u_m - u_n} = \norme{\sum_{k = n + 1}^m \unsur{\lambda^k} \cdot A^k(x)} \le \sum_{k = n + 1}^m \frac{ \norme{A^k(x)} }{ \abs{\lambda}^k }$">|; 

$key = q/displaystylenorme{u_m-u_n}ler^{n+1}cdotScdotnorme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \norme{u_m - u_n} \le r^{n + 1} \cdot S \cdot \norme{x}$">|; 

$key = q/displaystylenorme{u_n}leleft[sum_{k=0}^nfrac{norme{A}^k}{abs{lambda}^k}right]cdotnorme{x}=frac{1-r^{n+1}}{1-r}cdotnorme{x}lefrac{norme{x}}{1-r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \norme{u_n} \le \left[ \sum_{k = 0}^n \frac{ \norme{A}^k }{ \abs{...
...{x} = \frac{ 1 - r^{n + 1} }{1 - r} \cdot \norme{x} \le \frac{\norme{x}}{1 - r}$">|; 

$key = q/displaystylesum_{k=0}^nfrac{norme{A^k}}{abs{lambda}^k}lesum_{k=0}^nr^k=frac{1-r^k}{1-r}leunsur{1-r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \sum_{k = 0}^n \frac{ \norme{A^k} }{ \abs{\lambda}^k } \le \sum_{k = 0}^n r^k = \frac{1 - r^k}{1 - r} \le \unsur{1 - r}$">|; 

$key = q/displaystyleu_n=sum_{k=0}^nunsur{lambda^k}cdotA^k(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle u_n = \sum_{k = 0}^n \unsur{\lambda^k} \cdot A^k(x)$">|; 

$key = q/exp(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\exp(A)$">|; 

$key = q/i=k-(n+1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$i = k - (n + 1)$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$k$">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\lambda$">|; 

$key = q/lambdainsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\lambda \in \setC$">|; 

$key = q/m,ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img26.svg"
 ALT="$m,n \in \setN$">|; 

$key = q/mgen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img27.svg"
 ALT="$m \ge n$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img41.svg"
 ALT="$n \in \setN$">|; 

$key = q/norme{A^n}lenorme{A}^nto0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.56ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\norme{A^n} \le \norme{A}^n \to 0$">|; 

$key = q/norme{A}strictinferieur1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\norme{A} \strictinferieur 1$">|; 

$key = q/norme{A}strictinferieurabs{lambda};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\norme{A} \strictinferieur \abs{\lambda}$">|; 

$key = q/norme{B}strictinferieur1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\norme{B} \strictinferieur 1$">|; 

$key = q/ntoinfty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$n \to \infty$">|; 

$key = q/r=norme{A}slashabs{lambda}strictinferieur1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$r = \norme{A} / \abs{\lambda} \strictinferieur 1$">|; 

$key = q/u_0=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img24.svg"
 ALT="$u_0 = x$">|; 

$key = q/u_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img37.svg"
 ALT="$u_n$">|; 

$key = q/uinH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img52.svg"
 ALT="$u \in H$">|; 

$key = q/xinH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img23.svg"
 ALT="$x \in H$">|; 

$key = q/{eqnarraystar}sum_{k=0}^nA^k&=&identite+A+A^2+...+A^nAcircsum_{k=0}^nA^k&=&A+A^2+A^3+...+A^{n+1}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 19.29ex; " SRC="|."$dir".q|img3.svg"
 ALT="\begin{eqnarray*}
\sum_{k = 0}^n A^k &amp;=&amp; \identite + A + A^2 + ... + A^n \\\\
A \circ \sum_{k = 0}^n A^k &amp;=&amp; A + A^2 + A^3 + ... + A^{n + 1}
\end{eqnarray*}">|; 

1;

