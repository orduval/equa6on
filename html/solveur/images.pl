# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.29ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle q_k^\dual \cdot A \cdot r_{k + 1} = \unsur{\conjaccent{\gamma_k}}...
...{k + 1} = - \unsur{\conjaccent{\gamma_k}} \cdot s_{k + 1}^\dual \cdot r_{k + 1}$">|; 

$key = q/(Acdotx-b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$(A \cdot x - b)$">|; 

$key = q/(N,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$(N,n)$">|; 

$key = q/(m,m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$(m,m)$">|; 

$key = q/(n,m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$(n,m)$">|; 

$key = q/(n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$(n,n)$">|; 

$key = q/(r_k-r_{k+1})^dual=conjaccent{alpha_k}cdotp_k^dualcdotA^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.53ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img98.svg"
 ALT="$(r_k - r_{k + 1})^\dual = \conjaccent{\alpha_k} \cdot p_k^\dual \cdot A^\dual$">|; 

$key = q/(s_k-s_{k+1})^dual=conjaccent{gamma_k}cdotq_k^dualcdotA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.53ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img94.svg"
 ALT="$(s_k - s_{k + 1})^\dual = \conjaccent{\gamma_k} \cdot q_k^\dual \cdot A$">|; 

$key = q/(v_1,...,v_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$(v_1,...,v_m)$">|; 

$key = q/(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$(x,y)$">|; 

$key = q/(x_k,y_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$(x_k,y_k)$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$A$">|; 

$key = q/A^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$A^\dual$">|; 

$key = q/Acdotx=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$A \cdot x = b$">|; 

$key = q/Acdotx=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img35.svg"
 ALT="$A \cdot x = y$">|; 

$key = q/H;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$H$">|; 

$key = q/H=H^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$H = H^\dual$">|; 

$key = q/Hcdotx=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$H \cdot x = b$">|; 

$key = q/Ngen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img26.svg"
 ALT="$N \ge n$">|; 

$key = q/V;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img61.svg"
 ALT="$V$">|; 

$key = q/V=W;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img65.svg"
 ALT="$V = W$">|; 

$key = q/W;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img62.svg"
 ALT="$W$">|; 

$key = q/W^dualcdotAcdotV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img59.svg"
 ALT="$W^\dual \cdot A \cdot V$">|; 

$key = q/alpha_iincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\alpha_i \in \corps$">|; 

$key = q/alpha_k,gamma_kincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\alpha_k,\gamma_k \in \corps$">|; 

$key = q/alpha_kinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\alpha_k \in \setR$">|; 

$key = q/b,cincorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img74.svg"
 ALT="$b,c \in \corps^n$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$b$">|; 

$key = q/beta_k,delta_kincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\beta_k,\delta_k \in \corps$">|; 

$key = q/binsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img42.svg"
 ALT="$b \in \setR^n$">|; 

$key = q/displaystyle(W^dualcdotAcdotV)^{-1}=W^dualcdotAcdotV=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle (W^\dual \cdot A \cdot V)^{-1} = W^\dual \cdot A \cdot V = I$">|; 

$key = q/displaystyle-J=b-Hcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle -J = b - H \cdot x$">|; 

$key = q/displaystyleA^dualcdotAcdotx=A^dualcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.32ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle A^\dual \cdot A \cdot x = A^\dual \cdot y$">|; 

$key = q/displaystyleAcdotx=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle A \cdot x = y$">|; 

$key = q/displaystyleHcdotx=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle H \cdot x = b$">|; 

$key = q/displaystyleJ=(partialvarphi)^dual=Hcdotx-b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle J = (\partial \varphi)^\dual = H \cdot x - b$">|; 

$key = q/displaystyleW=[w_1...w_m];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle W = [w_1  ...  w_m]$">|; 

$key = q/displaystyleW^dualcdot(b-Acdotx_{k+1})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle W^\dual \cdot (b - A \cdot x_{k + 1}) = 0$">|; 

$key = q/displaystyleW^dualcdot(r_k-AcdotVcdoty)=W^dualcdotr_k-W^dualcdotAcdotVcdoty=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle W^\dual \cdot (r_k - A \cdot V \cdot y) = W^\dual \cdot r_k - W^\dual \cdot A \cdot V \cdot y = 0$">|; 

$key = q/displaystyleb-Acdotx_{k+1}=b-Acdotx_k-AcdotVcdoty=r_k-AcdotVcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle b - A \cdot x_{k + 1} = b - A \cdot x_k - A \cdot V \cdot y = r_k - A \cdot V \cdot y$">|; 

$key = q/displaystylebeta_k=frac{s_{k+1}^dualcdotr_{k+1}}{s_k^dualcdotr_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.65ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle \beta_k = \frac{ s_{k + 1}^\dual \cdot r_{k + 1} }{ s_k^\dual \cdot r_k }$">|; 

$key = q/displaystylecomposante_{ij}W^dualcdotAcdotV=w_i^dualcdotAcdotv_j=v_i^dualcdotAcdotv_j=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.25ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \composante_{ij} W^\dual \cdot A \cdot V = w_i^\dual \cdot A \cdot v_j = v_i^\dual \cdot A \cdot v_j = \indicatrice_{ij}$">|; 

$key = q/displaystyledelta_k=frac{r_{k+1}^dualcdots_{k+1}}{r_k^dualcdots_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.65ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle \delta_k = \frac{ r_{k + 1}^\dual \cdot s_{k + 1} }{ r_k^\dual \cdot s_k }$">|; 

$key = q/displaystylekrylov_m(A,u)=combilin{u,Acdotu,A^2cdotu,...,A^{m-1}cdotu};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \krylov_m(A,u) = \combilin{ u, A \cdot u, A^2 \cdot u, ..., A^{m - 1} \cdot u }$">|; 

$key = q/displaystylemathcal{E}(y)=(r_k-AcdotVcdoty)^dualcdot(r_k-AcdotVcdoty);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \mathcal{E}(y) = (r_k - A \cdot V \cdot y)^\dual \cdot (r_k - A \cdot V \cdot y)$">|; 

$key = q/displaystylepartialvarphi(x)=Hcdotx-b=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \partial \varphi(x) = H \cdot x - b = 0$">|; 

$key = q/displaystyler_k=b-Acdotx_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle r_k = b - A \cdot x_k$">|; 

$key = q/displaystyles_k^dualcdotr_{k+1}=r_k^dualcdots_{k+1}=q_k^dualcdotAcdotp_{k+1}=p_k^dualcdotA^dualcdotq_{k+1}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle s_k^\dual \cdot r_{k + 1} = r_k^\dual \cdot s_{k + 1} = q_k^\dual \cdot A \cdot p_{k + 1} = p_k^\dual \cdot A^\dual \cdot q_{k + 1} = 0$">|; 

$key = q/displaystylevarphi(x)=unsur{2}x^dualcdotHcdotx-x^dualcdotb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \varphi(x) = \unsur{2} x^\dual \cdot H \cdot x - x^\dual \cdot b$">|; 

$key = q/displaystylew_i^dualcdot(b-Acdotx_{k+1})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle w_i^\dual \cdot (b - A \cdot x_{k + 1}) = 0$">|; 

$key = q/displaystylex=H^{-1}cdotb=(A^dualcdotA)^{-1}cdotA^dualcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle x = H^{-1} \cdot b = (A^\dual \cdot A)^{-1} \cdot A^\dual \cdot y$">|; 

$key = q/displaystylex=left[sum_{i=0}^{m-1}alpha_icdotA^iright]cdotu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.22ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle x = \left[ \sum_{i = 0}^{m - 1} \alpha_i \cdot A^i \right] \cdot u$">|; 

$key = q/displaystylex^dualcdot(A^dualcdotA)cdotx=(Acdotx)^dualcdot(Acdotx)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle x^\dual \cdot (A^\dual \cdot A) \cdot x = (A \cdot x)^\dual \cdot (A \cdot x) \ge 0$">|; 

$key = q/displaystylex^dualcdotHcdotxge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.18ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle x^\dual \cdot H \cdot x \ge 0$">|; 

$key = q/displaystylex_{k+1}=I(x_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle x_{k + 1} = I(x_k)$">|; 

$key = q/displaystylex_{k+1}=x_k+Vcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle x_{k + 1} = x_k + V \cdot y$">|; 

$key = q/displaystylex_{k+1}=x_k+alpha_kcdot(b-Hcdotx);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle x_{k + 1} = x_k + \alpha_k \cdot (b - H \cdot x)$">|; 

$key = q/displaystylex_{k+1}=x_k+sum_{i=1}^mv_icdoty_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle x_{k + 1} = x_k + \sum_{i = 1}^m v_i \cdot y_i$">|; 

$key = q/displaystylexinargmin_{zinsetR^n}left[unsur{2}z^dualcdotA^dualcdotAcdotz-z^dualcdotA^dualcdotyright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle x \in \arg\min_{z \in \setR^n} \left[ \unsur{2} z^\dual \cdot A^\dual \cdot A \cdot z - z^\dual \cdot A^\dual \cdot y \right]$">|; 

$key = q/displaystyley=(V^dualcdotA^dualcdotAcdotV)^{-1}cdot(V^dualcdotA^dualcdotr_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle y = (V^\dual \cdot A^\dual \cdot A \cdot V)^{-1} \cdot (V^\dual \cdot A^\dual \cdot r_k)$">|; 

$key = q/displaystyley=(W^dualcdotAcdotV)^{-1}cdotW^dualcdotr_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle y = (W^\dual \cdot A \cdot V)^{-1} \cdot W^\dual \cdot r_k$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img21.svg"
 ALT="$k$">|; 

$key = q/krylov_m(A,r_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\krylov_m(A,r_k)$">|; 

$key = q/krylov_m(A^dual,r_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\krylov_m(A^\dual,r_k)$">|; 

$key = q/mlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img43.svg"
 ALT="$m \le n$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img44.svg"
 ALT="$n$">|; 

$key = q/norme{Acdotx-y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\norme{A \cdot x - y}$">|; 

$key = q/p_0=r_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img86.svg"
 ALT="$p_0 = r_0$">|; 

$key = q/p_k,q_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img88.svg"
 ALT="$p_k, q_k$">|; 

$key = q/p_k,q_kincorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img84.svg"
 ALT="$p_k,q_k \in \corps^n$">|; 

$key = q/partial^2varphi=H;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\partial^2 \varphi = H$">|; 

$key = q/q_0=s_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img87.svg"
 ALT="$q_0 = s_0$">|; 

$key = q/r_0=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img81.svg"
 ALT="$r_0 = b$">|; 

$key = q/s_0=c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img82.svg"
 ALT="$s_0 = c$">|; 

$key = q/setR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\setR^n$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img37.svg"
 ALT="$u$">|; 

$key = q/v_i=w_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img66.svg"
 ALT="$v_i = w_i$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\varphi$">|; 

$key = q/w_1,...,w_minsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img53.svg"
 ALT="$w_1,...,w_m \in \setR^n$">|; 

$key = q/x,yincorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img75.svg"
 ALT="$x,y \in \corps^n$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$x$">|; 

$key = q/x_0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img7.svg"
 ALT="$x_0 = 0$">|; 

$key = q/x_0=y_0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img80.svg"
 ALT="$x_0 = y_0 = 0$">|; 

$key = q/x_kinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img6.svg"
 ALT="$x_k \in \setR^n$">|; 

$key = q/x_{k+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img46.svg"
 ALT="$x_{k + 1}$">|; 

$key = q/xinkrylov_m(A,u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$x \in \krylov_m(A,u)$">|; 

$key = q/xinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$x \in \setR^n$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img72.svg"
 ALT="$y$">|; 

$key = q/y_iinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img48.svg"
 ALT="$y_i \in \setR$">|; 

$key = q/yinsetR^N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img27.svg"
 ALT="$y \in \setR^N$">|; 

$key = q/{Eqts}Acdotx=bquadtext{(systèmeprimal)}A^dualcdoty=cquadtext{(systèmedual)}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img76.svg"
 ALT="\begin{Eqts}
A \cdot x = b \quad \text{ (système primal)} \\\\
A^\dual \cdot y = c \quad \text{ (système dual)}
\end{Eqts}">|; 

$key = q/{Eqts}H=A^dualcdotAb=A^dualcdoty{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img31.svg"
 ALT="\begin{Eqts}
H = A^\dual \cdot A \\\\
b = A^\dual \cdot y
\end{Eqts}">|; 

$key = q/{Eqts}p_{k+1}=r_{k+1}+beta_kcdotp_kq_{k+1}=s_{k+1}+delta_kcdotq_k{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img89.svg"
 ALT="\begin{Eqts}
p_{k + 1} = r_{k + 1} + \beta_k \cdot p_k \\\\
q_{k + 1} = s_{k + 1} + \delta_k \cdot q_k
\end{Eqts}">|; 

$key = q/{Eqts}r_k=b-Acdotx_ks_k=c-A^dualcdoty_k{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img79.svg"
 ALT="\begin{Eqts}
r_k = b - A \cdot x_k \\\\
s_k = c - A^\dual \cdot y_k
\end{Eqts}">|; 

$key = q/{Eqts}x_{k+1}=x_k+alpha_kcdotp_ky_{k+1}=y_k+gamma_kcdotq_k{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img83.svg"
 ALT="\begin{Eqts}
x_{k + 1} = x_k + \alpha_k \cdot p_k \\\\
y_{k + 1} = y_k + \gamma_k \cdot q_k
\end{Eqts}">|; 

$key = q/{eqnarraystar}V&=&[v_1...v_m]y&=&[y_1...y_m]^dual{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img49.svg"
 ALT="\begin{eqnarray*}
V &amp;=&amp; [v_1  ...  v_m] \\\\
y &amp;=&amp; [y_1  ...  y_m]^\dual
\end{eqnarray*}">|; 

1;

