# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle \OD{I}{t}(t) = \deriveepartielle{F}{s}(b(t),t) \cdot \OD{b}{t}(t)...
...t),t) \cdot \OD{b}{t}(t) + \int_{a(t)}^{b(t)} \deriveepartielle{f}{t}(s,t)  ds$">|; 

$key = q/(xpmh,ypmh);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$(x \pm h, y \pm h)$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img24.svg"
 ALT="$0$">|; 

$key = q/Delta_{xx},Delta_{yy}simo(h^2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\Delta_{xx}, \Delta_{yy} \sim o(h^2)$">|; 

$key = q/F:setR^nmapstosetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img74.svg"
 ALT="$F : \setR^n \mapsto \setR^m$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img43.svg"
 ALT="$F$">|; 

$key = q/Fincontinue^2(setR^2,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$F \in \continue^2(\setR^2,\setR)$">|; 

$key = q/H=partial^2f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$H = \partial^2 f$">|; 

$key = q/I:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$I : \setR \mapsto \setR$">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img46.svg"
 ALT="$I$">|; 

$key = q/a,b:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img32.svg"
 ALT="$a,b : \setR \mapsto \setR$">|; 

$key = q/alpha,betainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\alpha,\beta \in \setR$">|; 

$key = q/alpha=a(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\alpha = a(t)$">|; 

$key = q/beta=b(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\beta = b(t)$">|; 

$key = q/canonique_iinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\canonique_i \in \setR^n$">|; 

$key = q/displaystyleH^dual=H;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle H^\dual = H$">|; 

$key = q/displaystyleI(t)=int_{a(t)}^{b(t)}f(s,t)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle I(t) = \int_{a(t)}^{b(t)} f(s,t)  ds$">|; 

$key = q/displaystyleOD{psi_t}{s}(s)=phi_t(s);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \OD{\psi_t}{s}(s) = \phi_t(s)$">|; 

$key = q/displaystylederiveepartielle{F}{s}(s,t)=f(s,t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \deriveepartielle{F}{s}(s,t) = f(s,t)$">|; 

$key = q/displaystylederiveepartielle{F}{t}=deriveepartielle{}{t}left[deriveepartielle{f}{s}right]=deriveepartielle{}{s}left[deriveepartielle{f}{t}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \deriveepartielle{F}{t} = \deriveepartielle{}{t} \left[ \deriveep...
...e{f}{s} \right] = \deriveepartielle{}{s} \left[ \deriveepartielle{f}{t} \right]$">|; 

$key = q/displaystylef(x+h,y)+f(x-h,y)approx2f(x,y)+frac{h^2}{2}cdotpartial^2f(x,y)+o(h^2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle f(x + h,y) + f(x - h,y) \approx 2 f(x,y) + \frac{h^2}{2} \cdot \partial^2 f(x,y) + o(h^2)$">|; 

$key = q/displaystylef(x+h,y)-f(x-h,y)approx2hcdotpartial_xf(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle f(x + h,y) - f(x - h,y) \approx 2 h \cdot \partial_x f(x,y)$">|; 

$key = q/displaystylef_{ij}(x,y)=F(u+xcdotcanonique_i+ycdotcanonique_j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle f_{ij}(x,y) = F(u + x \cdot \canonique_i + y \cdot \canonique_j)$">|; 

$key = q/displaystyleint_alpha^betaderiveepartielle{f}{t}(s,t)ds=deriveepartielle{F}{t}(beta,t)-deriveepartielle{F}{t}(alpha,t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \int_\alpha^\beta \deriveepartielle{f}{t}(s,t)  ds = \deriveepartielle{F}{t}(\beta,t) - \deriveepartielle{F}{t}(\alpha,t)$">|; 

$key = q/displaystyleint_{a(t)}^{b(t)}f(s,t)ds=F(b(t),t)-F(a(t),t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \int_{a(t)}^{b(t)} f(s,t)  ds = F(b(t),t) - F(a(t),t)$">|; 

$key = q/displaystylepartial_xf(x,y)approxfrac{f(x+h,y)-f(x-h,y)}{2h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.08ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle \partial_x f(x,y) \approx \frac{f(x + h, y) - f(x - h, y)}{2 h}$">|; 

$key = q/displaystylepartial_yf(x,y)approxfrac{f(x,y+h)-f(x,y-h)}{2h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.08ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle \partial_y f(x,y) \approx \frac{f(x, y + h) - f(x, y - h)}{2 h}$">|; 

$key = q/displaystylepartial_{ij}f=partial_{ji}f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \partial_{ij} f = \partial_{ji} f$">|; 

$key = q/displaystylepartial_{xx}^2f(x,y)approxfrac{f(x+h,y)-2f(x,y)+f(x-h,y)}{h^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.08ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle \partial_{xx}^2 f(x,y) \approx \frac{f(x + h, y) - 2 f(x,y) + f(x - h, y)}{h^2}$">|; 

$key = q/displaystylepartial_{xy}^2f(x,y)approxfrac{f(x+h,y+h)-f(x+h,y-h)-f(x-h,y+h)+f(x-h,y-h)}{h^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.08ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \partial_{xy}^2 f(x,y) \approx \frac{f(x + h, y + h) - f(x + h, y - h) - f(x - h, y + h) + f(x - h, y - h)}{h^2}$">|; 

$key = q/displaystylepartial_{xy}f(x,y)=partial_{yx}f(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \partial_{xy} f(x,y) = \partial_{yx} f(x,y)$">|; 

$key = q/displaystylepartial_{xy}f_{11}=partial_{yx}f_{11};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \partial_{xy} f_{11} = \partial_{yx} f_{11}$">|; 

$key = q/displaystylepartial_{yy}^2f(x,y)approxfrac{f(x,y+h)-2f(x,y)+f(x,y-h)}{h^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.08ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \partial_{yy}^2 f(x,y) \approx \frac{f(x, y + h) - 2 f(x,y) + f(x, y - h)}{h^2}$">|; 

$key = q/displaystylephi_t(s)=f(s,t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \phi_t(s) = f(s,t)$">|; 

$key = q/f:setR^2mapstosetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img56.svg"
 ALT="$f : \setR^2 \mapsto \setR^m$">|; 

$key = q/f:setRtimessetRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img33.svg"
 ALT="$f : \setR \times \setR \mapsto \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img50.svg"
 ALT="$f$">|; 

$key = q/f_{ij}:setR^nmapstosetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img77.svg"
 ALT="$f_{ij} : \setR^n \mapsto \setR^m$">|; 

$key = q/f_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img80.svg"
 ALT="$f_{ij}$">|; 

$key = q/fincontinue^2(setR^2,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f \in \continue^2(\setR^2,\setR)$">|; 

$key = q/fincontinue^2(setR^n,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$f \in \continue^2(\setR^n,\setR)$">|; 

$key = q/h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$h$">|; 

$key = q/hinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img58.svg"
 ALT="$h \in \setR$">|; 

$key = q/hne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img68.svg"
 ALT="$h \ne 0$">|; 

$key = q/i,jin{1,2,...,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$i,j \in \{1,2,...,n\}$">|; 

$key = q/o(1)=o(h^2)slashh^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$o(1) = o(h^2)/h^2$">|; 

$key = q/o(h)=o(h^2)slashh;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$o(h) = o(h^2)/h$">|; 

$key = q/o(h^2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$o(h^2)$">|; 

$key = q/partial_x=partial_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\partial_x = \partial_1$">|; 

$key = q/partial_y=partial_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\partial_y = \partial_2$">|; 

$key = q/partial_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\partial_{ij}$">|; 

$key = q/partial_{xx}f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\partial_{xx} f$">|; 

$key = q/partial_{xy}=partial_xpartial_y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\partial_{xy} = \partial_x \partial_y$">|; 

$key = q/partial_{xy}=partial_{12};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\partial_{xy} = \partial_{12}$">|; 

$key = q/partial_{xy}f_{11}-partial_{yx}f_{11}simo(1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\partial_{xy} f_{11} - \partial_{yx} f_{11} \sim o(1)$">|; 

$key = q/partial_{yx}=partial_ypartial_x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\partial_{yx} = \partial_y \partial_x$">|; 

$key = q/partial_{yx}=partial_{21};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\partial_{yx} = \partial_{21}$">|; 

$key = q/partial_{yx}^2f(x,y)=partial_{xy}^2f(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.90ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\partial_{yx}^2 f(x,y) = \partial_{xy}^2 f(x,y)$">|; 

$key = q/partial_{yy}f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\partial_{yy} f$">|; 

$key = q/phi_t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\phi_t$">|; 

$key = q/psi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\psi$">|; 

$key = q/psi_t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\psi_t$">|; 

$key = q/s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img42.svg"
 ALT="$s$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img36.svg"
 ALT="$t$">|; 

$key = q/uinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img75.svg"
 ALT="$u \in \setR^n$">|; 

$key = q/varphi=f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\varphi = f$">|; 

$key = q/x,y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img57.svg"
 ALT="$x,y$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img63.svg"
 ALT="$y$">|; 

$key = q/{Eqts}D=f_{22}-f_{21}-f_{12}+f_{11}Delta_{yy}=(partial_{yy}f_{11}-partial_{yy}f_{21})cdoth^2Delta_{xx}=(partial_{xx}f_{11}-partial_{xx}f_{12})cdoth^2{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img15.svg"
 ALT="\begin{Eqts}
D = f_{22} - f_{21} - f_{12} + f_{11} \\\\
\Delta_{yy} = (\partial_{...
...\Delta_{xx} = (\partial_{xx} f_{11} - \partial_{xx} f_{12}) \cdot h^2
\end{Eqts}">|; 

$key = q/{Eqts}Delta_{xy}cdoth=D+Delta_{yy}+o(h^2)Delta_{yx}cdoth=D+Delta_{xx}+o(h^2){Eqts};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img14.svg"
 ALT="\begin{Eqts}
\Delta_{xy} \cdot h = D + \Delta_{yy} + o(h^2) \\\\
\Delta_{yx} \cdot h = D + \Delta_{xx} + o(h^2)
\end{Eqts}">|; 

$key = q/{Eqts}Delta_{xy}cdoth=D+o(h^2)+o(h^2)=D+o(h^2)Delta_{yx}cdoth=D+o(h^2)+o(h^2)=D+o(h^2){Eqts};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img21.svg"
 ALT="\begin{Eqts}
\Delta_{xy} \cdot h = D + o(h^2) + o(h^2) = D + o(h^2) \\\\
\Delta_{yx} \cdot h = D + o(h^2) + o(h^2) = D + o(h^2)
\end{Eqts}">|; 

$key = q/{Eqts}Delta_{xy}cdoth=partial_{xy}f_{11}cdoth^2+o(h^2)Delta_{yx}cdoth=partial_{yx}f_{11}cdoth^2+o(h^2){Eqts};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img12.svg"
 ALT="\begin{Eqts}
\Delta_{xy} \cdot h = \partial_{xy} f_{11} \cdot h^2 + o(h^2) \\\\
\Delta_{yx} \cdot h = \partial_{yx} f_{11} \cdot h^2 + o(h^2)
\end{Eqts}">|; 

$key = q/{Eqts}partial_{xy}f_{11}cdoth^2=D+o(h^2)partial_{yx}f_{11}cdoth^2=D+o(h^2){Eqts};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img22.svg"
 ALT="\begin{Eqts}
\partial_{xy} f_{11} \cdot h^2 = D + o(h^2) \\\\
\partial_{yx} f_{11} \cdot h^2 = D + o(h^2)
\end{Eqts}">|; 

$key = q/{eqnarraystar}varphi_{11}&=&varphi(x,y)varphi_{21}&=&varphi(x+h,y)varphi_{12}&=&varphi(x,y+h)varphi_{22}&=&varphi(x+h,y+h){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.32ex; " SRC="|."$dir".q|img6.svg"
 ALT="\begin{eqnarray*}
\varphi_{11} &amp;=&amp; \varphi(x,y) \\\\
\varphi_{21} &amp;=&amp; \varphi(x +...
...&amp;=&amp; \varphi(x, y + h) \\\\
\varphi_{22} &amp;=&amp; \varphi(x + h, y + h)
\end{eqnarray*}">|; 

1;

