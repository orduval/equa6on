# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(a,b)inAtimesB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$(a,b) \in A \times B$">|; 

$key = q/+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img122.svg"
 ALT="$+\infty$">|; 

$key = q/-infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img126.svg"
 ALT="$-\infty$">|; 

$key = q/A,BsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$A,B \subseteq \Omega$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$A$">|; 

$key = q/A={a_1,a_2,...,a_{n-1},a_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img105.svg"
 ALT="$A = \{a_1,a_2,...,a_{n - 1},a_n\}$">|; 

$key = q/A_1={a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img95.svg"
 ALT="$A_1 = \{a\}$">|; 

$key = q/A_2={a_1,a_2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$A_2 = \{a_1,a_2\}$">|; 

$key = q/A_{n-1}={a_1,a_2,...,a_{n-1}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$A_{n - 1} = \{a_1,a_2,...,a_{n - 1}\}$">|; 

$key = q/AcapB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img150.svg"
 ALT="$A \cap B$">|; 

$key = q/AcupB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img140.svg"
 ALT="$A \cup B$">|; 

$key = q/AsubseteqB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img130.svg"
 ALT="$A \subseteq B$">|; 

$key = q/AsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img3.svg"
 ALT="$A \subseteq \Omega$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$B$">|; 

$key = q/BenssuperieurA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img27.svg"
 ALT="$B \enssuperieur A$">|; 

$key = q/BsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img26.svg"
 ALT="$B \subseteq \Omega$">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img88.svg"
 ALT="$I$">|; 

$key = q/IinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img86.svg"
 ALT="$I \in \Omega$">|; 

$key = q/M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$M$">|; 

$key = q/M=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$M = a$">|; 

$key = q/M=maxA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img60.svg"
 ALT="$M = \max A$">|; 

$key = q/MgeA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img75.svg"
 ALT="$M \ge A$">|; 

$key = q/Mgea;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img62.svg"
 ALT="$M \ge a$">|; 

$key = q/Mgex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img161.svg"
 ALT="$M \ge x$">|; 

$key = q/Min(majorA)cup(majorB);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img159.svg"
 ALT="$M \in (\major A) \cup (\major B)$">|; 

$key = q/MinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img30.svg"
 ALT="$M \in A$">|; 

$key = q/MinmajorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img160.svg"
 ALT="$M \in \major A$">|; 

$key = q/MinmajorB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img162.svg"
 ALT="$M \in \major B$">|; 

$key = q/Mlea;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img61.svg"
 ALT="$M \le a$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\Omega$">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img81.svg"
 ALT="$S$">|; 

$key = q/SinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img79.svg"
 ALT="$S \in \Omega$">|; 

$key = q/SinmajorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img83.svg"
 ALT="$S \in \major A$">|; 

$key = q/a_1gea_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.01ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img99.svg"
 ALT="$a_1 \ge a_2$">|; 

$key = q/a_1ge{a_1,a_2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img100.svg"
 ALT="$a_1 \ge \{a_1,a_2\}$">|; 

$key = q/a_2ge{a_1,a_2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$a_2 \ge \{a_1,a_2\}$">|; 

$key = q/ageM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img31.svg"
 ALT="$a \ge M$">|; 

$key = q/ainA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img9.svg"
 ALT="$a \in A$">|; 

$key = q/ainOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img124.svg"
 ALT="$a \in \Omega$">|; 

$key = q/aleB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img15.svg"
 ALT="$a \le B$">|; 

$key = q/alea;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img96.svg"
 ALT="$a \le a$">|; 

$key = q/alem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img38.svg"
 ALT="$a \le m$">|; 

$key = q/alpha=max{sigma,a_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\alpha = \max\{\sigma,a_n\}$">|; 

$key = q/alphageA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\alpha \ge A$">|; 

$key = q/alphagea_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.01ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\alpha \ge a_n$">|; 

$key = q/alphagesigmageA_{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\alpha \ge \sigma \ge A_{n - 1}$">|; 

$key = q/alphainA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\alpha \in A$">|; 

$key = q/astrictinferieur+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img125.svg"
 ALT="$a \strictinferieur +\infty$">|; 

$key = q/astrictsuperieur-infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img127.svg"
 ALT="$a \strictsuperieur -\infty$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img77.svg"
 ALT="$b$">|; 

$key = q/beta=min{lambda,a_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\beta = \min\{\lambda,a_n\}$">|; 

$key = q/betainA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\beta \in A$">|; 

$key = q/betaleA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\beta \le A$">|; 

$key = q/betalea_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\beta \le a_n$">|; 

$key = q/betalelambdaleA_{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\beta \le \lambda \le A_{n - 1}$">|; 

$key = q/bgeA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img73.svg"
 ALT="$b \ge A$">|; 

$key = q/bgeM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img74.svg"
 ALT="$b \ge M$">|; 

$key = q/binOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img72.svg"
 ALT="$b \in \Omega$">|; 

$key = q/displaystyle(majorA)cup(majorB)subseteqmajor(AcapB);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img164.svg"
 ALT="$\displaystyle (\major A) \cup (\major B) \subseteq \major (A \cap B)$">|; 

$key = q/displaystyleAcapBneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img145.svg"
 ALT="$\displaystyle A \cap B \ne \emptyset$">|; 

$key = q/displaystyleAcapmajorA={M};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle A \cap \major A = \{ M \}$">|; 

$key = q/displaystyleAcapminorA={m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle A \cap \minor A = \{ m \}$">|; 

$key = q/displaystyleAensinferieurB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle A \ensinferieur B$">|; 

$key = q/displaystyleAenssuperieurB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle A \enssuperieur B$">|; 

$key = q/displaystyleBensinferieurA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle B \ensinferieur A$">|; 

$key = q/displaystyleBensinferieurARightarrowBsubseteqminorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle B \ensinferieur A  \Rightarrow  B \subseteq \minor A$">|; 

$key = q/displaystyleBenssuperieurARightarrowBsubseteqmajorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle B \enssuperieur A  \Rightarrow  B \subseteq \major A$">|; 

$key = q/displaystyleI=infA=inf_{ainA}a=maxminorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.57ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle I = \inf A = \inf_{a \in A} a = \max \minor A$">|; 

$key = q/displaystyleM=maxA=max_{ainA}a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.54ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle M = \max A = \max_{a \in A} a$">|; 

$key = q/displaystyleM=max{maxA,maxB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle M = \max \{ \max A , \max B \}$">|; 

$key = q/displaystyleS=minmajorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle S = \min \major A$">|; 

$key = q/displaystyleS=supA=sup_{ainA}a=minmajorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.99ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle S = \sup A = \sup_{a \in A} a = \min \major A$">|; 

$key = q/displaystylea=max{a}=min{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle a = \max\{a\} = \min\{a\}$">|; 

$key = q/displaystyleageb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle a \ge b$">|; 

$key = q/displaystylealeb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle a \le b$">|; 

$key = q/displaystylebgeMgeA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle b \ge M \ge A$">|; 

$key = q/displaystyleinf(AcapB)gemax{infA,infB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img169.svg"
 ALT="$\displaystyle \inf (A \cap B) \ge \max \{ \inf A , \inf B \}$">|; 

$key = q/displaystyleinf(AcupB)lemin{infA,infB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img158.svg"
 ALT="$\displaystyle \inf (A \cup B) \le \min \{ \inf A , \inf B \}$">|; 

$key = q/displaystyleinfA=-infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\displaystyle \inf A = -\infty$">|; 

$key = q/displaystyleinfAgeinfB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle \inf A \ge \inf B$">|; 

$key = q/displaystyleinfAlesupA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle \inf A \le \sup A$">|; 

$key = q/displaystyleinfAlexlesupA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \inf A \le x \le \sup A$">|; 

$key = q/displaystylem=minA=min_{ainA}a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.54ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle m = \min A = \min_{a \in A} a$">|; 

$key = q/displaystylem=min{maxA,maxB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\displaystyle m = \min \{ \max A , \max B \}$">|; 

$key = q/displaystylemajor(AcupB)=(majorA)cap(majorB);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle \major (A \cup B) = (\major A) \cap (\major B)$">|; 

$key = q/displaystylemajorA={minOmega:mgeA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \major A = \{ m \in \Omega : m \ge A \}$">|; 

$key = q/displaystylemajorAgeMgeA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle \major A \ge M \ge A$">|; 

$key = q/displaystylemajorAgeSgeA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle \major A \ge S \ge A$">|; 

$key = q/displaystylemajorBsubseteqmajorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\displaystyle \major B \subseteq \major A$">|; 

$key = q/displaystylemax(AcapB)lemin{maxA,maxB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle \max (A \cap B) \le \min \{ \max A , \max B \}$">|; 

$key = q/displaystylemaxAlemaxB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle \max A \le \max B$">|; 

$key = q/displaystylemaximA=Big{MinA:major{M}capA={M}Big};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \maxim A = \Big\{ M \in A : \major \{ M \} \cap A = \{ M \} \Big\}$">|; 

$key = q/displaystylemaximA={maxA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle \maxim A = \{ \max A \}$">|; 

$key = q/displaystylemax{a_1,a_2,...,a_{n-1},a_n}=maxBig{max{a_1,a_2,...,a_{n-1}},a_nBig};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle \max \{a_1,a_2,...,a_{n - 1},a_n\} = \max \Big\{ \max \{a_1,a_2,...,a_{n - 1}\} , a_n \Big\}$">|; 

$key = q/displaystylemax{maxA,maxB}=max(AcupB);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle \max \{ \max A , \max B \} = \max (A \cup B)$">|; 

$key = q/displaystylemgeA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle m \ge A$">|; 

$key = q/displaystylemin(AcapB)gemax{minA,minB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\displaystyle \min (A \cap B) \ge \max \{ \min A , \min B \}$">|; 

$key = q/displaystyleminAgeminB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\displaystyle \min A \ge \min B$">|; 

$key = q/displaystyleminBig[(majorA)cap(majorB)Big]gemax{minmajorA,minmajorB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\displaystyle \min \Big[ (\major A) \cap (\major B) \Big] \ge \max \{ \min \major A , \min \major B \}$">|; 

$key = q/displaystyleminBig[(majorA)cup(majorB)Big]=min{min(majorA),min(majorB)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img166.svg"
 ALT="$\displaystyle \min \Big[ (\major A) \cup (\major B) \Big] = \min \{ \min (\major A) , \min (\major B) \}$">|; 

$key = q/displaystyleminBig[(majorA)cup(majorB)Big]geminmajor(AcapB);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img165.svg"
 ALT="$\displaystyle \min \Big[ (\major A) \cup (\major B) \Big] \ge \min \major (A \cap B)$">|; 

$key = q/displaystyleminimA=Big{minA:minor{m}capA={m}Big};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \minim A = \Big\{ m \in A : \minor \{ m \} \cap A = \{ m \} \Big\}$">|; 

$key = q/displaystyleminimA={minA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \minim A = \{ \min A \}$">|; 

$key = q/displaystyleminmajor(AcapB)lemin{min(majorA),min(majorB)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img167.svg"
 ALT="$\displaystyle \min \major (A \cap B) \le \min \{ \min (\major A) , \min (\major B) \}$">|; 

$key = q/displaystyleminmajor(AcupB)gemax{minmajorA,minmajorB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img155.svg"
 ALT="$\displaystyle \min \major (A \cup B) \ge \max \{ \min \major A , \min \major B \}$">|; 

$key = q/displaystyleminor(AcupB)=(minorA)cap(minorB);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle \minor (A \cup B) = (\minor A) \cap (\minor B)$">|; 

$key = q/displaystyleminorA={minOmega:mleA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \minor A = \{ m \in \Omega : m \le A \}$">|; 

$key = q/displaystyleminorAleIleA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle \minor A \le I \le A$">|; 

$key = q/displaystylemin{a_1,a_2,...,a_{n-1},a_n}=minBig{min{a_1,a_2,...,a_{n-1}},a_nBig};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle \min \{a_1,a_2,...,a_{n - 1},a_n\} = \min \Big\{ \min \{a_1,a_2,...,a_{n - 1}\} , a_n \Big\}$">|; 

$key = q/displaystylemin{minA,minB}=min(AcupB);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img144.svg"
 ALT="$\displaystyle \min \{ \min A , \min B \} = \min (A \cup B)$">|; 

$key = q/displaystylemleA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle m \le A$">|; 

$key = q/displaystylesup(AcapB)lemin{supA,supB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img168.svg"
 ALT="$\displaystyle \sup (A \cap B) \le \min \{ \sup A , \sup B \}$">|; 

$key = q/displaystylesup(AcupB)gemax{supA,supB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle \sup (A \cup B) \ge \max \{ \sup A , \sup B \}$">|; 

$key = q/displaystylesupA=+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\displaystyle \sup A = +\infty$">|; 

$key = q/displaystylesupAlesupB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\displaystyle \sup A \le \sup B$">|; 

$key = q/infA=minA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\inf A = \min A$">|; 

$key = q/infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\infty$">|; 

$key = q/le;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\le$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$m$">|; 

$key = q/m=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$m = a$">|; 

$key = q/m=minA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img67.svg"
 ALT="$m = \min A$">|; 

$key = q/majorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\major A$">|; 

$key = q/majorAneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\major A \ne \emptyset$">|; 

$key = q/majorB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\major B$">|; 

$key = q/major{M}capA={M};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\major \{ M \} \cap A = \{ M \}$">|; 

$key = q/maxAlemaxB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\max A \le \max B$">|; 

$key = q/maxBlemaxA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\max B \le \max A$">|; 

$key = q/maximA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\maxim A$">|; 

$key = q/mgea;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img11.svg"
 ALT="$m \ge a$">|; 

$key = q/minA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img37.svg"
 ALT="$m \in A$">|; 

$key = q/minOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img4.svg"
 ALT="$m \in \Omega$">|; 

$key = q/minimA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\minim A$">|; 

$key = q/minorAneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\minor A \ne \emptyset$">|; 

$key = q/minor{m}capA={m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\minor \{ m \} \cap A = \{ m \}$">|; 

$key = q/mlea;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img8.svg"
 ALT="$m \le a$">|; 

$key = q/n-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img104.svg"
 ALT="$n - 1$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img106.svg"
 ALT="$n$">|; 

$key = q/supA=maxA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\sup A = \max A$">|; 

$key = q/x,yinAcapmajorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$x,y \in A \cap \major A$">|; 

$key = q/x,yinAcapminorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img54.svg"
 ALT="$x,y \in A \cap \minor A$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img46.svg"
 ALT="$x$">|; 

$key = q/x=M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img65.svg"
 ALT="$x = M$">|; 

$key = q/x=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img70.svg"
 ALT="$x = m$">|; 

$key = q/x=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$x = y$">|; 

$key = q/xgem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img69.svg"
 ALT="$x \ge m$">|; 

$key = q/xgey;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img45.svg"
 ALT="$x \ge y$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img50.svg"
 ALT="$x \in A$">|; 

$key = q/xinAcapB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img147.svg"
 ALT="$x \in A \cap B$">|; 

$key = q/xinB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img163.svg"
 ALT="$x \in B$">|; 

$key = q/xinmaximA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img63.svg"
 ALT="$x \in \maxim A$">|; 

$key = q/xinminimA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img68.svg"
 ALT="$x \in \minim A$">|; 

$key = q/xleM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img64.svg"
 ALT="$x \le M$">|; 

$key = q/xlem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img149.svg"
 ALT="$x \le m$">|; 

$key = q/xley;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img55.svg"
 ALT="$x \le y$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img49.svg"
 ALT="$y$">|; 

$key = q/ygex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img48.svg"
 ALT="$y \ge x$">|; 

$key = q/yinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img47.svg"
 ALT="$y \in A$">|; 

$key = q/yinB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img142.svg"
 ALT="$y \in B$">|; 

$key = q/ylex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img56.svg"
 ALT="$y \le x$">|; 

$key = q/{Eqts}major_leA={minOmega:mgeA}minor_leA={minOmega:mleA}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.52ex; vertical-align: -5.70ex; " SRC="|."$dir".q|img25.svg"
 ALT="\begin{Eqts}
\major_\le A = \{ m \in \Omega : m \ge A \} \ \\\\
\minor_\le A = \{ m \in \Omega : m \le A \}
\end{Eqts}">|; 

$key = q/{Eqts}max_leA=maxAmin_leA=minA{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.07ex; vertical-align: -5.48ex; " SRC="|."$dir".q|img59.svg"
 ALT="\begin{Eqts}
\max_\le A = \max A \ \\\\
\min_\le A = \min A
\end{Eqts}">|; 

$key = q/{Eqts}max_{i=1}^na_i=max{a_1,a_2,...,a_n}min_{i=1}^na_i=min{a_1,a_2,...,a_n}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.06ex; vertical-align: -5.97ex; " SRC="|."$dir".q|img121.svg"
 ALT="\begin{Eqts}
\max_{i = 1}^n a_i = \max \{ a_1,a_2,...,a_n \} \ \\\\
\min_{i = 1}^n a_i = \min \{ a_1,a_2,...,a_n \}
\end{Eqts}">|; 

$key = q/{Eqts}maxim_leAminim_leA{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.07ex; vertical-align: -5.48ex; " SRC="|."$dir".q|img43.svg"
 ALT="\begin{Eqts}
\maxim_\le A \ \\\\
\minim_\le A
\end{Eqts}">|; 

$key = q/{Eqts}max{a_1,a_2}=cases{a_1&text{si}a_1gea_2a_2&text{sinon}cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img102.svg"
 ALT="\begin{Eqts}
\max\{a_1,a_2\} =
\begin{cases}
a_1 &amp; \text{ si } a_1 \ge a_2 \\\\
a_2 &amp; \text{ sinon}
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}min{a_1,a_2}=cases{a_1&text{si}a_1lea_2a_2&text{sinon}cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img103.svg"
 ALT="\begin{Eqts}
\min\{a_1,a_2\} =
\begin{cases}
a_1 &amp; \text{ si } a_1 \le a_2 \\\\
a_2 &amp; \text{ sinon}
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}sigma=max{a_1,a_2,...,a_{n-1}}lambda=min{a_1,a_2,...,a_{n-1}}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img108.svg"
 ALT="\begin{Eqts}
\sigma = \max \{a_1,a_2,...,a_{n - 1}\} \\\\
\lambda = \min \{a_1,a_2,...,a_{n - 1}\}
\end{Eqts}">|; 

$key = q/{Eqts}sup_leA=minmajorAinf_leA=maxminorA{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.52ex; vertical-align: -5.70ex; " SRC="|."$dir".q|img90.svg"
 ALT="\begin{Eqts}
\sup_\le A = \min \major A \ \\\\
\inf_\le A = \max \minor A
\end{Eqts}">|; 

$key = q/{Eqts}xinARightarrowxlemaxAxinBRightarrowxlemaxB{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img148.svg"
 ALT="\begin{Eqts}
x \in A  \Rightarrow  x \le \max A \\\\
x \in B  \Rightarrow  x \le \max B
\end{Eqts}">|; 

$key = q/{Eqts}xlemaxAleMylemaxBleM{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img141.svg"
 ALT="\begin{Eqts}
x \le \max A \le M \\\\
y \le \max B \le M
\end{Eqts}">|; 

1;

