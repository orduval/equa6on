# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 36.86ex; " SRC="|."$dir".q|img63.svg"
 ALT="\begin{eqnarray*}
\esperof{G(X)} &amp;=&amp; \int_\Omega (G \circ X)(\omega)  d\proba(\...
...ega_i}  d\proba(\omega) \\\\
&amp;=&amp; \sum_i g_i  \probaof{\Omega_i}
\end{eqnarray*}">|; 

$key = q/(GcircX)(omega)=g_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$(G \circ X)(\omega) = g_i$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img37.svg"
 ALT="$0$">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img36.svg"
 ALT="$1$">|; 

$key = q/A,B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img160.svg"
 ALT="$A,B$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img176.svg"
 ALT="$A$">|; 

$key = q/A=AcupOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img183.svg"
 ALT="$A = A \cup \Omega$">|; 

$key = q/A_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img57.svg"
 ALT="$A_i$">|; 

$key = q/AsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img185.svg"
 ALT="$A \subseteq \Omega$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img161.svg"
 ALT="$B$">|; 

$key = q/B=Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img177.svg"
 ALT="$B = \Omega$">|; 

$key = q/B_1,...,B_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img182.svg"
 ALT="$B_1,...,B_n$">|; 

$key = q/BcapB=B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img163.svg"
 ALT="$B \cap B = B$">|; 

$key = q/BsubseteqC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img166.svg"
 ALT="$B \subseteq C$">|; 

$key = q/BsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img188.svg"
 ALT="$B \subseteq \Omega$">|; 

$key = q/C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img165.svg"
 ALT="$C$">|; 

$key = q/CcapB=B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img167.svg"
 ALT="$C \cap B = B$">|; 

$key = q/DeltasubseteqGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img254.svg"
 ALT="$\Delta \subseteq \Gamma$">|; 

$key = q/DsubseteqsetR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.34ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img22.svg"
 ALT="$D \subseteq\setR^2$">|; 

$key = q/G(X(omega),Y(omega))=g_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img87.svg"
 ALT="$G(X(\omega), Y(\omega)) = g_i$">|; 

$key = q/G(X);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$G(X)$">|; 

$key = q/G(X,Y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img88.svg"
 ALT="$G(X,Y)$">|; 

$key = q/G(X,Y)=X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$G(X,Y) = X$">|; 

$key = q/G(X,Y)=aX+bY;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$G(X,Y) = a  X + b  Y$">|; 

$key = q/G(X,Y)insetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$G(X,Y) \in \setR$">|; 

$key = q/G:setR^2mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img49.svg"
 ALT="$G : \setR^2 \mapsto \setR$">|; 

$key = q/G:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img46.svg"
 ALT="$G : \setR \mapsto \setR$">|; 

$key = q/G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img66.svg"
 ALT="$G$">|; 

$key = q/G=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img69.svg"
 ALT="$G = \identite$">|; 

$key = q/Gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img238.svg"
 ALT="$\Gamma$">|; 

$key = q/Gamma=Lambda(Y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img264.svg"
 ALT="$\Gamma = \Lambda(Y)$">|; 

$key = q/Gammasubseteqsousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img216.svg"
 ALT="$\Gamma \subseteq \sousens(\Omega)$">|; 

$key = q/GcircX:OmegamapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img47.svg"
 ALT="$G \circ X : \Omega \mapsto \setR$">|; 

$key = q/I:mathcal{F}(Gamma)mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img220.svg"
 ALT="$I : \mathcal{F}(\Gamma) \mapsto \setR$">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img222.svg"
 ALT="$I$">|; 

$key = q/Lambda(W);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img219.svg"
 ALT="$\Lambda(W)$">|; 

$key = q/Lambda(X);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\Lambda(X)$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\Omega$">|; 

$key = q/Omega_1,...,Omega_N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img190.svg"
 ALT="$\Omega_1, ..., \Omega_N$">|; 

$key = q/OmegainGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img247.svg"
 ALT="$\Omega \in \Gamma$">|; 

$key = q/OmegasetminusPhiinGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img242.svg"
 ALT="$\Omega \setminus \Phi \in \Gamma$">|; 

$key = q/Phi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\Phi$">|; 

$key = q/Phi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\Phi_i$">|; 

$key = q/Phi_i^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img204.svg"
 ALT="$\Phi_i^+$">|; 

$key = q/Phi_i^+cupPhi_i^-=Omega_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img196.svg"
 ALT="$\Phi_i^+ \cup \Phi_i^- = \Omega_i$">|; 

$key = q/PhiinGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img241.svg"
 ALT="$\Phi \in \Gamma$">|; 

$key = q/PhisubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\Phi \subseteq \Omega$">|; 

$key = q/Theta(X,U);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\Theta(X,U)$">|; 

$key = q/Theta(indicatrice_Phi,U)=Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\Theta(\indicatrice_\Phi,U) = \Omega$">|; 

$key = q/Theta(indicatrice_Phi,U)=OmegasetminusPhi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\Theta(\indicatrice_\Phi,U) = \Omega \setminus \Phi$">|; 

$key = q/Theta(indicatrice_Phi,U)=Phi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\Theta(\indicatrice_\Phi,U) = \Phi$">|; 

$key = q/Theta(indicatrice_Phi,U)=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\Theta(\indicatrice_\Phi,U) = \emptyset$">|; 

$key = q/UsubseteqsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img18.svg"
 ALT="$U \subseteq \setR$">|; 

$key = q/W,X,Y,Z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img132.svg"
 ALT="$W,X,Y,Z$">|; 

$key = q/W;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img134.svg"
 ALT="$W$">|; 

$key = q/W=Z_1-Z_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img235.svg"
 ALT="$W = Z_1 - Z_2$">|; 

$key = q/Winmathcal{F}(Delta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img255.svg"
 ALT="$W \in \mathcal{F}(\Delta)$">|; 

$key = q/Winmathcal{F}(Gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img225.svg"
 ALT="$W \in \mathcal{F}(\Gamma)$">|; 

$key = q/X,Y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img82.svg"
 ALT="$X, Y$">|; 

$key = q/X:OmegamapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$X : \Omega \mapsto \setR$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img14.svg"
 ALT="$X$">|; 

$key = q/X=(X_1,...,X_N)^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img106.svg"
 ALT="$X = (X_1, ..., X_N)^T$">|; 

$key = q/X=indicatrice_Phi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img34.svg"
 ALT="$X = \indicatrice_\Phi$">|; 

$key = q/X^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$X^{-1}$">|; 

$key = q/X_0,Y_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img155.svg"
 ALT="$X_0, Y_0$">|; 

$key = q/X_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img139.svg"
 ALT="$X_0$">|; 

$key = q/X_0essegal0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img153.svg"
 ALT="$X_0 \essegal 0$">|; 

$key = q/X_1,...,X_N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img104.svg"
 ALT="$X_1, ..., X_N$">|; 

$key = q/Y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$Y$">|; 

$key = q/Z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img135.svg"
 ALT="$Z$">|; 

$key = q/Z=left(X-esperof{X}right)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img121.svg"
 ALT="$Z = \left(X-\esperof{X}\right)^2$">|; 

$key = q/Z^*;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img224.svg"
 ALT="$Z^*$">|; 

$key = q/Z_1,Z_2inmathcal{F}(Gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img231.svg"
 ALT="$Z_1, Z_2 \in \mathcal{F}(\Gamma)$">|; 

$key = q/Z_1-Z_2inmathcal{F}(Gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img234.svg"
 ALT="$Z_1 - Z_2 \in \mathcal{F}(\Gamma)$">|; 

$key = q/Z_1=Z_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img237.svg"
 ALT="$Z_1 = Z_2$">|; 

$key = q/Zinmathcal{F}(Gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img250.svg"
 ALT="$Z \in \mathcal{F}(\Gamma)$">|; 

$key = q/[0,1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$[0,1]$">|; 

$key = q/a,b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img143.svg"
 ALT="$a,b$">|; 

$key = q/a,binsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img53.svg"
 ALT="$a,b \in \setR$">|; 

$key = q/aX+bY;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img144.svg"
 ALT="$a  X + b  Y$">|; 

$key = q/ainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img126.svg"
 ALT="$a \in \setR$">|; 

$key = q/bigcup_iPhi_i^+=B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.76ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img203.svg"
 ALT="$\bigcup_i \Phi_i^+ = B$">|; 

$key = q/cov{X_0}{X_0}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\cov{X_0}{X_0} = 0$">|; 

$key = q/displaystyle(X,Y)(B_y)={(x,y)insetR^2:xinsetR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.79ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img209.svg"
 ALT="$\displaystyle (X,Y)(B_y) = \{ (x,y) \in \setR^2 : x \in \setR \}$">|; 

$key = q/displaystyleB_y={omega:Y(omega)=y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img207.svg"
 ALT="$\displaystyle B_y = \{ \omega : Y(\omega) = y \}$">|; 

$key = q/displaystyleG(x)=sum_ig_iindicatrice_{A_i}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle G(x) = \sum_i g_i  \indicatrice_{A_i}(x)$">|; 

$key = q/displaystyleG(x,y)=sum_ig_iindicatrice_{A_i}(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle G(x,y) = \sum_i g_i  \indicatrice_{A_i}(x,y)$">|; 

$key = q/displaystyleI(Y)=int_Omega(Y-Z)^2dprobage0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img252.svg"
 ALT="$\displaystyle I(Y) = \int_\Omega (Y - Z)^2  d\proba \ge 0$">|; 

$key = q/displaystyleI(Z)=int_Omegaleft[Z(omega)-X(omega)right]^2dproba(omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img221.svg"
 ALT="$\displaystyle I(Z) = \int_\Omega \left[ Z(\omega) - X(\omega) \right]^2  d\proba(\omega)$">|; 

$key = q/displaystyleJ_W(epsilon)=I(Z^*+epsilonW)=int_Omega(Z^*+epsilonW-X)^2dproba;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img223.svg"
 ALT="$\displaystyle J_W(\epsilon) = I(Z^* + \epsilon W) = \int_\Omega (Z^* + \epsilon W - X)^2  d\proba$">|; 

$key = q/displaystyleLambda(W)subseteqDeltasubseteqGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img256.svg"
 ALT="$\displaystyle \Lambda(W) \subseteq \Delta \subseteq \Gamma$">|; 

$key = q/displaystyleLambda(W)subseteqGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img218.svg"
 ALT="$\displaystyle \Lambda(W) \subseteq \Gamma$">|; 

$key = q/displaystyleLambda(W)subseteqLambda(Y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img265.svg"
 ALT="$\displaystyle \Lambda(W) \subseteq \Lambda(Y)$">|; 

$key = q/displaystyleLambda(X)={Theta(X,U):UsubseteqsetR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \Lambda(X) = \{ \Theta(X,U) : U \subseteq \setR \}$">|; 

$key = q/displaystyleLambda(indicatrice_Phi)={emptyset,Omega,Phi,OmegasetminusPhi};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \Lambda(\indicatrice_\Phi) = \{ \emptyset, \Omega, \Phi, \Omega \setminus \Phi \}$">|; 

$key = q/displaystyleLambda(indicatrice_Phi)={emptyset,Omega,Phi,OmegasetminusPhi}subseteqGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img243.svg"
 ALT="$\displaystyle \Lambda(\indicatrice_\Phi) = \{ \emptyset, \Omega, \Phi, \Omega \setminus \Phi \} \subseteq \Gamma$">|; 

$key = q/displaystyleOD{J_W}{epsilon}(0)=int_Omega2(Z^*-X)Wdproba=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.46ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img229.svg"
 ALT="$\displaystyle \OD{J_W}{\epsilon}(0) = \int_\Omega 2 (Z^* - X) W  d\proba = 0$">|; 

$key = q/displaystyleOD{J_W}{epsilon}(epsilon)=int_Omega2(Z^*+epsilonW-X)Wdproba=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.46ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img227.svg"
 ALT="$\displaystyle \OD{J_W}{\epsilon}(\epsilon) = \int_\Omega 2 (Z^* +\epsilon W - X) W  d\proba = 0$">|; 

$key = q/displaystyleOD{Psi}{u}(0)=esperof{X^kexp(0)}=esperof{X^k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle \OD{\Psi}{u}(0) = \esperof{X^k  \exp(0)} = \esperof{X^k}$">|; 

$key = q/displaystyleOmega={omega_i:iinsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img266.svg"
 ALT="$\displaystyle \Omega = \{ \omega_i : i \in \setN \}$">|; 

$key = q/displaystyleOmega_i=X^{-1}(A_i)={omegainOmega:X(omega)inA_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \Omega_i = X^{-1}(A_i) = \{ \omega \in \Omega : X(\omega) \in A_i \}$">|; 

$key = q/displaystyleOmega_i={omegainOmega:(X(omega),Y(omega))inA_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle \Omega_i = \{ \omega \in \Omega : (X(\omega), Y(\omega)) \in A_i \}$">|; 

$key = q/displaystylePsi(u)=exp(umu+unsur{2}u^2sigma^2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\displaystyle \Psi(u) = \exp(u \mu + \unsur{2} u^2 \sigma^2)$">|; 

$key = q/displaystylePsi(u)=unsur{sqrt{2pi}sigma}int_setRexp(xu)expleft(-frac{(x-mu)^2}{2sigma^2}right)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle \Psi(u) = \unsur{\sqrt{2 \pi} \sigma} \int_\setR \exp(x u) \exp\left(-\frac{(x-\mu)^2}{2 \sigma^2}\right) dx$">|; 

$key = q/displaystylePsi_X(u)=esperof{exp(Xcdotu)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle \Psi_X(u) = \esperof{\exp(X \cdot u)}$">|; 

$key = q/displaystylePsi_X(u)=expleft(u^Tcdotmu+unsur{2}u^TcdotTheta^{-1}cdoturight);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle \Psi_X(u) = \exp\left(u^T \cdot \mu + \unsur{2} u^T \cdot \Theta^{-1} \cdot u\right)$">|; 

$key = q/displaystyleTheta(X,U)=X^{-1}(U);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \Theta(X,U) = X^{-1}(U)$">|; 

$key = q/displaystyleTheta(X,U)={omegainOmega:X(omega)inU};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \Theta(X,U) = \{ \omega \in \Omega : X(\omega) \in U \}$">|; 

$key = q/displaystyleX_0=X-esperof{X};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle X_0 = X - \esperof{X}$">|; 

$key = q/displaystyleY=X+a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.95ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\displaystyle Y = X + a$">|; 

$key = q/displaystyleZ(omega)=sum_iZ_iindicatrice_{Omega_i}(omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img191.svg"
 ALT="$\displaystyle Z(\omega) = \sum_i Z_i  \indicatrice_{\Omega_i}(\omega)$">|; 

$key = q/displaystyleZ(omega)=sum_iZ_iindicatrice_{Phi_i^+}(omega)+sum_iZ_iindicatrice_{Phi_i^-}(omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img198.svg"
 ALT="$\displaystyle Z(\omega) = \sum_i Z_i \indicatrice_{\Phi_i^+}(\omega) + \sum_i Z_i \indicatrice_{\Phi_i^-}(\omega)$">|; 

$key = q/displaystylecov{X+a}{Y+b}=cov{X}{Y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle \cov{X+a}{Y+b} = \cov{X}{Y}$">|; 

$key = q/displaystylecov{X_0}{X_0}=esperof{X_0^2}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img150.svg"
 ALT="$\displaystyle \cov{X_0}{X_0} = \esperof{X_0^2} \ge 0$">|; 

$key = q/displaystylecov{X_0}{Y_0}=esperof{X_0Y_0}-esperof{X_0}esperof{Y_0}=esperof{X_0Y_0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle \cov{X_0}{Y_0} = \esperof{X_0  Y_0} - \esperof{X_0}  \esperof{Y_0} = \esperof{X_0  Y_0}$">|; 

$key = q/displaystylecov{X}{Y}=cov{X_0}{Y_0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle \cov{X}{Y} = \cov{X_0}{Y_0}$">|; 

$key = q/displaystylecov{X}{Y}=esperof{(X-esperof{X})cdot(Y-esperof{Y})};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\displaystyle \cov{X}{Y} = \esperof{(X-\esperof{X}) \cdot (Y-\esperof{Y})}$">|; 

$key = q/displaystylecov{X}{Y}^2lecov{X}{X}cov{Y}{Y}=var{X}var{Y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle \cov{X}{Y}^2 \le \cov{X}{X}  \cov{Y}{Y} = \var{X}  \var{Y}$">|; 

$key = q/displaystylecov{X}{Y}lesqrt{var{X}var{Y}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img158.svg"
 ALT="$\displaystyle \cov{X}{Y} \le \sqrt{\var{X}  \var{Y}}$">|; 

$key = q/displaystylecov{Y_0}{X_0}=esperof{Y_0cdotX_0}=esperof{X_0cdotY_0}=cov{X_0}{Y_0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img149.svg"
 ALT="$\displaystyle \cov{Y_0}{X_0} = \esperof{Y_0 \cdot X_0} = \esperof{X_0 \cdot Y_0} = \cov{X_0}{Y_0}$">|; 

$key = q/displaystyleesperof{1}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle \esperof{1} = 1$">|; 

$key = q/displaystyleesperof{G(X)}=int_Omega(GcircX)(omega)dproba(omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \esperof{G(X)} = \int_\Omega (G \circ X)(\omega)  d\proba(\omega)$">|; 

$key = q/displaystyleesperof{G(X)}=int_setRG(x)dmathcal{L}_X(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \esperof{G(X)} = \int_\setR G(x)  d\mathcal{L}_X(x)$">|; 

$key = q/displaystyleesperof{G(X)}=int_setRG(x)f_X(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \esperof{G(X)} = \int_\setR G(x)  f_X(x)  dx$">|; 

$key = q/displaystyleesperof{G(X)}=sum_ig_imathcal{L}_X(A_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle \esperof{G(X)} = \sum_i g_i  \mathcal{L}_X(A_i)$">|; 

$key = q/displaystyleesperof{G(X,Y)}=int_OmegaGleft(X(omega),Y(omega)right)dproba(omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \esperof{G(X,Y)} = \int_\Omega G\left(X(\omega),Y(\omega)\right)  d\proba(\omega)$">|; 

$key = q/displaystyleesperof{G(X,Y)}=int_{setR^2}G(x,y)dmathcal{L}_{X,Y}(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle \esperof{G(X,Y)} = \int_{\setR^2} G(x,y)  d\mathcal{L}_{X,Y}(x,y)$">|; 

$key = q/displaystyleesperof{G(X,Y)}=int_{setR^2}G(x,y)f_{X,Y}(x,y)dxdy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle \esperof{G(X,Y)} = \int_{\setR^2} G(x,y)  f_{X,Y}(x,y)  dx  dy$">|; 

$key = q/displaystyleesperof{G(X,Y)}=sum_ig_imathcal{L}_{X,Y}(A_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \esperof{G(X,Y)} = \sum_i g_i  \mathcal{L}_{X,Y}(A_i)$">|; 

$key = q/displaystyleesperof{X_0}=esperof{X-esperof{X}}=esperof{X}-esperof{X}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle \esperof{X_0} = \esperof{X - \esperof{X} } = \esperof{X} - \esperof{X} = 0$">|; 

$key = q/displaystyleesperof{X|B}=frac{int_BXdproba}{int_Bdproba};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.18ex; vertical-align: -2.52ex; " SRC="|."$dir".q|img206.svg"
 ALT="$\displaystyle \esperof{X \vert B} = \frac{ \int_B X  d\proba }{ \int_B  d\proba }$">|; 

$key = q/displaystyleesperof{X|Gamma}=argmin_{Zinmathcal{F}(Gamma)}int_{Omega}left[Z-Xright]^2dproba;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.50ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img239.svg"
 ALT="$\displaystyle \esperof{X \vert \Gamma} = \arg\min_{Z \in \mathcal{F}(\Gamma) } \int_{\Omega} \left[ Z - X \right]^2  d\proba$">|; 

$key = q/displaystyleesperof{X|Y=y}=esperof{X|B_y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img208.svg"
 ALT="$\displaystyle \esperof{X \vert Y = y} = \esperof{X \vert B_y}$">|; 

$key = q/displaystyleesperof{X|Y=y}=frac{int_setRxf_{X,Y}(x,y)dx}{int_setRf_{X,Y}(x,y)dx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.18ex; vertical-align: -2.52ex; " SRC="|."$dir".q|img213.svg"
 ALT="$\displaystyle \esperof{X \vert Y = y} = \frac{\int_\setR x  f_{X,Y}(x,y)  dx}{\int_\setR f_{X,Y}(x,y)  dx}$">|; 

$key = q/displaystyleesperof{X|Y=y}=int_setRxf_{X|Y}(x,y)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img215.svg"
 ALT="$\displaystyle \esperof{X \vert Y = y} = \int_\setR x  f_{X \vert Y}(x,y)  dx$">|; 

$key = q/displaystyleesperof{X|Y}=esperof{X|Lambda(Y)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img263.svg"
 ALT="$\displaystyle \esperof{X \vert Y} = \esperof{X \vert \Lambda(Y)}$">|; 

$key = q/displaystyleesperof{X}=int_setRxdmathcal{L}_X(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle \esperof{X} = \int_\setR x  d\mathcal{L}_X(x)$">|; 

$key = q/displaystyleesperof{X}=int_setRxf_X(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \esperof{X} = \int_\setR x  f_X(x)  dx$">|; 

$key = q/displaystyleesperof{X}=int_{Omega}X(omega)dproba(omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \esperof{X} = \int_{\Omega} X(\omega)  d\proba(\omega)$">|; 

$key = q/displaystyleesperof{X}=sum_ix_ip_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img270.svg"
 ALT="$\displaystyle \esperof{X} = \sum_i x_i  p_i$">|; 

$key = q/displaystyleesperof{Y}=int_setRyf_Y(y)dy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle \esperof{Y} = \int_\setR y  f_Y(y)  dy$">|; 

$key = q/displaystyleesperof{Z|B}=frac{int_BZdproba}{int_Bdproba};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.18ex; vertical-align: -2.52ex; " SRC="|."$dir".q|img205.svg"
 ALT="$\displaystyle \esperof{Z \vert B} = \frac{ \int_B Z  d\proba }{ \int_B  d\proba }$">|; 

$key = q/displaystyleesperof{Z|B}=unsur{probaof{B}}left[sum_iZ_iprobaof{Phi_i^+capB}+sum_iZ_iprobaof{Phi_i^-capB}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img199.svg"
 ALT="$\displaystyle \esperof{Z \vert B} = \unsur{ \probaof{B} } \left[ \sum_i Z_i \probaof{\Phi_i^+ \cap B} + \sum_i Z_i \probaof{\Phi_i^- \cap B} \right]$">|; 

$key = q/displaystyleesperof{Z|B}=unsur{probaof{B}}sum_iZ_iprobaof{Omega_icapB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img194.svg"
 ALT="$\displaystyle \esperof{Z \vert B} = \unsur{ \probaof{B} } \sum_i Z_i \probaof{\Omega_i \cap B}$">|; 

$key = q/displaystyleesperof{Z|B}=unsur{probaof{B}}sum_iZ_iprobaof{Phi_i^+};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img202.svg"
 ALT="$\displaystyle \esperof{Z \vert B} = \unsur{ \probaof{B} } \sum_i Z_i \probaof{\Phi_i^+}$">|; 

$key = q/displaystyleesperof{Z|Gamma}=Z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img253.svg"
 ALT="$\displaystyle \esperof{Z \vert \Gamma} = Z$">|; 

$key = q/displaystyleesperof{aX+bY}=aesperof{X}+besperof{Y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \esperof{a  X + b  Y} = a  \esperof{X} + b  \esperof{Y}$">|; 

$key = q/displaystyleesperof{esperof{X|Gamma}|Delta}=esperof{X|Delta};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img262.svg"
 ALT="$\displaystyle \esperof{ \esperof{X \vert \Gamma} \vert \Delta } = \esperof{X \vert \Delta}$">|; 

$key = q/displaystyleesperof{esperof{X|Gamma}}=esperof{X};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img249.svg"
 ALT="$\displaystyle \esperof{ \esperof{X \vert \Gamma} } = \esperof{X}$">|; 

$key = q/displaystyleesperof{indicatrice_A|B}=probaof{A|B};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img187.svg"
 ALT="$\displaystyle \esperof{\indicatrice_A \vert B} = \probaof{A \vert B}$">|; 

$key = q/displaystyleesperof{indicatrice_A}=probaof{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img186.svg"
 ALT="$\displaystyle \esperof{\indicatrice_A} = \probaof{A}$">|; 

$key = q/displaystyleesperof{indicatrice_Phi}=probaof{Phi};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \esperof{\indicatrice_\Phi} = \probaof{\Phi}$">|; 

$key = q/displaystylef_X(x)=int_setRf_{X,Y}(x,y)dy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\displaystyle f_X(x) = \int_\setR f_{X,Y}(x,y)  dy$">|; 

$key = q/displaystylef_X(x)=unsur{2pi^{nslash2}det{A}}expleft(-unsur{2}(x-mu)^TcdotTheta^{-1}cdot(x-mu)right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\displaystyle f_X(x) = \unsur{2 \pi^{n/2} \det{A}} \exp\left(-\unsur{2} (x-\mu)^T \cdot \Theta^{-1} \cdot (x-\mu) \right)$">|; 

$key = q/displaystylef_Y(y)=int_setRf_{X,Y}(x,y)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle f_Y(y) = \int_\setR f_{X,Y}(x,y)  dx$">|; 

$key = q/displaystylef_{X|Y}(x,y)=frac{f_{X,Y}(x,y)}{int_setRf_{X,Y}(x,y)dx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.91ex; vertical-align: -2.52ex; " SRC="|."$dir".q|img214.svg"
 ALT="$\displaystyle f_{X \vert Y}(x,y) = \frac{f_{X,Y}(x,y)}{ \int_\setR f_{X,Y}(x,y)  dx}$">|; 

$key = q/displaystylef_{X}(x)=frac{1}{sigmasqrt{2pi}}expleft(-frac{(x-mu)^2}{2sigma^2}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle f_{X}(x) = \frac{1}{ \sigma\sqrt{2 \pi} } \exp\left(-\frac{(x-\mu)^2}{2 \sigma^2}\right)$">|; 

$key = q/displaystylefrac{d^kPsi_X}{du^k}(u)=esperof{X^kexp(Xcdotu)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.26ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle \frac{d^k \Psi_X}{du^k}(u) = \esperof{X^k  \exp(X \cdot u)}$">|; 

$key = q/displaystyleint_Omega(Z-Z)^2dproba=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img251.svg"
 ALT="$\displaystyle \int_\Omega (Z - Z)^2  d\proba = 0$">|; 

$key = q/displaystyleint_Omega(Z_1-Z_2)^2dproba=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img236.svg"
 ALT="$\displaystyle \int_\Omega (Z_1 - Z_2)^2  d\proba = 0$">|; 

$key = q/displaystyleint_OmegaW(Z_1-Z_2)dproba=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img233.svg"
 ALT="$\displaystyle \int_\Omega W (Z_1 - Z_2)  d\proba = 0$">|; 

$key = q/displaystyleint_OmegaWZ^*dproba=int_OmegaWXdproba;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img230.svg"
 ALT="$\displaystyle \int_\Omega W Z^*  d\proba = \int_\Omega W X  d\proba$">|; 

$key = q/displaystyleint_OmegaWZ_1dproba=int_OmegaWZ_2dproba=int_OmegaWXdproba;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img232.svg"
 ALT="$\displaystyle \int_\Omega W Z_1  d\proba = \int_\Omega W Z_2  d\proba = \int_\Omega W X  d\proba$">|; 

$key = q/displaystyleint_OmegaWesperof{X|Delta}dproba=int_OmegaWesperof{X|Gamma}dproba;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img258.svg"
 ALT="$\displaystyle \int_\Omega W \esperof{X \vert \Delta}  d\proba = \int_\Omega W \esperof{X \vert \Gamma}  d\proba$">|; 

$key = q/displaystyleint_OmegaWesperof{X|Gamma}dproba=int_OmegaWXdproba;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img240.svg"
 ALT="$\displaystyle \int_\Omega W \esperof{X \vert \Gamma}  d\proba = \int_\Omega W X  d\proba$">|; 

$key = q/displaystyleint_OmegaX_0^2dproba(omega)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\displaystyle \int_\Omega X_0^2  d\proba(\omega) = 0$">|; 

$key = q/displaystyleint_Omegaesperof{X|Gamma}dproba=int_OmegaXdproba;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img248.svg"
 ALT="$\displaystyle \int_\Omega \esperof{X \vert \Gamma}  d\proba = \int_\Omega X  d\proba$">|; 

$key = q/displaystyleint_Omegaindicatrice_Phiesperof{X|Gamma}dproba=int_Omegaindicatrice_PhiXdproba;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img245.svg"
 ALT="$\displaystyle \int_\Omega \indicatrice_\Phi  \esperof{X \vert \Gamma}  d\proba = \int_\Omega \indicatrice_\Phi  X  d\proba$">|; 

$key = q/displaystyleint_Phiesperof{X|Gamma}dproba=int_PhiXdproba;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img246.svg"
 ALT="$\displaystyle \int_\Phi \esperof{X \vert \Gamma}  d\proba = \int_\Phi X  d\proba$">|; 

$key = q/displaystyleint_setRf_X(x)dx=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle \int_\setR f_X(x)  dx = 1$">|; 

$key = q/displaystyleint_{B_y}dproba=int_setRf_{X,Y}(x,y)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.85ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img212.svg"
 ALT="$\displaystyle \int_{B_y}  d\proba = \int_\setR f_{X,Y}(x,y)  dx$">|; 

$key = q/displaystylemathcal{L}_X(A_i)=probaof{X^{-1}(A_i)}=probaof{Omega_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle \mathcal{L}_X(A_i) = \probaof{X^{-1}(A_i)} = \probaof{\Omega_i}$">|; 

$key = q/displaystylemathcal{L}_X(U)=probaof{X^{-1}(U)}=probaof{{omegainOmega:X(omega)inU}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \mathcal{L}_X(U) = \probaof{X^{-1}(U)} = \probaof{ \{ \omega\in\Omega : X(\omega) \in U \} }$">|; 

$key = q/displaystylemathcal{L}_{X,Y}(A_i)=probaof{Omega_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle \mathcal{L}_{X,Y}(A_i) = \probaof{\Omega_i}$">|; 

$key = q/displaystylemathcal{L}_{X,Y}(D)=probaof{{omegainOmega:(X(omega),Y(omega))inD}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \mathcal{L}_{X,Y}(D) = \probaof{ \{ \omega\in\Omega : (X(\omega),Y(\omega)) \in D \} }$">|; 

$key = q/displaystylep_i=probaof{{omega_i}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img268.svg"
 ALT="$\displaystyle p_i = \probaof{ \{\omega_i\} }$">|; 

$key = q/displaystyleproba:mathcal{S}mapsto[0,1]quad;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \proba : \mathcal{S} \mapsto [0,1] \quad$">|; 

$key = q/displaystyleproba_Bleft[Aright]=probaof{A|B};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img174.svg"
 ALT="$\displaystyle \proba_B\left[ A \right] = \probaof{A \vert B}$">|; 

$key = q/displaystyleprobaof{AcapB}=probaof{A}cdotprobaof{B};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img180.svg"
 ALT="$\displaystyle \probaof{A \cap B} = \probaof{A} \cdot \probaof{B}$">|; 

$key = q/displaystyleprobaof{AcapB}leprobaof{B};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img169.svg"
 ALT="$\displaystyle \probaof{A \cap B} \le \probaof{B}$">|; 

$key = q/displaystyleprobaof{A|B}=frac{probaof{AcapB}}{probaof{B}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img159.svg"
 ALT="$\displaystyle \probaof{A \vert B} = \frac{ \probaof{A \cap B} }{ \probaof{B} }$">|; 

$key = q/displaystyleprobaof{A|B}=probaof{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img179.svg"
 ALT="$\displaystyle \probaof{A \vert B} = \probaof{A}$">|; 

$key = q/displaystyleprobaof{A|B}geprobaof{AcapB}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img172.svg"
 ALT="$\displaystyle \probaof{A \vert B} \ge \probaof{A \cap B} \ge 0$">|; 

$key = q/displaystyleprobaof{A|B}le1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img170.svg"
 ALT="$\displaystyle \probaof{A \vert B} \le 1$">|; 

$key = q/displaystyleprobaof{A|Omega}=probaof{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img178.svg"
 ALT="$\displaystyle \probaof{A \vert \Omega} = \probaof{A}$">|; 

$key = q/displaystyleprobaof{A}=sum_iprobaof{AcapB_i}=sum_iprobaof{A|B_i}cdotprobaof{B_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img184.svg"
 ALT="$\displaystyle \probaof{A} = \sum_i \probaof{A \cap B_i} = \sum_i \probaof{A \vert B_i} \cdot \probaof{B_i}$">|; 

$key = q/displaystyleprobaof{B|B}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img164.svg"
 ALT="$\displaystyle \probaof{ B \vert B } = 1$">|; 

$key = q/displaystyleprobaof{B}>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img162.svg"
 ALT="$\displaystyle \probaof{B} &gt; 0$">|; 

$key = q/displaystyleprobaof{C|B}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img168.svg"
 ALT="$\displaystyle \probaof{ C \vert B } = 1$">|; 

$key = q/displaystyleprobaof{Omega_i|B}=frac{probaof{Omega_icapB}}{probaof{B}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img193.svg"
 ALT="$\displaystyle \probaof{\Omega_i \vert B} = \frac{ \probaof{\Omega_i \cap B} }{ \probaof{B} }$">|; 

$key = q/displaystyleprobaof{Omega}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \probaof{\Omega} = 1$">|; 

$key = q/displaystyleprobaof{Phi}=probaof{Phicupemptyset}=probaof{Phi}+probaof{emptyset};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \probaof{\Phi} = \probaof{\Phi \cup \emptyset} = \probaof{\Phi} + \probaof{\emptyset}$">|; 

$key = q/displaystyleprobaof{bigcup_iPhi_i}=sum_iprobaof{Phi_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \probaof{\bigcup_i \Phi_i} = \sum_i \probaof{\Phi_i}$">|; 

$key = q/displaystylevar{X+a}=var{X};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\displaystyle \var{X+a} = \var{X}$">|; 

$key = q/displaystylevar{X_0}=esperof{X_0^2}-esperof{X_0}^2=esperof{X_0^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.12ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\displaystyle \var{X_0} = \esperof{X_0^2} - \esperof{X_0}^2 = \esperof{X_0^2}$">|; 

$key = q/displaystylevar{X}=cov{X}{X};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle \var{X} = \cov{X}{X}$">|; 

$key = q/displaystylevar{X}=esperof{X^2}-esperof{X}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.12ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\displaystyle \var{X} = \esperof{X^2} - \esperof{X}^2$">|; 

$key = q/displaystylevar{X}=esperof{left(X-esperof{X}right)^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.12ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle \var{X} = \esperof{\left(X-\esperof{X}\right)^2}$">|; 

$key = q/displaystylevar{aX+bY}=a^2var{X_0}+2abcov{X_0}{Y_0}+b^2var{Y_0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\displaystyle \var{a  X + b  Y} = a^2  \var{X_0} + 2  a  b  \cov{X_0}{Y_0} + b^2  \var{Y_0}$">|; 

$key = q/displaystylevar{aX+bY}=a^2var{X}+2abcov{X}{Y}+b^2var{Y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img148.svg"
 ALT="$\displaystyle \var{a  X + b  Y} = a^2  \var{X} + 2  a  b  \cov{X}{Y} + b^2  \var{Y}$">|; 

$key = q/displaystylex_i=X(omega_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img269.svg"
 ALT="$\displaystyle x_i = X(\omega_i)$">|; 

$key = q/dmathcal{L}_X=f_Xdx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img72.svg"
 ALT="$d\mathcal{L}_X = f_X  dx$">|; 

$key = q/dmathcal{L}_{X,Y}=f_{X,Y}dxdy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img95.svg"
 ALT="$d\mathcal{L}_{X,Y} = f_{X,Y}  dx  dy$">|; 

$key = q/dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img73.svg"
 ALT="$dx$">|; 

$key = q/dxdy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img96.svg"
 ALT="$dx  dy$">|; 

$key = q/emptyset,OmegainLambda(X);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\emptyset, \Omega \in \Lambda(X)$">|; 

$key = q/epsilon=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img228.svg"
 ALT="$\epsilon = 0$">|; 

$key = q/epsiloninsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img226.svg"
 ALT="$\epsilon \in \setR$">|; 

$key = q/esperof{X|Delta};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img259.svg"
 ALT="$\esperof{X \vert \Delta}$">|; 

$key = q/esperof{X|Gamma};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img260.svg"
 ALT="$\esperof{X \vert \Gamma}$">|; 

$key = q/esperof{X};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\esperof{X}$">|; 

$key = q/esperof{Y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\esperof{Y}$">|; 

$key = q/f_X:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img71.svg"
 ALT="$f_X : \setR \mapsto \setR$">|; 

$key = q/f_X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img76.svg"
 ALT="$f_X$">|; 

$key = q/f_{X,Y}:setR^2mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img94.svg"
 ALT="$f_{X,Y} : \setR^2 \mapsto \setR$">|; 

$key = q/f_{X,Y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img210.svg"
 ALT="$f_{X,Y}$">|; 

$key = q/g_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img58.svg"
 ALT="$g_i$">|; 

$key = q/indicatrice_Phiinmathcal{F}(Gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img244.svg"
 ALT="$\indicatrice_\Phi \in \mathcal{F}(\Gamma)$">|; 

$key = q/indicatrice_{Phi_i^+}+indicatrice_{Phi_i^-}=indicatrice_{Omega_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.95ex; vertical-align: -1.25ex; " SRC="|."$dir".q|img197.svg"
 ALT="$\indicatrice_{\Phi_i^+} + \indicatrice_{\Phi_i^-} = \indicatrice_{\Omega_i}$">|; 

$key = q/mathcal{F}(Delta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img261.svg"
 ALT="$\mathcal{F}(\Delta)$">|; 

$key = q/mathcal{F}(Gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img217.svg"
 ALT="$\mathcal{F}(\Gamma)$">|; 

$key = q/mathcal{L}_X:sousens(setR)mapsto[0,1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\mathcal{L}_X : \sousens(\setR) \mapsto [0,1]$">|; 

$key = q/mathcal{L}_X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\mathcal{L}_X$">|; 

$key = q/mathcal{L}_{X,Y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\mathcal{L}_{X,Y}$">|; 

$key = q/mathcal{S}={A:AsubseteqOmega};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\mathcal{S}=\{ A : A \subseteq \Omega \}$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\mu$">|; 

$key = q/omegainOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\omega \in \Omega$">|; 

$key = q/omegainOmega_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\omega \in \Omega_i$">|; 

$key = q/p_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img267.svg"
 ALT="$p_i$">|; 

$key = q/proba;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\proba$">|; 

$key = q/probaof{A|B};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img175.svg"
 ALT="$\probaof{A \vert B}$">|; 

$key = q/probaof{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img181.svg"
 ALT="$\probaof{A}$">|; 

$key = q/probaof{B}>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img189.svg"
 ALT="$\probaof{B} &gt; 0$">|; 

$key = q/probaof{B}le1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img171.svg"
 ALT="$\probaof{B} \le 1$">|; 

$key = q/probaof{Phi_i^-capB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img201.svg"
 ALT="$\probaof{\Phi_i^- \cap B}$">|; 

$key = q/probaof{Phi};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\probaof{\Phi}$">|; 

$key = q/probaof{emptyset}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\probaof{\emptyset} = 0$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\setR$">|; 

$key = q/setR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\setR^2$">|; 

$key = q/sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\sigma$">|; 

$key = q/sqrt{2pi}sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\sqrt{2 \pi} \sigma$">|; 

$key = q/uinsetR^N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.18ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img111.svg"
 ALT="$u \in \setR^N$">|; 

$key = q/var{X}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\var{X} \ge 0$">|; 

$key = q/x,yinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img83.svg"
 ALT="$x, y \in \setR$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img55.svg"
 ALT="$x \in \setR$">|; 

$key = q/xinsetR^N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.18ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img108.svg"
 ALT="$x \in \setR^N$">|; 

$key = q/{Eqts}Phi_i^+=Omega_icapBPhi_i^-=Omega_icap(OmegasetminusB){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img195.svg"
 ALT="\begin{Eqts}
\Phi_i^+ = \Omega_i \cap B \\\\
\Phi_i^- = \Omega_i \cap (\Omega \setminus B)
\end{Eqts}">|; 

$key = q/{Eqts}Phi_i^+capB=Phi_i^+Phi_i^-capB=emptyset{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img200.svg"
 ALT="\begin{Eqts}
\Phi_i^+ \cap B = \Phi_i^+ \\\\
\Phi_i^- \cap B = \emptyset
\end{Eqts}">|; 

$key = q/{Eqts}Phi_icapPhi_j=cases{Phi_i&i=jemptyset&inejcases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img9.svg"
 ALT="\begin{Eqts}
\Phi_i \cap \Phi_j =
\begin{cases}
\Phi_i &amp; i = j \\\\
\emptyset &amp; i \ne j
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}Theta(X,emptyset)=emptysetTheta(X,setR)=Omega{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img31.svg"
 ALT="\begin{Eqts}
\Theta(X,\emptyset) = \emptyset \\\\
\Theta(X,\setR) = \Omega
\end{Eqts}">|; 

$key = q/{Eqts}Theta(indicatrice_Phi,{1})={omega:indicatrice_Phi(omega)=1}=PhiTheta(indicatrice_Phi,{0})={omega:indicatrice_Phi(omega)=0}=OmegasetminusPhi{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img35.svg"
 ALT="\begin{Eqts}
\Theta(\indicatrice_\Phi, \{1\}) = \{ \omega : \indicatrice_\Phi(\o...
... \{ \omega : \indicatrice_\Phi(\omega) = 0 \} = \Omega \setminus \Phi
\end{Eqts}">|; 

$key = q/{Eqts}W=X+aZ=Y+b{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img133.svg"
 ALT="\begin{Eqts}
W = X + a \\\\
Z = Y + b
\end{Eqts}">|; 

$key = q/{Eqts}esperof{X_i}=mu_icov{X_i}{X_j}=sigma_{ij}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img109.svg"
 ALT="\begin{Eqts}
\esperof{X_i} = \mu_i \\\\
\cov{X_i}{X_j} = \sigma_{ij}
\end{Eqts}">|; 

$key = q/{Eqts}int_OmegaWesperof{X|Delta}dproba=int_OmegaWXdprobaint_OmegaWesperof{X|Gamma}dproba=int_OmegaWXdproba{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.44ex; vertical-align: -5.16ex; " SRC="|."$dir".q|img257.svg"
 ALT="\begin{Eqts}
\int_\Omega W \esperof{X \vert \Delta}  d\proba = \int_\Omega W ...
...ga W \esperof{X \vert \Gamma}  d\proba = \int_\Omega W X  d\proba
\end{Eqts}">|; 

$key = q/{Eqts}mathcal{L}_X(U)=mathcal{L}_{X,Y}(UtimessetR)mathcal{L}_Y(U)=mathcal{L}_{X,Y}(setRtimesU){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img23.svg"
 ALT="\begin{Eqts}
\mathcal{L}_X(U) = \mathcal{L}_{X,Y}(U \times \setR) \\\\
\mathcal{L}_Y(U) = \mathcal{L}_{X,Y}(\setR \times U)
\end{Eqts}">|; 

$key = q/{Eqts}mu=left(mu_iright)_iTheta=left(sigma_{ij}right)_{i,j}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.57ex; vertical-align: -2.72ex; " SRC="|."$dir".q|img105.svg"
 ALT="\begin{Eqts}
\mu = \left( \mu_i \right)_i \\\\
\Theta = \left( \sigma_{ij} \right)_{i,j}
\end{Eqts}">|; 

$key = q/{eqnarraystar}cov{X_0}{aY_0+bZ_0}&=&esperof{X_0(aY_0+bZ_0)}&=&aesperof{X_0Y_0}+besperof{X_0Z_0}&=&acov{X_0}{Y_0}+bcov{X_0}{Z_0}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img154.svg"
 ALT="\begin{eqnarray*}
\cov{X_0}{a  Y_0 + b  Z_0} &amp;=&amp; \esperof{X_0  (a  Y_0 + b \...
...perof{X_0  Z_0} \\\\
&amp;=&amp; a  \cov{X_0}{Y_0} + b  \cov{X_0}{Z_0}
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}esperof{X}&=&int_{setR^2}xf_{X,Y}(x,y)dxdy&=&int_setRxleft[int_setRf_{X,Y}(x,y)dyright]dx{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.58ex; " SRC="|."$dir".q|img99.svg"
 ALT="\begin{eqnarray*}
\esperof{X} &amp;=&amp; \int_{\setR^2} x  f_{X,Y}(x,y)  dx  dy \\\\
&amp;=&amp; \int_\setR x  \left[\int_\setR f_{X,Y}(x,y)  dy\right]  dx
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}esperof{Z|B}&=&sum_iZ_iesperof{indicatrice_{Omega_i}|B}&=&sum_iZ_iprobaof{Omega_i|B}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.64ex; " SRC="|."$dir".q|img192.svg"
 ALT="\begin{eqnarray*}
\esperof{Z \vert B} &amp;=&amp; \sum_i Z_i \esperof{\indicatrice_{\Omega_i} \vert B} \\\\
&amp;=&amp; \sum_i Z_i \probaof{\Omega_i \vert B}
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}esperof{indicatrice_Phi}&=&int_Omegaindicatrice_Phidproba&=&int_Phidproba{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.27ex; " SRC="|."$dir".q|img44.svg"
 ALT="\begin{eqnarray*}
\esperof{\indicatrice_\Phi} &amp;=&amp; \int_\Omega \indicatrice_\Phi  d\proba \\\\
&amp;=&amp; \int_\Phi  d\proba
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}int_{B_y}Xdproba&=&int_{(X,Y)(B_y)}xf_{X,Y}(x,y)dxdy&=&int_setRxf_{X,Y}(x,y)dx{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.68ex; " SRC="|."$dir".q|img211.svg"
 ALT="\begin{eqnarray*}
\int_{B_y} X  d\proba &amp;=&amp; \int_{(X,Y)(B_y)} x  f_{X,Y}(x,y)  dx  dy \\\\
&amp;=&amp; \int_\setR x  f_{X,Y}(x,y)  dx
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}var{aX+bY}&=&esperof{a^2X_0^2+2abX_0Y_0+b^2Y_0^2}&=&a^2esperof{X_0^2}+2abesperof{X_0Y_0}+b^2esperof{Y_0^2}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.80ex; " SRC="|."$dir".q|img146.svg"
 ALT="\begin{eqnarray*}
\var{a  X + b  Y} &amp;=&amp; \esperof{a^2  X_0^2 + 2  a  b  X_0...
...X_0^2} + 2  a  b  \esperof{X_0  Y_0} + b^2  \esperof{Y_0^2}
\end{eqnarray*}">|; 

1;

