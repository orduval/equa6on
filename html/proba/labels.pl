# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate labels original text with physical files.


$key = q/chap:proba/;
$external_labels{$key} = "$URL/" . q|proba.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:proba_cond/;
$external_labels{$key} = "$URL/" . q|proba.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2023 (Released January 1, 2023)
# labels from external_latex_labels array.


$key = q/chap:proba/;
$external_latex_labels{$key} = q|1 Probabilité|; 
$noresave{$key} = "$nosave";

$key = q/sec:proba_cond/;
$external_latex_labels{$key} = q|1.12 Probabilité conditionnelle|; 
$noresave{$key} = "$nosave";

1;

