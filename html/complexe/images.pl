# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(a,b)insetR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$(a, b) \in \setR^2$">|; 

$key = q/Im(z)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\Im(z) = 0$">|; 

$key = q/Re(z)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\Re(z) = 0$">|; 

$key = q/a,b,c,dinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img26.svg"
 ALT="$a,b,c,d \in \setR$">|; 

$key = q/a,binsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$a,b \in \setR$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$a$">|; 

$key = q/a=a+img0insetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img81.svg"
 ALT="$a = a + \img 0 \in \setC$">|; 

$key = q/ainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img80.svg"
 ALT="$a \in \setR$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$b$">|; 

$key = q/conjaccent{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.65ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\conjaccent{z}$">|; 

$key = q/displaystyle(a+imgb)cdotc=acdotc+imgbcdotc;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle (a + \img b) \cdot c = a \cdot c + \img b \cdot c$">|; 

$key = q/displaystyle(xcdotimg)^2=x^2cdotimg^2=x^2cdot(-1)=-x^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.76ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle (x \cdot \img)^2 = x^2 \cdot \img^2 = x^2 \cdot (-1) = - x^2$">|; 

$key = q/displaystyleIm(z)=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \Im(z) = b$">|; 

$key = q/displaystyleIm(z)=unsur{2img}(z-conjaccent{z});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \Im(z) = \unsur{2\img}(z - \conjaccent{z})$">|; 

$key = q/displaystyleRe(z)=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \Re(z) = a$">|; 

$key = q/displaystyleRe(z)=unsur{2}(z+conjaccent{z});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \Re(z) = \unsur{2}(z + \conjaccent{z})$">|; 

$key = q/displaystylea+imgb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle a + \img b$">|; 

$key = q/displaystyleabs{z}=sqrt{a^2+b^2}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.98ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \abs{z} = \sqrt{a^2 + b^2} \ge 0$">|; 

$key = q/displaystyleastrictinferieurc;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle a \strictinferieur c$">|; 

$key = q/displaystyleconjaccent{z}=conjugue(z)=a+img(-b)=a-imgb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \conjaccent{z} = \conjugue(z) = a + \img (-b) = a - \img b$">|; 

$key = q/displaystyleconjugue(x)cdotconjugue(y)=(acdotc-bcdotd)-img(acdotd+bcdotc)=conjugue(xcdoty);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \conjugue(x) \cdot \conjugue(y) = (a \cdot c - b \cdot d) - \img (a \cdot d + b \cdot c) = \conjugue(x \cdot y)$">|; 

$key = q/displaystyleconjugueconjuguez=z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.18ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \conjugue \conjugue z = z$">|; 

$key = q/displaystylefrac{x}{y}=frac{(a+imgb)cdot(c-imgd)}{c^2+d^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \frac{x}{y} = \frac{(a + \img b) \cdot (c - \img d)}{c^2 + d^2}$">|; 

$key = q/displaystylefrac{x}{y}=frac{(acdotc+bcdotd)+img(bcdotc-acdotd)}{c^2+d^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \frac{x}{y} = \frac{(a \cdot c + b \cdot d) + \img (b \cdot c - a \cdot d)}{c^2 + d^2}$">|; 

$key = q/displaystylefrac{x}{y}=xcdotunsur{y}=frac{xcdotconjaccent{y}}{abs{y}^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.69ex; vertical-align: -2.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \frac{x}{y} = x \cdot \unsur{y} = \frac{ x \cdot \conjaccent{y} }{ \abs{y}^2 }$">|; 

$key = q/displaystyleimg^2=-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \img^2 = -1$">|; 

$key = q/displaystyleimgz=imga+img^2b=imga-b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \img z = \img a + \img^2 b = \img a - b$">|; 

$key = q/displaystyleleft(zcdotabs{z}^2right)^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.51ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \left( z \cdot \abs{z}^2 \right)^{-1}$">|; 

$key = q/displaystylesetC={a+imgb:a,binsetR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \setC = \{ a + \img b :  a,b \in \setR \}$">|; 

$key = q/displaystylesqrt{-1}=img;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.39ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \sqrt{-1} = \img$">|; 

$key = q/displaystylesqrt{-x^2}=xcdotimg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.30ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \sqrt{-x^2} = x \cdot \img$">|; 

$key = q/displaystyleunsur{img}=-img;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \unsur{\img} = -\img$">|; 

$key = q/displaystyleunsur{z}=frac{a-imgb}{a^2+b^2}=frac{a}{a^2+b^2}-imgfrac{b}{a^2+b^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.13ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \unsur{z} = \frac{ a - \img b }{ a^2 + b^2 } = \frac{ a }{ a^2 + b^2 } - \img \frac{ b }{ a^2 + b^2 }$">|; 

$key = q/displaystyleunsur{z}=frac{conjaccent{z}}{abs{z}^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.69ex; vertical-align: -2.55ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \unsur{z} = \frac{ \conjaccent{z} }{ \abs{z}^2 }$">|; 

$key = q/displaystylex+y=a+imgb+c+imgd=(a+c)+img(b+d);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle x + y = a + \img b + c + \img d = (a + c) + \img (b + d)$">|; 

$key = q/displaystylex+y=a+imgb-(c+imgd)=a+imgb-c-imgd=(a-c)+img(b-d);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle x + y = a + \img b - (c + \img d) = a + \img b - c - \img d = (a - c) + \img (b - d)$">|; 

$key = q/displaystylex^2=-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.34ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle x^2 = -1$">|; 

$key = q/displaystylex^n=z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.81ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle x^n = z$">|; 

$key = q/displaystylexcdoty=(a+imgb)cdot(c+imgd)=acdotc+imgacdotd+imgbcdotc+img^2bcdotd;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.76ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle x \cdot y = (a + \img b) \cdot (c + \img d) = a \cdot c + \img a \cdot d + \img b \cdot c + \img^2 b \cdot d$">|; 

$key = q/displaystylexcdoty=(acdotc-bcdotd)+img(acdotd+bcdotc);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle x \cdot y = (a \cdot c - b \cdot d) + \img (a \cdot d + b \cdot c)$">|; 

$key = q/displaystylexcdoty=acdotc+imgacdotd+imgbcdotc-bcdotd;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle x \cdot y = a \cdot c + \img a \cdot d + \img b \cdot c - b \cdot d$">|; 

$key = q/displaystyleximg=imgx=xcdotimg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle x \img = \img x = x \cdot \img$">|; 

$key = q/displaystylez+conjaccent{z}=(a+imgb)+(a-imgb)=2a=2Re(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle z + \conjaccent{z} = (a + \img b) + (a - \img b) = 2 a = 2 \Re(z)$">|; 

$key = q/displaystylez-conjaccent{z}=(a+imgb)-(a-imgb)=2imgb=2imgIm(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle z - \conjaccent{z} = (a + \img b) - (a - \img b) = 2 \img b = 2 \img \Im(z)$">|; 

$key = q/displaystylez=Re(z)+imgIm(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle z = \Re(z) + \img \Im(z)$">|; 

$key = q/displaystylez=a+imgb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle z = a + \img b$">|; 

$key = q/displaystylez=x^{1slashn};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle z = x^{1/n}$">|; 

$key = q/displaystylez^s=lim_{itoinfty}z^{r_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.45ex; vertical-align: -1.74ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle z^s = \lim_{i \to \infty} z^{r_i}$">|; 

$key = q/displaystylez^{-n}=unsur{z^n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle z^{-n} = \unsur{z^n}$">|; 

$key = q/displaystylez^{mslashn}=left(z^{1slashn}right)^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.17ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle z^{m/n} = \left( z^{1/n} \right)^m$">|; 

$key = q/displaystylezcdotconjaccent{z}=(a+imgb)cdot(a-imgb)=a^2+b^2+img(acdotb-acdotb)=a^2+b^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle z \cdot \conjaccent{z} = (a + \img b) \cdot (a - \img b) = a^2 + b^2 + \img (a \cdot b - a \cdot b) = a^2 + b^2$">|; 

$key = q/displaystylezcdotconjaccent{z}=abs{z}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle z \cdot \conjaccent{z} = \abs{z}^2$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$i$">|; 

$key = q/img;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\img $">|; 

$key = q/minsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img67.svg"
 ALT="$m \in \setN$">|; 

$key = q/n^{ième};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.05ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img61.svg"
 ALT="$n^{ième}$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img59.svg"
 ALT="$n \in \setN$">|; 

$key = q/nne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img60.svg"
 ALT="$n \ne 0$">|; 

$key = q/s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img71.svg"
 ALT="$s$">|; 

$key = q/setRsubseteqsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\setR \subseteq \setC$">|; 

$key = q/sinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img69.svg"
 ALT="$s \in \setR$">|; 

$key = q/x,yinsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img25.svg"
 ALT="$x,y \in \setC$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img73.svg"
 ALT="$x$">|; 

$key = q/xinsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img64.svg"
 ALT="$x \in \setC$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img2.svg"
 ALT="$x \in \setR$">|; 

$key = q/xley;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img75.svg"
 ALT="$x \le y$">|; 

$key = q/xpreceqy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img77.svg"
 ALT="$x \preceq y$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img74.svg"
 ALT="$y$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$z$">|; 

$key = q/z=-conjaccent{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.85ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img57.svg"
 ALT="$z = -\conjaccent{z}$">|; 

$key = q/z=conjaccent{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.65ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img54.svg"
 ALT="$z = \conjaccent{z}$">|; 

$key = q/zcdotconjaccent{z}=abs{z}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$z \cdot \conjaccent{z} = \abs{z}^2$">|; 

$key = q/zinsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img13.svg"
 ALT="$z \in \setC$">|; 

$key = q/zinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img56.svg"
 ALT="$z\in\setR$">|; 

$key = q/{Eqts}Re(conjaccent{z})=Re(z)Im(conjaccent{z})=-Im(z){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img23.svg"
 ALT="\begin{Eqts}
\Re(\conjaccent{z}) = \Re(z) \\\\
\Im(\conjaccent{z}) = - \Im(z)
\end{Eqts}">|; 

$key = q/{Eqts}Re(imgz)=-b=-Im(z)Im(imgz)=a=Re(z){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img36.svg"
 ALT="\begin{Eqts}
\Re(\img z) = -b = - \Im(z) \\\\
\Im(\img z) = a = \Re(z)
\end{Eqts}">|; 

$key = q/{Eqts}Re(xcdoty)=Re(x)cdotRe(y)-Im(x)cdotIm(y)Im(xcdoty)=Re(x)cdotIm(y)+Im(x)cdotRe(y){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img33.svg"
 ALT="\begin{Eqts}
\Re(x \cdot y) = \Re(x) \cdot \Re(y) - \Im(x) \cdot \Im(y) \\\\
\Im(x \cdot y) = \Re(x) \cdot \Im(y) + \Im(x) \cdot \Re(y)
\end{Eqts}">|; 

$key = q/{Eqts}a=cbled{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img79.svg"
 ALT="\begin{Eqts}
a = c \\\\
b \le d
\end{Eqts}">|; 

$key = q/{Eqts}alecbled{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img76.svg"
 ALT="\begin{Eqts}
a \le c \\\\
b \le d
\end{Eqts}">|; 

$key = q/{Eqts}x=a+imgby=c+imgd{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img27.svg"
 ALT="\begin{Eqts}
x = a + \img b \\\\
y = c + \img d
\end{Eqts}">|; 

$key = q/{Eqts}z^0=1z^n=zcdotz^{n-1}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img62.svg"
 ALT="\begin{Eqts}
z^0 = 1 \\\\
z^n = z \cdot z^{n-1}
\end{Eqts}">|; 

$key = q/{r_iinsetQ:iinsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\{ r_i \in \setQ : i \in \setN \}$">|; 

1;

