# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(s,t)=(1,0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$(s,t) = (1,0)$">|; 

$key = q/(s,t)insetR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$(s,t) \in \setR^2$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$A$">|; 

$key = q/AsubseteqE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img21.svg"
 ALT="$A \subseteq E$">|; 

$key = q/Asubseteqconvexe(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$A \subseteq \convexe(A)$">|; 

$key = q/CsubseteqE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img34.svg"
 ALT="$C \subseteq E$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$E$">|; 

$key = q/[u,v]=sigma(L);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$[u,v] = \sigma(L)$">|; 

$key = q/a,b,c,dinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img38.svg"
 ALT="$a,b,c,d \in \setR$">|; 

$key = q/aleb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img39.svg"
 ALT="$a \le b$">|; 

$key = q/alpha,betainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\alpha,\beta \in \setR$">|; 

$key = q/alphalebeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\alpha \le \beta$">|; 

$key = q/cled;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img40.svg"
 ALT="$c \le d$">|; 

$key = q/convexe(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\convexe(A)$">|; 

$key = q/convexe(C)=C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\convexe(C) = C$">|; 

$key = q/lambda(0)=u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\lambda(0) = u$">|; 

$key = q/lambda(1)=v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\lambda(1) = v$">|; 

$key = q/lambda:[0,1]subseteqsetRmapstoE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\lambda : [0,1] \subseteq \setR \mapsto E$">|; 

$key = q/lambda:[alpha,beta]mapstoE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\lambda : [\alpha,\beta] \mapsto E$">|; 

$key = q/s+t=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img28.svg"
 ALT="$s + t = 1$">|; 

$key = q/s,tge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img27.svg"
 ALT="$s,t \ge 0$">|; 

$key = q/setR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\setR^n$">|; 

$key = q/sigma(0,1)=v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\sigma(0,1) = v$">|; 

$key = q/sigma(1,0)=u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\sigma(1,0) = u$">|; 

$key = q/sigma:LmapstoE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\sigma : L \mapsto E$">|; 

$key = q/sin[0,1]subseteqsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$s \in [0,1] \subseteq \setR$">|; 

$key = q/u,vinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img25.svg"
 ALT="$u,v \in A$">|; 

$key = q/uinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img31.svg"
 ALT="$u \in A$">|; 

$key = q/uinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$u \in E$">|; 

$key = q/varphi:[a,b]times[c,d]mapstoE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\varphi : [a,b] \times [c,d] \mapsto E$">|; 

$key = q/vinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img8.svg"
 ALT="$v \in E$">|; 

$key = q/{Eqts}L={(s,t)insetR^2:(s,t)ge0text{et}s+t=1}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img15.svg"
 ALT="\begin{Eqts}
L = \{ (s,t) \in \setR^2 : (s,t) \ge 0 \text{ et } s + t = 1 \}
\end{Eqts}">|; 

$key = q/{Eqts}Lambda={lambda(s):sin[alpha,beta]}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img3.svg"
 ALT="\begin{Eqts}
\Lambda = \{ \lambda(s) : s \in [\alpha,\beta] \}
\end{Eqts}">|; 

$key = q/{Eqts}Phi={varphi(s,t):(s,t)in[a,b]times[c,d]}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img36.svg"
 ALT="\begin{Eqts}
\Phi = \{ \varphi(s,t) : (s,t) \in [a,b] \times [c,d] \}
\end{Eqts}">|; 

$key = q/{Eqts}convexe(A)=bigcupmathcal{S}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.90ex; vertical-align: -1.37ex; " SRC="|."$dir".q|img24.svg"
 ALT="\begin{Eqts}
\convexe(A) = \bigcup \mathcal{S}
\end{Eqts}">|; 

$key = q/{Eqts}lambda(s)=u+scdot(v-u){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img10.svg"
 ALT="\begin{Eqts}
\lambda(s) = u + s \cdot (v - u)
\end{Eqts}">|; 

$key = q/{Eqts}mathcal{S}={[u,v]:u,vinA}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img23.svg"
 ALT="\begin{Eqts}
\mathcal{S} = \{ [u,v] : u,v \in A \}
\end{Eqts}">|; 

$key = q/{Eqts}relax[u,v]=lambda([0,1])={u+scdot(v-u):sin[0,1]}subseteqE{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img14.svg"
 ALT="\begin{Eqts}\relax
[u,v] = \lambda([0,1]) = \{ u + s \cdot (v - u) : s \in [0,1] \} \subseteq E
\end{Eqts}">|; 

$key = q/{Eqts}relaxsigma(s,t)=scdotu+tcdotv{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img17.svg"
 ALT="\begin{Eqts}\relax
\sigma(s,t) = s \cdot u + t \cdot v
\end{Eqts}">|; 

$key = q/{Eqts}scdotu+tcdotvinconvexe(A){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img29.svg"
 ALT="\begin{Eqts}
s \cdot u + t \cdot v \in \convexe(A)
\end{Eqts}">|; 

1;

