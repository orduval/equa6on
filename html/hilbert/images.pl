# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img124.svg"
 ALT="\begin{theoreme}
Soit une forme bilinéaire $\vartheta : H \times H \mapsto \cor...
... \forme{\varphi}{v}\end{displaymath}\par pour tout $v \in H$.
\par\end{theoreme}">|; 

$key = q/(V^orthogonal)^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$(V^\orthogonal)^\orthogonal$">|; 

$key = q/(V^orthogonal)^orthogonalsubseteqV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img115.svg"
 ALT="$(V^\orthogonal)^\orthogonal \subseteq V$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$0$">|; 

$key = q/A(u)=v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$A(u) = v$">|; 

$key = q/A(x_i)=y_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$A(x_i) = y_i$">|; 

$key = q/A:HmapstoH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img12.svg"
 ALT="$A : H \mapsto H$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$A$">|; 

$key = q/A^{-1}(v)=u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img129.svg"
 ALT="$A^{-1}(v) = u$">|; 

$key = q/H;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$H$">|; 

$key = q/H^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img117.svg"
 ALT="$H^\dual$">|; 

$key = q/Im(alpha)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\Im(\alpha) = 0$">|; 

$key = q/Im(alpha)=gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\Im(\alpha) = \gamma$">|; 

$key = q/Im(scalaire{e}{v})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\Im(\scalaire{e}{v}) = 0$">|; 

$key = q/Im(scalaire{e}{v})ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\Im(\scalaire{e}{v}) \ge 0$">|; 

$key = q/Im(scalaire{e}{v})le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\Im(\scalaire{e}{v}) \le 0$">|; 

$key = q/P(v)=v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$P(v) = v$">|; 

$key = q/P(x)inV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$P(x) \in V$">|; 

$key = q/P:HmapstoV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img35.svg"
 ALT="$P : H \mapsto V$">|; 

$key = q/P^2=P;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img85.svg"
 ALT="$P^2 = P$">|; 

$key = q/Re(alpha)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\Re(\alpha) = 0$">|; 

$key = q/Re(alpha)=gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\Re(\alpha) = \gamma$">|; 

$key = q/Re(scalaire{e}{v})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\Re(\scalaire{e}{v}) = 0$">|; 

$key = q/Re(scalaire{e}{v})ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\Re(\scalaire{e}{v}) \ge 0$">|; 

$key = q/Re(scalaire{e}{v})le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\Re(\scalaire{e}{v}) \le 0$">|; 

$key = q/V;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img99.svg"
 ALT="$V$">|; 

$key = q/V^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$V^\orthogonal$">|; 

$key = q/Vsubseteq(V^orthogonal)^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img104.svg"
 ALT="$V \subseteq (V^\orthogonal)^\orthogonal$">|; 

$key = q/VsubseteqH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img22.svg"
 ALT="$V \subseteq H$">|; 

$key = q/abs{alpha}^2=gamma^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\abs{\alpha}^2 = \gamma^2$">|; 

$key = q/alpha=-gammastrictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\alpha = - \gamma \strictinferieur 0$">|; 

$key = q/alpha=-imggamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\alpha = - \img \gamma$">|; 

$key = q/alpha=gammainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\alpha = \gamma \in \setR$">|; 

$key = q/alpha=imggamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\alpha = \img \gamma$">|; 

$key = q/alphaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\alpha \in \corps$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\corps$">|; 

$key = q/delta=-gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\delta = - \gamma$">|; 

$key = q/displaystyle(V^orthogonal)^orthogonal=V;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\displaystyle (V^\orthogonal)^\orthogonal = V$">|; 

$key = q/displaystyle0=norme{(u-v)+(y-z)}^2=norme{u-v}^2+norme{y-z}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle 0 = \norme{(u - v) + (y - z)}^2 = \norme{u - v}^2 + \norme{y - z}^2$">|; 

$key = q/displaystyle0=norme{x-x}^2=norme{u+y-v-z}^2=norme{(u-v)+(y-z)}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle 0 = \norme{x - x}^2 = \norme{u + y - v - z}^2 = \norme{(u - v) + (y - z)}^2$">|; 

$key = q/displaystyle0=scalaire{x}{z}=scalaire{u}{z}+scalaire{v}{z}=scalaire{v}{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle 0 = \scalaire{x}{z} = \scalaire{u}{z} + \scalaire{v}{z} = \scalaire{v}{z}$">|; 

$key = q/displaystyle0leD^2=norme{x}^2-sum_{i=1}^nabs{scalaire{u_i}{x}}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\displaystyle 0 \le D^2 = \norme{x}^2 - \sum_{i = 1}^n \abs{\scalaire{u_i}{x}}^2$">|; 

$key = q/displaystyle0lenorme{x}^2-sum_{i=1}^nabs{scalaire{u_i}{x}}^2leepsilon^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle 0 \le \norme{x}^2 - \sum_{i = 1}^n \abs{\scalaire{u_i}{x}}^2 \le \epsilon^2$">|; 

$key = q/displaystyle0lenorme{x}^2-sum_{i=1}^{+infty}abs{scalaire{u_i}{x}}^2le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle 0 \le \norme{x}^2 - \sum_{i = 1}^{+\infty} \abs{\scalaire{u_i}{x}}^2 \le 0$">|; 

$key = q/displaystyle2Im(scalaire{e}{v})ge-gammacdotnorme{v}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle 2 \Im(\scalaire{e}{v}) \ge - \gamma \cdot \norme{v}^2$">|; 

$key = q/displaystyle2Im(scalaire{e}{v})legammacdotnorme{v}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle 2 \Im(\scalaire{e}{v}) \le \gamma \cdot \norme{v}^2$">|; 

$key = q/displaystyle2Re(scalaire{e}{v})ge-gammacdotnorme{v}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle 2 \Re(\scalaire{e}{v}) \ge - \gamma \cdot \norme{v}^2$">|; 

$key = q/displaystyle2Re(scalaire{e}{v})legammacdotnorme{v}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle 2 \Re(\scalaire{e}{v}) \le \gamma \cdot \norme{v}^2$">|; 

$key = q/displaystyleD=norme{x-sum_{i=1}^nscalaire{u_i}{x}cdotu_i}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle D = \norme{x - \sum_{i = 1}^n \scalaire{u_i}{x} \cdot u_i} \le \epsilon$">|; 

$key = q/displaystyleH=VbigoplusV^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.90ex; vertical-align: -1.37ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\displaystyle H = V \bigoplus V^\orthogonal$">|; 

$key = q/displaystyleP(x)=argmin_{zinV}norme{x-z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle P(x) = \arg\min_{z \in V} \norme{x - z}$">|; 

$key = q/displaystyleP(x)=argument_Hinf_{zinV}norme{x-z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.05ex; vertical-align: -2.23ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle P(x) = \argument_H\inf_{z \in V} \norme{x - z}$">|; 

$key = q/displaystyleP^2(x)=PcircP(x)=P(v)=v=P(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle P^2(x) = P \circ P(x) = P(v) = v = P(x)$">|; 

$key = q/displaystyleabs{alpha}^2cdotnorme{v}^2-2Re(alpha)cdotRe(scalaire{e}{v})+2Im(alpha)cdotIm(scalaire{e}{v})ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \abs{\alpha}^2 \cdot \norme{v}^2 - 2 \Re(\alpha) \cdot \Re(\scalaire{e}{v}) + 2 \Im(\alpha) \cdot \Im(\scalaire{e}{v}) \ge 0$">|; 

$key = q/displaystyleabs{alpha}^2cdotnorme{v}^2-2Re(alphacdotscalaire{e}{v})ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \abs{\alpha}^2 \cdot \norme{v}^2 - 2 \Re(\alpha \cdot \scalaire{e}{v}) \ge 0$">|; 

$key = q/displaystyleabs{scalaire{x}{z}}=abs{scalaire{x-x_n}{z}}lenorme{x-x_n}cdotnorme{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \abs{\scalaire{x}{z}} = \abs{\scalaire{x - x_n}{z}} \le \norme{x - x_n} \cdot \norme{z}$">|; 

$key = q/displaystyledistance(u,v)=norme{u-v}=sqrt{scalaire{u-v}{u-v}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \distance(u,v) = \norme{u - v} = \sqrt{\scalaire{u - v}{u - v}}$">|; 

$key = q/displaystyleforme{varphi}{x}=lim_{ntoinfty}forme{varphi}{x_n}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \forme{\varphi}{x} = \lim_{n \to \infty} \forme{\varphi}{x_n} = 0$">|; 

$key = q/displaystylegamma^2cdotnorme{v}^2+2gammacdotIm(scalaire{e}{v})ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \gamma^2 \cdot \norme{v}^2 + 2 \gamma \cdot \Im(\scalaire{e}{v}) \ge 0$">|; 

$key = q/displaystylegamma^2cdotnorme{v}^2+2gammacdotRe(scalaire{e}{v})ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \gamma^2 \cdot \norme{v}^2 + 2 \gamma \cdot \Re(\scalaire{e}{v}) \ge 0$">|; 

$key = q/displaystylegamma^2cdotnorme{v}^2-2gammacdotIm(scalaire{e}{v})ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \gamma^2 \cdot \norme{v}^2 - 2 \gamma \cdot \Im(\scalaire{e}{v}) \ge 0$">|; 

$key = q/displaystylegamma^2cdotnorme{v}^2-2gammacdotRe(scalaire{e}{v})ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \gamma^2 \cdot \norme{v}^2 - 2 \gamma \cdot \Re(\scalaire{e}{v}) \ge 0$">|; 

$key = q/displaystylenorme{e}^2lenorme{e}^2-2Re(alphacdotscalaire{e}{v})+abs{alpha}^2cdotnorme{v}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \norme{e}^2 \le \norme{e}^2 - 2 \Re(\alpha \cdot \scalaire{e}{v}) + \abs{\alpha}^2 \cdot \norme{v}^2$">|; 

$key = q/displaystylenorme{p}^2=sum_iabs{scalaire{u_i}{x}}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle \norme{p}^2 = \sum_i \abs{\scalaire{u_i}{x}}^2$">|; 

$key = q/displaystylenorme{x-P(x)}=inf_{zinV}norme{x-z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \norme{x - P(x)} = \inf_{z \in V} \norme{x - z}$">|; 

$key = q/displaystylenorme{x-u}^2=norme{e}^2lenorme{x-u-alphacdotv}^2=norme{e-alphacdotv}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \norme{x - u}^2 = \norme{e}^2 \le \norme{x - u - \alpha \cdot v}^2 = \norme{e - \alpha \cdot v}^2$">|; 

$key = q/displaystylenorme{x}^2-norme{p}^2=norme{e}^2ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle \norme{x}^2 - \norme{p}^2 = \norme{e}^2 \ge 0$">|; 

$key = q/displaystylenorme{x}^2-sum_iabs{scalaire{u_i}{x}}^2ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\displaystyle \norme{x}^2 - \sum_i \abs{\scalaire{u_i}{x}}^2 \ge 0$">|; 

$key = q/displaystylenorme{x}^2=norme{p+e}^2=norme{p}^2+norme{e}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle \norme{x}^2 = \norme{p + e}^2 = \norme{p}^2 + \norme{e}^2$">|; 

$key = q/displaystylenorme{x}^2=sum_{i=1}^{+infty}abs{scalaire{u_i}{x}}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle \norme{x}^2 = \sum_{i = 1}^{+\infty} \abs{\scalaire{u_i}{x}}^2$">|; 

$key = q/displaystylep=sum_{i=1}^{+infty}scalaire{u_i}{x}cdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\displaystyle p = \sum_{i = 1}^{+\infty} \scalaire{u_i}{x} \cdot u_i$">|; 

$key = q/displaystylep_n=sum_{i=1}^nscalaire{u_i}{x}cdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle p_n = \sum_{i = 1}^n \scalaire{u_i}{x} \cdot u_i$">|; 

$key = q/displaystylescalaire{e}{v}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle \scalaire{e}{v} = 0$">|; 

$key = q/displaystylescalaire{p}{e}=sum_iscalaire{x}{u_i}cdotscalaire{u_i}{e}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle \scalaire{p}{e} = \sum_i \scalaire{x}{u_i} \cdot \scalaire{u_i}{e} = 0$">|; 

$key = q/displaystylescalaire{u_i}{u_j}=delta_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle \scalaire{u_i}{u_j} = \delta_{ij}$">|; 

$key = q/displaystylescalaire{x-x_n}{z}=scalaire{x}{z}-scalaire{x_n}{z}=scalaire{x}{z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \scalaire{x - x_n}{z} = \scalaire{x}{z} - \scalaire{x_n}{z} = \scalaire{x}{z}$">|; 

$key = q/displaystylescalaire{x}{y}=lim_{ntoinfty}sum_{i=1}^nconjaccent{x}_icdoty_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img162.svg"
 ALT="$\displaystyle \scalaire{x}{y} = \lim_{n \to \infty} \sum_{i = 1}^n \conjaccent{x}_i \cdot y_i$">|; 

$key = q/displaystylescalaire{x}{y}=sum_{i=1}^{+infty}conjaccent{x}_icdoty_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img163.svg"
 ALT="$\displaystyle \scalaire{x}{y} = \sum_{i = 1}^{+\infty} \conjaccent{x}_i \cdot y_i$">|; 

$key = q/displaystylescalaire{x}{z}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \scalaire{x}{z} = 0$">|; 

$key = q/displaystylesum_iabs{scalaire{u_i}{x}}^2lenorme{x}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle \sum_i \abs{\scalaire{u_i}{x}}^2 \le \norme{x}^2$">|; 

$key = q/displaystylex=lim_{ntoinfty}sum_{i=1}^nscalaire{u_i}{x}cdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img145.svg"
 ALT="$\displaystyle x = \lim_{n \to \infty} \sum_{i = 1}^n \scalaire{u_i}{x} \cdot u_i$">|; 

$key = q/displaystylex=sum_{i=1}^{+infty}scalaire{u_i}{x}cdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\displaystyle x = \sum_{i = 1}^{+\infty} \scalaire{u_i}{x} \cdot u_i$">|; 

$key = q/displaystylex=u+e;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle x = u + e$">|; 

$key = q/displaystylex=u+y=v+z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle x = u + y = v + z$">|; 

$key = q/displaystylex_i=scalaire{u_i}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img148.svg"
 ALT="$\displaystyle x_i = \scalaire{u_i}{x}$">|; 

$key = q/displaystyley=lim_{ntoinfty}y_n=lim_{ntoinfty}A(x_n)=A(x)inimageA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle y = \lim_{n \to \infty} y_n = \lim_{n \to \infty} A(x_n) = A(x) \in \image A$">|; 

$key = q/e=x-P(x)inV^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img88.svg"
 ALT="$e = x - P(x) \in V^\orthogonal$">|; 

$key = q/e=x-p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img134.svg"
 ALT="$e = x - p$">|; 

$key = q/e=x-u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img41.svg"
 ALT="$e = x - u$">|; 

$key = q/einV^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.20ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img77.svg"
 ALT="$e \in V^\orthogonal$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img149.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/epsilonto0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img155.svg"
 ALT="$\epsilon \to 0$">|; 

$key = q/gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\gamma$">|; 

$key = q/gammastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\gamma \strictsuperieur 0$">|; 

$key = q/i,jinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img132.svg"
 ALT="$i,j \in \setN$">|; 

$key = q/imageA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\image A$">|; 

$key = q/img=sqrt{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.50ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\img = \sqrt{-1}$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$n$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img150.svg"
 ALT="$n \in \setN$">|; 

$key = q/norme{u-v}=norme{y-z}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\norme{u - v} = \norme{y - z} = 0$">|; 

$key = q/norme{v-u}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\norme{v - u} \ge 0$">|; 

$key = q/norme{v-v}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\norme{v - v} = 0$">|; 

$key = q/norme{x-x_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\norme{x - x_n}$">|; 

$key = q/noyauvarphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\noyau \varphi$">|; 

$key = q/ntoinfty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img154.svg"
 ALT="$n \to \infty$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img139.svg"
 ALT="$p$">|; 

$key = q/scalaire{u-v}{y-z}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\scalaire{u - v}{y - z} = 0$">|; 

$key = q/scalaire{u}{z}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\scalaire{u}{z} = 0$">|; 

$key = q/scalaire{v}{v}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\scalaire{v}{v} = 0$">|; 

$key = q/scalaire{}{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\scalaire{}{}$">|; 

$key = q/u+alphacdotvinV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.95ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img44.svg"
 ALT="$u + \alpha \cdot v \in V$">|; 

$key = q/u,vinV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img89.svg"
 ALT="$u,v \in V$">|; 

$key = q/u-vinV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img93.svg"
 ALT="$u - v \in V$">|; 

$key = q/u=P(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$u = P(x)$">|; 

$key = q/u=P(x)inV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img87.svg"
 ALT="$u = P(x) \in V$">|; 

$key = q/u=v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img78.svg"
 ALT="$u = v$">|; 

$key = q/u_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img146.svg"
 ALT="$u_i$">|; 

$key = q/uinH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img127.svg"
 ALT="$u \in H$">|; 

$key = q/uinV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img80.svg"
 ALT="$u \in V$">|; 

$key = q/v=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img113.svg"
 ALT="$v = 0$">|; 

$key = q/v=P(x)inV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img83.svg"
 ALT="$v = P(x) \in V$">|; 

$key = q/v=x-P(x)inV^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img106.svg"
 ALT="$v = x - P(x) \in V^\orthogonal$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\varphi$">|; 

$key = q/varphiinH^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\varphi \in H^\dual$">|; 

$key = q/vinH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img126.svg"
 ALT="$v \in H$">|; 

$key = q/vinV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img43.svg"
 ALT="$v \in V$">|; 

$key = q/x,yinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img158.svg"
 ALT="$x,y \in E$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img144.svg"
 ALT="$x$">|; 

$key = q/x=u+v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img107.svg"
 ALT="$x = u + v$">|; 

$key = q/x=uinV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img114.svg"
 ALT="$x = u \in V$">|; 

$key = q/x_i=scalaire{u_i}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img160.svg"
 ALT="$x_i = \scalaire{u_i}{x}$">|; 

$key = q/x_iinH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img16.svg"
 ALT="$x_i \in H$">|; 

$key = q/xin(V^orthogonal)^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$x \in (V^\orthogonal)^\orthogonal$">|; 

$key = q/xin(V^orthogonal)^orthogonalsubseteqH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img105.svg"
 ALT="$x \in (V^\orthogonal)^\orthogonal \subseteq H$">|; 

$key = q/xinH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$x \in H$">|; 

$key = q/xinV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img101.svg"
 ALT="$x \in V$">|; 

$key = q/xinV^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.20ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img31.svg"
 ALT="$x \in V^\orthogonal$">|; 

$key = q/xinnoyauvarphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$x \in \noyau \varphi$">|; 

$key = q/y,zinV^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.56ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img90.svg"
 ALT="$y,z \in V^\orthogonal$">|; 

$key = q/y-zinV^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.56ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img94.svg"
 ALT="$y - z \in V^\orthogonal$">|; 

$key = q/y=z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img98.svg"
 ALT="$y = z$">|; 

$key = q/y_i=scalaire{u_i}{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img161.svg"
 ALT="$y_i = \scalaire{u_i}{y}$">|; 

$key = q/y_iinimageA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$y_i \in \image A$">|; 

$key = q/yinH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$y \in H$">|; 

$key = q/yinimageA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img20.svg"
 ALT="$y \in \image A$">|; 

$key = q/z=v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img111.svg"
 ALT="$z = v$">|; 

$key = q/zinV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img24.svg"
 ALT="$z \in V$">|; 

$key = q/zinV^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.20ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img102.svg"
 ALT="$z \in V^\orthogonal$">|; 

$key = q/{Eqts}x=sum_{i=1}^{+infty}x_icdotu_iy=sum_{i=1}^{+infty}y_icdotu_i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.90ex; vertical-align: -6.90ex; " SRC="|."$dir".q|img159.svg"
 ALT="\begin{Eqts}
x = \sum_{i = 1}^{+\infty} x_i \cdot u_i \\\\
y = \sum_{i = 1}^{+\infty} y_i \cdot u_i
\end{Eqts}">|; 

$key = q/{u_iinH:iinsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\{ u_i \in H : i \in \setN\}$">|; 

$key = q/{x_1,x_2,...}subseteqV^orthogonalsubseteqH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\{x_1,x_2,...\} \subseteq V^\orthogonal \subseteq H$">|; 

$key = q/{x_1,x_2,...}subseteqnoyauvarphisubseteqH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\{x_1,x_2,...\} \subseteq \noyau \varphi \subseteq H$">|; 

$key = q/{y_1,y_2,...}subseteqimageAsubseteqH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\{y_1,y_2,...\} \subseteq \image A \subseteq H$">|; 

1;

