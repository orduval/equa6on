# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(-r)^2=r^2=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$(-r)^2 = r^2 = 2$">|; 

$key = q/(r,s)insetR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$(r,s) \in \setR^2$">|; 

$key = q/-r=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img124.svg"
 ALT="$-r = 0$">|; 

$key = q/-rinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img107.svg"
 ALT="$-r \in \setR$">|; 

$key = q/-x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img186.svg"
 ALT="$-x$">|; 

$key = q/0insetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img83.svg"
 ALT="$0 \in \setQ$">|; 

$key = q/0insetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img102.svg"
 ALT="$0 \in \setR$">|; 

$key = q/2^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img155.svg"
 ALT="$2^n$">|; 

$key = q/2n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img189.svg"
 ALT="$2 n$">|; 

$key = q/Lambda(s)subseteqLambda(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\Lambda(s) \subseteq \Lambda(x)$">|; 

$key = q/Lambda(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\Lambda(x)$">|; 

$key = q/Lambda(x)lex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\Lambda(x) \le x$">|; 

$key = q/Lambda(x)neemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\Lambda(x) \ne \emptyset$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$R$">|; 

$key = q/R=S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img100.svg"
 ALT="$R = S$">|; 

$key = q/Rlemu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img41.svg"
 ALT="$R \le \mu$">|; 

$key = q/Rneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img39.svg"
 ALT="$R \ne \emptyset$">|; 

$key = q/RsubseteqS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img68.svg"
 ALT="$R \subseteq S$">|; 

$key = q/RsubseteqsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img38.svg"
 ALT="$R \subseteq \setQ$">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img65.svg"
 ALT="$S$">|; 

$key = q/S=R+Z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.95ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img85.svg"
 ALT="$S = R + Z$">|; 

$key = q/SsubseteqR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img76.svg"
 ALT="$S \subseteq R$">|; 

$key = q/SsubseteqsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img63.svg"
 ALT="$S \subseteq \setQ$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img140.svg"
 ALT="$X$">|; 

$key = q/Z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img97.svg"
 ALT="$Z$">|; 

$key = q/a,b,cinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img199.svg"
 ALT="$a,b,c \in \setR$">|; 

$key = q/a,b,x,yinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img193.svg"
 ALT="$a,b,x,y \in \setR$">|; 

$key = q/a,binsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img207.svg"
 ALT="$a,b \in \setZ$">|; 

$key = q/abs{-r}=abs{r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\abs{-r} = \abs{r}$">|; 

$key = q/abs{r+s}=(-r)+(-s)=abs{r}+abs{s};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\abs{r + s} = (-r) + (-s) = \abs{r} + \abs{s}$">|; 

$key = q/abs{r+s}=r+s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\abs{r + s} = r + s$">|; 

$key = q/abs{r+s}leabs{r}+abs{s};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\abs{r + s} \le \abs{r} + \abs{s}$">|; 

$key = q/abs{r+s}lemax{abs{r},abs{s}}leabs{r}+abs{s};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\abs{r + s} \le \max\{ \abs{r} , \abs{s} \} \le \abs{r} + \abs{s}$">|; 

$key = q/abs{r}=-rstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\abs{r} = -r \strictsuperieur 0$">|; 

$key = q/abs{r}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\abs{r} = 0$">|; 

$key = q/abs{r}=rge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\abs{r} = r \ge 0$">|; 

$key = q/abs{r}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\abs{r} \ge 0$">|; 

$key = q/abs{x-y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\abs{x - y}$">|; 

$key = q/arrondiinf{x}+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img145.svg"
 ALT="$\arrondiinf{x} + 1$">|; 

$key = q/d=x-y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img96.svg"
 ALT="$d = x - y$">|; 

$key = q/dinZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img99.svg"
 ALT="$d \in Z$">|; 

$key = q/displaystyle-requivLambda(-r)={xinsetQ:xstrictinferieur-r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle -r \equiv \Lambda(-r) = \{ x \in \setQ : x \strictinferieur -r \}$">|; 

$key = q/displaystyle-requiv{-x:xinsetQsetminusR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle -r \equiv \{ -x : x \in \setQ \setminus R \}$">|; 

$key = q/displaystyle0equiv{xinsetQ:xstrictinferieur0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle 0 \equiv \{ x \in \setQ : x \strictinferieur 0 \}$">|; 

$key = q/displaystyleLambda(s)=bigcup_{ninsetN}lambda(s_n)=bigcup_{ninsetN}{vinsetQ:vles_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.16ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \Lambda(s) = \bigcup_{n \in \setN} \lambda(s_n) = \bigcup_{n \in \setN} \{ v \in \setQ : v \le s_n \}$">|; 

$key = q/displaystyleLambda(s)=sup_subseteq{lambda(s_n):ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.29ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \Lambda(s) = \sup_\subseteq \{ \lambda(s_n) : n \in \setN \}$">|; 

$key = q/displaystyleLambda(x)={yinsetQ:ystrictinferieurx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \Lambda(x) = \{ y \in \setQ : y \strictinferieur x \}$">|; 

$key = q/displaystyleLambda(x)={yinsetQ:ystrictinferieurx}subseteqR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \Lambda(x) = \{ y \in \setQ : y \strictinferieur x \} \subseteq R$">|; 

$key = q/displaystyleR={xinsetQ:f(x)strictinferieur2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle R = \{ x \in \setQ : f(x) \strictinferieur 2 \}$">|; 

$key = q/displaystyleRsubseteqS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle R \subseteq S$">|; 

$key = q/displaystyleTheta(s)=bigcap_{ninsetN}theta(s_n)=bigcap_{ninsetN}{vinsetQ:vges_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.16ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \Theta(s) = \bigcap_{n \in \setN} \theta(s_n) = \bigcap_{n \in \setN} \{ v \in \setQ : v \ge s_n \}$">|; 

$key = q/displaystyleTheta(s)=inf_subseteq{theta(s_n):ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.84ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \Theta(s) = \inf_\subseteq \{ \theta(s_n) : n \in \setN \}$">|; 

$key = q/displaystyleZ={xinsetQ:xstrictinferieur0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle Z = \{ x \in \setQ : x \strictinferieur 0 \}$">|; 

$key = q/displaystylea=b^{1slashm}=left(c^{1slashn}right)^{1slashm};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.66ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img201.svg"
 ALT="$\displaystyle a = b^{1/m} = \left( c^{1/n} \right)^{1/m}$">|; 

$key = q/displaystylea=c^{1slash(mcdotn)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img203.svg"
 ALT="$\displaystyle a = c^{ 1/(m \cdot n) }$">|; 

$key = q/displaystylea^ncdotb^n=acdot...cdotacdotbcdot...cdotb=acdotbcdot...cdotacdotb=(acdotb)^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img195.svg"
 ALT="$\displaystyle a^n \cdot b^n = a \cdot ... \cdot a \cdot b \cdot ... \cdot b = a \cdot b \cdot ... \cdot a \cdot b = (a \cdot b)^n$">|; 

$key = q/displaystylea^{mcdotn}=left(a^mright)^n=b^n=c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.56ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img202.svg"
 ALT="$\displaystyle a^{m \cdot n} = \left( a^m \right)^n = b^n = c$">|; 

$key = q/displaystyleabs{arrondiinf{x}-x}ge1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle \abs{\arrondiinf{x} - x} \ge 1$">|; 

$key = q/displaystyleabs{frac{m}{2^n}-r}leunsur{2^n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle \abs{ \frac{m}{2^n} - r} \le \unsur{2^n}$">|; 

$key = q/displaystyleabs{r}=sup{-r,r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle \abs{r} = \sup \{ -r , r \}$">|; 

$key = q/displaystylearrondiinf{x}=sup{ninsetZ,Lambda(n)subseteqX};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\displaystyle \arrondiinf{x} = \sup \{ n \in \setZ,  \Lambda(n) \subseteq X \}$">|; 

$key = q/displaystylearrondisup{x}=inf{ninsetZ,XsubseteqLambda(n)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle \arrondisup{x} = \inf \{ n \in \setZ,  X \subseteq \Lambda(n) \}$">|; 

$key = q/displaystyledistance(x,y)=abs{x-y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\displaystyle \distance(x,y) = \abs{x - y}$">|; 

$key = q/displaystylefrac{r}{s}=rcdots^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img170.svg"
 ALT="$\displaystyle \frac{r}{s} = r \cdot s^{-1}$">|; 

$key = q/displaystylelambda(u)={vinsetQ:vleu};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \lambda(u) = \{ v \in \setQ : v \le u \}$">|; 

$key = q/displaystyleleft(c^{1slashn}right)^{1slashm}=c^{1slash(mcdotn)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.66ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img204.svg"
 ALT="$\displaystyle \left( c^{1/n} \right)^{1/m} = c^{ 1/(m \cdot n) }$">|; 

$key = q/displaystyleleft(x^{aslashb}right)^{cslashd}=left(z^{acdotd}right)^{cslashd}=z^{acdotc};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.66ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img213.svg"
 ALT="$\displaystyle \left( x^{a/b} \right)^{c/d} = \left( z^{a \cdot d} \right)^{c/d} = z^{a \cdot c}$">|; 

$key = q/displaystyleleft(x^{aslashb}right)^{cslashd}=x^{frac{acdotc}{bcdotd}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.66ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img214.svg"
 ALT="$\displaystyle \left( x^{a/b} \right)^{c/d} = x^{ \frac{a \cdot c}{b \cdot d} }$">|; 

$key = q/displaystylelim_{itoinfty}r_i=s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.44ex; vertical-align: -1.74ex; " SRC="|."$dir".q|img217.svg"
 ALT="$\displaystyle \lim_{i \to \infty} r_i = s$">|; 

$key = q/displaystylelim_{ntoinfty}abs{x_n-r}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img159.svg"
 ALT="$\displaystyle \lim_{n \to \infty} \abs{x_n - r} = 0$">|; 

$key = q/displaystylelim_{ntoinfty}x_n=r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.42ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img160.svg"
 ALT="$\displaystyle \lim_{n \to \infty} x_n = r$">|; 

$key = q/displaystylemathcal{E}(s)=accolades{tinmathfrak{C}:lim_n(s_n-t_n)=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.48ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \mathcal{E}(s) = \accolades{t \in \mathfrak{C} : \lim_n (s_n - t_n) = 0}$">|; 

$key = q/displaystylemathcal{E}(s)=accolades{tinmathfrak{C}:sequivt};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \mathcal{E}(s) = \accolades{t \in \mathfrak{C} : s \equiv t}$">|; 

$key = q/displaystylemathcal{E}^bot(s)=accolades{tinmathfrak{C}^bot:lim_n(s_n-t_n)=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.48ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \mathcal{E}^\bot(s) = \accolades{t \in \mathfrak{C}^\bot : \lim_n (s_n - t_n) = 0}$">|; 

$key = q/displaystylemathcal{E}^bot(s)=accolades{tinmathfrak{C}^bot:sequivt};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.03ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \mathcal{E}^\bot(s) = \accolades{t \in \mathfrak{C}^\bot : s \equiv t}$">|; 

$key = q/displaystylemathcal{E}^top(s)=accolades{tinmathfrak{C}^top:lim_n(s_n-t_n)=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.48ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \mathcal{E}^\top(s) = \accolades{t \in \mathfrak{C}^\top : \lim_n (s_n - t_n) = 0}$">|; 

$key = q/displaystylemathcal{E}^top(s)=accolades{tinmathfrak{C}^top:sequivt};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.03ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \mathcal{E}^\top(s) = \accolades{t \in \mathfrak{C}^\top : s \equiv t}$">|; 

$key = q/displaystylemathfrak{C}=mathfrak{C}^topcupmathfrak{C}^bot;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \mathfrak{C} = \mathfrak{C}^\top \cup \mathfrak{C}^\bot$">|; 

$key = q/displaystylemax{abs{arrondisup{x}-x},abs{arrondiinf{x}-x}}strictinferieur1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\displaystyle \max\{ \abs{\arrondisup{x} - x} , \abs{\arrondiinf{x} - x} \} \strictinferieur 1$">|; 

$key = q/displaystylemin{arrondisup{rcdot2^n},arrondiinf{rcdot2^n}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle m \in \{ \arrondisup{r \cdot 2^n} , \arrondiinf{r \cdot 2^n} \}$">|; 

$key = q/displaystyler+(-r)=(-r)+r=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle r + (-r) = (-r) + r = 0$">|; 

$key = q/displaystyler+sequiv{x+yinsetQ:xinR,yinS};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle r + s \equiv \{ x + y \in \setQ : x \in R,  y \in S \}$">|; 

$key = q/displaystyler-s=r+(-s);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle r - s = r + (-s)$">|; 

$key = q/displaystyler=supR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle r = \sup R$">|; 

$key = q/displaystylercdot2^n-1lemlercdot2^n+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.13ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle r \cdot 2^n - 1 \le m \le r \cdot 2^n + 1$">|; 

$key = q/displaystylercdotr^{-1}=r^{-1}cdotr=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img168.svg"
 ALT="$\displaystyle r \cdot r^{-1} = r^{-1} \cdot r = 1$">|; 

$key = q/displaystylercdots=lim_{ntoinfty}x_ncdoty_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.42ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img166.svg"
 ALT="$\displaystyle r \cdot s = \lim_{n \to \infty} x_n \cdot y_n$">|; 

$key = q/displaystylerdivideontimess=lim_{ntoinfty}left[x_ndivideontimesy_nright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img164.svg"
 ALT="$\displaystyle r \divideontimes s = \lim_{n \to \infty} \left[ x_n \divideontimes y_n \right]$">|; 

$key = q/displaystylerequivsup_subseteqsousens(R)=R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.29ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle r \equiv \sup_\subseteq \sousens(R) = R$">|; 

$key = q/displaystylerles;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle r \le s$">|; 

$key = q/displaystylerstrictinferieursquadLeftrightarrowquadRsubsetS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle r \strictinferieur s \quad \Leftrightarrow \quad R \subset S$">|; 

$key = q/displaystyles:nmapstos_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle s : n \mapsto s_n$">|; 

$key = q/displaystyles:setNmapstosetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle s : \setN \mapsto \setQ$">|; 

$key = q/displaystylesqrt[n]{a^ncdotb^n}=acdotb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.23ex; " SRC="|."$dir".q|img196.svg"
 ALT="$\displaystyle \sqrt[n]{a^n \cdot b^n} = a \cdot b$">|; 

$key = q/displaystylesqrt[n]{xcdoty}=sqrt[n]{x}cdotsqrt[n]{y}=(xcdoty)^{1slashn}=x^{1slashn}cdoty^{1slashn};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img197.svg"
 ALT="$\displaystyle \sqrt[n]{x \cdot y} = \sqrt[n]{x} \cdot \sqrt[n]{y} = (x \cdot y)^{1/n} = x^{1/n} \cdot y^{1/n}$">|; 

$key = q/displaystylesqrt{x}=sqrt[2]{x}=x^{1slash2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.75ex; vertical-align: -0.54ex; " SRC="|."$dir".q|img178.svg"
 ALT="$\displaystyle \sqrt{x} = \sqrt[2]{x} = x^{1/2}$">|; 

$key = q/displaystylesqrt{z}=abs{x}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img188.svg"
 ALT="$\displaystyle \sqrt{z} = \abs{x} \ge 0$">|; 

$key = q/displaystyletheta(u)={vinsetQ:vgeu};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \theta(u) = \{ v \in \setQ : v \ge u \}$">|; 

$key = q/displaystylex=x-y+y=d+y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle x = x - y + y = d + y$">|; 

$key = q/displaystylex^2=(-x)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img185.svg"
 ALT="$\displaystyle x^2 = (-x)^2$">|; 

$key = q/displaystylex^s=lim_{ito+infty}x^{r_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.61ex; vertical-align: -1.90ex; " SRC="|."$dir".q|img218.svg"
 ALT="$\displaystyle x^s = \lim_{i \to +\infty} x^{r_i}$">|; 

$key = q/displaystylex^{-n}=left(x^nright)^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img173.svg"
 ALT="$\displaystyle x^{-n} = \left( x^n \right)^{-1}$">|; 

$key = q/displaystylex^{-n}=left(x^{-1}right)^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img172.svg"
 ALT="$\displaystyle x^{-n} = \left( x^{-1} \right)^n$">|; 

$key = q/displaystylex^{2n+1}=left(x^2right)^ncdotx=-left((-x)^2right)^ncdot(-x)=-(-x)^{2n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img191.svg"
 ALT="$\displaystyle x^{2 n + 1} = \left( x^2 \right)^n \cdot x = - \left( (-x)^2 \right)^n \cdot (-x) = - (-x)^{2 n + 1}$">|; 

$key = q/displaystylex^{2n}=left(x^2right)^n=left((-x)^2right)^n=(-x)^{2n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img190.svg"
 ALT="$\displaystyle x^{2 n} = \left( x^2 \right)^n = \left( (-x)^2 \right)^n = (-x)^{2 n}$">|; 

$key = q/displaystylex^{aslashb}cdotx^{cslashd}=left(z^dright)^acdotleft(z^bright)^c=z^{acdotd}cdotz^{bcdotc}=z^{acdotd+bcdotc};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.12ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img210.svg"
 ALT="$\displaystyle x^{a/b} \cdot x^{c/d} = \left( z^d \right)^a \cdot \left( z^b \right)^c = z^{a \cdot d} \cdot z^{b \cdot c} = z^{a \cdot d + b \cdot c}$">|; 

$key = q/displaystylex^{aslashb}cdotx^{cslashd}=x^{frac{acdotd+bcdotc}{bcdotd}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img211.svg"
 ALT="$\displaystyle x^{a/b} \cdot x^{c/d} = x^{ \frac{a \cdot d + b \cdot c}{b \cdot d} }$">|; 

$key = q/displaystylex^{aslashb}cdotx^{cslashd}=x^{frac{a}{b}+frac{c}{d}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img212.svg"
 ALT="$\displaystyle x^{a/b} \cdot x^{c/d} = x^{ \frac{a}{b} + \frac{c}{d} }$">|; 

$key = q/displaystylex_n=frac{m}{2^n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle x_n = \frac{m}{2^n}$">|; 

$key = q/displaystylez=x^{1slash(bcdotd)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img208.svg"
 ALT="$\displaystyle z = x^{ 1/(b \cdot d) }$">|; 

$key = q/displaystylez=x^{1slashn}=sqrt[n]{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.75ex; vertical-align: -0.54ex; " SRC="|."$dir".q|img177.svg"
 ALT="$\displaystyle z = x^{1/n} = \sqrt[n]{x}$">|; 

$key = q/displaystylez^n=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.81ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img176.svg"
 ALT="$\displaystyle z^n = x$">|; 

$key = q/distance(x,y)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\distance(x,y) = 0$">|; 

$key = q/distance(x,y)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\distance(x,y) \ge 0$">|; 

$key = q/divideontimes;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.73ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img161.svg"
 ALT="$\divideontimes$">|; 

$key = q/e=2-x^2strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.22ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img32.svg"
 ALT="$e = 2 - x^2 \strictsuperieur 0$">|; 

$key = q/f(x)=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$f(x) = 2$">|; 

$key = q/f(x)strictinferieur2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$f(x) \strictinferieur 2$">|; 

$key = q/f(y)strictsuperieur2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$f(y) \strictsuperieur 2$">|; 

$key = q/f:setQmapstosetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$f : \setQ \mapsto \setQ$">|; 

$key = q/le;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\le$">|; 

$key = q/m,ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img198.svg"
 ALT="$m,n \in \setN$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img152.svg"
 ALT="$m$">|; 

$key = q/mathfrak{C}^bot;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\mathfrak{C}^\bot$">|; 

$key = q/mathfrak{C}^top;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\mathfrak{C}^\top$">|; 

$key = q/minsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img147.svg"
 ALT="$m \in \setZ$">|; 

$key = q/muinsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\mu \in \setQ$">|; 

$key = q/n^{ième};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.05ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img175.svg"
 ALT="$n^{ième}$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img148.svg"
 ALT="$n \in \setN$">|; 

$key = q/nne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img149.svg"
 ALT="$n \ne 0$">|; 

$key = q/r+0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img86.svg"
 ALT="$r + 0$">|; 

$key = q/r+0=r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img101.svg"
 ALT="$r + 0 = r$">|; 

$key = q/r,sge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img125.svg"
 ALT="$r,s \ge 0$">|; 

$key = q/r,sinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img114.svg"
 ALT="$r,s \in \setR$">|; 

$key = q/r,sstrictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img130.svg"
 ALT="$r,s \strictinferieur 0$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$r$">|; 

$key = q/r=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img123.svg"
 ALT="$r = 0$">|; 

$key = q/r^2=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$r^2 = 2$">|; 

$key = q/r^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img167.svg"
 ALT="$r^{-1}$">|; 

$key = q/rge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img117.svg"
 ALT="$r \ge 0$">|; 

$key = q/rinsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img109.svg"
 ALT="$r \in \setQ$">|; 

$key = q/rinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img61.svg"
 ALT="$r \in \setR$">|; 

$key = q/rinsetRsetminussetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img111.svg"
 ALT="$r \in \setR \setminus \setQ$">|; 

$key = q/rles;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img69.svg"
 ALT="$r \le s$">|; 

$key = q/rnes;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img81.svg"
 ALT="$r \ne s$">|; 

$key = q/rstrictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img119.svg"
 ALT="$r \strictinferieur 0$">|; 

$key = q/rstrictinferieurs;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img80.svg"
 ALT="$r \strictinferieur s$">|; 

$key = q/s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img59.svg"
 ALT="$s$">|; 

$key = q/s=x+z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img88.svg"
 ALT="$s = x + z$">|; 

$key = q/s=xinR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img91.svg"
 ALT="$s = x \in R$">|; 

$key = q/setNsubseteqsetZsubseteqsetQsubseteqsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\setN \subseteq \setZ \subseteq \setQ \subseteq \setR$">|; 

$key = q/setQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\setQ$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\setR$">|; 

$key = q/setR^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img184.svg"
 ALT="$\setR^+$">|; 

$key = q/sge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img129.svg"
 ALT="$s \ge 0$">|; 

$key = q/sinLambda(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$s \in \Lambda(x)$">|; 

$key = q/sinR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img94.svg"
 ALT="$s \in R$">|; 

$key = q/sinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img87.svg"
 ALT="$s \in S$">|; 

$key = q/sinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img62.svg"
 ALT="$s \in \setR$">|; 

$key = q/sler;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img77.svg"
 ALT="$s \le r$">|; 

$key = q/sne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img169.svg"
 ALT="$s \ne 0$">|; 

$key = q/sstrictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img127.svg"
 ALT="$s \strictinferieur 0$">|; 

$key = q/sstrictinferieurtstrictinferieurx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.68ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img58.svg"
 ALT="$s \strictinferieur t \strictinferieur x$">|; 

$key = q/sstrictinferieurx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img93.svg"
 ALT="$s \strictinferieur x$">|; 

$key = q/subseteq;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\subseteq$">|; 

$key = q/tinLambda(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$t \in \Lambda(x)$">|; 

$key = q/x,r,sinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img220.svg"
 ALT="$x,r,s \in \setR$">|; 

$key = q/x,sinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img215.svg"
 ALT="$x, s \in \setR$">|; 

$key = q/x,y,zinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img179.svg"
 ALT="$x,y,z \in \setR$">|; 

$key = q/x,yge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img180.svg"
 ALT="$x,y \ge 0$">|; 

$key = q/x-y=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img137.svg"
 ALT="$x - y = 0$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img26.svg"
 ALT="$x$">|; 

$key = q/x=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img138.svg"
 ALT="$x = y$">|; 

$key = q/x^n=y^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img183.svg"
 ALT="$x^n = y^n$">|; 

$key = q/x^nney^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img182.svg"
 ALT="$x^n \ne y^n$">|; 

$key = q/x_i,y_iinsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img162.svg"
 ALT="$x_i,y_i \in \setQ$">|; 

$key = q/x_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img158.svg"
 ALT="$x_n$">|; 

$key = q/x_ninsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img150.svg"
 ALT="$x_n \in \setQ$">|; 

$key = q/xinR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img31.svg"
 ALT="$x \in R$">|; 

$key = q/xinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img67.svg"
 ALT="$x \in S$">|; 

$key = q/xinsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img24.svg"
 ALT="$x \in \setQ$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img106.svg"
 ALT="$x \in \setR$">|; 

$key = q/xney;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img181.svg"
 ALT="$x \ne y$">|; 

$key = q/xnotinR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img49.svg"
 ALT="$x \notin R$">|; 

$key = q/xnotinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img70.svg"
 ALT="$x \notin S$">|; 

$key = q/xstrictinferieury;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img50.svg"
 ALT="$x \strictinferieur y$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$y$">|; 

$key = q/y_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img165.svg"
 ALT="$y_n$">|; 

$key = q/yinR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$y \in R$">|; 

$key = q/yinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img75.svg"
 ALT="$y \in S$">|; 

$key = q/yinsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img42.svg"
 ALT="$y \in \setQ$">|; 

$key = q/ylex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img95.svg"
 ALT="$y \le x$">|; 

$key = q/ynotinR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img47.svg"
 ALT="$y \notin R$">|; 

$key = q/ystrictinferieurx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img43.svg"
 ALT="$y \strictinferieur x$">|; 

$key = q/ystrictsuperieurx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img48.svg"
 ALT="$y \strictsuperieur x$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img174.svg"
 ALT="$z$">|; 

$key = q/z=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img90.svg"
 ALT="$z = 0$">|; 

$key = q/z=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img72.svg"
 ALT="$z = x$">|; 

$key = q/z=x^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img187.svg"
 ALT="$z = x^2$">|; 

$key = q/zinZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img89.svg"
 ALT="$z \in Z$">|; 

$key = q/zinsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img71.svg"
 ALT="$z \in \setQ$">|; 

$key = q/znotinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img74.svg"
 ALT="$z \notin S$">|; 

$key = q/zstrictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img92.svg"
 ALT="$z \strictinferieur 0$">|; 

$key = q/zstrictsuperieurx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img73.svg"
 ALT="$z \strictsuperieur x$">|; 

$key = q/{Eqts}a^m=bb^n=c{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img200.svg"
 ALT="\begin{Eqts}
a^m = b \\\\
b^n = c
\end{Eqts}">|; 

$key = q/{Eqts}a^n=xb^n=y{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img194.svg"
 ALT="\begin{Eqts}
a^n = x \\\\
b^n = y
\end{Eqts}">|; 

$key = q/{Eqts}f(x)=cases{0&text{si}xle0x^2&text{si}xstrictsuperieur0cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img23.svg"
 ALT="\begin{Eqts}
f(x) =
\begin{cases}
0 &amp; \text{ si } x \le 0 \\\\
x^2 &amp; \text{ si } x \strictsuperieur 0
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}frac{m}{2^n}-rleunsur{2^n}r-frac{m}{2^n}leunsur{2^n}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.25ex; vertical-align: -4.57ex; " SRC="|."$dir".q|img154.svg"
 ALT="\begin{Eqts}
\frac{m}{2^n} - r \le \unsur{2^n} \\\\
r - \frac{m}{2^n} \le \unsur{2^n}
\end{Eqts}">|; 

$key = q/{Eqts}lim_{ntoinfty}x_n=rlim_{ntoinfty}y_n=s{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.12ex; vertical-align: -3.49ex; " SRC="|."$dir".q|img163.svg"
 ALT="\begin{Eqts}
\lim_{n \to \infty} x_n = r \\\\
\lim_{n \to \infty} y_n = s
\end{Eqts}">|; 

$key = q/{Eqts}setR^+={xinsetR:xge0}setR^-={xinsetR:xle0}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img104.svg"
 ALT="\begin{Eqts}
\setR^+ = \{ x \in \setR : x \ge 0 \} \\\\
\setR^- = \{ x \in \setR : x \le 0 \}
\end{Eqts}">|; 

$key = q/{Eqts}signe(x)=cases{1&text{si}xge0-1&text{si}xstrictinferieur0cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img105.svg"
 ALT="\begin{Eqts}
\signe(x) =
\begin{cases}
1 &amp; \text{ si } x \ge 0 \\\\
-1 &amp; \text{ si } x \strictinferieur 0
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}sqrt[2n]{x^{2n}}=abs{x}sqrt[2n+1]{x^{2n+1}}=x{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.24ex; vertical-align: -3.05ex; " SRC="|."$dir".q|img192.svg"
 ALT="\begin{Eqts}
\sqrt[2 n]{ x^{2 n} } = \abs{x} \\\\
\sqrt[2 n + 1]{ x^{2 n + 1} } = x
\end{Eqts}">|; 

$key = q/{Eqts}x-arrondiinf{x}ge1xgearrondiinf{x}+1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img144.svg"
 ALT="\begin{Eqts}
x - \arrondiinf{x} \ge 1 \\\\
x \ge \arrondiinf{x} + 1
\end{Eqts}">|; 

$key = q/{Eqts}x^0=1x^n=xcdotx^{n-1}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img171.svg"
 ALT="\begin{Eqts}
x^0 = 1 \\\\
x^n = x \cdot x^{n - 1}
\end{Eqts}">|; 

$key = q/{Eqts}x^{1slashb}=z^dx^{1slashd}=z^b{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.87ex; vertical-align: -2.87ex; " SRC="|."$dir".q|img209.svg"
 ALT="\begin{Eqts}
x^{1/b} = z^d \\\\
x^{1/d} = z^b
\end{Eqts}">|; 

$key = q/{Eqts}x^{mslashn}=left(x^mright)^{1slashn}x^{-mslashn}=left(x^{-m}right)^{1slashn}=unsur{x^{mslashn}}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.73ex; vertical-align: -3.80ex; " SRC="|."$dir".q|img206.svg"
 ALT="\begin{Eqts}
x^{m/n} = \left( x^m \right)^{1/n} \\\\
x^{-m/n} = \left( x^{-m} \right)^{1/n} = \unsur{x^{m/n}}
\end{Eqts}">|; 

$key = q/{Eqts}x^{mslashn}=left(x^{1slashn}right)^mx^{-mslashn}=left(x^{1slashn}right)^{-m}=unsur{x^{mslashn}}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.62ex; vertical-align: -3.74ex; " SRC="|."$dir".q|img205.svg"
 ALT="\begin{Eqts}
x^{m/n} = \left( x^{1/n} \right)^m \\\\
x^{-m/n} = \left( x^{1/n} \right)^{-m} = \unsur{x^{m/n}}
\end{Eqts}">|; 

$key = q/{Eqts}x^{r+s}=x^rcdotx^sleft(x^rright)^s=x^{rcdots}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img219.svg"
 ALT="\begin{Eqts}
x^{r + s} = x^r \cdot x^s \\\\
\left( x^r \right)^s = x^{r \cdot s}
\end{Eqts}">|; 

$key = q/{eqnarraystar}distance(x,z)&=&abs{x-z}&le&abs{x-y}+abs{y-z}&le&distance(x,y)+distance(y,z){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img139.svg"
 ALT="\begin{eqnarray*}
\distance(x,z) &amp;=&amp; \abs{x - z} \\\\
&amp;\le&amp; \abs{x - y} + \abs{y - z} \\\\
&amp;\le&amp; \distance(x,y) + \distance(y,z)
\end{eqnarray*}">|; 

$key = q/{r_1,r_2,...};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img216.svg"
 ALT="$\{ r_1,r_2,... \}$">|; 

1;

