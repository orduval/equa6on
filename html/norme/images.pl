# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$1$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$E$">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$S$">|; 

$key = q/alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\alpha$">|; 

$key = q/alphaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\alpha \in \corps$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\corps$">|; 

$key = q/d;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$d$">|; 

$key = q/displaystyledistance(0,x+y)ledistance(0,x)+distance(x,x+y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \distance(0 , x + y) \le \distance(0,x) + \distance(x , x + y)$">|; 

$key = q/displaystyledistance(x,x+y)=distance(x-x,x+y-x)=distance(0,y)equivnorme{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \distance(x , x + y) = \distance(x - x , x + y - x) = \distance(0,y) \equiv \norme{y}$">|; 

$key = q/displaystyledistance(x,y)=norme{x-y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \distance(x,y) = \norme{x - y}$">|; 

$key = q/displaystylenorme{-x}=norme{(-1)cdotx}=abs{-1}cdotnorme{x}=norme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \norme{-x} = \norme{(-1) \cdot x} = \abs{-1} \cdot \norme{x} = \norme{x}$">|; 

$key = q/displaystylenorme{alphacdotx}=abs{alpha}cdotnorme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \norme{\alpha \cdot x} = \abs{\alpha} \cdot \norme{x}$">|; 

$key = q/displaystylenorme{u}=norme{frac{w}{norme{w}}}=unsur{norme{w}}cdotnorme{w}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \norme{u} = \norme{ \frac{w}{\norme{w}} } = \unsur{\norme{w}} \cdot \norme{w} = 1$">|; 

$key = q/displaystylenorme{x+y}lenorme{x}+norme{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \norme{x + y} \le \norme{x} + \norme{y}$">|; 

$key = q/displaystylenorme{x}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \norme{x} = 0$">|; 

$key = q/displaystylenorme{x}equivdistance(x,0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \norme{x} \equiv \distance(x,0)$">|; 

$key = q/displaystylenorme{x}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \norme{x} \ge 0$">|; 

$key = q/displaystylenorme{z-x}gemax{norme{z}-norme{x},norme{x}-norme{z}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \norme{z - x} \ge \max\{ \norme{z} - \norme{x}, \norme{x} -\norme{z} \}$">|; 

$key = q/displaystylenorme{z-x}genorme{z}-norme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \norme{z - x} \ge \norme{z} - \norme{x}$">|; 

$key = q/displaystylenorme{z}lenorme{x}+norme{y}=norme{x}+norme{z-x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \norme{z} \le \norme{x} + \norme{y} = \norme{x} + \norme{z-x}$">|; 

$key = q/displaystyleu=frac{w}{norme{w}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.92ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle u = \frac{w}{\norme{w}}$">|; 

$key = q/distance(x,0)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\distance(x,0) = 0$">|; 

$key = q/distance(x,0)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\distance(x,0) \ge 0$">|; 

$key = q/distance(x,x)=norme{x-x}=norme{0}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\distance(x,x) = \norme{x - x} = \norme{0} = 0$">|; 

$key = q/distance(x,x+y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\distance(x , x + y)$">|; 

$key = q/distance(x,y)+distance(y,z)=norme{x-y}+norme{y-z}genorme{x-y+y-z}=norme{x-z}=distance(x,z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\distance(x,y) + \distance(y,z) = \norme{x-y} + \norme{y-z} \ge \norme{x-y+y-z} = \norme{x-z} = \distance(x,z)$">|; 

$key = q/distance(x,y)=0=norme{x-y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\distance(x,y) = 0 = \norme{x-y}$">|; 

$key = q/distance(x,y)=norme{x-y}=norme{y-x}=distance(y,x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\distance(x,y) = \norme{x-y} = \norme{y-x} = \distance(y,x)$">|; 

$key = q/le;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\le$">|; 

$key = q/norme{.}:EtosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\norme{.} : E \to \setR$">|; 

$key = q/norme{.};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\norme{.}$">|; 

$key = q/norme{w}ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\norme{w} \ne 0$">|; 

$key = q/norme{z-x}=norme{(-1)cdot(z-x)}=norme{x-z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\norme{z-x} = \norme{(-1) \cdot (z-x)} = \norme{x-z}$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$u$">|; 

$key = q/wne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img38.svg"
 ALT="$w \ne 0$">|; 

$key = q/x,y,zinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$x,y,z \in E$">|; 

$key = q/x-y=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img34.svg"
 ALT="$x - y = 0$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$x$">|; 

$key = q/x=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img14.svg"
 ALT="$x = 0$">|; 

$key = q/x=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img35.svg"
 ALT="$x = y$">|; 

$key = q/xinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img6.svg"
 ALT="$x \in E$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$z$">|; 

$key = q/z=x+y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img23.svg"
 ALT="$z = x + y$">|; 

1;

