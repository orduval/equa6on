# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle J_w(\epsilon) = \unsur{2} \biforme{u}{a}{u} + \epsilon \cdot \bif...
...ilon^2}{2} \cdot \biforme{w}{a}{w} - \forme{b}{u} - \epsilon \cdot \forme{b}{w}$">|; 

$key = q/(u,lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img114.svg"
 ALT="$(u,\lambda)$">|; 

$key = q/(v,y)inEtimessetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$(v,y) \in E \times \setR$">|; 

$key = q/2unotinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img111.svg"
 ALT="$2 u \notin \Omega$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img47.svg"
 ALT="$A$">|; 

$key = q/Ainmatrice(setR,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$A \in \matrice(\setR,n,n)$">|; 

$key = q/B=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img162.svg"
 ALT="$B = I$">|; 

$key = q/Binmatrice(setR,n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$B \in \matrice(\setR,n,1)$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img104.svg"
 ALT="$E$">|; 

$key = q/E_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img147.svg"
 ALT="$E_0$">|; 

$key = q/E_0=Esetminus{0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img143.svg"
 ALT="$E_0 = E \setminus \{0\}$">|; 

$key = q/I(u_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$I(u_n)$">|; 

$key = q/I:mathcal{F}mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$I : \mathcal{F} \mapsto \setR$">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$I$">|; 

$key = q/J_w(0)leJ_w(epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$J_w(0) \le J_w(\epsilon)$">|; 

$key = q/J_w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img12.svg"
 ALT="$J_w$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\Omega$">|; 

$key = q/Omega_1=Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img149.svg"
 ALT="$\Omega_1 = \Omega$">|; 

$key = q/Omega_{n+1}subseteqOmega_nsubseteq...Omega_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\Omega_{n + 1} \subseteq \Omega_n \subseteq ... \Omega_1$">|; 

$key = q/OmegasubseteqF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img145.svg"
 ALT="$\Omega \subseteq F$">|; 

$key = q/R(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img135.svg"
 ALT="$R(z)$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img146.svg"
 ALT="$R$">|; 

$key = q/U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img102.svg"
 ALT="$U$">|; 

$key = q/U_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img37.svg"
 ALT="$U_i$">|; 

$key = q/Uinmatrice(setR,n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$U \in \matrice(\setR,n,1)$">|; 

$key = q/a,b:EtimesEmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img105.svg"
 ALT="$a,b : E \times E \mapsto \setR$">|; 

$key = q/a,b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img106.svg"
 ALT="$a,b$">|; 

$key = q/a:mathcal{F}timesmathcal{F}mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img50.svg"
 ALT="$a : \mathcal{F} \times \mathcal{F} \mapsto \setR$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img55.svg"
 ALT="$a$">|; 

$key = q/alphainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\alpha \in \setR$">|; 

$key = q/b:mathcal{F}mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img53.svg"
 ALT="$b : \mathcal{F} \mapsto \setR$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img56.svg"
 ALT="$b$">|; 

$key = q/biforme{e}{a}{delta}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\biforme{e}{a}{\delta} = 0$">|; 

$key = q/biforme{u}{a}{w}=forme{b}{w};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\biforme{u}{a}{w} = \forme{b}{w}$">|; 

$key = q/biforme{u}{b}{u}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\biforme{u}{b}{u} = 1$">|; 

$key = q/biforme{x}{a}{x}=alpha^2cdotbiforme{u}{a}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\biforme{x}{a}{x} = \alpha^2 \cdot \biforme{u}{a}{u}$">|; 

$key = q/biforme{x}{b}{x}=alpha^2cdotbiforme{u}{b}{u}=alpha^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\biforme{x}{b}{x} = \alpha^2 \cdot \biforme{u}{b}{u} = \alpha^2$">|; 

$key = q/delta=z-v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\delta = z - v$">|; 

$key = q/deltainmathcal{G};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.32ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\delta \in \mathcal{G}$">|; 

$key = q/displaystyleAcdotU-B=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle A \cdot U - B = 0$">|; 

$key = q/displaystyleAcdotU=B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle A \cdot U = B$">|; 

$key = q/displaystyleI(u)=int_ABig[u(x)-f(x)Big]^2dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle I(u) = \int_A \Big[ u(x) - f(x) \Big]^2 dx$">|; 

$key = q/displaystyleI(u)leI(v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle I(u) \le I(v)$">|; 

$key = q/displaystyleI(u_n)=J(U)=unsur{2}U^dualcdotAcdotU-U^dualcdotB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle I(u_n) = J(U) = \unsur{2} U^\dual \cdot A \cdot U - U^\dual \cdot B$">|; 

$key = q/displaystyleI(u_n)=unsur{2}sum_{i,j=1}^nU_icdotbiforme{varphi_i}{a}{varphi_j}cdotU_j-sum_{i=1}^nforme{b}{varphi_i}U_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle I(u_n) = \unsur{2} \sum_{i,j = 1}^n U_i \cdot \biforme{\varphi_i}{a}{\varphi_j} \cdot U_j - \sum_{i=1}^n \forme{b}{\varphi_i} U_i$">|; 

$key = q/displaystyleI(v)=K(1)geK(gamma)=-frac{forme{b}{v}^2}{2biforme{v}{a}{v}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.05ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle I(v) = K(1) \ge K(\gamma) = - \frac{ \forme{b}{v}^2 }{ 2 \biforme{v}{a}{v} }$">|; 

$key = q/displaystyleI(v)=biforme{v}{a}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\displaystyle I(v) = \biforme{v}{a}{v}$">|; 

$key = q/displaystyleI(v)=unsur{2}biforme{v}{a}{v}-forme{b}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle I(v) = \unsur{2} \biforme{v}{a}{v} - \forme{b}{v}$">|; 

$key = q/displaystyleI(v)ge-frac{norme{b}^2}{2varrho};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.92ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle I(v) \ge - \frac{\norme{b}^2}{2 \varrho}$">|; 

$key = q/displaystyleJ_v(0)leJ_v(epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle J_v(0) \le J_v(\epsilon)$">|; 

$key = q/displaystyleJ_v(epsilon)=I(u+epsiloncdotv)=int_ABig[u(x)+epsiloncdotv(x)-f(x)Big]^2dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle J_v(\epsilon) = I(u + \epsilon \cdot v) = \int_A \Big[ u(x) + \epsilon \cdot v(x) - f(x) \Big]^2 dx$">|; 

$key = q/displaystyleJ_w(0)=I(u)leJ_w(epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle J_w(0) = I(u) \le J_w(\epsilon)$">|; 

$key = q/displaystyleJ_w(epsilon)=I(u+epsiloncdotw);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle J_w(\epsilon) = I(u + \epsilon \cdot w)$">|; 

$key = q/displaystyleK(epsilon)=I(epsiloncdotv)=frac{epsilon^2}{2}cdotbiforme{v}{a}{v}-epsiloncdotforme{b}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle K(\epsilon) = I(\epsilon \cdot v) = \frac{\epsilon^2}{2} \cdot \biforme{v}{a}{v} - \epsilon \cdot \forme{b}{v}$">|; 

$key = q/displaystyleK(gamma)=frac{forme{b}{v}^2}{2biforme{v}{a}{v}}-frac{forme{b}{v}^2}{biforme{v}{a}{v}}=-frac{forme{b}{v}^2}{2biforme{v}{a}{v}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.05ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle K(\gamma) = \frac{ \forme{b}{v}^2 }{ 2 \biforme{v}{a}{v} } - \fra...
...{v}^2 }{ \biforme{v}{a}{v} } = - \frac{ \forme{b}{v}^2 }{ 2 \biforme{v}{a}{v} }$">|; 

$key = q/displaystyleOD{J_v^lambda}{epsilon}(0)=2biforme{w}{a}{u}-2lambdacdotbiforme{w}{b}{u}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.26ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\displaystyle \OD{J_v^\lambda}{\epsilon}(0) = 2 \biforme{w}{a}{u} - 2 \lambda \cdot \biforme{w}{b}{u} = 0$">|; 

$key = q/displaystyleOD{J_v}{epsilon}(0)=2int_Abig[u(x)-f(x)big]cdotv(x)dx=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.46ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \OD{J_v}{\epsilon}(0) = 2 \int_A \big[ u(x) - f(x) \big] \cdot v(x)  dx = 0$">|; 

$key = q/displaystyleOD{J_v}{epsilon}(0)=biforme{u}{a}{w}-forme{b}{w}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle \OD{J_v}{\epsilon}(0) = \biforme{u}{a}{w} - \forme{b}{w} = 0$">|; 

$key = q/displaystyleOD{J_v}{epsilon}(epsilon)=2biforme{w}{a}{u}+2epsiloncdotbiforme{w}{a}{w}-2lambdacdot[biforme{w}{b}{u}+epsiloncdotbiforme{w}{b}{w}];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\displaystyle \OD{J_v}{\epsilon}(\epsilon) = 2 \biforme{w}{a}{u} + 2 \epsilon \...
...}{w} - 2 \lambda \cdot [ \biforme{w}{b}{u} + \epsilon \cdot \biforme{w}{b}{w} ]$">|; 

$key = q/displaystyleOD{J_v}{epsilon}(epsilon)=biforme{u}{a}{w}+epsiloncdotbiforme{w}{a}{w}-forme{b}{w};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \OD{J_v}{\epsilon}(\epsilon) = \biforme{u}{a}{w} + \epsilon \cdot \biforme{w}{a}{w} - \forme{b}{w}$">|; 

$key = q/displaystyleOD{J_v}{epsilon}(epsilon)=int_A2big[u(x)+epsiloncdotv(x)-f(x)big]cdotv(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.46ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \OD{J_v}{\epsilon}(\epsilon) = \int_A 2 \big[ u(x) + \epsilon \cdot v(x) - f(x) \big] \cdot v(x)  dx$">|; 

$key = q/displaystyleOD{J_w}{epsilon}(0)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \OD{J_w}{\epsilon}(0) = 0$">|; 

$key = q/displaystyleOD{K}{epsilon}(gamma)=gammacdotbiforme{v}{a}{v}-forme{b}{v}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle \OD{K}{\epsilon}(\gamma) = \gamma \cdot \biforme{v}{a}{v} - \forme{b}{v} = 0$">|; 

$key = q/displaystyleOOD{J_w}{epsilon}(0)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \OOD{J_w}{\epsilon}(0) \ge 0$">|; 

$key = q/displaystyleOmega={vinE:biforme{v}{b}{v}=1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle \Omega = \{ v \in E : \biforme{v}{b}{v} = 1 \}$">|; 

$key = q/displaystyleOmega_{n+1}={vinOmega:biforme{v}{b}{e_1}=...=biforme{v}{b}{e_n}=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle \Omega_{n + 1} = \{ v \in \Omega : \biforme{v}{b}{e_1} = ... = \biforme{v}{b}{e_n} = 0 \}$">|; 

$key = q/displaystyleR(z)=frac{biforme{z}{a}{z}}{biforme{z}{b}{z}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle R(z) = \frac{ \biforme{z}{a}{z} }{ \biforme{z}{b}{z} }$">|; 

$key = q/displaystyleU=A^{-1}cdotB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle U = A^{-1} \cdot B$">|; 

$key = q/displaystylea(u,u)gevarrhocdotnorme{u}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle a(u,u) \ge \varrho \cdot \norme{u}^2$">|; 

$key = q/displaystylebiforme{2u}{b}{2u}=4biforme{u}{b}{u}=4ne1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle \biforme{2 u}{b}{2 u} = 4 \biforme{u}{b}{u} = 4 \ne 1$">|; 

$key = q/displaystylebiforme{e}{a}{w}=biforme{v}{a}{w}-biforme{u}{a}{w}=forme{b}{w}-forme{b}{w}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \biforme{e}{a}{w} = \biforme{v}{a}{w} - \biforme{u}{a}{w} = \forme{b}{w} - \forme{b}{w} = 0$">|; 

$key = q/displaystylebiforme{u}{a}{u}=lambdacdotbiforme{u}{b}{u}=lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\displaystyle \biforme{u}{a}{u} = \lambda \cdot \biforme{u}{b}{u} = \lambda$">|; 

$key = q/displaystylebiforme{u}{a}{v}=biforme{v}{a}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \biforme{u}{a}{v} = \biforme{v}{a}{u}$">|; 

$key = q/displaystylebiforme{u}{a}{w}=forme{b}{w};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle \biforme{u}{a}{w} = \forme{b}{w}$">|; 

$key = q/displaystylebiforme{varphi_i}{b}{varphi_j}=delta_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img161.svg"
 ALT="$\displaystyle \biforme{\varphi_i}{b}{\varphi_j} = \delta_{ij}$">|; 

$key = q/displaystylebiforme{v}{a}{v}=frac{biforme{z}{a}{z}}{biforme{z}{b}{z}}=R(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle \biforme{v}{a}{v} = \frac{ \biforme{z}{a}{z} }{ \biforme{z}{b}{z} } = R(z)$">|; 

$key = q/displaystylebiforme{v}{a}{w}=forme{b}{w};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \biforme{v}{a}{w} = \forme{b}{w}$">|; 

$key = q/displaystylebiforme{v}{b}{v}=frac{biforme{z}{b}{z}}{biforme{z}{b}{z}}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle \biforme{v}{b}{v} = \frac{ \biforme{z}{b}{z} }{ \biforme{z}{b}{z} } = 1$">|; 

$key = q/displaystylebiforme{w}{a}{u}=lambdacdotbiforme{w}{b}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle \biforme{w}{a}{u} = \lambda \cdot \biforme{w}{b}{u}$">|; 

$key = q/displaystylebiforme{z-u}{a}{z-u}=biforme{delta}{a}{delta}+2biforme{e}{a}{delta}+biforme{e}{a}{e};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle \biforme{z - u}{a}{z - u} = \biforme{\delta}{a}{\delta} + 2 \biforme{e}{a}{\delta} + \biforme{e}{a}{e}$">|; 

$key = q/displaystylebiforme{z-u}{a}{z-u}=biforme{delta}{a}{delta}+biforme{e}{a}{e}gebiforme{e}{a}{e};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle \biforme{z - u}{a}{z - u} = \biforme{\delta}{a}{\delta} + \biforme{e}{a}{e} \ge \biforme{e}{a}{e}$">|; 

$key = q/displaystyledistance(u_n,u)leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \distance(u_n,u) \le \epsilon$">|; 

$key = q/displaystylee_{n+1}inargmin_{vinOmega_{n+1}}biforme{v}{a}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.98ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\displaystyle e_{n + 1} \in \arg\min_{ v \in \Omega_{n + 1} } \biforme{v}{a}{v}$">|; 

$key = q/displaystylefrac{forme{b}{v}^2}{2biforme{v}{a}{v}}lefrac{norme{b}^2}{2varrho};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.05ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle \frac{ \forme{b}{v}^2 }{ 2 \biforme{v}{a}{v} } \le \frac{\norme{b}^2}{2 \varrho}$">|; 

$key = q/displaystylegamma=frac{forme{b}{v}}{biforme{v}{a}{v}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\displaystyle \gamma = \frac{ \forme{b}{v} }{ \biforme{v}{a}{v} }$">|; 

$key = q/displaystyleinf_{vinmathcal{F}}I(v)ge-frac{norme{b}^2}{2varrho};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.92ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle \inf_{ v \in \mathcal{F} } I(v) \ge - \frac{\norme{b}^2}{2\varrho}$">|; 

$key = q/displaystyleint_A(u_n(x)-f(x))cdotvarphi_i(x)dx=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \int_A (u_n(x) - f(x)) \cdot \varphi_i(x)  dx = 0$">|; 

$key = q/displaystyleint_Abig[u(x)-f(x)big]cdotv(x)dx=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \int_A \big[ u(x) - f(x) \big] \cdot v(x)  dx = 0$">|; 

$key = q/displaystyleint_Avarphi_isum_jU_jcdotvarphi_jdx=int_Avarphi_icdotfdx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.55ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \int_A \varphi_i \sum_j U_j \cdot \varphi_j  dx = \int_A \varphi_i \cdot f  dx$">|; 

$key = q/displaystylelagrangien(u,y)lelagrangien(u,lambda)lelagrangien(v,lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle \lagrangien(u,y) \le \lagrangien(u,\lambda) \le \lagrangien(v,\lambda)$">|; 

$key = q/displaystylelagrangien(u_n,y)=U^dualcdotAcdotU+ycdotleft[1-U^dualcdotBcdotUright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle \lagrangien(u_n,y) = U^\dual \cdot A \cdot U + y \cdot \left[ 1 - U^\dual \cdot B \cdot U \right]$">|; 

$key = q/displaystylelagrangien(v,y)=biforme{v}{a}{v}+ycdot(1-biforme{v}{b}{v});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle \lagrangien(v,y) = \biforme{v}{a}{v} + y \cdot (1 - \biforme{v}{b}{v})$">|; 

$key = q/displaystylelambda=R(x)=argmin_{zinE_0}R(z)=argmin_{zne0}R(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.92ex; vertical-align: -2.09ex; " SRC="|."$dir".q|img144.svg"
 ALT="$\displaystyle \lambda = R(x) = \arg\min_{z \in E_0} R(z) = \arg\min_{z \ne 0} R(z)$">|; 

$key = q/displaystylelambda=R(x)lebiforme{v}{a}{v}=R(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\displaystyle \lambda = R(x) \le \biforme{v}{a}{v} = R(z)$">|; 

$key = q/displaystylelambda=biforme{u}{a}{u}=frac{biforme{x}{a}{x}}{biforme{x}{b}{x}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\displaystyle \lambda = \biforme{u}{a}{u} = \frac{ \biforme{x}{a}{x} }{ \biforme{x}{b}{x} }$">|; 

$key = q/displaystylelambda=biforme{u}{a}{u}=min_{vinOmega}biforme{v}{a}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\displaystyle \lambda = \biforme{u}{a}{u} = \min_{v \in \Omega} \biforme{v}{a}{v}$">|; 

$key = q/displaystylelim_{ntoinfty}adhmathcal{F}_n=mathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.42ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \lim_{n \to \infty} \adh \mathcal{F}_n = \mathcal{F}$">|; 

$key = q/displaystylenorme{b}=sup{abs{b(u)}:uinmathcal{F},norme{u}=1}strictinferieur+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle \norme{b} = \sup \{ \abs{b(u)} : u \in \mathcal{F},  \norme{u} = 1 \} \strictinferieur +\infty$">|; 

$key = q/displaystylesum_jleft[int_Avarphi_icdotvarphi_jdxright]cdotU_j=int_Avarphi_icdotfdx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.76ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle \sum_j \left[ \int_A \varphi_i \cdot \varphi_j  dx \right] \cdot U_j = \int_A \varphi_i \cdot f  dx$">|; 

$key = q/displaystyleu=argmin_{vinmathcal{F}}I(v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle u = \arg\min_{ v \in \mathcal{F} } I(v)$">|; 

$key = q/displaystyleu_n(x)=sum_{i=1}^nU_icdotvarphi_i(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle u_n(x) = \sum_{i = 1}^n U_i \cdot \varphi_i(x)$">|; 

$key = q/displaystyleu_n=sum_{i=1}^nU_icdotvarphi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle u_n = \sum_{i = 1}^n U_i \cdot \varphi_i$">|; 

$key = q/displaystylevinargmin_{zinmathcal{G}}I(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.75ex; vertical-align: -1.93ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle v \in \arg\min_{ z \in \mathcal{G} } I(z)$">|; 

$key = q/displaystylevinargmin_{zinmathcal{G}}biforme{z-u}{a}{z-u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.75ex; vertical-align: -1.93ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle v \in \arg\min_{ z \in \mathcal{G} } \biforme{z - u}{a}{z - u}$">|; 

$key = q/distance;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\distance$">|; 

$key = q/e;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img73.svg"
 ALT="$e$">|; 

$key = q/e=v-u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img70.svg"
 ALT="$e = v - u$">|; 

$key = q/e_1=u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img150.svg"
 ALT="$e_1 = u$">|; 

$key = q/epsilon=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\epsilon = 0$">|; 

$key = q/epsilon=gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\epsilon = \gamma$">|; 

$key = q/epsilon>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\epsilon &gt; 0$">|; 

$key = q/epsiloninsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\epsilon\in\setR$">|; 

$key = q/f:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img25.svg"
 ALT="$f : A \mapsto \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$f$">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\lambda$">|; 

$key = q/lambda_{n+1}gelambda_nge...gelambda_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\lambda_{n + 1} \ge \lambda_n \ge ... \ge \lambda_1$">|; 

$key = q/mathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\mathcal{F}$">|; 

$key = q/mathcal{F}_n=combilin{varphi_1,varphi_2,...,varphi_n}subseteqmathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\mathcal{F}_n = \combilin{\varphi_1,\varphi_2,...,\varphi_n} \subseteq \mathcal{F}$">|; 

$key = q/mathcal{F}subseteqfonction(A,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\mathcal{F} \subseteq \fonction(A,\setR)$">|; 

$key = q/mathcal{G}subseteqmathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\mathcal{G} \subseteq \mathcal{F}$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img22.svg"
 ALT="$n \in \setN$">|; 

$key = q/partialJ(U)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\partial J(U) = 0$">|; 

$key = q/setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\setR \mapsto \setR$">|; 

$key = q/u+epsiloncdotwinmathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.95ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img10.svg"
 ALT="$u + \epsilon \cdot w \in \mathcal{F}$">|; 

$key = q/u,vinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img52.svg"
 ALT="$u,v \in F$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$u$">|; 

$key = q/u_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img103.svg"
 ALT="$u_n$">|; 

$key = q/u_n=sum_iU_icdotvarphi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img49.svg"
 ALT="$u_n = \sum_i U_i \cdot \varphi_i$">|; 

$key = q/u_napproxu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.66ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img38.svg"
 ALT="$u_n \approx u$">|; 

$key = q/u_ninmathcal{F}_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img23.svg"
 ALT="$u_n\in \mathcal{F}_n$">|; 

$key = q/uinmathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img27.svg"
 ALT="$u \in \mathcal{F}$">|; 

$key = q/v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img72.svg"
 ALT="$v$">|; 

$key = q/v=zslashsqrt{biforme{z}{b}{z}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.81ex; " SRC="|."$dir".q|img137.svg"
 ALT="$v = z / \sqrt{ \biforme{z}{b}{z} }$">|; 

$key = q/varphi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img160.svg"
 ALT="$\varphi_i$">|; 

$key = q/varphi_iinmathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\varphi_i \in \mathcal{F}$">|; 

$key = q/varrhostrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\varrho \strictsuperieur 0$">|; 

$key = q/vinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img35.svg"
 ALT="$v \in F$">|; 

$key = q/vinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img140.svg"
 ALT="$v \in \Omega$">|; 

$key = q/vinmathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img6.svg"
 ALT="$v\in\mathcal{F}$">|; 

$key = q/w=u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img125.svg"
 ALT="$w = u$">|; 

$key = q/w=v-u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img62.svg"
 ALT="$w = v - u$">|; 

$key = q/winE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img116.svg"
 ALT="$w \in E$">|; 

$key = q/winmathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img15.svg"
 ALT="$w \in \mathcal{F}$">|; 

$key = q/winmathcal{G};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.32ex; " SRC="|."$dir".q|img69.svg"
 ALT="$w \in \mathcal{G}$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img142.svg"
 ALT="$x$">|; 

$key = q/x=alphacdotu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img128.svg"
 ALT="$x = \alpha \cdot u$">|; 

$key = q/xslashsqrt{biforme{x}{b}{x}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.81ex; " SRC="|."$dir".q|img148.svg"
 ALT="$x/\sqrt{ \biforme{x}{b}{x} }$">|; 

$key = q/z-u=z-v+v-u=delta+e;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img76.svg"
 ALT="$z - u = z - v + v - u = \delta + e$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img136.svg"
 ALT="$z$">|; 

$key = q/zinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img133.svg"
 ALT="$z \in E$">|; 

$key = q/zinmathcal{G};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.32ex; " SRC="|."$dir".q|img74.svg"
 ALT="$z \in \mathcal{G}$">|; 

$key = q/{Eqts}A=left[biforme{varphi_i}{a}{varphi_j}right]_{i,j}B=left[biforme{varphi_i}{b}{varphi_j}right]_{i,j}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.97ex; vertical-align: -2.92ex; " SRC="|."$dir".q|img156.svg"
 ALT="\begin{Eqts}
A = \left[ \biforme{\varphi_i}{a}{\varphi_j} \right]_{i,j} \\\\
B = \left[ \biforme{\varphi_i}{b}{\varphi_j} \right]_{i,j}
\end{Eqts}">|; 

$key = q/{Eqts}AcdotU=lambdacdotBcdotUU^dualcdotBcdotU=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img159.svg"
 ALT="\begin{Eqts}
A \cdot U = \lambda \cdot B \cdot U \\\\
U^\dual \cdot B \cdot U = 1
\end{Eqts}">|; 

$key = q/{Eqts}AcdotU=lambdacdotUU^dualcdotU=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img163.svg"
 ALT="\begin{Eqts}
A \cdot U = \lambda \cdot U \\\\
U^\dual \cdot U = 1
\end{Eqts}">|; 

$key = q/{Eqts}forme{b}{v}^2lenorme{b}^2cdotnorme{v}^2biforme{v}{a}{v}gevarrhocdotnorme{v}^2{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.88ex; vertical-align: -2.87ex; " SRC="|."$dir".q|img91.svg"
 ALT="\begin{Eqts}
\forme{b}{v}^2 \le \norme{b}^2 \cdot \norme{v}^2 \\\\
\biforme{v}{a}{v} \ge \varrho \cdot \norme{v}^2
\end{Eqts}">|; 

$key = q/{Eqts}partial_vlagrangien(u_n,lambda)=2AcdotU-2lambdacdotBcdotU=0partial_ylagrangien(u_n,lambda)=1-U^dualcdotBcdotU=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img158.svg"
 ALT="\begin{Eqts}
\partial_v \lagrangien(u_n,\lambda) = 2 A \cdot U - 2 \lambda \cdot...
...\partial_y \lagrangien(u_n,\lambda) = 1 - U^\dual \cdot B \cdot U = 0
\end{Eqts}">|; 

$key = q/{J_w:winmathcal{F}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\{ J_w : w \in \mathcal{F} \}$">|; 

$key = q/{eqnarraystar}A&=&left[biforme{varphi_i}{a}{varphi_j}right]_{i,j}B&=&left[forme{b}{varphi_i}right]_iU&=&left[U_iright]_i{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.04ex; " SRC="|."$dir".q|img97.svg"
 ALT="\begin{eqnarray*}
A &amp;=&amp; \left[ \biforme{\varphi_i}{a}{\varphi_j} \right]_{i,j} \...
...t[ \forme{b}{\varphi_i} \right]_i \\\\
U &amp;=&amp; \left[ U_i \right]_i
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}A&=&left[int_Avarphi_icdotvarphi_jdxright]_{i,j}B&=&left[int_Avarphi_icdotfdxright]_iU&=&left[U_iright]_i{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 20.57ex; " SRC="|."$dir".q|img45.svg"
 ALT="\begin{eqnarray*}
A &amp;=&amp; \left[ \int_A \varphi_i \cdot \varphi_j  dx \right]_{i,...
... \varphi_i \cdot f  dx \right]_i \\\\
U &amp;=&amp; \left[ U_i \right]_i
\end{eqnarray*}">|; 

1;

