# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/I:setNsetminus{0}mapstosetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$I : \setN \setminus \{ 0 \} \mapsto \setQ$">|; 

$key = q/I_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img44.svg"
 ALT="$I_0$">|; 

$key = q/I_0strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img45.svg"
 ALT="$I_0 \strictsuperieur 0$">|; 

$key = q/K(epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$K(\epsilon)$">|; 

$key = q/K;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$K$">|; 

$key = q/abs{x-y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\abs{x - y}$">|; 

$key = q/displaystyleI:nmapstoI_n=frac{I_0}{n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.93ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle I : n \mapsto I_n = \frac{I_0}{n}$">|; 

$key = q/displaystyleI=u_K-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle I = u_K - 1$">|; 

$key = q/displaystyleS=u_K+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle S = u_K + 1$">|; 

$key = q/displaystyleabs{u_n-u_K}=u_K-u_nle1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \abs{u_n - u_K} = u_K - u_n \le 1$">|; 

$key = q/displaystyleabs{u_n-u_K}=u_n-u_Kle1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \abs{u_n - u_K} = u_n - u_K \le 1$">|; 

$key = q/displaystyledistance(s_n-t_n,0)=abs{(s_n-t_n)-0}=abs{s_n-t_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \distance(s_n - t_n, 0) = \abs{(s_n - t_n) - 0} = \abs{s_n - t_n}$">|; 

$key = q/displaystyledistance(s_n-t_n,0)leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \distance(s_n - t_n, 0) \le \epsilon$">|; 

$key = q/displaystyledistance(u_m,u_n)=abs{u_m-u_n}le1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \distance(u_m,u_n) = \abs{u_m - u_n} \le 1$">|; 

$key = q/displaystyledistance(x,y)=abs{x-y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \distance(x,y) = \abs{x - y}$">|; 

$key = q/displaystyledistanceparentheses{0,frac{I_0}{n}}=abs{frac{I_0}{n}-0}=abs{frac{I_0}{n}}strictinferieurepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \distance\parentheses{0,\frac{I_0}{n}} = \abs{\frac{I_0}{n} - 0} = \abs{\frac{I_0}{n}} \strictinferieur \epsilon$">|; 

$key = q/displaystyleepsiloncdotunsur{I_0}=frac{epsilon}{I_0}strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \epsilon \cdot \unsur{I_0} = \frac{\epsilon}{I_0} \strictsuperieur 0$">|; 

$key = q/displaystylefrac{I_0}{n}strictinferieurepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.93ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \frac{I_0}{n} \strictinferieur \epsilon$">|; 

$key = q/displaystylelim_{ntoinfty}(s_n-t_n)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \lim_{n \to \infty} (s_n - t_n) = 0$">|; 

$key = q/displaystylelim_{ntoinfty}distance(s_n,t_n)=lim_{ntoinfty}abs{s_n-t_n}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \lim_{n \to \infty} \distance(s_n,t_n) = \lim_{n \to \infty} \abs{s_n - t_n} = 0$">|; 

$key = q/displaystylelim_{ntoinfty}frac{I_0}{n}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.96ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \lim_{n \to \infty} \frac{I_0}{n} = 0$">|; 

$key = q/displaystyles:nmapstos_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle s : n \mapsto s_n$">|; 

$key = q/displaystyleu:nmapstou_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle u : n \mapsto u_n$">|; 

$key = q/displaystyleu_0geu_1ge...geu_kge...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.01ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle u_0 \ge u_1 \ge ... \ge u_k \ge ...$">|; 

$key = q/displaystyleu_0leu_1le...leu_kle...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.01ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle u_0 \le u_1 \le ... \le u_k \le ...$">|; 

$key = q/displaystyleu_kgeu_ngeu_K-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle u_k \ge u_n \ge u_K - 1$">|; 

$key = q/displaystyleu_kleu_nleu_K+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle u_k \le u_n \le u_K + 1$">|; 

$key = q/displaystyleu_ngeI;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle u_n \ge I$">|; 

$key = q/displaystyleu_ngeu_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.01ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle u_n \ge u_0$">|; 

$key = q/displaystyleu_ngeu_K-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle u_n \ge u_K - 1$">|; 

$key = q/displaystyleu_nleS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle u_n \le S$">|; 

$key = q/displaystyleu_nleu_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.01ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle u_n \le u_0$">|; 

$key = q/displaystyleu_nleu_K+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle u_n \le u_K + 1$">|; 

$key = q/displaystyleunsur{n}strictinferieurfrac{epsilon}{I_0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \unsur{n} \strictinferieur \frac{\epsilon}{I_0}$">|; 

$key = q/distance(x,y)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\distance(x,y) = 0$">|; 

$key = q/distance(x,y)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\distance(x,y) \ge 0$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/kgeK(epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$k \ge K(\epsilon)$">|; 

$key = q/kleK;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img33.svg"
 ALT="$k \le K$">|; 

$key = q/m,ngeK;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$m,n \ge K$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img48.svg"
 ALT="$n$">|; 

$key = q/ngeK;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img30.svg"
 ALT="$n \ge K$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img25.svg"
 ALT="$n \in \setN$">|; 

$key = q/nne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img49.svg"
 ALT="$n \ne 0$">|; 

$key = q/s,t:setNmapstosetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$s,t : \setN \mapsto \setQ$">|; 

$key = q/s:setNmapstosetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img1.svg"
 ALT="$s : \setN \mapsto \setQ$">|; 

$key = q/sequivt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$s \equiv t$">|; 

$key = q/setQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\setQ$">|; 

$key = q/u:setNmapstosetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img21.svg"
 ALT="$u : \setN \mapsto \setQ$">|; 

$key = q/u_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img26.svg"
 ALT="$u_0$">|; 

$key = q/x,yinsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$x,y \in \setQ$">|; 

$key = q/x-y=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$x - y = 0$">|; 

$key = q/x=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$x = y$">|; 

$key = q/{eqnarraystar}distance(x,z)&=&abs{x-z}&le&abs{x-y}+abs{y-z}&le&distance(x,y)+distance(y,z){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img11.svg"
 ALT="\begin{eqnarray*}
\distance(x,z) &amp;=&amp; \abs{x - z} \\\\
&amp;\le&amp; \abs{x - y} + \abs{y - z} \\\\
&amp;\le&amp; \distance(x,y) + \distance(y,z)
\end{eqnarray*}">|; 

1;

