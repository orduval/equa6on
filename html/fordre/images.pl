# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img48.svg"
 ALT="$A$">|; 

$key = q/AsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img46.svg"
 ALT="$A \subseteq \Omega$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$B$">|; 

$key = q/M=max{f,g};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$M = \max\{f,g\}$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\Omega$">|; 

$key = q/U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img59.svg"
 ALT="$U$">|; 

$key = q/Z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$Z$">|; 

$key = q/ainA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img58.svg"
 ALT="$a \in A$">|; 

$key = q/c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$c$">|; 

$key = q/cinB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img32.svg"
 ALT="$c \in B$">|; 

$key = q/displaystylealpha=argmax_{xinA}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \alpha = \arg\max_{x \in A} f(x)$">|; 

$key = q/displaystylealpha=argument_Omegasup_{xinA}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle \alpha = \argument_\Omega\sup_{x \in A} f(x)$">|; 

$key = q/displaystyleargmax_{xinA}f(x)=left{alphainA:f(alpha)=max_{xinA}f(x)right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \arg\max_{x \in A} f(x) = \left\{ \alpha \in A : f(\alpha) = \max_{x \in A} f(x) \right\}$">|; 

$key = q/displaystyleargmin_{xinA}f(x)=left{betainA:f(beta)=min_{xinA}f(x)right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \arg\min_{x \in A} f(x) = \left\{ \beta \in A : f(\beta) = \min_{x \in A} f(x) \right\}$">|; 

$key = q/displaystyleargument_Omegainf_{xinA}f(x)=left{betainOmega:f(beta)=inf_{xinA}f(x)right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \argument_\Omega\inf_{x \in A} f(x) = \left\{ \beta \in \Omega : f(\beta) = \inf_{x \in A} f(x) \right\}$">|; 

$key = q/displaystyleargument_Omegasup_{xinA}f(x)=left{alphainOmega:f(alpha)=sup_{xinA}f(x)right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \argument_\Omega\sup_{x \in A} f(x) = \left\{ \alpha \in \Omega : f(\alpha) = \sup_{x \in A} f(x) \right\}$">|; 

$key = q/displaystylebeta=argmin_{xinA}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \beta = \arg\min_{x \in A} f(x)$">|; 

$key = q/displaystylebeta=argument_Omegainf_{xinA}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.05ex; vertical-align: -2.23ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle \beta = \argument_\Omega\inf_{x \in A} f(x)$">|; 

$key = q/displaystylef(a)gef(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle f(a) \ge f(x)$">|; 

$key = q/displaystylef(a)lef(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle f(a) \le f(x)$">|; 

$key = q/displaystylef(x)gec;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle f(x) \ge c$">|; 

$key = q/displaystylef(x)gef(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle f(x) \ge f(y)$">|; 

$key = q/displaystylef(x)geg(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle f(x) \ge g(x)$">|; 

$key = q/displaystylef(x)lec;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle f(x) \le c$">|; 

$key = q/displaystylef(x)lef(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle f(x) \le f(y)$">|; 

$key = q/displaystylef(x)leg(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle f(x) \le g(x)$">|; 

$key = q/displaystylef(x)strictinferieurf(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle f(x) \strictinferieur f(y)$">|; 

$key = q/displaystylef(x)strictsuperieurf(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle f(x) \strictsuperieur f(y)$">|; 

$key = q/displaystylefgec;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle f \ge c$">|; 

$key = q/displaystylefgeg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle f \ge g$">|; 

$key = q/displaystyleflec;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle f \le c$">|; 

$key = q/displaystylefleg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle f \le g$">|; 

$key = q/displaystyleinf_{xinA}f(x)=inff(A)leinfg(A)=inf_{xinA}g(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle \inf_{x \in A} f(x) = \inf f(A) \le \inf g(A) = \inf_{x \in A} g(x)$">|; 

$key = q/displaystyleleft[inf_{zinZ}f_zright](x)=inf_{zinZ}f_z(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \left[ \inf_{z \in Z} f_z \right](x) = \inf_{z \in Z} f_z(x)$">|; 

$key = q/displaystyleleft[sup_{zinZ}f_zright](x)=sup_{zinZ}f_z(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \left[ \sup_{z \in Z} f_z \right](x) = \sup_{z \in Z} f_z(x)$">|; 

$key = q/displaystylemaxminorf(A)lemaxminorg(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle \max \minor f(A) \le \max \minor g(A)$">|; 

$key = q/displaystyleminmajorf(A)leminmajorg(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \min \major f(A) \le \min \major g(A)$">|; 

$key = q/displaystylesup_{xinA}f(x)=supf(A)lesupg(A)=sup_{xinA}g(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \sup_{x \in A} f(x) = \sup f(A) \le \sup g(A) = \sup_{x \in A} g(x)$">|; 

$key = q/displaystyle{f_zinB^A:zinZ};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \{ f_z \in B^A : z \in Z \}$">|; 

$key = q/f(x)strictinferieurc;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$f(x) \strictinferieur c$">|; 

$key = q/f(x)strictinferieurg(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$f(x) \strictinferieur g(x)$">|; 

$key = q/f(x)strictsuperieurc;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$f(x) \strictsuperieur c$">|; 

$key = q/f(x)strictsuperieurg(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$f(x) \strictsuperieur g(x)$">|; 

$key = q/f,g:AmapstoB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img64.svg"
 ALT="$f,g : A \mapsto B$">|; 

$key = q/f,ginB^A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$f,g \in B^A$">|; 

$key = q/f:AtoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f : A \to F$">|; 

$key = q/f:OmegamapstoB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img45.svg"
 ALT="$f : \Omega \mapsto B$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$f$">|; 

$key = q/finB^A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img31.svg"
 ALT="$f \in B^A$">|; 

$key = q/fleg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img65.svg"
 ALT="$f \le g$">|; 

$key = q/fstrictinferieurc;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img38.svg"
 ALT="$f \strictinferieur c$">|; 

$key = q/fstrictinferieurg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img18.svg"
 ALT="$f \strictinferieur g$">|; 

$key = q/fstrictsuperieurc;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img40.svg"
 ALT="$f \strictsuperieur c$">|; 

$key = q/fstrictsuperieurg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img20.svg"
 ALT="$f \strictsuperieur g$">|; 

$key = q/g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$g$">|; 

$key = q/lambdainminorf(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\lambda \in \minor f(A)$">|; 

$key = q/lambdainminorg(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\lambda \in \minor g(A)$">|; 

$key = q/lambdalef(x)leg(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\lambda \le f(x) \le g(x)$">|; 

$key = q/lambdaleg(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\lambda \le g(x)$">|; 

$key = q/m=min{f,g};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$m = \min\{f,g\}$">|; 

$key = q/majorg(A)subseteqmajorf(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\major g(A) \subseteq \major f(A)$">|; 

$key = q/max{f,c};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\max\{f,c\}$">|; 

$key = q/minorf(A)subseteqminorg(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\minor f(A) \subseteq \minor g(A)$">|; 

$key = q/min{f,c};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\min\{f,c\}$">|; 

$key = q/sigmagef(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\sigma \ge f(x)$">|; 

$key = q/sigmageg(x)gef(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\sigma \ge g(x) \ge f(x)$">|; 

$key = q/sigmainmajorf(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\sigma \in \major f(A)$">|; 

$key = q/sigmainmajorg(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\sigma \in \major g(A)$">|; 

$key = q/x,yinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$x,y \in A$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img60.svg"
 ALT="$x$">|; 

$key = q/xgey;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$x \ge y$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img15.svg"
 ALT="$x \in A$">|; 

$key = q/xinU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img62.svg"
 ALT="$x \in U$">|; 

$key = q/xstrictinferieury;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$x \strictinferieur y$">|; 

$key = q/{Eqts}M(x)=max{f,g}(x)=max{f(x),g(x)}m(x)=min{f,g}(x)=min{f(x),g(x)}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img30.svg"
 ALT="\begin{Eqts}
M(x) = \max\{ f , g \}(x) = \max\{ f(x) , g(x) \} \\\\
m(x) = \min\{ f , g \}(x) = \min\{ f(x) , g(x) \}
\end{Eqts}">|; 

$key = q/{Eqts}left[max_{zinZ}f_zright](x)=max_{zinZ}f_z(x)left[min_{zinZ}f_zright](x)=min_{zinZ}f_z(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.64ex; vertical-align: -7.27ex; " SRC="|."$dir".q|img26.svg"
 ALT="\begin{Eqts}
\left[ \max_{z \in Z} f_z \right](x) = \max_{z \in Z} f_z(x) \ \\\\
\left[ \min_{z \in Z} f_z \right](x) = \min_{z \in Z} f_z(x)
\end{Eqts}">|; 

$key = q/{Eqts}max_{xinA}f(x)=max{f(x):xinA}min_{xinA}f(x)=min{f(x):xinA}sup_{xinA}f(x)=sup{f(x):xinA}inf_{xinA}f(x)=inf{f(x):xinA}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 27.99ex; vertical-align: -13.47ex; " SRC="|."$dir".q|img47.svg"
 ALT="\begin{Eqts}
\max_{x \in A} f(x) = \max \{ f(x) : x \in A \} \ \\\\
\min_{x \in ...
...) : x \in A \} \ \\\\
\inf_{x \in A} f(x) = \inf \{ f(x) : x \in A \}
\end{Eqts}">|; 

$key = q/{Eqts}max{f,c}(x)=max{f(x),c}min{f,c}(x)=min{f(x),c}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img44.svg"
 ALT="\begin{Eqts}
\max\{ f , c \}(x) = \max\{ f(x) , c \} \\\\
\min\{ f , c \}(x) = \min\{ f(x) , c \}
\end{Eqts}">|; 

$key = q/{Eqts}sup{f_z:zinZ}=sup_{zinZ}f_zinf{f_z:zinZ}=inf_{zinZ}f_zmax{f_z:zinZ}=max_{zinZ}f_zmin{f_z:zinZ}=min_{zinZ}f_z{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 27.99ex; vertical-align: -13.47ex; " SRC="|."$dir".q|img27.svg"
 ALT="\begin{Eqts}
\sup \{ f_z : z \in Z \} = \sup_{z \in Z} f_z \ \\\\
\inf \{ f_z : ...
...ax_{z \in Z} f_z \ \\\\
\min \{ f_z : z \in Z \} = \min_{z \in Z} f_z
\end{Eqts}">|; 

1;

