# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.55ex; vertical-align: -5.22ex; " SRC="|."$dir".q|img14.svg"
 ALT="\begin{Eqts}
\int_0^\pi T_m(\cos(\theta)) T_n(\cos(\theta)) d\theta =
\int_0^\pi...
...(m+n)\theta] d\theta +
\unsur{2} \int_0^\pi \cos[(m-n)\theta] d\theta
\end{Eqts}">|; 

$key = q/T_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img1.svg"
 ALT="$T_n$">|; 

$key = q/displaystyle(T_ncircT_m)(x)=T_{mcdotn}(x)=(T_mcircT_n)(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle (T_n \circ T_m)(x) = T_{m \cdot n}(x) = (T_m \circ T_n)(x)$">|; 

$key = q/displaystyleT_nleft[cosleft(frac{2k+1}{2n}right)right]=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle T_n \left[ \cos\left(\frac{2k+1}{2n}\right) \right] = 0$">|; 

$key = q/displaystyleT_{n+1}(x)+T_{n-1}(x)=2xT_n(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle T_{n+1}(x) + T_{n-1}(x) = 2 x T_n(x)$">|; 

$key = q/displaystyleT_{n+m}(x)+T_{n-m}(x)=2T_n(x)T_m(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle T_{n+m}(x) + T_{n-m}(x) = 2 T_n(x) T_m(x)$">|; 

$key = q/displaystyleint_0^piT_0(cos(theta))T_0(cos(theta))dtheta=int_0^pidtheta=pi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \int_0^\pi T_0(\cos(\theta)) T_0(\cos(\theta)) d\theta = \int_0^\pi d\theta = \pi$">|; 

$key = q/displaystyleint_0^piT_m(cos(theta))T_n(cos(theta))dtheta=unsur{2n}[sin(2npi)-sin(0)]+frac{pi}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \int_0^\pi T_m(\cos(\theta)) T_n(\cos(\theta)) d\theta = \unsur{2n} [\sin(2 n \pi) - \sin(0)] + \frac{\pi}{2}$">|; 

$key = q/displaystyleint_0^piT_m(costheta)T_n(costheta)dtheta=int_{-1}^1frac{T_m(x)T_n(x)}{sqrt{1-x^2}}dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.98ex; vertical-align: -2.36ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \int_0^\pi T_m(\cos\theta) T_n(\cos\theta) d\theta = \int_{-1}^1 \frac{T_m(x) T_n(x)}{\sqrt{1-x^2}}dx$">|; 

$key = q/displaystyleint_{-1}^1frac{T_m(x)T_n(x)}{sqrt{1-x^2}}dx=delta_{mn}frac{pi}{2}(1+delta_{m,0});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.98ex; vertical-align: -2.36ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \int_{-1}^1 \frac{T_m(x) T_n(x)}{\sqrt{1-x^2}} dx = \delta_{mn} \frac{\pi}{2} (1+\delta_{m,0})$">|; 

$key = q/displaystyleint_{-infty}^{+infty}H_n(x)H_m(x)exp(-x^2)dx=2^nn!sqrt{pi}delta_{mn};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.92ex; vertical-align: -2.36ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \int_{-\infty}^{+\infty} H_n(x) H_m(x) \exp(-x^2) dx = 2^n n ! \sqrt{\pi} \delta_{mn}$">|; 

$key = q/displaystyleint_{0}^{+infty}L_n(x)L_m(x)exp(-x)dx=delta_{mn};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \int_{0}^{+\infty} L_n(x) L_m(x) \exp(-x) dx = \delta_{mn}$">|; 

$key = q/displaystylex_k=cosleft(frac{2k+1}{2n}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle x_k = \cos\left(\frac{2k+1}{2n}\right)$">|; 

$key = q/k=0,1,...,n-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$k=0,1,...,n-1$">|; 

$key = q/m,ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img25.svg"
 ALT="$m,n\in\setN$">|; 

$key = q/m,nne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img13.svg"
 ALT="$m, n \ne 0$">|; 

$key = q/m=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$m = n$">|; 

$key = q/m=n=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$m = n = 0$">|; 

$key = q/mnen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img18.svg"
 ALT="$m \ne n$">|; 

$key = q/sin;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.73ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\sin$">|; 

$key = q/x=cos(theta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$x = \cos(\theta)$">|; 

$key = q/{Eqts}H_0(x)=1H_1(x)=2xH_{n+1}(x)=2xH_n(x)-2nH_{n-1}(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img27.svg"
 ALT="\begin{Eqts}
H_0(x) = 1 \\\\
H_1(x) = 2 x \\\\
H_{n+1}(x) = 2 x H_n(x) - 2 n H_{n-1}(x) \\\\
\end{Eqts}">|; 

$key = q/{Eqts}L_0(x)=1L_1(x)=1-x(n+1)L_{n+1}(x)=(2n+1-x)L_n(x)-nL_{n-1}(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img29.svg"
 ALT="\begin{Eqts}
L_0(x) = 1 \\\\
L_1(x) = 1-x \\\\
(n+1) L_{n+1}(x) = (2 n + 1 - x) L_n(x) - n L_{n-1}(x)
\end{Eqts}">|; 

$key = q/{Eqts}T_0(x)=1T_1(x)=x{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img3.svg"
 ALT="\begin{Eqts}
T_0(x) = 1 \\\\
T_1(x) = x
\end{Eqts}">|; 

$key = q/{Eqts}T_n:xmapstocos(narccos(x))T_n(cos(theta))=cos(ntheta){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img2.svg"
 ALT="\begin{Eqts}
T_n : x\mapsto\cos(n \arccos(x) ) \\\\
T_n(\cos(\theta)) = \cos(n \theta)
\end{Eqts}">|; 

$key = q/{Eqts}cos[(n+m)theta]=cos[ntheta]cos[mtheta]+sin[ntheta]sin[mtheta]cos[(n-m)theta]=cos[ntheta]cos[mtheta]-sin[ntheta]sin[mtheta]{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img4.svg"
 ALT="\begin{Eqts}
\cos[(n+m)\theta]= \cos[n\theta]\cos[m\theta]+\sin[n\theta]\sin[m\t...
...s[(n-m)\theta]= \cos[n\theta]\cos[m\theta]-\sin[n\theta]\sin[m\theta]
\end{Eqts}">|; 

$key = q/{Eqts}dx=-sin(theta)dthetadtheta=frac{dx}{sqrt{1-x^2}}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.09ex; vertical-align: -3.98ex; " SRC="|."$dir".q|img22.svg"
 ALT="\begin{Eqts}
dx = -\sin(\theta) d\theta \\\\
d\theta = \frac{dx}{\sqrt{1-x^2}}
\end{Eqts}">|; 

$key = q/{Eqts}int_0^piT_m(cos(theta))T_n(cos(theta))dtheta=unsur{2(m+n)}[sin[(m+n)pi]-sin(0)]+unsur{2(m-n)}[sin[(m-n)pi]-sin(0)]=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 17.48ex; vertical-align: -8.19ex; " SRC="|."$dir".q|img19.svg"
 ALT="\begin{Eqts}
\int_0^\pi T_m(\cos(\theta)) T_n(\cos(\theta)) d\theta = \\\\
\unsur...
...n)\pi] - \sin(0)] + \\\\
\unsur{2(m-n)} [\sin[(m-n)\pi] - \sin(0)] = 0
\end{Eqts}">|; 

$key = q/{Eqts}int_0^piT_m(costheta)T_n(costheta)dtheta=cases{pi&mbox{si}m=0pislash2&mbox{si}m=nne00&mbox{si}mnencases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.26ex; vertical-align: -4.57ex; " SRC="|."$dir".q|img20.svg"
 ALT="\begin{Eqts}
\int_0^\pi T_m(\cos\theta) T_n(\cos\theta) d\theta =
\begin{cases}
...
...
\pi/2 &amp; \mbox{si } m = n \ne 0 \\\\
0 &amp; \mbox{si } m \ne n
\end{cases}\end{Eqts}">|; 

1;

