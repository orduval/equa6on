# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.09ex; vertical-align: -6.49ex; " SRC="|."$dir".q|img38.svg"
 ALT="\begin{Eqts}
\int_{\setR} \varphi^{(n)}(x) dx = \sum_k a_k^0 \int_{\setR} \varph...
... a_k^0 \right) \frac{1}{m} \int_{\setR} \varphi^{(n-1)}(\xi) d\xi = 1
\end{Eqts}">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$1$">|; 

$key = q/1slash2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$1/2$">|; 

$key = q/JleR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img89.svg"
 ALT="$J \le R$">|; 

$key = q/K_j(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img84.svg"
 ALT="$K_j(x)$">|; 

$key = q/M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img59.svg"
 ALT="$M$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img105.svg"
 ALT="$R$">|; 

$key = q/S_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img18.svg"
 ALT="$S_j$">|; 

$key = q/a^i_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img33.svg"
 ALT="$a^i_k$">|; 

$key = q/a_0^0=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img6.svg"
 ALT="$a_0^0 = 1$">|; 

$key = q/a_0^1=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img8.svg"
 ALT="$a_0^1 = 1$">|; 

$key = q/a_1^0=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img7.svg"
 ALT="$a_1^0 = 1$">|; 

$key = q/a_1^1=-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img9.svg"
 ALT="$a_1^1 = -1$">|; 

$key = q/c_{Rk}=scalaire{f}{varphi_{Rk}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img88.svg"
 ALT="$c_{Rk} = \scalaire{f}{\varphi_{Rk}}$">|; 

$key = q/c_{j+1,p};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img103.svg"
 ALT="$c_{j+1,p}$">|; 

$key = q/c_{jk};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img94.svg"
 ALT="$c_{jk}$">|; 

$key = q/d^i_{jk};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.96ex; vertical-align: -1.00ex; " SRC="|."$dir".q|img95.svg"
 ALT="$d^i_{jk}$">|; 

$key = q/displaystyle#K_j(x)le2g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \char93  K_j(x) \le 2 g$">|; 

$key = q/displaystyleK_j(x)={kinsetZ:varphi_{jk}(x)ne0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle K_j(x) = \{ k \in \setZ : \varphi_{jk}(x) \ne 0 \}$">|; 

$key = q/displaystylea_k^iquad:quadi=0,1,...,m-1qquadk,minsetZ,m>1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle a_k^i \quad : \quad i = 0,1,...,m-1 \qquad k, m \in \setZ, m &gt; 1$">|; 

$key = q/displaystyleabs{f(x)-f(y)}leepsilon_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \abs{f(x)-f(y)} \le \epsilon_j$">|; 

$key = q/displaystyleabs{x-y}leindicatrice_0m^{-j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.75ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle \abs{x-y} \le \indicatrice_0 m^{-j}$">|; 

$key = q/displaystylec_{Rk}approxm^{Rslash2}fleft(frac{k}{m^R}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle c_{Rk} \approx m^{R/2} f \left(\frac{k}{m^R}\right)$">|; 

$key = q/displaystylef(x)=lim_{Rto+infty}sum_kc_{Rk}varphi_{Rk}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\displaystyle f(x) = \lim_{R \to +\infty} \sum_k c_{Rk} \varphi_{Rk}(x)$">|; 

$key = q/displaystylef(x)=sum_kc_{Jk}varphi_{Jk}(x)+sum_ksum_{i=1}^{m-1}sum_{j=J}^{+infty}d^i_{jk}psi^i_{jk}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.56ex; vertical-align: -3.35ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle f(x) = \sum_k c_{Jk} \varphi_{Jk}(x) + \sum_k \sum_{i=1}^{m-1} \sum_{j=J}^{+\infty} d^i_{jk} \psi^i_{jk}(x)$">|; 

$key = q/displaystylef(x)=sum_kc_{Jk}varphi_{Jk}(x)+sum_ksum_{i=1}^{m-1}sum_{j=J}^{R}d^i_{jk}psi^i_{jk}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.62ex; vertical-align: -3.35ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle f(x) = \sum_k c_{Jk} \varphi_{Jk}(x) + \sum_k \sum_{i=1}^{m-1} \sum_{j=J}^{R} d^i_{jk} \psi^i_{jk}(x)$">|; 

$key = q/displaystylef(x)=sum_kc_{Rk}varphi_{Rk}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle f(x) = \sum_k c_{Rk} \varphi_{Rk}(x)$">|; 

$key = q/displaystylef(x)=sum_kvarphi_{jk}(x)int_{setR}f(x)varphi_{jk}(y)dy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.31ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle f(x) = \sum_k \varphi_{jk}(x) \int_{\setR} f(x) \varphi_{jk}(y) dy$">|; 

$key = q/displaystylef(x)approxtilde{f}(x)=sum_kc_{Rk}varphi_{Rk}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle f(x) \approx \tilde{f}(x) = \sum_k c_{Rk} \varphi_{Rk}(x)$">|; 

$key = q/displaystylef=sum_kscalaire{f}{varphi_{jk}}varphi_{jk};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle f = \sum_k\scalaire{f}{\varphi_{jk}}\varphi_{jk}$">|; 

$key = q/displaystylelim_{jrightarrow+infty}epsilon_j=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.74ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \lim_{j \rightarrow +\infty} \epsilon_j = 0$">|; 

$key = q/displaystylem^jx-kinsupport(varphi)Rightarrowm^jx-2gleklem^jx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.75ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle m^j x - k \in \support(\varphi) \Rightarrow m^j x - 2 g \le k \le m^j x$">|; 

$key = q/displaystylemu([a,b])=abs{b-a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle \mu([a,b]) = \abs{b-a}$">|; 

$key = q/displaystylemu(support(varphi_{jk}))=m^{-j}mu(support(varphi))=m^{-j}indicatrice_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.82ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \mu(\support(\varphi_{jk})) = m^{-j} \mu(\support(\varphi)) = m^{-j} \indicatrice_0$">|; 

$key = q/displaystylescalaire{u}{v}=int_{setR}u(x)v(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \scalaire{u}{v} = \int_{\setR} u(x) v(x) dx$">|; 

$key = q/displaystylesum_kscalaire{f}{varphi_{jk}}varphi_{jk}(x)=sum_{kinK_j(x)}scalaire{f}{varphi_{jk}}varphi_{jk}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.13ex; vertical-align: -3.62ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle \sum_k\scalaire{f}{\varphi_{jk}}\varphi_{jk}(x) = \sum_{k \in K_j(x) } \scalaire{f}{\varphi_{jk}}\varphi_{jk}(x)$">|; 

$key = q/displaystylesum_kvarphi(x-k)=1qquadforallxinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle \sum_k \varphi(x-k) = 1 \qquad \forall x \in \setR$">|; 

$key = q/displaystylesum_kvarphi(x-k)=lim_{nrightarrow+infty}sum_kvarphi^{(n)}(x-k)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \sum_k \varphi(x-k) = \lim_{n \rightarrow +\infty} \sum_k \varphi^{(n)}(x-k) = 1$">|; 

$key = q/displaystylesum_kvarphi^{(n)}(x-k)=sum_pvarphi^{(n-1)}(mx-p)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -3.30ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \sum_k \varphi^{(n)}(x-k) = \sum_p \varphi^{(n-1)}(m x - p) = 1$">|; 

$key = q/displaystylesum_kvarphi^{(n)}(x-k)=sum_pvarphi^{(n-1)}(mx-p)sum_ka^0_{p-mk};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -3.30ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \sum_k \varphi^{(n)}(x-k) = \sum_p \varphi^{(n-1)}(m x - p) \sum_k a^0_{p - m k}$">|; 

$key = q/displaystylesum_kvarphi_{jk}(x)=m^{jslash2}sum_kvarphi(m^jx-k)=m^{jslash2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle \sum_k \varphi_{jk}(x) = m^{j/2} \sum_k \varphi( m^j x - k ) = m^{j/2}$">|; 

$key = q/displaystylesup_{xiinsetR}varphi_{jk}(xi)=m^{jslash2}sup_{xiinsetR}varphi(xi)=m^{jslash2}M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.75ex; vertical-align: -2.54ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \sup_{\xi\in\setR} \varphi_{jk}(\xi) = m^{j/2} \sup_{\xi\in\setR} \varphi(\xi) = m^{j/2} M$">|; 

$key = q/displaystylesupport(varphi)subseteqleft[0,1+(g-1)sum_{k=0}^{+infty}left(frac{1}{m}right)^kright]=left[0,1+(g-1)frac{m}{m-1}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \support(\varphi) \subseteq \left[0, 1 + (g-1) \sum_{k=0}^{+\infty} \left( \frac{1}{m} \right)^k \right] = \left[0, 1 + (g-1) \frac{m}{m-1} \right]$">|; 

$key = q/displaystyletilde{f}(x)=sum_kc_{Jk}varphi_{Jk}(x)+sum_ksum_{i=1}^{m-1}sum_{j=J}^{R}d^i_{jk}psi^i_{jk}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.62ex; vertical-align: -3.35ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle \tilde{f}(x) = \sum_k c_{Jk} \varphi_{Jk}(x) + \sum_k \sum_{i=1}^{m-1} \sum_{j=J}^{R} d^i_{jk} \psi^i_{jk}(x)$">|; 

$key = q/displaystylevarphi(x)=sum_ka_k^0varphi(mx-k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \varphi(x) = \sum_k a_k^0 \varphi(m x - k)$">|; 

$key = q/displaystylevarphi=lim_{nrightarrowinfty}varphi^{(n)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.93ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \varphi = \lim_{n\rightarrow\infty} \varphi^{(n)}$">|; 

$key = q/displaystylevarphi^{(0)}(x)=indicatrice_{[0,1[}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.05ex; vertical-align: -0.85ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \varphi^{(0)}(x) = \indicatrice_{[0,1[}(x)$">|; 

$key = q/displaystylevarphi^{(n)}(x)=sum_ka_k^0varphi^{(n-1)}(mx-k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \varphi^{(n)}(x) = \sum_k a_k^0 \varphi^{(n-1)}(m x - k)$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$f$">|; 

$key = q/g>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img25.svg"
 ALT="$g &gt; 0$">|; 

$key = q/indicatrice_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\indicatrice_0$">|; 

$key = q/int_{setR}varphi^{(0)}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\int_{\setR} \varphi^{(0)} = 1$">|; 

$key = q/int_{setR}varphi^{(n-1)}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\int_{\setR} \varphi^{(n-1)} = 1$">|; 

$key = q/j+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img93.svg"
 ALT="$j+1$">|; 

$key = q/j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img19.svg"
 ALT="$j$">|; 

$key = q/j=R-1,R-2,...,J;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img104.svg"
 ALT="$j = R-1,R-2,...,J$">|; 

$key = q/jinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$j \in \setN$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img83.svg"
 ALT="$k$">|; 

$key = q/kinK_j(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img68.svg"
 ALT="$k\in K_j(x)$">|; 

$key = q/m^jxinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img78.svg"
 ALT="$m^j x \in \setR$">|; 

$key = q/m^{jslash2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img54.svg"
 ALT="$m^{j/2}$">|; 

$key = q/mathcal{O}(N);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\mathcal{O}(N)$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\mu$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$n$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img48.svg"
 ALT="$p$">|; 

$key = q/p=mk+l;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img45.svg"
 ALT="$p = m k + l$">|; 

$key = q/psi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\psi$">|; 

$key = q/scalaire{psi^i_{jk}}{psi^i_{jk}}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -1.00ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\scalaire{\psi^i_{jk} }{\psi^i_{jk} } = 1$">|; 

$key = q/scalaire{varphi_{jk}}{varphi_{jk}}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\scalaire{\varphi_{jk} }{\varphi_{jk} } = 1$">|; 

$key = q/sum_k=sum_{kinsetZ};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.70ex; vertical-align: -0.87ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\sum_k = \sum_{k \in \setZ}$">|; 

$key = q/sum_ka^0_{p-mk}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.93ex; vertical-align: -1.00ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\sum_k a^0_{p - m k} = 1$">|; 

$key = q/sum_kvarphi^{(0)}(x-k)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.87ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\sum_k \varphi^{(0)}(x-k) = 1$">|; 

$key = q/sum_kvarphi^{(n-1)}(x-k)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.87ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\sum_k \varphi^{(n-1)}(x-k) = 1$">|; 

$key = q/varphi(2x),varphi(2x-1),psi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\varphi(2x), \varphi(2x-1),\psi(x) $">|; 

$key = q/varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\varphi(x)$">|; 

$key = q/varphi,psi^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.50ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\varphi,\psi^i$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\varphi$">|; 

$key = q/varphi_{j+1,p};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\varphi_{j+1,p}$">|; 

$key = q/varphi_{jk}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\varphi_{jk}(x)$">|; 

$key = q/varphi_{jk}:xmapstovarphi(2^jx-k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.70ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\varphi_{jk} : x \mapsto \varphi( 2^j x - k )$">|; 

$key = q/varphi_{jk};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\varphi_{jk}$">|; 

$key = q/x,yinsupport(varphi_{jk});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img72.svg"
 ALT="$x,y \in \support(\varphi_{jk})$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img70.svg"
 ALT="$x$">|; 

$key = q/xi=m^jy-k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.50ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\xi = m^j y - k$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img66.svg"
 ALT="$x \in \setR$">|; 

$key = q/{Eqts}a^i_k=0qquadforallknotin{0,1,2,....mg-1}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.01ex; vertical-align: -0.93ex; " SRC="|."$dir".q|img24.svg"
 ALT="\begin{Eqts}
a^i_k = 0 \qquad \forall k \notin \{0,1,2,.... m g - 1 \}
\end{Eqts}">|; 

$key = q/{Eqts}c_{jk}=scalaire{f}{varphi_{jk}}d^i_{jk}=scalaire{f}{psi^i_{jk}}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.61ex; vertical-align: -2.74ex; " SRC="|."$dir".q|img91.svg"
 ALT="\begin{Eqts}
c_{jk} = \scalaire{ f }{ \varphi_{jk} } \\\\
d^i_{jk} = \scalaire{ f }{ \psi^i_{jk} }
\end{Eqts}">|; 

$key = q/{Eqts}f(x)=f_1varphi(2x)+f_2varphi(2x-1)f(x)=frac{f_1+f_2}{2}varphi(x)+frac{f_1-f_2}{2}psi(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.52ex; vertical-align: -3.70ex; " SRC="|."$dir".q|img11.svg"
 ALT="\begin{Eqts}
f(x) = f_1 \varphi(2x) + f_2 \varphi(2x-1) \\\\
f(x) = \frac{f_1+f_2}{2} \varphi(x) + \frac{f_1 - f_2}{2} \psi(x)
\end{Eqts}">|; 

$key = q/{Eqts}int_{setR}psi^i(x)dx=sum_ka_k^iint_{setR}varphi(mx-k)dxint_{setR}psi^i(x)dx=left(sum_ka_k^iright)frac{1}{m}int_{setR}varphi(xi)dxi=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.09ex; vertical-align: -6.49ex; " SRC="|."$dir".q|img39.svg"
 ALT="\begin{Eqts}
\int_{\setR} \psi^i(x) dx = \sum_k a_k^i \int_{\setR} \varphi(m x -...
...( \sum_k a_k^i \right) \frac{1}{m} \int_{\setR} \varphi(\xi) d\xi = 0
\end{Eqts}">|; 

$key = q/{Eqts}int_{setR}varphi(x)dx=1int_{setR}psi^i(x)dx=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.44ex; vertical-align: -5.16ex; " SRC="|."$dir".q|img35.svg"
 ALT="\begin{Eqts}
\int_{\setR} \varphi(x) dx = 1 \\\\
\int_{\setR} \psi^i(x) dx = 0
\end{Eqts}">|; 

$key = q/{Eqts}sum_ka^0_{p+mk}=1sum_ka_k^i=mindicatrice_{i,0}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.75ex; vertical-align: -5.32ex; " SRC="|."$dir".q|img22.svg"
 ALT="\begin{Eqts}
\sum_k a^0_{p+m k} = 1 \\\\
\sum_k a_k^i = m \indicatrice_{i,0}
\end{Eqts}">|; 

$key = q/{Eqts}sum_ka^0_{p-mk}=1sum_ksum_{i=0}^{m-1}a^i_{r-m.k}a^i_{s-m.k}=mindicatrice_{rs}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.44ex; vertical-align: -6.16ex; " SRC="|."$dir".q|img26.svg"
 ALT="\begin{Eqts}
\sum_k a^0_{p-m k} = 1 \\\\
\sum_k\sum_{i=0}^{m-1} a^i_{r-m.k} a^i_{s-m.k} = m \indicatrice_{rs} \\\\
\end{Eqts}">|; 

$key = q/{Eqts}sum_ka_{k+mr}^ia_{k+ms}^j=mindicatrice_{ij}indicatrice_{rs}sum_ksum_{i=0}^{m-1}a^i_{r+mk}a^i_{s+mk}=mindicatrice_{rs}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.44ex; vertical-align: -6.16ex; " SRC="|."$dir".q|img23.svg"
 ALT="\begin{Eqts}
\sum_k a_{k + m r}^i a_{k + m s}^j = m \indicatrice_{ij} \indicatri...
...
\sum_k\sum_{i=0}^{m-1} a^i_{r+m k} a^i_{s+m k} = m \indicatrice_{rs}
\end{Eqts}">|; 

$key = q/{Eqts}sum_kvarphi^{(n)}(x-k)=sum_ksum_la^0_lvarphi^{(n-1)}left(m(x-k)-lright)sum_kvarphi^{(n)}(x-k)=sum_lsum_ka^0_lvarphi^{(n-1)}(mx-mk-l){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.75ex; vertical-align: -5.32ex; " SRC="|."$dir".q|img44.svg"
 ALT="\begin{Eqts}
\sum_k \varphi^{(n)}(x-k) = \sum_k\sum_l a^0_l \varphi^{(n-1)}\left...
...varphi^{(n)}(x-k) = \sum_l\sum_k a^0_l \varphi^{(n-1)}(m x - m k - l)
\end{Eqts}">|; 

$key = q/{Eqts}varphi(2x)=frac{1}{2}(varphi(x)+psi(x))varphi(2x-1)=frac{1}{2}(varphi(x)-psi(x)){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.25ex; vertical-align: -4.57ex; " SRC="|."$dir".q|img10.svg"
 ALT="\begin{Eqts}
\varphi(2x) = \frac{1}{2} ( \varphi(x) + \psi(x) ) \\\\
\varphi(2x-1) = \frac{1}{2} ( \varphi(x) - \psi(x) )
\end{Eqts}">|; 

$key = q/{Eqts}varphi(x)=indicatrice_{[0,1[}(x)psi(x)=indicatrice_{[0,1slash2[}(x)-indicatrice_{[1slash2,1[}(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img2.svg"
 ALT="\begin{Eqts}
\varphi(x) = \indicatrice_{[0,1[}(x) \\\\
\psi(x) = \indicatrice_{[0,1/2[}(x) - \indicatrice_{[1/2,1[}(x)
\end{Eqts}">|; 

$key = q/{Eqts}varphi(x)=sum_ka_k^0varphi(mx-k)psi^i(x)=sum_ka_k^ivarphi(mx-k){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.75ex; vertical-align: -5.32ex; " SRC="|."$dir".q|img32.svg"
 ALT="\begin{Eqts}
\varphi(x) = \sum_k a_k^0 \varphi(m x - k) \\\\
\psi^i(x) = \sum_k a_k^i \varphi(m x - k)
\end{Eqts}">|; 

$key = q/{Eqts}varphi(x)=varphi(2x)+varphi(2x-1)=a_0^0varphi(2x)+a_1^0varphi(2x-1)psi(x)=varphi(2x)-varphi(2x-1)=a_0^1varphi(2x)+a_1^1varphi(2x-1){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img5.svg"
 ALT="\begin{Eqts}
\varphi(x) = \varphi(2x) + \varphi(2x-1) = a_0^0 \varphi(2x) + a_1^...
...\varphi(2x) - \varphi(2x-1) = a_0^1 \varphi(2x) + a_1^1 \varphi(2x-1)
\end{Eqts}">|; 

$key = q/{Eqts}varphi_{jk}(x)=m^{jslash2}varphi(m^jx-k)psi^i_{jk}(x)=m^{jslash2}psi^i(m^jx-k){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.90ex; vertical-align: -2.88ex; " SRC="|."$dir".q|img53.svg"
 ALT="\begin{Eqts}
\varphi_{jk}(x) = m^{j/2} \varphi(m^j x - k) \\\\
\psi^i_{jk}(x) = m^{j/2} \psi^i(m^j x - k)
\end{Eqts}">|; 

1;

