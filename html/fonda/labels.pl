# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate labels original text with physical files.


$key = q/chap:fonda/;
$external_labels{$key} = "$URL/" . q|fonda.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:derivee_integrale/;
$external_labels{$key} = "$URL/" . q|fonda.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:integrale_derivee/;
$external_labels{$key} = "$URL/" . q|fonda.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2023 (Released January 1, 2023)
# labels from external_latex_labels array.


$key = q/chap:fonda/;
$external_latex_labels{$key} = q|1 Théorème fondamental|; 
$noresave{$key} = "$nosave";

$key = q/sec:derivee_integrale/;
$external_latex_labels{$key} = q|1.1 Dérivée de l'intégrale|; 
$noresave{$key} = "$nosave";

$key = q/sec:integrale_derivee/;
$external_latex_labels{$key} = q|1.2 Intégrale de la dérivée|; 
$noresave{$key} = "$nosave";

1;

