# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate internals original text with physical files.


$key = q/chap:fonda/;
$ref_files{$key} = "$dir".q|fonda.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:derivee_integrale/;
$ref_files{$key} = "$dir".q|fonda.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:integrale_derivee/;
$ref_files{$key} = "$dir".q|fonda.html|; 
$noresave{$key} = "$nosave";

1;

