# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img63.svg"
 ALT="\begin{eqnarray*}
F(x + h) - F(x) - 2 \epsilon \cdot h &amp;\le f(x) \cdot h - \epsi...
...t h + \epsilon \cdot h \le&amp; F(x + h) - F(x) + 2 \epsilon \cdot h
\end{eqnarray*}">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img145.svg"
 ALT="$0$">|; 

$key = q/F(b)-F(a)=F(b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$F(b) - F(a) = F(b)$">|; 

$key = q/F,G:[a,b]mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img110.svg"
 ALT="$F,G : [a,b] \mapsto \setR$">|; 

$key = q/F,Gincontinue^1([a,b],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img112.svg"
 ALT="$F,G \in \continue^1([a,b],\setR)$">|; 

$key = q/F:[a,b]mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img97.svg"
 ALT="$F : [a,b] \mapsto \setR$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$F$">|; 

$key = q/Fincontinue^1([a,b],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img100.svg"
 ALT="$F \in \continue^1([a,b],\setR)$">|; 

$key = q/Fincontinue^1([alpha,beta],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$F \in \continue^1([\alpha,\beta],\setR)$">|; 

$key = q/I:[alpha,beta]mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$I : [\alpha,\beta] \mapsto \setR$">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$I$">|; 

$key = q/[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$[\alpha,\beta]$">|; 

$key = q/[b,b+h];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$[b, b + h]$">|; 

$key = q/[s,t]=[varphi^{-1}(x),varphi^{-1}(y)];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img148.svg"
 ALT="$[s,t] = [\varphi^{-1}(x),\varphi^{-1}(y)]$">|; 

$key = q/[x_{i-1},x_i];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img149.svg"
 ALT="$[x_{i - 1},x_i]$">|; 

$key = q/a,b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img94.svg"
 ALT="$a,b$">|; 

$key = q/a,bin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$a,b \in [\alpha,\beta]$">|; 

$key = q/a=x_0lex_1le...lex_n=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img70.svg"
 ALT="$a = x_0 \le x_1 \le ... \le x_n = b$">|; 

$key = q/abs{h}lemin{vartheta,varpi};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\abs{h} \le \min \{ \vartheta , \varpi \}$">|; 

$key = q/abs{h}levarpi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\abs{h} \le \varpi$">|; 

$key = q/abs{h}levartheta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\abs{h} \le \vartheta$">|; 

$key = q/abs{s}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\abs{s} \le \delta$">|; 

$key = q/ain[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$a \in [\alpha,\beta]$">|; 

$key = q/aleb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img45.svg"
 ALT="$a \le b$">|; 

$key = q/alpha,betainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\alpha,\beta \in \setR$">|; 

$key = q/alphalebeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\alpha \le \beta$">|; 

$key = q/astrictinferieurb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img95.svg"
 ALT="$a \strictinferieur b$">|; 

$key = q/b+hin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$b + h \in [\alpha,\beta]$">|; 

$key = q/bin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$b \in [\alpha,\beta]$">|; 

$key = q/cinintervalleouvert{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$c \in \intervalleouvert{a}{b}$">|; 

$key = q/delta>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\delta &gt; 0$">|; 

$key = q/deltato0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\delta \to 0$">|; 

$key = q/differencex_ito0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.13ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\difference x_i \to 0$">|; 

$key = q/displaystyle(f(b)-epsilon)cdothleint_b^{b+h}le(f(b)+epsilon)cdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle (f(b) - \epsilon) \cdot h \le \int_b^{b + h} \le (f(b) + \epsilon) \cdot h$">|; 

$key = q/displaystyle(f(x)-epsilon)cdothleint_x^{x+h}le(f(x)+epsilon)cdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle (f(x) - \epsilon) \cdot h \le \int_x^{x + h} \le (f(x) + \epsilon) \cdot h$">|; 

$key = q/displaystyleF(a)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle F(a) = 0$">|; 

$key = q/displaystyleF(a)=int_a^af(t)dt=int_{{a}}f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.87ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle F(a) = \int_a^a f(t)  dt = \int_{ \{ a \} } f(t)  dt$">|; 

$key = q/displaystyleF(b)-F(a)-2epsiloncdot(b-a)leint_a^bleF(b)-F(a)+2epsiloncdot(b-a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle F(b) - F(a) - 2 \epsilon \cdot (b - a) \le \int_a^b \le F(b) - F(a) + 2 \epsilon \cdot (b - a)$">|; 

$key = q/displaystyleF(b)-F(a)-2epsiloncdothcdotnleint_a^bleF(b)-F(a)+2epsiloncdothcdotn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle F(b) - F(a) - 2 \epsilon \cdot h \cdot n \le \int_a^b \le F(b) - F(a) + 2 \epsilon \cdot h \cdot n$">|; 

$key = q/displaystyleF(b)=int_a^bf(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle F(b) = \int_a^b f(t)  dt$">|; 

$key = q/displaystyleF(s)=(fcircvarphi)(s)cdotOD{varphi}{s}(s);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\displaystyle F(s) = (f \circ \varphi)(s) \cdot \OD{\varphi}{s}(s)$">|; 

$key = q/displaystyleF(x)=int_a^xf(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle F(x) = \int_a^x f(t)  dt$">|; 

$key = q/displaystyleF(x+h)-F(x)-2epsiloncdothleint_x^{x+h}leF(x+h)-F(x)+2epsiloncdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle F(x + h) - F(x) - 2 \epsilon \cdot h \le \int_x^{x + h} \le F(x + h) - F(x) + 2 \epsilon \cdot h$">|; 

$key = q/displaystyleF(x+h)-F(x)-epsiloncdothlef(x)cdothleF(x+h)-F(x)+epsiloncdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle F(x + h) - F(x) - \epsilon \cdot h \le f(x) \cdot h \le F(x + h) - F(x) + \epsilon \cdot h$">|; 

$key = q/displaystyleF(x_i)-F(x_{i-1})-2epsiloncdothleint_{x_{i-1}}^{x_i}leF(x_i)-F(x_{i-1})+2epsiloncdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.88ex; vertical-align: -2.59ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle F(x_i) - F(x_{i - 1}) - 2 \epsilon \cdot h \le \int_{ x_{i - 1} }^{x_i} \le F(x_i) - F(x_{i - 1}) + 2 \epsilon \cdot h$">|; 

$key = q/displaystyleI(a)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle I(a) = 0$">|; 

$key = q/displaystyleI(a)=int_a^af(s)ds=int_{{a}}f(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.87ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle I(a) = \int_a^a f(s)  ds = \int_{ \{a\} } f(s)  ds$">|; 

$key = q/displaystyleI(b+h)=I(b)+int_b^{b+h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle I(b + h) = I(b) + \int_b^{b + h}$">|; 

$key = q/displaystyleI(x)=int_a^xf(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle I(x) = \int_a^x f(s)  ds$">|; 

$key = q/displaystyleOD{F}{t}=f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle \OD{F}{t} = f$">|; 

$key = q/displaystyleOD{I}{x}(b)=lim_{hto0}frac{I(b+h)-I(b)}{h}=f(b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \OD{I}{x}(b) = \lim_{h \to 0} \frac{I(b + h) - I(b)}{h} = f(b)$">|; 

$key = q/displaystyleOD{}{t}big(t^nbig)=ncdott^{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle \OD{}{t}\big(t^n\big) = n \cdot t^{n - 1}$">|; 

$key = q/displaystyleOD{}{t}left(frac{t^n}{n}right)=t^{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle \OD{}{t}\left( \frac{t^n}{n} \right) = t^{n - 1}$">|; 

$key = q/displaystyleOD{}{t}left(frac{t^{m+1}}{m+1}right)=t^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle \OD{}{t}\left( \frac{t^{m + 1}}{m + 1} \right) = t^m$">|; 

$key = q/displaystyleOD{}{x}int_a^xf(s)ds=f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \OD{}{x} \int_a^x f(s)  ds = f(x)$">|; 

$key = q/displaystyleabs{F(x+h)-F(x)-f(x)cdoth}leepsiloncdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle \abs{F(x + h) - F(x) - f(x) \cdot h} \le \epsilon \cdot h$">|; 

$key = q/displaystyleabs{f(b+s)-f(b)}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \abs{f(b + s) - f(b)} \le \epsilon$">|; 

$key = q/displaystyleabs{f(x+h)-f(x)}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \abs{f(x + h) - f(x)} \le \epsilon$">|; 

$key = q/displaystyleabs{frac{b-a}{n}}lemin{vartheta,varpi};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \abs{ \frac{b - a}{n} } \le \min \{ \vartheta , \varpi \}$">|; 

$key = q/displaystyledifferenceF=int_a^bOD{F}{x}dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \difference F = \int_a^b \OD{F}{x}  dx$">|; 

$key = q/displaystyledifferenceF=intdF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.34ex; vertical-align: -2.10ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle \difference F = \int dF$">|; 

$key = q/displaystyledifferenceF=sum_idifferenceF_i=sum_ifrac{differenceF_i}{differencex_i}cdotdifferencex_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.26ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \difference F = \sum_i \difference F_i = \sum_i \frac{ \difference F_i }{ \difference x_i } \cdot \difference x_i$">|; 

$key = q/displaystylef(b)-epsilonlef(b+s)lef(b)+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle f(b) - \epsilon \le f(b + s) \le f(b) + \epsilon$">|; 

$key = q/displaystylef(b)-epsilonlelim_{hto0}unsur{h}int_b^{b+h}lef(b)+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle f(b) - \epsilon \le \lim_{h \to 0} \unsur{h} \int_b^{b + h} \le f(b) + \epsilon$">|; 

$key = q/displaystylef(b)-epsilonleunsur{h}int_b^{b+h}lef(b)+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle f(b) - \epsilon \le \unsur{h} \int_b^{b + h} \le f(b) + \epsilon$">|; 

$key = q/displaystylef(c)=unsur{b-a}int_a^bf(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle f(c) = \unsur{b - a} \int_a^b f(t)  dt$">|; 

$key = q/displaystylef(c)int_a^bg(t)dt=g(c)int_a^bf(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\displaystyle f(c)  \int_a^b g(t)  dt = g(c)  \int_a^b f(t)  dt$">|; 

$key = q/displaystylef=partialF=OD{F}{s};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle f = \partial F = \OD{F}{s}$">|; 

$key = q/displaystylefrac{f(c)}{g(c)}=frac{int_a^bf(t)dt}{int_a^bg(t)dt};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.10ex; vertical-align: -2.98ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle \frac{f(c)}{g(c)} = \frac{ \int_a^b f(t)  dt }{ \int_a^b g(t)  dt }$">|; 

$key = q/displaystyleint_0^xt^2dt=frac{x^3}{3};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.68ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle \int_0^x t^2  dt = \frac{ x^3 }{3}$">|; 

$key = q/displaystyleint_0^xt^mdt=frac{x^{m+1}}{m+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.68ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \int_0^x t^m  dt = \frac{ x^{m + 1} }{m + 1}$">|; 

$key = q/displaystyleint_0^xtdt=frac{x^2}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.68ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle \int_0^x t  dt = \frac{ x^2 }{2}$">|; 

$key = q/displaystyleint_a^b=F(b)-F(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle \int_a^b = F(b) - F(a)$">|; 

$key = q/displaystyleint_a^bf(s)ds=int_a^bOD{F}{s}(s)ds=F(b)-F(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle \int_a^b f(s)  ds = \int_a^b \OD{F}{s}(s)  ds = F(b) - F(a)$">|; 

$key = q/displaystyleint_a^bf(s)ds=int_a^bOD{F}{x}(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \int_a^b f(s)  ds = \int_a^b \OD{F}{x}(s)  ds$">|; 

$key = q/displaystyleint_a^bf(x)cdotdg(x)=difference(fcdotg)-int_a^bg(x)cdotdf(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\displaystyle \int_a^b f(x) \cdot dg(x) = \difference (f \cdot g) - \int_a^b g(x) \cdot df(x)$">|; 

$key = q/displaystyleint_a^bf(x)cdotpartialg(x)dx=(fcdotg)(b)-(fcdotg)(a)-int_a^bpartialf(x)cdotg(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\displaystyle \int_a^b f(x) \cdot \partial g(x)  dx = (f \cdot g)(b) - (f \cdot g)(a) - \int_a^b \partial f(x) \cdot g(x)  dx$">|; 

$key = q/displaystyleint_a^bf(x)dx=f(b)cdotb-f(a)cdota-int_a^bpartialf(x)cdotxdx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\displaystyle \int_a^b f(x)  dx = f(b) \cdot b - f(a) \cdot a - \int_a^b \partial f(x) \cdot x  dx$">|; 

$key = q/displaystyleint_a^bf(x)dx=int_a^bf(x)cdot1dx=int_a^bf(x)cdotpartialg(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\displaystyle \int_a^b f(x)  dx = \int_a^b f(x) \cdot 1  dx = \int_a^b f(x) \cdot \partial g(x)  dx$">|; 

$key = q/displaystyleint_a^bf(x)dx=int_{varphi^{-1}(a)}^{varphi^{-1}(b)}(fcircvarphi)(s)cdotOD{varphi}{s}(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.50ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle \int_a^b f(x)  dx = \int_{\varphi^{-1}(a)}^{\varphi^{-1}(b)} (f \circ \varphi)(s) \cdot \OD{\varphi}{s}(s)  ds$">|; 

$key = q/displaystyleint_a^bpartial(fcdotg)dx=f(b)cdotg(b)-f(a)cdotg(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle \int_a^b \partial (f \cdot g)  dx = f(b) \cdot g(b) - f(a) \cdot g(a)$">|; 

$key = q/displaystyleint_a^bt^mdt=frac{b^{m+1}-a^{m+1}}{m+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle \int_a^b t^m  dt = \frac{ b^{m + 1} - a^{m + 1} }{m + 1}$">|; 

$key = q/displaystyleint_a^{b+h}=int_a^b+int_b^{b+h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \int_a^{b + h} = \int_a^b + \int_b^{b + h}$">|; 

$key = q/displaystyleint_b^{b+h}ge(f(b)-epsilon)cdotmu_L([a,a+h])=(f(b)-epsilon)cdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \int_b^{b + h} \ge (f(b) - \epsilon) \cdot \mu_L([a, a + h]) = (f(b) - \epsilon) \cdot h$">|; 

$key = q/displaystyleint_b^{b+h}le(f(b)+epsilon)cdotmu_L([a,a+h])=(f(b)+epsilon)cdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \int_b^{b + h} \le (f(b) + \epsilon) \cdot \mu_L([a, a + h]) = (f(b) + \epsilon) \cdot h$">|; 

$key = q/displaystyleint_x^y=int_x^yf(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \int_x^y = \int_x^y f(s)  ds$">|; 

$key = q/displaystyleint_x^yapprox(fcircvarphi)(s)cdotOD{varphi}{s}(s)cdot(t-s);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\displaystyle \int_x^y \approx (f \circ \varphi)(s) \cdot \OD{\varphi}{s}(s) \cdot (t - s)$">|; 

$key = q/displaystyleint_x^yapproxf(x)cdot(y-x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle \int_x^y \approx f(x) \cdot (y - x)$">|; 

$key = q/displaystyleintfdx=intfOD{x}{s}ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.36ex; vertical-align: -2.10ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle \int f  dx = \int f  \OD{x}{s}  ds$">|; 

$key = q/displaystylelim_{hto0}frac{I(b+h)-I(b)}{h}=f(b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \lim_{h \to 0} \frac{I(b + h) - I(b)}{h} = f(b)$">|; 

$key = q/displaystylepartial(fcdotg)=partialfcdotg+fcdotpartialg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\displaystyle \partial (f \cdot g) = \partial f \cdot g + f \cdot \partial g$">|; 

$key = q/displaystylepartialF(c)=f(c);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle \partial F(c) = f(c)$">|; 

$key = q/displaystylepartialF(c)=frac{F(b)-F(a)}{b-a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.28ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle \partial F(c) = \frac{F(b) - F(a)}{b - a}$">|; 

$key = q/displaystylepartialF(c)cdotbig[G(b)-G(a)big]=big[F(b)-F(a)big]cdotpartialG(c);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle \partial F(c) \cdot \big[G(b) - G(a)\big] = \big[F(b) - F(a)\big] \cdot \partial G(c)$">|; 

$key = q/displaystylesum_if_icdotdifferencex_i=sum_if_icdotfrac{differencex_i}{differences_i}cdotdifferences_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.26ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\displaystyle \sum_i f_i \cdot \difference x_i = \sum_i f_i \cdot \frac{\difference x_i}{\difference s_i} \cdot \difference s_i$">|; 

$key = q/displaystylesum_{i=1}^n(F(x_i)-F(x_{i-1}))=F(x_n)-F(x_0)=F(b)-F(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \sum_{i = 1}^n (F(x_i) - F(x_{i - 1})) = F(x_n) - F(x_0) = F(b) - F(a)$">|; 

$key = q/displaystylesum_{i=1}^nint_{x_{i-1}}^{x_i}=int_a^b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \sum_{i = 1}^n \int_{ x_{i - 1} }^{x_i} = \int_a^b$">|; 

$key = q/displaystyleunsur{h}int_b^{b+h}f(s)ds=frac{I(b+h)-I(b)}{h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \unsur{h} \int_b^{b + h} f(s)  ds = \frac{I(b + h) - I(b)}{h}$">|; 

$key = q/displaystylex_i=a+icdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle x_i = a + i \cdot h$">|; 

$key = q/displaystyley-x=varphi(t)-varphi(s)=OD{varphi}{s}(s)cdot(t-s)+e(abs{s-t});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle y - x = \varphi(t) - \varphi(s) = \OD{\varphi}{s}(s) \cdot (t - s) + e(\abs{s - t})$">|; 

$key = q/e;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img143.svg"
 ALT="$e$">|; 

$key = q/epsiloncdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\epsilon \cdot h$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/f(x)cdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$f(x) \cdot h$">|; 

$key = q/f,gincontinue([a,b],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$f,g \in \continue([a,b],\setR)$">|; 

$key = q/f,gincontinue^1(setR,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img121.svg"
 ALT="$f,g \in \continue^1(\setR,\setR)$">|; 

$key = q/f:tmapstof(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$f : t \mapsto f(t)$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img18.svg"
 ALT="$f$">|; 

$key = q/fincontinue([a,b],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img96.svg"
 ALT="$f \in \continue([a,b],\setR)$">|; 

$key = q/fincontinue([alpha,beta],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f \in \continue([\alpha,\beta],\setR)$">|; 

$key = q/fincontinue(setR,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$f \in \continue(\setR,\setR)$">|; 

$key = q/g(c);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img119.svg"
 ALT="$g(c)$">|; 

$key = q/g(x)=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$g(x) = x$">|; 

$key = q/g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img118.svg"
 ALT="$g$">|; 

$key = q/g=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img127.svg"
 ALT="$g = \identite$">|; 

$key = q/h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img14.svg"
 ALT="$h$">|; 

$key = q/h=(b-a)slashn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$h = (b - a)/n$">|; 

$key = q/h=x_i-x_{i-1}to0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img150.svg"
 ALT="$h = x_i - x_{i - 1} \to 0$">|; 

$key = q/hcdotn=b-a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img78.svg"
 ALT="$h \cdot n = b - a$">|; 

$key = q/hin(0,delta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$h \in (0,\delta)$">|; 

$key = q/hto0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$h \to 0$">|; 

$key = q/i=1,2,...,n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img76.svg"
 ALT="$i = 1,2,...,n$">|; 

$key = q/m=n-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img88.svg"
 ALT="$m = n - 1$">|; 

$key = q/mu([alpha,beta])=beta-alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\mu([\alpha,\beta]) = \beta - \alpha$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img86.svg"
 ALT="$n$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img66.svg"
 ALT="$n \in \setN$">|; 

$key = q/partialg=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\partial g = 1$">|; 

$key = q/s-t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.81ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img144.svg"
 ALT="$s - t$">|; 

$key = q/s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img21.svg"
 ALT="$s$">|; 

$key = q/s=varphi^{-1}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img140.svg"
 ALT="$s = \varphi^{-1}(x)$">|; 

$key = q/t=varphi^{-1}(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img141.svg"
 ALT="$t = \varphi^{-1}(y)$">|; 

$key = q/varphiinhomeomorphisme^1(setR,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\varphi \in \homeomorphisme^1(\setR,\setR)$">|; 

$key = q/varpistrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\varpi \strictsuperieur 0$">|; 

$key = q/varthetastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\vartheta \strictsuperieur 0$">|; 

$key = q/x+h=x_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img74.svg"
 ALT="$x + h = x_i$">|; 

$key = q/x,h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$x,h$">|; 

$key = q/x,x+hin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$x,x + h \in [\alpha,\beta]$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img137.svg"
 ALT="$x$">|; 

$key = q/x=varphi(s);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img132.svg"
 ALT="$x = \varphi(s)$">|; 

$key = q/x=x_{i-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img73.svg"
 ALT="$x = x_{i - 1}$">|; 

$key = q/xin[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img99.svg"
 ALT="$x \in [a,b]$">|; 

$key = q/xin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$x \in [\alpha,\beta]$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img138.svg"
 ALT="$y$">|; 

$key = q/{Eqts}F(x+h)-F(x)-f(x)cdothleepsiloncdothf(x)cdoth-(F(x+h)-F(x))leepsiloncdoth{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img59.svg"
 ALT="\begin{Eqts}
F(x + h) - F(x) - f(x) \cdot h \le \epsilon \cdot h \\\\
f(x) \cdot h - (F(x + h) - F(x)) \le \epsilon \cdot h
\end{Eqts}">|; 

$key = q/{Eqts}partialF(c)=f(c)partialG(c)=g(c){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img114.svg"
 ALT="\begin{Eqts}
\partial F(c) = f(c) \\\\
\partial G(c) = g(c)
\end{Eqts}">|; 

$key = q/{Eqts}supBig{f(b+s):abs{s}ledeltaBig}lef(b)+epsiloninfBig{f(b+s):abs{s}ledeltaBig}gef(b)-epsilon{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.30ex; vertical-align: -4.08ex; " SRC="|."$dir".q|img24.svg"
 ALT="\begin{Eqts}
\sup \Big\{ f(b + s) : \abs{s} \le \delta \Big\} \le f(b) + \epsilo...
...
\inf \Big\{ f(b + s) : \abs{s} \le \delta \Big\} \ge f(b) - \epsilon
\end{Eqts}">|; 

$key = q/{Eqts}sup_{xiin[x-vartheta,x+vartheta]}f(xi)lef(x)+epsiloninf_{xiin[x-vartheta,x+vartheta]}f(xi)gef(x)-epsilon{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.65ex; vertical-align: -4.26ex; " SRC="|."$dir".q|img54.svg"
 ALT="\begin{Eqts}
\sup_{\xi \in [x - \vartheta , x + \vartheta]} f(\xi) \le f(x) + \e...
..._{\xi \in [x - \vartheta , x + \vartheta]} f(\xi) \ge f(x) - \epsilon
\end{Eqts}">|; 

$key = q/{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\{a\}$">|; 

$key = q/{eqnarraystar}F(a)&=&int_a^af(t)dt=0G(a)&=&int_a^ag(t)dt=0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.37ex; " SRC="|."$dir".q|img116.svg"
 ALT="\begin{eqnarray*}
F(a) &amp;=&amp; \int_a^a f(t)  dt = 0 \\\\
G(a) &amp;=&amp; \int_a^a g(t)  dt = 0
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}F(b)&=&int_a^bf(t)dtG(b)&=&int_a^bg(t)dt{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 17.19ex; " SRC="|."$dir".q|img115.svg"
 ALT="\begin{eqnarray*}
F(b) &amp;=&amp; \int_a^b f(t)  dt \\\\
G(b) &amp;=&amp; \int_a^b g(t)  dt
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}F(x)&=&int_a^xf(t)dtG(x)&=&int_a^xg(t)dt{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.37ex; " SRC="|."$dir".q|img111.svg"
 ALT="\begin{eqnarray*}
F(x) &amp;=&amp; \int_a^x f(t)  dt \\\\
G(x) &amp;=&amp; \int_a^x g(t)  dt
\end{eqnarray*}">|; 

1;

