# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 32.67ex; " SRC="|."$dir".q|img9.svg"
 ALT="\begin{eqnarray*}
\int_A w(x)  d\mu(x) &amp;=&amp; \sum_{i = 1}^n w_i \cdot \mu(A_i) \\\\...
..._i) \\\\
&amp;=&amp; \int_A w(x)  d\gamma(x) + \int_A w(x)  d\lambda(x)
\end{eqnarray*}">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$A$">|; 

$key = q/A_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img37.svg"
 ALT="$A_i$">|; 

$key = q/A_iinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img7.svg"
 ALT="$A_i \in \mathcal{T}$">|; 

$key = q/Asetminus{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$A \setminus \{a\}$">|; 

$key = q/AsubseteqsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img13.svg"
 ALT="$A \subseteq \setR^n$">|; 

$key = q/S[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$S[a,b]$">|; 

$key = q/[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$[a,b]$">|; 

$key = q/ainA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img51.svg"
 ALT="$a \in A$">|; 

$key = q/alpha_iinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\alpha_i \in \setR$">|; 

$key = q/anotinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img49.svg"
 ALT="$a \notin A$">|; 

$key = q/displaystyleS[a,b]=Big{{x_0,x_1,x_2,...,x_n}:alex_0lex_1lex_2le...leb,ninsetNBig};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle S[a,b] = \Big\{ \{x_0, x_1, x_2, ..., x_n \} : a \le x_0 \le x_1 \le x_2 \le ... \le b,  n \in \setN \Big\}$">|; 

$key = q/displaystyledmu(x)=left[varphi(x)+sum_ialpha_icdotindicatrice(x-x_i)right]dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle d\mu(x) = \left[ \varphi(x) + \sum_i \alpha_i \cdot \indicatrice(x - x_i) \right]  dx$">|; 

$key = q/displaystyledmu(x)=varphi(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle d\mu(x) = \varphi(x)  dx$">|; 

$key = q/displaystyledmu_L(x)=dx=dx_1...dx_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle d\mu_L(x) = dx = dx_1  ...  dx_n$">|; 

$key = q/displaystyleint_Admu_D^a(x)=mu_D^a(A)=indicatrice_A(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \int_A d\mu_D^a(x) = \mu_D^a(A) = \indicatrice_A(a)$">|; 

$key = q/displaystyleint_Af(x)d(gamma+lambda)(x)=int_Af(x)dgamma(x)+int_Af(x)dlambda(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \int_A f(x)  d(\gamma + \lambda)(x) = \int_A f(x)  d\gamma(x) + \int_A f(x)  d\lambda(x)$">|; 

$key = q/displaystyleint_Af(x)dg(x)=int_Af(x)dsigma(x)-int_Af(x)dlambda(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \int_A f(x)  dg(x) = \int_A f(x)  d\sigma(x) - \int_A f(x)  d\lambda(x)$">|; 

$key = q/displaystyleint_Af(x)dgamma(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \int_A f(x)  d\gamma(x)$">|; 

$key = q/displaystyleint_Af(x)dgamma(x)=int_Af(x)dmu_gamma(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \int_A f(x)  d\gamma(x) = \int_A f(x)  d\mu_\gamma(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=int_Af(x)cdotvarphi(x)dx+sum_ialpha_icdotf(x_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.25ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) = \int_A f(x) \cdot \varphi(x)  dx + \sum_i \alpha_i \cdot f(x_i)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=int_Af(x)cdotvarphi(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) = \int_A f(x) \cdot \varphi(x)  dx$">|; 

$key = q/displaystyleint_Af(x)dmu_D^a(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu_D^a(x) = 0$">|; 

$key = q/displaystyleint_Af(x)dmu_D^a(x)=f(a)cdotindicatrice_A(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu_D^a(x) = f(a) \cdot \indicatrice_A(a)$">|; 

$key = q/displaystyleint_Af(x)dmu_D^a(x)=f(a)cdotindicatrice_{{a}}(a)=f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu_D^a(x) = f(a) \cdot \indicatrice_{\{a\}}(a) = f(a)$">|; 

$key = q/displaystyleint_Af(x)dmu_D^a(x)=int_{{a}}f(x)dmu_D^a(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu_D^a(x) = \int_{\{a\}} f(x)  d\mu_D^a(x)$">|; 

$key = q/displaystyleint_Af(x)dx=int_Af(x)dmu_L(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \int_A f(x)  dx = \int_A f(x)  d\mu_L(x)$">|; 

$key = q/displaystyleint_Af(x)indicatrice(x-a)dx=f(a)cdotindicatrice_A(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \int_A f(x)  \indicatrice(x - a)  dx = f(a) \cdot \indicatrice_A(a)$">|; 

$key = q/displaystyleint_Aw(x)dmu(x)=int_Aw(x)cdotvarphi(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \int_A w(x)  d\mu(x) = \int_A w(x) \cdot \varphi(x)  dx$">|; 

$key = q/displaystyleint_Aw(x)dmu(x)=sum_iw_icdotint_{A_i}varphi(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.25ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \int_A w(x)  d\mu(x) = \sum_i w_i \cdot \int_{A_i} \varphi(x)  dx$">|; 

$key = q/displaystyleint_{Asetminus{a}}dmu_D^a(x)=indicatrice_{Asetminus{a}}(a)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \int_{A \setminus \{a\}} d\mu_D^a(x) = \indicatrice_{A \setminus \{a\}}(a) = 0$">|; 

$key = q/displaystylelambda(x)=-infaccolades{sum_{i=0}^nmin{g(x_{i+1})-g(x_i),0}:{x_0,x_1,...,x_n}inS[alpha,x]};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \lambda(x) = - \inf \accolades{ \sum_{i = 0}^n \min\{g(x_{i + 1}) - g(x_i), 0\} : \{x_0, x_1, ..., x_n \} \in S[\alpha,x] }$">|; 

$key = q/displaystylemu(A)=int_Avarphi(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \mu(A) = \int_A \varphi(x)  dx$">|; 

$key = q/displaystylemu=gamma+lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \mu = \gamma + \lambda$">|; 

$key = q/displaystylesigma(x)=supaccolades{sum_{i=0}^nmax{g(x_{i+1})-g(x_i),0}:{x_0,x_1,...,x_n}inS[alpha,x]};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \sigma(x) = \sup \accolades{ \sum_{i = 0}^n \max\{g(x_{i + 1}) - g(x_i), 0\} : \{x_0, x_1, ..., x_n \} \in S[\alpha,x] }$">|; 

$key = q/displaystylew(x)=sum_{i=1}^nw_icdotindicatrice_{A_i}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle w(x) = \sum_{i = 1}^n w_i \cdot \indicatrice_{A_i}(x)$">|; 

$key = q/displaystylew=sum_iw_icdotindicatrice_{A_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle w = \sum_i w_i \cdot \indicatrice_{A_i}$">|; 

$key = q/dmu_L(x)=dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$d\mu_L(x) = dx$">|; 

$key = q/f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$f(a)$">|; 

$key = q/f:AtosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$f : A \to \setR$">|; 

$key = q/f:setRtosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$f : \setR \to \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$f$">|; 

$key = q/g:[alpha,beta]mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$g : [\alpha,\beta] \mapsto \setR$">|; 

$key = q/g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img27.svg"
 ALT="$g$">|; 

$key = q/gamma,lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\gamma, \lambda$">|; 

$key = q/gamma:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\gamma : A \mapsto \setR$">|; 

$key = q/gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\gamma$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\mu$">|; 

$key = q/mu_D^a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\mu_D^a$">|; 

$key = q/mu_L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\mu_L$">|; 

$key = q/mu_gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\mu_\gamma$">|; 

$key = q/sigma,lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\sigma,\lambda$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\varphi$">|; 

$key = q/w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$w$">|; 

$key = q/w_iinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img6.svg"
 ALT="$w_i \in \setR$">|; 

$key = q/wcdotvarphiessinferieurfcdotvarphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img42.svg"
 ALT="$w \cdot \varphi \essinferieur f \cdot \varphi$">|; 

$key = q/wessinferieurf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img41.svg"
 ALT="$w \essinferieur f$">|; 

$key = q/winetagee(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$w \in \etagee(A)$">|; 

$key = q/winmathcal{E}_A(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$w \in \mathcal{E}_A(f)$">|; 

$key = q/x_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img62.svg"
 ALT="$x_i$">|; 

$key = q/xin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$x \in [\alpha,\beta]$">|; 

$key = q/{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\{a\}$">|; 

1;

