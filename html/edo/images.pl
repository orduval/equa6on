# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle \deriveepartielle{f}{u} - \OD{}{t} \deriveepartielle{f}{v} +
\sum...
...m_{i=1}^m \OD{}{t} \left[ \lambda_i \cdot \deriveepartielle{g_i}{v} \right] = 0$">|; 

$key = q/(t,u(t),u'(t));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$(t,u(t),u'(t))$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$0$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$A$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img111.svg"
 ALT="$F$">|; 

$key = q/I:continue^2([a,b],setR)mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$I : \continue^2([a,b],\setR) \mapsto \setR$">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$I$">|; 

$key = q/J_w(epsilon)geJ_w(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$J_w(\epsilon) \ge J_w(0)$">|; 

$key = q/J_w:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img50.svg"
 ALT="$J_w : \setR \mapsto \setR$">|; 

$key = q/J_w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img65.svg"
 ALT="$J_w$">|; 

$key = q/[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img102.svg"
 ALT="$[a,b]$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img79.svg"
 ALT="$a$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img80.svg"
 ALT="$b$">|; 

$key = q/displaystyle-int_a^bOD{}{t}Big[p(t)OD{u}{t}(t)Big]v(t)dt+int_a^bq(t)u(t)v(t)dt=int_a^bf(t)v(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle - \int_a^b \OD{}{t} \Big[ p(t)  \OD{u}{t}(t) \Big]  v(t)  dt + \int_a^b q(t)  u(t)  v(t)  dt = \int_a^b f(t)  v(t)  dt$">|; 

$key = q/displaystyle0=int_a^bOD{}{t}left[deriveepartielle{f}{v}right]wdt+int_a^bderiveepartielle{f}{v}w'dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.00ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle 0 = \int_a^b \OD{}{t} \left[ \deriveepartielle{f}{v} \right]  w  dt +
\int_a^b \deriveepartielle{f}{v}  w'  dt$">|; 

$key = q/displaystyleA(u)(t)=u_0+int_0^tf(s,u(s))ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle A(u)(t) = u_0 + \int_0^t f(s, u(s))  ds$">|; 

$key = q/displaystyleF={uincontinue^2([a,b],setR):u(a)=u(b)=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle F = \{u \in \continue^2([a,b],\setR) : u(a) = u(b) = 0\}$">|; 

$key = q/displaystyleH(u)=I(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle H(u) = I(u)$">|; 

$key = q/displaystyleH(u)=int_a^bleft(f(t,u(t),u'(t))+sum_{i=1}^mlambda_i(t)cdotg_i(t,u(t),u'(t))right)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle H(u) = \int_a^b \left( f(t, u(t), u'(t)) +
\sum_{i=1}^m \lambda_i(t) \cdot g_i(t,u(t),u'(t)) \right)  dt$">|; 

$key = q/displaystyleI(u)=int_a^bf(t,u(t),u'(t))dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle I(u) = \int_a^b f(t, u(t), u'(t))  dt$">|; 

$key = q/displaystyleI(v)=int_a^bBig[p(x)left(OD{v}{x}(x)right)^2+q(x)v(x)^2Big]dx-int_a^bf(x)v(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.13ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle I(v) = \int_a^b \Big[ p(x)  \left(\OD{v}{x}(x)\right)^2 + q(x)  v(x)^2 \Big]  dx - \int_a^b f(x)  v(x)  dx$">|; 

$key = q/displaystyleJ_w(epsilon)=I(u+epsiloncdotw);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle J_w(\epsilon) = I(u + \epsilon \cdot w)$">|; 

$key = q/displaystyleJ_w(epsilon)=I(u+epsiloncdotw)=H(u+epsiloncdotw);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle J_w(\epsilon) = I(u + \epsilon \cdot w) = H(u + \epsilon \cdot w)$">|; 

$key = q/displaystyleJ_w(epsilon)=int_a^bf(t,u+epsiloncdotw,u'+epsiloncdotw')dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle J_w(\epsilon) = \int_a^b f(t, u + \epsilon \cdot w, u' + \epsilon \cdot w')  dt$">|; 

$key = q/displaystyleJ_w(epsilon)=int_a^bleft(f(t,u+epsiloncdotw,u'+epsiloncdotw')+sum_{i=1}^mlambda_i(t)cdotg_i(t,u+epsiloncdotw,u'+epsiloncdotw')right)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle J_w(\epsilon) = \int_a^b \left(
f(t, u + \epsilon \cdot w, u' + \...
...da_i(t) \cdot g_i(t, u + \epsilon \cdot w ,u' + \epsilon \cdot w')
\right)  dt$">|; 

$key = q/displaystyleOD{J_w}{epsilon}(0)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \OD{J_w}{\epsilon}(0) = 0$">|; 

$key = q/displaystyleOD{J_w}{epsilon}(0)=int_a^bleft(deriveepartielle{f}{u}(t,u,u')cdotw+deriveepartielle{f}{v}(t,u,u')cdotw'right)dt=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.00ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \OD{J_w}{\epsilon}(0) = \int_a^b \left(
\deriveepartielle{f}{u}(t,u,u') \cdot w +
\deriveepartielle{f}{v}(t,u,u') \cdot w' \right) dt = 0$">|; 

$key = q/displaystyleOD{u}{t}(t)=OD{}{t}A(u)(t)=OD{}{t}int_0^tf(s,u(s))ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \OD{u}{t}(t) = \OD{}{t} A(u)(t) = \OD{}{t} \int_0^t f(s, u(s))  ds$">|; 

$key = q/displaystyleOD{u}{t}(t)=f(t,u(t));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \OD{u}{t}(t) = f(t, u(t))$">|; 

$key = q/displaystyleOD{}{t}Big[p(t)OD{u}{t}(t)v(t)Big]=OD{}{t}Big[p(t)OD{u}{t}(t)Big]v(t)+p(t)OD{u}{t}(t)OD{v}{t}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle \OD{}{t} \Big[ p(t)  \OD{u}{t}(t)  v(t) \Big] = \OD{}{t} \Big[ p(t)  \OD{u}{t}(t) \Big]  v(t) + p(t)  \OD{u}{t}(t)  \OD{v}{t}(t)$">|; 

$key = q/displaystyleOD{}{t}left[deriveepartielle{f}{v}right]=dfdxdy{f}{t}{v}+dfdxdy{f}{u}{v}u'+dfdxdy{f}{v}{v}u'';MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \OD{}{t}\left[\deriveepartielle{f}{v} \right] = \dfdxdy{f}{t}{v} + \dfdxdy{f}{u}{v}  u' + \dfdxdy{f}{v}{v}  u''$">|; 

$key = q/displaystyleOD{}{t}left[deriveepartielle{f}{v}wright]=OD{}{t}left[deriveepartielle{f}{v}right]w+deriveepartielle{f}{v}w';MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle \OD{}{t}\left[\deriveepartielle{f}{v}  w \right] =
\OD{}{t}\left[ \deriveepartielle{f}{v} \right]  w + \deriveepartielle{f}{v}  w'$">|; 

$key = q/displaystylea(u,v)=b(v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle a(u,v) = b(v)$">|; 

$key = q/displaystylea,b:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle a, b : \setR \mapsto \setR$">|; 

$key = q/displaystylea_0,a_1,...,a_{n-1}:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.30ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle a_0, a_1, ..., a_{n - 1} : \setR \mapsto \setR$">|; 

$key = q/displaystylederiveepartielle{f}{u}-OD{}{t}deriveepartielle{f}{v}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \deriveepartielle{f}{u} - \OD{}{t} \deriveepartielle{f}{v} = 0$">|; 

$key = q/displaystylederiveepartielle{f}{u}-dfdxdy{f}{t}{v}-dfdxdy{f}{u}{v}u'-dfdxdy{f}{v}{v}cdotu''=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \deriveepartielle{f}{u} - \dfdxdy{f}{t}{v} - \dfdxdy{f}{u}{v}  u' - \dfdxdy{f}{v}{v} \cdot u'' = 0$">|; 

$key = q/displaystyledistance(u,v)=sup_{aletleb}norme{u(t)-v(t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.33ex; vertical-align: -2.50ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \distance(u,v) = \sup_{a \le t \le b} \norme{u(t) - v(t)}$">|; 

$key = q/displaystylef(t,u)=a(t)cdotb(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\displaystyle f(t, u) = a(t) \cdot b(u)$">|; 

$key = q/displaystylefincontinue^2([a,b]timessetR^2,setR),(t,u,v)mapstof(t,u,v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle f\in \continue^2([a,b]\times\setR^2,\setR),  (t,u,v) \mapsto f(t,u,v)$">|; 

$key = q/displaystylefrac{du}{a(u)}=b(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\displaystyle \frac{du}{a(u)} = b(t) dt$">|; 

$key = q/displaystyleg_i(t,u(t),u'(t))=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle g_i(t,u(t),u'(t)) = 0$">|; 

$key = q/displaystyleg_iincontinue^1([a,b]timessetR^2,setR),(t,u,v)mapstog(t,u,v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle g_i\in \continue^1([a,b]\times\setR^2,\setR),  (t,u,v) \mapsto g(t,u,v)$">|; 

$key = q/displaystyleint_a^bBig[p(t)OD{u}{t}(t)OD{v}{t}(t)+q(t)u(t)v(t)Big]dt=int_a^bf(t)v(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle \int_a^b \Big[ p(t)  \OD{u}{t}(t)  \OD{v}{t}(t) + q(t)  u(t)  v(t) \Big]  dt = \int_a^b f(t)  v(t)  dt$">|; 

$key = q/displaystyleint_a^bOD{}{t}Big[p(t)OD{u}{t}(t)Big]v(t)dt=-int_a^bp(t)OD{u}{t}(t)OD{v}{t}(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle \int_a^b \OD{}{t} \Big[ p(t)  \OD{u}{t}(t) \Big]  v(t)  dt = - \int_a^b p(t)  \OD{u}{t}(t)  \OD{v}{t}(t)  dt$">|; 

$key = q/displaystyleint_a^bOD{}{t}Big[p(t)OD{u}{t}(t)v(t)Big]dt=p(b)OD{u}{t}(b)v(b)-p(a)OD{u}{t}(a)v(a)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle \int_a^b \OD{}{t} \Big[ p(t)  \OD{u}{t}(t)  v(t) \Big]  dt = p(b)  \OD{u}{t}(b)  v(b) - p(a)  \OD{u}{t}(a)  v(a) = 0$">|; 

$key = q/displaystyleint_a^bOD{}{t}left[deriveepartielle{f}{v}wright]dt=deriveepartielle{f}{v}(b,u(b),u'(b))w(b)-deriveepartielle{f}{v}(a,u(a),u'(a))w(a)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.00ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \int_a^b \OD{}{t}\left[\deriveepartielle{f}{v}  w \right] dt = \...
...e{f}{v}(b,u(b),u'(b))  w(b) - \deriveepartielle{f}{v}(a,u(a),u'(a))  w(a) = 0$">|; 

$key = q/displaystyleint_a^bOD{}{t}left[deriveepartielle{f}{v}wright]dt=int_a^bOD{}{t}left[deriveepartielle{f}{v}right]wdt+int_a^bderiveepartielle{f}{v}w'dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.00ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle \int_a^b \OD{}{t}\left[\deriveepartielle{f}{v}  w \right] dt =
\...
...eepartielle{f}{v} \right]  w  dt +
\int_a^b \deriveepartielle{f}{v}  w'  dt$">|; 

$key = q/displaystyleint_a^bderiveepartielle{f}{v}w'dt=-int_a^bOD{}{t}left[deriveepartielle{f}{v}right]wdt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.00ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle \int_a^b \deriveepartielle{f}{v}  w'  dt = - \int_a^b
\OD{}{t} \left[ \deriveepartielle{f}{v} \right]  w  dt$">|; 

$key = q/displaystyleint_a^bg_i(t,u(t),u'(t))dt=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle \int_a^b g_i(t,u(t),u'(t))  dt = 0$">|; 

$key = q/displaystyleint_a^blambda_i(t)cdotg_i(t,u(t),u'(t))dt=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle \int_a^b \lambda_i(t) \cdot g_i(t,u(t),u'(t))  dt = 0$">|; 

$key = q/displaystyleint_a^blambda_icdotderiveepartielle{g_i}{v}w'dt=-int_a^bOD{}{t}left[lambda_icdotderiveepartielle{g_i}{v}right]wdt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.00ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle \int_a^b \lambda_i \cdot \deriveepartielle{g_i}{v}  w'  dt = - ...
..._a^b
\OD{}{t} \left[ \lambda_i \cdot \deriveepartielle{g_i}{v} \right]  w  dt$">|; 

$key = q/displaystyleint_a^bleft(deriveepartielle{f}{u}-OD{}{t}deriveepartielle{f}{v}right)wdt=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.00ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle \int_a^b \left( \deriveepartielle{f}{u} - \OD{}{t} \deriveepartielle{f}{v} \right)  w  dt = 0$">|; 

$key = q/displaystyleint_{u(0)}^{u(s)}frac{du}{A(u)}=int_0^sB(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle \int_{u(0)}^{u(s)} \frac{du}{A(u)} = \int_0^s B(t) dt$">|; 

$key = q/displaystylelambda_iincontinue([a,b]timessetR^2,setR),tmapstolambda_i(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle \lambda_i \in \continue([a,b]\times\setR^2,\setR),  t \mapsto \lambda_i(t)$">|; 

$key = q/displaystylelim_{nto+infty}sup_{aletleb}norme{u(t)-u^{(n)}(t)}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.71ex; vertical-align: -2.50ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \lim_{n \to +\infty} \sup_{a \le t \le b} \norme{u(t) - u^{(n)}(t)} = 0$">|; 

$key = q/displaystylelipschitz(A,B)={finfonction(A,B):existsLinsetR:forallx,yinA:norme{f(x)-f(y)}leLnorme{x-y}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\displaystyle \lipschitz(A,B) = \{ f \in \fonction(A,B) : \exists L \in \setR : \forall x,y \in A : \norme{f(x)-f(y)} \le L \norme{x-y} \}$">|; 

$key = q/displaystylemathcal{F}={uincontinue^2([a,b],setR):u(a)=U_1,u(b)=U_2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \mathcal{F} = \{ u \in \continue^2([a,b],\setR) : u(a) = U_1,  u(b) = U_2 \}$">|; 

$key = q/displaystylemathcal{G}={uinmathcal{F}:g_i(t,u(t),u'(t))=0foralliin{1,2,...,m}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle \mathcal{G} = \{ u \in \mathcal{F} : g_i(t,u(t),u'(t)) = 0    \forall i \in \{1,2,...,m\} \}$">|; 

$key = q/displaystylemathcal{L}(u)=OD{}{t}left[pcdotOD{u}{t}right]-qcdotu+fdollar;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle \mathcal{L}(u) = \OD{}{t}\left[p \cdot \OD{u}{t} \right] - q \cdot u + f \ $">|; 

$key = q/displaystylemathcal{L}(u)cdotv=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle \mathcal{L}(u) \cdot v = 0$">|; 

$key = q/displaystylemathcal{W}={wincontinue^2([a,b],setR):w(a)=w(b)=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \mathcal{W} = \{ w \in \continue^2([a,b],\setR) : w(a) = w(b) = 0 \}$">|; 

$key = q/displaystylemathcal{X}={uinmathcal{W}:g_i(t,u(t),u'(t))=0foralliin{1,2,...,m}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle \mathcal{X} = \{ u \in \mathcal{W} : g_i(t,u(t),u'(t)) = 0    \forall i \in \{1,2,...,m\} \}$">|; 

$key = q/displaystylenorme{x}=sqrt{sum_{i=1}^nx_i^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.62ex; vertical-align: -3.62ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \norme{x} = \sqrt{ \sum_{i=1}^n x_i^2 }$">|; 

$key = q/displaystylep,q,f:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle p, q, f : \setR \mapsto \setR$">|; 

$key = q/displaystyleu(0)=u_0+int_0^0f(s,u(s))ds=u_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle u(0) = u_0 + \int_0^0 f(s, u(s))  ds = u_0$">|; 

$key = q/displaystyleu(t)-u_0=int_0^tf(s,u(s))ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle u(t) - u_0 = \int_0^t f(s, u(s))  ds$">|; 

$key = q/displaystyleu(t)=u_0+int_0^tf(s,u(s))ds=A(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle u(t) = u_0 + \int_0^t f(s, u(s))  ds = A(u)$">|; 

$key = q/displaystyleu:setRmapstosetR,tmapstou(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle u : \setR \mapsto \setR,  t \mapsto u(t)$">|; 

$key = q/displaystyleu:tmapstov_0(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle u : t \mapsto v_0(t)$">|; 

$key = q/displaystyleu=A(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle u = A(u)$">|; 

$key = q/displaystyleu^{(n)}(t)=Abig(u^{(n-1)}big)=u_0+int_0^tfbig(s,u^{(n-1)}(s)big)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle u^{(n)}(t) = A\big(u^{(n-1)}\big) = u_0 + \int_0^t f\big(s,u^{(n-1)}(s)\big)  ds$">|; 

$key = q/dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img118.svg"
 ALT="$dt$">|; 

$key = q/epsiloninsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\epsilon \in \setR$">|; 

$key = q/f(t,u(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$f(t, u(t)$">|; 

$key = q/f:setR^2mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img115.svg"
 ALT="$f : \setR^2 \mapsto \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img69.svg"
 ALT="$f$">|; 

$key = q/finlipschitz(setRtimessetR^n,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$f\in\lipschitz( \setR \times \setR^n , \setR)$">|; 

$key = q/g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img78.svg"
 ALT="$g$">|; 

$key = q/g_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img82.svg"
 ALT="$g_i$">|; 

$key = q/iin{1,2,...,m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$i \in \{1, 2, ..., m\}$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img73.svg"
 ALT="$m$">|; 

$key = q/mathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\mathcal{F}$">|; 

$key = q/mathcal{L}(u)cdotv=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\mathcal{L}(u) \cdot v = 0$">|; 

$key = q/mathcal{L}:Fmapstocontinue([a,b],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\mathcal{L} : F \mapsto \continue([a,b],\setR)$">|; 

$key = q/mathcal{W};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\mathcal{W}$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img31.svg"
 ALT="$n$">|; 

$key = q/norme{.};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\norme{.}$">|; 

$key = q/setR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\setR^n$">|; 

$key = q/sup;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\sup$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$t$">|; 

$key = q/tinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img35.svg"
 ALT="$t \in \setR$">|; 

$key = q/u+epsiloncdotwinmathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.95ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img46.svg"
 ALT="$u + \epsilon \cdot w \in \mathcal{F}$">|; 

$key = q/u,v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$u,v$">|; 

$key = q/u:UsubseteqsetRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img5.svg"
 ALT="$u : U \subseteq \setR \mapsto \setR$">|; 

$key = q/u:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img30.svg"
 ALT="$u : \setR \mapsto \setR$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$u$">|; 

$key = q/u^{(n)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$u^{(n)}$">|; 

$key = q/uincontinue^2([a,b],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$u \in \continue^2([a,b],\setR)$">|; 

$key = q/uinmathcal{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img43.svg"
 ALT="$u \in \mathcal{F}$">|; 

$key = q/v:setRmapstosetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$v : \setR \mapsto \setR^n$">|; 

$key = q/v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img38.svg"
 ALT="$v$">|; 

$key = q/vinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img100.svg"
 ALT="$v\in F$">|; 

$key = q/vincontinue^2([a,b],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img112.svg"
 ALT="$v \in \continue^2([a,b],\setR)$">|; 

$key = q/w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img44.svg"
 ALT="$w$">|; 

$key = q/winmathcal{W};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img47.svg"
 ALT="$w \in \mathcal{W}$">|; 

$key = q/winmathcal{X};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img88.svg"
 ALT="$w \in \mathcal{X}$">|; 

$key = q/{Eqts}OD{tan(x)}{x}=1+tan(x)^2OD{arcsin(x)}{x}=frac{1}{sqrt{1-x^2}}OD{arccos(x)}{x}=-frac{1}{sqrt{1-x^2}}OD{arctan(x)}{x}=frac{1}{1+x^2}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 23.37ex; vertical-align: -11.15ex; " SRC="|."$dir".q|img124.svg"
 ALT="\begin{Eqts}
\OD{\tan(x)}{x} = 1 + \tan(x)^2 \\\\
\OD{\arcsin(x)}{x} = \frac{1}{\...
... -\frac{1}{\sqrt{1-x^2}} \\\\
\OD{\arctan(x)}{x} = \frac{1}{1+x^2} \\\\
\end{Eqts}">|; 

$key = q/{Eqts}OD{v}{t}=Matrix{{ccccc}0&1&0&hdots&0&0&1&0&hdotsvdots&&&&0&hdots&hdots&0&1-a_0slasha_n&-a_1slasha_n&hdots&hdots&-a_{n-1}slasha_nMatrix{cdotv{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.47ex; vertical-align: -7.18ex; " SRC="|."$dir".q|img36.svg"
 ALT="\begin{Eqts}
\OD{v}{t} =
\begin{Matrix}{ccccc}
0 &amp; 1 &amp; 0 &amp; \hdots &amp; \\\\
0 &amp; 0 &amp; ...
...0/a_n &amp; -a_1/a_n &amp; \hdots &amp; \hdots &amp; -a_{n-1}/a_n
\end{Matrix}\cdot
v
\end{Eqts}">|; 

$key = q/{Eqts}OD{y}{x}=cos(x)sin(x)^2+cos(x)^2=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.52ex; vertical-align: -3.70ex; " SRC="|."$dir".q|img122.svg"
 ALT="\begin{Eqts}
\OD{y}{x} = \cos(x) \\\\
\sin(x)^2 + \cos(x)^2 = 1
\end{Eqts}">|; 

$key = q/{Eqts}OD{y}{x}=sqrt{1-y^2}OD{}{y}arcsin(y)=unsur{sqrt{1-y^2}}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.51ex; vertical-align: -5.20ex; " SRC="|."$dir".q|img123.svg"
 ALT="\begin{Eqts}
\OD{y}{x} = \sqrt{1-y^2} \\\\
\OD{}{y}\arcsin(y) = \unsur{\sqrt{1-y^2}}
\end{Eqts}">|; 

$key = q/{Eqts}cos(x)=sum_{k=0}^{+infty}frac{(-1)^k}{(2k)!}x^{2k}sin(x)=sum_{k=0}^{+infty}frac{(-1)^k}{(2k+1)!}x^{2k+1}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.00ex; vertical-align: -6.95ex; " SRC="|."$dir".q|img125.svg"
 ALT="\begin{Eqts}
\cos(x) = \sum_{k=0}^{+\infty} \frac{(-1)^k}{(2k)!} x^{2k} \\\\
\sin(x) = \sum_{k=0}^{+\infty} \frac{(-1)^k}{(2k+1)!} x^{2k+1}
\end{Eqts}">|; 

$key = q/{Eqts}dot{u}=OD{u}{t}ddot{u}=OOD{u}{t}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.71ex; vertical-align: -4.80ex; " SRC="|."$dir".q|img27.svg"
 ALT="\begin{Eqts}
\dot{u} = \OD{u}{t} \\\\
\ddot{u} = \OOD{u}{t}
\end{Eqts}">|; 

$key = q/{Eqts}int_0^x{frac{1}{sqrt{1-xi^2}}dxi}=arcsin(x)int_0^x{frac{1}{1+xi^2}dxi}=arctan(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.08ex; vertical-align: -5.48ex; " SRC="|."$dir".q|img126.svg"
 ALT="\begin{Eqts}
\int_0^x {\frac{1}{\sqrt{1-\xi^2}}d\xi} = \arcsin(x) \\\\
\int_0^x {\frac{1}{1+\xi^2}d\xi} = \arctan(x)
\end{Eqts}">|; 

$key = q/{Eqts}u'(t)=OD{u}{t}(t)u''(t)=OOD{u}{t}(t){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.71ex; vertical-align: -4.80ex; " SRC="|."$dir".q|img28.svg"
 ALT="\begin{Eqts}
u'(t) = \OD{u}{t}(t) \\\\
u''(t) = \OOD{u}{t}(t)
\end{Eqts}">|; 

$key = q/{Eqts}u(a)+epsiloncdotw(a)=u(a)=U_1u(b)+epsiloncdotw(b)=u(b)=U_2{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img49.svg"
 ALT="\begin{Eqts}
u(a) + \epsilon \cdot w(a) = u(a) = U_1 \\\\
u(b) + \epsilon \cdot w(b) = u(b) = U_2
\end{Eqts}">|; 

$key = q/{Eqts}v(0)=(U_i)_{i=0,...,n-1}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img37.svg"
 ALT="\begin{Eqts}
v(0) = (U_i)_{i = 0, ..., n - 1}
\end{Eqts}">|; 

$key = q/{Eqts}v(t)=Big(v_i(t)Big)_{i=0,...,n-1}=Big(NOD{u}{t}{i}(t)Big)_{i=0,...,n-1}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.52ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img34.svg"
 ALT="\begin{Eqts}
v(t) = \Big(v_i(t)\Big)_{i = 0, ..., n - 1} = \Big( \NOD{u}{t}{i}(t) \Big)_{i = 0, ..., n - 1}
\end{Eqts}">|; 

$key = q/{Eqts}y=sin(x)x=arcsin(y){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img121.svg"
 ALT="\begin{Eqts}
y = \sin(x) \\\\
x = \arcsin(y)
\end{Eqts}">|; 

$key = q/{eqnarraystar}OD{u}{t}(t)&=&f(t,u(t))=a(t)cdotbbig(u(t)big)u(0)&=&u_0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.52ex; " SRC="|."$dir".q|img117.svg"
 ALT="\begin{eqnarray*}
\OD{u}{t}(t) &amp;=&amp; f(t, u(t)) = a(t) \cdot b\big( u(t) \big) \ \\\\
u(0) &amp;=&amp; u_0
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}OD{u}{t}(t)&=&f(t,u(t))u(0)&=&u_0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.52ex; " SRC="|."$dir".q|img17.svg"
 ALT="\begin{eqnarray*}
\OD{u}{t}(t) &amp;=&amp; f(t,u(t)) \ \\\\
u(0) &amp;=&amp; u_0
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}a(u,v)&=&int_a^bleft[p(t)cdotOD{u}{t}(t)cdotOD{v}{t}(t)+q(t)cdotu(t)cdotv(t)right]dtb(v)&=&int_a^bf(t)cdotv(t)dt{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.47ex; " SRC="|."$dir".q|img109.svg"
 ALT="\begin{eqnarray*}
a(u,v) &amp;=&amp; \int_a^b \left[ p(t) \cdot \OD{u}{t}(t) \cdot \OD{v...
... v(t) \right]  dt \ \\\\
b(v) &amp;=&amp; \int_a^b f(t) \cdot v(t)  dt
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}a_0(t)cdotu(t)+sum_{i=1}^{n-1}a_i(t)cdotNOD{u}{t}{i}(t)&=&0u(0)&=&U_0NOD{u}{t}{i}(0)&=&U_iqquad(i=1,...,n-1){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 25.35ex; " SRC="|."$dir".q|img32.svg"
 ALT="\begin{eqnarray*}
a_0(t) \cdot u(t) + \sum_{i=1}^{n-1} a_i(t) \cdot \NOD{u}{t}{i...
...) &amp;=&amp; U_0 \ \\\\
\NOD{u}{t}{i}(0) &amp;=&amp; U_i \qquad (i=1,...,n - 1)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}mathcal{L}(u)&=&0u(a)=u(b)&=&0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img99.svg"
 ALT="\begin{eqnarray*}
\mathcal{L}(u) &amp;=&amp; 0 \\\\
u(a) = u(b) &amp;=&amp; 0
\end{eqnarray*}">|; 

1;

