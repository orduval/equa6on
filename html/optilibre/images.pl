# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="\begin{theoreme}
\par Soit $\varphi \in \continue^2(\setR^n,\setR)$. Supposons q...
..., les conditions sur le gradient et la Hessienne seront remplies.
\end{theoreme}">|; 

$key = q/(A^dualcdotA)^dual=A^dualcdotA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img119.svg"
 ALT="$(A^\dual \cdot A)^\dual = A^\dual \cdot A$">|; 

$key = q/(gamma,lambda)insetR^ntimessetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$(\gamma,\lambda) \in \setR^n \times \setR^m$">|; 

$key = q/(s,t)inL;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$(s,t) \in L$">|; 

$key = q/(s,t)inLsetminus{(1,0),(0,1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$(s,t) \in L \setminus \{ (1,0),(0,1) \}$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$0$">|; 

$key = q/2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img88.svg"
 ALT="$2$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img128.svg"
 ALT="$A$">|; 

$key = q/A=[c_1...c_n];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img125.svg"
 ALT="$A = [c_1  ...  c_n]$">|; 

$key = q/A^dualcdotA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img123.svg"
 ALT="$A^\dual \cdot A$">|; 

$key = q/Ainmatrice(setR,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$A \in \matrice(\setR,m,n)$">|; 

$key = q/Delta=lambdacdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\Delta = \lambda \cdot h$">|; 

$key = q/Delta=tcdotdelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\Delta = t \cdot \delta$">|; 

$key = q/Delta=v-u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\Delta = v - u$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$E$">|; 

$key = q/Esimpetito{Delta^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$E \sim \petito{\Delta^2}$">|; 

$key = q/a,b,cinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img86.svg"
 ALT="$a,b,c \in \setR$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$a$">|; 

$key = q/ane0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img87.svg"
 ALT="$a \ne 0$">|; 

$key = q/astrictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img97.svg"
 ALT="$a \strictinferieur 0$">|; 

$key = q/astrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img95.svg"
 ALT="$a \strictsuperieur 0$">|; 

$key = q/binmatrice(setR,m,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img110.svg"
 ALT="$b \in \matrice(\setR,m,1)$">|; 

$key = q/delta_+,delta_-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\delta_+, \delta_-$">|; 

$key = q/deltainsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\delta \in \setR^n$">|; 

$key = q/displaystyle0=partialvarphi(gamma)cdot(lambda-gamma)levarphi(lambda)-varphi(gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle 0 = \partial \varphi(\gamma) \cdot (\lambda - \gamma) \le \varphi(\lambda) - \varphi(\gamma)$">|; 

$key = q/displaystyleA^dualcdot(Acdotxi-b)=A^dualcdotAcdotxi-A^dualcdotb=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\displaystyle A^\dual \cdot (A \cdot \xi - b) = A^\dual \cdot A \cdot \xi - A^\dual \cdot b = 0$">|; 

$key = q/displaystyleA^dualcdotAcdotxi=A^dualcdotb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.32ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\displaystyle A^\dual \cdot A \cdot \xi = A^\dual \cdot b$">|; 

$key = q/displaystyleDelta^dualcdotpartial^2f(a)cdotDelta+E(Delta)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \Delta^\dual \cdot \partial^2 f(a) \cdot \Delta + E(\Delta) \ge 0$">|; 

$key = q/displaystyleDelta^dualcdotpartial^2varphi(u)cdotDelta+petito{Delta^2}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \Delta^\dual \cdot \partial^2 \varphi(u) \cdot \Delta + \petito{\Delta^2} \ge 0$">|; 

$key = q/displaystyleL={(s,t)insetR^2:(s,t)ge0text{et}s+t=1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle L = \{ (s,t) \in \setR^2 : (s,t) \ge 0 \text{ et } s + t = 1 \}$">|; 

$key = q/displaystyleOD{p}{x}(gamma)=2cdotacdotgamma+b=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle \OD{p}{x}(\gamma) = 2 \cdot a \cdot \gamma + b = 0$">|; 

$key = q/displaystyleOD{p}{x}(x)=2cdotacdotx+b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \OD{p}{x}(x) = 2 \cdot a \cdot x + b$">|; 

$key = q/displaystyleOOD{p}{x}(x)=2cdota;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle \OOD{p}{x}(x) = 2 \cdot a$">|; 

$key = q/displaystylea=(A^dualcdotA)^{-1}cdotA^dualcdotb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle a = (A^\dual \cdot A)^{-1} \cdot A^\dual \cdot b$">|; 

$key = q/displaystylea=argmin_z(Acdotz-b)^dualcdot(Acdotz-b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle a = \arg\min_z (A \cdot z - b)^\dual \cdot (A \cdot z - b)$">|; 

$key = q/displaystyleabs{E(lambdacdoth)}lefrac{lambda^2}{2}cdoth^dualcdotpartial^2varphi(a)cdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \abs{E(\lambda \cdot h)} \le \frac{\lambda^2}{2} \cdot h^\dual \cdot \partial^2 \varphi(a) \cdot h$">|; 

$key = q/displaystyleargmax_{xinA}varphi(x)=argmin_{xinA}(-varphi(x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \arg\max_{x \in A} \varphi(x) = \arg\min_{x \in A} (-\varphi(x))$">|; 

$key = q/displaystyledelta=x-gamma=x+frac{b}{2a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle \delta = x - \gamma = x + \frac{b}{2 a}$">|; 

$key = q/displaystyledelta^2=frac{b^2-4cdotacdotc}{4cdota^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle \delta^2 = \frac{b^2 - 4 \cdot a \cdot c}{4 \cdot a^2}$">|; 

$key = q/displaystyledelta^dualcdotpartial^2varphi(u)cdotdeltage0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle \delta^\dual \cdot \partial^2 \varphi(u) \cdot \delta \ge 0$">|; 

$key = q/displaystyledifferentielle{varphi}{u}(tcdot(v-u))+petito{tcdot(v-u)}letcdot(varphi(v)-varphi(u));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \differentielle{\varphi}{u}(t \cdot (v - u)) + \petito{t \cdot (v - u)} \le t \cdot (\varphi(v) - \varphi(u))$">|; 

$key = q/displaystyledifferentielle{varphi}{u}(v-u)levarphi(v)-varphi(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \differentielle{\varphi}{u}(v - u) \le \varphi(v) - \varphi(u)$">|; 

$key = q/displaystylee(x)=Acdotx-b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle e(x) = A \cdot x - b$">|; 

$key = q/displaystylefrac{lambda^2}{2}cdoth^dualcdotpartial^2f(a)cdoth+E(lambdacdoth)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \frac{\lambda^2}{2} \cdot h^\dual \cdot \partial^2 f(a) \cdot h + E(\lambda \cdot h) \ge 0$">|; 

$key = q/displaystylegamma=-frac{b}{2a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle \gamma = -\frac{b}{2a}$">|; 

$key = q/displaystylelagrangien(gamma,y)lelagrangien(gamma,lambda)lelagrangien(x,lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \lagrangien(\gamma,y) \le \lagrangien(\gamma,\lambda) \le \lagrangien(x,\lambda)$">|; 

$key = q/displaystylelambdainargmin_{xinsetR^n}varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \lambda \in \arg\min_{x \in \setR^n} \varphi(x)$">|; 

$key = q/displaystylemathcal{E}(x)=norme{x}^2=e(x)^dualcdote(x)=(Acdotx-b)^dualcdot(Acdotx-b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\displaystyle \mathcal{E}(x) = \norme{x}^2 = e(x)^\dual \cdot e(x) = (A \cdot x - b)^\dual \cdot (A \cdot x - b)$">|; 

$key = q/displaystylemathcal{E}(x)=x^dualcdotA^dualcdotAcdotx-x^dualcdotA^dualcdotb-b^dualcdotAcdotx+b^dualcdotb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\displaystyle \mathcal{E}(x) = x^\dual \cdot A^\dual \cdot A \cdot x - x^\dual \cdot A^\dual \cdot b - b^\dual \cdot A \cdot x + b^\dual \cdot b$">|; 

$key = q/displaystylep(delta+gamma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle p(\delta + \gamma) = 0$">|; 

$key = q/displaystylep(x)=acdotx^2+bcdotx+c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle p(x) = a \cdot x^2 + b \cdot x + c$">|; 

$key = q/displaystylepartialmathcal{E}(xi)=2A^dualcdotAcdotxi-2A^dualcdotb=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\displaystyle \partial \mathcal{E}(\xi) = 2 A^\dual \cdot A \cdot \xi - 2 A^\dual \cdot b = 0$">|; 

$key = q/displaystylepartialvarphi(u)cdot(v-u)levarphi(v)-varphi(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \partial \varphi(u) \cdot (v - u) \le \varphi(v) - \varphi(u)$">|; 

$key = q/displaystylepartialvarphi(u)cdotDeltalevarphi(v)-varphi(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \partial \varphi(u) \cdot \Delta \le \varphi(v) - \varphi(u)$">|; 

$key = q/displaystylepsi(scdotu+tcdotv)gescdotpsi(u)+tcdotpsi(v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle \psi(s \cdot u + t \cdot v) \ge s \cdot \psi(u) + t \cdot \psi(v)$">|; 

$key = q/displaystylepsi(scdotu+tcdotv)strictsuperieurscdotpsi(u)+tcdotpsi(v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle \psi(s \cdot u + t \cdot v) \strictsuperieur s \cdot \psi(u) + t \cdot \psi(v)$">|; 

$key = q/displaystylescalaire{c_i}{r}=c_i^dualcdotr=ligne_i[A^dualcdot(Acdotxi-b)]=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.02ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\displaystyle \scalaire{c_i}{r} = c_i^\dual \cdot r = \ligne_i [ A^\dual \cdot (A \cdot \xi - b) ] = 0$">|; 

$key = q/displaystylesum_{i=1}^{m}(u(x_i)-w(x_i))^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle \sum_{i=1}^{m} ( u(x_i) - w(x_i) )^2$">|; 

$key = q/displaystylet^2cdotdelta^dualcdotpartial^2varphi(u)cdotdelta+petito{t^2cdotdelta^2}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle t^2 \cdot \delta^\dual \cdot \partial^2 \varphi(u) \cdot \delta + \petito{t^2 \cdot \delta^2} \ge 0$">|; 

$key = q/displaystyletcdotdifferentielle{varphi}{u}(v-u)+petito{tcdot(v-u)}letcdot(varphi(v)-varphi(u));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle t \cdot \differentielle{\varphi}{u}(v - u) + \petito{t \cdot (v - u)} \le t \cdot (\varphi(v) - \varphi(u))$">|; 

$key = q/displaystyleu=argmin_{xinsetR^n}varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle u = \arg\min_{x \in \setR^n} \varphi(x)$">|; 

$key = q/displaystyleunsur{2}Delta^dualcdotpartial^2varphi(a)cdotDelta+E(Delta)geunsur{2}Delta^dualcdotpartial^2varphi(a)cdotDelta-abs{E(Delta)}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \unsur{2} \Delta^\dual \cdot \partial^2 \varphi(a) \cdot \Delta +...
...} \Delta^\dual \cdot \partial^2 \varphi(a) \cdot \Delta - \abs{E(\Delta)} \ge 0$">|; 

$key = q/displaystyleunsur{2}cdotDelta^dualcdotpartial^2varphi(u)cdotDelta+petito{Delta^2}=varphi(v)-varphi(u)-partialvarphi(u)cdotDeltage0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \unsur{2} \cdot \Delta^\dual \cdot \partial^2 \varphi(u) \cdot \D...
...to{\Delta^2} = \varphi(v) - \varphi(u) - \partial \varphi(u) \cdot \Delta \ge 0$">|; 

$key = q/displaystyleunsur{2}cdoth^dualcdotpartial^2f(a)cdoth+frac{E(lambdacdoth)}{lambda^2cdotnorme{h}^2}cdotnorme{h}^2ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.94ex; vertical-align: -2.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \unsur{2} \cdot h^\dual \cdot \partial^2 f(a) \cdot h + \frac{E(\lambda \cdot h)}{\lambda^2 \cdot \norme{h}^2} \cdot \norme{h}^2 \ge 0$">|; 

$key = q/displaystyleunsur{2}cdoth^dualcdotpartial^2f(a)cdothge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \unsur{2} \cdot h^\dual \cdot \partial^2 f(a) \cdot h \ge 0$">|; 

$key = q/displaystylevarphi(a+Delta)=varphi(a)+frac{lambda^2}{2}cdoth^dualcdotpartial^2varphi(a)cdoth+E(lambdacdoth);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \varphi(a + \Delta) = \varphi(a) + \frac{\lambda^2}{2} \cdot h^\dual \cdot \partial^2 \varphi(a) \cdot h + E(\lambda \cdot h)$">|; 

$key = q/displaystylevarphi(a+Delta)=varphi(a)+partialvarphi(a)cdotDelta+unsur{2}Delta^dualcdotpartial^2varphi(a)cdotDelta+E(Delta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle \varphi(a + \Delta) = \varphi(a) + \partial \varphi(a) \cdot \Delta + \unsur{2} \Delta^\dual \cdot \partial^2 \varphi(a) \cdot \Delta + E(\Delta)$">|; 

$key = q/displaystylevarphi(a+Delta)=varphi(a)+unsur{2}Delta^dualcdotpartial^2varphi(a)cdotDelta+E(Delta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \varphi(a + \Delta) = \varphi(a) + \unsur{2} \Delta^\dual \cdot \partial^2 \varphi(a) \cdot \Delta + E(\Delta)$">|; 

$key = q/displaystylevarphi(a+Delta)=varphi(a)+unsur{2}Delta^dualcdotpartial^2varphi(a)cdotDelta+E(Delta)gevarphi(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \varphi(a + \Delta) = \varphi(a) + \unsur{2} \Delta^\dual \cdot \partial^2 \varphi(a) \cdot \Delta + E(\Delta) \ge \varphi(a)$">|; 

$key = q/displaystylevarphi(a+Delta)=varphi(a)+unsur{2}cdotDelta^dualcdotpartial^2varphi(a)cdotDelta+E(Delta)gevarphi(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \varphi(a + \Delta) = \varphi(a) + \unsur{2} \cdot \Delta^\dual \cdot \partial^2 \varphi(a) \cdot \Delta + E(\Delta) \ge \varphi(a)$">|; 

$key = q/displaystylevarphi(gamma)levarphi(lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \varphi(\gamma) \le \varphi(\lambda)$">|; 

$key = q/displaystylevarphi(scdotu+tcdotv)lescdotvarphi(u)+tcdotvarphi(v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \varphi(s \cdot u + t \cdot v) \le s \cdot \varphi(u) + t \cdot \varphi(v)$">|; 

$key = q/displaystylevarphi(scdotu+tcdotv)strictinferieurscdotvarphi(u)+tcdotvarphi(v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \varphi(s \cdot u + t \cdot v) \strictinferieur s \cdot \varphi(u) + t \cdot \varphi(v)$">|; 

$key = q/displaystylevarphi(u+tcdot(v-u))-varphi(u)letcdot(varphi(v)-varphi(u));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \varphi(u + t \cdot (v - u)) - \varphi(u) \le t \cdot (\varphi(v) - \varphi(u))$">|; 

$key = q/displaystylevarphi(u+tcdot(v-u))levarphi(u)+tcdot(varphi(v)-varphi(u));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle \varphi(u + t \cdot (v - u)) \le \varphi(u) + t \cdot (\varphi(v) - \varphi(u))$">|; 

$key = q/displaystylevarphi(v)=varphi(u)+partialvarphi(u)cdotDelta+unsur{2}cdotDelta^dualcdotpartial^2varphi(u)cdotDelta+petito{Delta^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \varphi(v) = \varphi(u) + \partial \varphi(u) \cdot \Delta + \uns...
...\cdot \Delta^\dual \cdot \partial^2 \varphi(u) \cdot \Delta + \petito{\Delta^2}$">|; 

$key = q/displaystylew(x)=sum_{i=1}^ma_icdotvarphi_i(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle w(x) = \sum_{i=1}^m a_i \cdot \varphi_i(x)$">|; 

$key = q/displaystylex=gamma+delta=-frac{b}{2a}+delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle x = \gamma + \delta = - \frac{b}{2a} + \delta$">|; 

$key = q/displaystylex^2=(gamma+delta)^2=delta^2-frac{deltacdotb}{a}+frac{b^2}{4cdota^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\displaystyle x^2 = (\gamma + \delta)^2 = \delta^2 - \frac{\delta \cdot b}{a} + \frac{b^2}{4 \cdot a^2}$">|; 

$key = q/displaystylexi=left(A^dualcdotAright)^{-1}cdotA^dualcdotb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\displaystyle \xi = \left(A^\dual \cdot A\right)^{-1} \cdot A^\dual \cdot b$">|; 

$key = q/e;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img112.svg"
 ALT="$e$">|; 

$key = q/f:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img26.svg"
 ALT="$f: \setR \mapsto \setR$">|; 

$key = q/gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\gamma$">|; 

$key = q/hinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img5.svg"
 ALT="$h \in \setR^n$">|; 

$key = q/lagrangien(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\lagrangien(x,y)$">|; 

$key = q/lagrangien:setR^ntimessetR^mmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\lagrangien : \setR^n \times \setR^m \mapsto \setR$">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\lambda$">|; 

$key = q/lambda^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\lambda^2$">|; 

$key = q/lambdainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\lambda \in \setR$">|; 

$key = q/lambdastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\lambda \strictsuperieur 0$">|; 

$key = q/nlem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img136.svg"
 ALT="$n \le m$">|; 

$key = q/norme{Delta}^2=lambda^2cdotnorme{h}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\norme{\Delta}^2 = \lambda^2 \cdot \norme{h}^2$">|; 

$key = q/norme{x}insetR^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\norme{x} \in \setR^+$">|; 

$key = q/norme{x}mapstonorme{x}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\norme{x} \mapsto \norme{x}^2$">|; 

$key = q/p(x_+)=p(x_-)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$p(x_+) = p(x_-) = 0$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img96.svg"
 ALT="$p$">|; 

$key = q/partialvarphi(gamma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\partial \varphi(\gamma) = 0$">|; 

$key = q/partialvarphi(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\partial \varphi(x) = 0$">|; 

$key = q/psi:setR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\psi : \setR^n \mapsto \setR$">|; 

$key = q/psi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\psi$">|; 

$key = q/r=Acdotxi-b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img129.svg"
 ALT="$r = A \cdot \xi - b$">|; 

$key = q/s=1-t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img40.svg"
 ALT="$s = 1 - t$">|; 

$key = q/setR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\setR^n$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img44.svg"
 ALT="$t$">|; 

$key = q/t^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img60.svg"
 ALT="$t^2$">|; 

$key = q/tinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img56.svg"
 ALT="$t \in \setR$">|; 

$key = q/tto0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img46.svg"
 ALT="$t \to 0$">|; 

$key = q/u,vinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img37.svg"
 ALT="$u, v \in \setR^n$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img132.svg"
 ALT="$u$">|; 

$key = q/uinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img62.svg"
 ALT="$u \in \setR^n$">|; 

$key = q/unev;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img73.svg"
 ALT="$u \ne v$">|; 

$key = q/v=u+tcdotdelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img57.svg"
 ALT="$v = u + t \cdot \delta$">|; 

$key = q/varphi(gamma)=varphi(lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\varphi(\gamma) = \varphi(\lambda)$">|; 

$key = q/varphi(lambda)levarphi(gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\varphi(\lambda) \le \varphi(\gamma)$">|; 

$key = q/varphi(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\varphi(u)$">|; 

$key = q/varphi(u)=varphi(v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\varphi(u) = \varphi(v)$">|; 

$key = q/varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\varphi(x)$">|; 

$key = q/varphi:setR^nmapstosetR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\varphi : \setR^n \mapsto \setR)$">|; 

$key = q/varphi:setR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\varphi : \setR^n \mapsto \setR$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\varphi$">|; 

$key = q/varphi=-psi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\varphi = -\psi$">|; 

$key = q/varphi_1(x),...,varphi_n(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\varphi_1(x),...,\varphi_n(x)$">|; 

$key = q/w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img131.svg"
 ALT="$w$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$x$">|; 

$key = q/x^dualcdotA^dualcdotb=b^dualcdotA^dualcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img120.svg"
 ALT="$x^\dual \cdot A^\dual \cdot b = b^\dual \cdot A^\dual \cdot x$">|; 

$key = q/x_+,x_-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img106.svg"
 ALT="$x_+,x_-$">|; 

$key = q/x_1,...,x_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img133.svg"
 ALT="$x_1,...,x_m$">|; 

$key = q/xiinmatrice(setR^n,n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\xi \in \matrice(\setR^n,n,1)$">|; 

$key = q/xinmatrice(setR^n,n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img114.svg"
 ALT="$x \in \matrice(\setR^n,n,1)$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img90.svg"
 ALT="$x \in \setR$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img33.svg"
 ALT="$y$">|; 

$key = q/{Eqts}A^dual=Matrix{{c}c_1^dualvdotsc_n^dualMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.91ex; vertical-align: -4.39ex; " SRC="|."$dir".q|img126.svg"
 ALT="\begin{Eqts}
A^\dual =
\begin{Matrix}{c}
c_1^\dual \ \vdots \ c_n^\dual
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}OD{f}{t}(a)=0OOD{f}{t}(a)ge0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.71ex; vertical-align: -4.80ex; " SRC="|."$dir".q|img27.svg"
 ALT="\begin{Eqts}
\OD{f}{t}(a) = 0 \\\\
\OOD{f}{t}(a) \ge 0
\end{Eqts}">|; 

$key = q/{Eqts}OD{f}{t}(a)=0OOD{f}{t}(a)le0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.71ex; vertical-align: -4.80ex; " SRC="|."$dir".q|img28.svg"
 ALT="\begin{Eqts}
\OD{f}{t}(a) = 0 \\\\
\OOD{f}{t}(a) \le 0
\end{Eqts}">|; 

$key = q/{Eqts}delta_+=frac{sqrt{b^2-4cdotacdotc}}{2cdota}delta_-=-frac{sqrt{b^2-4cdotacdotc}}{2cdota}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.45ex; vertical-align: -5.17ex; " SRC="|."$dir".q|img105.svg"
 ALT="\begin{Eqts}
\delta_+ = \frac{\sqrt{ b^2 - 4 \cdot a \cdot c }}{2 \cdot a} \\\\
\delta_- = - \frac{\sqrt{ b^2 - 4 \cdot a \cdot c }}{2 \cdot a}
\end{Eqts}">|; 

$key = q/{Eqts}x_+=frac{-b+sqrt{b^2-4cdotacdotc}}{2cdota}x_-=frac{-b-sqrt{b^2-4cdotacdotc}}{2cdota}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.02ex; vertical-align: -6.96ex; " SRC="|."$dir".q|img107.svg"
 ALT="\begin{Eqts}
x_+ = \frac{-b + \sqrt{b^2 - 4 \cdot a \cdot c}}{2 \cdot a} \ \\\\
x_- = \frac{-b - \sqrt{b^2 - 4 \cdot a \cdot c}}{2 \cdot a}
\end{Eqts}">|; 

$key = q/{eqnarraystar}A&=&[varphi_j(x_i)]_{i,j}a&=&[a_1a_2...a_m]^Tb&=&[u(x_1)u(x_2)...u(x_n)]^T{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img138.svg"
 ALT="\begin{eqnarray*}
A &amp;=&amp; [\varphi_j(x_i)]_{i,j} \\\\
a &amp;=&amp; [a_1  a_2  ...  a_m]^T \\\\
b &amp;=&amp; [u(x_1)  u(x_2)  ...  u(x_n)]^T
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}varphi(scdotu+tcdotv)&strictinferieur&scdotvarphi(u)+tcdotvarphi(v)&strictinferieur&(s+t)cdotvarphi(u)=varphi(u){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img77.svg"
 ALT="\begin{eqnarray*}
\varphi(s \cdot u + t \cdot v) &amp;\strictinferieur&amp; s \cdot \var...
...(v) \\\\
&amp;\strictinferieur&amp; (s + t) \cdot \varphi(u) = \varphi(u)
\end{eqnarray*}">|; 

1;

