# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 25.47ex; " SRC="|."$dir".q|img17.svg"
 ALT="\begin{eqnarray*}
\distance( u_{n + m} , u_n ) &amp;\le&amp; \sum_{i = 0}^{m - 1} c^{n +...
...\sum_{i = 0}^{m - 1} c^i \\\\
&amp;\le&amp; c^n \cdot \frac{1 - c^m}{1-c}
\end{eqnarray*}">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$0$">|; 

$key = q/1-c^mle1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.01ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img18.svg"
 ALT="$1 - c^m \le 1$">|; 

$key = q/A(p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$A(p)$">|; 

$key = q/A(u_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$A(u_n)$">|; 

$key = q/A:XmapstoX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$A : X \mapsto X$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img45.svg"
 ALT="$A$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$N$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$X$">|; 

$key = q/c^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img24.svg"
 ALT="$c^n$">|; 

$key = q/cinintervallesemiouvertdroite{0}{1}subseteqsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$c \in \intervallesemiouvertdroite{0}{1} \subseteq \setR$">|; 

$key = q/cstrictinferieur1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img50.svg"
 ALT="$c \strictinferieur 1$">|; 

$key = q/displaystyleA(p)=lim_{ntoinfty}A(u_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle A(p) = \lim_{n \to \infty} A(u_n)$">|; 

$key = q/displaystyleA(p)=p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle A(p) = p$">|; 

$key = q/displaystylec^Nlefrac{epsiloncdot(1-c)}{distance(u_1,u_0)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle c^N \le \frac{\epsilon \cdot (1 - c)}{\distance( u_1 , u_0 )}$">|; 

$key = q/displaystyledistance(p_1,p_2)leccdotdistancebig(A(p_1),A(p_2)big)leccdotdistanceleft(p_1,p_2right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \distance( p_1 , p_2 ) \le c \cdot \distance\big( A(p_1) , A(p_2) \big) \le c \cdot \distance\left( p_1 , p_2 \right)$">|; 

$key = q/displaystyledistance(u_i,u_j)=distance(u_j,u_i)leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \distance( u_i , u_j ) = \distance( u_j , u_i ) \le \epsilon$">|; 

$key = q/displaystyledistance(u_n,p)ledistancebig(A(u_{n-1}),A(p)big)leccdotdistance(u_{n-1},p)le...lec^ncdotdistance(u_0,p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \distance(u_n,p) \le \distance\big(A(u_{n - 1}),A(p)\big) \le c \cdot \distance(u_{n - 1},p) \le ... \le c^n \cdot \distance(u_0,p)$">|; 

$key = q/displaystyledistance(u_{n+1},u_n)leccdotdistance(u_n,u_{n-1})le...lec^ncdotdistance(u_1,u_0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \distance( u_{n + 1} , u_n ) \le c \cdot \distance( u_n , u_{n - 1} ) \le ... \le c^n \cdot \distance( u_1 , u_0 )$">|; 

$key = q/displaystyledistance(u_{n+m},u_n)lefrac{c^n}{1-c}cdotdistance(u_1,u_0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.04ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \distance( u_{n + m} , u_n ) \le \frac{c^n}{1 - c} \cdot \distance( u_1 , u_0 )$">|; 

$key = q/displaystyledistance(u_{n+m},u_n)lesum_{i=0}^{m-1}distance(u_{n+i+1},u_{n+i});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.22ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \distance( u_{n + m} , u_n ) \le \sum_{i = 0}^{m - 1} \distance( u_{n + i + 1} , u_{n + i} )$">|; 

$key = q/displaystyledistancebig(A(p),A(u_n)big)leccdotdistancebig(p,u_nbig);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \distance\big( A(p) , A(u_n) \big) \le c \cdot \distance\big( p , u_n \big)$">|; 

$key = q/displaystyledistancebig(A(p),pbig)ledistancebig(A(p),A(u_n)big)+distancebig(A(u_n),u_nbig)+distancebig(u_n,pbig);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \distance\big( A(p) , p \big) \le \distance\big( A(p) , A(u_n) \big) + \distance\big( A(u_n) , u_n \big) + \distance\big( u_n , p \big)$">|; 

$key = q/displaystyledistancebig(A(u),A(v)big)leccdotdistance(u,v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \distance\big( A(u) , A(v) \big) \le c \cdot \distance(u,v)$">|; 

$key = q/displaystylep=lim_{ntoinfty}u_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.42ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle p = \lim_{n \to \infty} u_n$">|; 

$key = q/displaystylep_1=p_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle p_1 = p_2$">|; 

$key = q/displaystyleu_n=A(u_{n-1})=...=A^n(u_0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle u_n = A(u_{n - 1}) = ... = A^n(u_0)$">|; 

$key = q/distance(p_1,p_2)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\distance(p_1,p_2) = 0$">|; 

$key = q/distance(u_n,p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\distance( u_n , p )$">|; 

$key = q/distance(u_{n+i+1},u_{n+i})lec^{n+i}cdotdistance(u_1,u_0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\distance( u_{n + i + 1} , u_{n + i} ) \le c^{n + i} \cdot \distance( u_1 , u_0 )$">|; 

$key = q/distance(x,y)=norme{x-y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\distance(x,y) = \norme{x - y}$">|; 

$key = q/distancebig(A(p),pbig)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\distance\big( A(p) , p \big) = 0$">|; 

$key = q/distancebig(A(u_n),u_nbig)=distance(u_{n+1},u_n)to0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\distance\big( A(u_n) , u_n \big) = \distance( u_{n + 1} , u_n ) \to 0$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/i,jgeN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$i,j \ge N$">|; 

$key = q/i,jinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$i,j \in \setN$">|; 

$key = q/m,ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$m,n \in \setN$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$m$">|; 

$key = q/minsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img19.svg"
 ALT="$m \in \setN$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$n$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$n \in \setN$">|; 

$key = q/ntoinfty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img26.svg"
 ALT="$n \to \infty$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img37.svg"
 ALT="$p$">|; 

$key = q/p_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$p_1$">|; 

$key = q/p_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img47.svg"
 ALT="$p_2$">|; 

$key = q/pinX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$p \in X$">|; 

$key = q/setC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\setC$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\setR$">|; 

$key = q/u,vinX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$u,v\in X$">|; 

$key = q/u_0,u_1,u_2,...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$u_0,u_1,u_2,...$">|; 

$key = q/u_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img53.svg"
 ALT="$u_0$">|; 

$key = q/u_0inX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img9.svg"
 ALT="$u_0 \in X$">|; 

$key = q/u_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img32.svg"
 ALT="$u_n$">|; 

$key = q/{Eqts}A(p_1)=p_1A(p_2)=p_2{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img48.svg"
 ALT="\begin{Eqts}
A(p_1) = p_1 \\\\
A(p_2) = p_2
\end{Eqts}">|; 

1;

