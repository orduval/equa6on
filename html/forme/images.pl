# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img48.svg"
 ALT="\begin{Eqts}
\biforme{\alpha \cdot x + \beta \cdot y}{\vartheta}{u} = \alpha \cd...
...dot \biforme{x}{\vartheta}{u} + \beta \cdot \biforme{x}{\vartheta}{v}
\end{Eqts}">|; 

$key = q/(Phi_1,...,Phi_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$(\Phi_1,...,\Phi_m)$">|; 

$key = q/(e_1,...,e_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$(e_1,...,e_n)$">|; 

$key = q/(f_1,...,f_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$(f_1,...,f_m)$">|; 

$key = q/(i,j)insetZ(0,m)timessetZ(0,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$(i,j) \in \setZ(0,m) \times \setZ(0,n)$">|; 

$key = q/A:EmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$A : E \mapsto F$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$A$">|; 

$key = q/A^dual:F^dualmapstoE^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img42.svg"
 ALT="$A^\dual : F^\dual \mapsto E^\dual$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$E$">|; 

$key = q/E^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$E^\dual$">|; 

$key = q/E^{dualdual};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$E^{\dual \dual}$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$F$">|; 

$key = q/Thetainmatrice(K,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\Theta \in \matrice(K,m,n)$">|; 

$key = q/alpha,betainS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\alpha,\beta \in S$">|; 

$key = q/alpha,betaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\alpha,\beta \in \corps$">|; 

$key = q/alpha_1,...,alpha_minS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\alpha_1,...,\alpha_m \in S$">|; 

$key = q/alpha_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\alpha_j$">|; 

$key = q/beta_1,...,beta_ninS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\beta_1,...,\beta_n \in S$">|; 

$key = q/beta_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\beta_j$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\corps$">|; 

$key = q/corps^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\corps^m$">|; 

$key = q/corps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\corps^n$">|; 

$key = q/displaystyleE^dual={varphiinlineaire(E,corps):norme{varphi}_lineairestrictinferieur+infty};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle E^\dual = \{ \varphi \in \lineaire(E,\corps) : \norme{\varphi}_\lineaire \strictinferieur +\infty \}$">|; 

$key = q/displaystyleE^{dualdual}=(E^dual)^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle E^{\dual \dual} = (E^\dual)^\dual$">|; 

$key = q/displaystylePhi_j(u)=forme{Phi_j}{u}=sum_{i=1}^nbeta_icdotforme{Phi_j}{e_i}=sum_{i=1}^nbeta_icdotindicatrice_{ij}=beta_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \Phi_j(u) = \forme{\Phi_j}{u} = \sum_{i = 1}^n \beta_i \cdot \forme{\Phi_j}{e_i} = \sum_{i = 1}^n \beta_i \cdot \indicatrice_{ij} = \beta_j$">|; 

$key = q/displaystylebiforme{v}{vartheta}{u}=v^TcdotThetacdotu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle \biforme{v}{\vartheta}{u} = v^T \cdot \Theta \cdot u$">|; 

$key = q/displaystylebiforme{x}{vartheta}{u}=vartheta(x,u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \biforme{x}{\vartheta}{u} = \vartheta(x,u)$">|; 

$key = q/displaystylecomposante_{ij}Theta=biforme{f_i}{vartheta}{e_j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.32ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle \composante_{ij} \Theta = \biforme{f_i}{\vartheta}{e_j}$">|; 

$key = q/displaystyleforme{A^dual(varphi)}{u}=forme{varphi}{A(u)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \forme{ A^\dual(\varphi) }{u} = \forme{\varphi}{ A(u) }$">|; 

$key = q/displaystyleforme{Phi_i}{e_j}=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \forme{\Phi_i}{e_j} = \indicatrice_{ij}$">|; 

$key = q/displaystyleforme{alphacdotvarphi+betacdotpsi}{u}=alphacdotforme{varphi}{u}+betacdotforme{psi}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \forme{\alpha \cdot \varphi + \beta \cdot \psi}{u} = \alpha \cdot \forme{\varphi}{u} + \beta \cdot \forme{\psi}{u}$">|; 

$key = q/displaystyleforme{hat{u}}{varphi}=forme{varphi}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \forme{\hat{u}}{\varphi} = \forme{\varphi}{u}$">|; 

$key = q/displaystyleforme{varphi}{alphacdotu+betacdotv}=alphacdotforme{varphi}{u}+betacdotforme{varphi}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \forme{\varphi}{\alpha \cdot u + \beta \cdot v} = \alpha \cdot \forme{\varphi}{u} + \beta \cdot \forme{\varphi}{v}$">|; 

$key = q/displaystyleforme{varphi}{u}=forme{psi}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \forme{\varphi}{u} = \forme{\psi}{u}$">|; 

$key = q/displaystyleforme{varphi}{u}=forme{varphi}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \forme{\varphi}{u} = \forme{\varphi}{v}$">|; 

$key = q/displaystyleforme{varphi}{u}=hat{varphi}^Tcdotu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \forme{\varphi}{u} = \hat{\varphi}^T \cdot u$">|; 

$key = q/displaystyleforme{varphi}{u}=sum_iforme{varphi}{e_i}cdotforme{Phi_i}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \forme{\varphi}{u} = \sum_i \forme{\varphi}{e_i} \cdot \forme{\Phi_i}{u}$">|; 

$key = q/displaystyleforme{varphi}{u}=sum_{i,j}alpha_icdotforme{Phi_i}{u_j}cdotbeta_j=sum_{i,j}alpha_icdotindicatrice_{ij}cdotbeta_j=sum_ialpha_icdotbeta_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \forme{\varphi}{u} = \sum_{i,j} \alpha_i \cdot \forme{\Phi_i}{u_j...
... \alpha_i \cdot \indicatrice_{ij} \cdot \beta_j = \sum_i \alpha_i \cdot \beta_i$">|; 

$key = q/displaystyleforme{varphi}{v}=varphi(v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \forme{\varphi}{v} = \varphi(v)$">|; 

$key = q/displaystylehat{u}(varphi)=varphi(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \hat{u}(\varphi) = \varphi(u)$">|; 

$key = q/displaystylehat{varphi}_i=forme{varphi}{e_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle \hat{\varphi}_i = \forme{\varphi}{e_i}$">|; 

$key = q/displaystylemathcal{Q}(x)=biforme{x}{vartheta}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \mathcal{Q}(x) = \biforme{x}{\vartheta}{x}$">|; 

$key = q/displaystyleu=sum_{i=1}^nbeta_icdote_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle u = \sum_{i = 1}^n \beta_i \cdot e_i$">|; 

$key = q/displaystylevarphi(e_j)=forme{varphi}{e_j}=sum_{i=1}^malpha_icdotforme{Phi_i}{e_j}=sum_{i=1}^malpha_icdotindicatrice_{ij}=alpha_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \varphi(e_j) = \forme{\varphi}{e_j} = \sum_{i = 1}^m \alpha_i \cd...
...forme{\Phi_i}{e_j} = \sum_{i = 1}^m \alpha_i \cdot \indicatrice_{ij} = \alpha_j$">|; 

$key = q/displaystylevarphi=sum_{i=1}^malpha_icdotPhi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \varphi = \sum_{i = 1}^m \alpha_i \cdot \Phi_i$">|; 

$key = q/forme{}{}:E^dualtimesEmapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\forme{}{} : E^\dual \times E \mapsto \corps$">|; 

$key = q/forme{}{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\forme{}{}$">|; 

$key = q/hat{u}inE^{dualdual};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\hat{u} \in E^{\dual \dual}$">|; 

$key = q/hat{varphi}incorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\hat{\varphi} \in \corps^n$">|; 

$key = q/mathcal{Q}:Emapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.01ex; vertical-align: -0.32ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\mathcal{Q} : E \mapsto \corps$">|; 

$key = q/u,vinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$u,v \in E$">|; 

$key = q/uinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img33.svg"
 ALT="$u \in E$">|; 

$key = q/uincorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img59.svg"
 ALT="$u \in \corps^n$">|; 

$key = q/varphi,psiinE^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\varphi,\psi \in E^\dual$">|; 

$key = q/varphi:Emapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\varphi : E \mapsto \corps$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\varphi$">|; 

$key = q/varphiinE^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\varphi \in E^\dual$">|; 

$key = q/varphiinF^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\varphi \in F^\dual$">|; 

$key = q/varphiinlineaire(corps^n,corps);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\varphi \in \lineaire(\corps^n,\corps)$">|; 

$key = q/vartheta:EtimesEmapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\vartheta : E \times E \mapsto \corps$">|; 

$key = q/vartheta:FtimesEmapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\vartheta : F \times E \mapsto \corps$">|; 

$key = q/varthetainlineaire(corps^mtimescorps^n,corps);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\vartheta \in \lineaire(\corps^m \times \corps^n,\corps)$">|; 

$key = q/vinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$v \in E$">|; 

$key = q/vincorps^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img66.svg"
 ALT="$v \in \corps^m$">|; 

$key = q/x,yinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img50.svg"
 ALT="$x,y \in F$">|; 

$key = q/xinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img47.svg"
 ALT="$x \in F$">|; 

1;

