# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/a,b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img45.svg"
 ALT="$a, b$">|; 

$key = q/a,binsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img39.svg"
 ALT="$a, b \in \setR$">|; 

$key = q/ainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img42.svg"
 ALT="$a \in \setR$">|; 

$key = q/displaystyleOD{ln}{y}(y)=unsur{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.40ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \OD{\ln}{y}(y) = \unsur{y}$">|; 

$key = q/displaystyleOD{x}{y}=unsur{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.40ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \OD{x}{y} = \unsur{y}$">|; 

$key = q/displaystyleOD{y}{x}=exp(x)=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \OD{y}{x} = \exp(x) = y$">|; 

$key = q/displaystyleexpBig[ln(acdotb)Big]=acdotb=expbig[ln(a)big]cdotexpbig[ln(b)big]=expBig[ln(a)+ln(b)Big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \exp\Big[\ln(a \cdot b)\Big] = a \cdot b = \exp\big[\ln(a)\big] \cdot \exp\big[\ln(b)\big] = \exp\Big[\ln(a) + \ln(b)\Big]$">|; 

$key = q/displaystylefrac{(k-1)!}{k!}=unsur{k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.08ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \frac{(k - 1) !}{k !} = \unsur{k}$">|; 

$key = q/displaystylefrac{du}{u}=gammacdottdt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \frac{du}{u} = \gamma \cdot t  dt$">|; 

$key = q/displaystyleint_a^bunsur{x}dx=ln(b)-ln(a)=lnleft[frac{b}{a}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.00ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \int_a^b \unsur{x}  dx = \ln(b) - \ln(a) = \ln\left[\frac{b}{a}\right]$">|; 

$key = q/displaystyleint_{u_0}^{u(s)}frac{du}{u}=int_0^sgammacdottdt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.19ex; vertical-align: -2.41ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle \int_{u_0}^{u(s)} \frac{du}{u} = \int_0^s \gamma \cdot t  dt$">|; 

$key = q/displaystyleln(1)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \ln(1) = 0$">|; 

$key = q/displaystyleln(1+t)=sum_{k=1}^{+infty}frac{(-1)^{1+k}cdot(k-1)!}{k!}t^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \ln(1+t) = \sum_{k=1}^{+\infty} \frac{(-1)^{1+k} \cdot (k - 1) !}{k !}  t^k$">|; 

$key = q/displaystyleln(1+t)=sum_{k=1}^{+infty}frac{(-1)^{1+k}}{k}t^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \ln(1+t) = \sum_{k=1}^{+\infty} \frac{(-1)^{1+k}}{k}  t^k$">|; 

$key = q/displaystyleln(a)+lnleft(unsur{a}right)=lnleft(acdotunsur{a}right)=ln(1)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \ln(a) + \ln\left(\unsur{a}\right) = \ln\left(a \cdot \unsur{a}\right) = \ln(1) = 0$">|; 

$key = q/displaystyleln(acdotb)=ln(a)+ln(b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle \ln(a \cdot b) = \ln(a) + \ln(b)$">|; 

$key = q/displaystyleln(u(s))-ln(u_0)=gammacdotfrac{s^2}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \ln(u(s))-\ln(u_0) = \gamma \cdot \frac{s^2}{2}$">|; 

$key = q/displaystyleln(x)=sum_{k=1}^{+infty}frac{(-1)^{1+k}}{k}(x-1)^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \ln(x) = \sum_{k=1}^{+\infty} \frac{(-1)^{1+k}}{k}  (x - 1)^k$">|; 

$key = q/displaystyleln(y)=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \ln(y) = x$">|; 

$key = q/displaystyleln:setR^+setminus{0}mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \ln : \setR^+ \setminus \{ 0 \} \mapsto \setR$">|; 

$key = q/displaystyleln=exp^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \ln = \exp^{-1}$">|; 

$key = q/displaystylelnleft(frac{a}{b}right)=ln(a)+lnleft(unsur{b}right)=ln(a)-ln(b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \ln\left(\frac{a}{b}\right) = \ln(a) + \ln\left(\unsur{b}\right) = \ln(a) - \ln(b)$">|; 

$key = q/displaystylelnleft(frac{u(s)}{u_0}right)=gammacdotfrac{s^2}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle \ln\left(\frac{u(s)}{u_0}\right) = \gamma \cdot \frac{s^2}{2}$">|; 

$key = q/displaystylelnleft(unsur{a}right)=-ln(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \ln\left(\unsur{a}\right) = - \ln(a)$">|; 

$key = q/displaystylepartial^2u(0)=-unsur{(1+0)^2}=-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \partial^2 u(0) = -\unsur{(1 + 0)^2} = -1$">|; 

$key = q/displaystylepartial^2u(t)=-unsur{(1+t)^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \partial^2 u(t) = -\unsur{(1 + t)^2}$">|; 

$key = q/displaystylepartial^3u(0)=frac{2}{(1+0)^3}=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \partial^3 u(0) = \frac{2}{(1 + 0)^3} = 2$">|; 

$key = q/displaystylepartial^3u(t)=frac{2}{(1+t)^3};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \partial^3 u(t) = \frac{2}{(1 + t)^3}$">|; 

$key = q/displaystylepartial^4u(0)=frac{-6}{(1+0)^4}=-6;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \partial^4 u(0) = \frac{-6}{(1 + 0)^4} = -6$">|; 

$key = q/displaystylepartial^4u(t)=frac{-6}{(1+t)^4};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \partial^4 u(t) = \frac{-6}{(1 + t)^4}$">|; 

$key = q/displaystylepartial^ku(0)=(-1)^{1+k}cdot(k-1)!;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \partial^k u(0) = (-1)^{1+k} \cdot (k - 1) !$">|; 

$key = q/displaystylepartialu(0)=unsur{1+0}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.02ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \partial u(0) = \unsur{1 + 0} = 1$">|; 

$key = q/displaystylepartialu(t)=unsur{1+t};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.02ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \partial u(t) = \unsur{1 + t}$">|; 

$key = q/displaystyleu(0)=ln(1+0)=ln(1)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle u(0) = \ln(1 + 0) = \ln(1) = 0$">|; 

$key = q/displaystyleu(s)=u_0cdotexpleft(gammacdotfrac{s^2}{2}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle u(s) = u_0 \cdot \exp\left( \gamma \cdot \frac{s^2}{2} \right)$">|; 

$key = q/displaystyleu(t)=ln(1+t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle u(t) = \ln(1 + t)$">|; 

$key = q/displaystyleu:setRsetminus{0},xmapsto1slashx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle u : \setR \setminus \{ 0 \},  x \mapsto 1/x$">|; 

$key = q/displaystylex=ln(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle x = \ln(y)$">|; 

$key = q/displaystyley=exp(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle y = \exp(x)$">|; 

$key = q/exp:setRmapstosetR^+setminus{0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\exp : \setR \mapsto \setR^+ \setminus \{ 0 \}$">|; 

$key = q/exp;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\exp$">|; 

$key = q/gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\gamma$">|; 

$key = q/k!=kcdot(k-1)!;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$k ! = k \cdot (k - 1) !$">|; 

$key = q/kge1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img32.svg"
 ALT="$k \ge 1$">|; 

$key = q/kinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img31.svg"
 ALT="$k \in \setN$">|; 

$key = q/ln;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\ln$">|; 

$key = q/setR^+setminus{0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\setR^+ \setminus \{ 0 \}$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$t$">|; 

$key = q/u:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$u : \setR \mapsto \setR$">|; 

$key = q/u_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img52.svg"
 ALT="$u_0$">|; 

$key = q/x,y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$x,y$">|; 

$key = q/x,yinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$x,y \in \setR$">|; 

$key = q/x=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$x = 0$">|; 

$key = q/x=1+t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img37.svg"
 ALT="$x = 1 + t$">|; 

$key = q/xmapsto1slashx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$x \mapsto 1/x$">|; 

$key = q/y=exp(0)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$y = \exp(0) = 1$">|; 

$key = q/y=exp(x)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$y = \exp(x) \strictsuperieur 0$">|; 

$key = q/{eqnarraystar}OD{u}{t}&=&gammacdottcdotuu(0)&=&u_0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.52ex; " SRC="|."$dir".q|img53.svg"
 ALT="\begin{eqnarray*}
\OD{u}{t} &amp;=&amp; \gamma \cdot t \cdot u \ \\\\
u(0) &amp;=&amp; u_0
\end{eqnarray*}">|; 

1;

