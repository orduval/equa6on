# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(A,+,cdot);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$(A,+,\cdot)$">|; 

$key = q/(A,autreaddition,autremultiplication);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$(A,\autreaddition,\autremultiplication)$">|; 

$key = q/(G,opera);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$(G,\opera)$">|; 

$key = q/(K,autreaddition,autremultiplication);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$(K,\autreaddition,\autremultiplication)$">|; 

$key = q/(M,opera);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$(M,\opera)$">|; 

$key = q/(x,y)inAtimesB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$(x,y) \in A \times B$">|; 

$key = q/-b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img41.svg"
 ALT="$-b$">|; 

$key = q/-x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img28.svg"
 ALT="$-x$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img24.svg"
 ALT="$0$">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img26.svg"
 ALT="$1$">|; 

$key = q/1slashx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$1/x$">|; 

$key = q/A,BsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img50.svg"
 ALT="$A,B \subseteq \Omega$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$A$">|; 

$key = q/AsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img59.svg"
 ALT="$A \subseteq \Omega$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img56.svg"
 ALT="$B$">|; 

$key = q/G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$G$">|; 

$key = q/K;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$K$">|; 

$key = q/M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$M$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\Omega$">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img55.svg"
 ALT="$S$">|; 

$key = q/autreaddition;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.76ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\autreaddition$">|; 

$key = q/autremultiplication;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.73ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\autremultiplication$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$b$">|; 

$key = q/b^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img43.svg"
 ALT="$b^{-1}$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\corps$">|; 

$key = q/displaystyleA+B={x+y:(x,y)inAtimesB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle A + B = \{ x + y : (x,y) \in A \times B \}$">|; 

$key = q/displaystyleS=AbigoplusB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.90ex; vertical-align: -1.37ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle S = A \bigoplus B$">|; 

$key = q/displaystylea-b=a+(-b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle a - b = a + (-b)$">|; 

$key = q/displaystylea=0text{ou}b=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle a = 0  \text{ ou }  b = 0$">|; 

$key = q/displaystyleacdotb=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle a \cdot b = 0$">|; 

$key = q/displaystyleaslashb=frac{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle a / b = \frac{a}{b}$">|; 

$key = q/displaystylefrac{a}{b}=acdotb^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \frac{a}{b} = a \cdot b^{-1}$">|; 

$key = q/displaystylelambdacdotA={lambdacdotx:xinA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle \lambda \cdot A = \{ \lambda \cdot x : x \in A \}$">|; 

$key = q/displaystylenoperax=xoperax^inverseoperax=xoperan=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle n \opera x = x \opera x^\inverse \opera x = x \opera n = x$">|; 

$key = q/displaystylex+(-x)=(-x)+x=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle x + (-x) = (-x) + x = 0$">|; 

$key = q/displaystylex+0=0+x=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle x + 0 = 0 + x = x$">|; 

$key = q/displaystylex+y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle x + y$">|; 

$key = q/displaystylex^inverseoperax=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle x^\inverse \opera x = n$">|; 

$key = q/displaystylex^{-k}=(x^{-1})^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle x^{-k} = (x^{-1})^k$">|; 

$key = q/displaystylexcdot1=1cdotx=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle x \cdot 1 = 1 \cdot x = x$">|; 

$key = q/displaystylexcdotx^{-1}=x^{-1}cdotx=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle x \cdot x^{-1} = x^{-1} \cdot x = 1$">|; 

$key = q/displaystylexcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.67ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle x \cdot y$">|; 

$key = q/displaystylexoperax^inverse=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle x \opera x^\inverse = n$">|; 

$key = q/displaystyley=x^inverseoperanoperax=x^inverseoperaxoperax^inverseoperax=yoperay;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle y = x^\inverse \opera n \opera x = x^\inverse \opera x \opera x^\inverse \opera x = y \opera y$">|; 

$key = q/displaystyley=x^inverseoperax;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle y = x^\inverse \opera x$">|; 

$key = q/displaystyley=yoperan=yoperayoperay^inverse=yoperay^inverse=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle y = y \opera n = y \opera y \opera y^\inverse = y \opera y^\inverse = n$">|; 

$key = q/kinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img47.svg"
 ALT="$k \in \setN$">|; 

$key = q/lambdainOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\lambda \in \Omega$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$n$">|; 

$key = q/ninG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img6.svg"
 ALT="$n \in G$">|; 

$key = q/opera;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.73ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\opera$">|; 

$key = q/s=x+y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img54.svg"
 ALT="$s = x + y$">|; 

$key = q/sinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img52.svg"
 ALT="$s \in S$">|; 

$key = q/x,yinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img21.svg"
 ALT="$x,y \in A$">|; 

$key = q/x,yinK;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img36.svg"
 ALT="$x,y \in K$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img14.svg"
 ALT="$x$">|; 

$key = q/x^inverse;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$x^\inverse$">|; 

$key = q/x^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img37.svg"
 ALT="$x^{-1}$">|; 

$key = q/xinG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$x \in G$">|; 

$key = q/{eqnarraystar}x^0&=&1x^k&=&xcdotx^{k-1}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.99ex; " SRC="|."$dir".q|img46.svg"
 ALT="\begin{eqnarray*}
x^0 &amp;=&amp; 1 \\\\
x^k &amp;=&amp; x \cdot x^{k - 1}
\end{eqnarray*}">|; 

1;

