# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.21ex; vertical-align: -1.97ex; " SRC="|."$dir".q|img252.svg"
 ALT="$\displaystyle \lambda_m = \min_{X \in \mathcal{V}_m} \max_{x \in X} \frac{x^\du...
...n \mathcal{W}_m} \min_{x \in Y} \frac{x^\dual \cdot A \cdot x}{x^\dual \cdot x}$">|; 

$key = q/(T,U)=schur(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img162.svg"
 ALT="$(T,U) = \schur(A)$">|; 

$key = q/(lambda_1,...,lambda_{m-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img269.svg"
 ALT="$(\lambda_1,...,\lambda_{m - 1})$">|; 

$key = q/(n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img106.svg"
 ALT="$(n,n)$">|; 

$key = q/(n-1,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$(n - 1, n)$">|; 

$key = q/(u,lambda)in(Esetminus{0})timescorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$(u,\lambda) \in (E \setminus \{0\}) \times \corps$">|; 

$key = q/(u_1,...,u_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$(u_1,...,u_n)$">|; 

$key = q/(u_1,...,u_{m-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img270.svg"
 ALT="$(u_1,...,u_{m - 1})$">|; 

$key = q/(u_2,...,u_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img110.svg"
 ALT="$(u_2, ..., u_n)$">|; 

$key = q/(v_1,...,v_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img218.svg"
 ALT="$(v_1,...,v_m)$">|; 

$key = q/(v_1,...,v_{m-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img240.svg"
 ALT="$(v_1,...,v_{m - 1})$">|; 

$key = q/(v_m,...,v_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img272.svg"
 ALT="$(v_m,...,v_n)$">|; 

$key = q/A(u)=lambdacdotu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$A(u) = \lambda \cdot u$">|; 

$key = q/A(x)=mathcal{A}cdotx=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$A(x) = \mathcal{A} \cdot x = y$">|; 

$key = q/A:EmapstoE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$A : E \mapsto E$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$A$">|; 

$key = q/A=A^dual);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img161.svg"
 ALT="$A = A^\dual)$">|; 

$key = q/A=UcdotLambdacdotU^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img170.svg"
 ALT="$A = U \cdot \Lambda \cdot U^\dual$">|; 

$key = q/A^dual=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$A^\dual = A$">|; 

$key = q/A^{(n-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img118.svg"
 ALT="$A^{(n - 1)}$">|; 

$key = q/A^{(n-k)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img123.svg"
 ALT="$A^{(n - k)}$">|; 

$key = q/A_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img142.svg"
 ALT="$A_k$">|; 

$key = q/A_k=Q_kcdotR_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img144.svg"
 ALT="$A_k = Q_k \cdot R_k$">|; 

$key = q/A_p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img149.svg"
 ALT="$A_p$">|; 

$key = q/A_pcdotQ_p^dual=R_p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.65ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img152.svg"
 ALT="$A_p \cdot Q_p^\dual = R_p$">|; 

$key = q/A_{k+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img146.svg"
 ALT="$A_{k+1}$">|; 

$key = q/Acdotu=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img102.svg"
 ALT="$A \cdot u = 0$">|; 

$key = q/Ainmatrice(corps,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$A \in \matrice(\corps,n,n)$">|; 

$key = q/Ccdota=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img229.svg"
 ALT="$C \cdot a = 0$">|; 

$key = q/Cinmatrice(setC,m-1,m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img226.svg"
 ALT="$C \in \matrice(\setC, m - 1, m)$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$E$">|; 

$key = q/H_k=partial^2R(x_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img263.svg"
 ALT="$H_k = \partial^2 R(x_k)$">|; 

$key = q/Hinmatrice(setR,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$H \in \matrice(\setR,m,n)$">|; 

$key = q/J_k=(partialR(x_k))^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img257.svg"
 ALT="$J_k = (\partial R(x_k))^\dual$">|; 

$key = q/J_k^dualcdotdelta_k=scalaire{J_k}{delta_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.53ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img259.svg"
 ALT="$J_k^\dual \cdot \delta_k = \scalaire{J_k}{\delta_k}$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img159.svg"
 ALT="$N$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\Omega$">|; 

$key = q/Q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img70.svg"
 ALT="$Q$">|; 

$key = q/QR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img139.svg"
 ALT="$Q R$">|; 

$key = q/Q_k^dual=Q_k^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.79ex; vertical-align: -0.74ex; " SRC="|."$dir".q|img145.svg"
 ALT="$Q_k^\dual = Q_k^{-1}$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img134.svg"
 ALT="$R$">|; 

$key = q/R=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$R = 1$">|; 

$key = q/R_NapproxR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img160.svg"
 ALT="$R_N \approx R$">|; 

$key = q/R_p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img155.svg"
 ALT="$R_p$">|; 

$key = q/Rne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$R \ne 0$">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img232.svg"
 ALT="$S$">|; 

$key = q/T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img79.svg"
 ALT="$T$">|; 

$key = q/T=T^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img164.svg"
 ALT="$T = T^\dual$">|; 

$key = q/T^{(n-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img87.svg"
 ALT="$T^{(n-1)}$">|; 

$key = q/Tinmatrice(corps,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$T \in \matrice(\corps,n,n)$">|; 

$key = q/U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img112.svg"
 ALT="$U$">|; 

$key = q/U^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img129.svg"
 ALT="$U^\dual$">|; 

$key = q/U^dualcdotU=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img174.svg"
 ALT="$U^\dual \cdot U = I$">|; 

$key = q/U^{(n-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img119.svg"
 ALT="$U^{(n - 1)}$">|; 

$key = q/U_1^{-1}=U_1^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img114.svg"
 ALT="$U_1^{-1} = U_1^\dual$">|; 

$key = q/V^dualcdotV=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img278.svg"
 ALT="$V^\dual \cdot V = I$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img219.svg"
 ALT="$X$">|; 

$key = q/X=mathcal{P}_minmathcal{V}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img235.svg"
 ALT="$X = \mathcal{P}_m \in \mathcal{V}_m$">|; 

$key = q/Xcapmathcal{Q}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img233.svg"
 ALT="$X \cap \mathcal{Q}_m$">|; 

$key = q/Xinmathcal{D}_{m-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img238.svg"
 ALT="$X \in \mathcal{D}_{m - 1}$">|; 

$key = q/Xinmathcal{V}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img217.svg"
 ALT="$X \in \mathcal{V}_m$">|; 

$key = q/Y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img248.svg"
 ALT="$Y$">|; 

$key = q/Y=X^orthogonalsetminus{0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img239.svg"
 ALT="$Y = X^\orthogonal \setminus \{0\}$">|; 

$key = q/Y=mathcal{Q}_minmathcal{V}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img249.svg"
 ALT="$Y = \mathcal{Q}_m \in \mathcal{V}_m$">|; 

$key = q/Ycapmathcal{P}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img246.svg"
 ALT="$Y \cap \mathcal{P}_m$">|; 

$key = q/Yinmathcal{W}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img237.svg"
 ALT="$Y \in \mathcal{W}_m$">|; 

$key = q/a=[a_1...a_m]^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img228.svg"
 ALT="$a = [a_1  ...  a_m]^\dual$">|; 

$key = q/a_1,...,a_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img189.svg"
 ALT="$a_1,...,a_m$">|; 

$key = q/a_i=scalaire{u_i}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img181.svg"
 ALT="$a_i = \scalaire{u_i}{x}$">|; 

$key = q/alpha_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img264.svg"
 ALT="$\alpha_k$">|; 

$key = q/alpha_kinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img255.svg"
 ALT="$\alpha_k \in \setR$">|; 

$key = q/ane0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img231.svg"
 ALT="$a \ne 0$">|; 

$key = q/boule(0,norme{J_k});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img260.svg"
 ALT="$\boule(0,\norme{J_k})$">|; 

$key = q/combilin{u_1,...,u_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\combilin{u_1,...,u_n}$">|; 

$key = q/combilin{v_m,...,v_n}=mathcal{Q}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img274.svg"
 ALT="$\combilin{v_m,...,v_n} = \mathcal{Q}_m$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\corps$">|; 

$key = q/delta_k=J_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img261.svg"
 ALT="$\delta_k = J_k$">|; 

$key = q/delta_kinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img256.svg"
 ALT="$\delta_k \in \setR^n$">|; 

$key = q/displaystyle(A-lambda_1cdotI)cdotu_1=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle (A - \lambda_1 \cdot I) \cdot u_1 = 0$">|; 

$key = q/displaystyle(A-lambdacdotI)cdotu=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle (A - \lambda \cdot I) \cdot u = 0$">|; 

$key = q/displaystyle(A-lambdacdotidentite)(u)=A(u)-lambdacdotu=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle (A - \lambda \cdot \identite)(u) = A(u) - \lambda \cdot u = 0$">|; 

$key = q/displaystyle(Q_k,R_k)=householder(A_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle (Q_k, R_k) = \householder(A_k)$">|; 

$key = q/displaystyle(R,V)=schur(A^{(n-1)});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\displaystyle (R,V) = \schur(A^{(n - 1)})$">|; 

$key = q/displaystyle(T,U)=schur(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle (T,U) = \schur(A)$">|; 

$key = q/displaystyle(lambda_j-lambda_i)cdotscalaire{u_i}{u_j}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle (\lambda_j - \lambda_i) \cdot \scalaire{u_i}{u_j} = 0$">|; 

$key = q/displaystyle0neu=Q^{-1}cdotv=Q^{-1}cdot0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle 0 \ne u = Q^{-1} \cdot v = Q^{-1} \cdot 0 = 0$">|; 

$key = q/displaystyleA(u)=lambdacdotu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle A(u) = \lambda \cdot u$">|; 

$key = q/displaystyleA(x)=mathcal{A}cdotx=contraction{mathcal{A}}{1}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle A(x) = \mathcal{A} \cdot x = \contraction{ \mathcal{A} }{1}{x}$">|; 

$key = q/displaystyleA=Q_0cdot...Q_pcdotR_pcdotQ_{p-1}^dualcdot...cdotQ_0^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle A = Q_0 \cdot ... Q_p \cdot R_p \cdot Q_{p - 1}^\dual \cdot ... \cdot Q_0^\dual$">|; 

$key = q/displaystyleA=Q_0cdot...cdotQ_pcdotA_pcdotQ_p^dualcdot...cdotQ_0^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle A = Q_0 \cdot ... \cdot Q_p \cdot A_p \cdot Q_p^\dual \cdot ... \cdot Q_0^\dual$">|; 

$key = q/displaystyleA=UcdotLambdacdotU^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img176.svg"
 ALT="$\displaystyle A = U \cdot \Lambda \cdot U^\dual$">|; 

$key = q/displaystyleA=UcdotRcdotU^dualapproxmathcal{U}_NcdotR_Ncdotmathcal{U}_N^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img158.svg"
 ALT="$\displaystyle A = U \cdot R \cdot U^\dual \approx \mathcal{U}_N \cdot R_N \cdot \mathcal{U}_N^\dual$">|; 

$key = q/displaystyleA=UcdotTcdotU^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\displaystyle A = U \cdot T \cdot U^\dual$">|; 

$key = q/displaystyleA=UcdotTcdotU^dual=A^dual=UcdotT^dualcdotU^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img163.svg"
 ALT="$\displaystyle A = U \cdot T \cdot U^\dual = A^\dual = U \cdot T^\dual \cdot U^\dual$">|; 

$key = q/displaystyleA=UcdotTcdotU^dual=UcdotTcdotU^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\displaystyle A = U \cdot T \cdot U^\dual = U \cdot T \cdot U^{-1}$">|; 

$key = q/displaystyleA_0=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle A_0 = A$">|; 

$key = q/displaystyleA_p=Q_p^dualcdot...cdotQ_0^dualcdotAcdotQ_0cdot...cdotQ_p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img150.svg"
 ALT="$\displaystyle A_p = Q_p^\dual \cdot ... \cdot Q_0^\dual \cdot A \cdot Q_0 \cdot ... \cdot Q_p$">|; 

$key = q/displaystyleA_{k+1}=Q_k^dualcdotA_kcdotQ_k=Q_k^dualcdotQ_kcdotR_kcdotQ_k=R_kcdotQ_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\displaystyle A_{k + 1} = Q_k^\dual \cdot A_k \cdot Q_k = Q_k^\dual \cdot Q_k \cdot R_k \cdot Q_k = R_k \cdot Q_k$">|; 

$key = q/displaystyleAcdotU=LambdacdotU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img171.svg"
 ALT="$\displaystyle A \cdot U = \Lambda \cdot U$">|; 

$key = q/displaystyleAcdotu=lambdacdotu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle A \cdot u = \lambda \cdot u$">|; 

$key = q/displaystyleAcdotu_i=lambda_icdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img173.svg"
 ALT="$\displaystyle A \cdot u_i = \lambda_i \cdot u_i$">|; 

$key = q/displaystyleB=QcdotAcdotQ^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle B = Q \cdot A \cdot Q^{-1}$">|; 

$key = q/displaystyleHcdotx=lambdacdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle H \cdot x = \lambda \cdot x$">|; 

$key = q/displaystyleOmega={xincorps^n:norme{x}=sqrt{x^dualcdotx}=R};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.93ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \Omega = \{ x \in \corps^n : \norme{x} = \sqrt{x^\dual \cdot x} = R \}$">|; 

$key = q/displaystyleOmega={xincorps^n:x^dualcdotx=R^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \Omega = \{ x \in \corps^n : x^\dual \cdot x = R^2 \}$">|; 

$key = q/displaystyleQcdotAcdotQ^{-1}cdotv=lambdacdotv;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle Q \cdot A \cdot Q^{-1} \cdot v = \lambda \cdot v$">|; 

$key = q/displaystyleQcdotAcdotu=lambdacdotQcdotu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle Q \cdot A \cdot u = \lambda \cdot Q \cdot u$">|; 

$key = q/displaystyleR(u_m)=lambda_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img194.svg"
 ALT="$\displaystyle R(u_m) = \lambda_m$">|; 

$key = q/displaystyleR(v)=frac{scalaire{v}{A(v)}}{scalaire{v}{v}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle R(v) = \frac{ \scalaire{v}{A(v)} }{ \scalaire{v}{v} }$">|; 

$key = q/displaystyleR(x)=frac{x^dualcdotAcdotx}{x^dualcdotx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.93ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img178.svg"
 ALT="$\displaystyle R(x) = \frac{x^\dual \cdot A \cdot x}{x^\dual \cdot x}$">|; 

$key = q/displaystyleR(x)=sum_{i=1}^mw_icdotlambda_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img190.svg"
 ALT="$\displaystyle R(x) = \sum_{i = 1}^m w_i \cdot \lambda_i$">|; 

$key = q/displaystyleR(x)=sum_{i=1}^nw_icdotlambda_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img186.svg"
 ALT="$\displaystyle R(x) = \sum_{i = 1}^n w_i \cdot \lambda_i$">|; 

$key = q/displaystyleR(x)=sum_{i=m}^nw_icdotlambda_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img206.svg"
 ALT="$\displaystyle R(x) = \sum_{i = m}^n w_i \cdot \lambda_i$">|; 

$key = q/displaystyleR(x)gelambda_msum_{i=1}^mw_i=lambda_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img192.svg"
 ALT="$\displaystyle R(x) \ge \lambda_m \sum_{i = 1}^m w_i = \lambda_m$">|; 

$key = q/displaystyleR(x)lelambda_msum_{i=m}^nw_i=lambda_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img208.svg"
 ALT="$\displaystyle R(x) \le \lambda_m \sum_{i = m}^n w_i = \lambda_m$">|; 

$key = q/displaystyleR(x_{k+1})approxR(x_k)+alpha_kcdotJ_k^dualcdotdelta_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img258.svg"
 ALT="$\displaystyle R(x_{k + 1}) \approx R(x_k) + \alpha_k \cdot J_k^\dual \cdot \delta_k$">|; 

$key = q/displaystyleS={ainsetC^m:Ccdota=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img230.svg"
 ALT="$\displaystyle S = \{ a \in \setC^m : C \cdot a = 0 \}$">|; 

$key = q/displaystyleT=Lambda=diagonale_n(lambda_1,...,lambda_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.00ex; vertical-align: -2.17ex; " SRC="|."$dir".q|img169.svg"
 ALT="$\displaystyle T = \Lambda = \diagonale_n(\lambda_1,...,\lambda_n)$">|; 

$key = q/displaystyleT^{(n-1)}cdotu_i^{(n-1)}=lambda_icdotu_i^{(n-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.13ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle T^{(n-1)} \cdot u_i^{(n-1)} = \lambda_i \cdot u_i^{(n-1)}$">|; 

$key = q/displaystyleTcdotu=[lambda]cdot[mu]=lambdacdot[1]cdot[mu]=lambdacdotu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle T \cdot u = [\lambda] \cdot [\mu] = \lambda \cdot [1] \cdot [\mu] = \lambda \cdot u$">|; 

$key = q/displaystyleU=U_1cdot...cdotU_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\displaystyle U = U_1 \cdot ... \cdot U_n$">|; 

$key = q/displaystyleU^{-1}=U_n^dualcdot...cdotU_1^dual=U^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\displaystyle U^{-1} = U_n^\dual \cdot ... \cdot U_1^\dual = U^\dual$">|; 

$key = q/displaystyleU_1=[u_1u_2...u_n];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle U_1 = [u_1  u_2  ...  u_n]$">|; 

$key = q/displaystyleV=[v_mv_{m+1}...v_n];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img273.svg"
 ALT="$\displaystyle V = [v_m  v_{m + 1}  ...  v_n]$">|; 

$key = q/displaystylea_k=scalaire{u_k}{x}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img203.svg"
 ALT="$\displaystyle a_k = \scalaire{u_k}{x} = 0$">|; 

$key = q/displaystylealpha_k=frac{J_k^dualcdotJ_k}{J_k^dualcdotH_kcdotJ_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.57ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img265.svg"
 ALT="$\displaystyle \alpha_k = \frac{J_k^\dual \cdot J_k}{J_k^\dual \cdot H_k \cdot J_k}$">|; 

$key = q/displaystylealpha_k=frac{partialvarrho(z_k)^dualcdotpartialvarrho(z_k)}{partialvarrho(z_k)^dualcdotpartial^2varrho(z_k)cdotpartialvarrho(z_k)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img283.svg"
 ALT="$\displaystyle \alpha_k = \frac{\partial \varrho(z_k)^\dual \cdot \partial \varr...
...l \varrho(z_k)^\dual \cdot \partial^2 \varrho(z_k) \cdot \partial \varrho(z_k)}$">|; 

$key = q/displaystylecomposante_{ij}C=scalaire{u_i}{v_j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.32ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img227.svg"
 ALT="$\displaystyle \composante_{ij} C = \scalaire{u_i}{v_j}$">|; 

$key = q/displaystylecomposante_{ij}C=scalaire{v_i}{u_j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.32ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img245.svg"
 ALT="$\displaystyle \composante_{ij} C = \scalaire{v_i}{u_j}$">|; 

$key = q/displaystylelagrangien(x,lambda)=x^dualcdotHcdotx+lambdacdot(R^2-x^dualcdotx);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \lagrangien(x,\lambda) = x^\dual \cdot H \cdot x + \lambda \cdot (R^2 - x^\dual \cdot x)$">|; 

$key = q/displaystylelambda=frac{scalaire{u}{A(u)}}{scalaire{u}{u}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \lambda = \frac{ \scalaire{u}{A(u)} }{ \scalaire{u}{u} }$">|; 

$key = q/displaystylelambda=frac{u^dualcdotAcdotu}{u^dualcdotu}=R(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.93ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \lambda = \frac{u^\dual \cdot A \cdot u}{u^\dual \cdot u} = R(u)$">|; 

$key = q/displaystylelambda=scalaire{u}{A(u)}=scalaire{A(u)}{u}=conjuguescalaire{u}{A(u)}=bar{lambda};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.69ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \lambda = \scalaire{u}{A(u)} = \scalaire{A(u)}{u} = \conjugue \scalaire{u}{A(u)} = \bar{\lambda}$">|; 

$key = q/displaystylelambda_1gelambda_2gelambda_3ge...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle \lambda_1 \ge \lambda_2 \ge \lambda_3 \ge ...$">|; 

$key = q/displaystylelambda_1lelambda_2lelambda_3le...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img251.svg"
 ALT="$\displaystyle \lambda_1 \le \lambda_2 \le \lambda_3 \le ...$">|; 

$key = q/displaystylelambda_2gelambda_3ge...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\displaystyle \lambda_2 \ge \lambda_3 \ge ...$">|; 

$key = q/displaystylelambda_m=max_{Xinmathcal{V}_m}min_{xinX}frac{x^dualcdotAcdotx}{x^dualcdotx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.21ex; vertical-align: -1.97ex; " SRC="|."$dir".q|img236.svg"
 ALT="$\displaystyle \lambda_m = \max_{X \in \mathcal{V}_m} \min_{x \in X} \frac{x^\dual \cdot A \cdot x}{x^\dual \cdot x}$">|; 

$key = q/displaystylelambda_m=max_{xinmathcal{Q}_m}frac{x^dualcdotAcdotx}{x^dualcdotx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.21ex; vertical-align: -1.97ex; " SRC="|."$dir".q|img211.svg"
 ALT="$\displaystyle \lambda_m = \max_{x \in \mathcal{Q}_m} \frac{x^\dual \cdot A \cdot x}{x^\dual \cdot x}$">|; 

$key = q/displaystylelambda_m=min_{Yinmathcal{W}_m}max_{xinY}frac{x^dualcdotAcdotx}{x^dualcdotx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.21ex; vertical-align: -1.97ex; " SRC="|."$dir".q|img250.svg"
 ALT="$\displaystyle \lambda_m = \min_{Y \in \mathcal{W}_m} \max_{x \in Y} \frac{x^\dual \cdot A \cdot x}{x^\dual \cdot x}$">|; 

$key = q/displaystylelambda_m=min_{xinmathcal{P}_m}frac{x^dualcdotAcdotx}{x^dualcdotx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.21ex; vertical-align: -1.97ex; " SRC="|."$dir".q|img197.svg"
 ALT="$\displaystyle \lambda_m = \min_{x \in \mathcal{P}_m} \frac{x^\dual \cdot A \cdot x}{x^\dual \cdot x}$">|; 

$key = q/displaystylelambda_m=min_{zinmathcal{P}_m}R(z)leR(x)lemax_{zinY}R(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.79ex; vertical-align: -1.97ex; " SRC="|."$dir".q|img247.svg"
 ALT="$\displaystyle \lambda_m = \min_{z \in \mathcal{P}_m} R(z) \le R(x) \le \max_{z \in Y} R(z)$">|; 

$key = q/displaystylelim_{ptoinfty}mathcal{U}_{p-1}^dual=U^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.79ex; vertical-align: -2.02ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle \lim_{p \to \infty} \mathcal{U}_{p - 1}^\dual = U^\dual$">|; 

$key = q/displaystylemathcal{A}=sum_ilambda_icdotu_iotimesu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \mathcal{A} = \sum_i \lambda_i \cdot u_i \otimes u_i$">|; 

$key = q/displaystylemathcal{A}^{-1}=sum_iunsur{lambda_i}cdotu_iotimesu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle \mathcal{A}^{-1} = \sum_i \unsur{\lambda_i} \cdot u_i \otimes u_i$">|; 

$key = q/displaystylemathcal{D}_m={combilin{v_1,...,v_m}:v_iinsetC^n,scalaire{v_i}{v_j}=delta_{ij}text{pourtout}i,jin{1,...,m}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img215.svg"
 ALT="$\displaystyle \mathcal{D}_m = \{ \combilin{v_1,...,v_m} : v_i \in \setC^n,  \scalaire{v_i}{v_j} = \delta_{ij} \text{ pour tout } i,j \in \{1,...,m\} \}$">|; 

$key = q/displaystylemathcal{U}_p=Q_0cdot...Q_p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\displaystyle \mathcal{U}_p = Q_0 \cdot ... Q_p$">|; 

$key = q/displaystylemin_{zinX}R(z)leR(x)lemax_{zinmathcal{Q}_m}R(z)=lambda_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.79ex; vertical-align: -1.97ex; " SRC="|."$dir".q|img234.svg"
 ALT="$\displaystyle \min_{z \in X} R(z) \le R(x) \le \max_{z \in \mathcal{Q}_m} R(z) = \lambda_m$">|; 

$key = q/displaystylescalaire{u_1}{x}=...=scalaire{u_{m-1}}{x}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img271.svg"
 ALT="$\displaystyle \scalaire{u_1}{x} = ... = \scalaire{u_{m - 1}}{x} = 0$">|; 

$key = q/displaystylescalaire{u_i}{A(u_j)}=lambda_jcdotscalaire{u_i}{u_j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \scalaire{u_i}{A(u_j)} = \lambda_j \cdot \scalaire{u_i}{u_j}$">|; 

$key = q/displaystylescalaire{u_i}{A(u_j)}=lambda_jcdotscalaire{u_i}{u_j}=lambda_jcdotindicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \scalaire{u_i}{A(u_j)} = \lambda_j \cdot \scalaire{u_i}{u_j} = \lambda_j \cdot \indicatrice_{ij}$">|; 

$key = q/displaystylescalaire{u_i}{A(u_j)}=scalaire{A(u_i)}{u_j}=lambda_icdotscalaire{u_i}{u_j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \scalaire{u_i}{A(u_j)} = \scalaire{A(u_i)}{u_j} = \lambda_i \cdot \scalaire{u_i}{u_j}$">|; 

$key = q/displaystylescalaire{u_i}{u_j}=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \scalaire{u_i}{u_j} = \indicatrice_{ij}$">|; 

$key = q/displaystylescalaire{u_i}{x}=sum_{j=1}^mscalaire{u_i}{v_j}cdota_j=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img224.svg"
 ALT="$\displaystyle \scalaire{u_i}{x} = \sum_{j = 1}^m \scalaire{u_i}{v_j} \cdot a_j = 0$">|; 

$key = q/displaystylescalaire{u_i}{x}=unsur{lambda_i}cdotscalaire{u_i}{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle \scalaire{u_i}{x} = \unsur{\lambda_i} \cdot \scalaire{u_i}{y}$">|; 

$key = q/displaystylescalaire{u}{A(u)}=lambdacdotscalaire{u}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \scalaire{u}{A(u)} = \lambda \cdot \scalaire{u}{u}$">|; 

$key = q/displaystylescalaire{v_i}{x}=sum_{j=1}^mscalaire{v_i}{u_j}cdota_j=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img244.svg"
 ALT="$\displaystyle \scalaire{v_i}{x} = \sum_{j = 1}^m \scalaire{v_i}{u_j} \cdot a_j = 0$">|; 

$key = q/displaystylesum_{i=1}^nw_i=frac{sum_{i=1}^nabs{a_i}^2}{sum_{j=1}^nabs{a_j}^2}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img185.svg"
 ALT="$\displaystyle \sum_{i = 1}^n w_i = \frac{\sum_{i = 1}^n \abs{a_i}^2}{\sum_{j = 1}^n \abs{a_j}^2} = 1$">|; 

$key = q/displaystylesum_{i=m}^nw_i=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img207.svg"
 ALT="$\displaystyle \sum_{i = m}^n w_i = 1$">|; 

$key = q/displaystyleu^dualcdotAcdotu=lambdacdotu^dualcdotu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle u^\dual \cdot A \cdot u = \lambda \cdot u^\dual \cdot u$">|; 

$key = q/displaystyleu_i^dualcdotu_j=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img175.svg"
 ALT="$\displaystyle u_i^\dual \cdot u_j = \indicatrice_{ij}$">|; 

$key = q/displaystyleu_minargmax_{xinmathcal{Q}_m}frac{x^dualcdotAcdotx}{x^dualcdotx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.21ex; vertical-align: -1.97ex; " SRC="|."$dir".q|img212.svg"
 ALT="$\displaystyle u_m \in \arg\max_{x \in \mathcal{Q}_m} \frac{x^\dual \cdot A \cdot x}{x^\dual \cdot x}$">|; 

$key = q/displaystyleu_minargmin_{xinmathcal{P}_m}frac{x^dualcdotAcdotx}{x^dualcdotx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.21ex; vertical-align: -1.97ex; " SRC="|."$dir".q|img199.svg"
 ALT="$\displaystyle u_m \in \arg\min_{x \in \mathcal{P}_m} \frac{x^\dual \cdot A \cdot x}{x^\dual \cdot x}$">|; 

$key = q/displaystylevarphi(x)=x^dualcdotHcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \varphi(x) = x^\dual \cdot H \cdot x$">|; 

$key = q/displaystylevarrho(z)=R(Vcdotz)=frac{z^dualcdotV^dualcdotAcdotVcdotz}{z^dualcdotV^dualcdotVcdotz};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.93ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img277.svg"
 ALT="$\displaystyle \varrho(z) = R(V \cdot z) = \frac{z^\dual \cdot V^\dual \cdot A \cdot V \cdot z}{z^\dual \cdot V^\dual \cdot V \cdot z}$">|; 

$key = q/displaystylevarrho(z)=frac{z^dualcdotV^dualcdotAcdotVcdotz}{z^dualcdotz};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.93ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img279.svg"
 ALT="$\displaystyle \varrho(z) = \frac{z^\dual \cdot V^\dual \cdot A \cdot V \cdot z}{z^\dual \cdot z}$">|; 

$key = q/displaystylew_i=frac{abs{a_i}^2}{sum_{j=1}^nabs{a_j}^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.72ex; vertical-align: -2.95ex; " SRC="|."$dir".q|img183.svg"
 ALT="$\displaystyle w_i = \frac{\abs{a_i}^2}{\sum_{j = 1}^n \abs{a_j}^2}$">|; 

$key = q/displaystylex=mathcal{A}^{-1}cdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle x = \mathcal{A}^{-1} \cdot y$">|; 

$key = q/displaystylex=sum_iscalaire{u_i}{x}cdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle x = \sum_i \scalaire{u_i}{x} \cdot u_i$">|; 

$key = q/displaystylex=sum_iscalaire{u_i}{x}cdotu_i=sum_iunsur{lambda_i}cdotscalaire{u_i}{y}cdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle x = \sum_i \scalaire{u_i}{x} \cdot u_i = \sum_i \unsur{\lambda_i} \cdot \scalaire{u_i}{y} \cdot u_i$">|; 

$key = q/displaystylex=sum_{i=1}^na_icdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img180.svg"
 ALT="$\displaystyle x = \sum_{i = 1}^n a_i \cdot u_i$">|; 

$key = q/displaystylex=sum_{i=m}^nz_icdotv_i=Vcdotz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img276.svg"
 ALT="$\displaystyle x = \sum_{i = m}^n z_i \cdot v_i = V \cdot z$">|; 

$key = q/displaystylex=sum_{j=1}^ma_jcdotu_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img242.svg"
 ALT="$\displaystyle x = \sum_{j = 1}^m a_j \cdot u_j$">|; 

$key = q/displaystylex=sum_{j=1}^ma_jcdotv_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img222.svg"
 ALT="$\displaystyle x = \sum_{j = 1}^m a_j \cdot v_j$">|; 

$key = q/displaystylex_{k+1}=x_k+alpha_kcdotJ_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img262.svg"
 ALT="$\displaystyle x_{k + 1} = x_k + \alpha_k \cdot J_k$">|; 

$key = q/displaystylex_{k+1}=x_k+alpha_kcdotdelta_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img254.svg"
 ALT="$\displaystyle x_{k + 1} = x_k + \alpha_k \cdot \delta_k$">|; 

$key = q/displaystyley=sum_iscalaire{u_i}{y}cdotu_i=mathcal{A}cdotx=sum_ilambda_icdotu_icdotscalaire{u_i}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle y = \sum_i \scalaire{u_i}{y} \cdot u_i = \mathcal{A} \cdot x = \sum_i \lambda_i \cdot u_i \cdot \scalaire{u_i}{x}$">|; 

$key = q/displaystylez_{k+1}=z_k+alpha_kcdotpartialvarrho(z_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img282.svg"
 ALT="$\displaystyle z_{k + 1} = z_k + \alpha_k \cdot \partial \varrho(z_k)$">|; 

$key = q/i=j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img168.svg"
 ALT="$i = j$">|; 

$key = q/iin{1,...,m-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img225.svg"
 ALT="$i \in \{1,...,m - 1\}$">|; 

$key = q/iin{1,2,...,n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img96.svg"
 ALT="$i \in \{1,2,...,n-1\}$">|; 

$key = q/istrictsuperieurj;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img166.svg"
 ALT="$i \strictsuperieur j$">|; 

$key = q/k+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img122.svg"
 ALT="$k + 1$">|; 

$key = q/k+1=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img125.svg"
 ALT="$k + 1 = n$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img141.svg"
 ALT="$k$">|; 

$key = q/kin{1,...,m-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img204.svg"
 ALT="$k \in \{1, ..., m - 1\}$">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\lambda$">|; 

$key = q/lambda=R(u)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\lambda = R(u) \ge 0$">|; 

$key = q/lambda=composante_{11}(T);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\lambda = \composante_{11}(T)$">|; 

$key = q/lambda_1,...,lambda_{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\lambda_1,...,\lambda_{n-1}$">|; 

$key = q/lambda_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\lambda_1$">|; 

$key = q/lambda_1gemax{lambda_2,...,lambda_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\lambda_1 \ge \max\{\lambda_2,...,\lambda_n\}$">|; 

$key = q/lambda_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\lambda_i$">|; 

$key = q/lambda_icdotscalaire{u_i}{x}=scalaire{u_i}{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\lambda_i \cdot \scalaire{u_i}{x} = \scalaire{u_i}{y}$">|; 

$key = q/lambda_iinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\lambda_i \in \setR$">|; 

$key = q/lambda_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img195.svg"
 ALT="$\lambda_m$">|; 

$key = q/lambda_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\lambda_n$">|; 

$key = q/lambdaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\lambda \in \corps$">|; 

$key = q/lambdainsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\lambda \in \setC$">|; 

$key = q/lambdainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\lambda \in \setR$">|; 

$key = q/lim_{ktoinfty}R(x_k)=lambda_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img268.svg"
 ALT="$\lim_{k \to \infty} R(x_k) = \lambda_1$">|; 

$key = q/lim_{ktoinfty}x_k=u_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img267.svg"
 ALT="$\lim_{k \to \infty} x_k = u_1$">|; 

$key = q/m-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img205.svg"
 ALT="$m - 1$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img214.svg"
 ALT="$m$">|; 

$key = q/mathcal{P}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img196.svg"
 ALT="$\mathcal{P}_m$">|; 

$key = q/mathcal{Q}_1=setR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img253.svg"
 ALT="$\mathcal{Q}_1 = \setR^n$">|; 

$key = q/mathcal{Q}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img210.svg"
 ALT="$\mathcal{Q}_m$">|; 

$key = q/minsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img213.svg"
 ALT="$m \in \setN$">|; 

$key = q/mu=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\mu = 0$">|; 

$key = q/mu=composante_1(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\mu = \composante_1(u)$">|; 

$key = q/muinsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\mu \in \setC$">|; 

$key = q/n-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img84.svg"
 ALT="$n - 1$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img78.svg"
 ALT="$n$">|; 

$key = q/n=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img80.svg"
 ALT="$n = 1$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img148.svg"
 ALT="$p$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img104.svg"
 ALT="$r$">|; 

$key = q/rlen-1strictinferieurn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img105.svg"
 ALT="$r \le n - 1 \strictinferieur n$">|; 

$key = q/scalaire{u}{u}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\scalaire{u}{u} = 1$">|; 

$key = q/scalaire{z}{x}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img201.svg"
 ALT="$\scalaire{z}{x} = 0$">|; 

$key = q/setR^{n-m+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img280.svg"
 ALT="$\setR^{n - m + 1}$">|; 

$key = q/t_{ij}=composante_{ij}(T)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.95ex; " SRC="|."$dir".q|img165.svg"
 ALT="$t_{ij} = \composante_{ij}(T) = 0$">|; 

$key = q/t_{ji}=conjaccent{t}_{ij}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.73ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img167.svg"
 ALT="$t_{ji} = \conjaccent{t}_{ij} = 0$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img21.svg"
 ALT="$u$">|; 

$key = q/u=Q^{-1}cdotv;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img73.svg"
 ALT="$u = Q^{-1} \cdot v$">|; 

$key = q/u^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img67.svg"
 ALT="$u^\dual$">|; 

$key = q/u_1,...,u_{m-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img223.svg"
 ALT="$u_1,...,u_{m - 1}$">|; 

$key = q/u_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img108.svg"
 ALT="$u_1$">|; 

$key = q/u_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img51.svg"
 ALT="$u_i$">|; 

$key = q/u_i=colonne_iU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img172.svg"
 ALT="$u_i = \colonne_i U$">|; 

$key = q/u_i^{(n-1)}ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.13ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img91.svg"
 ALT="$u_i^{(n-1)} \ne 0$">|; 

$key = q/u_iinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img37.svg"
 ALT="$u_i \in \Omega$">|; 

$key = q/u_j^dualcdotu_i=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img111.svg"
 ALT="$u_j^\dual \cdot u_i = \indicatrice_{ij}$">|; 

$key = q/u_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img198.svg"
 ALT="$u_m$">|; 

$key = q/u_minmathcal{P}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img193.svg"
 ALT="$u_m \in \mathcal{P}_m$">|; 

$key = q/u_minmathcal{Q}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img209.svg"
 ALT="$u_m \in \mathcal{Q}_m$">|; 

$key = q/uinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img33.svg"
 ALT="$u \in E$">|; 

$key = q/uincorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img64.svg"
 ALT="$u \in \corps^n$">|; 

$key = q/une0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$u \ne 0$">|; 

$key = q/v=Qcdotu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img72.svg"
 ALT="$v = Q \cdot u$">|; 

$key = q/v=u_i^{(n-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.13ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img95.svg"
 ALT="$v = u_i^{(n-1)}$">|; 

$key = q/v_1,...,v_{m-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img243.svg"
 ALT="$v_1,...,v_{m - 1}$">|; 

$key = q/varphi:corps^nmapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\varphi : \corps^n \mapsto \corps$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\varphi$">|; 

$key = q/vinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img28.svg"
 ALT="$v \in E$">|; 

$key = q/vincorps^{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img93.svg"
 ALT="$v \in \corps^{n - 1}$">|; 

$key = q/vne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img75.svg"
 ALT="$v \ne 0$">|; 

$key = q/w_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img184.svg"
 ALT="$w_i$">|; 

$key = q/x,yinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img53.svg"
 ALT="$x,y \in E$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img58.svg"
 ALT="$x$">|; 

$key = q/x_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img266.svg"
 ALT="$x_k$">|; 

$key = q/xinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img9.svg"
 ALT="$x \in \Omega$">|; 

$key = q/xinXcapmathcal{Q}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img220.svg"
 ALT="$x \in X \cap \mathcal{Q}_m$">|; 

$key = q/xinYcapmathcal{P}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img241.svg"
 ALT="$x \in Y \cap \mathcal{P}_m$">|; 

$key = q/xincombilin{u_1,...,u_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$x \in \combilin{u_1,...,u_n}$">|; 

$key = q/xincorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img8.svg"
 ALT="$x \in \corps^n$">|; 

$key = q/xinmathcal{P}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img188.svg"
 ALT="$x \in \mathcal{P}_m$">|; 

$key = q/xinmathcal{Q}_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img200.svg"
 ALT="$x \in \mathcal{Q}_m$">|; 

$key = q/xinsetC^nsetminus{0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img179.svg"
 ALT="$x \in \setC^n \setminus \{0\}$">|; 

$key = q/xne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img221.svg"
 ALT="$x \ne 0$">|; 

$key = q/z=[z_m...z_n]^dualinsetR^{n-m+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img275.svg"
 ALT="$z = [z_m  ...  z_n]^\dual \in \setR^{n - m + 1}$">|; 

$key = q/z_0ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img281.svg"
 ALT="$z_0 \ne 0$">|; 

$key = q/zinmathcal{P}_{m-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img202.svg"
 ALT="$z \in \mathcal{P}_{m - 1}$">|; 

$key = q/{Eqts}(T-lambda_ncdotI)cdotu=Matrix{{c}A0Matrix{cdotu=Matrix{{c}Acdotu0Matrix{=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img101.svg"
 ALT="\begin{Eqts}
(T - \lambda_n \cdot I) \cdot u =
\begin{Matrix}{c}
A \ 0
\end{Matrix}\cdot u =
\begin{Matrix}{c}
A \cdot u \ 0
\end{Matrix}= 0
\end{Eqts}">|; 

$key = q/{Eqts}A=Matrix{{cccc}lambda_1-lambda_n&hdots&hdots&hdots0&ddots&hdots&hdots0&0&lambda_{n-1}-lambda_n&hdotsMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.91ex; vertical-align: -4.39ex; " SRC="|."$dir".q|img100.svg"
 ALT="\begin{Eqts}
A =
\begin{Matrix}{cccc}
\lambda_1 - \lambda_n &amp; \hdots &amp; \hdots &amp; ...
...s &amp; \hdots \\\\
0 &amp; 0 &amp; \lambda_{n-1} - \lambda_n &amp; \hdots
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}Matrix{{cc}1&00&V^dualMatrix{cdotU_1^dualcdotAcdotU_1cdotMatrix{{cc}1&00&VMatrix{=Matrix{{cc}lambda_1&hdots0&RMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img136.svg"
 ALT="\begin{Eqts}
\begin{Matrix}{cc}
1 &amp; 0 \\\\
0 &amp; V^\dual
\end{Matrix}\cdot U_1^\dua...
...{Matrix}=
\begin{Matrix}{cc}
\lambda_1 &amp; \hdots \\\\
0 &amp; R
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}T=Matrix{{cc}T^{(n-1)}&z0&lambda_nMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img86.svg"
 ALT="\begin{Eqts}
T =
\begin{Matrix}{cc}
T^{(n-1)} &amp; z \\\\
0 &amp; \lambda_n
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}T^{(n-1)}=Matrix{{ccc}lambda_1&hdots&hdots0&ddots&hdots0&0&lambda_{n-1}Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.91ex; vertical-align: -4.39ex; " SRC="|."$dir".q|img88.svg"
 ALT="\begin{Eqts}
T^{(n-1)} =
\begin{Matrix}{ccc}
\lambda_1 &amp; \hdots &amp; \hdots \\\\
0 &amp; \ddots &amp; \hdots \\\\
0 &amp; 0 &amp; \lambda_{n-1} \\\\
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}U^dualcdotAcdotU=T=Matrix{{ccc}lambda_1&hdots&hdots0&ddots&hdots0&0&lambda_nMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.91ex; vertical-align: -4.39ex; " SRC="|."$dir".q|img127.svg"
 ALT="\begin{Eqts}
U^\dual \cdot A \cdot U = T =
\begin{Matrix}{ccc}
\lambda_1 &amp; \hdots &amp; \hdots \\\\
0 &amp; \ddots &amp; \hdots \\\\
0 &amp; 0 &amp; \lambda_n
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}U_1^dualcdotAcdotU_1=Matrix{{cc}lambda_1&hdots0&A^{(n-1)}Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img117.svg"
 ALT="\begin{Eqts}
U_1^\dual \cdot A \cdot U_1 =
\begin{Matrix}{cc}
\lambda_1 &amp; \hdots \ 0 &amp; A^{(n - 1)}
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}U_2=Matrix{{cc}1&00&U^{(n-1)}Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img120.svg"
 ALT="\begin{Eqts}
U_2 =
\begin{Matrix}{cc}
1 &amp; 0 \\\\
0 &amp; U^{(n - 1)}
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}U_2^dualcdotU_1^dualcdotAcdotU_1cdotU_2=Matrix{{ccc}lambda_1&hdots&hdots0&lambda_2&hdots0&0&A^{(n-2)}Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.62ex; vertical-align: -3.75ex; " SRC="|."$dir".q|img121.svg"
 ALT="\begin{Eqts}
U_2^\dual \cdot U_1^\dual \cdot A \cdot U_1 \cdot U_2 =
\begin{Matr...
...\hdots \\\\
0 &amp; \lambda_2 &amp; \hdots \\\\
0 &amp; 0 &amp; A^{(n - 2)}
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}U_{k+1}=Matrix{{cc}I_k&00&U^{(n-k)}Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img124.svg"
 ALT="\begin{Eqts}
U_{k + 1} =
\begin{Matrix}{cc}
I_k &amp; 0 \\\\
0 &amp; U^{(n-k)}
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}lim_{ptoinfty}mathcal{U}_p=Ulim_{ptoinfty}R_p=R{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.72ex; vertical-align: -3.80ex; " SRC="|."$dir".q|img156.svg"
 ALT="\begin{Eqts}
\lim_{p \to \infty} \mathcal{U}_p = U \\\\
\lim_{p \to \infty} R_p = R
\end{Eqts}">|; 

$key = q/{Eqts}partial_xlagrangien(x,lambda)=2Hcdotx-2lambdacdotx=0partial_lambdalagrangien(x,lambda)=R^2-x^dualcdotx=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img13.svg"
 ALT="\begin{Eqts}
\partial_x \lagrangien(x,\lambda) = 2 H \cdot x - 2 \lambda \cdot x...
...
\partial_\lambda \lagrangien(x,\lambda) = R^2 - x^\dual \cdot x = 0
\end{Eqts}">|; 

$key = q/{Eqts}u=Matrix{{c}vmuMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img92.svg"
 ALT="\begin{Eqts}
u =
\begin{Matrix}{c}
v \ \mu
\end{Matrix}\end{Eqts}">|; 

$key = q/{eqnarraystar}1=sum_{i=1}^nw_i&=&sum_{i=1}^mw_i+sum_{i=m+1}^nw_i&=&sum_{i=1}^mw_i+0&=&sum_{i=1}^mw_i{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 26.82ex; " SRC="|."$dir".q|img191.svg"
 ALT="\begin{eqnarray*}
1 = \sum_{i = 1}^n w_i &amp;=&amp; \sum_{i = 1}^m w_i + \sum_{i = m + 1}^n w_i \\\\
&amp;=&amp; \sum_{i = 1}^m w_i + 0 \\\\
&amp;=&amp; \sum_{i = 1}^m w_i
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}A(x)&=&sum_iscalaire{u_i}{x}cdotA(u_i)&=&sum_ilambda_icdotscalaire{u_i}{x}cdotu_i{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.64ex; " SRC="|."$dir".q|img47.svg"
 ALT="\begin{eqnarray*}
A(x) &amp;=&amp; \sum_i \scalaire{u_i}{x} \cdot A(u_i) \\\\
&amp;=&amp; \sum_i \lambda_i \cdot \scalaire{u_i}{x} \cdot u_i
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}lambda_i&=&composante_{ii}Lambdau_i&=&colonne_iU{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.72ex; " SRC="|."$dir".q|img177.svg"
 ALT="\begin{eqnarray*}
\lambda_i &amp;=&amp; \composante_{ii} \Lambda \\\\
u_i &amp;=&amp; \colonne_i U
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}mathcal{P}_m&=&combilin{u_1,u_2,...,u_m}setminus{0}mathcal{Q}_m&=&mathcal{P}_{m-1}^orthogonalsetminus{0}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.73ex; " SRC="|."$dir".q|img187.svg"
 ALT="\begin{eqnarray*}
\mathcal{P}_m &amp;=&amp; \combilin{u_1,u_2,...,u_m} \setminus \{0\} \...
...mathcal{Q}_m &amp;=&amp; \mathcal{P}_{m - 1}^\orthogonal \setminus \{0\}
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}mathcal{V}_m&=&{Xsetminus{0}:Xinmathcal{D}_m}mathcal{W}_m&=&{X^orthogonalsetminus{0}:Xinmathcal{D}_{m-1}}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img216.svg"
 ALT="\begin{eqnarray*}
\mathcal{V}_m &amp;=&amp; \{ X \setminus \{0\} : X \in \mathcal{D}_m \...
... \{ X^\orthogonal \setminus \{0\} : X \in \mathcal{D}_{m - 1} \}
\end{eqnarray*}">|; 

1;

