# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(m,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$(m,1)$">|; 

$key = q/(m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$(m,n)$">|; 

$key = q/(n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$(n,1)$">|; 

$key = q/(n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$(n,n)$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$A$">|; 

$key = q/Acdot(x_k+s_k)=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$A \cdot (x_k + s_k) = b$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$N$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\Omega$">|; 

$key = q/OmegasubseteqsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\Omega \subseteq \setR^n$">|; 

$key = q/Phi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\Phi$">|; 

$key = q/Phiin{varphi,J,H};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\Phi \in \{\varphi,J,H\}$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$b$">|; 

$key = q/displaystyleAcdots_k=Acdot(x_k+s_k)-Acdotx_k=b-b=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle A \cdot s_k = A \cdot (x_k + s_k) - A \cdot x_k = b - b = 0$">|; 

$key = q/displaystyleAcdotx_k=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle A \cdot x_k = b$">|; 

$key = q/displaystyleH=partial^2varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle H = \partial^2 \varphi$">|; 

$key = q/displaystyleJ=partialvarphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle J = \partial \varphi$">|; 

$key = q/displaystyleOmega={xinsetR^n:Acdotx=b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \Omega = \{ x \in \setR^n : A \cdot x = b \}$">|; 

$key = q/displaystyleOmega={xinsetR^n:omega(x)le0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \Omega = \{ x \in \setR^n : \omega(x) \le 0 \}$">|; 

$key = q/displaystylePhi_k=Phi(x_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \Phi_k = \Phi(x_k)$">|; 

$key = q/displaystyleV=[v_{r+1}...v_n];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle V = [v_{r + 1}  ...  v_n]$">|; 

$key = q/displaystyleV^dualcdotJ_k+V^dualcdotH_kcdotVcdotp_k=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.32ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle V^\dual \cdot J_k + V^\dual \cdot H_k \cdot V \cdot p_k = 0$">|; 

$key = q/displaystylelim_{ktoinfty}mu(k)=argmin_{xinOmega}varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \lim_{k \to \infty} \mu(k) = \arg\min_{x \in \Omega} \varphi(x)$">|; 

$key = q/displaystylemu(k)=argmin_{xinsetR^n}psi_k(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \mu(k) = \arg\min_{x \in \setR^n} \psi_k(x)$">|; 

$key = q/displaystylep_k=-(V^dualcdotH_kcdotV)^{-1}cdotV^dualcdotJ_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle p_k = - (V^\dual \cdot H_k \cdot V)^{-1} \cdot V^\dual \cdot J_k$">|; 

$key = q/displaystylepsi_k(x)=varphi(x)+kcdotvarpi(x)^dualcdotvarpi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \psi_k(x) = \varphi(x) + k \cdot \varpi(x)^\dual \cdot \varpi(x)$">|; 

$key = q/displaystyles_k=sum_{i=r+1}^np_{k,i}cdotv_i=Vcdotp_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.05ex; vertical-align: -3.18ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle s_k = \sum_{i = r + 1}^n p_{k,i} \cdot v_i = V \cdot p_k$">|; 

$key = q/displaystylevarphi_{k+1}approxvarphi_k+J_k^dualcdotVcdotp_k+unsur{2}cdotp_k^dualcdotV^dualcdotH_kcdotVcdotp_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \varphi_{k+1} \approx \varphi_k + J_k^\dual \cdot V \cdot p_k + \unsur{2} \cdot p_k^\dual \cdot V^\dual \cdot H_k \cdot V \cdot p_k$">|; 

$key = q/displaystylevarpi_i(x)=max{omega_i(x),0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \varpi_i(x) = \max\{\omega_i(x),0\}$">|; 

$key = q/displaystylex_Napproxlim_{ktoinfty}x_k=argmin_{xinOmega}varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle x_N \approx \lim_{k \to \infty} x_k = \arg\min_{x \in \Omega} \varphi(x)$">|; 

$key = q/displaystylex_{k+1}=I(x_k)=x_k+p_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle x_{k + 1} = I(x_k) = x_k + p_k$">|; 

$key = q/displaystylex_{k+1}=x_k+Vcdotp_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle x_{k + 1} = x_k + V \cdot p_k$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img21.svg"
 ALT="$k$">|; 

$key = q/kge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img50.svg"
 ALT="$k \ge 0$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\mu$">|; 

$key = q/noyauA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\noyau A$">|; 

$key = q/p_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img37.svg"
 ALT="$p_k$">|; 

$key = q/p_k=[p_{k,r+1}...p_{k,n}]^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img34.svg"
 ALT="$p_k = [p_{k, r + 1} ... p_{k,n}]^\dual$">|; 

$key = q/p_kinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$p_k \in \setR^n$">|; 

$key = q/s_kinnoyauA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img27.svg"
 ALT="$s_k \in \noyau A$">|; 

$key = q/sigma_iinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\sigma_i \in \setR$">|; 

$key = q/u_iinsetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img28.svg"
 ALT="$u_i \in \setR^m$">|; 

$key = q/v_iinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img29.svg"
 ALT="$v_i \in \setR^n$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\varphi$">|; 

$key = q/varpi(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\varpi(x) = 0$">|; 

$key = q/varpi(x)^dualcdotvarpi(x)=sum_ivarpi_i(x)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.70ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\varpi(x)^\dual \cdot \varpi(x) = \sum_i \varpi_i(x)^2$">|; 

$key = q/varpi(x)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\varpi(x) \strictsuperieur 0$">|; 

$key = q/varpi:setR^nmapstosetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\varpi : \setR^n \mapsto \setR^m$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$x$">|; 

$key = q/x_0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img20.svg"
 ALT="$x_0 = 0$">|; 

$key = q/x_0inOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img3.svg"
 ALT="$x_0 \in \Omega$">|; 

$key = q/x_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img22.svg"
 ALT="$x_k$">|; 

$key = q/x_{k+1}=x_k+s_kinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img24.svg"
 ALT="$x_{k + 1} = x_k + s_k \in \Omega$">|; 

$key = q/xinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img46.svg"
 ALT="$x \in \Omega$">|; 

$key = q/xnotinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img48.svg"
 ALT="$x \notin \Omega$">|; 

1;

