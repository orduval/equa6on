# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/A:setRmapstomatrice(setR,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$A : \setR \mapsto \matrice(\setR,m,n)$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$A$">|; 

$key = q/AcdotA^{-1}=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$A \cdot A^{-1} = I$">|; 

$key = q/AcdotB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img35.svg"
 ALT="$A \cdot B$">|; 

$key = q/Ainmatrice(setR,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$A \in \matrice(\setR,m,n)$">|; 

$key = q/Ainmatrice(setR,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$A \in \matrice(\setR,n,n)$">|; 

$key = q/B:setRmapstomatrice(setR,n,p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$B : \setR \mapsto \matrice(\setR,n,p)$">|; 

$key = q/H;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img28.svg"
 ALT="$H$">|; 

$key = q/H^dual=H;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$H^\dual = H$">|; 

$key = q/displaystyle0=dI=d(AcdotA^{-1})=dAcdotA^{-1}+AcdotdA^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle 0 = dI = d(A \cdot A^{-1}) = dA \cdot A^{-1} + A \cdot dA^{-1}$">|; 

$key = q/displaystyled(A^{-1})=-A^{-1}cdotdAcdotA^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle d(A^{-1}) = - A^{-1} \cdot dA \cdot A^{-1}$">|; 

$key = q/displaystyled(AcdotB)=dAcdotB+AcdotdB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle d(A \cdot B) = dA \cdot B + A \cdot dB$">|; 

$key = q/displaystylederiveepartielle{mathcal{A}_i}{x_k}(x)=A_{ik};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.30ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \deriveepartielle{\mathcal{A}_i}{x_k}(x) = A_{ik}$">|; 

$key = q/displaystylederiveepartielle{varphi}{x_k}(x)=u_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.30ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \deriveepartielle{\varphi}{x_k}(x) = u_i$">|; 

$key = q/displaystylederiveepartielle{}{x}(Acdotx)=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \deriveepartielle{}{x}(A \cdot x) = A$">|; 

$key = q/displaystylederiveepartielle{}{x}(u^dualcdotx)=u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \deriveepartielle{}{x}(u^\dual \cdot x) = u$">|; 

$key = q/displaystylederiveepartielle{}{x}(x^dualcdotAcdotx)=(A+A^dual)cdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \deriveepartielle{}{x}(x^\dual \cdot A \cdot x) = (A + A^\dual) \cdot x$">|; 

$key = q/displaystylederiveepartielle{}{x}(x^dualcdotHcdotx)=2cdotHcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \deriveepartielle{}{x}(x^\dual \cdot H \cdot x) = 2 \cdot H \cdot x$">|; 

$key = q/displaystylederiveepartielle{}{x}(x^dualcdotu)=u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \deriveepartielle{}{x}(x^\dual \cdot u) = u$">|; 

$key = q/displaystyledfdxdy{}{x}{x}(x^dualcdotAcdotx)=A+A^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \dfdxdy{}{x}{x}(x^\dual \cdot A \cdot x) = A + A^\dual$">|; 

$key = q/displaystyledfdxdy{}{x}{x}(x^dualcdotHcdotx)=2cdotH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \dfdxdy{}{x}{x}(x^\dual \cdot H \cdot x) = 2 \cdot H$">|; 

$key = q/displaystylemathcal{A}(x)=Acdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \mathcal{A}(x) = A \cdot x$">|; 

$key = q/displaystylemathcal{A}_i(x)=sum_jA_{ij}cdotx_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \mathcal{A}_i(x) = \sum_j A_{ij} \cdot x_j$">|; 

$key = q/displaystylemathcal{Q}(x)=sum_{i,j}x_icdotA_{ij}cdotx_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \mathcal{Q}(x) = \sum_{i,j} x_i \cdot A_{ij} \cdot x_j$">|; 

$key = q/displaystylemathcal{Q}(x)=x^dualcdotAcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \mathcal{Q}(x) = x^\dual \cdot A \cdot x$">|; 

$key = q/displaystylepartial(AcdotB)=partialAcdotB+AcdotpartialB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \partial (A \cdot B) = \partial A \cdot B + A \cdot \partial B$">|; 

$key = q/displaystylepartialsum_{k=1}^nA_{ik}(t)cdotB_{kj}(t)=sum_{k=1}^npartialA_{ik}(t)cdotB_{kj}(t)+sum_{k=1}^nA_{ik}(t)cdotpartialB_{kj}(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \partial \sum_{k = 1}^n A_{ik}(t) \cdot B_{kj}(t) = \sum_{k = 1}^...
...ial A_{ik}(t) \cdot B_{kj}(t) + \sum_{k=1}^n A_{ik}(t) \cdot \partial B_{kj}(t)$">|; 

$key = q/displaystylevarphi(x)=sum_ix_icdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \varphi(x) = \sum_i x_i \cdot u_i$">|; 

$key = q/displaystylevarphi(x)=x^dualcdotu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \varphi(x) = x^\dual \cdot u$">|; 

$key = q/displaystylevartheta(x,y)=sum_{i,j}x_icdotA_{ij}cdoty_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \vartheta(x,y) = \sum_{i,j} x_i \cdot A_{ij} \cdot y_j$">|; 

$key = q/displaystylevartheta(x,y)=x^dualcdotAcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \vartheta(x,y) = x^\dual \cdot A \cdot y$">|; 

$key = q/m=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$m = n$">|; 

$key = q/mathcal{A}:setR^mtimessetR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\mathcal{A} : \setR^m \times \setR^n \mapsto \setR$">|; 

$key = q/mathcal{Q}:setR^mtimessetR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.01ex; vertical-align: -0.32ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\mathcal{Q} : \setR^m \times \setR^n \mapsto \setR$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$t$">|; 

$key = q/u^dualcdotx=x^dualcdotu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$u^\dual \cdot x = x^\dual \cdot u$">|; 

$key = q/uinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$u \in \setR^n$">|; 

$key = q/varphi:setR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\varphi : \setR^n \mapsto \setR$">|; 

$key = q/vartheta:setR^mtimessetR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\vartheta : \setR^m \times \setR^n \mapsto \setR$">|; 

$key = q/{Eqts}deriveepartielle{vartheta}{x_k}(x,y)=sum_jA_{kj}cdoty_jderiveepartielle{vartheta}{y_k}(x,y)=sum_ix_icdotA_{ik}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 17.00ex; vertical-align: -7.95ex; " SRC="|."$dir".q|img18.svg"
 ALT="\begin{Eqts}
\deriveepartielle{\vartheta}{x_k}(x,y) = \sum_j A_{kj} \cdot y_j \ \\\\
\deriveepartielle{\vartheta}{y_k}(x,y) = \sum_i x_i \cdot A_{ik}
\end{Eqts}">|; 

$key = q/{Eqts}deriveepartielle{}{x}(x^dualcdotAcdoty)=Acdotyderiveepartielle{}{y}(x^dualcdotAcdoty)=A^dualcdotx{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.50ex; vertical-align: -6.70ex; " SRC="|."$dir".q|img19.svg"
 ALT="\begin{Eqts}
\deriveepartielle{}{x}(x^\dual \cdot A \cdot y) = A \cdot y \ \\\\
\deriveepartielle{}{y}(x^\dual \cdot A \cdot y) = A^\dual \cdot x
\end{Eqts}">|; 

$key = q/{Eqts}dfdxdy{}{x}{x}(x^dualcdotAcdoty)=0dfdxdy{}{y}{y}(x^dualcdotAcdoty)=0dfdxdy{}{y}{x}(x^dualcdotAcdoty)=A{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 24.74ex; vertical-align: -11.83ex; " SRC="|."$dir".q|img20.svg"
 ALT="\begin{Eqts}
\dfdxdy{}{x}{x}(x^\dual \cdot A \cdot y) = 0 \ \\\\
\dfdxdy{}{y}{y}...
...ot A \cdot y) = 0 \ \\\\
\dfdxdy{}{y}{x}(x^\dual \cdot A \cdot y) = A
\end{Eqts}">|; 

$key = q/{eqnarraystar}deriveepartielle{mathcal{Q}}{x_k}(x)&=&sum_jA_{kj}cdotx_j+sum_ix_icdotA_{ik}&=&sum_i(A_{ki}+A_{ik})cdotx_i{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 17.51ex; " SRC="|."$dir".q|img25.svg"
 ALT="\begin{eqnarray*}
\deriveepartielle{\mathcal{Q}}{x_k}(x) &amp;=&amp; \sum_j A_{kj} \cdot...
...um_i x_i \cdot A_{ik} \\\\
&amp;=&amp; \sum_i (A_{ki} + A_{ik}) \cdot x_i
\end{eqnarray*}">|; 

1;

