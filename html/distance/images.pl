# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(a,b)inAtimesB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$(a,b) \in A \times B$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$A$">|; 

$key = q/AsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img15.svg"
 ALT="$A \subseteq \Omega$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img28.svg"
 ALT="$B$">|; 

$key = q/BsubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img18.svg"
 ALT="$B \subseteq A$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\Omega$">|; 

$key = q/U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img42.svg"
 ALT="$U$">|; 

$key = q/Uneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$U \ne \emptyset$">|; 

$key = q/ainA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img21.svg"
 ALT="$a \in A$">|; 

$key = q/ainU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img45.svg"
 ALT="$a \in U$">|; 

$key = q/b=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$b = a$">|; 

$key = q/binA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img24.svg"
 ALT="$b \in A$">|; 

$key = q/binB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img67.svg"
 ALT="$b \in B$">|; 

$key = q/boule(x,epsilon)subseteqboule(a,delta)subseteqUsubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\boule(x,\epsilon) \subseteq \boule(a,\delta) \subseteq U \subseteq A$">|; 

$key = q/boule[c,r];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\boule[c,r]$">|; 

$key = q/c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$c$">|; 

$key = q/cinC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img64.svg"
 ALT="$c \in C$">|; 

$key = q/cinadhA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img72.svg"
 ALT="$c \in \adh A$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\corps$">|; 

$key = q/d=distance(a,x)strictinferieurdelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$d = \distance(a,x) \strictinferieur \delta$">|; 

$key = q/d=distance(x,OmegasetminusA)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$d = \distance( x , \Omega \setminus A ) \strictsuperieur 0$">|; 

$key = q/delta-dstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\delta - d \strictsuperieur 0$">|; 

$key = q/deltastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\delta \strictsuperieur 0$">|; 

$key = q/displaystyleadhA={xinOmega:distance(x,A)=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \adh A = \{ x \in \Omega : \distance(x,A) = 0 \}$">|; 

$key = q/displaystyleadhadhA=adhA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \adh \adh A = \adh A$">|; 

$key = q/displaystyleadhadhAsubseteqadhA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle \adh \adh A \subseteq \adh A$">|; 

$key = q/displaystyleboule(c,r)={xinOmega:distance(x,c)strictinferieurr};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \boule(c,r) = \{ x \in \Omega : \distance(x,c) \strictinferieur r \}$">|; 

$key = q/displaystyleboule[c,r]={xinOmega:distance(x,c)ler};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \boule[c,r] = \{ x \in \Omega : \distance(x,c) \le r \}$">|; 

$key = q/displaystyledistance(A,B)=inf{distance(a,B):ainA}=inf{distance(A,b):binB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \distance(A,B) = \inf \{ \distance(a,B) : a \in A \} = \inf \{ \distance(A,b) : b \in B \}$">|; 

$key = q/displaystyledistance(A,B)=inf{distance(a,b):(a,b)inAtimesB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \distance(A,B) = \inf \{ \distance(a,b) : (a,b) \in A \times B \}$">|; 

$key = q/displaystyledistance(A,a)=distance(a,A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \distance(A,a) = \distance(a,A)$">|; 

$key = q/displaystyledistance(a,A)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \distance(a,A) = 0$">|; 

$key = q/displaystyledistance(a,A)ledistance(a,B);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \distance(a,A) \le \distance(a,B)$">|; 

$key = q/displaystyledistance(a,y)ledistance(a,x)+distance(x,y)strictinferieurd+epsilon=delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \distance(a,y) \le \distance(a,x) + \distance(x,y) \strictinferieur d + \epsilon = \delta$">|; 

$key = q/displaystyledistance(b,a)leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \distance(b,a) \le \epsilon$">|; 

$key = q/displaystyledistance(c,A)=inf_{ainA}distance(c,a)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \distance(c,A) = \inf_{a \in A} \distance(c,a) = 0$">|; 

$key = q/displaystyledistance(c,a)ledistance(c,b)+distance(b,a)leepsilon+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle \distance(c,a) \le \distance(c,b) + \distance(b,a) \le \epsilon + \epsilon$">|; 

$key = q/displaystyledistance(c,b)leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \distance(c,b) \le \epsilon$">|; 

$key = q/displaystyledistance(x,A)=inf_{ainA}distance(x,a)=inf{distance(x,a):ainA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \distance(x,A) = \inf_{a \in A} \distance(x,a) = \inf \{ \distance(x,a) : a \in A \}$">|; 

$key = q/displaystyledistance(x,x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \distance(x,x) = 0$">|; 

$key = q/displaystyledistance(x,y)=0Rightarrowx=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \distance(x,y) = 0  \Rightarrow  x = y$">|; 

$key = q/displaystyledistance(x,y)=0Rightarrowxequivy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \distance(x,y) = 0  \Rightarrow  x \equiv y$">|; 

$key = q/displaystyledistance(x,y)=distance(y,x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \distance(x,y) = \distance(y,x)$">|; 

$key = q/displaystyledistance(x,y)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \distance(x,y) \ge 0$">|; 

$key = q/displaystyledistance(x,z)ledistance(x,y)+distance(y,z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \distance(x,z) \le \distance(x,y) + \distance(y,z)$">|; 

$key = q/displaystyleinterieur(OmegasetminusA)={xinOmega:distance(x,A)strictsuperieur0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle \interieur (\Omega \setminus A) = \{ x \in \Omega : \distance(x,A) \strictsuperieur 0 \}$">|; 

$key = q/displaystyleinterieurA={xinOmega:distance(x,OmegasetminusA)strictsuperieur0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \interieur A = \{ x \in \Omega : \distance( x , \Omega \setminus A ) \strictsuperieur 0 \}$">|; 

$key = q/displaystylemathcal{B}={boule(c,r):cinOmega,rincorps,rstrictsuperieur0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \mathcal{B} = \{ \boule(c,r) : c \in \Omega,  r \in \corps,  r \strictsuperieur 0 \}$">|; 

$key = q/displaystyletopologie=topologies(mathcal{B},Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \topologie = \topologies(\mathcal{B},\Omega)$">|; 

$key = q/distance(a,a)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\distance(a,a) = 0$">|; 

$key = q/distance(a,b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\distance(a,b)$">|; 

$key = q/distance(a,b)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\distance(a,b) \ge 0$">|; 

$key = q/distance(x,A)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\distance(x,A) = 0$">|; 

$key = q/distance(x,OmegasetminusA)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\distance( x , \Omega \setminus A ) \strictsuperieur 0$">|; 

$key = q/distance(x,c)ler;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\distance(x,c) \le r$">|; 

$key = q/distance:OmegatimesOmegatocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\distance : \Omega \times \Omega \to \corps$">|; 

$key = q/epsilon=delta-d;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\epsilon = \delta - d$">|; 

$key = q/epsilonincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\epsilon \in \corps$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$r$">|; 

$key = q/topologie;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\topologie$">|; 

$key = q/x,y,zinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$x,y,z \in \Omega$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$x$">|; 

$key = q/xinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img14.svg"
 ALT="$x \in \Omega$">|; 

$key = q/xinU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img43.svg"
 ALT="$x \in U$">|; 

$key = q/xinboule(a,delta)subseteqU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$x \in \boule(a,\delta) \subseteq U$">|; 

$key = q/xininterieurA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img41.svg"
 ALT="$x \in \interieur A$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$y$">|; 

$key = q/yinboule(x,epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$y \in \boule(x,\epsilon)$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$z$">|; 

$key = q/zinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img55.svg"
 ALT="$z \in \Omega$">|; 

$key = q/zinboule(z,dslash2)subseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$z \in \boule(z,d/2) \subseteq A$">|; 

$key = q/zininterieurA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img58.svg"
 ALT="$z \in \interieur A$">|; 

$key = q/{Eqts}B=adhAC=adhB=adhadhA{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img63.svg"
 ALT="\begin{Eqts}
B = \adh A \\\\
C = \adh B = \adh \adh A
\end{Eqts}">|; 

1;

