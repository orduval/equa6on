# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img35.svg"
 ALT="$2$">|; 

$key = q/A_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img71.svg"
 ALT="$A_k$">|; 

$key = q/B_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img185.svg"
 ALT="$B_k$">|; 

$key = q/E_kge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img136.svg"
 ALT="$E_k \ge 0$">|; 

$key = q/K;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img126.svg"
 ALT="$K$">|; 

$key = q/KinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img140.svg"
 ALT="$K \in \setN$">|; 

$key = q/M:kmapstoM_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img57.svg"
 ALT="$M : k \mapsto M_k$">|; 

$key = q/M_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img73.svg"
 ALT="$M_k$">|; 

$key = q/M_{k+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img83.svg"
 ALT="$M_{k+1}$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img37.svg"
 ALT="$a$">|; 

$key = q/alpha_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\alpha_k$">|; 

$key = q/displaystyle-alpha_k=(2-alpha_k)-2strictinferieurx_k^2-2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\displaystyle -\alpha_k = (2 - \alpha_k) - 2 \strictinferieur x_k^2 - 2$">|; 

$key = q/displaystyle0lerle2-1=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle 0 \le r \le 2 - 1 = 1$">|; 

$key = q/displaystyle0strictinferieurepsilonstrictinferieurdelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle 0 \strictinferieur \epsilon \strictinferieur \delta$">|; 

$key = q/displaystyle10M_kinaccolades{pinsetN:frac{p^2}{n_{k+1}^2}le2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.99ex; vertical-align: -2.50ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle 10  M_k \in \accolades{p \in \setN : \frac{p^2}{n_{k+1}^2} \le 2}$">|; 

$key = q/displaystyle10m_kinaccolades{pinsetN:frac{p^2}{n_{k+1}^2}ge2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.99ex; vertical-align: -2.50ex; " SRC="|."$dir".q|img194.svg"
 ALT="$\displaystyle 10  m_k \in \accolades{p \in \setN : \frac{p^2}{n_{k+1}^2} \ge 2}$">|; 

$key = q/displaystyle1=x_0lex_1le...lex_kle...le2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img275.svg"
 ALT="$\displaystyle 1 = x_0 \le x_1 \le ... \le x_k \le ... \le 2$">|; 

$key = q/displaystyle1=x_0lex_k=frac{M_k}{n_k}lefrac{M_k+1}{n_k}=frac{m_k}{n_k}=y_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.28ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img190.svg"
 ALT="$\displaystyle 1 = x_0 \le x_k = \frac{M_k}{n_k} \le \frac{M_k + 1}{n_k} = \frac{m_k}{n_k} = y_k$">|; 

$key = q/displaystyle1len_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle 1 \le n_k$">|; 

$key = q/displaystyle2-alpha_istrictinferieurx_i^2le2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle 2 - \alpha_i \strictinferieur x_i^2 \le 2$">|; 

$key = q/displaystyle2-alpha_jstrictinferieurx_j^2le2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\displaystyle 2 - \alpha_j \strictinferieur x_j^2 \le 2$">|; 

$key = q/displaystyle2-alpha_kstrictinferieurx_k^2le2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle 2 - \alpha_k \strictinferieur x_k^2 \le 2$">|; 

$key = q/displaystyle2=y_0gey_1ge...gey_kge...ge1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img276.svg"
 ALT="$\displaystyle 2 = y_0 \ge y_1 \ge ... \ge y_k \ge ... \ge 1$">|; 

$key = q/displaystyle2lefrac{p^2}{n_k^2}le2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img175.svg"
 ALT="$\displaystyle 2 \le \frac{p^2}{n_k^2} \le 2$">|; 

$key = q/displaystyle2ley_i^2strictinferieur2+gamma_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img215.svg"
 ALT="$\displaystyle 2 \le y_i^2 \strictinferieur 2 + \gamma_i$">|; 

$key = q/displaystyle2ley_j^2strictinferieur2+gamma_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img216.svg"
 ALT="$\displaystyle 2 \le y_j^2 \strictinferieur 2 + \gamma_j$">|; 

$key = q/displaystyle2ley_k^2strictinferieur2+gamma_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img213.svg"
 ALT="$\displaystyle 2 \le y_k^2 \strictinferieur 2 + \gamma_k$">|; 

$key = q/displaystyleA_k=accolades{pinsetN:frac{p^2}{n_k^2}le2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle A_k = \accolades{p \in \setN : \frac{p^2}{n_k^2} \le 2}$">|; 

$key = q/displaystyleA_k={0,1,...,M_k-1,M_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img184.svg"
 ALT="$\displaystyle A_k = \{ 0, 1, ..., M_k - 1, M_k \}$">|; 

$key = q/displaystyleA_kcapB_k=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img176.svg"
 ALT="$\displaystyle A_k \cap B_k = \emptyset$">|; 

$key = q/displaystyleA_kcupB_k=setN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img173.svg"
 ALT="$\displaystyle A_k \cup B_k = \setN$">|; 

$key = q/displaystyleA_ksubseteq{0,1,...,2n_k-1,2n_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle A_k \subseteq \{ 0, 1, ..., 2  n_k - 1, 2  n_k \}$">|; 

$key = q/displaystyleB_k=accolades{pinsetN:frac{p^2}{n_k^2}ge2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img165.svg"
 ALT="$\displaystyle B_k = \accolades{p \in \setN : \frac{p^2}{n_k^2} \ge 2}$">|; 

$key = q/displaystyleB_k=setNsetminusA_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img178.svg"
 ALT="$\displaystyle B_k = \setN \setminus A_k$">|; 

$key = q/displaystyleB_k=setNsetminusA_k={M_k+1,M_k+2,...}={pinsetN:pgeM_k+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img186.svg"
 ALT="$\displaystyle B_k = \setN \setminus A_k = \{ M_k + 1, M_k + 2, ... \} = \{ p \in \setN : p \ge M_k + 1 \}$">|; 

$key = q/displaystyleDelta_k=frac{2x_k}{n_k}+unsur{n_k^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.48ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle \Delta_k = \frac{2  x_k}{n_k} + \unsur{n_k^2}$">|; 

$key = q/displaystyleDelta_klefrac{2cdot2+1}{n_k}=frac{5}{n_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle \Delta_k \le \frac{2 \cdot 2 + 1}{n_k} = \frac{5}{n_k}$">|; 

$key = q/displaystyleDelta_klefrac{2x_k}{n_k}+unsur{n_k}=frac{2x_k+1}{n_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\displaystyle \Delta_k \le \frac{2  x_k}{n_k} + \unsur{n_k} = \frac{2  x_k + 1}{n_k}$">|; 

$key = q/displaystyleE_k=2-x_k^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle E_k = 2 - x_k^2$">|; 

$key = q/displaystyleE_k=2-x_k^2strictinferieuralpha_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle E_k = 2 - x_k^2 \strictinferieur \alpha_k$">|; 

$key = q/displaystyleM_k=maxA_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img183.svg"
 ALT="$\displaystyle M_k = \max A_k$">|; 

$key = q/displaystyleM_k=maxA_k=supA_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle M_k = \max A_k = \sup A_k$">|; 

$key = q/displaystyleM_k=supaccolades{pinsetN:frac{p^2}{n_k^2}le2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle M_k = \sup \accolades{p \in \setN : \frac{p^2}{n_k^2} \le 2}$">|; 

$key = q/displaystyleM_kle2n_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle M_k \le 2  n_k$">|; 

$key = q/displaystyleM_{k+1}ge10M_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle M_{k+1} \ge 10  M_k$">|; 

$key = q/displaystyleX=accolades{x_k^2:kinsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle X = \accolades{x_k^2 : k \in \setN}$">|; 

$key = q/displaystyleXle2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img144.svg"
 ALT="$\displaystyle X \le 2$">|; 

$key = q/displaystyleY=accolades{Y_k^2:kinsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img245.svg"
 ALT="$\displaystyle Y = \accolades{Y_k^2 : k \in \setN}$">|; 

$key = q/displaystyleYge2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img246.svg"
 ALT="$\displaystyle Y \ge 2$">|; 

$key = q/displaystylea=2n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle a = 2  n$">|; 

$key = q/displaystylea=idiventierek;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle a = i \diventiere k$">|; 

$key = q/displaystylea^2=2b^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle a^2 = 2  b^2$">|; 

$key = q/displaystylea^2=4n^2=2b^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle a^2 = 4  n^2 = 2  b^2$">|; 

$key = q/displaystylea^2diventiere2=b^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.34ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle a^2 \diventiere 2 = b^2$">|; 

$key = q/displaystylea^2modulo2=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle a^2 \modulo 2 = 0$">|; 

$key = q/displaystyleabs{E_k-0}=abs{E_k}=E_klealpha_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle \abs{E_k - 0} = \abs{E_k} = E_k \le \alpha_k$">|; 

$key = q/displaystyleabs{alpha_k}=abs{alpha_k-0}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\displaystyle \abs{\alpha_k} = \abs{\alpha_k - 0} \le \epsilon$">|; 

$key = q/displaystyleabs{e_k-0}=abs{e_k}=e_klegamma_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img240.svg"
 ALT="$\displaystyle \abs{e_k - 0} = \abs{e_k} = e_k \le \gamma_k$">|; 

$key = q/displaystyleabs{gamma_k}=abs{gamma_k-0}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img234.svg"
 ALT="$\displaystyle \abs{\gamma_k} = \abs{\gamma_k - 0} \le \epsilon$">|; 

$key = q/displaystyleabs{x_i-x_j}=x_i-x_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\displaystyle \abs{x_i - x_j} = x_i - x_j$">|; 

$key = q/displaystyleabs{x_i-x_j}leunsur{2}abs{x_i^2-x_j^2}lefrac{alpha_j}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle \abs{x_i - x_j} \le \unsur{2}  \abs{x_i^2 - x_j^2} \le \frac{\alpha_j}{2}$">|; 

$key = q/displaystyleabs{x_i-x_j}leunsur{2}max{alpha_i,alpha_j}lealpha_Kleepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\displaystyle \abs{x_i - x_j} \le \unsur{2}  \max \{ \alpha_i, \alpha_j \} \le \alpha_K \le \epsilon$">|; 

$key = q/displaystyleabs{x_i^2-x_j^2}=x_i^2-x_j^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.03ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\displaystyle \abs{x_i^2 - x_j^2} = x_i^2 - x_j^2$">|; 

$key = q/displaystyleabs{x_i^2-x_j^2}=x_i^2-x_j^2lealpha_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.03ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle \abs{x_i^2 - x_j^2} = x_i^2 - x_j^2 \le \alpha_j$">|; 

$key = q/displaystyleabs{x_k^2-2}=2-x_k^2leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle \abs{x_k^2 - 2} = 2 - x_k^2 \le \epsilon$">|; 

$key = q/displaystyleabs{y_i-y_j}=y_i-y_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img230.svg"
 ALT="$\displaystyle \abs{y_i - y_j} = y_i - y_j$">|; 

$key = q/displaystyleabs{y_i-y_j}leunsur{2}abs{y_i^2-y_j^2}lefrac{gamma_i}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img232.svg"
 ALT="$\displaystyle \abs{y_i - y_j} \le \unsur{2}  \abs{y_i^2 - y_j^2} \le \frac{\gamma_i}{2}$">|; 

$key = q/displaystyleabs{y_i-y_j}leunsur{2}max{gamma_i,gamma_j}legamma_Kleepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img236.svg"
 ALT="$\displaystyle \abs{y_i - y_j} \le \unsur{2}  \max \{ \gamma_i, \gamma_j \} \le \gamma_K \le \epsilon$">|; 

$key = q/displaystyleabs{y_i^2-y_j^2}=y_i^2-y_j^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.03ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img231.svg"
 ALT="$\displaystyle \abs{y_i^2 - y_j^2} = y_i^2 - y_j^2$">|; 

$key = q/displaystyleabs{y_i^2-y_j^2}=y_i^2-y_j^2legamma_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.03ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img223.svg"
 ALT="$\displaystyle \abs{y_i^2 - y_j^2} = y_i^2 - y_j^2 \le \gamma_i$">|; 

$key = q/displaystyleabs{y_k-x_k}=y_k-x_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img260.svg"
 ALT="$\displaystyle \abs{y_k - x_k} = y_k - x_k$">|; 

$key = q/displaystyleabs{y_k-x_k}lefrac{abs{y_k^2-x_k^2}}{2}lefrac{varpi_k}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img271.svg"
 ALT="$\displaystyle \abs{y_k - x_k} \le \frac{\abs{y_k^2 - x_k^2}}{2} \le \frac{\varpi_k}{2}$">|; 

$key = q/displaystyleabs{y_k^2-2}=y_k^2-2leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img253.svg"
 ALT="$\displaystyle \abs{y_k^2 - 2} = y_k^2 - 2 \le \epsilon$">|; 

$key = q/displaystyleabs{y_k^2-x_k^2}=y_k^2-x_k^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img262.svg"
 ALT="$\displaystyle \abs{y_k^2 - x_k^2} = y_k^2 - x_k^2$">|; 

$key = q/displaystyleabs{y_k^2-x_k^2}levarpi_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img267.svg"
 ALT="$\displaystyle \abs{y_k^2 - x_k^2} \le \varpi_k$">|; 

$key = q/displaystylealpha_k=frac{5}{10^k}lefrac{5}{10^K}=alpha_K;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\displaystyle \alpha_k = \frac{5}{10^k} \le \frac{5}{10^K} = \alpha_K$">|; 

$key = q/displaystylealpha_k=frac{5}{n_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle \alpha_k = \frac{5}{n_k}$">|; 

$key = q/displaystyleamodulo2=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle a \modulo 2 = 0$">|; 

$key = q/displaystyleb=2p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle b = 2  p$">|; 

$key = q/displaystyleb=jdiventierek;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle b = j \diventiere k$">|; 

$key = q/displaystyleb^2=2n^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle b^2 = 2  n^2$">|; 

$key = q/displaystyleb^2diventiere2=n^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.34ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle b^2 \diventiere 2 = n^2$">|; 

$key = q/displaystyleb^2modulo2=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle b^2 \modulo 2 = 0$">|; 

$key = q/displaystylebmodulo2=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle b \modulo 2 = 0$">|; 

$key = q/displaystyledelta=2-vstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img149.svg"
 ALT="$\displaystyle \delta = 2 - v \strictsuperieur 0$">|; 

$key = q/displaystyledelta=v-2strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img251.svg"
 ALT="$\displaystyle \delta = v - 2 \strictsuperieur 0$">|; 

$key = q/displaystyledelta_k=frac{2y_k}{n_k}-unsur{n_k^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.48ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img206.svg"
 ALT="$\displaystyle \delta_k = \frac{2  y_k}{n_k} - \unsur{n_k^2}$">|; 

$key = q/displaystyledelta_klefrac{2cdot2}{n_k}=frac{4}{n_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img210.svg"
 ALT="$\displaystyle \delta_k \le \frac{2 \cdot 2}{n_k} = \frac{4}{n_k}$">|; 

$key = q/displaystyledelta_klefrac{2y_k}{n_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img208.svg"
 ALT="$\displaystyle \delta_k \le \frac{2  y_k}{n_k}$">|; 

$key = q/displaystyledistance(x_k^2,2)=abs{x_k^2-2}=abs{E_k}=abs{E_k-0}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\displaystyle \distance(x_k^2,2) = \abs{x_k^2 - 2} = \abs{E_k} = \abs{E_k - 0} \le \epsilon$">|; 

$key = q/displaystyledistance(y_k^2,2)=abs{y_k^2-2}=abs{e_k}=abs{e_k-0}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img243.svg"
 ALT="$\displaystyle \distance(y_k^2,2) = \abs{y_k^2 - 2} = \abs{e_k} = \abs{e_k - 0} \le \epsilon$">|; 

$key = q/displaystylee_k=y_k^2-2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img161.svg"
 ALT="$\displaystyle e_k = y_k^2 - 2$">|; 

$key = q/displaystylee_k=y_k^2-2strictinferieurgamma_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img237.svg"
 ALT="$\displaystyle e_k = y_k^2 - 2 \strictinferieur \gamma_k$">|; 

$key = q/displaystylefrac{(10M_k)^2}{n_{k+1}^2}=frac{(10M_k)^2}{(10n_k)^2}=frac{100M_k^2}{100n_k^2}=frac{M_k^2}{n_k^2}le2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.99ex; vertical-align: -2.50ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle \frac{(10  M_k)^2}{n_{k+1}^2} = \frac{(10  M_k)^2}{(10  n_k)^2} = \frac{100  M_k^2}{100  n_k^2} = \frac{M_k^2}{n_k^2} \le 2$">|; 

$key = q/displaystylefrac{(10m_k)^2}{n_{k+1}^2}=frac{(10m_k)^2}{(10n_k)^2}=frac{100m_k^2}{100n_k^2}=frac{m_k^2}{n_k^2}ge2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.99ex; vertical-align: -2.50ex; " SRC="|."$dir".q|img193.svg"
 ALT="$\displaystyle \frac{(10  m_k)^2}{n_{k+1}^2} = \frac{(10  m_k)^2}{(10  n_k)^2} = \frac{100  m_k^2}{100  n_k^2} = \frac{m_k^2}{n_k^2} \ge 2$">|; 

$key = q/displaystylefrac{M_k^2}{n_k^2}le2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle \frac{M_k^2}{n_k^2} \le 2$">|; 

$key = q/displaystylefrac{i^2}{j^2}=0ne2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.63ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \frac{i^2}{j^2} = 0 \ne 2$">|; 

$key = q/displaystylefrac{m_k^2}{n_k^2}ge2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img192.svg"
 ALT="$\displaystyle \frac{m_k^2}{n_k^2} \ge 2$">|; 

$key = q/displaystylefrac{p^2}{n_k^2}=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img168.svg"
 ALT="$\displaystyle \frac{p^2}{n_k^2} = 2$">|; 

$key = q/displaystylefrac{p^2}{n_k^2}strictinferieur2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img169.svg"
 ALT="$\displaystyle \frac{p^2}{n_k^2} \strictinferieur 2$">|; 

$key = q/displaystylefrac{p^2}{n_k^2}strictsuperieur2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img171.svg"
 ALT="$\displaystyle \frac{p^2}{n_k^2} \strictsuperieur 2$">|; 

$key = q/displaystylefrac{p^2}{n_k^2}strictsuperieurfrac{2^2n_k^2}{n_k^2}=frac{4n_k^2}{n_k^2}=4strictsuperieur2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \frac{p^2}{n_k^2} \strictsuperieur \frac{2^2  n_k^2}{n_k^2} = \frac{4  n_k^2}{n_k^2} = 4 \strictsuperieur 2$">|; 

$key = q/displaystylefrac{u^2}{n_k^2}lefrac{p^2}{n_k^2}le2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img181.svg"
 ALT="$\displaystyle \frac{u^2}{n_k^2} \le \frac{p^2}{n_k^2} \le 2$">|; 

$key = q/displaystylegamma_k=frac{4}{10^k}lefrac{4}{10^K}=gamma_K;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img235.svg"
 ALT="$\displaystyle \gamma_k = \frac{4}{10^k} \le \frac{4}{10^K} = \gamma_K$">|; 

$key = q/displaystylegamma_k=frac{4}{n_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img212.svg"
 ALT="$\displaystyle \gamma_k = \frac{4}{n_k}$">|; 

$key = q/displaystylei=2ncdotk=ncdot(2k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle i = 2  n \cdot k = n \cdot (2  k)$">|; 

$key = q/displaystylei=acdotk;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle i = a \cdot k$">|; 

$key = q/displaystyleimodulot=jmodulot=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle i \modulo t = j \modulo t = 0$">|; 

$key = q/displaystyleinfaccolades{y_k^2:kinsetN}=max{uinsetQ:ule2}=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img257.svg"
 ALT="$\displaystyle \inf \accolades{y_k^2 : k \in \setN} = \max \{ u \in \setQ : u \le 2 \} = 2$">|; 

$key = q/displaystylej=2pcdotk=pcdot(2k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle j = 2  p \cdot k = p \cdot (2  k)$">|; 

$key = q/displaystylej=bcdotk;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle j = b \cdot k$">|; 

$key = q/displaystylek=pgcd(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle k = \pgcd(i,j)$">|; 

$key = q/displaystylelim_{ktoinfty}(x_k-y_k)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.62ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img274.svg"
 ALT="$\displaystyle \lim_{k \to \infty} (x_k - y_k) = 0$">|; 

$key = q/displaystylelim_{ktoinfty}E_k=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.49ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle \lim_{k \to \infty} E_k = 0$">|; 

$key = q/displaystylelim_{ktoinfty}abs{y_k-x_k}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.62ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img273.svg"
 ALT="$\displaystyle \lim_{k \to \infty} \abs{y_k - x_k} = 0$">|; 

$key = q/displaystylelim_{ktoinfty}alpha_k=lim_{ktoinfty}frac{5}{n_k}=lim_{ktoinfty}frac{5}{10^k}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\displaystyle \lim_{k \to \infty} \alpha_k = \lim_{k \to \infty} \frac{5}{n_k} = \lim_{k \to \infty} \frac{5}{10^k} = 0$">|; 

$key = q/displaystylelim_{ktoinfty}e_k=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.49ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img242.svg"
 ALT="$\displaystyle \lim_{k \to \infty} e_k = 0$">|; 

$key = q/displaystylelim_{ktoinfty}gamma_k=lim_{ktoinfty}frac{4}{n_k}=lim_{ktoinfty}frac{4}{10^k}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img233.svg"
 ALT="$\displaystyle \lim_{k \to \infty} \gamma_k = \lim_{k \to \infty} \frac{4}{n_k} = \lim_{k \to \infty} \frac{4}{10^k} = 0$">|; 

$key = q/displaystylelim_{ktoinfty}varpi_k=lim_{ktoinfty}frac{9}{n_k}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img272.svg"
 ALT="$\displaystyle \lim_{k \to \infty} \varpi_k = \lim_{k \to \infty} \frac{9}{n_k} = 0$">|; 

$key = q/displaystylelim_{ktoinfty}x_k^2=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.83ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle \lim_{k \to \infty} x_k^2 = 2$">|; 

$key = q/displaystylelim_{ktoinfty}x_k^2=lim_{ktoinfty}y_k^2=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.83ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img277.svg"
 ALT="$\displaystyle \lim_{k \to \infty} x_k^2 = \lim_{k \to \infty} y_k^2 = 2$">|; 

$key = q/displaystylelim_{ktoinfty}y_k^2=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.83ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img244.svg"
 ALT="$\displaystyle \lim_{k \to \infty} y_k^2 = 2$">|; 

$key = q/displaystylem=2n+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle m = 2  n + 1$">|; 

$key = q/displaystylem^2=(2n+1)^2=4n^2+4n+1=4(n^2+n)+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle m^2 = (2  n + 1)^2 = 4  n^2 + 4  n + 1 = 4  (n^2 + n) + 1$">|; 

$key = q/displaystylem^2diventiere2=2(n^2+n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle m^2 \diventiere 2 = 2  (n^2 + n)$">|; 

$key = q/displaystylem^2modulo2=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle m^2 \modulo 2 = 0$">|; 

$key = q/displaystylem^2modulo2=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle m^2 \modulo 2 = 1$">|; 

$key = q/displaystylem_k=infB_k=minB_k=M_k+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img188.svg"
 ALT="$\displaystyle m_k = \inf B_k = \min B_k = M_k + 1$">|; 

$key = q/displaystylem_k=infaccolades{pinsetN:frac{p^2}{n_k^2}ge2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img159.svg"
 ALT="$\displaystyle m_k = \inf \accolades{p \in \setN : \frac{p^2}{n_k^2} \ge 2}$">|; 

$key = q/displaystylem_{k+1}le10m_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.19ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img196.svg"
 ALT="$\displaystyle m_{k+1} \le 10  m_k$">|; 

$key = q/displaystylemajorX={uinsetQ:uge2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle \major X = \{ u \in \setQ : u \ge 2 \}$">|; 

$key = q/displaystylemaxA_klemax{0,1,...,2n_k}=2n_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \max A_k \le \max \{ 0, 1, ..., 2  n_k \} = 2  n_k$">|; 

$key = q/displaystyleminorY={uinsetQ:ule2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img256.svg"
 ALT="$\displaystyle \minor Y = \{ u \in \setQ : u \le 2 \}$">|; 

$key = q/displaystylemmodulo2=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle m \modulo 2 = 0$">|; 

$key = q/displaystylen=adiventiere2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle n = a \diventiere 2$">|; 

$key = q/displaystylen=mdiventiere2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle n = m \diventiere 2$">|; 

$key = q/displaystylen_0=1qquadM_0=1qquadx_0=1qquadE_0=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle n_0 = 1 \qquad M_0 = 1 \qquad x_0 = 1 \qquad E_0 = 1$">|; 

$key = q/displaystylen_0=1qquadm_0=2qquady_0=2qquade_0=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img162.svg"
 ALT="$\displaystyle n_0 = 1 \qquad m_0 = 2 \qquad y_0 = 2 \qquad e_0 = 2$">|; 

$key = q/displaystylen_1=10qquadM_1=14qquadx_1=1,4qquadE_1=0,04;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle n_1 = 10 \qquad M_1 = 14 \qquad x_1 = 1,4 \qquad E_1 = 0,04$">|; 

$key = q/displaystylen_1=10qquadm_1=15qquady_1=1,5qquade_1=0,25;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img163.svg"
 ALT="$\displaystyle n_1 = 10 \qquad m_1 = 15 \qquad y_1 = 1,5 \qquad e_1 = 0,25$">|; 

$key = q/displaystylen_2=100qquadM_2=141qquadx_2=1,41qquadE_2=0,0119;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle n_2 = 100 \qquad M_2 = 141 \qquad x_2 = 1,41 \qquad E_2 = 0,0119$">|; 

$key = q/displaystylen_2=100qquadm_2=142qquady_2=1,42qquade_2=0,0164;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img164.svg"
 ALT="$\displaystyle n_2 = 100 \qquad m_2 = 142 \qquad y_2 = 1,42 \qquad e_2 = 0,0164$">|; 

$key = q/displaystylen_k=10^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.57ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle n_k = 10^k$">|; 

$key = q/displaystyleparentheses{frac{M_k+1}{n_k}}^2=parentheses{x_k+unsur{n_k}}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.13ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle \parentheses{\frac{M_k + 1}{n_k}}^2 = \parentheses{x_k + \unsur{n_k}}^2$">|; 

$key = q/displaystyleparentheses{frac{M_k+1}{n_k}}^2=x_k^2+frac{2x_k}{n_k}+unsur{n_k^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.17ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle \parentheses{\frac{M_k + 1}{n_k}}^2 = x_k^2 + \frac{2  x_k}{n_k} + \unsur{n_k^2}$">|; 

$key = q/displaystyleparentheses{frac{M_k+1}{n_k}}^2strictsuperieur2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.13ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \parentheses{\frac{M_k + 1}{n_k}}^2 \strictsuperieur 2$">|; 

$key = q/displaystyleparentheses{frac{a}{b}}^2=frac{a^2}{b^2}=frac{a^2cdotk^2}{b^2cdotk^2}=frac{i^2}{j^2}=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.63ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \parentheses{\frac{a}{b}}^2 = \frac{a^2}{b^2} = \frac{a^2 \cdot k^2}{b^2 \cdot k^2} = \frac{i^2}{j^2} = 2$">|; 

$key = q/displaystyleparentheses{frac{m_k-1}{n_k}}^2=parentheses{y_k-unsur{n_k}}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.13ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img202.svg"
 ALT="$\displaystyle \parentheses{\frac{m_k - 1}{n_k}}^2 = \parentheses{y_k - \unsur{n_k}}^2$">|; 

$key = q/displaystyleparentheses{frac{m_k-1}{n_k}}^2=y_k^2-frac{2y_k}{n_k}+unsur{n_k^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.17ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img203.svg"
 ALT="$\displaystyle \parentheses{\frac{m_k - 1}{n_k}}^2 = y_k^2 - \frac{2  y_k}{n_k} + \unsur{n_k^2}$">|; 

$key = q/displaystyleparentheses{frac{m_k-1}{n_k}}^2strictinferieur2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.13ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img201.svg"
 ALT="$\displaystyle \parentheses{\frac{m_k - 1}{n_k}}^2 \strictinferieur 2$">|; 

$key = q/displaystyler=mmodulo2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle r = m \modulo 2$">|; 

$key = q/displaystylesqrt{2}=lim_{ktoinfty}x_k=lim_{ktoinfty}y_k=supaccolades{x_k:kinsetN}=infaccolades{y_k:kinsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.09ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img279.svg"
 ALT="$\displaystyle \sqrt{2} = \lim_{k \to \infty} x_k = \lim_{k \to \infty} y_k = \sup \accolades{x_k : k \in \setN} = \inf \accolades{y_k : k \in \setN}$">|; 

$key = q/displaystylesupaccolades{x_k^2:kinsetN}=infaccolades{y_k^2:kinsetN}=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img278.svg"
 ALT="$\displaystyle \sup \accolades{x_k^2 : k \in \setN} = \inf \accolades{y_k^2 : k \in \setN} = 2$">|; 

$key = q/displaystylesupaccolades{x_k^2:kinsetN}=min{uinsetQ:uge2}=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle \sup \accolades{x_k^2 : k \in \setN} = \min \{ u \in \setQ : u \ge 2 \} = 2$">|; 

$key = q/displaystyleuinmajorX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\displaystyle u \in \major X$">|; 

$key = q/displaystyleuinminorY;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img248.svg"
 ALT="$\displaystyle u \in \minor Y$">|; 

$key = q/displaystyleunsur{n_k^2}leunsur{n_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.48ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle \unsur{n_k^2} \le \unsur{n_k}$">|; 

$key = q/displaystyleunsur{n_k^2}strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.48ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img207.svg"
 ALT="$\displaystyle \unsur{n_k^2} \strictsuperieur 0$">|; 

$key = q/displaystylevarpi_k=gamma_k+alpha_k=frac{4}{n_k}+frac{5}{n_k}=frac{9}{n_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img266.svg"
 ALT="$\displaystyle \varpi_k = \gamma_k + \alpha_k = \frac{4}{n_k} + \frac{5}{n_k} = \frac{9}{n_k}$">|; 

$key = q/displaystylevnotinmajorX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img155.svg"
 ALT="$\displaystyle v \notin \major X$">|; 

$key = q/displaystylevnotinminorY;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img255.svg"
 ALT="$\displaystyle v \notin \minor Y$">|; 

$key = q/displaystylex=frac{i}{j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.31ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle x = \frac{i}{j}$">|; 

$key = q/displaystylex=sqrt{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\displaystyle x = \sqrt{2}$">|; 

$key = q/displaystylex^2=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle x^2 = 2$">|; 

$key = q/displaystylex^2=frac{i^2}{j^2}=frac{(-i)^2}{(-j)^2}=frac{(-i)^2}{j^2}=frac{i^2}{(-j)^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.76ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle x^2 = \frac{i^2}{j^2} = \frac{(-i)^2}{(-j)^2} = \frac{(-i)^2}{j^2} = \frac{i^2}{(-j)^2}$">|; 

$key = q/displaystylex^2=parentheses{sqrt{2}}^2=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.74ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle x^2 = \parentheses{\sqrt{2}}^2 = 2$">|; 

$key = q/displaystylex_0lex_1lex_2le...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.01ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle x_0 \le x_1 \le x_2 \le ...$">|; 

$key = q/displaystylex_i+x_jge1+1=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.33ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\displaystyle x_i + x_j \ge 1 + 1 = 2$">|; 

$key = q/displaystylex_i,x_jgex_0=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.33ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\displaystyle x_i,x_j \ge x_0 = 1$">|; 

$key = q/displaystylex_i-x_jge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.33ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle x_i - x_j \ge 0$">|; 

$key = q/displaystylex_i-x_jleunsur{2}parentheses{x_i^2-x_j^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\displaystyle x_i - x_j \le \unsur{2}  \parentheses{x_i^2 - x_j^2}$">|; 

$key = q/displaystylex_i^2-x_j^2=(x_i-x_j)cdot(x_i+x_j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle x_i^2 - x_j^2 = (x_i - x_j) \cdot (x_i + x_j)$">|; 

$key = q/displaystylex_i^2-x_j^2=(x_i-x_j)cdot(x_i+x_j)ge(x_i-x_j)cdot2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\displaystyle x_i^2 - x_j^2 = (x_i - x_j) \cdot (x_i + x_j) \ge (x_i - x_j) \cdot 2$">|; 

$key = q/displaystylex_i^2-x_j^2ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle x_i^2 - x_j^2 \ge 0$">|; 

$key = q/displaystylex_i^2-x_j^2le2-(2-alpha_j)=alpha_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle x_i^2 - x_j^2 \le 2 - (2 - \alpha_j) = \alpha_j$">|; 

$key = q/displaystylex_i^2gex_j^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle x_i^2 \ge x_j^2$">|; 

$key = q/displaystylex_i^2le2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle x_i^2 \le 2$">|; 

$key = q/displaystylex_j^2strictsuperieur2-alpha_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle x_j^2 \strictsuperieur 2 - \alpha_j$">|; 

$key = q/displaystylex_k=frac{M_k}{n_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.28ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle x_k = \frac{M_k}{n_k}$">|; 

$key = q/displaystylex_k=frac{M_k}{n_k}le2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.28ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle x_k = \frac{M_k}{n_k} \le 2$">|; 

$key = q/displaystylex_k=frac{M_k}{n_k}lefrac{M_k+1}{n_k}=frac{m_k}{n_k}=y_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.28ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img258.svg"
 ALT="$\displaystyle x_k = \frac{M_k}{n_k} \le \frac{M_k + 1}{n_k} = \frac{m_k}{n_k} = y_k$">|; 

$key = q/displaystylex_k^2+frac{2x_k}{n_k}+unsur{n_k^2}strictsuperieur2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.48ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle x_k^2 + \frac{2  x_k}{n_k} + \unsur{n_k^2} \strictsuperieur 2$">|; 

$key = q/displaystylex_k^2=frac{M_k^2}{n_k^2}le2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle x_k^2 = \frac{M_k^2}{n_k^2} \le 2$">|; 

$key = q/displaystylex_k^2ge2-alpha_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img264.svg"
 ALT="$\displaystyle x_k^2 \ge 2 - \alpha_k$">|; 

$key = q/displaystylex_k^2ge2-epsilonstrictsuperieur2-delta=v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\displaystyle x_k^2 \ge 2 - \epsilon \strictsuperieur 2 - \delta = v$">|; 

$key = q/displaystylex_k^2strictsuperieur2-Delta_kge2-frac{5}{n_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle x_k^2 \strictsuperieur 2 - \Delta_k \ge 2 - \frac{5}{n_k}$">|; 

$key = q/displaystylex_k^2strictsuperieur2-parentheses{frac{2x_k}{n_k}+unsur{n_k^2}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.78ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle x_k^2 \strictsuperieur 2 - \parentheses{\frac{2  x_k}{n_k} + \unsur{n_k^2}}$">|; 

$key = q/displaystylex_{k+1}=frac{M_{k+1}}{n_{k+1}}gefrac{10M_k}{n_{k+1}}=frac{10M_k}{10n_k}=frac{M_k}{n_k}=x_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.44ex; vertical-align: -2.20ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle x_{k+1} = \frac{M_{k+1}}{n_{k+1}} \ge \frac{10  M_k}{n_{k+1}} = \frac{10  M_k}{10  n_k} = \frac{M_k}{n_k} = x_k$">|; 

$key = q/displaystyley_0gey_1gey_2ge...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img198.svg"
 ALT="$\displaystyle y_0 \ge y_1 \ge y_2 \ge ...$">|; 

$key = q/displaystyley_i+y_jge1+1=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.33ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img226.svg"
 ALT="$\displaystyle y_i + y_j \ge 1 + 1 = 2$">|; 

$key = q/displaystyley_i,y_jge1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.33ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img225.svg"
 ALT="$\displaystyle y_i,y_j \ge 1$">|; 

$key = q/displaystyley_i-y_jge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.33ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img229.svg"
 ALT="$\displaystyle y_i - y_j \ge 0$">|; 

$key = q/displaystyley_i-y_jleunsur{2}parentheses{y_i^2-y_j^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img228.svg"
 ALT="$\displaystyle y_i - y_j \le \unsur{2}  \parentheses{y_i^2 - y_j^2}$">|; 

$key = q/displaystyley_i^2-y_j^2=(y_i-y_j)cdot(y_i+y_j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img224.svg"
 ALT="$\displaystyle y_i^2 - y_j^2 = (y_i - y_j) \cdot (y_i + y_j)$">|; 

$key = q/displaystyley_i^2-y_j^2=(y_i-y_j)cdot(y_i+y_j)ge(y_i-y_j)cdot2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img227.svg"
 ALT="$\displaystyle y_i^2 - y_j^2 = (y_i - y_j) \cdot (y_i + y_j) \ge (y_i - y_j) \cdot 2$">|; 

$key = q/displaystyley_i^2-y_j^2ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img222.svg"
 ALT="$\displaystyle y_i^2 - y_j^2 \ge 0$">|; 

$key = q/displaystyley_i^2-y_j^2le2+gamma_i-2=gamma_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img219.svg"
 ALT="$\displaystyle y_i^2 - y_j^2 \le 2 + \gamma_i - 2 = \gamma_i$">|; 

$key = q/displaystyley_i^2gey_j^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img221.svg"
 ALT="$\displaystyle y_i^2 \ge y_j^2$">|; 

$key = q/displaystyley_i^2strictinferieur2+gamma_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img217.svg"
 ALT="$\displaystyle y_i^2 \strictinferieur 2 + \gamma_i$">|; 

$key = q/displaystyley_j^2ge2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img218.svg"
 ALT="$\displaystyle y_j^2 \ge 2$">|; 

$key = q/displaystyley_k+x_kge1+1=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img269.svg"
 ALT="$\displaystyle y_k + x_k \ge 1 + 1 = 2$">|; 

$key = q/displaystyley_k-x_kge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img259.svg"
 ALT="$\displaystyle y_k - x_k \ge 0$">|; 

$key = q/displaystyley_k=frac{m_k}{n_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.69ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img160.svg"
 ALT="$\displaystyle y_k = \frac{m_k}{n_k}$">|; 

$key = q/displaystyley_k^2-frac{2y_k}{n_k}+unsur{n_k^2}strictinferieur2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.48ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img204.svg"
 ALT="$\displaystyle y_k^2 - \frac{2  y_k}{n_k} + \unsur{n_k^2} \strictinferieur 2$">|; 

$key = q/displaystyley_k^2-x_k^2=(y_k-x_k)cdot(y_k+x_k)ge(y_k-x_k)cdot2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img270.svg"
 ALT="$\displaystyle y_k^2 - x_k^2 = (y_k - x_k) \cdot (y_k + x_k) \ge (y_k - x_k) \cdot 2$">|; 

$key = q/displaystyley_k^2-x_k^2le(2+gamma_k)-(2-alpha_k)=gamma_k+alpha_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img265.svg"
 ALT="$\displaystyle y_k^2 - x_k^2 \le (2 + \gamma_k) - (2 - \alpha_k) = \gamma_k + \alpha_k$">|; 

$key = q/displaystyley_k^2=frac{m_k^2}{n_k^2}ge2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img200.svg"
 ALT="$\displaystyle y_k^2 = \frac{m_k^2}{n_k^2} \ge 2$">|; 

$key = q/displaystyley_k^2le2+epsilonstrictinferieur2+delta=v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img254.svg"
 ALT="$\displaystyle y_k^2 \le 2 + \epsilon \strictinferieur 2 + \delta = v$">|; 

$key = q/displaystyley_k^2le2+gamma_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img263.svg"
 ALT="$\displaystyle y_k^2 \le 2 + \gamma_k$">|; 

$key = q/displaystyley_k^2strictinferieur2+delta_kle2+frac{4}{n_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img211.svg"
 ALT="$\displaystyle y_k^2 \strictinferieur 2 + \delta_k \le 2 + \frac{4}{n_k}$">|; 

$key = q/displaystyley_k^2strictinferieur2+parentheses{frac{2y_k}{n_k}-unsur{n_k^2}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.78ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img205.svg"
 ALT="$\displaystyle y_k^2 \strictinferieur 2 + \parentheses{\frac{2  y_k}{n_k} - \unsur{n_k^2}}$">|; 

$key = q/displaystyley_{k+1}=frac{m_{k+1}}{n_{k+1}}lefrac{10m_k}{n_{k+1}}=frac{10m_k}{10n_k}=frac{m_k}{n_k}=y_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.35ex; vertical-align: -2.20ex; " SRC="|."$dir".q|img197.svg"
 ALT="$\displaystyle y_{k+1} = \frac{m_{k+1}}{n_{k+1}} \le \frac{10  m_k}{n_{k+1}} = \frac{10  m_k}{10  n_k} = \frac{m_k}{n_k} = y_k$">|; 

$key = q/displaystyle{uinsetQ:uge2}subseteqmajorX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\displaystyle \{ u \in \setQ : u \ge 2 \} \subseteq \major X$">|; 

$key = q/displaystyle{uinsetQ:ule2}subseteqminorY;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img249.svg"
 ALT="$\displaystyle \{ u \in \setQ : u \le 2 \} \subseteq \minor Y$">|; 

$key = q/e_kge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img239.svg"
 ALT="$e_k \ge 0$">|; 

$key = q/epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img150.svg"
 ALT="$\epsilon$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/f:xmapstox^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img111.svg"
 ALT="$f : x \mapsto x^2$">|; 

$key = q/gamma_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img241.svg"
 ALT="$\gamma_k$">|; 

$key = q/i,jgeK;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img131.svg"
 ALT="$i,j \ge K$">|; 

$key = q/i,jinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img88.svg"
 ALT="$i,j \in \setN$">|; 

$key = q/i,jinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$i,j \in \setZ$">|; 

$key = q/i=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$i = 0$">|; 

$key = q/igej;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img89.svg"
 ALT="$i \ge j$">|; 

$key = q/ilej;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img214.svg"
 ALT="$i \le j$">|; 

$key = q/ine0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$i \ne 0$">|; 

$key = q/jne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$j \ne 0$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img128.svg"
 ALT="$k$">|; 

$key = q/k=pgcd(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$k = \pgcd(i,j)$">|; 

$key = q/kgeK;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img129.svg"
 ALT="$k \ge K$">|; 

$key = q/kinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img60.svg"
 ALT="$k \in \setN$">|; 

$key = q/kmapstox_k^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img152.svg"
 ALT="$k \mapsto x_k^2$">|; 

$key = q/kmapstoy_k^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img252.svg"
 ALT="$k \mapsto y_k^2$">|; 

$key = q/kne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$k \ne 0$">|; 

$key = q/m:kmapstom_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img158.svg"
 ALT="$m : k \mapsto m_k$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$m$">|; 

$key = q/m_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img187.svg"
 ALT="$m_k$">|; 

$key = q/m_k=M_k+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img189.svg"
 ALT="$m_k = M_k + 1$">|; 

$key = q/m_{k+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img195.svg"
 ALT="$m_{k+1}$">|; 

$key = q/minsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img21.svg"
 ALT="$m \in \setN$">|; 

$key = q/n:kmapston_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img56.svg"
 ALT="$n : k \mapsto n_k$">|; 

$key = q/n_k^2strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img98.svg"
 ALT="$n_k^2 \strictsuperieur 0$">|; 

$key = q/n_kstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img77.svg"
 ALT="$n_k \strictsuperieur 0$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img167.svg"
 ALT="$p$">|; 

$key = q/p=bdiventiere2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$p = b \diventiere 2$">|; 

$key = q/pinA_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img170.svg"
 ALT="$p \in A_k$">|; 

$key = q/pinA_kcapB_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img174.svg"
 ALT="$p \in A_k \cap B_k$">|; 

$key = q/pinB_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img172.svg"
 ALT="$p \in B_k$">|; 

$key = q/pinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img67.svg"
 ALT="$p \in \setN$">|; 

$key = q/pinsetNsetminusA_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img177.svg"
 ALT="$p \in \setN \setminus A_k$">|; 

$key = q/pstrictsuperieur2n_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img68.svg"
 ALT="$p \strictsuperieur 2  n_k$">|; 

$key = q/r=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$r = 0$">|; 

$key = q/r=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img28.svg"
 ALT="$r = 1$">|; 

$key = q/rinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img25.svg"
 ALT="$r \in \setN$">|; 

$key = q/rin{0,1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$r \in \{0,1\}$">|; 

$key = q/setN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\setN$">|; 

$key = q/setQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img166.svg"
 ALT="$\setQ$">|; 

$key = q/sqrt{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img280.svg"
 ALT="$\sqrt{2}$">|; 

$key = q/t=2kstrictsuperieurk;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img50.svg"
 ALT="$t = 2  k \strictsuperieur k$">|; 

$key = q/uge2geX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img145.svg"
 ALT="$u \ge 2 \ge X$">|; 

$key = q/uinA_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img182.svg"
 ALT="$u \in A_k$">|; 

$key = q/uinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img179.svg"
 ALT="$u \in \setN$">|; 

$key = q/ule2leY;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img247.svg"
 ALT="$u \le 2 \le Y$">|; 

$key = q/ulep;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img180.svg"
 ALT="$u \le p$">|; 

$key = q/vstrictinferieur2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img148.svg"
 ALT="$v \strictinferieur 2$">|; 

$key = q/vstrictsuperieur2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img250.svg"
 ALT="$v \strictsuperieur 2$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img53.svg"
 ALT="$x$">|; 

$key = q/x^2=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img55.svg"
 ALT="$x^2 = 2$">|; 

$key = q/x_igex_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img87.svg"
 ALT="$x_i \ge x_j$">|; 

$key = q/x_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img79.svg"
 ALT="$x_k$">|; 

$key = q/x_k^2le2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img135.svg"
 ALT="$x_k^2 \le 2$">|; 

$key = q/x_k^2ley_k^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img261.svg"
 ALT="$x_k^2 \le y_k^2$">|; 

$key = q/x_kle2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img101.svg"
 ALT="$x_k \le 2$">|; 

$key = q/xinsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.17ex; vertical-align: -0.48ex; " SRC="|."$dir".q|img3.svg"
 ALT="$x \in \setQ$">|; 

$key = q/y_igey_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img220.svg"
 ALT="$y_i \ge y_j$">|; 

$key = q/y_iley_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img199.svg"
 ALT="$y_i \le y_j$">|; 

$key = q/y_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img191.svg"
 ALT="$y_k$">|; 

$key = q/y_k^2ge2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img238.svg"
 ALT="$y_k^2 \ge 2$">|; 

$key = q/y_kgex_kgex_0=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img268.svg"
 ALT="$y_k \ge x_k \ge x_0 = 1$">|; 

$key = q/y_kley_0=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img209.svg"
 ALT="$y_k \le y_0 = 2$">|; 

1;

