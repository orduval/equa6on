# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/-A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img39.svg"
 ALT="$-A$">|; 

$key = q/-AleSle-alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img48.svg"
 ALT="$-A \le S \le -\alpha$">|; 

$key = q/-alphage-x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img46.svg"
 ALT="$-\alpha \ge -x$">|; 

$key = q/-alphainmajor(-A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$-\alpha \in \major(-A)$">|; 

$key = q/-lambdage-x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img37.svg"
 ALT="$-\lambda \ge -x$">|; 

$key = q/-lambdainmajor(-A)neemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$-\lambda \in \major(-A) \ne \emptyset$">|; 

$key = q/0strictinferieurdeltaleD;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img58.svg"
 ALT="$0 \strictinferieur \delta \le D$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$A$">|; 

$key = q/Aneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img2.svg"
 ALT="$A \ne \emptyset$">|; 

$key = q/AsubseteqsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img1.svg"
 ALT="$A \subseteq \setR$">|; 

$key = q/I=-S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img50.svg"
 ALT="$I = -S$">|; 

$key = q/I=-Slex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img42.svg"
 ALT="$I = -S \le x$">|; 

$key = q/I=infA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img77.svg"
 ALT="$I = \inf A$">|; 

$key = q/IinminorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img43.svg"
 ALT="$I \in \minor A$">|; 

$key = q/Istrictinferieury;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img80.svg"
 ALT="$I \strictinferieur y$">|; 

$key = q/IstrictinferieuryleA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img83.svg"
 ALT="$I \strictinferieur y \le A$">|; 

$key = q/M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img24.svg"
 ALT="$M$">|; 

$key = q/MinQ(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$M \in Q(x)$">|; 

$key = q/Q(mu);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$Q(\mu)$">|; 

$key = q/Q(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$Q(x)$">|; 

$key = q/Q(x)leM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$Q(x) \le M$">|; 

$key = q/Q(x)subseteqQ(mu);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$Q(x) \subseteq Q(\mu)$">|; 

$key = q/Q(x)subseteqSleM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$Q(x) \subseteq S \le M$">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$S$">|; 

$key = q/S=sup(-A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$S = \sup(-A)$">|; 

$key = q/S=supA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img67.svg"
 ALT="$S = \sup A$">|; 

$key = q/Sge-x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img41.svg"
 ALT="$S \ge -x$">|; 

$key = q/Slesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img17.svg"
 ALT="$S \le \sigma$">|; 

$key = q/Sstrictsuperieury;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img70.svg"
 ALT="$S \strictsuperieur y$">|; 

$key = q/SstrictsuperieurygeA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img73.svg"
 ALT="$S \strictsuperieur y \ge A$">|; 

$key = q/alphainQ(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\alpha \in Q(x)$">|; 

$key = q/alphainS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\alpha \in S$">|; 

$key = q/alphainminorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\alpha \in \minor A$">|; 

$key = q/alphaleIleA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\alpha \le I \le A$">|; 

$key = q/alphalex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\alpha \le x$">|; 

$key = q/beta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\beta$">|; 

$key = q/betainQ(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\beta \in Q(x)$">|; 

$key = q/betainsetQ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\beta \in \setQ$">|; 

$key = q/betastrictinferieuralpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\beta \strictinferieur \alpha$">|; 

$key = q/d=distance(x,r)ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$d = \distance(x,r) \le \delta$">|; 

$key = q/deltale0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\delta \le 0$">|; 

$key = q/deltale0leD;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\delta \le 0 \le D$">|; 

$key = q/deltaledinD;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\delta \le d \in D$">|; 

$key = q/deltanotinminorD;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\delta \notin \minor D$">|; 

$key = q/deltastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\delta \strictsuperieur 0$">|; 

$key = q/displaystyle-A={-xinsetR:xinA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle -A = \{ -x \in \setR : x \in A \}$">|; 

$key = q/displaystyleD={distance(r,x):xinA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle D = \{ \distance(r,x) : x \in A \}$">|; 

$key = q/displaystyleS=bigcup_{xinA}Q(x)subseteqQ(mu);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle S = \bigcup_{x \in A} Q(x) \subseteq Q(\mu)$">|; 

$key = q/displaystyleS=sup_subseteq{Q(x)insousens(setQ):xinA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.29ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle S = \sup_\subseteq \{ Q(x) \in \sousens(\setQ) : x \in A \} $">|; 

$key = q/displaystyleSsubseteqQ(mu)lesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle S \subseteq Q(\mu) \le \sigma$">|; 

$key = q/displaystyledistance(I,x)=abs{I-x}=x-Igedelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle \distance(I,x) = \abs{I - x} = x - I \ge \delta$">|; 

$key = q/displaystyledistance(S,x)=abs{S-x}=S-xgedelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \distance(S,x) = \abs{S - x} = S - x \ge \delta$">|; 

$key = q/displaystyledistance(r,A)=infD=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle \distance(r,A) = \inf D = 0$">|; 

$key = q/displaystyledistance(r,A)=infD=maxminorD=maxintervallesemiouvertgauche{-infty}{0}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle \distance(r,A) = \inf D = \max \minor D = \max \intervallesemiouvertgauche{-\infty}{0} = 0$">|; 

$key = q/displaystyleinfA=-sup(-A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \inf A = - \sup(-A)$">|; 

$key = q/displaystyleinfAinadhA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle \inf A \in \adh A$">|; 

$key = q/displaystyles=supA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle s = \sup A$">|; 

$key = q/displaystylesupAinadhA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle \sup A \in \adh A$">|; 

$key = q/displaystylex-y=(x-I)+(I-y)gedelta-frac{delta}{2}=frac{delta}{2}strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle x - y = (x - I) + (I - y) \ge \delta - \frac{\delta}{2} = \frac{\delta}{2} \strictsuperieur 0$">|; 

$key = q/displaystyley-x=(y-S)+(S-x)ge-frac{delta}{2}+delta=frac{delta}{2}strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle y - x = (y - S) + (S - x) \ge - \frac{\delta}{2} + \delta = \frac{\delta}{2} \strictsuperieur 0$">|; 

$key = q/displaystyley=I+frac{delta}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle y = I + \frac{\delta}{2}$">|; 

$key = q/displaystyley=S-frac{delta}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle y = S - \frac{\delta}{2}$">|; 

$key = q/distance(I,A)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\distance(I,A) = 0$">|; 

$key = q/distance(I,x)ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\distance(I,x) \le \delta$">|; 

$key = q/distance(S,A)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\distance(S,A) = 0$">|; 

$key = q/distance(S,x)ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\distance(S,x) \le \delta$">|; 

$key = q/distance(x,r)ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\distance(x,r) \le \delta$">|; 

$key = q/distance(x,r)strictsuperieurdelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\distance(x,r) \strictsuperieur \delta$">|; 

$key = q/lambdainminorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\lambda \in \minor A$">|; 

$key = q/lambdalex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\lambda \le x$">|; 

$key = q/le;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\le$">|; 

$key = q/majorAneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\major A \ne \emptyset$">|; 

$key = q/minorAneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\minor A \ne \emptyset$">|; 

$key = q/minorD=intervallesemiouvertgauche{-infty}{0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\minor D = \intervallesemiouvertgauche{-\infty}{0}$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\mu$">|; 

$key = q/mugeA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\mu \ge A$">|; 

$key = q/muinmajorA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\mu \in \major A$">|; 

$key = q/rinadhA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img54.svg"
 ALT="$r \in \adh A$">|; 

$key = q/rinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img52.svg"
 ALT="$r \in \setR$">|; 

$key = q/s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img28.svg"
 ALT="$s$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\setR$">|; 

$key = q/sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\sigma$">|; 

$key = q/subseteq;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\subseteq$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img3.svg"
 ALT="$x \in A$">|; 

$key = q/ygeA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img72.svg"
 ALT="$y \ge A$">|; 

$key = q/yleA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img82.svg"
 ALT="$y \le A$">|; 

1;

