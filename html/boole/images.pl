# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$0$">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$1$">|; 

$key = q/=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$=1$">|; 

$key = q/C_1,C_2insetB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$C_1, C_2 \in \setB$">|; 

$key = q/C_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img5.svg"
 ALT="$C_1$">|; 

$key = q/C_1=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img8.svg"
 ALT="$C_1 = 1$">|; 

$key = q/C_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img6.svg"
 ALT="$C_2$">|; 

$key = q/C_2=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img9.svg"
 ALT="$C_2 = 1$">|; 

$key = q/C_i=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img12.svg"
 ALT="$C_i = 1$">|; 

$key = q/CinsetB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img15.svg"
 ALT="$C \in \setB$">|; 

$key = q/cdot:setBtimessetBmapstosetB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\cdot : \setB \times \setB \mapsto \setB$">|; 

$key = q/displaystylesetB={0,1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\displaystyle \setB = \{ 0 , 1 \}$">|; 

$key = q/negC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\neg C$">|; 

$key = q/oplus:setBtimessetBmapstosetB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\oplus : \setB \times \setB \mapsto \setB$">|; 

$key = q/{Eqts}1cdot1=11cdot0=00cdot1=00cdot0=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.12ex; vertical-align: -6.00ex; " SRC="|."$dir".q|img11.svg"
 ALT="\begin{Eqts}
1 \cdot 1 = 1 \\\\
1 \cdot 0 = 0 \\\\
0 \cdot 1 = 0 \\\\
0 \cdot 0 = 0
\end{Eqts}">|; 

$key = q/{Eqts}1oplus1=11oplus0=10oplus1=10oplus0=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.12ex; vertical-align: -6.00ex; " SRC="|."$dir".q|img14.svg"
 ALT="\begin{Eqts}
1 \oplus 1 = 1 \\\\
1 \oplus 0 = 1 \\\\
0 \oplus 1 = 1 \\\\
0 \oplus 0 = 0
\end{Eqts}">|; 

$key = q/{Eqts}neg1=0neg0=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img17.svg"
 ALT="\begin{Eqts}
\neg 1 = 0 \\\\
\neg 0 = 1
\end{Eqts}">|; 

1;

