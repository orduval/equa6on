# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="\begin{demonstration}
\par Choisissons $y \in B$ et considérons l'ensemble de ...
...e et&nbsp;:
\par\begin{displaymath}f^{-1} = g\end{displaymath}\par\end{demonstration}">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$A$">|; 

$key = q/AcupB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img49.svg"
 ALT="$A \cup B$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img28.svg"
 ALT="$B$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$R$">|; 

$key = q/ainA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img57.svg"
 ALT="$a \in A$">|; 

$key = q/binB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img59.svg"
 ALT="$b \in B$">|; 

$key = q/displaystyle(f_ncirc...circf_2circf_1)^{-1}=f_1^{-1}circf_2^{-1}circ...circf_n^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle (f_n \circ ... \circ f_2 \circ f_1)^{-1} = f_1^{-1} \circ f_2^{-1} \circ ... \circ f_n^{-1}$">|; 

$key = q/displaystyle(gcircf)^{-1}=f^{-1}circg^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle (g \circ f)^{-1} = f^{-1} \circ g^{-1}$">|; 

$key = q/displaystyleR^{-1}(y)={f^{-1}(y)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle R^{-1}(y) = \{ f^{-1}(y) \}$">|; 

$key = q/displaystylebijection(A,B)={finB^A:existsf^{-1}inA^B};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \bijection(A,B) = \{ f \in B^A : \exists f^{-1} \in A^B \}$">|; 

$key = q/displaystylebijection(A,B)neemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \bijection(A,B) \ne \emptyset$">|; 

$key = q/displaystylef(x)=(fcircr)(y)=identite(y)=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle f(x) = (f \circ r)(y) = \identite(y) = y$">|; 

$key = q/displaystylef^{-1}(y)=r(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle f^{-1}(y) = r(y)$">|; 

$key = q/displaystylef^{-1}=r=l;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle f^{-1} = r = l$">|; 

$key = q/displaystylef^{-1}circf=fcircf^{-1}=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle f^{-1} \circ f = f \circ f^{-1} = \identite$">|; 

$key = q/displaystylef^{-n}=left(f^{-1}right)^n=f^{-1}circ...circf^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle f^{-n} = \left( f^{-1} \right)^n = f^{-1} \circ ... \circ f^{-1}$">|; 

$key = q/displaystylefcircr=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle f \circ r = \identite$">|; 

$key = q/displaystyleh=f^{-1}circg^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle h = f^{-1} \circ g^{-1}$">|; 

$key = q/displaystylehcircgcircfcircf^{-1}circg^{-1}=hcircgcircidentitecircg^{-1}=hcircgcircg^{-1}=hcircidentite=h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle h \circ g \circ f \circ f^{-1} \circ g^{-1} = h \circ g \circ \identite \circ g^{-1} = h \circ g \circ g^{-1} = h \circ \identite = h$">|; 

$key = q/displaystylehcircgcircfcircf^{-1}circg^{-1}=identitecircf^{-1}circg^{-1}=f^{-1}circg^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle h \circ g \circ f \circ f^{-1} \circ g^{-1} = \identite \circ f^{-1} \circ g^{-1} = f^{-1} \circ g^{-1}$">|; 

$key = q/displaystylel(y)=(lcircf)(x_1)=(lcircf)(x_2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle l(y) = (l \circ f)(x_1) = (l \circ f)(x_2)$">|; 

$key = q/displaystylel(y)=x_1=x_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle l(y) = x_1 = x_2$">|; 

$key = q/displaystylel=lcircidentite=lcircfcircr=identitecircr=r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle l = l \circ \identite = l \circ f \circ r = \identite \circ r = r$">|; 

$key = q/displaystylelcircf=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle l \circ f = \identite$">|; 

$key = q/displaystyleleft(f^{-1}right)^n=f^{-n}=left(f^nright)^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.12ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \left( f^{-1} \right)^n = f^{-n} = \left( f^n \right)^{-1}$">|; 

$key = q/displaystylemathcal{E}(a)={a,f(a)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \mathcal{E}(a) = \{ a , f(a) \}$">|; 

$key = q/displaystylemathcal{E}(b)={b,f^{-1}(b)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \mathcal{E}(b) = \{ b , f^{-1}(b) \}$">|; 

$key = q/f(x)=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$f(x) = y$">|; 

$key = q/f(x_1)=f(x_2)=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$f(x_1) = f(x_2) = y$">|; 

$key = q/f:AmapstoB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f : A \mapsto B$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$f$">|; 

$key = q/f^{-1}:BmapstoA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img21.svg"
 ALT="$f^{-1} : B \mapsto A$">|; 

$key = q/f^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img25.svg"
 ALT="$f^{-1}$">|; 

$key = q/f_1=...=f_n=f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$f_1 = ... = f_n = f$">|; 

$key = q/finbijection(A,B);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$f \in \bijection(A,B)$">|; 

$key = q/g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img35.svg"
 ALT="$g$">|; 

$key = q/gcircfcirch=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img39.svg"
 ALT="$g \circ f \circ h = \identite$">|; 

$key = q/ginbijection(B,C);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$g \in \bijection(B,C)$">|; 

$key = q/h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$h$">|; 

$key = q/h=f^{-1}circg^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img40.svg"
 ALT="$h = f^{-1} \circ g^{-1}$">|; 

$key = q/hat{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\hat{x}$">|; 

$key = q/hcircgcircf=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img34.svg"
 ALT="$h \circ g \circ f = \identite$">|; 

$key = q/identite(x)=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\identite(x) = x$">|; 

$key = q/l:BmapstoA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$l : B \mapsto A$">|; 

$key = q/l;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$l$">|; 

$key = q/lcircf=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$l \circ f = \identite$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img42.svg"
 ALT="$n$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img45.svg"
 ALT="$n \in \setN$">|; 

$key = q/r:BmapstoA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$r : B \mapsto A$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$r$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img54.svg"
 ALT="$x$">|; 

$key = q/x=hat{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img61.svg"
 ALT="$x = \hat{x}$">|; 

$key = q/x=r(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$x = r(y)$">|; 

$key = q/x_1,x_2inA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$x_1, x_2 \in A$">|; 

$key = q/xequivy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.72ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img50.svg"
 ALT="$x \equiv y$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$x \in A$">|; 

$key = q/xinB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img52.svg"
 ALT="$x \in B$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img55.svg"
 ALT="$y$">|; 

$key = q/y=f(x)inB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$y = f(x) \in B$">|; 

$key = q/y=f^{-1}(x)inA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$y = f^{-1}(x) \in A$">|; 

$key = q/yinB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$y \in B$">|; 

$key = q/{theoreme}{par{Soitlafonction{f:AmapstoB{avec{B=f(A){.Si{f{eststrictementcroissante(oudécroissante),alorslafonction{f{estinversible.{par{{theoreme};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="\begin{theoreme}
\par Soit la fonction $f : A \mapsto B$ avec $B = f(A)$. Si $f...
...te (ou décroissante), alors la fonction $f$ est inversible.
\par\end{theoreme}">|; 

1;

