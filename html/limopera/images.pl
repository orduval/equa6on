# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 23.52ex; " SRC="|."$dir".q|img37.svg"
 ALT="\begin{eqnarray*}
\abs{\unsur{g(x)} - \unsur{G}} &amp;=&amp; \abs{\frac{G - g(x)}{g(x) \...
...abs{G} } \\\\
&amp;=&amp; \frac{ \gamma }{ \abs{g(x)} \cdot \abs{G} } \\\\
\end{eqnarray*}">|; 

$key = q/-G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img24.svg"
 ALT="$-G$">|; 

$key = q/-g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img23.svg"
 ALT="$-g$">|; 

$key = q/1slashG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$1/G$">|; 

$key = q/1slashg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$1/g$">|; 

$key = q/F+G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.95ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img14.svg"
 ALT="$F + G$">|; 

$key = q/F,GinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$F,G \in \setR$">|; 

$key = q/FcdotG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$F \cdot G$">|; 

$key = q/GinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img16.svg"
 ALT="$G \in \setR$">|; 

$key = q/Gne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img36.svg"
 ALT="$G \ne 0$">|; 

$key = q/abs{(-g(x))-(-G)}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\abs{(-g(x)) - (-G)} \le \epsilon$">|; 

$key = q/abs{f(x)+g(x)-(F+G)}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\abs{f(x) + g(x) - (F + G)} \le \epsilon$">|; 

$key = q/abs{unsur{g(x)}-unsur{G}}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\abs{\unsur{g(x)} - \unsur{G}} \le \epsilon$">|; 

$key = q/delta(gamma)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\delta(\gamma) \strictsuperieur 0$">|; 

$key = q/displaystyle(1+epsiloncdotabs{G})cdotgammaleG^2cdotepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle (1 + \epsilon \cdot \abs{G}) \cdot \gamma \le G^2 \cdot \epsilon$">|; 

$key = q/displaystyle0strictinferieurgammalefrac{G^2cdotepsilon}{1+epsiloncdotabs{G}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.76ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle 0 \strictinferieur \gamma \le \frac{G^2 \cdot \epsilon}{1 + \epsilon \cdot \abs{G}}$">|; 

$key = q/displaystyle0strictinferieurgammastrictinferieurminleft{abs{G},frac{G^2cdotepsilon}{1+epsiloncdotabs{G}}right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle 0 \strictinferieur \gamma \strictinferieur \min\left\{ \abs{G} , \frac{G^2 \cdot \epsilon}{1 + \epsilon \cdot \abs{G}} \right\}$">|; 

$key = q/displaystyleabs{big[-g(x)big]-(-G)}=abs{G-g(x)}=abs{g(x)-G}legamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \abs{\big[-g(x)\big] - (-G)} = \abs{G - g(x)} = \abs{g(x) - G} \le \gamma$">|; 

$key = q/displaystyleabs{f(x)+g(x)-(F+G)}leabs{f(x)-F}+abs{g(x)-G}=2gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \abs{f(x) + g(x) - (F + G)} \le \abs{f(x) - F} + \abs{g(x) - G} = 2 \gamma$">|; 

$key = q/displaystyleabs{f(x)cdotg(x)-FcdotG}le(1+abs{F}+abs{G})cdotgammaleepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \abs{f(x) \cdot g(x) - F \cdot G} \le (1 + \abs{F} + \abs{G}) \cdot \gamma \le \epsilon$">|; 

$key = q/displaystyleabs{f(x)}=abs{f(x)-F+F}leabs{f(x)-F}+Flegamma+abs{F};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \abs{f(x)} = \abs{f(x) - F + F} \le \abs{f(x) - F} + F \le \gamma + \abs{F}$">|; 

$key = q/displaystyleabs{unsur{g(x)}-unsur{G}}lefrac{gamma}{(abs{G}-gamma)cdotabs{G}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \abs{\unsur{g(x)} - \unsur{G}} \le \frac{ \gamma }{ (\abs{G} - \gamma) \cdot \abs{G} }$">|; 

$key = q/displaystylefrac{gamma}{(abs{G}-gamma)cdotabs{G}}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.92ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \frac{ \gamma }{ (\abs{G} - \gamma) \cdot \abs{G} } \le \epsilon$">|; 

$key = q/displaystyleg:xmapstoG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle g : x \mapsto G$">|; 

$key = q/displaystylegammaleepsiloncdot(abs{G}-gamma)cdotabs{G}=G^2cdotepsilon-epsiloncdotabs{G}cdotgamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \gamma \le \epsilon \cdot (\abs{G} - \gamma) \cdot \abs{G} = G^2 \cdot \epsilon - \epsilon \cdot \abs{G} \cdot \gamma$">|; 

$key = q/displaystylelim_{xtoa}big[-g(x)big]=-lim_{xtoa}g(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.78ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \lim_{x \to a} \big[-g(x)\big] = - \lim_{x \to a} g(x)$">|; 

$key = q/displaystylelim_{xtoa}big[f(x)+Gbig]=lim_{xtoa}f(x)+G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.78ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \lim_{x \to a} \big[f(x) + G\big] = \lim_{x \to a} f(x) + G$">|; 

$key = q/displaystylelim_{xtoa}big[f(x)+g(x)big]=lim_{xtoa}f(x)+lim_{xtoa}g(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.78ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \lim_{x \to a} \big[f(x) + g(x)\big] = \lim_{x \to a} f(x) + \lim_{x \to a} g(x)$">|; 

$key = q/displaystylelim_{xtoa}big[f(x)cdotg(x)big]=left[lim_{xtoa}f(x)right]cdotleft[lim_{xtoa}g(x)right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.48ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \lim_{x \to a} \big[f(x) \cdot g(x)\big] = \left[ \lim_{x \to a} f(x) \right] \cdot \left[ \lim_{x \to a} g(x) \right]$">|; 

$key = q/displaystylelim_{xtoa}frac{f(x)}{g(x)}=left[lim_{xtoa}f(x)right]cdotleft[lim_{xtoa}unsur{g(x)}right]=frac{lim_{xtoa}f(x)}{lim_{xtoa}g(x)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \lim_{x \to a} \frac{f(x)}{g(x)} = \left[\lim_{x \to a} f(x)\righ...
...{x \to a} \unsur{g(x)}\right] = \frac{\lim_{x \to a} f(x)}{\lim_{x \to a} g(x)}$">|; 

$key = q/displaystylelim_{xtoa}g(x)=lim_{xtoa}G=G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \lim_{x \to a} g(x) = \lim_{x \to a} G = G$">|; 

$key = q/displaystylelim_{xtoa}unsur{g(x)}=unsur{lim_{xtoa}g(x)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \lim_{x \to a} \unsur{g(x)} = \unsur{\lim_{x \to a} g(x)}$">|; 

$key = q/distance(x,a)=abs{x-a}ledelta(gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\distance(x,a) = \abs{x - a} \le \delta(\gamma)$">|; 

$key = q/epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\epsilon$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/f+g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img13.svg"
 ALT="$f + g$">|; 

$key = q/f,g:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f,g : \setR \mapsto \setR$">|; 

$key = q/fcdotg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img33.svg"
 ALT="$f \cdot g$">|; 

$key = q/gamma=epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\gamma = \epsilon$">|; 

$key = q/gamma=epsilonslash2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\gamma = \epsilon / 2$">|; 

$key = q/gamma=min{1,epsilonslash(1+abs{F}+abs{G})};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\gamma = \min\{ 1 , \epsilon / (1 + \abs{F} + \abs{G}) \}$">|; 

$key = q/gammale1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\gamma \le 1$">|; 

$key = q/gammastrictinferieurabs{G};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\gamma \strictinferieur \abs{G}$">|; 

$key = q/gammastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\gamma \strictsuperieur 0$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img8.svg"
 ALT="$x \in \setR$">|; 

$key = q/{Eqts}F=lim_{xtoa}f(x)G=lim_{xtoa}g(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.50ex; vertical-align: -5.19ex; " SRC="|."$dir".q|img2.svg"
 ALT="\begin{Eqts}
F = \lim_{x \to a} f(x) \ \\\\
G = \lim_{x \to a} g(x)
\end{Eqts}">|; 

$key = q/{Eqts}distance(f(x),F)=abs{f(x)-F}legammadistance(g(x),G)=abs{g(x)-G}legamma{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img7.svg"
 ALT="\begin{Eqts}
\distance(f(x),F) = \abs{f(x) - F} \le \gamma \\\\
\distance(g(x),G) = \abs{g(x) - G} \le \gamma
\end{Eqts}">|; 

$key = q/{eqnarraystar}abs{f(x)cdotg(x)-FcdotG}&le&(gamma+abs{F})cdotgamma+gammacdotabs{G}&le&(gamma+abs{F}+abs{G})cdotgamma{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img29.svg"
 ALT="\begin{eqnarray*}
\abs{f(x) \cdot g(x) - F \cdot G} &amp;\le&amp; (\gamma + \abs{F}) \cd...
...cdot \abs{G} \\\\
&amp;\le&amp; (\gamma + \abs{F} + \abs{G}) \cdot \gamma
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}abs{g(x)}=abs{g(x)-G+G}&=&abs{G-(G-g(x))}&ge&abs{G}-abs{G-g(x)}&ge&abs{G}-gamma{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img38.svg"
 ALT="\begin{eqnarray*}
\abs{g(x)} = \abs{g(x) - G + G} &amp;=&amp; \abs{G - (G - g(x))} \\\\
&amp;\ge&amp; \abs{G} - \abs{G - g(x)} \\\\
&amp;\ge&amp; \abs{G} - \gamma
\end{eqnarray*}">|; 

1;

