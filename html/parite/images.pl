# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/alpha:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\alpha : \setR \mapsto \setR$">|; 

$key = q/alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\alpha$">|; 

$key = q/displaystylealpha(-x)=-alpha(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \alpha(-x) = - \alpha(x)$">|; 

$key = q/displaystylealpha(-x)=unsur{2}Big[varphi(-x)-varphi(x)Big]=-unsur{2}Big[varphi(x)-varphi(-x)Big]=-alpha(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \alpha(-x) = \unsur{2}  \Big[\varphi(-x) - \varphi(x)\Big] = - \unsur{2}  \Big[\varphi(x) - \varphi(-x)\Big] = - \alpha(x)$">|; 

$key = q/displaystylealpha(x)=unsur{2}Big[varphi(x)-varphi(-x)Big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \alpha(x) = \unsur{2}  \Big[\varphi(x) - \varphi(-x)\Big]$">|; 

$key = q/displaystylesigma(-x)=sigma(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle \sigma(-x) = \sigma(x)$">|; 

$key = q/displaystylesigma(-x)=unsur{2}Big[varphi(-x)+varphi(x)Big]=unsur{2}Big[varphi(x)+varphi(-x)Big]=sigma(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \sigma(-x) = \unsur{2}  \Big[\varphi(-x) + \varphi(x)\Big] = \unsur{2}  \Big[\varphi(x) + \varphi(-x)\Big] = \sigma(x)$">|; 

$key = q/displaystylesigma(x)+alpha(x)=unsur{2}Big[varphi(x)+varphi(-x)+varphi(x)-varphi(-x)Big]=frac{2}{2}varphi(x)=varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \sigma(x) + \alpha(x) = \unsur{2}  \Big[\varphi(x) + \varphi(-x) + \varphi(x) - \varphi(-x)\Big] = \frac{2}{2}  \varphi(x) = \varphi(x)$">|; 

$key = q/displaystylesigma(x)=unsur{2}Big[varphi(x)+varphi(-x)Big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \sigma(x) = \unsur{2}  \Big[\varphi(x) + \varphi(-x)\Big]$">|; 

$key = q/displaystylevarphi=sigma+alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \varphi = \sigma + \alpha$">|; 

$key = q/sigma:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\sigma : \setR \mapsto \setR$">|; 

$key = q/sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\sigma$">|; 

$key = q/varphi:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\varphi : \setR \mapsto \setR$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\varphi$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$x$">|; 

1;

