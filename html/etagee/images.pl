# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 26.66ex; " SRC="|."$dir".q|img175.svg"
 ALT="\begin{eqnarray*}
\{ x \in A : f(x) + w(x) \strictsuperieur a \} &amp;=&amp; \bigcup_{i ...
...bigcup_{i = 1}^n \{ x \in A_i : f(x) \strictsuperieur a - w_i \}
\end{eqnarray*}">|; 

$key = q/(AsetminusB)cupB=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$(A \setminus B) \cup B = A$">|; 

$key = q/(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$(i,j)$">|; 

$key = q/(i,j)inF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img111.svg"
 ALT="$(i,j) \in F$">|; 

$key = q/(i,j)inM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img126.svg"
 ALT="$(i,j) \in M$">|; 

$key = q/(i,j)inP;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img80.svg"
 ALT="$(i,j) \in P$">|; 

$key = q/(i,j)inPsetminusF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$(i,j) \in P \setminus F$">|; 

$key = q/(i,j)inPsetminusM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$(i,j) \in P \setminus M$">|; 

$key = q/(k,l)inPsetminus(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$(k,l) \in P \setminus (i,j)$">|; 

$key = q/-(f+w)=-f-w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img182.svg"
 ALT="$-(f + w) = -f - w$">|; 

$key = q/-(f-w)=w-f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img185.svg"
 ALT="$-(f - w) = w - f$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img63.svg"
 ALT="$0$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$A$">|; 

$key = q/A_alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img46.svg"
 ALT="$A_\alpha$">|; 

$key = q/A_beta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img54.svg"
 ALT="$A_\beta$">|; 

$key = q/A_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img70.svg"
 ALT="$A_i$">|; 

$key = q/A_i=C_icapA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img147.svg"
 ALT="$A_i = C_i \cap A$">|; 

$key = q/A_iinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img66.svg"
 ALT="$A_i \in \mathcal{T}$">|; 

$key = q/AcupB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img145.svg"
 ALT="$A \cup B$">|; 

$key = q/Ainmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img3.svg"
 ALT="$A \in \mathcal{T}$">|; 

$key = q/AsetminusB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$A \setminus B$">|; 

$key = q/AsubseteqPhi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img4.svg"
 ALT="$A \subseteq \Phi$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$B$">|; 

$key = q/B_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img23.svg"
 ALT="$B_i$">|; 

$key = q/B_i=A_icapB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img18.svg"
 ALT="$B_i = A_i \cap B$">|; 

$key = q/B_i=C_icapB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img148.svg"
 ALT="$B_i = C_i \cap B$">|; 

$key = q/B_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img90.svg"
 ALT="$B_j$">|; 

$key = q/B_jinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img73.svg"
 ALT="$B_j \in \mathcal{T}$">|; 

$key = q/BsubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img16.svg"
 ALT="$B \subseteq A$">|; 

$key = q/C_iinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img144.svg"
 ALT="$C_i \in \mathcal{T}$">|; 

$key = q/C_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img87.svg"
 ALT="$C_{ij}$">|; 

$key = q/C_{ij}=A_icupB_jinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img79.svg"
 ALT="$C_{ij} = A_i \cup B_j \in \mathcal{T}$">|; 

$key = q/C_{ij}neemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img96.svg"
 ALT="$C_{ij} \ne \emptyset$">|; 

$key = q/C_{kl}capC_{ij}=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img99.svg"
 ALT="$C_{kl} \cap C_{ij} = \emptyset$">|; 

$key = q/M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img30.svg"
 ALT="$M$">|; 

$key = q/McupN={1,...,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$M \cup N = \{1,...,n\}$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$N$">|; 

$key = q/P={1,...,m}times{1,...,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$P = \{1,...,m\} \times \{1,...,n\}$">|; 

$key = q/Phiinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\Phi \in \mathcal{T}$">|; 

$key = q/U_i,V_iinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img155.svg"
 ALT="$U_i,V_i \in \mathcal{T}$">|; 

$key = q/W_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img127.svg"
 ALT="$W_{ij}$">|; 

$key = q/W_{ij}=U_icapV_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img119.svg"
 ALT="$W_{ij} = U_i \cap V_j$">|; 

$key = q/W_{ij}=U_icupV_jinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img159.svg"
 ALT="$W_{ij} = U_i \cup V_j \in \mathcal{T}$">|; 

$key = q/a_i,b_jinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img76.svg"
 ALT="$a_i,b_j \in \setR$">|; 

$key = q/a_i-b_j=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img103.svg"
 ALT="$a_i - b_j = 0$">|; 

$key = q/a_i=b_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img104.svg"
 ALT="$a_i = b_j$">|; 

$key = q/ainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$a \in \setR$">|; 

$key = q/alpha,betainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img162.svg"
 ALT="$\alpha,\beta \in \setR$">|; 

$key = q/cinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img61.svg"
 ALT="$c \in \setR$">|; 

$key = q/displaystyle(a_i-b_j)cdotindicatrice[C_{ij}](x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle (a_i - b_j) \cdot \indicatrice[C_{ij}](x) = 0$">|; 

$key = q/displaystyle-w(x)=-sum_{i=1}^nw_icdotindicatrice_{A_i}(x)=sum_{i=1}^n(-w_i)cdotindicatrice_{A_i}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img183.svg"
 ALT="$\displaystyle -w(x) = - \sum_{i = 1}^n w_i \cdot \indicatrice_{A_i}(x) = \sum_{i = 1}^n (-w_i) \cdot \indicatrice_{A_i}(x)$">|; 

$key = q/displaystyleA_icapB_i=C_icapAcapC_icapB=C_icap(AcapB)=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img149.svg"
 ALT="$\displaystyle A_i \cap B_i = C_i \cap A \cap C_i \cap B = C_i \cap (A \cap B) = \emptyset$">|; 

$key = q/displaystyleAcapB=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.06ex; vertical-align: -0.23ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle A \cap B = \emptyset$">|; 

$key = q/displaystyleAsetminusZ=Asetminusbigcup_{iinN}A_i=bigcup_{iinM}A_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle A \setminus Z = A \setminus \bigcup_{i \in N} A_i = \bigcup_{i \in M} A_i$">|; 

$key = q/displaystyleB_icapB_j=(A_icapB)cap(A_jcapB)=(A_icapA_j)capB=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle B_i \cap B_j = (A_i \cap B) \cap (A_j \cap B) = (A_i \cap A_j) \cap B = \emptyset$">|; 

$key = q/displaystyleC_i=C_icap(AcupB)=(C_icapA)cup(C_icapB)=A_icupB_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img150.svg"
 ALT="$\displaystyle C_i = C_i \cap (A \cup B) = (C_i \cap A) \cup (C_i \cap B) = A_i \cup B_i$">|; 

$key = q/displaystyleC_{ij}capC_{kl}=A_icapB_jcapA_kcapB_l=(A_icapA_k)cap(B_jcapB_l)=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle C_{ij} \cap C_{kl} = A_i \cap B_j \cap A_k \cap B_l = (A_i \cap A_k) \cap (B_j \cap B_l) = \emptyset$">|; 

$key = q/displaystyleF={(i,j)inP:C_{ij}neemptyset};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle F = \{ (i,j) \in P : C_{ij} \ne \emptyset \}$">|; 

$key = q/displaystyleF_i={xinA_i:f(x)strictsuperieura-w_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img177.svg"
 ALT="$\displaystyle F_i = \{ x \in A_i : f(x) \strictsuperieur a - w_i \}$">|; 

$key = q/displaystyleI=sum_ia_icdotmu(A_i)=sum_{(i,j)inP}a_icdotmu(C_{ij});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.06ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle I = \sum_i a_i \cdot \mu(A_i) = \sum_{(i,j) \in P} a_i \cdot \mu(C_{ij})$">|; 

$key = q/displaystyleI=sum_{(i,j)inF}a_icdotmu(C_{ij});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.06ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle I = \sum_{(i,j) \in F} a_i \cdot \mu(C_{ij})$">|; 

$key = q/displaystyleJ=sum_jb_jcdotmu(B_j)=sum_{(i,j)inP}b_jcdotmu(C_{ij})=sum_{(i,j)inF}b_jcdotmu(C_{ij});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.06ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle J = \sum_j b_j \cdot \mu(B_j) = \sum_{(i,j) \in P} b_j \cdot \mu(C_{ij}) = \sum_{(i,j) \in F} b_j \cdot \mu(C_{ij})$">|; 

$key = q/displaystyleJ=sum_{(i,j)inF}a_icdotmu(C_{ij})=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.06ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle J = \sum_{(i,j) \in F} a_i \cdot \mu(C_{ij}) = I$">|; 

$key = q/displaystyleM={(i,j)inP:mu(W_{ij})strictsuperieur0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\displaystyle M = \{ (i,j) \in P : \mu(W_{ij}) \strictsuperieur 0 \}$">|; 

$key = q/displaystyleM={iin{1,...,n}:mu(A_i)strictsuperieur0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle M = \{ i \in \{1,...,n\} : \mu(A_i) \strictsuperieur 0 \}$">|; 

$key = q/displaystyleN={iin{1,...,n}:mu(A_i)=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle N = \{ i \in \{1,...,n\} : \mu(A_i) = 0 \}$">|; 

$key = q/displaystyleZ=bigcup_{iinN}A_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle Z = \bigcup_{i \in N} A_i$">|; 

$key = q/displaystylebigcup_iB_i=bigcup_i(A_icapB)=Bcapbigcup_iA_i=BcapA=B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \bigcup_i B_i = \bigcup_i (A_i \cap B) = B \cap \bigcup_i A_i = B \cap A = B$">|; 

$key = q/displaystylebigcup_{i,j}C_{ij}=bigcup_iA_i=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle \bigcup_{i,j} C_{ij} = \bigcup_i A_i = A$">|; 

$key = q/displaystyleinfessentiel_{xinA}w(x)=lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \infessentiel_{x \in A} w(x) = \lambda$">|; 

$key = q/displaystyleinfessentiel_{xinA}w(x)essinferieurwessinferieursupessentiel_{xinA}w(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle \infessentiel_{x \in A} w(x) \essinferieur w \essinferieur \supessentiel_{x \in A} w(x)$">|; 

$key = q/displaystyleinfessentiel_{xinA}w(x)gelambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \infessentiel_{x \in A} w(x) \ge \lambda$">|; 

$key = q/displaystyleinfessentiel_{xinA}w(x)lelambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle \infessentiel_{x \in A} w(x) \le \lambda$">|; 

$key = q/displaystyleint_A(alphacdotu(x)+betacdotv(x))dmu(x)=alphacdotint_Au(x)dmu(x)+betacdotint_Av(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img168.svg"
 ALT="$\displaystyle \int_A (\alpha \cdot u(x) + \beta \cdot v(x))  d\mu(x) = \alpha \cdot \int_A u(x)  d\mu(x) + \beta \cdot \int_A v(x)  d\mu(x)$">|; 

$key = q/displaystyleint_A0dmu(x)=0cdotint_Admu(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle \int_A 0  d\mu(x) = 0 \cdot \int_A d\mu(x) = 0$">|; 

$key = q/displaystyleint_Acdmu(x)=ccdotint_Admu(x)=ccdotmu(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \int_A c  d\mu(x) = c \cdot \int_A d\mu(x) = c \cdot \mu(A)$">|; 

$key = q/displaystyleint_Admu(x)=mu(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle \int_A d\mu(x) = \mu(A)$">|; 

$key = q/displaystyleint_Au(x)dmu(x)=int_Av(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\displaystyle \int_A u(x)  d\mu(x) = \int_A v(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Au(x)dmu(x)=sum_{i,j}u_icdotmu(W_{ij})=sum_{(i,j)inM}u_icdotmu(W_{ij});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.78ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\displaystyle \int_A u(x)  d\mu(x) = \sum_{i,j} u_i \cdot \mu(W_{ij}) = \sum_{(i,j) \in M} u_i \cdot \mu(W_{ij})$">|; 

$key = q/displaystyleint_Au(x)dmu(x)leint_Av(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle \int_A u(x)  d\mu(x) \le \int_A v(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Av(x)dmu(x)=sum_{i,j}v_jcdotmu(W_{ij})=sum_{(i,j)inM}v_jcdotmu(W_{ij});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.78ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\displaystyle \int_A v(x)  d\mu(x) = \sum_{i,j} v_j \cdot \mu(W_{ij}) = \sum_{(i,j) \in M} v_j \cdot \mu(W_{ij})$">|; 

$key = q/displaystyleint_Aw(x)dmu(x)=infleft{int_Av(x)dmu(x):vinetagee(A),vesssuperieurwright};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\displaystyle \int_A w(x)  d\mu(x) = \inf \left\{ \int_A v(x)  d\mu(x) : v \in \etagee(A),  v \esssuperieur w \right\}$">|; 

$key = q/displaystyleint_Aw(x)dmu(x)=sum_{i=1}^nint_{A_i}w(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \int_A w(x)  d\mu(x) = \sum_{i = 1}^n \int_{A_i} w(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Aw(x)dmu(x)=sum_{i=1}^nw_icdotmu(A_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \int_A w(x)  d\mu(x) = \sum_{i = 1}^n w_i \cdot \mu(A_i)$">|; 

$key = q/displaystyleint_Aw(x)dmu(x)=sum_{i=1}^nw_icdotmu(A_i)=sum_{i=1}^nw_icdot0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img173.svg"
 ALT="$\displaystyle \int_A w(x)  d\mu(x) = \sum_{i = 1}^n w_i \cdot \mu(A_i) = \sum_{i = 1}^n w_i \cdot 0 = 0$">|; 

$key = q/displaystyleint_Aw(x)dmu(x)=supleft{int_Au(x)dmu(x):uinetagee(A),uessinferieurwright};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle \int_A w(x)  d\mu(x) = \sup \left\{ \int_A u(x)  d\mu(x) : u \in \etagee(A),  u \essinferieur w \right\}$">|; 

$key = q/displaystyleint_{A_i}w(x)dmu(x)=int_{A_i}w_idmu(x)=w_icdotmu(A_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -2.43ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \int_{A_i} w(x)  d\mu(x) = \int_{A_i} w_i  d\mu(x) = w_i \cdot \mu(A_i)$">|; 

$key = q/displaystylemax{u,v}(x)=sum_{i,j}max{u_i,v_j}cdotindicatrice[W_{ij}](x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img169.svg"
 ALT="$\displaystyle \max\{u,v\}(x) = \sum_{i,j} \max\{u_i,v_j\} \cdot \indicatrice[W_{ij}](x)$">|; 

$key = q/displaystylemin_{iinM}w_ilew(x)in{w_i:iinM}lemax_{iinM}w_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \min_{i \in M} w_i \le w(x) \in \{ w_i : i \in M \} \le \max_{i \in M} w_i$">|; 

$key = q/displaystylemin{u,v}(x)=sum_{i,j}min{u_i,v_j}cdotindicatrice[W_{ij}](x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img170.svg"
 ALT="$\displaystyle \min\{u,v\}(x) = \sum_{i,j} \min\{u_i,v_j\} \cdot \indicatrice[W_{ij}](x)$">|; 

$key = q/displaystylemin{w_i:iinM}essinferieurwessinferieurmax{w_i:iinM};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \min \{ w_i : i \in M \} \essinferieur w \essinferieur \max \{ w_i : i \in M \}$">|; 

$key = q/displaystylemu(A)cdotinfessentiel_{xinA}w(x)leint_Aw(x)dmu(x)lemu(A)cdotsupessentiel_{xinA}w(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.56ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle \mu(A) \cdot \infessentiel_{x \in A} w(x) \le \int_A w(x)  d\mu(x) \le \mu(A) \cdot \supessentiel_{x \in A} w(x)$">|; 

$key = q/displaystylesum_{(i,j)inP}(a_i-b_j)cdotindicatrice[C_{ij}](x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.06ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle \sum_{(i,j) \in P} (a_i - b_j) \cdot \indicatrice[C_{ij}](x) = 0$">|; 

$key = q/displaystylesum_{(k,l)inP}(a_k-b_l)cdotindicatrice[C_{kl}](x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.06ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle \sum_{(k,l) \in P} (a_k - b_l) \cdot \indicatrice[C_{kl}](x) = 0$">|; 

$key = q/displaystylesupessentiel_{xinA}w(x)=sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \supessentiel_{x \in A} w(x) = \sigma$">|; 

$key = q/displaystylesupessentiel_{xinA}w(x)gesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \supessentiel_{x \in A} w(x) \ge \sigma$">|; 

$key = q/displaystylesupessentiel_{xinA}w(x)lesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \supessentiel_{x \in A} w(x) \le \sigma$">|; 

$key = q/displaystyleu(x)=w(x)cdotindicatrice_B(x)=sum_{i=1}^nw_icdotindicatrice_{B_i}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle u(x) = w(x) \cdot \indicatrice_B(x) = \sum_{i = 1}^n w_i \cdot \indicatrice_{B_i}(x)$">|; 

$key = q/displaystyleu=0cdotindicatrice[AsetminusB]+sum_{i=1}^nw_icdotindicatrice_{B_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle u = 0 \cdot \indicatrice[A \setminus B] + \sum_{i = 1}^n w_i \cdot \indicatrice_{B_i}$">|; 

$key = q/displaystylew(x)=alphacdotu(x)+betacdotv(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img164.svg"
 ALT="$\displaystyle w(x) = \alpha \cdot u(x) + \beta \cdot v(x)$">|; 

$key = q/displaystylew(x)=sum_iw_icdotindicatrice_{C_i}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\displaystyle w(x) = \sum_i w_i \cdot \indicatrice_{C_i}(x)$">|; 

$key = q/displaystylew(x)=sum_{(i,j)inP}a_icdotindicatrice[C_{ij}](x)=sum_{(i,j)inP}b_jcdotindicatrice[C_{ij}](x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.06ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle w(x) = \sum_{(i,j) \in P} a_i \cdot \indicatrice[C_{ij}](x) = \sum_{(i,j) \in P} b_j \cdot \indicatrice[C_{ij}](x)$">|; 

$key = q/displaystylew(x)=sum_{i,j}(alphacdotu_i+betacdotv_j)cdotindicatrice[W_{ij}](x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img165.svg"
 ALT="$\displaystyle w(x) = \sum_{i,j} (\alpha \cdot u_i + \beta \cdot v_j) \cdot \indicatrice[W_{ij}](x)$">|; 

$key = q/displaystylew(x)=sum_{i=1}^ma_icdotindicatrice[A_i](x)=sum_{j=1}^nb_jcdotindicatrice[B_j](x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle w(x) = \sum_{i = 1}^m a_i \cdot \indicatrice[A_i](x) = \sum_{j = 1}^n b_j \cdot \indicatrice[B_j](x)$">|; 

$key = q/displaystylew(x)=sum_{i=1}^nw_icdotindicatrice_{A_i}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle w(x) = \sum_{i = 1}^n w_i \cdot \indicatrice_{A_i}(x)$">|; 

$key = q/displaystyle{xinA:f(x)+w(x)strictinferieura}=bigcup_{i=1}^n{xinA_i:f(x)strictinferieura-w_i}inmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img180.svg"
 ALT="$\displaystyle \{ x \in A : f(x) + w(x) \strictinferieur a \} = \bigcup_{i = 1}^n \{ x \in A_i : f(x) \strictinferieur a - w_i \} \in \mathcal{T}$">|; 

$key = q/displaystyle{xinA:f(x)+w(x)strictsuperieura}=bigcup_{i=1}^nF_iinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img179.svg"
 ALT="$\displaystyle \{ x \in A : f(x) + w(x) \strictsuperieur a \} = \bigcup_{i = 1}^n F_i \in \mathcal{T}$">|; 

$key = q/displaystyle{xinA:w(x)strictinferieura}=bigcup{A_i:iin{1,...,n},w_istrictinferieura}inmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.90ex; vertical-align: -1.37ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \{ x \in A : w(x) \strictinferieur a \} = \bigcup \{ A_i : i \in \{1,...,n\},  w_i \strictinferieur a \} \in \mathcal{T}$">|; 

$key = q/displaystyle{xinA:w(x)strictsuperieura}=bigcup{A_i:iin{1,...,n},w_istrictsuperieura};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.90ex; vertical-align: -1.37ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \{ x \in A : w(x) \strictsuperieur a \} = \bigcup \{ A_i : i \in \{1,...,n\},  w_i \strictsuperieur a \}$">|; 

$key = q/displaystyle{xinA:w(x)strictsuperieura}inmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \{ x \in A : w(x) \strictsuperieur a \} \in \mathcal{T}$">|; 

$key = q/etagee(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\etagee(A)$">|; 

$key = q/f+w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img181.svg"
 ALT="$f + w$">|; 

$key = q/f-w=f+(-w);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img184.svg"
 ALT="$f - w = f + (-w)$">|; 

$key = q/f:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img174.svg"
 ALT="$f : A \mapsto \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img176.svg"
 ALT="$f$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img89.svg"
 ALT="$i$">|; 

$key = q/indicatrice[C_i]=indicatrice[A_i]+indicatrice[B_i];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\indicatrice[C_i] = \indicatrice[A_i] + \indicatrice[B_i]$">|; 

$key = q/indicatrice[C_{ij}]ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\indicatrice[C_{ij}] \ne 0$">|; 

$key = q/indicatrice[C_{kl}](x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\indicatrice[C_{kl}](x) = 0$">|; 

$key = q/inej;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img21.svg"
 ALT="$i \ne j$">|; 

$key = q/inek;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img81.svg"
 ALT="$i \ne k$">|; 

$key = q/j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img88.svg"
 ALT="$j$">|; 

$key = q/jnel;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img82.svg"
 ALT="$j \ne l$">|; 

$key = q/mathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img178.svg"
 ALT="$\mathcal{T}$">|; 

$key = q/mu(A)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img171.svg"
 ALT="$\mu(A) = 0$">|; 

$key = q/mu(A_alpha)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\mu(A_\alpha) \strictsuperieur 0$">|; 

$key = q/mu(A_beta)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\mu(A_\beta) \strictsuperieur 0$">|; 

$key = q/mu(A_i)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img172.svg"
 ALT="$\mu(A_i) = 0$">|; 

$key = q/mu(C_{ij})=mu(emptyset)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\mu(C_{ij}) = \mu(\emptyset) = 0$">|; 

$key = q/mu(W_{ij})strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\mu(W_{ij}) \strictsuperieur 0$">|; 

$key = q/mu(Z)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\mu(Z) = 0$">|; 

$key = q/mu:mathcal{T}mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\mu : \mathcal{T} \mapsto \setR$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img166.svg"
 ALT="$\setR$">|; 

$key = q/u,v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img132.svg"
 ALT="$u,v$">|; 

$key = q/u,vinetagee(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$u,v \in \etagee(A)$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img26.svg"
 ALT="$u$">|; 

$key = q/u=wcdotindicatrice_B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img19.svg"
 ALT="$u = w \cdot \indicatrice_B$">|; 

$key = q/u=wessinferieurw;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img139.svg"
 ALT="$u = w \essinferieur w$">|; 

$key = q/u_i,v_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img117.svg"
 ALT="$u_i,v_j$">|; 

$key = q/u_i,v_jinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img154.svg"
 ALT="$u_i,v_j \in \setR$">|; 

$key = q/u_ilev_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img124.svg"
 ALT="$u_i \le v_j$">|; 

$key = q/uessegalv;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.27ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img133.svg"
 ALT="$u \essegal v$">|; 

$key = q/uessinferieurv;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img114.svg"
 ALT="$u \essinferieur v$">|; 

$key = q/uessinferieurw;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img138.svg"
 ALT="$u \essinferieur w$">|; 

$key = q/uesssuperieurv;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img134.svg"
 ALT="$u \esssuperieur v$">|; 

$key = q/v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img123.svg"
 ALT="$v$">|; 

$key = q/w:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img163.svg"
 ALT="$w : A \mapsto \setR$">|; 

$key = q/w:PhitosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$w : \Phi \to \setR$">|; 

$key = q/w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img68.svg"
 ALT="$w$">|; 

$key = q/w=w_alpha=sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img45.svg"
 ALT="$w = w_\alpha = \sigma$">|; 

$key = q/w=w_beta=lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img53.svg"
 ALT="$w = w_\beta =\lambda$">|; 

$key = q/w_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img69.svg"
 ALT="$w_i$">|; 

$key = q/w_iinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img8.svg"
 ALT="$w_i \in \setR$">|; 

$key = q/wessinferieursigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img43.svg"
 ALT="$w \essinferieur \sigma$">|; 

$key = q/wesssuperieurlambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img51.svg"
 ALT="$w \esssuperieur \lambda$">|; 

$key = q/wgesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img47.svg"
 ALT="$w \ge \sigma$">|; 

$key = q/winetagee(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$w \in \etagee(A)$">|; 

$key = q/winetagee(AcupB);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img143.svg"
 ALT="$w \in \etagee(A \cup B)$">|; 

$key = q/wlelambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img55.svg"
 ALT="$w \le \lambda$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img10.svg"
 ALT="$x \in A$">|; 

$key = q/xinAsetminusZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$x \in A \setminus Z$">|; 

$key = q/xinC_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img97.svg"
 ALT="$x \in C_{ij}$">|; 

$key = q/xinW_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img122.svg"
 ALT="$x \in W_{ij}$">|; 

$key = q/{A_1,...,A_n}subseteqmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\{ A_1, ..., A_n \} \subseteq \mathcal{T}$">|; 

$key = q/{A_1,A_2,...,A_m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\{A_1,A_2,...,A_m\}$">|; 

$key = q/{B_1,B_2,...,B_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\{B_1,B_2,...,B_n\}$">|; 

$key = q/{Eqts}alpha=argmax_{iinM}w_iinMbeta=argmin_{iinM}w_iinM{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.42ex; vertical-align: -3.64ex; " SRC="|."$dir".q|img41.svg"
 ALT="\begin{Eqts}
\alpha = \arg\max_{i \in M} w_i \in M \\\\
\beta = \arg\min_{i \in M} w_i \in M
\end{Eqts}">|; 

$key = q/{Eqts}bigcup_jC_{ij}=bigcup_j(A_icapB_j)=A_icapbigcup_jB_j=A_icapA=A_ibigcup_iC_{ij}=bigcup_i(A_icapB_j)=B_jcapbigcup_iA_i=AcapB_j=B_j{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.95ex; vertical-align: -5.41ex; " SRC="|."$dir".q|img84.svg"
 ALT="\begin{Eqts}
\bigcup_j C_{ij} = \bigcup_j (A_i \cap B_j) = A_i \cap \bigcup_j B_...
... \bigcup_i (A_i \cap B_j) = B_j \cap \bigcup_i A_i = A \cap B_j = B_j
\end{Eqts}">|; 

$key = q/{Eqts}indicatrice[A_i]=sum_jindicatrice[C_{ij}]indicatrice[B_j]=sum_iindicatrice[C_{ij}]{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.95ex; vertical-align: -5.41ex; " SRC="|."$dir".q|img92.svg"
 ALT="\begin{Eqts}
\indicatrice[A_i] = \sum_j \indicatrice[C_{ij}] \\\\
\indicatrice[B_j] = \sum_i \indicatrice[C_{ij}]
\end{Eqts}">|; 

$key = q/{Eqts}indicatrice[U_i]=sum_jindicatrice[W_{ij}]indicatrice[V_j]=sum_iindicatrice[W_{ij}]{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.95ex; vertical-align: -5.41ex; " SRC="|."$dir".q|img161.svg"
 ALT="\begin{Eqts}
\indicatrice[U_i] = \sum_j \indicatrice[W_{ij}] \\\\
\indicatrice[V_j] = \sum_i \indicatrice[W_{ij}]
\end{Eqts}">|; 

$key = q/{Eqts}mu(A_i)=sum_jmu(C_{ij})mu(B_j)=sum_imu(C_{ij}){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.95ex; vertical-align: -5.41ex; " SRC="|."$dir".q|img91.svg"
 ALT="\begin{Eqts}
\mu(A_i) = \sum_j \mu(C_{ij}) \\\\
\mu(B_j) = \sum_i \mu(C_{ij})
\end{Eqts}">|; 

$key = q/{Eqts}mu(U_i)=sum_jmu(W_{ij})mu(V_j)=sum_imu(W_{ij}){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.95ex; vertical-align: -5.41ex; " SRC="|."$dir".q|img160.svg"
 ALT="\begin{Eqts}
\mu(U_i) = \sum_j \mu(W_{ij}) \\\\
\mu(V_j) = \sum_i \mu(W_{ij})
\end{Eqts}">|; 

$key = q/{Eqts}sigma=w_alpha=max{w_i:iinM}lambda=w_beta=min{w_i:iinM}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img42.svg"
 ALT="\begin{Eqts}
\sigma = w_\alpha = \max \{ w_i : i \in M \} \\\\
\lambda = w_\beta = \min \{ w_i : i \in M \}
\end{Eqts}">|; 

$key = q/{Eqts}supessentiel_{xinA}w(x)=max{w_i:iin{1,...,n},mu(A_i)strictsuperieur0}infessentiel_{xinA}w(x)=min{w_i:iin{1,...,n},mu(A_i)strictsuperieur0}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.24ex; vertical-align: -5.56ex; " SRC="|."$dir".q|img59.svg"
 ALT="\begin{Eqts}
\supessentiel_{x \in A} w(x) = \max \{ w_i : i \in \{1,...,n\},  \...
...) = \min \{ w_i : i \in \{1,...,n\},  \mu(A_i) \strictsuperieur 0 \}
\end{Eqts}">|; 

$key = q/{Eqts}u(x)=sum_{i,j}u_icdotindicatrice[W_{ij}](x)v(x)=sum_{i,j}v_jcdotindicatrice[W_{ij}](x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.25ex; vertical-align: -5.56ex; " SRC="|."$dir".q|img120.svg"
 ALT="\begin{Eqts}
u(x) = \sum_{i,j} u_i \cdot \indicatrice[W_{ij}](x) \\\\
v(x) = \sum_{i,j} v_j \cdot \indicatrice[W_{ij}](x)
\end{Eqts}">|; 

$key = q/{Eqts}u(x)=sum_{i=1}^mu_icdotindicatrice_{U_i}(x)v(x)=sum_{j=1}^nv_jcdotindicatrice_{V_j}(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.66ex; vertical-align: -6.78ex; " SRC="|."$dir".q|img118.svg"
 ALT="\begin{Eqts}
u(x) = \sum_{i = 1}^m u_i \cdot \indicatrice_{U_i}(x) \\\\
v(x) = \sum_{j = 1}^n v_j \cdot \indicatrice_{V_j}(x)
\end{Eqts}">|; 

$key = q/{Eqts}u(x)=sum_{i=1}^nu_icdotindicatrice[U_i](x)v(x)=sum_{j=1}^mv_jcdotindicatrice[V_j](x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.66ex; vertical-align: -6.78ex; " SRC="|."$dir".q|img158.svg"
 ALT="\begin{Eqts}
u(x) = \sum_{i = 1}^n u_i \cdot \indicatrice[U_i](x) \\\\
v(x) = \sum_{j = 1}^m v_j \cdot \indicatrice[V_j](x)
\end{Eqts}">|; 

$key = q/{U_1,...,U_m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\{U_1,...,U_m\}$">|; 

$key = q/{U_1,...,U_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\{U_1,...,U_n\}$">|; 

$key = q/{V_1,...,V_m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\{V_1,...,V_m\}$">|; 

$key = q/{V_1,...,V_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\{V_1,...,V_n\}$">|; 

$key = q/{eqnarraystar}int_{AcupB}w(x)dmu(x)&=&sum_iw_icdotmu(A_i)+sum_iw_icdotmu(B_i)&=&int_Aw(x)dmu(x)+int_Bw(x)dmu(x){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 17.09ex; " SRC="|."$dir".q|img153.svg"
 ALT="\begin{eqnarray*}
\int_{A \cup B} w(x)  d\mu(x) &amp;=&amp; \sum_i w_i \cdot \mu(A_i) +...
...t \mu(B_i) \\\\
&amp;=&amp; \int_A w(x)  d\mu(x) + \int_B w(x)  d\mu(x)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}w&=&sum_iw_icdot(indicatrice[A_i]+indicatrice[B_i])&=&sum_iw_icdotindicatrice[A_i]+sum_iw_icdotindicatrice[B_i]{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.64ex; " SRC="|."$dir".q|img152.svg"
 ALT="\begin{eqnarray*}
w &amp;=&amp; \sum_i w_i \cdot (\indicatrice[A_i] + \indicatrice[B_i])...
...w_i \cdot \indicatrice[A_i] + \sum_i w_i \cdot \indicatrice[B_i]
\end{eqnarray*}">|; 

1;

