# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.32ex; " SRC="|."$dir".q|img39.svg"
 ALT="\begin{eqnarray*}
\scalaire{A^\dual(\alpha \cdot x + \beta \cdot y)}{u} &amp;=&amp; \sca...
...=&amp;\scalaire{\alpha \cdot A^\dual(x) + \beta \cdot A^\dual(y)}{u}
\end{eqnarray*}">|; 

$key = q/(A^dual)^dual=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$(A^\dual)^\dual = A$">|; 

$key = q/(imageA^dual)^orthogonalsubseteqnoyauA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$(\image A^\dual)^\orthogonal \subseteq \noyau A$">|; 

$key = q/(u,v)inEtimesF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$(u,v) \in E \times F$">|; 

$key = q/(u,v)inEtimesG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$(u,v) \in E \times G$">|; 

$key = q/A(u)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$A(u) = 0$">|; 

$key = q/A,B:EmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img19.svg"
 ALT="$A,B : E \mapsto F$">|; 

$key = q/A:EmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$A : E \mapsto F$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$A$">|; 

$key = q/A=A^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$A = A^\dual$">|; 

$key = q/A^dual:F^dualmapstoE^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$A^\dual : F^\dual \mapsto E^\dual$">|; 

$key = q/A^dual:FmapstoE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img12.svg"
 ALT="$A^\dual : F \mapsto E$">|; 

$key = q/A^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$A^\dual$">|; 

$key = q/A^dualcircA:EmapstoE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img31.svg"
 ALT="$A^\dual \circ A : E \mapsto E$">|; 

$key = q/A^dualinmatrice(corps,n,m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$A^\dual \in \matrice(\corps,n,m)$">|; 

$key = q/AcircA^dual:FmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img35.svg"
 ALT="$A \circ A^\dual : F \mapsto F$">|; 

$key = q/Ainmatrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img69.svg"
 ALT="$A \in \matrice(\corps,m,n)$">|; 

$key = q/B:FmapstoG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$B : F \mapsto G$">|; 

$key = q/Binmatrice(corps,n,p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img80.svg"
 ALT="$B \in \matrice(\corps,n,p)$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$E$">|; 

$key = q/E=F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$E = F$">|; 

$key = q/E^F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$E^F$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$F$">|; 

$key = q/G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img26.svg"
 ALT="$G$">|; 

$key = q/^dual:AmapstoA^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img23.svg"
 ALT="$^\dual : A \mapsto A^\dual$">|; 

$key = q/alpha,betaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\alpha,\beta \in \corps$">|; 

$key = q/conjaccent{A}=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\conjaccent{A} = A$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\corps$">|; 

$key = q/corpsin{setR,setC};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\corps \in \{ \setR , \setC \}$">|; 

$key = q/displaystyle(A^dual)^{-1}=(A^{-1})^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle (A^\dual)^{-1} = (A^{-1})^\dual$">|; 

$key = q/displaystyle(A^dualcircA)^dual=A^dualcircA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle (A^\dual \circ A)^\dual = A^\dual \circ A$">|; 

$key = q/displaystyle(AcdotB)^dual=B^dualcdotA^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle (A \cdot B)^\dual = B^\dual \cdot A^\dual$">|; 

$key = q/displaystyle(AcircA^dual)^dual=AcircA^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle (A \circ A^\dual)^\dual = A \circ A^\dual$">|; 

$key = q/displaystyle(AcircB)^dual=B^dualcircA^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle (A \circ B)^\dual = B^\dual \circ A^\dual$">|; 

$key = q/displaystyle(alphacdotA+betacdotB)^dual=conjaccent{alpha}cdotA^dual+conjaccent{beta}cdotB^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.84ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle (\alpha \cdot A + \beta \cdot B)^\dual = \conjaccent{\alpha} \cdot A^\dual + \conjaccent{\beta} \cdot B^\dual$">|; 

$key = q/displaystyleA^dual(alphacdotx+betacdoty)=alphacdotA^dual(x)+betacdotA^dual(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle A^\dual(\alpha \cdot x + \beta \cdot y) = \alpha \cdot A^\dual(x) + \beta \cdot A^\dual(y)$">|; 

$key = q/displaystyleA^dual=A^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.20ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle A^\dual = A^T$">|; 

$key = q/displaystyleA^dual=conjaccent{A^T}=conjugueA^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.83ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle A^\dual = \conjaccent{A^T} = \conjugue A^T$">|; 

$key = q/displaystylebig(conjaccent{A^dual}big)^T=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.41ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \big( \conjaccent{A^\dual} \big)^T = A$">|; 

$key = q/displaystyleforme{varphi}{A(u)}=forme{A^dual(varphi)}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \forme{\varphi}{A(u)} = \forme{A^\dual(\varphi)}{u}$">|; 

$key = q/displaystylefrac{norme{A^dual(x)}}{norme{x}}lenorme{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \frac{ \norme{A^\dual(x)} }{ \norme{x} } \le \norme{A}$">|; 

$key = q/displaystyleleft(A^dualright)^dual=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.62ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \left( A^\dual \right)^\dual = A$">|; 

$key = q/displaystylenorme{A^dual(x)}^2lenorme{A}cdotnorme{A^dual(x)}cdotnorme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \norme{A^\dual(x)}^2 \le \norme{A} \cdot \norme{A^\dual(x)} \cdot \norme{x}$">|; 

$key = q/displaystylenorme{A^dual(x)}lenorme{A}cdotnorme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \norme{A^\dual(x)} \le \norme{A} \cdot \norme{x}$">|; 

$key = q/displaystylenorme{A^dual}=norme{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \norme{A^\dual} = \norme{A}$">|; 

$key = q/displaystylenorme{A^dual}lenorme{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \norme{A^\dual} \le \norme{A}$">|; 

$key = q/displaystylenorme{A}=norme{(A^dual)^dual}lenorme{A^dual};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \norme{A} = \norme{(A^\dual)^\dual} \le \norme{A^\dual}$">|; 

$key = q/displaystylenoyauA=(imageA^dual)^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle \noyau A = (\image A^\dual)^\orthogonal$">|; 

$key = q/displaystylenoyauA^dual=(imageA)^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \noyau A^\dual = (\image A)^\orthogonal$">|; 

$key = q/displaystylescalaire{A(u)}{x}=scalaire{u}{A^dual(x)}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \scalaire{A(u)}{x} = \scalaire{u}{A^\dual(x)} = 0$">|; 

$key = q/displaystylescalaire{A^dualcircA(v)}{u}=scalaire{A(v)}{A(u)}=scalaire{v}{A^dualcircA(u)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \scalaire{A^\dual \circ A(v)}{u} = \scalaire{A(v)}{A(u)} = \scalaire{v}{A^\dual \circ A(u)}$">|; 

$key = q/displaystylescalaire{AcircA^dual(x)}{y}=scalaire{A^dual(x)}{A^dual(y)}=scalaire{x}{AcircA^dual(y)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \scalaire{A \circ A^\dual(x)}{y} =\scalaire{A^\dual(x)}{A^\dual(y)} = \scalaire{x}{A \circ A^\dual(y)}$">|; 

$key = q/displaystylescalaire{mathcal{A}^dual(y)}{x}=big(conjaccent{A^dual}cdotconjaccent{y}big)^Tcdotx=conjaccent{y^T}cdotbig(conjaccent{A^dual}big)^Tcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.41ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \scalaire{\mathcal{A}^\dual(y)}{ x } = \big( \conjaccent{A^\dual}...
...)^T \cdot x = \conjaccent{y^T} \cdot \big( \conjaccent{A^\dual} \big)^T \cdot x$">|; 

$key = q/displaystylescalaire{u}{A^dual(v)}=conjaccent{scalaire{A^dual(v)}{u}}=conjaccent{scalaire{v}{A(u)}}=scalaire{A(u)}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \scalaire{u}{A^\dual(v)} = \conjaccent{\scalaire{A^\dual(v)}{u}} = \conjaccent{\scalaire{v}{A(u)}} = \scalaire{A(u)}{v}$">|; 

$key = q/displaystylescalaire{u}{v}=scalaire{u}{A^dual(x)}=scalaire{A(u)}{x}=scalaire{0}{x}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \scalaire{u}{v} = \scalaire{u}{A^\dual(x)} = \scalaire{A(u)}{x} = \scalaire{0}{x} = 0$">|; 

$key = q/displaystylescalaire{v}{(AcircB)(u)}=scalaire{A^dual(v)}{B(u)}=scalaire{(B^dualcircA^dual)(v)}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \scalaire{v}{(A \circ B)(u)} = \scalaire{A^\dual(v)}{B(u)} = \scalaire{(B^\dual \circ A^\dual)(v)}{u}$">|; 

$key = q/displaystylescalaire{v}{A(u)}=scalaire{A^dual(v)}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \scalaire{v}{A(u)} = \scalaire{A^\dual(v)}{u}$">|; 

$key = q/displaystylescalaire{v}{identite(u)}=scalaire{identite(v)}{u}=scalaire{v}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \scalaire{v}{\identite(u)} = \scalaire{\identite(v)}{u} = \scalaire{v}{u}$">|; 

$key = q/displaystylescalaire{y}{mathcal{A}(x)}=conjaccent{y^T}cdotAcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.96ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle \scalaire{y}{ \mathcal{A}(x) } = \conjaccent{y^T} \cdot A \cdot x$">|; 

$key = q/identite^dual=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.91ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\identite^\dual = \identite$">|; 

$key = q/mathcal{A}:corps^nmapstocorps^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\mathcal{A} : \corps^n \mapsto \corps^m$">|; 

$key = q/mathcal{A}^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\mathcal{A}^\dual$">|; 

$key = q/norme{A^dual(x)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\norme{A^\dual(x)}$">|; 

$key = q/norme{A^dual(x)}=0lenorme{A}cdotnorme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\norme{A^\dual(x)} = 0 \le \norme{A} \cdot \norme{x}$">|; 

$key = q/norme{A^dual(x)}ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\norme{A^\dual(x)} \ne 0$">|; 

$key = q/noyauAsubseteq(imageA^dual)^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\noyau A \subseteq (\image A^\dual)^\orthogonal$">|; 

$key = q/u,vinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img33.svg"
 ALT="$u,v \in E$">|; 

$key = q/uin(imageA^dual)^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$u \in (\image A^\dual)^\orthogonal$">|; 

$key = q/uinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img9.svg"
 ALT="$u \in E$">|; 

$key = q/uinnoyauA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img56.svg"
 ALT="$u \in \noyau A$">|; 

$key = q/v=A^dual(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$v = A^\dual(x)$">|; 

$key = q/varphiinF^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\varphi \in F^\dual$">|; 

$key = q/vinimageA^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img57.svg"
 ALT="$v \in \image A^\dual$">|; 

$key = q/x,yinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img37.svg"
 ALT="$x,y \in F$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img48.svg"
 ALT="$x$">|; 

$key = q/xinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img63.svg"
 ALT="$x \in E$">|; 

$key = q/xinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img41.svg"
 ALT="$x \in F$">|; 

$key = q/{Eqts}identite=identite^dual=(A^{-1}circA)^dual=A^dualcirc(A^{-1})^dualidentite=identite^dual=(AcircA^{-1})^dual=(A^{-1})^dualcircA^dual{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img54.svg"
 ALT="\begin{Eqts}
\identite = \identite^\dual = (A^{-1} \circ A)^\dual = A^\dual \cir...
...dentite^\dual = (A \circ A^{-1})^\dual = (A^{-1})^\dual \circ A^\dual
\end{Eqts}">|; 

$key = q/{Eqts}norme{A^dual(x)}^2=scalaire{A^dual(x)}{A^dual(x)}=scalaire{AcircA^dual(x)}{x}lenorme{AcircA^dual(x)}cdotnorme{x}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.15ex; vertical-align: -1.00ex; " SRC="|."$dir".q|img42.svg"
 ALT="\begin{Eqts}
\norme{A^\dual(x)}^2 = \scalaire{A^\dual(x)}{A^\dual(x)} = \scalaire{A \circ A^\dual(x)}{x} \le \norme{A \circ A^\dual(x)} \cdot \norme{x}
\end{Eqts}">|; 

1;

