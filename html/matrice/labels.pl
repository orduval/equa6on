# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate labels original text with physical files.


$key = q/chap:matrices/;
$external_labels{$key} = "$URL/" . q|matrice.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2023 (Released January 1, 2023)
# labels from external_latex_labels array.


$key = q/chap:matrices/;
$external_latex_labels{$key} = q|1 Matrices|; 
$noresave{$key} = "$nosave";

1;

