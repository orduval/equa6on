# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.91ex; vertical-align: -8.91ex; " SRC="|."$dir".q|img19.svg"
 ALT="\begin{Eqts}
A =
\begin{Matrix}{cc}
B &amp; C \\\\
D &amp; E
\end{Matrix}=
\begin{Matrix}...
...s \\\\
d_{r1} &amp; \ldots &amp; d_{rn} &amp; e_{r1} &amp; \ldots &amp; e_{rp}
\end{Matrix}\end{Eqts}">|; 

$key = q/(1,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$(1,n)$">|; 

$key = q/(i,j)in{1,2,...,m}times{1,2,...,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$(i,j) \in \{ 1,2,...,m \} \times \{ 1,2,...,n \}$">|; 

$key = q/(m,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$(m,1)$">|; 

$key = q/(m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$(m,n)$">|; 

$key = q/(p,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$(p,1)$">|; 

$key = q/(x_1,x_2,...,x_n)incorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$(x_1,x_2,...,x_n) \in \corps^n$">|; 

$key = q/-A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img84.svg"
 ALT="$-A$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img80.svg"
 ALT="$0$">|; 

$key = q/0_{m,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.33ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img83.svg"
 ALT="$0_{m,n}$">|; 

$key = q/A,Binmatrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$A,B \in \matrice(\corps,m,n)$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img28.svg"
 ALT="$A$">|; 

$key = q/A=(a_{ij})_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img10.svg"
 ALT="$A = (a_{ij})_{i,j}$">|; 

$key = q/A=(a_{ij})_{i,j}inmatrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img33.svg"
 ALT="$A = (a_{ij})_{i,j} \in \matrice(\corps,m,n)$">|; 

$key = q/A=(a_{ij})_{i,j}inmatrice(setC,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img92.svg"
 ALT="$A = (a_{ij})_{i,j} \in \matrice(\setC,m,n)$">|; 

$key = q/A=B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img73.svg"
 ALT="$A = B$">|; 

$key = q/A^Tinmatrice(corps,n,m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$A^T \in \matrice(\corps,n,m)$">|; 

$key = q/Ainmatrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$A \in \matrice(\corps,m,n)$">|; 

$key = q/Ainmatrice(corps,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$A \in \matrice(\corps,n,n)$">|; 

$key = q/AleB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img76.svg"
 ALT="$A \le B$">|; 

$key = q/B,C,D,E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img21.svg"
 ALT="$B,C,D,E$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$B$">|; 

$key = q/C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img23.svg"
 ALT="$C$">|; 

$key = q/D;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img24.svg"
 ALT="$D$">|; 

$key = q/Dinmatrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$D \in \matrice(\corps,m,n)$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$E$">|; 

$key = q/a_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img4.svg"
 ALT="$a_{ij}$">|; 

$key = q/b_{ij},c_{ij},d_{ij},e_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img20.svg"
 ALT="$b_{ij},c_{ij},d_{ij},e_{ij}$">|; 

$key = q/betaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\beta \in \corps$">|; 

$key = q/bloc_{ijkl};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\bloc_{ijkl}$">|; 

$key = q/c_iinmatrice(corps,m,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$c_i \in \matrice(\corps,m,1)$">|; 

$key = q/cdot:corpstimesmatrice(corps,m,n)tomatrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\cdot : \corps \times \matrice(\corps,m,n) \to \matrice(\corps,m,n)$">|; 

$key = q/composante_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.04ex; vertical-align: -0.95ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\composante_{ij}$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\corps$">|; 

$key = q/corps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\corps^n$">|; 

$key = q/d=(d_i)_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$d = (d_i)_i$">|; 

$key = q/displaystyle-A=(-a_{ij})_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle -A = (-a_{ij})_{i,j}$">|; 

$key = q/displaystyle0=(0)_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle 0 = ( 0 )_{i,j}$">|; 

$key = q/displaystyleA+(-A)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle A + (-A) = 0$">|; 

$key = q/displaystyleA+0=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.95ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle A + 0 = A$">|; 

$key = q/displaystyleA+B=B+A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.95ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle A + B = B + A$">|; 

$key = q/displaystyleA=(a_{ij})_{i,j}=[a_{ij}]_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle A = ( a_{ij} )_{i,j} = [ a_{ij} ]_{i,j}$">|; 

$key = q/displaystyleA=[c_1c_2hdotsc_n];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle A = [c_1  c_2 \hdots  c_n]$">|; 

$key = q/displaystyleD=(d_icdotindicatrice_{ij})_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle D = ( d_i \cdot \indicatrice_{ij} )_{i,j}$">|; 

$key = q/displaystyleD=diagonale_{m,n}(d)=diagonale_{m,n}(d_1,...,d_p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.30ex; vertical-align: -2.48ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle D = \diagonale_{m,n}(d) = \diagonale_{m,n}(d_1,...,d_p)$">|; 

$key = q/displaystyleT=(t_{ij}cdottau^+_{ij})_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.98ex; vertical-align: -0.99ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle T = (t_{ij} \cdot \tau^+_{ij})_{i,j}$">|; 

$key = q/displaystyleT=(t_{ij}cdottau^-_{ij})_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.98ex; vertical-align: -0.99ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle T = (t_{ij} \cdot \tau^-_{ij})_{i,j}$">|; 

$key = q/displaystylea_{ij}=b_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle a_{ij} = b_{ij}$">|; 

$key = q/displaystylea_{ij}=composante_{ij}A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.16ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle a_{ij} = \composante_{ij} A$">|; 

$key = q/displaystylea_{ij}leb_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle a_{ij} \le b_{ij}$">|; 

$key = q/displaystylebetacdotA=(betacdota_{ij})_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\displaystyle \beta \cdot A = (\beta \cdot a_{ij})_{i,j}$">|; 

$key = q/displaystylebig(A^Tbig)^T=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.41ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \big( A^T \big)^T = A$">|; 

$key = q/displaystylecolonne_iA=c_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.44ex; vertical-align: -1.74ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \colonne_i A = c_i$">|; 

$key = q/displaystyleconjugueA=bar{A}=(bar{a}_{ij})_{i,j}=(conjuguea_{ij})_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.73ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle \conjugue A = \bar{A} = ( \bar{a}_{ij} )_{i,j} = ( \conjugue a_{ij} )_{i,j}$">|; 

$key = q/displaystylediagonale_n(d)=diagonale_{n,n}(d);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.30ex; vertical-align: -2.48ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle \diagonale_n(d) = \diagonale_{n,n}(d)$">|; 

$key = q/displaystyleligne_iA=l_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \ligne_i A = l_i$">|; 

$key = q/displaystylex=(x_i)_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle x = (x_i)_i$">|; 

$key = q/displaystylex=[x_1x_2hdotsx_n];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle x = [x_1  x_2  \hdots  x_n]$">|; 

$key = q/displaystylex_i=composante_ix;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.28ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle x_i = \composante_i x$">|; 

$key = q/gammaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\gamma \in \corps$">|; 

$key = q/i^{ème};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img12.svg"
 ALT="$i^{ème}$">|; 

$key = q/igej;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img68.svg"
 ALT="$i \ge j$">|; 

$key = q/ilej;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img65.svg"
 ALT="$i \le j$">|; 

$key = q/j^{ème};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img13.svg"
 ALT="$j^{ème}$">|; 

$key = q/k^{ième};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.05ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$k^{ième}$">|; 

$key = q/l^{ième};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.05ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$l^{ième}$">|; 

$key = q/l_iinmatrice(corps,1,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$l_i \in \matrice(\corps,1,n)$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$m$">|; 

$key = q/m=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img63.svg"
 ALT="$m = n$">|; 

$key = q/matrice(corps,1,n),matrice(corps,n,1)equivcorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\matrice(\corps,1,n),\matrice(\corps,n,1) \equiv \corps^n$">|; 

$key = q/matrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\matrice(\corps,m,n)$">|; 

$key = q/mnen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img53.svg"
 ALT="$m \ne n$">|; 

$key = q/mstrictinferieurn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img54.svg"
 ALT="$m \strictinferieur n$">|; 

$key = q/mstrictsuperieurn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img55.svg"
 ALT="$m \strictsuperieur n$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$n$">|; 

$key = q/plemin{m,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$p \le \min \{ m , n \}$">|; 

$key = q/u^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img50.svg"
 ALT="$u^T$">|; 

$key = q/u^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img51.svg"
 ALT="$u^\dual$">|; 

$key = q/uincorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img48.svg"
 ALT="$u \in \corps^n$">|; 

$key = q/uinmatrice(corps,n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$u \in \matrice(\corps,n,1)$">|; 

$key = q/x'inmatrice(corps,n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$x' \in \matrice(\corps,n,1)$">|; 

$key = q/xinmatrice(corps,1,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$x \in \matrice(\corps,1,n)$">|; 

$key = q/{Eqts}A+B=(a_{ij}+b_{ij})_{i,j}A-B=(a_{ij}-b_{ij})_{i,j}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img78.svg"
 ALT="\begin{Eqts}
A + B = (a_{ij} + b_{ij})_{i,j} \\\\
A - B = (a_{ij} - b_{ij})_{i,j}
\end{Eqts}">|; 

$key = q/{Eqts}A=(a_{ij})_{i,j}B=(b_{ij})_{i,j}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img72.svg"
 ALT="\begin{Eqts}
A = ( a_{ij} )_{i,j} \\\\
B = ( b_{ij} )_{i,j}
\end{Eqts}">|; 

$key = q/{Eqts}A=Matrix{{cccc}a_{11}&a_{12}&ldots&a_{1n}a_{21}&a_{22}&ldots&a_{2n}vdots&&ddots&vdotsa_{m1}&a_{m2}&ldots&a_{mn}Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.69ex; vertical-align: -5.79ex; " SRC="|."$dir".q|img3.svg"
 ALT="\begin{Eqts}
A =
\begin{Matrix}{cccc}
a_{11} &amp; a_{12} &amp; \ldots &amp; a_{1n} \\\\
a_{2...
...&amp; &amp; \ddots &amp; \vdots \\\\
a_{m1} &amp; a_{m2} &amp; \ldots &amp; a_{mn}
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}A=Matrix{{c}l_1l_2vdotsl_mMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.69ex; vertical-align: -5.79ex; " SRC="|."$dir".q|img26.svg"
 ALT="\begin{Eqts}
A =
\begin{Matrix}{c}
l_1 \\\\
l_2 \\\\
\vdots \\\\
l_m
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}A^T=Matrix{{cccc}a_{11}&a_{21}&ldots&a_{m1}a_{12}&a_{22}&ldots&a_{m2}vdots&&ddots&vdotsa_{1n}&a_{2n}&ldots&a_{mn}Matrix{=(a_{ji})_{i,j}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.69ex; vertical-align: -5.79ex; " SRC="|."$dir".q|img34.svg"
 ALT="\begin{Eqts}
A^T =
\begin{Matrix}{cccc}
a_{11} &amp; a_{21} &amp; \ldots &amp; a_{m1} \\\\
a_...
...s \\\\
a_{1n} &amp; a_{2n} &amp; \ldots &amp; a_{mn}
\end{Matrix} = (a_{ji})_{i,j}
\end{Eqts}">|; 

$key = q/{Eqts}Acdotbeta=betacdotAgammacdotbetacdotA=(gammacdotbeta)cdotAbetaA=betacdotA{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img91.svg"
 ALT="\begin{Eqts}
A \cdot \beta = \beta \cdot A \\\\
\gamma \cdot \beta \cdot A = (\gamma \cdot \beta) \cdot A \\\\
\beta A = \beta \cdot A
\end{Eqts}">|; 

$key = q/{Eqts}composante_{ij}diagonale_{m,n}(d_1,...,d_p)=cases{d_i&text{si}i=jin{1,2,...,p}0&text{sinon}cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img62.svg"
 ALT="\begin{Eqts}
\composante_{ij} \diagonale_{m,n}(d_1,...,d_p) =
\begin{cases}
d_i &amp; \text{ si } i = j \in \{1,2,...,p\} \\\\
0 &amp; \text{ sinon}
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}tau^+_{ij}=cases{1&mbox{si}ilej0&mbox{si}istrictsuperieurjcases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img67.svg"
 ALT="\begin{Eqts}
\tau^+_{ij} =
\begin{cases}
1 &amp; \mbox{ si } i \le j \\\\
0 &amp; \mbox{ si } i \strictsuperieur j
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}tau^-_{ij}=cases{1&mbox{si}igej0&mbox{si}istrictinferieurjcases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img70.svg"
 ALT="\begin{Eqts}
\tau^-_{ij} =
\begin{cases}
1 &amp; \mbox{ si } i \ge j \\\\
0 &amp; \mbox{ si } i \strictinferieur j
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}x'=Matrix{{c}x_1x_2vdotsx_nMatrix{=[x_1x_2hdotsx_n]^T{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.69ex; vertical-align: -5.79ex; " SRC="|."$dir".q|img46.svg"
 ALT="\begin{Eqts}
x' =
\begin{Matrix}{c}
x_1 \\\\
x_2 \\\\
\vdots \\\\
x_n
\end{Matrix}=
[x_1  x_2  \hdots  x_n]^T
\end{Eqts}">|; 

1;

