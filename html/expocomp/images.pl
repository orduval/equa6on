# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img50.svg"
 ALT="\begin{Eqts}
\cos(u+v) + \img \sin(u+v) = \exp\left[ \img ( u + v ) \right] \\\\
...
...) + \img \sin(u+v) = (\cos(u) + \img \sin(u))(\cos(v) + \img \sin(v))
\end{Eqts}">|; 

$key = q/2pi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$2\pi$">|; 

$key = q/[0,2pi);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$[0,2\pi)$">|; 

$key = q/cos;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\cos$">|; 

$key = q/displaystyleOD{u}{t}=-sin(t)+imgcos(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \OD{u}{t} = -\sin(t) + \img \cos(t)$">|; 

$key = q/displaystyleexp(-imgt)=cos(t)-imgsin(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \exp(-\img t) = \cos(t) - \img \sin(t)$">|; 

$key = q/displaystyleexp(-imgt)cdotexp(it)=exp(it-it)=exp(0)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \exp(-\img t) \cdot \exp(i t) = \exp(i t - i t) = \exp(0) = 1$">|; 

$key = q/displaystyleexp(imgt)=cos(t)+imgsin(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \exp(\img t) = \cos(t) + \img \sin(t)$">|; 

$key = q/displaystyleexp(t+2pi)=exp(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \exp(t+2\pi) = \exp(t)$">|; 

$key = q/displaystyleexp(y)=z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \exp(y) = z$">|; 

$key = q/displaystyleexp(z)=exp(a)cdotexp(imgb)=exp(a)cdot(cos(b)+imgsin(b));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \exp(z) = \exp(a) \cdot \exp(\img b) = \exp(a) \cdot (\cos(b) + \img \sin(b))$">|; 

$key = q/displaystyleexp(z)=x(1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \exp(z) = x(1)$">|; 

$key = q/displaystyleexp(z_1+z_2)=exp(z_1)cdotexp(z_2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \exp(z_1 + z_2) = \exp(z_1) \cdot \exp(z_2)$">|; 

$key = q/displaystyleexp(zcdott)=x(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \exp( z \cdot t ) = x(t)$">|; 

$key = q/displaystylefrac{d^2u}{dt^2}(t)=-u(t)qquadu(0)=1qquadOD{u}{t}(0)=img;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \frac{d^2 u}{dt^2}(t) = - u(t) \qquad u(0) = 1 \qquad \OD{u}{t}(0) = \img$">|; 

$key = q/displaystyleln(xcdoty)=ln(x)+ln(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \ln(x \cdot y) = \ln(x) + \ln(y)$">|; 

$key = q/displaystylemathcal{Y}={y_k=ln(abs{z})+imgarg(z)+2piimgk:kinsetZ};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \mathcal{Y} = \{ y_k = \ln(\abs{z}) + \img \arg(z) + 2 \pi \img k : k \in \setZ \}$">|; 

$key = q/displaystyleu(t)=cos(t)+imgsin(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle u(t) = \cos(t) + \img \sin(t)$">|; 

$key = q/displaystylez=abs{z}(cos(theta)+imgsin(theta))=abs{z}exp(imgtheta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle z = \abs{z}(\cos(\theta)+\img\sin(\theta)) = \abs{z}\exp(\img\theta)$">|; 

$key = q/displaystylez=abs{z}exp(imgarg(z));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle z = \abs{z}\exp(\img\arg(z))$">|; 

$key = q/s(t)=p(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$s(t) = p(t)$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\setR$">|; 

$key = q/sin;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.73ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\sin$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$t$">|; 

$key = q/t=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$t=1$">|; 

$key = q/theta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\theta$">|; 

$key = q/thetainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\theta\in\setR$">|; 

$key = q/tinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img22.svg"
 ALT="$t\in\setR$">|; 

$key = q/u(t)=exp(-i.t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$u(t) = \exp(-i.t)$">|; 

$key = q/u(t)=exp(imgt);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$u(t) = \exp(\img t)$">|; 

$key = q/u:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$u : \setR \mapsto \setR$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$x$">|; 

$key = q/yinsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img42.svg"
 ALT="$y \in \setC$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img31.svg"
 ALT="$z$">|; 

$key = q/zinsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img3.svg"
 ALT="$z \in \setC$">|; 

$key = q/{Eqts}(cos(t)+imgsin(t))(cos(t)-imgsin(t))=1cos(t)^2+sin(t)^2=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img47.svg"
 ALT="\begin{Eqts}
( \cos(t) + \img \sin(t) ) ( \cos(t) - \img \sin(t) ) = 1 \\\\
\cos(t)^2 + \sin(t)^2 = 1
\end{Eqts}">|; 

$key = q/{Eqts}OD{p}{t}(t)=z_1p(t)+z_2p(t)=(z_1+z_2)p(t)p(0)=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.52ex; vertical-align: -3.70ex; " SRC="|."$dir".q|img8.svg"
 ALT="\begin{Eqts}
\OD{p}{t}(t) = z_1  p(t) + z_2  p(t) = (z_1 + z_2)  p(t) \\\\
p(0) = 1
\end{Eqts}">|; 

$key = q/{Eqts}OD{s}{t}(t)=(z_1+z_2)s(t)s(0)=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.52ex; vertical-align: -3.70ex; " SRC="|."$dir".q|img7.svg"
 ALT="\begin{Eqts}
\OD{s}{t}(t) = (z_1 + z_2)  s(t) \\\\
s(0) = 1
\end{Eqts}">|; 

$key = q/{Eqts}OD{u}{t}(t)=imgu(t)u(0)=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.52ex; vertical-align: -3.70ex; " SRC="|."$dir".q|img14.svg"
 ALT="\begin{Eqts}
\OD{u}{t}(t) = \img u(t) \\\\
u(0) = 1
\end{Eqts}">|; 

$key = q/{Eqts}OD{x}{t}(t)=zcdotx(t)x(0)=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.52ex; vertical-align: -3.70ex; " SRC="|."$dir".q|img2.svg"
 ALT="\begin{Eqts}
\OD{x}{t}(t) = z \cdot x(t) \\\\
x(0) = 1
\end{Eqts}">|; 

$key = q/{Eqts}OD{}{t}(cos(t)+imgsin(t))=OD{}{t}exp(imgt)OD{}{t}cos(t)+imgOD{}{t}sin(t)=imgexp(imgt)OD{}{t}cos(t)+imgOD{}{t}sin(t)=imgcos(t)-sin(t){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.02ex; vertical-align: -7.46ex; " SRC="|."$dir".q|img48.svg"
 ALT="\begin{Eqts}
\OD{}{t}( \cos(t) + \img \sin(t) ) = \OD{}{t}\exp(\img t) \\\\
\OD{}...
... \\\\
\OD{}{t} \cos(t) + \img \OD{}{t}\sin(t) = \img \cos(t) - \sin(t)
\end{Eqts}">|; 

$key = q/{Eqts}OD{}{t}cos(t)=-sin(t)OD{}{t}sin(t)=cos(t){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.49ex; vertical-align: -4.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="\begin{Eqts}
\OD{}{t} \cos(t) = -\sin(t) \\\\
\OD{}{t}\sin(t) = \cos(t)
\end{Eqts}">|; 

$key = q/{Eqts}cos(theta)=frac{Re(z)}{abs{z}}sin(theta)=frac{Im(z)}{abs{z}}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.90ex; vertical-align: -5.39ex; " SRC="|."$dir".q|img30.svg"
 ALT="\begin{Eqts}
\cos(\theta) = \frac{\Re(z)}{\abs{z}} \\\\
\sin(\theta) = \frac{\Im(z)}{\abs{z}}
\end{Eqts}">|; 

$key = q/{Eqts}cos(u)=frac{exp(imgu)+exp(-imgu)}{2}sin(u)=frac{exp(imgu)-exp(-imgu)}{2img}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.74ex; vertical-align: -4.81ex; " SRC="|."$dir".q|img28.svg"
 ALT="\begin{Eqts}
\cos(u) = \frac{ \exp(\img u) + \exp(-\img u) }{2} \\\\
\sin(u) = \frac{ \exp(\img u) - \exp(-\img u) }{2 \img}
\end{Eqts}">|; 

$key = q/{Eqts}cos(u+v)=cos(u)cos(v)-sin(u)sin(v)sin(u+v)=sin(u)cos(v)+sin(v)cos(u){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img51.svg"
 ALT="\begin{Eqts}
\cos(u+v) = \cos(u)\cos(v) - \sin(u)\sin(v) \\\\
\sin(u+v) = \sin(u)\cos(v) + \sin(v)\cos(u)
\end{Eqts}">|; 

$key = q/{Eqts}exp(imgpislash2)=imgexp(imgpi)=-1exp(3piimgslash2)=imgexp(2piimg)=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.12ex; vertical-align: -6.00ex; " SRC="|."$dir".q|img20.svg"
 ALT="\begin{Eqts}
\exp(\img \pi/2) = \img \\\\
\exp(\img \pi) = -1 \\\\
\exp(3\pi\img/2) = \img \\\\
\exp(2\pi\img) = 1
\end{Eqts}">|; 

$key = q/{Eqts}exp(imgu)=cos(u)+imgsin(u)exp(-imgu)=cos(u)-imgsin(u){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img27.svg"
 ALT="\begin{Eqts}
\exp( \img u ) = \cos(u) + \img \sin(u) \\\\
\exp( - \img u ) = \cos(u) - \img \sin(u)
\end{Eqts}">|; 

$key = q/{Eqts}frac{d^2u}{dt^2}(t)=-u(t)u(0)=1qquadOD{u}{t}(0)=-i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.71ex; vertical-align: -4.80ex; " SRC="|."$dir".q|img25.svg"
 ALT="\begin{Eqts}
\frac{d^2 u}{dt^2}(t) = - u(t) \\\\
u(0) = 1 \qquad \OD{u}{t}(0) = -i
\end{Eqts}">|; 

$key = q/{Eqts}ln(z)=ln(abs{z})+ln(exp(imgarg(z)))ln(z)=ln(abs{z})+imgarg(z){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img41.svg"
 ALT="\begin{Eqts}
\ln(z) = \ln(\abs{z}) + \ln(\exp(\img\arg(z))) \\\\
\ln(z) = \ln(\abs{z}) + \img\arg(z)
\end{Eqts}">|; 

$key = q/{Eqts}s(t)=exp((z_1+z_2)cdott)p(t)=exp(z_1t)cdotexp(z_2t){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img6.svg"
 ALT="\begin{Eqts}
s(t) = \exp( (z_1 + z_2) \cdot t ) \\\\
p(t) = \exp(z_1 t) \cdot \exp(z_2 t)
\end{Eqts}">|; 

$key = q/{Eqts}theta=arg(z)quadLeftrightarrowquadcases{z=abs{z}exp(imgtheta)thetain[0,2pi)cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img37.svg"
 ALT="\begin{Eqts}
\theta = \arg(z) \quad\Leftrightarrow\quad
\begin{cases}
z = \abs{z}\exp(\img\theta) \\\\
\theta\in [0,2\pi)
\end{cases}\end{Eqts}">|; 

1;

