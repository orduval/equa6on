# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img43.svg"
 ALT="$2$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$A$">|; 

$key = q/K:A^2toB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img49.svg"
 ALT="$K : A^2 \to B$">|; 

$key = q/abs{f}:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\abs{f} : A \mapsto \setR$">|; 

$key = q/abs{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\abs{f}$">|; 

$key = q/abs{f}=f^++f^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\abs{f} = f^+ + f^-$">|; 

$key = q/corps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\corps^n$">|; 

$key = q/displaystyleabs{f(x)}=-f(x)=f^-(x)=f^+(x)+f^-(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \abs{f(x)} = -f(x) = f^-(x) = f^+(x) + f^-(x)$">|; 

$key = q/displaystyleabs{f(x)}=f(x)=f^+(x)=f^+(x)+f^-(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \abs{f(x)} = f(x) = f^+(x) = f^+(x) + f^-(x)$">|; 

$key = q/displaystyleabs{f}(x)=abs{f(x)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \abs{f}(x) = \abs{f(x)}$">|; 

$key = q/displaystyleabs{int_Aconjaccent{u(x)}cdotv(x)dmu(x)}leleft[int_Aabs{u(x)}^2dmu(x)right]cdotleft[int_Aabs{v(x)}^2dmu(x)right]strictinferieur+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle \abs{\int_A \conjaccent{u(x)} \cdot v(x)  d\mu(x)} \le \left[ \i...
...ht] \cdot \left[ \int_A \abs{v(x)}^2  d\mu(x) \right] \strictinferieur +\infty$">|; 

$key = q/displaystylebraket{u}{K}{v}=scalaire{u}{v}_K=int_{A^2}u(x)cdotK(x,y)cdotv(y)dmu(x)dmu(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \braket{u}{K}{v} = \scalaire{u}{v}_K = \int_{A^2} u(x) \cdot K(x,y) \cdot v(y)  d\mu(x)  d\mu(y)$">|; 

$key = q/displaystyleint_Aabs{f(x)}dmu(x)=int_Af^+(x)dmu(x)+int_Af^-(x)dmu(x)strictinferieur+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \int_A \abs{f(x)}  d\mu(x) = \int_A f^+(x)  d\mu(x) + \int_A f^-(x)  d\mu(x) \strictinferieur +\infty$">|; 

$key = q/displaystyleint_Au(x)dmu(x)=int_Aphi(x)dmu(x)+imgint_Apsi(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \int_A u(x)  d\mu(x) = \int_A \phi(x)  d\mu(x) + \img \int_A \psi(x)  d\mu(x)$">|; 

$key = q/displaystylelebesgue(A)=left{finsetR^A:int_Aabs{f(x)}dmu(x)strictinferieur+inftyright};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \lebesgue(A) = \left\{ f \in \setR^A : \int_A \abs{f(x)}  d\mu(x) \strictinferieur +\infty \right\}$">|; 

$key = q/displaystylelebesgue^2(A)=left{uinsetC^A:int_Aabs{u(x)}^2dmu(x)strictinferieur+inftyright};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \lebesgue^2(A) = \left\{ u \in \setC^A : \int_A \abs{u(x)}^2  d\mu(x) \strictinferieur +\infty \right\}$">|; 

$key = q/displaystylelebesgue^infty(A,B)=left{uinsetR^A:supessentiel{abs{u(x)}:xinA}strictinferieur+inftyright};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.01ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \lebesgue^\infty(A,B) = \left\{ u \in \setR^A : \supessentiel \{ \abs{u(x)} : x \in A \} \strictinferieur +\infty \right\}$">|; 

$key = q/displaystylelebesgue^k(A,B)=left{uinsetC^A:int_Aabs{u(x)}^kdmu(x)strictinferieur+inftyright};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \lebesgue^k(A,B) = \left\{ u \in \setC^A : \int_A \abs{u(x)}^k  d\mu(x) \strictinferieur +\infty \right\}$">|; 

$key = q/displaystylenorme{u}=sqrt{scalaire{u}{u}}=sqrt{int_Aabs{u(x)}^2dmu(x)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.23ex; vertical-align: -2.83ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \norme{u} = \sqrt{ \scalaire{u}{u} } = \sqrt{ \int_A \abs{u(x)}^2  d\mu(x) }$">|; 

$key = q/displaystylenorme{u}^2=scalaire{u}{u}=int_Aabs{u(x)}^2dmu(x)strictinferieur+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \norme{u}^2 = \scalaire{u}{u} = \int_A \abs{u(x)}^2  d\mu(x) \strictinferieur +\infty$">|; 

$key = q/displaystylenorme{u}_k=left[int_Aabs{u(x)}^kdxright]^{1slashk};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.29ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \norme{u}_k = \left[ \int_A \abs{u(x)}^k  dx \right]^{1/k}$">|; 

$key = q/displaystylescalaire{u}{u}=int_Aabs{u(x)}^2dmu(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \scalaire{u}{u} = \int_A \abs{u(x)}^2  d\mu(x) = 0$">|; 

$key = q/displaystylescalaire{u}{u}=int_Aabs{u(x)}^2dmu(x)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \scalaire{u}{u} = \int_A \abs{u(x)}^2  d\mu(x) \ge 0$">|; 

$key = q/displaystylescalaire{u}{v}=int_Aconjaccent{u(x)}cdotv(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \scalaire{u}{v} = \int_A \conjaccent{u(x)} \cdot v(x)  d\mu(x)$">|; 

$key = q/displaystylescalaire{x}{y}=sum_iconjaccent{x}_icdoty_iequivsum_iconjaccent{x}(i)cdoty(i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \scalaire{x}{y} = \sum_i \conjaccent{x}_i \cdot y_i \equiv \sum_i \conjaccent{x}(i) \cdot y(i)$">|; 

$key = q/f(x)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$f(x) \ge 0$">|; 

$key = q/f(x)strictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$f(x) \strictinferieur 0$">|; 

$key = q/f:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f : A \mapsto \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img13.svg"
 ALT="$f$">|; 

$key = q/f=f^+-f^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img2.svg"
 ALT="$f = f^+ - f^-$">|; 

$key = q/f^+(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$f^+(x) = 0$">|; 

$key = q/f^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$f^+$">|; 

$key = q/f^+=abs{f}-f^-leabs{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$f^+ = \abs{f} - f^- \le \abs{f}$">|; 

$key = q/f^-(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$f^-(x) = 0$">|; 

$key = q/f^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$f^-$">|; 

$key = q/f^-=abs{f}-f^+leabs{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$f^- = \abs{f} - f^+ \le \abs{f}$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img44.svg"
 ALT="$k$">|; 

$key = q/lebesgue^2(A,B);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.76ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\lebesgue^2(A,B)$">|; 

$key = q/scalaire{}{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\scalaire{}{}$">|; 

$key = q/scalaire{}{}_K;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\scalaire{}{}_K$">|; 

$key = q/u,v:AmapstosetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$u,v : A \mapsto \setC$">|; 

$key = q/u:AmapstosetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$u : A \mapsto \setC$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$u$">|; 

$key = q/u=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$u = 0$">|; 

$key = q/uessegal0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img36.svg"
 ALT="$u \essegal 0$">|; 

$key = q/v,w:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img23.svg"
 ALT="$v,w : A \mapsto \setR$">|; 

$key = q/v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$v$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img5.svg"
 ALT="$x \in A$">|; 

$key = q/xmapstoabs{u(x)}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$x \mapsto \abs{u(x)}^2$">|; 

$key = q/{Eqts}int_Af^+(x)dmu(x)leint_Aabs{f(x)}dmu(x)strictinferieur+inftyint_Af^-(x)dmu(x)leint_Aabs{f(x)}dmu(x)strictinferieur+infty{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.01ex; vertical-align: -6.95ex; " SRC="|."$dir".q|img20.svg"
 ALT="\begin{Eqts}
\int_A f^+(x)  d\mu(x) \le \int_A \abs{f(x)}  d\mu(x) \strictinfe...
...x)  d\mu(x) \le \int_A \abs{f(x)}  d\mu(x) \strictinferieur +\infty
\end{Eqts}">|; 

$key = q/{Eqts}phi(x)=Re(u(x))psi(x)=Im(u(x)){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img24.svg"
 ALT="\begin{Eqts}
\phi(x) = \Re(u(x)) \\\\
\psi(x) = \Im(u(x)) \\\\
\end{Eqts}">|; 

1;

