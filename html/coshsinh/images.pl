# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/cosh;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\cosh$">|; 

$key = q/displaystyleOD{cosh}{x}(x)=unsur{2}Big[exp(x)-exp(-x)Big]=sinh(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \OD{\cosh}{x}(x) = \unsur{2} \Big[ \exp(x) - \exp(-x) \Big] = \sinh(x)$">|; 

$key = q/displaystyleOD{sinh}{x}(x)=unsur{2}Big[exp(x)+exp(-x)Big]=cosh(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \OD{\sinh}{x}(x) = \unsur{2} \Big[ \exp(x) + \exp(-x) \Big] = \cosh(x)$">|; 

$key = q/displaystyleOD{tanh}{x}(x)=frac{cosh(x)}{cosh(x)}-frac{sinh(x)cdotsinh(x)}{cosh(x)^2}=1-tanh(x)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \OD{\tanh}{x}(x) = \frac{\cosh(x)}{\cosh(x)} - \frac{\sinh(x) \cdot \sinh(x)}{\cosh(x)^2} = 1 - \tanh(x)^2$">|; 

$key = q/displaystylec^2=unsur{4}Big[exp(x)^2+2exp(x)cdotexp(-x)+exp(-x)^2Big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle c^2 = \unsur{4}  \Big[ \exp(x)^2 + 2  \exp(x) \cdot \exp(-x) + \exp(-x)^2 \Big]$">|; 

$key = q/displaystylec^2=unsur{4}Big[exp(x)^2+exp(-x)^2+2Big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle c^2 = \unsur{4}  \Big[ \exp(x)^2 + \exp(-x)^2 + 2\Big]$">|; 

$key = q/displaystylecosh(0)=unsur{2}Big[exp(0)+exp(0)Big]=unsur{2}(1+1)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \cosh(0) = \unsur{2} \Big[ \exp(0) + \exp(0) \Big] = \unsur{2} (1 + 1) = 1$">|; 

$key = q/displaystylecosh(x)=unsur{2}Big[exp(x)+exp(-x)Big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle \cosh(x) = \unsur{2}  \Big[ \exp(x) + \exp(-x) \Big]$">|; 

$key = q/displaystylecosh(x)^2-sinh(x)^2=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \cosh(x)^2 - \sinh(x)^2 = 1$">|; 

$key = q/displaystyleexp=cosh+sinh;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \exp = \cosh + \sinh$">|; 

$key = q/displaystyleint_a^bcosh(x)dx=sinh(b)-sinh(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \int_a^b \cosh(x)  dx = \sinh(b) - \sinh(a)$">|; 

$key = q/displaystyleint_a^bsinh(x)dx=cosh(b)-cosh(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \int_a^b \sinh(x)  dx = \cosh(b) - \cosh(a)$">|; 

$key = q/displaystyles^2=unsur{4}Big[exp(x)^2+exp(-x)^2-2Big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle s^2 = \unsur{4}  \Big[ \exp(x)^2 + \exp(-x)^2 - 2\Big]$">|; 

$key = q/displaystyles^2=unsur{4}Big[exp(x)^2-2exp(x)cdotexp(-x)+exp(-x)^2Big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle s^2 = \unsur{4}  \Big[ \exp(x)^2 - 2  \exp(x) \cdot \exp(-x) + \exp(-x)^2 \Big]$">|; 

$key = q/displaystylesinh(0)=unsur{2}Big[exp(0)-exp(0)Big]=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \sinh(0) = \unsur{2} \Big[ \exp(0) - \exp(0) \Big] = 0$">|; 

$key = q/displaystylesinh(x)=unsur{2}Big[exp(x)-exp(-x)Big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \sinh(x) = \unsur{2}  \Big[ \exp(x) - \exp(-x) \Big]$">|; 

$key = q/displaystyletanh(0)=frac{sinh(0)}{cosh(0)}=frac{0}{1}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \tanh(0) = \frac{\sinh(0)}{\cosh(0)} = \frac{0}{1} = 0$">|; 

$key = q/displaystyletanh(x)=frac{sinhx}{coshx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \tanh(x) = \frac{\sinh x}{\cosh x}$">|; 

$key = q/exp(x)cdotexp(-x)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\exp(x) \cdot \exp(-x) = 1$">|; 

$key = q/sinh;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\sinh$">|; 

$key = q/tanh;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\tanh$">|; 

$key = q/tinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img29.svg"
 ALT="$t \in \setR$">|; 

$key = q/u:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$u : \setR \mapsto \setR$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$x$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img3.svg"
 ALT="$x \in \setR$">|; 

$key = q/{Eqts}c=cosh(x)s=sinh(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img11.svg"
 ALT="\begin{Eqts}
c = \cosh(x) \\\\
s = \sinh(x)
\end{Eqts}">|; 

$key = q/{eqnarraystar}c^2-s^2&=&frac{exp(x)^2+exp(-x)^2+2-exp(x)^2-exp(-x)^2+2}{4}&=&frac{4}{4}=1{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.42ex; " SRC="|."$dir".q|img17.svg"
 ALT="\begin{eqnarray*}
c^2 - s^2 &amp;=&amp; \frac{\exp(x)^2 + \exp(-x)^2 + 2 - \exp(x)^2 - \exp(-x)^2 + 2}{4} \\\\
&amp;=&amp; \frac{4}{4} = 1
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}cosh(x)&=&cosh(-x)sinh(x)&=&-sinh(-x){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img7.svg"
 ALT="\begin{eqnarray*}
\cosh(x) &amp;=&amp; \cosh(-x) \\\\
\sinh(x) &amp;=&amp; -\sinh(-x)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}partialu(t)&=&1-u(t)^2u(0)&=&0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img28.svg"
 ALT="\begin{eqnarray*}
\partial u(t) &amp;=&amp; 1 - u(t)^2 \\\\
u(0) &amp;=&amp; 0
\end{eqnarray*}">|; 

1;

