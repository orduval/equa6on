# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/displaystyletheta^*=argmin_{theta}sum_nnorme{y^{(n)}-R_{theta}left(x^{(n)}right)}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.58ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \theta^* = \arg\min_{\theta} \sum_n \norme{ y^{(n)} - R_{\theta}\left( x^{(n)} \right) }^2$">|; 

$key = q/displaystyley_i=P_{theta}(x)=c+sum_jv_jsigmaleft(sum_k(w_{jk}x_k)+b_jright);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.45ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle y_i = P_{\theta}(x) = c + \sum_j v_j  \sigma\left( \sum_k ( w_{jk}  x_k ) + b_j \right)$">|; 

$key = q/displaystyley_i=sigmaleft(sum_j(w_{ij}x_j)+b_iright);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.45ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle y_i = \sigma\left( \sum_j ( w_{ij}  x_j ) + b_i \right)$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$i$">|; 

$key = q/n=1,...,K;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$n=1,...,K$">|; 

$key = q/sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\sigma$">|; 

$key = q/theta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\theta$">|; 

$key = q/theta=(v,w,b,c);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\theta = (v,w,b,c)$">|; 

$key = q/x^{(n)}insetR^{N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img13.svg"
 ALT="$x^{(n)}\in\setR^{N}$">|; 

$key = q/x_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img7.svg"
 ALT="$x_i$">|; 

$key = q/x_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img3.svg"
 ALT="$x_j$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$y$">|; 

$key = q/y=R_{theta}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$y=R_{\theta}(x)$">|; 

$key = q/y^{(n)}insetR^M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$y^{(n)}\in\setR^M$">|; 

$key = q/y_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$y_i$">|; 

$key = q/y_i=P_{theta}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$y_i = P_{\theta}(x)$">|; 

$key = q/{Eqts}sigma(x)=mathrm{sign}(x)sigma(x)=tanh(x)sigma(x)=exp(-x^2)sigma(x)=xexp(-x^2){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.12ex; vertical-align: -6.00ex; " SRC="|."$dir".q|img6.svg"
 ALT="\begin{Eqts}
\sigma(x) = \mathrm{sign}(x) \\\\
\sigma(x) = \tanh(x) \\\\
\sigma(x) = \exp(-x^2) \\\\
\sigma(x) = x \exp(-x^2)
\end{Eqts}">|; 

1;

