# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/F';MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img37.svg"
 ALT="$F'$">|; 

$key = q/F(s)+s=s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$F(s) + s = s$">|; 

$key = q/F(s)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$F(s) = 0$">|; 

$key = q/F(x_{k+1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$F(x_{k + 1})$">|; 

$key = q/F,f:setRtosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$F,f : \setR \to \setR$">|; 

$key = q/F:setR^nmapstosetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$F : \setR^n \mapsto \setR^n$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$F$">|; 

$key = q/K;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$K$">|; 

$key = q/XsubseteqsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img9.svg"
 ALT="$X \subseteq \setR^n$">|; 

$key = q/displaystyleDelta_k=-left[partialF(x_k)right]^{-1}cdotF(x_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \Delta_k = - \left[\partial F(x_k)\right]^{-1} \cdot F(x_k)$">|; 

$key = q/displaystyleF'(u_1)approxfrac{(u_2-u1)-(u_1-u_0)}{u_1-u_0}=frac{(u_2-2u_1+u_0)}{u_1-u_0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle F'(u_1) \approx \frac{(u_2 - u1) - (u_1 - u_0)}{u_1 - u_0} = \frac{(u_2 - 2 u_1 + u_0)}{u_1 - u_0}$">|; 

$key = q/displaystyleF'(u_1)approxfrac{F(u_0)-F(u_1)}{u_0-u_1}=frac{F(u_1)-F(u_0)}{u_1-u_0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle F'(u_1) \approx \frac{F(u_0) - F(u_1)}{u_0 - u_1} = \frac{F(u_1) - F(u_0)}{u_1 - u_0}$">|; 

$key = q/displaystyleF'(x)=OD{F}{x}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle F'(x) = \OD{F}{x}(x)$">|; 

$key = q/displaystyleF(u_0)approxF(u_1)+F'(u_1)cdot(u_0-u_1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle F(u_0) \approx F(u_1) + F'(u_1) \cdot (u_0 - u_1)$">|; 

$key = q/displaystyleF(x_K)approxlim_{ktoinfty}F(x_k)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.62ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle F(x_K) \approx \lim_{k \to \infty} F(x_k) = 0$">|; 

$key = q/displaystyleF(x_{k+1})approxF(x_k)+partialF(x_k)cdotDelta_kapprox0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle F(x_{k + 1}) \approx F(x_k) + \partial F(x_k) \cdot \Delta_k \approx 0$">|; 

$key = q/displaystyleS={sinsetR^n:F(s)=0}=noyauF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle S = \{ s \in \setR^n : F(s) = 0 \} = \noyau F$">|; 

$key = q/displaystylef(x)=F(x)+x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle f(x) = F(x) + x$">|; 

$key = q/displaystylepartialF(x_k)cdotDelta_k=-F(x_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \partial F(x_k) \cdot \Delta_k = - F(x_k)$">|; 

$key = q/displaystylex_Kapproxlim_{ktoinfty}x_k=s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.49ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle x_K \approx \lim_{k \to \infty} x_k = s$">|; 

$key = q/displaystylex_{k+1}=I(x_k)=x_k+Delta_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle x_{k + 1} = I(x_k) = x_k + \Delta_k$">|; 

$key = q/displaystylex_{k+1}=f(x_k)=f^{k+1}(x_0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle x_{k + 1} = f(x_k) = f^{k + 1}(x_0)$">|; 

$key = q/displaystylex_{k+1}=x_k-frac{(u_1-u_0)^2}{u_2-2u_1+u_0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle x_{k+1} = x_k - \frac{(u_1 - u_0)^2}{u_2 - 2 u_1 + u_0}$">|; 

$key = q/displaystylex_{k+1}=x_k-left[partialF(x_k)right]^{-1}cdotF(x_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle x_{k + 1} = x_k - \left[\partial F(x_k)\right]^{-1} \cdot F(x_k)$">|; 

$key = q/displaystylex_{k+1}=x_{k}-frac{F(x_k)}{F'(x_k)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle x_{k + 1} = x_{k} - \frac{F(x_k)}{F'(x_k)}$">|; 

$key = q/f(s)=F(s)+s=s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$f(s) = F(s) + s = s$">|; 

$key = q/f(x)=F(x)+x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$f(x) = F(x) + x$">|; 

$key = q/f:setR^ntosetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img20.svg"
 ALT="$f : \setR^n \to \setR^n$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img25.svg"
 ALT="$f$">|; 

$key = q/f^k(x_0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$f^k(x_0)$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$k$">|; 

$key = q/partialF(x_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\partial F(x_k)$">|; 

$key = q/s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img23.svg"
 ALT="$s$">|; 

$key = q/sinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img3.svg"
 ALT="$s \in S$">|; 

$key = q/x_0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img10.svg"
 ALT="$x_0 = 0$">|; 

$key = q/x_0insetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img4.svg"
 ALT="$x_0 \in \setR^n$">|; 

$key = q/x_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img11.svg"
 ALT="$x_k$">|; 

$key = q/x_{k+1}=x_k+Delta_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img12.svg"
 ALT="$x_{k + 1} = x_k + \Delta_k$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img30.svg"
 ALT="$x \in \setR$">|; 

$key = q/xinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img22.svg"
 ALT="$x \in \setR^n$">|; 

$key = q/{Eqts}F(u_0)=f(u_0)-u_0=u_1-u_0F(u_1)=f(u_1)-u_1=u_2-u_1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img35.svg"
 ALT="\begin{Eqts}
F(u_0) = f(u_0) - u_0 = u_1 - u_0 \\\\
F(u_1) = f(u_1) - u_1 = u_2 - u_1
\end{Eqts}">|; 

$key = q/{eqnarraystar}u_0&=&x_ku_1&=&f(u_0)u_2&=&f(u_1){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img33.svg"
 ALT="\begin{eqnarray*}
u_0 &amp;=&amp; x_k \\\\
u_1 &amp;=&amp; f(u_0) \\\\
u_2 &amp;=&amp; f(u_1)
\end{eqnarray*}">|; 

1;

