# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img52.svg"
 ALT="\begin{eqnarray*}
\{ x \in A : f^-(x) \strictinferieur a \} &amp;=&amp; (A^+ \cup A^0) \...
... \cup \{ x \in A^- : -f(x) \strictsuperieur a \} \in \mathcal{T}
\end{eqnarray*}">|; 

$key = q/-f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img33.svg"
 ALT="$-f$">|; 

$key = q/A=A^+cupA^0cupA^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$A = A^+ \cup A^0 \cup A^-$">|; 

$key = q/A^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$A^+$">|; 

$key = q/A^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img12.svg"
 ALT="$A^-$">|; 

$key = q/A^0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$A^0$">|; 

$key = q/age0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img23.svg"
 ALT="$a \ge 0$">|; 

$key = q/ale0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img36.svg"
 ALT="$a \le 0$">|; 

$key = q/astrictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img16.svg"
 ALT="$a \strictinferieur 0$">|; 

$key = q/astrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img40.svg"
 ALT="$a \strictsuperieur 0$">|; 

$key = q/f:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f : A \mapsto \setR$">|; 

$key = q/f=-f^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$f = -f^-$">|; 

$key = q/f=f^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img45.svg"
 ALT="$f = f^+$">|; 

$key = q/f^+(x)=0strictinferieura;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$f^+(x) = 0 \strictinferieur a$">|; 

$key = q/f^+(x)ge0strictsuperieura;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$f^+(x) \ge 0 \strictsuperieur a$">|; 

$key = q/f^+(x)strictinferieura;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$f^+(x) \strictinferieur a$">|; 

$key = q/f^+(x)strictinferieurale0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$f^+(x) \strictinferieur a \le 0$">|; 

$key = q/f^+(x)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$f^+(x) \strictsuperieur 0$">|; 

$key = q/f^+(x)strictsuperieura;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$f^+(x) \strictsuperieur a$">|; 

$key = q/f^+,f^-:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img2.svg"
 ALT="$f^+,f^- : A \mapsto \setR$">|; 

$key = q/f^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img19.svg"
 ALT="$f^+$">|; 

$key = q/f^+=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img13.svg"
 ALT="$f^+ = 0$">|; 

$key = q/f^+=f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img26.svg"
 ALT="$f^+ = f$">|; 

$key = q/f^+=f=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$f^+ = f = 0$">|; 

$key = q/f^+=fstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$f^+ = f \strictsuperieur 0$">|; 

$key = q/f^+ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img37.svg"
 ALT="$f^+ \ge 0$">|; 

$key = q/f^-(x)=0strictinferieura;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$f^-(x) = 0 \strictinferieur a$">|; 

$key = q/f^-(x)strictinferieura;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$f^-(x) \strictinferieur a$">|; 

$key = q/f^-(x)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$f^-(x) \strictsuperieur 0$">|; 

$key = q/f^-(x)strictsuperieura;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$f^-(x) \strictsuperieur a$">|; 

$key = q/f^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img21.svg"
 ALT="$f^-$">|; 

$key = q/f^-=-f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img31.svg"
 ALT="$f^- = -f$">|; 

$key = q/f^-=-f=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$f^- = -f = 0$">|; 

$key = q/f^-=-fstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$f^- = -f \strictsuperieur 0$">|; 

$key = q/f^-=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$f^- = 0$">|; 

$key = q/strictinferieura;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\strictinferieur a$">|; 

$key = q/strictsuperieura;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\strictsuperieur a$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$x$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img20.svg"
 ALT="$x \in A$">|; 

$key = q/xinA^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.06ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img27.svg"
 ALT="$x \in A^+$">|; 

$key = q/xinA^+cupA^0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img48.svg"
 ALT="$x \in A^+ \cup A^0$">|; 

$key = q/xinA^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.06ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img32.svg"
 ALT="$x \in A^-$">|; 

$key = q/xinA^-cupA^0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img42.svg"
 ALT="$x \in A^- \cup A^0$">|; 

$key = q/xinAsetminus(A^+cupA^0)=A^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$x \in A \setminus (A^+ \cup A^0) = A^-$">|; 

$key = q/xinAsetminus(A^-cupA^0)=A^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$x \in A \setminus (A^- \cup A^0) = A^+$">|; 

$key = q/{Eqts}{xinA:f^+(x)strictinferieura}=emptysetinmathcal{T}{xinA:f^-(x)strictinferieura}=emptysetinmathcal{T}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img39.svg"
 ALT="\begin{Eqts}
\{ x \in A : f^+(x) \strictinferieur a \} = \emptyset \in \mathcal{...
...\{ x \in A : f^-(x) \strictinferieur a \} = \emptyset \in \mathcal{T}
\end{Eqts}">|; 

$key = q/{Eqts}{xinA:f^+(x)strictsuperieura}={xinA:f^+(x)ge0}=Ainmathcal{T}{xinA:f^-(x)strictsuperieura}={xinA:f^-(x)ge0}=Ainmathcal{T}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img22.svg"
 ALT="\begin{Eqts}
\{ x \in A : f^+(x) \strictsuperieur a \} = \{ x \in A : f^+(x) \ge...
...rictsuperieur a \} = \{ x \in A : f^-(x) \ge 0 \} = A \in \mathcal{T}
\end{Eqts}">|; 

$key = q/{eqnarraystar}f^+(x)&=&max{f(x),0}ge0f^-(x)&=&max{-f(x),0}ge0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img3.svg"
 ALT="\begin{eqnarray*}
f^+(x) &amp;=&amp; \max \{ f(x) , 0 \} \ge 0 \\\\
f^-(x) &amp;=&amp; \max \{ -f(x) , 0 \} \ge 0
\end{eqnarray*}">|; 

1;

