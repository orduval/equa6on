# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 22.28ex; " SRC="|."$dir".q|img101.svg"
 ALT="\begin{eqnarray*}
\int_A f(x) \cdot \indicatrice_C(x)  d\mu(x) &amp;=&amp; \int_A f^+(x...
... d\mu(x) - \int_C f^-(x)  d\mu(x) \\\\
&amp;=&amp; \int_C f(x)  d\mu(x)
\end{eqnarray*}">|; 

$key = q/0essinferieurfessinferieuralpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img310.svg"
 ALT="$0 \essinferieur f \essinferieur \alpha$">|; 

$key = q/0inmathcal{E}_A(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$0 \in \mathcal{E}_A(f)$">|; 

$key = q/0inmathcal{I}_A(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$0 \in \mathcal{I}_A(f)$">|; 

$key = q/A,Binmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img143.svg"
 ALT="$A,B \in \mathcal{T}$">|; 

$key = q/A,BsubseteqAcupB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img113.svg"
 ALT="$A,B \subseteq A \cup B$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$A$">|; 

$key = q/A=C(alpha)cupZ(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img155.svg"
 ALT="$A = C(\alpha) \cup Z(\alpha)$">|; 

$key = q/A_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img221.svg"
 ALT="$A_0$">|; 

$key = q/A_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img238.svg"
 ALT="$A_n$">|; 

$key = q/AcapB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img147.svg"
 ALT="$A \cap B$">|; 

$key = q/AcapB=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.06ex; vertical-align: -0.23ex; " SRC="|."$dir".q|img103.svg"
 ALT="$A \cap B = \emptyset$">|; 

$key = q/AcupB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img104.svg"
 ALT="$A \cup B$">|; 

$key = q/AsetminusA_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img225.svg"
 ALT="$A \setminus A_0$">|; 

$key = q/AsetminusA_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img242.svg"
 ALT="$A \setminus A_n$">|; 

$key = q/AsetminusB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img145.svg"
 ALT="$A \setminus B$">|; 

$key = q/AsetminusC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img79.svg"
 ALT="$A \setminus C$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img102.svg"
 ALT="$B$">|; 

$key = q/C(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img178.svg"
 ALT="$C(\alpha)$">|; 

$key = q/C(alpha)subseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img181.svg"
 ALT="$C(\alpha) \subseteq A$">|; 

$key = q/C(alpha)subseteqF(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img309.svg"
 ALT="$C(\alpha) \subseteq F(\alpha)$">|; 

$key = q/C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img38.svg"
 ALT="$C$">|; 

$key = q/C=AsetminusZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img137.svg"
 ALT="$C = A \setminus Z$">|; 

$key = q/Cinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img75.svg"
 ALT="$C \in \mathcal{T}$">|; 

$key = q/CsubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img36.svg"
 ALT="$C \subseteq A$">|; 

$key = q/Delta_0=Mslash2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img214.svg"
 ALT="$\Delta_0 = M / 2$">|; 

$key = q/Delta_0cdotindicatrice[A_0];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img218.svg"
 ALT="$\Delta_0 \cdot \indicatrice[A_0]$">|; 

$key = q/Delta_ncdotindicatrice[A_n];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img235.svg"
 ALT="$\Delta_n \cdot \indicatrice[A_n]$">|; 

$key = q/I(alpha)leI(beta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img185.svg"
 ALT="$I(\alpha) \le I(\beta)$">|; 

$key = q/I:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img175.svg"
 ALT="$I : \setR \mapsto \setR$">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img186.svg"
 ALT="$I$">|; 

$key = q/M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img187.svg"
 ALT="$M$">|; 

$key = q/M=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img208.svg"
 ALT="$M = 0$">|; 

$key = q/Mstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img211.svg"
 ALT="$M \strictsuperieur 0$">|; 

$key = q/N(alpha)subseteqN(beta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img292.svg"
 ALT="$N(\alpha) \subseteq N(\beta)$">|; 

$key = q/P;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img325.svg"
 ALT="$P$">|; 

$key = q/T(alpha)subseteqT(beta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img293.svg"
 ALT="$T(\alpha) \subseteq T(\beta)$">|; 

$key = q/Thetage0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img206.svg"
 ALT="$\Theta \ge 0$">|; 

$key = q/Z(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img158.svg"
 ALT="$Z(\alpha)$">|; 

$key = q/Z(alpha)subseteqZ(beta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img169.svg"
 ALT="$Z(\alpha) \subseteq Z(\beta)$">|; 

$key = q/Z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img141.svg"
 ALT="$Z$">|; 

$key = q/Z=AsetminusC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img140.svg"
 ALT="$Z = A \setminus C$">|; 

$key = q/ZsubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img138.svg"
 ALT="$Z \subseteq A$">|; 

$key = q/alpha,beta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img291.svg"
 ALT="$\alpha,\beta$">|; 

$key = q/alpha,betainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img170.svg"
 ALT="$\alpha,\beta \in \setR$">|; 

$key = q/alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img188.svg"
 ALT="$\alpha$">|; 

$key = q/alpha=-lambdastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img263.svg"
 ALT="$\alpha = - \lambda \strictsuperieur 0$">|; 

$key = q/alphageIslashepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img162.svg"
 ALT="$\alpha \ge I / \epsilon$">|; 

$key = q/alphagebeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img171.svg"
 ALT="$\alpha \ge \beta$">|; 

$key = q/alphainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\alpha \in \setR$">|; 

$key = q/alphamapstomu(Z(alpha));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img304.svg"
 ALT="$\alpha \mapsto \mu(Z(\alpha))$">|; 

$key = q/alphato+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img314.svg"
 ALT="$\alpha \to +\infty$">|; 

$key = q/displaystyle(f^+-f^-)cdotindicatrice_C=f^+cdotindicatrice_C-f^-cdotindicatrice_C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle (f^+ - f^-) \cdot \indicatrice_C = f^+ \cdot \indicatrice_C - f^- \cdot \indicatrice_C$">|; 

$key = q/displaystyle0leint_Af(x)dmu(x)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle 0 \le \int_A f(x)  d\mu(x) \le 0$">|; 

$key = q/displaystyle0lelim_{alphato+infty}I(alpha)leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img191.svg"
 ALT="$\displaystyle 0 \le \lim_{\alpha \to +\infty} I(\alpha) \le \epsilon$">|; 

$key = q/displaystyle0lemu(Z(alpha))lemu(N(alpha))+mu(T(alpha));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img302.svg"
 ALT="$\displaystyle 0 \le \mu(Z(\alpha)) \le \mu(N(\alpha)) + \mu(T(\alpha))$">|; 

$key = q/displaystyle0lesupessentiel_{xinA}[f(x)-w_n(x)]lefrac{M}{2^n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.56ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img247.svg"
 ALT="$\displaystyle 0 \le \supessentiel_{x \in A} [f(x) - w_n(x)] \le \frac{M}{2^n}$">|; 

$key = q/displaystyle0lesupessentiel{f(x):xinC(alpha)}lealpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img311.svg"
 ALT="$\displaystyle 0 \le \supessentiel \{ f(x) : x \in C(\alpha) \} \le \alpha$">|; 

$key = q/displaystyle0lesupessentiel{g(x):xinC(alpha)}lealpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img312.svg"
 ALT="$\displaystyle 0 \le \supessentiel \{ g(x) : x \in C(\alpha) \} \le \alpha$">|; 

$key = q/displaystyle1=indicatrice[AcupB](x)=indicatrice_A(x)+indicatrice_B(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle 1 = \indicatrice[A \cup B](x) = \indicatrice_A(x) + \indicatrice_B(x)$">|; 

$key = q/displaystyleA_0={xinA:f(x)-w_0(x)strictsuperieurDelta_0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img215.svg"
 ALT="$\displaystyle A_0 = \{ x \in A : f(x) - w_0(x) \strictsuperieur \Delta_0 \}$">|; 

$key = q/displaystyleC(alpha)=F(alpha)capG(alpha)=Asetminus(N(alpha)cupT(alpha));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img294.svg"
 ALT="$\displaystyle C(\alpha) = F(\alpha) \cap G(\alpha) = A \setminus (N(\alpha) \cup T(\alpha))$">|; 

$key = q/displaystyleDelta_n=frac{Delta_{n-1}}{2}=...=frac{Delta_0}{2^{n-1}}=frac{M}{2^n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.93ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img246.svg"
 ALT="$\displaystyle \Delta_n = \frac{\Delta_{n - 1}}{2} = ... = \frac{\Delta_0}{2^{n - 1}} = \frac{M}{2^n}$">|; 

$key = q/displaystyleI(alpha)=int_{Z(alpha)}f(x)dmu(x)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img176.svg"
 ALT="$\displaystyle I(\alpha) = \int_{Z(\alpha)} f(x)  d\mu(x) \ge 0$">|; 

$key = q/displaystyleI(alpha)leint_{Z(alpha)}w(x)dmu(x)+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img179.svg"
 ALT="$\displaystyle I(\alpha) \le \int_{Z(\alpha)} w(x)  d\mu(x) + \epsilon$">|; 

$key = q/displaystyleI(alpha)lemu(Z(alpha))cdotM+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img184.svg"
 ALT="$\displaystyle I(\alpha) \le \mu(Z(\alpha)) \cdot M + \epsilon$">|; 

$key = q/displaystyleI=int_Af(x)dmu(x)strictinferieur+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\displaystyle I = \int_A f(x)  d\mu(x) \strictinferieur +\infty$">|; 

$key = q/displaystyleM=supessentiel_{xinA}f(x)strictinferieur+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img199.svg"
 ALT="$\displaystyle M = \supessentiel_{x \in A} f(x) \strictinferieur +\infty$">|; 

$key = q/displaystyleM=supessentiel_{xinA}w(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img180.svg"
 ALT="$\displaystyle M = \supessentiel_{x \in A} w(x)$">|; 

$key = q/displaystyleM=unsur{mu(A)}int_Af(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.51ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle M = \unsur{\mu(A)} \int_A f(x)  d\mu(x)$">|; 

$key = q/displaystyleMgeinfessentiel_{xinA}f(x)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img200.svg"
 ALT="$\displaystyle M \ge \infessentiel_{x \in A} f(x) \ge 0$">|; 

$key = q/displaystyleS(alpha)=supessentiel_{xinC(alpha)}w(x)leM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.54ex; vertical-align: -2.72ex; " SRC="|."$dir".q|img182.svg"
 ALT="$\displaystyle S(\alpha) = \supessentiel_{x \in C(\alpha)} w(x) \le M$">|; 

$key = q/displaystyleTheta=left{supessentiel_{xinA}[f(x)-w(x)]:winmathcal{E}_A(f)right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img201.svg"
 ALT="$\displaystyle \Theta = \left\{ \supessentiel_{x \in A} [f(x) - w(x)] : w \in \mathcal{E}_A(f) \right\}$">|; 

$key = q/displaystyleZ(alpha)=AsetminusC(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img167.svg"
 ALT="$\displaystyle Z(\alpha) = A \setminus C(\alpha)$">|; 

$key = q/displaystyleZ(alpha)=AsetminusC(alpha)=N(alpha)cupT(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img295.svg"
 ALT="$\displaystyle Z(\alpha) = A \setminus C(\alpha) = N(\alpha) \cup T(\alpha)$">|; 

$key = q/displaystyleabs{int_Af(x)cdotindicatrice_C(x)dmu(x)-int_Au(x)dmu(x)}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle \abs{\int_A f(x) \cdot \indicatrice_C(x)  d\mu(x) - \int_A u(x)  d\mu(x)} \le \epsilon$">|; 

$key = q/displaystylef+g=f^+-f^-+g^+-g^-=(f^++g^+)-(f^-+g^-)=s^+-s^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img322.svg"
 ALT="$\displaystyle f + g = f^+ - f^- + g^+ - g^- = (f^+ + g^+) - (f^- + g^-) = s^+ - s^-$">|; 

$key = q/displaystylef-w_1=f-w_0-Delta_0leM-frac{M}{2}=frac{M}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.93ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img227.svg"
 ALT="$\displaystyle f - w_1 = f - w_0 - \Delta_0 \le M - \frac{M}{2} = \frac{M}{2}$">|; 

$key = q/displaystylef-w_n=f-w_{n-1}-Delta_nleDelta_{n-1}-Delta_n=2Delta_n-Delta_n=Delta_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img244.svg"
 ALT="$\displaystyle f - w_n = f - w_{n - 1} - \Delta_n \le \Delta_{n - 1} - \Delta_n = 2 \Delta_n - \Delta_n = \Delta_n$">|; 

$key = q/displaystylef=f^+-f^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.53ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle f = f^+ - f^-$">|; 

$key = q/displaystylefessinferieursupessentiel{f(x):xinA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle f \essinferieur \supessentiel \{ f(x) : x \in A \}$">|; 

$key = q/displaystylefesssuperieurinfessentiel{f(x):xinA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle f \esssuperieur \infessentiel \{ f(x) : x \in A \}$">|; 

$key = q/displaystyleinf_{winmathcal{E}_A(f)}supessentiel_{xinA}[f(x)-w(x)]=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img255.svg"
 ALT="$\displaystyle \inf_{w \in \mathcal{E}_A(f)} \supessentiel_{x \in A} [f(x) - w(x)] = 0$">|; 

$key = q/displaystyleinfessentiel_{xinA}f(x)leunsur{mu(A)}int_Af(x)dmu(x)lesupessentiel_{xinA}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.56ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \infessentiel_{x \in A} f(x) \le \unsur{\mu(A)} \int_A f(x)  d\mu(x) \le \supessentiel_{x \in A} f(x)$">|; 

$key = q/displaystyleinfessentiel_{xinZ(alpha)}f(x)gealpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.09ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img159.svg"
 ALT="$\displaystyle \infessentiel_{x \in Z(\alpha)} f(x) \ge \alpha$">|; 

$key = q/displaystyleint_A(-f(x))dmu(x)=-int_Af(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \int_A (-f(x))  d\mu(x) = - \int_A f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_A[f(x)+g(x)]dmu(x)=int_Af(x)dmu(x)+int_Ag(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img288.svg"
 ALT="$\displaystyle \int_A [f(x) + g(x)]  d\mu(x) = \int_A f(x)  d\mu(x) + \int_A g(x)  d\mu(x)$">|; 

$key = q/displaystyleint_A[f(x)+g(x)]dmu(x)leint_Af(x)dmu(x)+int_Ag(x)dmu(x)+2epsiloncdotmu(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img285.svg"
 ALT="$\displaystyle \int_A [f(x) + g(x)]  d\mu(x) \le \int_A f(x)  d\mu(x) + \int_A g(x)  d\mu(x) + 2 \epsilon \cdot \mu(A)$">|; 

$key = q/displaystyleint_A[f(x)+g(x)]dmu(x)leint_Af(x)dmu(x)+int_Ag(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img286.svg"
 ALT="$\displaystyle \int_A [f(x) + g(x)]  d\mu(x) \le \int_A f(x)  d\mu(x) + \int_A g(x)  d\mu(x)$">|; 

$key = q/displaystyleint_A[f(x)-g(x)]dmu(x)=int_Af(x)dmu(x)-int_Ag(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img319.svg"
 ALT="$\displaystyle \int_A [f(x) - g(x)]  d\mu(x) = \int_A f(x)  d\mu(x) - \int_A g(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)cdotindicatrice_C(x)dmu(x)-epsilonleint_Au(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle \int_A f(x) \cdot \indicatrice_C(x)  d\mu(x) - \epsilon \le \int_A u(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)cdotindicatrice_C(x)dmu(x)-epsilonleint_Cf(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle \int_A f(x) \cdot \indicatrice_C(x)  d\mu(x) - \epsilon \le \int_C f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)cdotindicatrice_C(x)dmu(x)=int_Cf(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle \int_A f(x) \cdot \indicatrice_C(x)  d\mu(x) = \int_C f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)+int_Ag(x)dmu(x)-2epsilonleint_A[f(x)+g(x)]dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img277.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) + \int_A g(x)  d\mu(x) - 2 \epsilon \le \int_A [f(x) + g(x)]  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)+int_Ag(x)dmu(x)leint_A[f(x)+g(x)]dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img278.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) + \int_A g(x)  d\mu(x) \le \int_A [f(x) + g(x)]  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) = +\infty$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=-infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) = -\infty$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) = 0$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=int_Af^+(x)dmu(x)-int_Af^-(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) = \int_A f^+(x)  d\mu(x) - \int_A f^-(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=int_Ag(x)dmu(x)+int_Ah(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img317.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) = \int_A g(x)  d\mu(x) + \int_A h(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=int_Ag(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) = \int_A g(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=int_{AsetminusB}f(x)dmu(x)+int_{AcapB}f(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img148.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) = \int_{A \setminus B} f(x)  d\mu(x) + \int_{A \cap B} f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=int_{AsetminusC}f(x)dmu(x)+int_Cf(x)dmu(x)geint_Cf(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) = \int_{A \setminus C} f(x)  d\mu(x) + \int_C f(x)  d\mu(x) \ge \int_C f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=supleft{int_Aw(x)dmu(x):winmathcal{E}_A(f)right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) = \sup \left\{ \int_A w(x)  d\mu(x)  : w \in \mathcal{E}_A(f) \right\}$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=supmathcal{I}_A(f)=infmathcal{J}_A(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) = \sup \mathcal{I}_A(f) = \inf \mathcal{J}_A(f)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)geint_A0dmu(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) \ge \int_A 0  d\mu(x) = 0$">|; 

$key = q/displaystyleint_Af(x)dmu(x)geint_Aepsiloncdotindicatrice_C(x)dmu(x)=epsiloncdotmu(C)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) \ge \int_A \epsilon \cdot \indicatrice_C(x)  d\mu(x) = \epsilon \cdot \mu(C) \strictsuperieur 0$">|; 

$key = q/displaystyleint_Af(x)dmu(x)gemu(A)cdotinfessentiel_{xinA}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) \ge \mu(A) \cdot \infessentiel_{x \in A} f(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)leint_Ag(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) \le \int_A g(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)leint_Aw(x)dmu(x)+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img172.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) \le \int_A w(x)  d\mu(x) + \epsilon$">|; 

$key = q/displaystyleint_Af(x)dmu(x)lemu(A)cdotsupessentiel_{xinA}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.56ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) \le \mu(A) \cdot \supessentiel_{x \in A} f(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)strictinferieur+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) \strictinferieur +\infty$">|; 

$key = q/displaystyleint_Af^+(x)dmu(x)-int_Af^-(x)dmu(x)leint_Ag^+(x)dmu(x)-int_Ag^-(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \int_A f^+(x)  d\mu(x) - \int_A f^-(x)  d\mu(x) \le \int_A g^+(x)  d\mu(x) - \int_A g^-(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Ag(x)dmu(x)=int_Af^-(x)dmu(x)-int_Af^+(x)dmu(x)=-int_Af(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle \int_A g(x)  d\mu(x) = \int_A f^-(x)  d\mu(x) - \int_A f^+(x)  d\mu(x) = - \int_A f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Ah(x)dmu(x)=int_Af(x)dmu(x)-int_Ag(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img318.svg"
 ALT="$\displaystyle \int_A h(x)  d\mu(x) = \int_A f(x)  d\mu(x) - \int_A g(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Alambdacdotf(x)dmu(x)=lambdacdotint_Af(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img261.svg"
 ALT="$\displaystyle \int_A \lambda \cdot f(x)  d\mu(x) = \lambda \cdot \int_A f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Alambdacdotw(x)dmu(x)=lambdacdotint_Aw(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img259.svg"
 ALT="$\displaystyle \int_A \lambda \cdot w(x)  d\mu(x) = \lambda \cdot \int_A w(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Au(x)dmu(x)=0+int_Cu(x)dmu(x)=int_Cw(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \int_A u(x)  d\mu(x) = 0 + \int_C u(x)  d\mu(x) = \int_C w(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Au(x)dmu(x)=int_{AsetminusC}u(x)dmu(x)+int_Cu(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle \int_A u(x)  d\mu(x) = \int_{A \setminus C} u(x)  d\mu(x) + \int_C u(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Au(x)dmu(x)geint_Af(x)dmu(x)-epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img269.svg"
 ALT="$\displaystyle \int_A u(x)  d\mu(x) \ge \int_A f(x)  d\mu(x) - \epsilon$">|; 

$key = q/displaystyleint_Au(x)dmu(x)leint_Af(x)cdotindicatrice_C(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle \int_A u(x)  d\mu(x) \le \int_A f(x) \cdot \indicatrice_C(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Au(x)dmu(x)leint_Cu(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle \int_A u(x)  d\mu(x) \le \int_C u(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Av(x)dmu(x)geint_Ag(x)dmu(x)-epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img271.svg"
 ALT="$\displaystyle \int_A v(x)  d\mu(x) \ge \int_A g(x)  d\mu(x) - \epsilon$">|; 

$key = q/displaystyleint_Cf(x)dmu(x)-epsilonleint_Af(x)cdotindicatrice_C(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle \int_C f(x)  d\mu(x) - \epsilon \le \int_A f(x) \cdot \indicatrice_C(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Cf(x)dmu(x)-epsilonleint_Cw(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\displaystyle \int_C f(x)  d\mu(x) - \epsilon \le \int_C w(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Cf(x)dmu(x)geint_Af(x)cdotindicatrice_C(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle \int_C f(x)  d\mu(x) \ge \int_A f(x) \cdot \indicatrice_C(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Cf(x)dmu(x)leint_Af(x)cdotindicatrice_C(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle \int_C f(x)  d\mu(x) \le \int_A f(x) \cdot \indicatrice_C(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Cu(x)dmu(x)leint_Cf(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle \int_C u(x)  d\mu(x) \le \int_C f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_P[s^+(x)-s^-(x)]dmu(x)=int_Ps^+(x)dmu(x)-int_Ps^-(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img326.svg"
 ALT="$\displaystyle \int_P [s^+(x) - s^-(x)]  d\mu(x) = \int_P s^+(x)  d\mu(x) - \int_P s^-(x)  d\mu(x)$">|; 

$key = q/displaystyleint_{AcupB}f(x)dmu(x)-epsilonleint_{AcupB}w(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle \int_{A \cup B} f(x)  d\mu(x) - \epsilon \le \int_{A \cup B} w(x)  d\mu(x)$">|; 

$key = q/displaystyleint_{AcupB}f(x)dmu(x)=int_Af(x)dmu(x)+int_Bf(x)dmu(x)-int_{AcapB}f(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img150.svg"
 ALT="$\displaystyle \int_{A \cup B} f(x)  d\mu(x) = \int_A f(x)  d\mu(x) + \int_B f(x)  d\mu(x) - \int_{A \cap B} f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_{AcupB}f(x)dmu(x)=int_Af(x)dmu(x)+int_Bf(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle \int_{A \cup B} f(x)  d\mu(x) = \int_A f(x)  d\mu(x) + \int_B f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_{AcupB}f(x)dmu(x)=int_{AsetminusB}f(x)dmu(x)+int_Bf(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\displaystyle \int_{A \cup B} f(x)  d\mu(x) = \int_{A \setminus B} f(x)  d\mu(x) + \int_B f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_{AcupB}f(x)dmu(x)geint_Af(x)dmu(x)+int_Bf(x)dmu(x)-2epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle \int_{A \cup B} f(x)  d\mu(x) \ge \int_A f(x)  d\mu(x) + \int_B f(x)  d\mu(x) - 2 \epsilon$">|; 

$key = q/displaystyleint_{AcupB}f(x)dmu(x)geint_Af(x)dmu(x)+int_Bf(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\displaystyle \int_{A \cup B} f(x)  d\mu(x) \ge \int_A f(x)  d\mu(x) + \int_B f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_{AcupB}f(x)dmu(x)geint_{AcupB}[u(x)+v(x)]dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\displaystyle \int_{A \cup B} f(x)  d\mu(x) \ge \int_{A \cup B} [u(x) + v(x)]  d\mu(x)$">|; 

$key = q/displaystyleint_{AcupB}f(x)dmu(x)leint_Af(x)dmu(x)+int_Bf(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\displaystyle \int_{A \cup B} f(x)  d\mu(x) \le \int_A f(x)  d\mu(x) + \int_B f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_{AsetminusB}f(x)dmu(x)=int_Af(x)dmu(x)-int_{AcapB}f(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img149.svg"
 ALT="$\displaystyle \int_{A \setminus B} f(x)  d\mu(x) = \int_A f(x)  d\mu(x) - \int_{A \cap B} f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_{C(alpha)}[f(x)+g(x)]dmu(x)=int_{C(alpha)}f(x)dmu(x)+int_{C(alpha)}g(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img313.svg"
 ALT="$\displaystyle \int_{C(\alpha)} [f(x) + g(x)]  d\mu(x) = \int_{C(\alpha)} f(x)  d\mu(x) + \int_{C(\alpha)} g(x)  d\mu(x)$">|; 

$key = q/displaystyleint_{Z(alpha)}f(x)dmu(x)leint_{C(alpha)}f(x)dmu(x)+int_{Z(alpha)}f(x)dmu(x)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle \int_{Z(\alpha)} f(x)  d\mu(x) \le \int_{C(\alpha)} f(x)  d\mu(x) + \int_{Z(\alpha)} f(x)  d\mu(x) = I$">|; 

$key = q/displaystyleint_{Z(alpha)}f(x)dmu(x)leint_{C(alpha)}w(x)dmu(x)-int_{C(alpha)}f(x)dmu(x)+int_{Z(alpha)}w(x)dmu(x)+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img174.svg"
 ALT="$\displaystyle \int_{Z(\alpha)} f(x)  d\mu(x) \le \int_{C(\alpha)} w(x)  d\mu(x) - \int_{C(\alpha)} f(x)  d\mu(x) + \int_{Z(\alpha)} w(x)  d\mu(x) + \epsilon$">|; 

$key = q/displaystyleint_{Z(alpha)}w(x)dmu(x)lemu(Z(alpha))cdotS(alpha)lemu(Z(alpha))cdotM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img183.svg"
 ALT="$\displaystyle \int_{Z(\alpha)} w(x)  d\mu(x) \le \mu(Z(\alpha)) \cdot S(\alpha) \le \mu(Z(\alpha)) \cdot M$">|; 

$key = q/displaystylelambda=inf_{winmathcal{E}_A(f)}supessentiel_{xinA}[f(x)-w(x)];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img202.svg"
 ALT="$\displaystyle \lambda = \inf_{w \in \mathcal{E}_A(f)} \supessentiel_{x \in A} [f(x) - w(x)]$">|; 

$key = q/displaystylelambdalesupessentiel_{xinA}[f(x)-0]=M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img203.svg"
 ALT="$\displaystyle \lambda \le \supessentiel_{x \in A} [f(x) - 0] = M$">|; 

$key = q/displaystylelim_{alphato+infty}I(alpha)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img192.svg"
 ALT="$\displaystyle \lim_{\alpha \to +\infty} I(\alpha) = 0$">|; 

$key = q/displaystylelim_{alphato+infty}[mu(Z(alpha))cdotM+epsilon]=0cdotM+epsilon=epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img189.svg"
 ALT="$\displaystyle \lim_{\alpha \to +\infty} [\mu(Z(\alpha)) \cdot M + \epsilon] = 0 \cdot M + \epsilon = \epsilon$">|; 

$key = q/displaystylelim_{alphato+infty}int_{C(alpha)}f(x)dmu(x)=int_Af(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img195.svg"
 ALT="$\displaystyle \lim_{\alpha \to +\infty} \int_{C(\alpha)} f(x)  d\mu(x) = \int_A f(x)  d\mu(x)$">|; 

$key = q/displaystylelim_{alphato+infty}int_{C(alpha)}varphi(x)dmu(x)=int_Avarphi(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img308.svg"
 ALT="$\displaystyle \lim_{\alpha \to +\infty} \int_{C(\alpha)} \varphi(x)  d\mu(x) = \int_A \varphi(x)  d\mu(x)$">|; 

$key = q/displaystylelim_{alphato+infty}int_{Z(alpha)}f(x)dmu(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img193.svg"
 ALT="$\displaystyle \lim_{\alpha \to +\infty} \int_{Z(\alpha)} f(x)  d\mu(x) = 0$">|; 

$key = q/displaystylelim_{alphato+infty}mu(Z(alpha))=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img305.svg"
 ALT="$\displaystyle \lim_{\alpha \to +\infty} \mu(Z(\alpha)) = 0$">|; 

$key = q/displaystylelim_{alphato+infty}mu(Z(alpha))=lim_{alphato+infty}mu({xinA:f(x)strictsuperieuralpha})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img164.svg"
 ALT="$\displaystyle \lim_{\alpha \to +\infty} \mu(Z(\alpha)) = \lim_{\alpha \to +\infty} \mu(\{ x \in A : f(x) \strictsuperieur \alpha \}) = 0$">|; 

$key = q/displaystylelim_{alphatoinfty}mu(Z(alpha))=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img168.svg"
 ALT="$\displaystyle \lim_{\alpha \to \infty} \mu(Z(\alpha)) = 0$">|; 

$key = q/displaystylelim_{ntoinfty}supessentiel_{xinA}[f(x)-w_n(x)]=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img250.svg"
 ALT="$\displaystyle \lim_{n \to \infty} \supessentiel_{x \in A} [f(x) - w_n(x)] = 0$">|; 

$key = q/displaystylelimsup_{alphato+infty}[mu(N(alpha))+mu(T(alpha))]=liminf_{alphato+infty}0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.16ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img303.svg"
 ALT="$\displaystyle \limsup_{\alpha \to +\infty} [\mu(N(\alpha)) + \mu(T(\alpha))] = \liminf_{\alpha \to +\infty} 0 = 0$">|; 

$key = q/displaystylelimsup_{ntoinfty}frac{M}{2^n}=liminf_{ntoinfty}0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.41ex; vertical-align: -2.17ex; " SRC="|."$dir".q|img248.svg"
 ALT="$\displaystyle \limsup_{n \to \infty} \frac{M}{2^n} = \liminf_{n \to \infty} 0 = 0$">|; 

$key = q/displaystylemathcal{E}_A(f)={winetagee(A):wessinferieurf};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \mathcal{E}_A(f) = \{ w \in \etagee(A) : w \essinferieur f \}$">|; 

$key = q/displaystylemathcal{F}_A(f)={winetagee(A):wesssuperieurf};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \mathcal{F}_A(f) = \{ w \in \etagee(A) : w \esssuperieur f \}$">|; 

$key = q/displaystylemathcal{I}_A(f)=left{int_Aw(x)dmu(x):winmathcal{E}_A(f)right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \mathcal{I}_A(f) = \left\{ \int_A w(x)  d\mu(x)  : w \in \mathcal{E}_A(f) \right\}$">|; 

$key = q/displaystylemathcal{J}_A(f)=left{int_Aw(x)dmu(x):winmathcal{F}_A(f)right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \mathcal{J}_A(f) = \left\{ \int_A w(x)  d\mu(x)  : w \in \mathcal{F}_A(f) \right\}$">|; 

$key = q/displaystylemu(Z(alpha))cdotalphaleI;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img161.svg"
 ALT="$\displaystyle \mu(Z(\alpha)) \cdot \alpha \le I$">|; 

$key = q/displaystylemu(Z(alpha))cdotalphalemu(Z(alpha))cdotinfessentiel_{xinZ(alpha)}f(x)leint_{Z(alpha)}f(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img160.svg"
 ALT="$\displaystyle \mu(Z(\alpha)) \cdot \alpha \le \mu(Z(\alpha)) \cdot \infessentiel_{x \in Z(\alpha)} f(x) \le \int_{Z(\alpha)} f(x)  d\mu(x)$">|; 

$key = q/displaystylemu(Z(alpha))lefrac{I}{alpha}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.93ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img163.svg"
 ALT="$\displaystyle \mu(Z(\alpha)) \le \frac{I}{\alpha} \le \epsilon$">|; 

$key = q/displaystylemu({x:f(x)strictinferieur0})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \mu(\{x : f(x) \strictinferieur 0\}) = 0$">|; 

$key = q/displaystylemu({xinA:w(x)strictsuperieurf(x)})=mu({xinA:w(x)-f(x)strictsuperieur0});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \mu(\{ x \in A : w(x) \strictsuperieur f(x) \}) = \mu(\{ x \in A : w(x) - f(x) \strictsuperieur 0 \})$">|; 

$key = q/displaystylesupessentiel_{xinA}[f(x)-w(x)]geinfessentiel_{xinA}[f(x)-w(x)]ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img205.svg"
 ALT="$\displaystyle \supessentiel_{x \in A} [f(x) - w(x)] \ge \infessentiel_{x \in A} [f(x) - w(x)] \ge 0$">|; 

$key = q/displaystylesupessentiel_{xinA}[f(x)-w_1(x)]lefrac{M}{2}=Delta_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.56ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img228.svg"
 ALT="$\displaystyle \supessentiel_{x \in A} [f(x) - w_1(x)] \le \frac{M}{2} = \Delta_0$">|; 

$key = q/displaystylesupessentiel_{xinA}[f(x)-w_m(x)]leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img251.svg"
 ALT="$\displaystyle \supessentiel_{x \in A} [f(x) - w_m(x)] \le \epsilon$">|; 

$key = q/displaystylesupessentiel_{xinA}[f(x)-w_n(x)]leDelta_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img245.svg"
 ALT="$\displaystyle \supessentiel_{x \in A} [f(x) - w_n(x)] \le \Delta_n$">|; 

$key = q/displaystylesupessentiel_{xinA}[f(x)-w_{n-1}(x)]leDelta_{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img232.svg"
 ALT="$\displaystyle \supessentiel_{x \in A} [f(x) - w_{n - 1}(x)] \le \Delta_{n - 1}$">|; 

$key = q/displaystylesupmathcal{I}_A(f)lesupmathcal{I}_A(g);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \sup \mathcal{I}_A(f) \le \sup \mathcal{I}_A(g)$">|; 

$key = q/displaystyleu+vessinferieurfcdotindicatrice_A+fcdotindicatrice_B=fcdot(indicatrice_A+indicatrice_B)=f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\displaystyle u + v \essinferieur f \cdot \indicatrice_A + f \cdot \indicatrice_B = f \cdot (\indicatrice_A + \indicatrice_B) = f$">|; 

$key = q/displaystylew=wcdot(indicatrice_A+indicatrice_B)=wcdotindicatrice_A+wcdotindicatrice_B=u+v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\displaystyle w = w \cdot (\indicatrice_A + \indicatrice_B) = w \cdot \indicatrice_A + w \cdot \indicatrice_B = u + v$">|; 

$key = q/epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\epsilon$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/f+g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img287.svg"
 ALT="$f + g$">|; 

$key = q/f+gessinferieuru+v+2epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img282.svg"
 ALT="$f + g \essinferieur u + v + 2 \epsilon$">|; 

$key = q/f,g:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img266.svg"
 ALT="$f,g : A \mapsto \setR$">|; 

$key = q/f,g:AtosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img45.svg"
 ALT="$f,g : A \to \setR$">|; 

$key = q/f,g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img54.svg"
 ALT="$f,g$">|; 

$key = q/f-w_1=f-w_0leDelta_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img226.svg"
 ALT="$f - w_1 = f - w_0 \le \Delta_0$">|; 

$key = q/f-w_n=f-w_{n-1}leDelta_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img243.svg"
 ALT="$f - w_n = f - w_{n - 1} \le \Delta_n$">|; 

$key = q/f-wesssuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img204.svg"
 ALT="$f - w \esssuperieur 0$">|; 

$key = q/f:AcupBmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img144.svg"
 ALT="$f : A \cup B \mapsto \setR$">|; 

$key = q/f:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f : A \mapsto \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$f$">|; 

$key = q/f=f^+-f^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img98.svg"
 ALT="$f = f^+ - f^-$">|; 

$key = q/f=g+h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img316.svg"
 ALT="$f = g + h$">|; 

$key = q/f^+,f^-:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img21.svg"
 ALT="$f^+,f^- : A \mapsto \setR$">|; 

$key = q/f^+,f^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$f^+,f^-$">|; 

$key = q/f^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img25.svg"
 ALT="$f^+$">|; 

$key = q/f^+essinferieurg^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.50ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img56.svg"
 ALT="$f^+ \essinferieur g^+$">|; 

$key = q/f^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img26.svg"
 ALT="$f^-$">|; 

$key = q/fcdotindicatrice_A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img111.svg"
 ALT="$f \cdot \indicatrice_A$">|; 

$key = q/fcdotindicatrice_B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img112.svg"
 ALT="$f \cdot \indicatrice_B$">|; 

$key = q/fcdotindicatrice_C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img96.svg"
 ALT="$f \cdot \indicatrice_C$">|; 

$key = q/fessegal0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$f \essegal 0$">|; 

$key = q/fessegalg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img59.svg"
 ALT="$f \essegal g$">|; 

$key = q/fessinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img43.svg"
 ALT="$f \essinferieur 0$">|; 

$key = q/fessinferieurepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img42.svg"
 ALT="$f \essinferieur \epsilon$">|; 

$key = q/fessinferieurg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img46.svg"
 ALT="$f \essinferieur g$">|; 

$key = q/fessinferieuru+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img280.svg"
 ALT="$f \essinferieur u + \epsilon$">|; 

$key = q/fesssuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img2.svg"
 ALT="$f \esssuperieur 0$">|; 

$key = q/fesssuperieuralpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img157.svg"
 ALT="$f \esssuperieur \alpha$">|; 

$key = q/fesssuperieurg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img61.svg"
 ALT="$f \esssuperieur g$">|; 

$key = q/finetagee(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$f \in \etagee(A)$">|; 

$key = q/fstrictsuperieurepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img37.svg"
 ALT="$f \strictsuperieur \epsilon$">|; 

$key = q/g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img60.svg"
 ALT="$g$">|; 

$key = q/g=-f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img71.svg"
 ALT="$g = -f$">|; 

$key = q/g=g^+-g^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img320.svg"
 ALT="$g = g^+ - g^-$">|; 

$key = q/g^-essinferieurf^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.50ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img57.svg"
 ALT="$g^- \essinferieur f^-$">|; 

$key = q/gessinferieurv+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img281.svg"
 ALT="$g \essinferieur v + \epsilon$">|; 

$key = q/h=f-gesssuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img315.svg"
 ALT="$h = f - g \esssuperieur 0$">|; 

$key = q/indicatrice[AcapB]=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\indicatrice[A \cap B] = 0$">|; 

$key = q/intwleintf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.77ex; vertical-align: -0.81ex; " SRC="|."$dir".q|img177.svg"
 ALT="$\int w \le \int f$">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img254.svg"
 ALT="$\lambda$">|; 

$key = q/lambda=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img210.svg"
 ALT="$\lambda = 0$">|; 

$key = q/lambda=infThetage0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img207.svg"
 ALT="$\lambda = \inf \Theta \ge 0$">|; 

$key = q/lambdacdotwinmathcal{E}_A(lambdacdotf);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img258.svg"
 ALT="$\lambda \cdot w \in \mathcal{E}_A(\lambda \cdot f)$">|; 

$key = q/lambdainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img265.svg"
 ALT="$\lambda \in \setR$">|; 

$key = q/lambdale0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img209.svg"
 ALT="$\lambda \le 0$">|; 

$key = q/lambdastrictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img262.svg"
 ALT="$\lambda \strictinferieur 0$">|; 

$key = q/lambdastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img256.svg"
 ALT="$\lambda \strictsuperieur 0$">|; 

$key = q/limsup=lim;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img190.svg"
 ALT="$\limsup = \lim$">|; 

$key = q/mathcal{E}_A(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\mathcal{E}_A(f)$">|; 

$key = q/mathcal{E}_A(f)subseteqmathcal{E}_A(g);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\mathcal{E}_A(f) \subseteq \mathcal{E}_A(g)$">|; 

$key = q/mathcal{I}_A(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\mathcal{I}_A(f)$">|; 

$key = q/mathcal{I}_A(f)subseteqmathcal{I}_A(g);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\mathcal{I}_A(f) \subseteq \mathcal{I}_A(g)$">|; 

$key = q/mgen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img253.svg"
 ALT="$m \ge n$">|; 

$key = q/minsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img252.svg"
 ALT="$m \in \setN$">|; 

$key = q/mu(A)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\mu(A) = 0$">|; 

$key = q/mu(A)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img198.svg"
 ALT="$\mu(A) \strictsuperieur 0$">|; 

$key = q/mu(AcapB)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\mu(A \cap B) = 0$">|; 

$key = q/mu(Z)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\mu(Z) = 0$">|; 

$key = q/n-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img229.svg"
 ALT="$n - 1$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img233.svg"
 ALT="$n$">|; 

$key = q/s^+-s^-essinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.50ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img327.svg"
 ALT="$s^+ - s^- \essinferieur 0$">|; 

$key = q/s^+-s^-esssuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.50ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img324.svg"
 ALT="$s^+ - s^- \esssuperieur 0$">|; 

$key = q/s^--s^+esssuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.50ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img328.svg"
 ALT="$s^- - s^+ \esssuperieur 0$">|; 

$key = q/supessentiel_{xinA}[f(x)-w_n(x)];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.57ex; vertical-align: -0.74ex; " SRC="|."$dir".q|img249.svg"
 ALT="$\supessentiel_{x \in A} [f(x) - w_n(x)]$">|; 

$key = q/supmathcal{I}_A(f)=+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\sup \mathcal{I}_A(f) = +\infty$">|; 

$key = q/u+v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img117.svg"
 ALT="$u + v$">|; 

$key = q/u+vessinferieurf+g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img274.svg"
 ALT="$u + v \essinferieur f + g$">|; 

$key = q/u+vinmathcal{E}_A(f+g);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img275.svg"
 ALT="$u + v \in \mathcal{E}_A(f + g)$">|; 

$key = q/u+vinmathcal{E}_{AcupB}(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img118.svg"
 ALT="$u + v \in \mathcal{E}_{A \cup B}(f)$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img82.svg"
 ALT="$u$">|; 

$key = q/u=w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img90.svg"
 ALT="$u = w$">|; 

$key = q/u=wcdotindicatrice_A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img124.svg"
 ALT="$u = w \cdot \indicatrice_A$">|; 

$key = q/uessinferieurf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img272.svg"
 ALT="$u \essinferieur f$">|; 

$key = q/uessinferieurfcdotindicatrice_C=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img81.svg"
 ALT="$u \essinferieur f \cdot \indicatrice_C = 0$">|; 

$key = q/uinmathcal{E}_A(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img268.svg"
 ALT="$u \in \mathcal{E}_A(f)$">|; 

$key = q/uinmathcal{E}_A(fcdotindicatrice_C);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img76.svg"
 ALT="$u \in \mathcal{E}_A(f \cdot \indicatrice_C)$">|; 

$key = q/uinmathcal{E}_{AcupB}(fcdotindicatrice_A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$u \in \mathcal{E}_{A \cup B}(f \cdot \indicatrice_A)$">|; 

$key = q/v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img127.svg"
 ALT="$v$">|; 

$key = q/v=wcdotindicatrice_B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img125.svg"
 ALT="$v = w \cdot \indicatrice_B$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img306.svg"
 ALT="$\varphi$">|; 

$key = q/varphiin{f,g,f+g};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img307.svg"
 ALT="$\varphi \in \{f, g, f + g\}$">|; 

$key = q/vessinferieurg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img273.svg"
 ALT="$v \essinferieur g$">|; 

$key = q/vinmathcal{E}_A(g);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img270.svg"
 ALT="$v \in \mathcal{E}_A(g)$">|; 

$key = q/vinmathcal{E}_{AcupB}(fcdotindicatrice_B);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$v \in \mathcal{E}_{A \cup B}(f \cdot \indicatrice_B)$">|; 

$key = q/w-f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$w - f$">|; 

$key = q/w:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$w : A \mapsto \setR$">|; 

$key = q/w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$w$">|; 

$key = q/w=epsiloncdotindicatrice_C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img39.svg"
 ALT="$w = \epsilon \cdot \indicatrice_C$">|; 

$key = q/w_0+Delta_0strictinferieurf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img220.svg"
 ALT="$w_0 + \Delta_0 \strictinferieur f$">|; 

$key = q/w_0=0inmathcal{E}_A(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img213.svg"
 ALT="$w_0 = 0 \in \mathcal{E}_A(f)$">|; 

$key = q/w_0essinferieurf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img219.svg"
 ALT="$w_0 \essinferieur f$">|; 

$key = q/w_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img216.svg"
 ALT="$w_1$">|; 

$key = q/w_1essinferieurf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img223.svg"
 ALT="$w_1 \essinferieur f$">|; 

$key = q/w_1inmathcal{E}_A(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img224.svg"
 ALT="$w_1 \in \mathcal{E}_A(f)$">|; 

$key = q/w_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img236.svg"
 ALT="$w_n$">|; 

$key = q/w_nessinferieurf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img240.svg"
 ALT="$w_n \essinferieur f$">|; 

$key = q/w_ninmathcal{E}_A(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img241.svg"
 ALT="$w_n \in \mathcal{E}_A(f)$">|; 

$key = q/w_{n-1}+Delta_nstrictinferieurf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img237.svg"
 ALT="$w_{n - 1} + \Delta_n \strictinferieur f$">|; 

$key = q/w_{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img230.svg"
 ALT="$w_{n - 1}$">|; 

$key = q/w_{n-1}essinferieurf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img231.svg"
 ALT="$w_{n - 1} \essinferieur f$">|; 

$key = q/wessinferieurf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img40.svg"
 ALT="$w \essinferieur f$">|; 

$key = q/wessinferieurg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img48.svg"
 ALT="$w \essinferieur g$">|; 

$key = q/winmathcal{E}_A(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$w \in \mathcal{E}_A(f)$">|; 

$key = q/winmathcal{E}_A(f)Leftrightarrowlambdacdotwinmathcal{E}_A(lambdacdotf);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img260.svg"
 ALT="$w \in \mathcal{E}_A(f) \Leftrightarrow \lambda \cdot w \in \mathcal{E}_A(\lambda \cdot f)$">|; 

$key = q/winmathcal{E}_A(g);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$w \in \mathcal{E}_A(g)$">|; 

$key = q/winmathcal{E}_C(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img87.svg"
 ALT="$w \in \mathcal{E}_C(f)$">|; 

$key = q/winmathcal{E}_{AcupB}(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img122.svg"
 ALT="$w \in \mathcal{E}_{A \cup B}(f)$">|; 

$key = q/wlefLeftrightarrowlambdacdotwlelambdacdotf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img257.svg"
 ALT="$w \le f \Leftrightarrow \lambda \cdot w \le \lambda \cdot f$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img23.svg"
 ALT="$x \in A$">|; 

$key = q/xinAcupB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img107.svg"
 ALT="$x \in A \cup B$">|; 

$key = q/xinN(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img297.svg"
 ALT="$x \in N(\alpha)$">|; 

$key = q/xinN(beta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img298.svg"
 ALT="$x \in N(\beta)$">|; 

$key = q/xinT(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img300.svg"
 ALT="$x \in T(\alpha)$">|; 

$key = q/xinT(beta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img301.svg"
 ALT="$x \in T(\beta)$">|; 

$key = q/xinZ(alpha);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img296.svg"
 ALT="$x \in Z(\alpha)$">|; 

$key = q/xinZ(beta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img299.svg"
 ALT="$x \in Z(\beta)$">|; 

$key = q/{C(alpha)insousens(A):alphainsetR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img165.svg"
 ALT="$\{ C(\alpha) \in \sousens(A) : \alpha \in \setR \}$">|; 

$key = q/{Eqts}(fcdotindicatrice_C)^+=f^+cdotindicatrice_C(fcdotindicatrice_C)^-=f^-cdotindicatrice_C{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img100.svg"
 ALT="\begin{Eqts}
(f \cdot \indicatrice_C)^+ = f^+ \cdot \indicatrice_C \\\\
(f \cdot \indicatrice_C)^- = f^- \cdot \indicatrice_C
\end{Eqts}">|; 

$key = q/{Eqts}C(alpha)={xinA:f(x)lealpha}Z(alpha)={xinA:f(x)strictsuperieuralpha}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img153.svg"
 ALT="\begin{Eqts}
C(\alpha) = \{ x \in A : f(x) \le \alpha \} \\\\
Z(\alpha) = \{ x \in A : f(x) \strictsuperieur \alpha \}
\end{Eqts}">|; 

$key = q/{Eqts}F(alpha)={xinA:f(x)lealpha}G(alpha)={xinA:g(x)lealpha}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img289.svg"
 ALT="\begin{Eqts}
F(\alpha) = \{ x \in A : f(x) \le \alpha \} \\\\
G(\alpha) = \{ x \in A : g(x) \le \alpha \}
\end{Eqts}">|; 

$key = q/{Eqts}N(alpha)={xinA:f(x)strictsuperieuralpha}=AsetminusF(alpha)T(alpha)={xinA:g(x)strictsuperieuralpha}=AsetminusG(alpha){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img290.svg"
 ALT="\begin{Eqts}
N(\alpha) = \{ x \in A : f(x) \strictsuperieur \alpha \} = A \setmi...
... \{ x \in A : g(x) \strictsuperieur \alpha \} = A \setminus G(\alpha)
\end{Eqts}">|; 

$key = q/{Eqts}P={xinA:s^+(x)-s^-(x)ge0}M={xinA:s^+(x)-s^-(x)strictinferieur0}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img323.svg"
 ALT="\begin{Eqts}
P = \{ x \in A : s^+(x) - s^-(x) \ge 0 \} \\\\
M = \{ x \in A : s^+(x) - s^-(x) \strictinferieur 0 \}
\end{Eqts}">|; 

$key = q/{Eqts}int_Af(x)dmu(x)=int_{C(alpha)}f(x)dmu(x)+int_{Z(alpha)}f(x)dmu(x)int_Aw(x)dmu(x)=int_{C(alpha)}w(x)dmu(x)+int_{Z(alpha)}w(x)dmu(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.78ex; vertical-align: -7.34ex; " SRC="|."$dir".q|img173.svg"
 ALT="\begin{Eqts}
\int_A f(x)  d\mu(x) = \int_{C(\alpha)} f(x)  d\mu(x) + \int_{Z(\...
...) = \int_{C(\alpha)} w(x)  d\mu(x) + \int_{Z(\alpha)} w(x)  d\mu(x)
\end{Eqts}">|; 

$key = q/{Eqts}int_Au(x)dmu(x)leint_Af(x)dmu(x)int_Av(x)dmu(x)leint_Ag(x)dmu(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.01ex; vertical-align: -6.95ex; " SRC="|."$dir".q|img284.svg"
 ALT="\begin{Eqts}
\int_A u(x)  d\mu(x) \le \int_A f(x)  d\mu(x) \ \\\\
\int_A v(x)  d\mu(x) \le \int_A g(x)  d\mu(x)
\end{Eqts}">|; 

$key = q/{Eqts}int_{AcupB}u(x)dmu(x)leint_Af(x)dmu(x)int_{AcupB}v(x)dmu(x)leint_Bf(x)dmu(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.44ex; vertical-align: -5.16ex; " SRC="|."$dir".q|img131.svg"
 ALT="\begin{Eqts}
\int_{A \cup B} u(x)  d\mu(x) \le \int_A f(x)  d\mu(x) \\\\
\int_{A \cup B} v(x)  d\mu(x) \le \int_B f(x)  d\mu(x)
\end{Eqts}">|; 

$key = q/{Eqts}int_{AcupB}u(x)dmu(x)leint_{AcupB}f(x)cdotindicatrice_A(x)dmu(x)int_{AcupB}v(x)dmu(x)leint_{AcupB}f(x)cdotindicatrice_B(x)dmu(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.44ex; vertical-align: -5.16ex; " SRC="|."$dir".q|img130.svg"
 ALT="\begin{Eqts}
\int_{A \cup B} u(x)  d\mu(x) \le \int_{A \cup B} f(x) \cdot \indi...
...  d\mu(x) \le \int_{A \cup B} f(x) \cdot \indicatrice_B(x)  d\mu(x)
\end{Eqts}">|; 

$key = q/{Eqts}lim_{alphato+infty}int_{C(alpha)}f^+(x)dmu(x)=int_Af^+(x)dmu(x)lim_{alphato+infty}int_{C(alpha)}f^-(x)dmu(x)=int_Af^-(x)dmu(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.78ex; vertical-align: -7.34ex; " SRC="|."$dir".q|img196.svg"
 ALT="\begin{Eqts}
\lim_{\alpha \to +\infty} \int_{C(\alpha)} f^+(x)  d\mu(x) = \int_...
... +\infty} \int_{C(\alpha)} f^-(x)  d\mu(x) = \int_A f^-(x)  d\mu(x)
\end{Eqts}">|; 

$key = q/{Eqts}s^+=f^++g^+s^-=f^-+g^-{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img321.svg"
 ALT="\begin{Eqts}
s^+ = f^+ + g^+ \\\\
s^- = f^- + g^-
\end{Eqts}">|; 

$key = q/{Eqts}supessentiel{f(x)-u(x):xinA}leepsilonsupessentiel{g(x)-v(x):xinA)}leepsilon{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img279.svg"
 ALT="\begin{Eqts}
\supessentiel \{ f(x) - u(x) : x \in A \} \le \epsilon \\\\
\supessentiel \{ g(x) - v(x) : x \in A) \} \le \epsilon
\end{Eqts}">|; 

$key = q/{Eqts}supessentiel{f(x):xinA}strictinferieur+inftysupessentiel{g(x):xinA}strictinferieur+infty{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img267.svg"
 ALT="\begin{Eqts}
\supessentiel \{ f(x) : x \in A \} \strictinferieur +\infty \\\\
\supessentiel \{ g(x) : x \in A \} \strictinferieur +\infty
\end{Eqts}">|; 

$key = q/{Eqts}u=cases{w&text{sur}A0&text{sur}Bcases{essinferieurcases{f&text{sur}A0&text{sur}Bcases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img128.svg"
 ALT="\begin{Eqts}
u =
\begin{cases}
w &amp; \text{ sur } A \\\\
0 &amp; \text{ sur } B
\end{ca...
...ur
\begin{cases}
f &amp; \text{ sur } A \\\\
0 &amp; \text{ sur } B
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}u=cases{w&text{sur}C0&text{sur}AsetminusCcases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img89.svg"
 ALT="\begin{Eqts}
u =
\begin{cases}
w &amp; \text{ sur } C \\\\
0 &amp; \text{ sur } A \setminus C
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}uessinferieurcases{f&text{sur}C0&text{sur}AsetminusCcases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img92.svg"
 ALT="\begin{Eqts}
u \essinferieur
\begin{cases}
f &amp; \text{ sur } C \\\\
0 &amp; \text{ sur } A \setminus C
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}v=cases{0&text{sur}Aw&text{sur}Bcases{essinferieurcases{0&text{sur}Af&text{sur}Bcases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img129.svg"
 ALT="\begin{Eqts}
v =
\begin{cases}
0 &amp; \text{ sur } A \\\\
w &amp; \text{ sur } B
\end{ca...
...ur
\begin{cases}
0 &amp; \text{ sur } A \\\\
f &amp; \text{ sur } B
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}w_1=cases{w_0+Delta_0&text{sur}A_0w_0&text{sur}AsetminusA_0cases{essinferieurcases{f&text{sur}A_0f&text{sur}AsetminusA_0cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img222.svg"
 ALT="\begin{Eqts}
w_1 =
\begin{cases}
w_0 + \Delta_0 &amp; \text{ sur } A_0 \\\\
w_0 &amp; \te...
...
f &amp; \text{ sur } A_0 \\\\
f &amp; \text{ sur } A \setminus A_0
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}w_1=w_0+Delta_0cdotindicatrice[A_0]=cases{w_0+Delta_0&text{sur}A_0w_0&text{sur}AsetminusA_0cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img217.svg"
 ALT="\begin{Eqts}
w_1 = w_0 + \Delta_0 \cdot \indicatrice[A_0] =
\begin{cases}
w_0 + ...
... &amp; \text{ sur } A_0 \\\\
w_0 &amp; \text{ sur } A \setminus A_0
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}w_n=cases{w_{n-1}+Delta_n&text{sur}A_nw_{n-1}&text{sur}AsetminusA_ncases{essinferieurcases{f&text{sur}A_nf&text{sur}AsetminusA_ncases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img239.svg"
 ALT="\begin{Eqts}
w_n =
\begin{cases}
w_{n - 1} + \Delta_n &amp; \text{ sur } A_n \\\\
w_{...
...
f &amp; \text{ sur } A_n \\\\
f &amp; \text{ sur } A \setminus A_n
\end{cases}\end{Eqts}">|; 

$key = q/{Z(alpha)insousens(A):alphainsetR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img166.svg"
 ALT="$\{ Z(\alpha) \in \sousens(A) : \alpha \in \setR \}$">|; 

$key = q/{eqnarraystar}f^+(x)&=&max{f(x),0}f^-(x)&=&max{-f(x),0}g^+(x)&=&max{g(x),0}g^-(x)&=&max{-g(x),0}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.32ex; " SRC="|."$dir".q|img55.svg"
 ALT="\begin{eqnarray*}
f^+(x) &amp;=&amp; \max \{ f(x) , 0 \} \\\\
f^-(x) &amp;=&amp; \max \{ -f(x) , ...
...+(x) &amp;=&amp; \max \{ g(x) , 0 \} \\\\
g^-(x) &amp;=&amp; \max \{ -g(x) , 0 \}
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}f^+(x)&=&max{f(x),0}ge0f^-(x)&=&max{-f(x),0}ge0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img22.svg"
 ALT="\begin{eqnarray*}
f^+(x) &amp;=&amp; \max \{ f(x) , 0 \} \ge 0 \\\\
f^-(x) &amp;=&amp; \max \{ -f(x) , 0 \} \ge 0
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}g^+(x)&=&max{g(x),0}=max{-f(x),0}=f^-(x)g^-(x)&=&max{-g(x),0}=max{f(x),0}=f^+(x){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img72.svg"
 ALT="\begin{eqnarray*}
g^+(x) &amp;=&amp; \max \{ g(x) , 0 \} = \max \{ -f(x) , 0 \} = f^-(x) \\\\
g^-(x) &amp;=&amp; \max \{ -g(x) , 0 \} = \max \{ f(x) , 0 \} = f^+(x)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}int_A[f(x)+g(x)]dmu(x)&le&int_A[u(x)+v(x)+2epsilon]dmu(x)&le&int_Au(x)dmu(x)+int_Av(x)dmu(x)+2epsiloncdotmu(A){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.27ex; " SRC="|."$dir".q|img283.svg"
 ALT="\begin{eqnarray*}
\int_A [f(x) + g(x)]  d\mu(x) &amp;\le&amp; \int_A [u(x) + v(x) + 2 \...
...u(x)  d\mu(x) + \int_A v(x)  d\mu(x) + 2 \epsilon \cdot \mu(A)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}int_Af(x)dmu(x)&=&int_Cf(x)dmu(x)+int_Zf(x)dmu(x)&=&int_Cf(x)dmu(x)+0&=&int_Cf(x)dmu(x){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 22.28ex; " SRC="|."$dir".q|img142.svg"
 ALT="\begin{eqnarray*}
\int_A f(x)  d\mu(x) &amp;=&amp; \int_C f(x)  d\mu(x) + \int_Z f(x) ...
... \\\\
&amp;=&amp; \int_C f(x)  d\mu(x) + 0 \\\\
&amp;=&amp; \int_C f(x)  d\mu(x)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}int_Au(x)dmu(x)+int_Av(x)dmu(x)&=&int_A[u(x)+v(x)]dmu(x)&le&int_A[f(x)+g(x)]dmu(x){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.27ex; " SRC="|."$dir".q|img276.svg"
 ALT="\begin{eqnarray*}
\int_A u(x)  d\mu(x) + \int_A v(x)  d\mu(x) &amp;=&amp; \int_A [u(x) + v(x)]  d\mu(x) \\\\
&amp;\le&amp; \int_A [f(x) + g(x)]  d\mu(x)
\end{eqnarray*}">|; 

$key = q/{w_n:ninsetN}subseteqmathcal{E}_A(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img212.svg"
 ALT="$\{ w_n : n \in \setN \} \subseteq \mathcal{E}_A(f)$">|; 

1;

