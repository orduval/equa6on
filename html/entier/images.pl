# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$(i,j)$">|; 

$key = q/(i,j)=i-j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$(i,j) = i - j$">|; 

$key = q/(i,j)insetN^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$(i,j) \in \setN^2$">|; 

$key = q/(setZ,+,cdot);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img191.svg"
 ALT="$(\setZ,+,\cdot)$">|; 

$key = q/-u,-v,-w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img150.svg"
 ALT="$-u,-v,-w$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img111.svg"
 ALT="$0$">|; 

$key = q/0=0-0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img118.svg"
 ALT="$0 = 0 - 0$">|; 

$key = q/1=1-0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img158.svg"
 ALT="$1 = 1 - 0$">|; 

$key = q/D(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$D(i,j)$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$X$">|; 

$key = q/X^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$X^2$">|; 

$key = q/Y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$Y$">|; 

$key = q/displaystyle((-u)cdotv)cdotw=(-(ucdotv))cdotw=-((ucdotv)cdotw);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle ((-u) \cdot v) \cdot w = (- (u \cdot v)) \cdot w = - ((u \cdot v) \cdot w)$">|; 

$key = q/displaystyle((-u)cdotv)cdotw=-(ucdot(vcdotw))=(-u)cdot(vcdotw);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\displaystyle ((-u) \cdot v) \cdot w = - (u \cdot (v \cdot w)) = (-u) \cdot (v \cdot w)$">|; 

$key = q/displaystyle(-1)cdot(-1)=-(-1)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img161.svg"
 ALT="$\displaystyle (-1) \cdot (-1) = -(-1) = 1$">|; 

$key = q/displaystyle(-1)cdot(i,j)=-(1cdot(i,j))=-(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img160.svg"
 ALT="$\displaystyle (-1) \cdot (i,j) = - (1 \cdot (i,j)) = - (i,j)$">|; 

$key = q/displaystyle(-i)-(-j)=(-i)+j=j+(-i)=j-i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle (-i) - (-j) = (-i) + j = j + (-i) = j - i$">|; 

$key = q/displaystyle(-i)-j=(-i)+(-j)=-(i+j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle (-i) - j = (-i) + (-j) = - ( i + j )$">|; 

$key = q/displaystyle(-m)diventiere(-n)=k=mdiventieren;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img182.svg"
 ALT="$\displaystyle (-m) \diventiere (-n) = k = m \diventiere n$">|; 

$key = q/displaystyle(-m)diventieren=-k=-(mdiventieren);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img185.svg"
 ALT="$\displaystyle (-m) \diventiere n = -k = -(m \diventiere n)$">|; 

$key = q/displaystyle(-m)modulo(-n)=-r=-(mmodulon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img183.svg"
 ALT="$\displaystyle (-m) \modulo (-n) = -r = - (m \modulo n)$">|; 

$key = q/displaystyle(-m)modulon=-r=-(mmodulon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img186.svg"
 ALT="$\displaystyle (-m) \modulo n = -r = - (m \modulo n)$">|; 

$key = q/displaystyle(-u)+(-v)=-(u+v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle (-u) + (-v) = - ( u + v)$">|; 

$key = q/displaystyle(-u)cdot(-v)=-(ucdot(-v))=-(-(ucdotv))=ucdotv;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\displaystyle (-u) \cdot (-v) = - (u \cdot (-v)) = - ( - (u \cdot v)) = u \cdot v$">|; 

$key = q/displaystyle(-u)cdotv=-(ucdotv);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\displaystyle (-u) \cdot v = - (u \cdot v)$">|; 

$key = q/displaystyle(0,j-i)=(0,j-i)+(i,i)=(i,j-i+i)=(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle (0, j - i) = (0, j - i) + (i,i) = (i, j - i + i) = (i,j)$">|; 

$key = q/displaystyle(i+m)-(j+n)le(k+r)-(l+s);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle (i + m) - (j + n) \le (k + r) - (l + s)$">|; 

$key = q/displaystyle(i,j)+(j,i)=(i+j,i+j)=(0,0)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle (i,j) + (j,i) = (i + j, i + j) = (0,0) = 0$">|; 

$key = q/displaystyle(i,j)+(k,l)=(i+j,k+l);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle (i,j) + (k,l) = (i + j, k + l)$">|; 

$key = q/displaystyle(i,j)=(i+n,j+n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle (i,j) = (i + n, j + n)$">|; 

$key = q/displaystyle(i,j)cdot(k,l)=(icdotk+jcdotl,icdotl+jcdotk);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\displaystyle (i,j) \cdot (k,l) = (i \cdot k + j \cdot l, i \cdot l + j \cdot k)$">|; 

$key = q/displaystyle(i-j)+(0-0)=(i+0)-(j+0)=i-j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle (i - j) + (0 - 0) = (i + 0) - (j + 0) = i - j$">|; 

$key = q/displaystyle(i-j)+(j-i)=(i+j)-(j+i)=(i+j)-(i+j)=0-0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle (i - j) + (j - i) = (i + j) - (j + i) = (i + j) - (i + j) = 0 - 0 = 0$">|; 

$key = q/displaystyle(i-j)+(k-l)=(i+j)-k-l=(i+j)-(k+l);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle (i - j) + (k - l) = (i + j) - k - l = (i + j) - (k + l)$">|; 

$key = q/displaystyle(i-j)+j+lle(k-l)+j+l;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle (i - j) + j + l \le (k - l) + j + l$">|; 

$key = q/displaystyle(i-j)cdot1=(i-j)cdot(1-0)=(icdot1+jcdot0)-(icdot0+jcdot1)=i-j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle (i - j) \cdot 1 = (i - j) \cdot (1 - 0) = (i \cdot 1 + j \cdot 0) - (i \cdot 0 + j \cdot 1) = i - j$">|; 

$key = q/displaystyle(i-j,0)=(i-j,0)+(j,j)=(i-j+j,j)=(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle (i - j, 0) = (i - j, 0) + (j,j) = (i - j + j, j) = (i,j)$">|; 

$key = q/displaystyle(i_0,i_1,i_2,...i_{n-1},i_n)in{0,1,2,3,4,5,6,7,8,9}^{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img169.svg"
 ALT="$\displaystyle (i_0,i_1,i_2,...i_{n - 1},i_n) \in \{ 0,1,2,3,4,5,6,7,8,9 \}^{n + 1}$">|; 

$key = q/displaystyle(u+v)+((-u)+(-v))=((i-j)+(k-l))+((j-i)+(l-k));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle (u + v) + ((-u) + (-v)) = ((i - j) + (k - l)) + ((j - i) + (l - k))$">|; 

$key = q/displaystyle(u+v)+((-u)+(-v))=(i+k+j+l)-(j+l+i+k)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle (u + v) + ((-u) + (-v)) = (i + k + j + l) - (j + l + i + k) = 0$">|; 

$key = q/displaystyle(ucdotv)cdot(-w)=-((ucdotv)cdotw);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle (u \cdot v) \cdot (-w) = - ((u \cdot v) \cdot w)$">|; 

$key = q/displaystyle(ucdotv)cdotw=(icdotj)cdot(k-0)=icdotjcdotk-0=icdotjcdotk;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\displaystyle (u \cdot v) \cdot w = (i \cdot j) \cdot (k - 0) = i \cdot j \cdot k - 0 = i \cdot j \cdot k$">|; 

$key = q/displaystyle(ucdotv)cdotw=-(ucdot(vcdotw))=ucdot(-(vcdotw))=ucdot(vcdot(-w));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\displaystyle (u \cdot v) \cdot w = - (u \cdot (v \cdot w)) = u \cdot (-(v \cdot w)) = u \cdot (v \cdot (-w))$">|; 

$key = q/displaystyle(ucdotv)cdotw=ucdot(vcdotw);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img149.svg"
 ALT="$\displaystyle (u \cdot v) \cdot w = u \cdot (v \cdot w)$">|; 

$key = q/displaystyle-(-(i,j))=-(j,i)=(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle - ( - (i,j)) = - (j,i) = (i,j)$">|; 

$key = q/displaystyle-(-(i-j))=-(j-i)=i-j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle - ( - (i - j)) = - (j - i) = i - j$">|; 

$key = q/displaystyle-(-1)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle -(-1) = 1$">|; 

$key = q/displaystyle-(1-0)=0-1=-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle -(1 - 0) = 0 - 1 = -1$">|; 

$key = q/displaystyle-(i,j)=(j,i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle - (i,j) = (j,i)$">|; 

$key = q/displaystyle-(i-j)=j-i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle - (i - j) = j - i$">|; 

$key = q/displaystyle-(u+v)=-(u+v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle -(u + v) = -(u + v)$">|; 

$key = q/displaystyle-(u+v)le-(u+v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle -(u + v) \le -(u + v)$">|; 

$key = q/displaystyle-7512=-parentheses{2+1cdot10+5cdot10^2+7cdot10^3};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img172.svg"
 ALT="$\displaystyle -7512 = -\parentheses{2 + 1 \cdot 10 + 5 \cdot 10^2 + 7 \cdot 10^3}$">|; 

$key = q/displaystyle-i_ni_{n-1}...i_2i_1i_0=-parentheses{i_0+i_1cdot10+i_2cdot10^2+...+i_{n-1}cdot10^{n-1}+i_ncdot10^n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img171.svg"
 ALT="$\displaystyle -i_n i_{n - 1} ... i_2 i_1 i_0 = -\parentheses{i_0 + i_1 \cdot 10 + i_2 \cdot 10^2 + ... + i_{n - 1} \cdot 10^{n - 1} + i_n \cdot 10^n}$">|; 

$key = q/displaystyle-m=(-k)cdotn-r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img184.svg"
 ALT="$\displaystyle -m = (-k) \cdot n - r$">|; 

$key = q/displaystyle-m=kcdot(-n)-r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img181.svg"
 ALT="$\displaystyle -m = k \cdot (-n) - r$">|; 

$key = q/displaystyle-u+v=(-u)+v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle - u + v = (-u) + v$">|; 

$key = q/displaystyle-u-v=(-u)-v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle - u - v = (-u) - v$">|; 

$key = q/displaystyle-ucdotv=-(ucdotv);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img159.svg"
 ALT="$\displaystyle - u \cdot v = - (u \cdot v)$">|; 

$key = q/displaystyle-uge-v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle -u \ge -v$">|; 

$key = q/displaystyle-v=u-u-vlev-u-v=-u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle -v = u - u - v \le v - u - v = -u$">|; 

$key = q/displaystyle-zge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle -z \ge 0$">|; 

$key = q/displaystyle-zle0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle -z \le 0$">|; 

$key = q/displaystyle0=0-0=n-n=(0,0)=(n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle 0 = 0 - 0 = n - n = (0,0) = (n,n)$">|; 

$key = q/displaystyle1cdot(i-j)=(i-j)cdot1=i-j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle 1 \cdot (i - j) = (i - j) \cdot 1 = i - j$">|; 

$key = q/displaystyleD(i,j)={(i+n,j+n):ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle D(i,j) = \{ (i + n, j + n) : n \in \setN \}$">|; 

$key = q/displaystyleabs{x+y}leabs{x}+abs{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\displaystyle \abs{x + y} \le \abs{x} + \abs{y}$">|; 

$key = q/displaystyleabs{z}=cases{z&text{si}zge0-z&text{si}zstrictinferieur0cases{;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle \abs{z} =
\begin{cases}
z &amp; \text{si }  z \ge 0 \\\\
-z &amp; \text{si }  z \strictinferieur 0
\end{cases}$">|; 

$key = q/displaystyleabs{z}=max{z,-z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle \abs{z} = \max \{ z , -z \}$">|; 

$key = q/displaystyleabs{z}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\displaystyle \abs{z} \ge 0$">|; 

$key = q/displaystylef(n)=(n,0)=n-0=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle f(n) = (n,0) = n - 0 = n$">|; 

$key = q/displaystylei+0ge0+0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle i + 0 \ge 0 + 0$">|; 

$key = q/displaystylei+l+m+slej+k+n+r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle i + l + m + s \le j + k + n + r$">|; 

$key = q/displaystylei+llej+k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle i + l \le j + k$">|; 

$key = q/displaystylei+llek+j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle i + l \le k + j$">|; 

$key = q/displaystylei-0ge0-0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle i - 0 \ge 0 - 0 = 0$">|; 

$key = q/displaystylei-j=(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle i - j = (i,j)$">|; 

$key = q/displaystylei-j=(i-j)+0=(i-j)+(n-n)=(i+n)-(j+n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle i - j = (i - j) + 0 = (i - j) + (n - n) = (i + n) - (j + n)$">|; 

$key = q/displaystylei-j=D(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle i - j = D(i,j)$">|; 

$key = q/displaystylei-jge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle i - j \ge 0$">|; 

$key = q/displaystylei-jlek-l;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle i - j \le k - l$">|; 

$key = q/displaystylei_ni_{n-1}...i_2i_1i_0=i_0+i_1cdot10+i_2cdot10^2+...+i_{n-1}cdot10^{n-1}+i_ncdot10^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.65ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img170.svg"
 ALT="$\displaystyle i_n i_{n - 1} ... i_2 i_1 i_0 = i_0 + i_1 \cdot 10 + i_2 \cdot 10^2 + ... + i_{n - 1} \cdot 10^{n - 1} + i_n \cdot 10^n$">|; 

$key = q/displaystyleigej;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle i \ge j$">|; 

$key = q/displaystylem+slen+r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle m + s \le n + r$">|; 

$key = q/displaystylem-nler-s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle m - n \le r - s$">|; 

$key = q/displaystylem=(-k)cdot(-n)+r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img178.svg"
 ALT="$\displaystyle m = (-k) \cdot (-n) + r$">|; 

$key = q/displaystylem=kcdotn+r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img177.svg"
 ALT="$\displaystyle m = k \cdot n + r$">|; 

$key = q/displaystylemdiventiere(-n)=-k=-(mdiventieren);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img179.svg"
 ALT="$\displaystyle m \diventiere (-n) = -k = - (m \diventiere n)$">|; 

$key = q/displaystylemmodulo(-n)=r=mmodulon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img180.svg"
 ALT="$\displaystyle m \modulo (-n) = r = m \modulo n$">|; 

$key = q/displaystylen-n=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle n - n = 0$">|; 

$key = q/displaystylesetZ={...,-5,-4,-3,-2,-1,0,1,2,3,4,5,...};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \setZ = \{ ...,-5,-4,-3,-2,-1,0,1,2,3,4,5,... \}$">|; 

$key = q/displaystylesetZ={i-j:i,jinsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \setZ = \{ i - j : i,j \in \setN \}$">|; 

$key = q/displaystylesetZ^+={i=D(i,0):iinsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \setZ^+ = \{ i = D(i,0) : i \in \setN \}$">|; 

$key = q/displaystylesetZ^+={uinsetZ:uge0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle \setZ^+ = \{ u \in \setZ : u \ge 0 \}$">|; 

$key = q/displaystylesetZ^-={-i=D(0,i):iinsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \setZ^- = \{ -i = D(0,i) : i \in \setN \}$">|; 

$key = q/displaystylesetZ^-={uinsetZ:ule0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle \setZ^- = \{ u \in \setZ : u \le 0 \}$">|; 

$key = q/displaystylesigne(z)=cases{1&text{si}zge0-1&text{si}zstrictinferieur0cases{;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle \signe(z) =
\begin{cases}
1 &amp; \text{si }  z \ge 0 \\\\
-1 &amp; \text{si }  z \strictinferieur 0
\end{cases}$">|; 

$key = q/displaystyleu+(-(u+v))lev+(-(u+v));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle u + (-(u + v)) \le v + (-(u + v))$">|; 

$key = q/displaystyleu+...+u=icdotk-jcdotk=ucdotv;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle u + ... + u = i \cdot k - j \cdot k = u \cdot v$">|; 

$key = q/displaystyleu+w=(i+m)-(j+n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle u + w = (i + m) - (j + n)$">|; 

$key = q/displaystyleu+wlev+z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\displaystyle u + w \le v + z$">|; 

$key = q/displaystyleu-v=u+(-v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle u - v = u + (-v)$">|; 

$key = q/displaystyleu=(i-j,0)insetZ^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle u = (i - j,0) \in \setZ^+$">|; 

$key = q/displaystyleu=i-0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.90ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\displaystyle u = i - 0$">|; 

$key = q/displaystyleu=i-j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle u = i - j$">|; 

$key = q/displaystyleu=i-jlek-l=v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle u = i - j \le k - l = v$">|; 

$key = q/displaystyleucdot(-v)=-(ucdotv);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\displaystyle u \cdot (-v) = -(u \cdot v)$">|; 

$key = q/displaystyleucdot(vcdotw)=(i-0)cdot(jcdotk)=icdotjcdotk-0=icdotjcdotk;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img148.svg"
 ALT="$\displaystyle u \cdot (v \cdot w) = (i - 0) \cdot (j \cdot k) = i \cdot j \cdot k - 0 = i \cdot j \cdot k$">|; 

$key = q/displaystyleucdot0=0cdotu=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\displaystyle u \cdot 0 = 0 \cdot u = 0$">|; 

$key = q/displaystyleucdot1=1cdotu=u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle u \cdot 1 = 1 \cdot u = u$">|; 

$key = q/displaystyleucdotv+(-u)cdotv=(u+(-u))cdotv=0cdotv=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\displaystyle u \cdot v + (-u) \cdot v = (u + (-u)) \cdot v = 0 \cdot v = 0$">|; 

$key = q/displaystyleucdotv+ucdot(-v)=ucdot(v+(-v))=ucdot0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle u \cdot v + u \cdot (-v) = u \cdot (v + (-v)) = u \cdot 0 = 0$">|; 

$key = q/displaystyleucdotv=(i-0)cdot(j-0)=(icdotj+0cdot0)-(icdot0+0cdotj)=icdotj-0=icdotj;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle u \cdot v = (i - 0) \cdot (j - 0) = (i \cdot j + 0 \cdot 0) - (i \cdot 0 + 0 \cdot j) = i \cdot j - 0 = i \cdot j$">|; 

$key = q/displaystyleucdotv=(i-j)cdot(k-0)=(icdotk+jcdot0)-(jcdotk+icdot0)=icdotk-jcdotk;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle u \cdot v = (i - j) \cdot (k - 0) = (i \cdot k + j \cdot 0) - (j \cdot k + i \cdot 0) = i \cdot k - j \cdot k$">|; 

$key = q/displaystyleucdotv=(i-j)cdot(k-l)=(i+(-j))cdot(k+(-l));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\displaystyle u \cdot v = (i - j) \cdot (k - l) = (i + (-j)) \cdot (k + (-l))$">|; 

$key = q/displaystyleucdotv=icdot(k+(-l))+(-j)cdot(k+(-l));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\displaystyle u \cdot v = i \cdot (k + (-l)) + (-j) \cdot (k + (-l))$">|; 

$key = q/displaystyleucdotv=icdotj;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img145.svg"
 ALT="$\displaystyle u \cdot v = i \cdot j$">|; 

$key = q/displaystyleucdotv=icdotk+icdot(-l)+(-j)cdotk+(-j)cdot(-l);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\displaystyle u \cdot v = i \cdot k + i \cdot (-l) + (-j) \cdot k + (-j) \cdot (-l)$">|; 

$key = q/displaystyleucdotv=icdotk-icdotl-jcdotk+jcdotl=(icdotk+jcdotl)-(icdotl+jcdotk);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\displaystyle u \cdot v = i \cdot k - i \cdot l - j \cdot k + j \cdot l = (i \cdot k + j \cdot l) - (i \cdot l + j \cdot k)$">|; 

$key = q/displaystyleucdotvcdotw=(ucdotv)cdotw=ucdot(vcdotw);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img155.svg"
 ALT="$\displaystyle u \cdot v \cdot w = (u \cdot v) \cdot w = u \cdot (v \cdot w)$">|; 

$key = q/displaystyleulev;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle u \le v$">|; 

$key = q/displaystylev+z=(k+r)-(l+s);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle v + z = (k + r) - (l + s)$">|; 

$key = q/displaystylev=j-0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\displaystyle v = j - 0$">|; 

$key = q/displaystylev=k-0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle v = k - 0$">|; 

$key = q/displaystylev=k-l;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle v = k - l$">|; 

$key = q/displaystylevcdotw=jcdotk;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\displaystyle v \cdot w = j \cdot k$">|; 

$key = q/displaystylew=k-0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img144.svg"
 ALT="$\displaystyle w = k - 0$">|; 

$key = q/displaystylew=m-n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle w = m - n$">|; 

$key = q/displaystylewlez;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle w \le z$">|; 

$key = q/displaystylex,y,u,vge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img167.svg"
 ALT="$\displaystyle x,y,u,v \ge 0$">|; 

$key = q/displaystylexcdot(-z)=-xcdotzge-ycdotz=ycdot(-z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img165.svg"
 ALT="$\displaystyle x \cdot (-z) = - x \cdot z \ge - y \cdot z = y \cdot (-z)$">|; 

$key = q/displaystylexcdotulexcdotvleycdotv;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img168.svg"
 ALT="$\displaystyle x \cdot u \le x \cdot v \le y \cdot v$">|; 

$key = q/displaystylexcdotz=x+...+xley+...+y=ycdotz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img164.svg"
 ALT="$\displaystyle x \cdot z = x + ... + x \le y + ... + y = y \cdot z$">|; 

$key = q/displaystylexley;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img163.svg"
 ALT="$\displaystyle x \le y$">|; 

$key = q/displaystylexstrictinferieury;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle x \strictinferieur y$">|; 

$key = q/displaystyleygex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle y \ge x$">|; 

$key = q/displaystyleystrictsuperieurx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle y \strictsuperieur x$">|; 

$key = q/displaystylez=r-s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle z = r - s$">|; 

$key = q/displaystylez^n=zcdot...cdotz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.81ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img188.svg"
 ALT="$\displaystyle z^n = z \cdot ... \cdot z$">|; 

$key = q/f:setNmapstosetZ^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img39.svg"
 ALT="$f : \setN \mapsto \setZ^+$">|; 

$key = q/i,j,k,l,m,n,r,sinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img77.svg"
 ALT="$i,j,k,l,m,n,r,s \in \setN$">|; 

$key = q/i,j,k,linsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$i,j,k,l \in \setN$">|; 

$key = q/i,j,kinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img136.svg"
 ALT="$i,j,k \in \setN$">|; 

$key = q/i,j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img132.svg"
 ALT="$i,j$">|; 

$key = q/i,jinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$i,j \in \setN$">|; 

$key = q/i-0=(i,0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$i - 0 = (i,0)$">|; 

$key = q/i-j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img25.svg"
 ALT="$i - j$">|; 

$key = q/i-j=(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$i - j = (i,j)$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img36.svg"
 ALT="$i$">|; 

$key = q/igej;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img27.svg"
 ALT="$i \ge j$">|; 

$key = q/iinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img9.svg"
 ALT="$i \in \setN$">|; 

$key = q/ilej;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$i \le j$">|; 

$key = q/j-i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img31.svg"
 ALT="$j - i$">|; 

$key = q/j-i=(j,i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$j - i = (j,i)$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img139.svg"
 ALT="$k$">|; 

$key = q/m,ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img173.svg"
 ALT="$m,n \in \setN$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img190.svg"
 ALT="$n$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img16.svg"
 ALT="$n \in \setN$">|; 

$key = q/nne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img174.svg"
 ALT="$n \ne 0$">|; 

$key = q/setN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\setN$">|; 

$key = q/setN=setZ^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\setN = \setZ^+$">|; 

$key = q/setNsubseteqsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\setN \subseteq \setZ$">|; 

$key = q/setZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img176.svg"
 ALT="$\setZ$">|; 

$key = q/setZ=setZ^+cupsetZ^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\setZ = \setZ^+ \cup \setZ^-$">|; 

$key = q/u,v,w,zinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img76.svg"
 ALT="$u,v,w,z \in \setZ$">|; 

$key = q/u,v,wge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img143.svg"
 ALT="$u, v, w \ge 0$">|; 

$key = q/u,v,winsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img142.svg"
 ALT="$u,v,w \in \setZ$">|; 

$key = q/u,vge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img131.svg"
 ALT="$u,v \ge 0$">|; 

$key = q/u,vinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img50.svg"
 ALT="$u,v \in \setZ$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img107.svg"
 ALT="$u$">|; 

$key = q/uge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img103.svg"
 ALT="$u \ge 0$">|; 

$key = q/uinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img104.svg"
 ALT="$u \in \setZ$">|; 

$key = q/uinsetZ^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.06ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img99.svg"
 ALT="$u \in \setZ^+$">|; 

$key = q/vge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img135.svg"
 ALT="$v \ge 0$">|; 

$key = q/x,y,u,vinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img166.svg"
 ALT="$x,y,u,v \in \setZ$">|; 

$key = q/x,y,zinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img162.svg"
 ALT="$x,y,z \in \setZ$">|; 

$key = q/x,yinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img70.svg"
 ALT="$x,y \in \setZ$">|; 

$key = q/xley;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img72.svg"
 ALT="$x \le y$">|; 

$key = q/xney;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img75.svg"
 ALT="$x \ne y$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img113.svg"
 ALT="$z$">|; 

$key = q/z^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img189.svg"
 ALT="$z^n$">|; 

$key = q/zge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img95.svg"
 ALT="$z \ge 0$">|; 

$key = q/zinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img94.svg"
 ALT="$z \in \setZ$">|; 

$key = q/zle0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img97.svg"
 ALT="$z \le 0$">|; 

$key = q/{Eqts}1=(1,0)0=(0,0)-1=(0,1){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img11.svg"
 ALT="\begin{Eqts}
1 = (1,0) \\\\
0 = (0,0) \\\\
-1 = (0,1)
\end{Eqts}">|; 

$key = q/{Eqts}i=i-0=(i,0)-i=0-i=(0,i){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img10.svg"
 ALT="\begin{Eqts}
i = i - 0 = (i,0) \\\\
-i = 0 - i = (0,i)
\end{Eqts}">|; 

$key = q/{eqnarraystar}(i,j)-(k,l)=(i,j)+(-(k,l))=(i,j)+(l,k){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.20ex; " SRC="|."$dir".q|img60.svg"
 ALT="\begin{eqnarray*}
(i,j) - (k,l) = (i,j) + (-(k,l)) = (i,j) + (l,k)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}(k-l)cdot(i-j)&=&(kcdoti+lcdotj)-(kcdotj+lcdoti)&=&(icdotk+jcdotl)-(icdotl+jcdotk)&=&(i-j)cdot(k-l){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img141.svg"
 ALT="\begin{eqnarray*}
(k - l) \cdot (i - j) &amp;=&amp; (k \cdot i + l \cdot j) - (k \cdot j...
...\cdot l) - (i \cdot l + j \cdot k) \\\\
&amp;=&amp; (i - j) \cdot (k - l)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}i+(-j)=(i,0)+(0,j)=(i,j)=i-j{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.20ex; " SRC="|."$dir".q|img59.svg"
 ALT="\begin{eqnarray*}
i + (-j) = (i,0) + (0,j) = (i,j) = i - j
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}k&=&mdiventierenr&=&mmodulon{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.99ex; " SRC="|."$dir".q|img175.svg"
 ALT="\begin{eqnarray*}
k &amp;=&amp; m \diventiere n \\\\
r &amp;=&amp; m \modulo n
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}z^0&=&1z^n&=&zcdotz^{n-1}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.99ex; " SRC="|."$dir".q|img187.svg"
 ALT="\begin{eqnarray*}
z^0 &amp;=&amp; 1 \\\\
z^n &amp;=&amp; z \cdot z^{n - 1}
\end{eqnarray*}">|; 

1;

