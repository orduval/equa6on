# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/A(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$A(x)$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$A$">|; 

$key = q/A_1,A_2,...,A_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img18.svg"
 ALT="$A_1,A_2,...,A_n$">|; 

$key = q/A_1,A_2,A_3,...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$A_1,A_2,A_3,...$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\Omega$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$X$">|; 

$key = q/displaystyleAcapbigcup_{xinX}B(x)=bigcup_{xinX}big[AcapB(x)big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle A \cap \bigcup_{x \in X} B(x) = \bigcup_{x \in X} \big[A \cap B(x)\big]$">|; 

$key = q/displaystyleAcupbigcap_{xinX}B(x)=bigcap_{xinX}big[AcupB(x)big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle A \cup \bigcap_{x \in X} B(x) = \bigcap_{x \in X} \big[A \cup B(x)\big]$">|; 

$key = q/displaystyleOmegasetminusbigcap_{xinX}A(x)=bigcup_{xinX}big[OmegasetminusA(x)big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \Omega \setminus \bigcap_{x \in X} A(x) = \bigcup_{x \in X} \big[\Omega \setminus A(x)\big]$">|; 

$key = q/displaystyleOmegasetminusbigcup_{xinX}A(x)=bigcap_{xinX}big[OmegasetminusA(x)big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \Omega \setminus \bigcup_{x \in X} A(x) = \bigcap_{x \in X} \big[\Omega \setminus A(x)\big]$">|; 

$key = q/displaystylebigcap_iA_i=A_1capA_2capA_3cap...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \bigcap_i A_i = A_1 \cap A_2 \cap A_3 \cap ...$">|; 

$key = q/displaystylebigcap_{i=1}^nA_i=A_1capA_2cap...capA_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \bigcap_{i = 1}^n A_i = A_1 \cap A_2 \cap ... \cap A_n$">|; 

$key = q/displaystylebigcap_{i=1}^{+infty}A_i=A_1capA_2cap...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \bigcap_{i = 1}^{+\infty} A_i = A_1 \cap A_2 \cap ...$">|; 

$key = q/displaystylebigcap_{xinX}A(x)={ainOmega:ainA(x)text{pourtout}xinX};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \bigcap_{x \in X} A(x) = \{ a \in \Omega : a \in A(x) \text{ pour tout } x \in X \}$">|; 

$key = q/displaystylebigcapmathcal{C}={ainOmega:ainAtext{pourtout}Ainmathcal{C}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.90ex; vertical-align: -1.37ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \bigcap \mathcal{C} = \{ a \in \Omega : a \in A \text{ pour tout } A \in \mathcal{C} \}$">|; 

$key = q/displaystylebigcup_iA_i=A_1cupA_2cupA_3cup...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \bigcup_i A_i = A_1 \cup A_2 \cup A_3 \cup ...$">|; 

$key = q/displaystylebigcup_{i=1}^nA_i=A_1cupA_2cup...cupA_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \bigcup_{i = 1}^n A_i = A_1 \cup A_2 \cup ... \cup A_n$">|; 

$key = q/displaystylebigcup_{i=1}^{+infty}A_i=A_1cupA_2cup...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \bigcup_{i = 1}^{+\infty} A_i = A_1 \cup A_2 \cup ...$">|; 

$key = q/displaystylebigcup_{xinX}A(x)={ainOmega:text{ilexiste}xinXtext{telque}ainA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \bigcup_{x \in X} A(x) = \{ a \in \Omega : \text{ il existe } x \in X \text{ tel que } a \in A \}$">|; 

$key = q/displaystylebigcupmathcal{C}={ainOmega:text{ilexiste}Ainmathcal{C}text{telque}ainA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.90ex; vertical-align: -1.37ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \bigcup \mathcal{C} = \{ a \in \Omega : \text{ il existe } A \in \mathcal{C} \text{ tel que } a \in A \}$">|; 

$key = q/displaystylemathcal{C}={A(x)insousens(Omega):xinX};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \mathcal{C} = \{ A(x) \in \sousens(\Omega) : x \in X \}$">|; 

$key = q/displaystylesousens(A)={S:SsubseteqA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle \sousens(A) = \{ S : S \subseteq A \}$">|; 

$key = q/mathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\mathcal{C}$">|; 

$key = q/mathcal{C}subseteqsousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\mathcal{C} \subseteq \sousens(\Omega)$">|; 

$key = q/sousens;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.54ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\sousens$">|; 

$key = q/xinX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$x \in X$">|; 

$key = q/{A(x):xinX}subseteqsousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\{ A(x) : x \in X \} \subseteq \sousens(\Omega)$">|; 

$key = q/{Eqts}Acapbigcup_iB_i=bigcup_ibig[AcapB_ibig]Acupbigcap_iB_i=bigcap_ibig[AcupB_ibig]{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.21ex; vertical-align: -7.05ex; " SRC="|."$dir".q|img25.svg"
 ALT="\begin{Eqts}
A \cap \bigcup_i B_i = \bigcup_i \big[A \cap B_i\big] \ \\\\
A \cup \bigcap_i B_i = \bigcap_i \big[A \cup B_i\big]
\end{Eqts}">|; 

$key = q/{Eqts}Omegasetminusbigcup_iA_i=bigcap_ibig[OmegasetminusA_ibig]Omegasetminusbigcap_iA_i=bigcup_ibig[OmegasetminusA_ibig]{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.21ex; vertical-align: -7.05ex; " SRC="|."$dir".q|img29.svg"
 ALT="\begin{Eqts}
\Omega \setminus \bigcup_i A_i = \bigcap_i \big[\Omega \setminus A_...
...ga \setminus \bigcap_i A_i = \bigcup_i \big[\Omega \setminus A_i\big]
\end{Eqts}">|; 

1;

