# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/[k,k+1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$[k,k+1]$">|; 

$key = q/[k-1,k];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$[k-1,k]$">|; 

$key = q/displaystylef(k)=f(k)cdot1=int_{k-1}^kf(k)dxleint_{k-1}^kf(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.05ex; vertical-align: -2.36ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle f(k) = f(k) \cdot 1 = \int_{k - 1}^k f(k)  dx \le \int_{k - 1}^k f(x)  dx$">|; 

$key = q/displaystyleint_0^{+infty}f(x)dxlesum_{k=0}^{+infty}f(k)leint_{-1}^{+infty}f(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \int_0^{+\infty} f(x)  dx \le \sum_{k = 0}^{+\infty} f(k) \le \int_{-1}^{+\infty} f(x)  dx$">|; 

$key = q/displaystyleint_k^{k+1}f(x)dxlef(k)leint_{k-1}^kf(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.05ex; vertical-align: -2.36ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \int_k^{k + 1} f(x)  dx \le f(k) \le \int_{k - 1}^k f(x)  dx$">|; 

$key = q/displaystyleint_k^{k+1}f(x)dxleint_k^{k+1}f(k)dx=f(k)cdot1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \int_k^{k + 1} f(x)  dx \le \int_k^{k + 1} f(k)  dx= f(k) \cdot 1$">|; 

$key = q/displaystyleint_m^{n+1}f(x)dxlesum_{k=m}^nf(k)leint_{m-1}^nf(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \int_m^{n + 1} f(x)  dx \le \sum_{k = m}^n f(k) \le \int_{m - 1}^n f(x)  dx$">|; 

$key = q/displaystylesum_{k=m}^nint_k^{k+1}f(x)dx=int_m^{n+1}f(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \sum_{k = m}^n \int_k^{k + 1} f(x)  dx = \int_m^{n + 1} f(x)  dx$">|; 

$key = q/displaystylesum_{k=m}^nint_{k-1}^kf(x)dx=int_{m-1}^nf(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \sum_{k = m}^n \int_{k - 1}^k f(x)  dx = \int_{m - 1}^n f(x)  dx$">|; 

$key = q/f(k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$f(k)$">|; 

$key = q/f:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f : \setR \mapsto \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$f$">|; 

$key = q/kinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img2.svg"
 ALT="$k \in \setZ$">|; 

$key = q/kinsetZ[m,n];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$k \in \setZ[m,n]$">|; 

$key = q/m,ninsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$m, n \in \setZ$">|; 

1;

