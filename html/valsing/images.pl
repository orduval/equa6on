# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.05ex; vertical-align: -3.18ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle \mathcal{E}(z) = \sum_{i = 1}^r \abs{\scalaire{u_i}{b} - \sigma_i...
...re{u_i}{b}}^2 \ge \sum_{i = r + 1}^n \abs{\scalaire{u_i}{b}}^2 = \mathcal{E}(x)$">|; 

$key = q/(u_1,...,u_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img96.svg"
 ALT="$(u_1,...,u_m)$">|; 

$key = q/(u_1,...,u_r);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$(u_1,...,u_r)$">|; 

$key = q/(u_1,...u_r);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$(u_1,...u_r)$">|; 

$key = q/(v_1,...,v_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img93.svg"
 ALT="$(v_1,...,v_n)$">|; 

$key = q/(v_1,...,v_r);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$(v_1,...,v_r)$">|; 

$key = q/A:EmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$A : E \mapsto F$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img30.svg"
 ALT="$A$">|; 

$key = q/A=sum_{i=1}^rsigma_icdotu_icdotv_i^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img92.svg"
 ALT="$A = \sum_{i = 1}^r \sigma_i \cdot u_i \cdot v_i^\dual$">|; 

$key = q/A^dual:FmapstoE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$A^\dual : F \mapsto E$">|; 

$key = q/A^dualcdotA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img68.svg"
 ALT="$A^\dual \cdot A$">|; 

$key = q/A^dualcircA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$A^\dual \circ A$">|; 

$key = q/A^pinverse;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img52.svg"
 ALT="$A^\pinverse$">|; 

$key = q/A^pinversecdotA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img135.svg"
 ALT="$A^\pinverse \cdot A$">|; 

$key = q/AcdotA^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img69.svg"
 ALT="$A \cdot A^\dual$">|; 

$key = q/Acdotz=sum_{i=1}^rsigma_icdot0cdotu_i=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img171.svg"
 ALT="$A \cdot z = \sum_{i = 1}^r \sigma_i \cdot 0 \cdot u_i = 0$">|; 

$key = q/AcircA^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$A \circ A^\dual$">|; 

$key = q/Ainmatrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$A \in \matrice(\corps,m,n)$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$E$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$F$">|; 

$key = q/Gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\Gamma$">|; 

$key = q/Gamma=S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\Gamma = S$">|; 

$key = q/GammasubseteqS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\Gamma \subseteq S$">|; 

$key = q/P;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img134.svg"
 ALT="$P$">|; 

$key = q/Pcdoty=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img129.svg"
 ALT="$P \cdot y = y$">|; 

$key = q/Pcdotzincombilin{v_{r+1},...,v_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$P \cdot z \in \combilin{v_{r + 1},...,v_n}$">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img152.svg"
 ALT="$S$">|; 

$key = q/Sneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img147.svg"
 ALT="$S \ne \emptyset$">|; 

$key = q/SsubseteqGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img145.svg"
 ALT="$S \subseteq \Gamma$">|; 

$key = q/U,Lambda_1inmatrice(corps,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$U,\Lambda_1 \in \matrice(\corps,n,n)$">|; 

$key = q/U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img75.svg"
 ALT="$U$">|; 

$key = q/U^{-1}=U^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img78.svg"
 ALT="$U^{-1} = U^\dual$">|; 

$key = q/V,Lambda_2inmatrice(corps,m,m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$V,\Lambda_2 \in \matrice(\corps,m,m)$">|; 

$key = q/V;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img76.svg"
 ALT="$V$">|; 

$key = q/V^{-1}=V^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img79.svg"
 ALT="$V^{-1} = V^\dual$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img98.svg"
 ALT="$b$">|; 

$key = q/b=Acdotx=sum_{i=1}^rsigma_icdotscalaire{v_i}{x}cdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img162.svg"
 ALT="$b = A \cdot x = \sum_{i = 1}^r \sigma_i \cdot \scalaire{v_i}{x} \cdot u_i$">|; 

$key = q/bincombilin{u_1,...,u_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img164.svg"
 ALT="$b \in \combilin{u_1,...,u_r}$">|; 

$key = q/bincorps^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img87.svg"
 ALT="$b \in \corps^m$">|; 

$key = q/combilin{u_1,...,u_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\combilin{u_1,...,u_r}$">|; 

$key = q/combilin{u_1,...,u_r}subseteqimageA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img167.svg"
 ALT="$\combilin{u_1,...,u_r} \subseteq \image A$">|; 

$key = q/combilin{v_1,...,v_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\combilin{v_1,...,v_n}$">|; 

$key = q/combilin{v_1,...,v_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\combilin{v_1,...,v_r}$">|; 

$key = q/combilin{v_{r+1},...,v_n}subseteqnoyauA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img172.svg"
 ALT="$\combilin{v_{r + 1},...,v_n} \subseteq \noyau A$">|; 

$key = q/corps^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\corps^m$">|; 

$key = q/corps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\corps^n$">|; 

$key = q/displaystyle(A^dualcdotA)^{-1}=sum_{i=1}^runsur{sigma_i^2}cdotv_iotimesv_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle (A^\dual \cdot A)^{-1} = \sum_{i = 1}^r \unsur{\sigma_i^2} \cdot v_i \otimes v_i$">|; 

$key = q/displaystyle(AcdotA^dual)^{-1}=sum_{i=1}^runsur{sigma_i^2}cdotu_iotimesu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img160.svg"
 ALT="$\displaystyle (A \cdot A^\dual)^{-1} = \sum_{i = 1}^r \unsur{\sigma_i^2} \cdot u_i \otimes u_i$">|; 

$key = q/displaystyle(U,S,V)=singuliere(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle (U,S,V) = \singuliere(A)$">|; 

$key = q/displaystyle2(scalaire{u_i}{b}-sigma_icdotx_i)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\displaystyle 2 (\scalaire{u_i}{b} - \sigma_i \cdot x_i) = 0$">|; 

$key = q/displaystyleA(x)=mathcal{A}cdotx=contraction{mathcal{A}}{1}{x}=sum_{i=1}^rsigma_icdotu_icdotscalaire{v_i}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle A(x) = \mathcal{A} \cdot x = \contraction{ \mathcal{A} }{1}{x} = \sum_{i = 1}^r \sigma_i \cdot u_i \cdot \scalaire{v_i}{x}$">|; 

$key = q/displaystyleA=UcdotScdotV^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle A = U \cdot S \cdot V^\dual$">|; 

$key = q/displaystyleA=sum_{i=1}^rsigma_icdotu_icdotv_i^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img180.svg"
 ALT="$\displaystyle A = \sum_{i = 1}^r \sigma_i \cdot u_i \cdot v_i^\dual$">|; 

$key = q/displaystyleA=sum_{i=1}^rsigma_icdotu_iotimesv_i=sum_{i=1}^rsigma_icdotu_icdotv_i^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle A = \sum_{i = 1}^r \sigma_i \cdot u_i \otimes v_i = \sum_{i = 1}^r \sigma_i \cdot u_i \cdot v_i^\dual$">|; 

$key = q/displaystyleA^dual(u_i)=frac{lambda_i}{sigma_i}cdotv_i=sigma_icdotv_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.30ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle A^\dual(u_i) = \frac{\lambda_i}{\sigma_i} \cdot v_i = \sigma_i \cdot v_i$">|; 

$key = q/displaystyleA^dual(z_i)=A^dualcircA(v_i)=lambda_icdotv_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle A^\dual(z_i) = A^\dual \circ A(v_i) = \lambda_i \cdot v_i$">|; 

$key = q/displaystyleA^dualcircA(v_i)=lambda_icdotv_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle A^\dual \circ A(v_i) = \lambda_i \cdot v_i$">|; 

$key = q/displaystyleA^pinverse(y)=mathcal{A}^pinversecdoty=sum_{i=1}^runsur{sigma_i}cdotv_icdotscalaire{u_i}{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle A^\pinverse(y) = \mathcal{A}^\pinverse \cdot y = \sum_{i = 1}^r \unsur{\sigma_i} \cdot v_i \cdot \scalaire{u_i}{y}$">|; 

$key = q/displaystyleA^pinverse=VcdotS^pinversecdotU^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle A^\pinverse = V \cdot S^\pinverse \cdot U^\dual$">|; 

$key = q/displaystyleA^pinverse=sum_{i=1}^runsur{sigma_i}cdotv_icdotu_i^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle A^\pinverse = \sum_{i = 1}^r \unsur{\sigma_i} \cdot v_i \cdot u_i^\dual$">|; 

$key = q/displaystyleA^pinversecdotb=sum_{i=1}^runsur{sigma_i}cdotv_icdotscalaire{u_i}{b}=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle A^\pinverse \cdot b = \sum_{i = 1}^r \unsur{\sigma_i} \cdot v_i \cdot \scalaire{u_i}{b} = x$">|; 

$key = q/displaystyleA^pinversecdotbinargmin_{zinS}norme{z}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.08ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle A^\pinverse \cdot b \in \arg\min_{z \in S} \norme{z}^2$">|; 

$key = q/displaystyleAcdotx=sum_{i=1}^rsigma_icdotu_icdotscalaire{v_i}{x}=sum_{i=1}^rsigma_icdotu_icdotx_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\displaystyle A \cdot x = \sum_{i = 1}^r \sigma_i \cdot u_i \cdot \scalaire{v_i}{x} = \sum_{i = 1}^r \sigma_i \cdot u_i \cdot x_i$">|; 

$key = q/displaystyleAcircA^dual(z_i)=A(lambda_icdotv_i)=lambda_icdotA(v_i)=lambda_icdotz_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle A \circ A^\dual(z_i) = A(\lambda_i \cdot v_i) = \lambda_i \cdot A(v_i) = \lambda_i \cdot z_i$">|; 

$key = q/displaystyleGamma={A^pinversecdotb}+combilin{v_{r+1},...,v_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\displaystyle \Gamma = \{ A^\pinverse \cdot b \} + \combilin{v_{r + 1},...,v_n}$">|; 

$key = q/displaystyleI-A^pinversecdotA=sum_{i=1}^nv_iotimesv_i-sum_{i=1}^rv_iotimesv_i=sum_{i=r+1}^nv_iotimesv_i=P;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.05ex; vertical-align: -3.18ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle I - A^\pinverse \cdot A = \sum_{i = 1}^n v_i \otimes v_i - \sum_{i = 1}^r v_i \otimes v_i = \sum_{i = r + 1}^n v_i \otimes v_i = P$">|; 

$key = q/displaystyleP=sum_{i=r+1}^nv_iotimesv_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.05ex; vertical-align: -3.18ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\displaystyle P = \sum_{i = r + 1}^n v_i \otimes v_i$">|; 

$key = q/displaystyleS=diagonale_{m,n}(sigma_1,...,sigma_r);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.30ex; vertical-align: -2.48ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle S = \diagonale_{m,n}(\sigma_1,...,\sigma_r)$">|; 

$key = q/displaystyleS={xinsetC^n:Acdotx=b}={xinsetC^n:mathcal{E}(x)=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle S = \{ x \in \setC^n : A \cdot x = b \} = \{ x \in \setC^n : \mathcal{E}(x) = 0 \}$">|; 

$key = q/displaystyleS^pinverse=diagonale_{n,m}left(unsur{sigma_1},...,unsur{sigma_r}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.92ex; vertical-align: -2.48ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle S^\pinverse = \diagonale_{n,m}\left(\unsur{\sigma_1},...,\unsur{\sigma_r}\right)$">|; 

$key = q/displaystyleargmin_{zinsetC^n}mathcal{E}(z)=Gamma=left{left(A^pinversecdotb+sum_{i=r+1}^nx_icdotv_iright):x_{r+1},...,x_ninsetCright};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.31ex; vertical-align: -3.18ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\displaystyle \arg\min_{z \in \setC^n} \mathcal{E}(z) = \Gamma = \left\{ \left(...
...um_{i = r + 1}^n x_i \cdot v_i \right) :  x_{r + 1},...,x_n \in \setC \right\}$">|; 

$key = q/displaystyleb=sum_{i=1}^mscalaire{u_i}{b}cdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle b = \sum_{i = 1}^m \scalaire{u_i}{b} \cdot u_i$">|; 

$key = q/displaystylee(x)=b-Acdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle e(x) = b - A \cdot x$">|; 

$key = q/displaystylee(x)=sum_{i=1}^r(scalaire{u_i}{b}-sigma_icdotx_i)cdotu_i+sum_{i=r+1}^mscalaire{u_i}{b}cdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.05ex; vertical-align: -3.18ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle e(x) = \sum_{i = 1}^r (\scalaire{u_i}{b} - \sigma_i \cdot x_i) \cdot u_i + \sum_{i = r + 1}^m \scalaire{u_i}{b} \cdot u_i$">|; 

$key = q/displaystylef(A)=sum_{i=1}^rf(sigma_i)cdotu_icdotv_i^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img181.svg"
 ALT="$\displaystyle f(A) = \sum_{i = 1}^r f(\sigma_i) \cdot u_i \cdot v_i^\dual$">|; 

$key = q/displaystyleimageA=combilin{u_1,...,u_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img168.svg"
 ALT="$\displaystyle \image A = \combilin{u_1,...,u_r}$">|; 

$key = q/displaystylelambda_i=scalaire{A(v_i)}{A(v_i)}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \lambda_i = \scalaire{A(v_i)}{A(v_i)} \ge 0$">|; 

$key = q/displaystylemathcal{A}=sum_{i=1}^rsigma_icdotu_iotimesv_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \mathcal{A} = \sum_{i = 1}^r \sigma_i \cdot u_i \otimes v_i$">|; 

$key = q/displaystylemathcal{A}^dual=sum_{i=1}^rsigma_icdotv_iotimesu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \mathcal{A}^\dual = \sum_{i = 1}^r \sigma_i \cdot v_i \otimes u_i$">|; 

$key = q/displaystylemathcal{A}^pinverse=sum_{i=1}^runsur{sigma_i}cdotv_iotimesu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \mathcal{A}^\pinverse = \sum_{i = 1}^r \unsur{\sigma_i} \cdot v_i \otimes u_i$">|; 

$key = q/displaystylemathcal{A}^{-1}=sum_{i=1}^runsur{sigma_i}cdotv_iotimesu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \mathcal{A}^{-1} = \sum_{i = 1}^r \unsur{\sigma_i} \cdot v_i \otimes u_i$">|; 

$key = q/displaystylemathcal{E}(x)=e(x)^dualcdote(x)=norme{e(x)}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \mathcal{E}(x) = e(x)^\dual \cdot e(x) = \norme{e(x)}^2$">|; 

$key = q/displaystylemathcal{E}(x)=norme{e(x)}^2=sum_{i,j=1}^mconjaccent{e}_i(x)cdote_j(x)cdotu_i^dualcdotu_j=sum_{i=1}^mabs{e_i(x)}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle \mathcal{E}(x) = \norme{e(x)}^2 = \sum_{i,j = 1}^m \conjaccent{e}_i(x) \cdot e_j(x) \cdot u_i^\dual \cdot u_j = \sum_{i = 1}^m \abs{e_i(x)}^2$">|; 

$key = q/displaystylemathcal{E}(x)=sum_{i=1}^rabs{scalaire{u_i}{b}-sigma_icdotx_i}^2+sum_{i=r+1}^mabs{scalaire{u_i}{b}}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.05ex; vertical-align: -3.18ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle \mathcal{E}(x) = \sum_{i = 1}^r \abs{\scalaire{u_i}{b} - \sigma_i \cdot x_i}^2 + \sum_{i = r + 1}^m \abs{\scalaire{u_i}{b}}^2$">|; 

$key = q/displaystylemathcal{E}(x)=sum_{i=r+1}^mabs{scalaire{u_i}{b}}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.05ex; vertical-align: -3.18ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle \mathcal{E}(x) = \sum_{i = r + 1}^m \abs{\scalaire{u_i}{b}}^2$">|; 

$key = q/displaystylenorme{A}_F=sqrt{A^dual:A}=sqrt{sum_{i=1}^rsigma_i^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.62ex; vertical-align: -3.62ex; " SRC="|."$dir".q|img178.svg"
 ALT="$\displaystyle \norme{A}_F = \sqrt{A^\dual : A} = \sqrt{\sum_{i = 1}^r \sigma_i^2}$">|; 

$key = q/displaystylenorme{A}_lineaire=sup_{xne0}frac{norme{Acdotx}}{norme{x}}=max{sigma_1,...,sigma_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.93ex; vertical-align: -2.54ex; " SRC="|."$dir".q|img177.svg"
 ALT="$\displaystyle \norme{A}_\lineaire = \sup_{x \ne 0} \frac{\norme{A \cdot x}}{\norme{x}} = \max \{\sigma_1,...,\sigma_r\}$">|; 

$key = q/displaystylenorme{x}^2=x^dualcdotx=sum_{i=1}^runsur{sigma_i^2}cdotabs{scalaire{u_i}{b}}^2+sum_{i=r+1}^nabs{x_i}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.05ex; vertical-align: -3.18ex; " SRC="|."$dir".q|img150.svg"
 ALT="$\displaystyle \norme{x}^2 = x^\dual \cdot x = \sum_{i = 1}^r \unsur{\sigma_i^2} \cdot \abs{\scalaire{u_i}{b}}^2 + \sum_{i = r + 1}^n \abs{x_i}^2$">|; 

$key = q/displaystylenorme{x}^2gesum_{i=1}^runsur{sigma_i^2}cdotabs{scalaire{u_i}{b}}^2=norme{A^pinversecdotb}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle \norme{x}^2 \ge \sum_{i = 1}^r \unsur{\sigma_i^2} \cdot \abs{\scalaire{u_i}{b}}^2 = \norme{A^\pinverse \cdot b}^2$">|; 

$key = q/displaystylenoyauA=combilin{v_{r+1},...,v_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img176.svg"
 ALT="$\displaystyle \noyau A = \combilin{v_{r + 1},...,v_n}$">|; 

$key = q/displaystylescalaire{u_i}{u_j}=scalaire{v_i}{v_j}=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \scalaire{u_i}{u_j} = \scalaire{v_i}{v_j} = \indicatrice_{ij}$">|; 

$key = q/displaystylescalaire{v_i}{x}=unsur{sigma_i}cdotscalaire{u_i}{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \scalaire{v_i}{x} = \unsur{\sigma_i} \cdot \scalaire{u_i}{y}$">|; 

$key = q/displaystylesigma_i=sqrt{lambda_i}strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.64ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \sigma_i = \sqrt{\lambda_i} \strictsuperieur 0$">|; 

$key = q/displaystylesum_{i=1}^rabs{scalaire{u_i}{b}-sigma_icdotscalaire{v_i}{z}}^2strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle \sum_{i = 1}^r \abs{\scalaire{u_i}{b} - \sigma_i \cdot \scalaire{v_i}{z}}^2 \strictsuperieur 0$">|; 

$key = q/displaystyleu_i=unsur{sigma_i}cdotz_i=unsur{sigma_i}cdotA(v_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.19ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle u_i = \unsur{\sigma_i} \cdot z_i = \unsur{\sigma_i} \cdot A(v_i)$">|; 

$key = q/displaystylex=(A^dualcdotA)^{-1}cdotA^dualcdotb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\displaystyle x = (A^\dual \cdot A)^{-1} \cdot A^\dual \cdot b$">|; 

$key = q/displaystylex=A^dualcdot(AcdotA^dual)^{-1}cdotb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img159.svg"
 ALT="$\displaystyle x = A^\dual \cdot (A \cdot A^\dual)^{-1} \cdot b$">|; 

$key = q/displaystylex=A^pinversecdotb+(I-A^pinversecdotA)cdotz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle x = A^\pinverse \cdot b + (I - A^\pinverse \cdot A) \cdot z$">|; 

$key = q/displaystylex=A^pinversecdotb+Pcdotz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.40ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\displaystyle x = A^\pinverse \cdot b + P \cdot z$">|; 

$key = q/displaystylex=A^pinversecdotbinargmin_{zinsetC^n}mathcal{E}(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.00ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\displaystyle x = A^\pinverse \cdot b \in \arg\min_{z \in \setC^n} \mathcal{E}(z)$">|; 

$key = q/displaystylex=mathcal{A}^{-1}cdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle x = \mathcal{A}^{-1} \cdot y$">|; 

$key = q/displaystylex=sum_iscalaire{v_i}{x}cdotv_i=sum_iunsur{sigma_i}cdotscalaire{u_i}{y}cdotv_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle x = \sum_i \scalaire{v_i}{x} \cdot v_i = \sum_i \unsur{\sigma_i} \cdot \scalaire{u_i}{y} \cdot v_i$">|; 

$key = q/displaystylex=sum_{i=1}^nx_icdotv_i=sum_{i=1}^nscalaire{v_i}{x}cdotv_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle x = \sum_{i = 1}^n x_i \cdot v_i = \sum_{i = 1}^n \scalaire{v_i}{x} \cdot v_i$">|; 

$key = q/displaystylex=sum_{i=1}^rscalaire{v_i}{x}cdotv_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle x = \sum_{i = 1}^r \scalaire{v_i}{x} \cdot v_i$">|; 

$key = q/displaystylex=sum_{i=1}^runsur{sigma_i}cdotscalaire{u_i}{b}cdotv_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\displaystyle x = \sum_{i = 1}^r \unsur{\sigma_i} \cdot \scalaire{u_i}{b} \cdot v_i$">|; 

$key = q/displaystyley=sum_{i=1}^rscalaire{u_i}{y}cdotu_i=mathcal{A}cdotx=sum_{i=1}^rsigma_icdotu_icdotscalaire{v_i}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle y = \sum_{i = 1}^r \scalaire{u_i}{y} \cdot u_i = \mathcal{A} \cdot x = \sum_{i = 1}^r \sigma_i \cdot u_i \cdot \scalaire{v_i}{x}$">|; 

$key = q/e(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img90.svg"
 ALT="$e(x)$">|; 

$key = q/e(x)=sum_{i=1}^me_i(x)cdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img103.svg"
 ALT="$e(x) = \sum_{i = 1}^m e_i(x) \cdot u_i$">|; 

$key = q/f:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img179.svg"
 ALT="$f : \setR \mapsto \setR$">|; 

$key = q/iin{1,...,r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$i \in \{1,...,r\}$">|; 

$key = q/iin{1,2,...,p};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img72.svg"
 ALT="$i \in \{1,2,...,p\}$">|; 

$key = q/imageAsubseteqcombilin{u_1,...,u_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img163.svg"
 ALT="$\image A \subseteq \combilin{u_1,...,u_r}$">|; 

$key = q/lambda_1gelambda_2ge...gelambda_rstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\lambda_1 \ge \lambda_2 \ge ... \ge \lambda_r \strictsuperieur 0$">|; 

$key = q/lambda_1gelambda_2gelambda_3ge...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\lambda_1 \ge \lambda_2 \ge \lambda_3 \ge ...$">|; 

$key = q/lambda_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\lambda_i$">|; 

$key = q/lambda_iincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\lambda_i \in \corps$">|; 

$key = q/lambda_n=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\lambda_n = 0$">|; 

$key = q/mathcal{E}(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\mathcal{E}(x) = 0$">|; 

$key = q/mathcal{E}(z)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img144.svg"
 ALT="$\mathcal{E}(z) = 0$">|; 

$key = q/mathcal{E}(z)=mathcal{E}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\mathcal{E}(z) = \mathcal{E}(x)$">|; 

$key = q/mathcal{E}(z)strictsuperieurmathcal{E}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\mathcal{E}(z) \strictsuperieur \mathcal{E}(x)$">|; 

$key = q/mathcal{E};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img155.svg"
 ALT="$\mathcal{E}$">|; 

$key = q/noyauAsubseteqcombilin{v_{r+1},...,v_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img175.svg"
 ALT="$\noyau A \subseteq \combilin{v_{r + 1},...,v_n}$">|; 

$key = q/nstrictsuperieurr;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img21.svg"
 ALT="$n \strictsuperieur r$">|; 

$key = q/p=min{m,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$p = \min \{ m , n \}$">|; 

$key = q/rinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img19.svg"
 ALT="$r \in \setN$">|; 

$key = q/scalaire{u_{r+1}}{b}=...=scalaire{u_m}{b}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\scalaire{u_{r + 1}}{b} = ... = \scalaire{u_m}{b} = 0$">|; 

$key = q/scalaire{u_{r+1}}{b}=scalaire{u_m}{b}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img165.svg"
 ALT="$\scalaire{u_{r + 1}}{b} = \scalaire{u_m}{b} = 0$">|; 

$key = q/scalaire{v_1}{z}=...=scalaire{v_r}{z}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img170.svg"
 ALT="$\scalaire{v_1}{z} = ... = \scalaire{v_r}{z} = 0$">|; 

$key = q/setC^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\setC^n$">|; 

$key = q/setR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\setR^n$">|; 

$key = q/sigma_1gesigma_2ge...gesigma_rstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\sigma_1 \ge \sigma_2 \ge ... \ge \sigma_r \strictsuperieur 0$">|; 

$key = q/sigma_i=sqrt{lambda_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.52ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\sigma_i = \sqrt{\lambda_i}$">|; 

$key = q/sigma_icdotscalaire{v_i}{x}=scalaire{u_i}{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\sigma_i \cdot \scalaire{v_i}{x} = \scalaire{u_i}{y}$">|; 

$key = q/sigma_{r+1}=...=sigma_p=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.33ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\sigma_{r + 1} = ... = \sigma_p = 0$">|; 

$key = q/sum_{i=1}^rsigma_icdotscalaire{v_i}{z}cdotu_i=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img174.svg"
 ALT="$\sum_{i = 1}^r \sigma_i \cdot \scalaire{v_i}{z} \cdot u_i = 0$">|; 

$key = q/tenseuridentite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\tenseuridentite$">|; 

$key = q/v_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img45.svg"
 ALT="$v_i$">|; 

$key = q/v_iinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img9.svg"
 ALT="$v_i \in E$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img44.svg"
 ALT="$x$">|; 

$key = q/x=A^pinversecdotb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img113.svg"
 ALT="$x = A^\pinverse \cdot b$">|; 

$key = q/x_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img106.svg"
 ALT="$x_i$">|; 

$key = q/x_{r+1},...,x_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img109.svg"
 ALT="$x_{r + 1},...,x_n$">|; 

$key = q/xinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img39.svg"
 ALT="$x \in E$">|; 

$key = q/xinGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img131.svg"
 ALT="$x \in \Gamma$">|; 

$key = q/xinGamma=S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img148.svg"
 ALT="$x \in \Gamma = S$">|; 

$key = q/xinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img141.svg"
 ALT="$x \in S$">|; 

$key = q/xincombilin{v_1,...,v_r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$x \in \combilin{v_1,...,v_r}$">|; 

$key = q/xincorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img88.svg"
 ALT="$x \in \corps^n$">|; 

$key = q/y=A(x)=mathcal{A}cdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$y = A(x) = \mathcal{A} \cdot x$">|; 

$key = q/yinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img40.svg"
 ALT="$y \in F$">|; 

$key = q/yincombilin{v_{r+1},...,v_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img130.svg"
 ALT="$y \in \combilin{v_{r + 1},...,v_n}$">|; 

$key = q/z_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img14.svg"
 ALT="$z_i$">|; 

$key = q/z_i=A(v_i)inF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$z_i = A(v_i) \in F$">|; 

$key = q/zinGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img121.svg"
 ALT="$z \in \Gamma$">|; 

$key = q/zinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img143.svg"
 ALT="$z \in S$">|; 

$key = q/zincombilin{v_{r+1},...,v_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img169.svg"
 ALT="$z \in \combilin{v_{r + 1},...,v_n}$">|; 

$key = q/zincorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img133.svg"
 ALT="$z \in \corps^n$">|; 

$key = q/zinnoyauA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img173.svg"
 ALT="$z \in \noyau A$">|; 

$key = q/zinsetC^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img116.svg"
 ALT="$z \in \setC^n$">|; 

$key = q/znotinGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img122.svg"
 ALT="$z \notin \Gamma$">|; 

$key = q/{Eqts}(Lambda_1,U)=schur(AcdotA^dual)(Lambda_2,V)=schur(A^dualcdotA){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img65.svg"
 ALT="\begin{Eqts}
(\Lambda_1, U) = \schur(A \cdot A^\dual) \\\\
(\Lambda_2, V) = \schur(A^\dual \cdot A)
\end{Eqts}">|; 

$key = q/{Eqts}A(v_i)=sigma_icdotu_iA^dual(u_i)=sigma_icdotv_i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img26.svg"
 ALT="\begin{Eqts}
A(v_i) = \sigma_i \cdot u_i \\\\
A^\dual(u_i) = \sigma_i \cdot v_i
\end{Eqts}">|; 

$key = q/{Eqts}Lambda_1=diagonale_n(lambda_1,...lambda_p)Lambda_2=diagonale_m(lambda_1,...lambda_p){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.02ex; vertical-align: -3.95ex; " SRC="|."$dir".q|img70.svg"
 ALT="\begin{Eqts}
\Lambda_1 = \diagonale_n(\lambda_1,...\lambda_p) \\\\
\Lambda_2 = \diagonale_m(\lambda_1,...\lambda_p)
\end{Eqts}">|; 

$key = q/{Eqts}e_i(x)=cases{scalaire{u_i}{b}-sigma_icdotx_i&text{si}iin{1,...,r}scalaire{u_i}{b}&text{si}iin{r+1,...,m}cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img102.svg"
 ALT="\begin{Eqts}
e_i(x) =
\begin{cases}
\scalaire{u_i}{b} - \sigma_i \cdot x_i &amp; \te...
...\scalaire{u_i}{b} &amp; \text{ si } i \in \{r + 1, ...,m\} \\\\
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}mathcal{A}cdot(tenseuridentite-mathcal{A}^pinversecdotmathcal{A})=0mathcal{A}^pinversecdot(tenseuridentite-mathcal{A}cdotmathcal{A}^pinverse)=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.70ex; vertical-align: -2.78ex; " SRC="|."$dir".q|img62.svg"
 ALT="\begin{Eqts}
\mathcal{A} \cdot (\tenseuridentite - \mathcal{A}^\pinverse \cdot \...
...cdot (\tenseuridentite - \mathcal{A} \cdot \mathcal{A}^\pinverse) = 0
\end{Eqts}">|; 

$key = q/{Eqts}scalaire{z_i}{z_j}=scalaire{A(v_i)}{A(v_j)}=scalaire{v_i}{A^dualcircA(v_j)}=lambda_jcdotscalaire{v_i}{v_j}=lambda_icdotindicatrice_{ij}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img16.svg"
 ALT="\begin{Eqts}
\scalaire{z_i}{z_j} = \scalaire{A(v_i)}{A(v_j)} = \scalaire{v_i}{A^...
...ambda_j \cdot \scalaire{v_i}{v_j} = \lambda_i \cdot \indicatrice_{ij}
\end{Eqts}">|; 

$key = q/{Eqts}u_i=colonne_iUv_i=colonne_iV{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.15ex; vertical-align: -3.51ex; " SRC="|."$dir".q|img77.svg"
 ALT="\begin{Eqts}
u_i = \colonne_i U \\\\
v_i = \colonne_i V
\end{Eqts}">|; 

$key = q/{Eqts}x_i=cases{scalaire{u_i}{b}slashsigma_i&text{si}iin{1,...,r}0&text{si}iin{r+1,...,n}cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img110.svg"
 ALT="\begin{Eqts}
x_i =
\begin{cases}
\scalaire{u_i}{b} / \sigma_i &amp; \text{ si } i \in \{1,...,r\} \\\\
0 &amp; \text{ si } i \in \{r + 1, ...,n\} \\\\
\end{cases}\end{Eqts}">|; 

$key = q/{eqnarraystar}A(x)&=&sum_{i=1}^rscalaire{v_i}{x}cdotA(v_i)&=&sum_{i=1}^rscalaire{v_i}{x}cdotsigma_icdotu_i{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 19.19ex; " SRC="|."$dir".q|img29.svg"
 ALT="\begin{eqnarray*}
A(x) &amp;=&amp; \sum_{i = 1}^r \scalaire{v_i}{x} \cdot A(v_i) \\\\
&amp;=&amp; \sum_{i = 1}^r \scalaire{v_i}{x} \cdot \sigma_i \cdot u_i
\end{eqnarray*}">|; 

$key = q/{xinsetC^n:Acdotx=b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img166.svg"
 ALT="$\{ x \in \setC^n : A \cdot x = b\}$">|; 

1;

