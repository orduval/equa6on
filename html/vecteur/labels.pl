# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate labels original text with physical files.


$key = q/chap:vecteur/;
$external_labels{$key} = "$URL/" . q|vecteur.html|; 
$noresave{$key} = "$nosave";

$key = q/eq:mixte/;
$external_labels{$key} = "$URL/" . q|vecteur.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2023 (Released January 1, 2023)
# labels from external_latex_labels array.


$key = q/chap:vecteur/;
$external_latex_labels{$key} = q|1 Espaces vectoriels|; 
$noresave{$key} = "$nosave";

$key = q/eq:mixte/;
$external_latex_labels{$key} = q|1.1 Introduction|; 
$noresave{$key} = "$nosave";

1;

