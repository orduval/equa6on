# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(e_1,...,e_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$(e_1,...,e_n)$">|; 

$key = q/(x_1,x_2,...,x_n)incorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$(x_1,x_2,...,x_n) \in \corps^n$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img63.svg"
 ALT="$0$">|; 

$key = q/0inE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img16.svg"
 ALT="$0 \in E$">|; 

$key = q/0inF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img26.svg"
 ALT="$0 \in F$">|; 

$key = q/0incorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img17.svg"
 ALT="$0 \in \corps$">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img61.svg"
 ALT="$1$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$A$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$E$">|; 

$key = q/Ein{corps^n,corps^A,matrice(corps,m,n)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$E \in \{ \corps^n , \corps^A , \matrice(\corps,m,n) \}$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img28.svg"
 ALT="$F$">|; 

$key = q/FsubseteqE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img25.svg"
 ALT="$F \subseteq E$">|; 

$key = q/alpha,betaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\alpha, \beta \in \corps$">|; 

$key = q/alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\alpha$">|; 

$key = q/alpha_i-beta_i=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\alpha_i - \beta_i = 0$">|; 

$key = q/alpha_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\alpha_i$">|; 

$key = q/alpha_i=1ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\alpha_i = 1 \ne 0$">|; 

$key = q/alpha_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\alpha_j$">|; 

$key = q/beta_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\beta_i$">|; 

$key = q/canonique_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\canonique_i$">|; 

$key = q/cdot:corpstimesEmapstoE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\cdot : \corps \times E \mapsto E$">|; 

$key = q/cdot:corpstimescorps^Atocorps^A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\cdot : \corps \times \corps^A \to \corps^A$">|; 

$key = q/cdot:corpstimescorps^ntocorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\cdot : \corps \times \corps^n \to \corps^n$">|; 

$key = q/combilin{e_1,...,e_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\combilin{e_1,...,e_n}$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\corps$">|; 

$key = q/corps^A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\corps^A$">|; 

$key = q/corps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\corps^n$">|; 

$key = q/corps^nleftrightarrowcorps^A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\corps^n \leftrightarrow \corps^A$">|; 

$key = q/corpsin{setR,setC};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\corps \in \{ \setR, \setC \}$">|; 

$key = q/displaystyleEsubseteqcombilin{e_1,...,e_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle E \subseteq \combilin{e_1,...,e_n}$">|; 

$key = q/displaystyleJ(i)=setZ(0,n)setminus{i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle J(i) = \setZ(0,n) \setminus \{ i \}$">|; 

$key = q/displaystylealpha_i=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \alpha_i = 0$">|; 

$key = q/displaystylealpha_i=beta_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \alpha_i = \beta_i$">|; 

$key = q/displaystylecombilin{e_1,...,e_n}=left{sum_{i=1}^{n}alpha_icdote_i:alpha_iincorpsright};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \combilin{e_1,...,e_n} = \left\{ \sum_{i=1}^{n} \alpha_i \cdot e_i : \alpha_i \in \corps \right\}$">|; 

$key = q/displaystylee_i-sum_{jinJ(i)}alpha_jcdote_j=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.06ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle e_i - \sum_{ j \in J(i) } \alpha_j \cdot e_j = 0$">|; 

$key = q/displaystylee_i=sum_{jinJ(i)}alpha_jcdote_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.06ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle e_i = \sum_{ j \in J(i) } \alpha_j \cdot e_j$">|; 

$key = q/displaystylefrac{x}{alpha}=unsur{alpha}cdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \frac{x}{\alpha} = \unsur{\alpha} \cdot x$">|; 

$key = q/displaystylesum_{i=1}^n(alpha_i-beta_i)cdote_i=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \sum_{i=1}^n (\alpha_i - \beta_i) \cdot e_i = 0$">|; 

$key = q/displaystylesum_{i=1}^{n}alpha_icdote_i=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \sum_{i=1}^{n} \alpha_i \cdot e_i = 0$">|; 

$key = q/displaystylex=sum_{i=1}^nalpha_icdote_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle x = \sum_{i = 1}^n \alpha_i \cdot e_i$">|; 

$key = q/displaystylex=sum_{i=1}^nbeta_icdote_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle x = \sum_{i=1}^n \beta_i \cdot e_i$">|; 

$key = q/displaystylex=sum_{i=1}^nx_icdotcanonique_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle x = \sum_{i = 1}^n x_i \cdot \canonique_i$">|; 

$key = q/displaystylez=alphacdotx+betacdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle z = \alpha \cdot x + \beta \cdot y$">|; 

$key = q/e_1,...,e_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img34.svg"
 ALT="$e_1,...,e_n$">|; 

$key = q/e_1,...,e_ninE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img53.svg"
 ALT="$e_1,...,e_n \in E$">|; 

$key = q/e_1,e_2,...,e_ninE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$e_1,e_2,...,e_n \in E$">|; 

$key = q/e_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img31.svg"
 ALT="$e_i$">|; 

$key = q/i^{ème};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img62.svg"
 ALT="$i^{ème}$">|; 

$key = q/iin{1,2,...,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$i \in \{1,2,...,n\}$">|; 

$key = q/mathbb{K}^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\mathbb {K}^n$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$n$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img4.svg"
 ALT="$n \in \setN$">|; 

$key = q/setC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\setC$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\setR$">|; 

$key = q/u,vinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$u,v \in E$">|; 

$key = q/x,yinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$x,y \in F$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img47.svg"
 ALT="$x$">|; 

$key = q/x=(x_1,...,x_n)incorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$x = (x_1,...,x_n) \in \corps^n$">|; 

$key = q/x=(x_1,x_2,...,x_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$x = (x_1,x_2,...,x_n)$">|; 

$key = q/x_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img52.svg"
 ALT="$x_i$">|; 

$key = q/xinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img48.svg"
 ALT="$x \in E$">|; 

$key = q/xincombilin{e_1,...,e_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$x \in \combilin{e_1,...,e_n}$">|; 

$key = q/{Eqts}(alphacdotbeta)cdotu=alphacdot(betacdotu)(alpha+beta)cdotx=alphacdotx+betacdotxalphacdot(u+v)=alphacdotu+alphacdotv1cdotu=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.12ex; vertical-align: -6.00ex; " SRC="|."$dir".q|img13.svg"
 ALT="\begin{Eqts}
(\alpha \cdot \beta) \cdot u = \alpha \cdot (\beta \cdot u) \\\\
(\a...
...pha \cdot (u + v) = \alpha \cdot u + \alpha \cdot v \\\\
1 \cdot u = 1
\end{Eqts}">|; 

$key = q/{Eqts}0cdotu=(1-1)cdotu=u-u=0alphacdot0=alphacdot(u-u)=alpha-alpha=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img22.svg"
 ALT="\begin{Eqts}
0 \cdot u = (1 - 1) \cdot u = u - u = 0 \\\\
\alpha \cdot 0 = \alpha \cdot (u - u) = \alpha - \alpha = 0
\end{Eqts}">|; 

$key = q/{Eqts}canonique_1=[10hdots0]^Tcanonique_2=[01hdots0]^Tvdotscanonique_n=[0hdots01]^T{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.82ex; vertical-align: -6.86ex; " SRC="|."$dir".q|img68.svg"
 ALT="\begin{Eqts}
\canonique_1 = [1  0  \hdots  0]^T \\\\
\canonique_2 = [0  1  \hdots  0]^T \\\\
\vdots \\\\
\canonique_n = [0  \hdots  0  1]^T
\end{Eqts}">|; 

$key = q/{Eqts}canonique_i=(indicatrice_{ij})_j=Matrix{{c}vdots010vdotsMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.11ex; vertical-align: -7.51ex; " SRC="|."$dir".q|img67.svg"
 ALT="\begin{Eqts}
\canonique_i = ( \indicatrice_{ij} )_j =
\begin{Matrix}{c}
\vdots \ 0 \ 1 \ 0 \ \vdots
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}x-y=x+(-1)cdotyxcdotalpha=alphacdotxalphacdotbetacdotx=(alphacdotbeta)cdotxalphax=alphacdotxfrac{x}{alpha}=alpha^{-1}cdotx{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.04ex; vertical-align: -8.47ex; " SRC="|."$dir".q|img19.svg"
 ALT="\begin{Eqts}
x - y = x + (-1) \cdot y \\\\
x \cdot \alpha = \alpha \cdot x \\\\
\a...
...
\alpha x = \alpha \cdot x \\\\
\frac{x}{\alpha} = \alpha^{-1} \cdot x
\end{Eqts}">|; 

$key = q/{eqnarraystar}canonique_1&=&(1,0,...,0)canonique_2&=&(0,1,0,...,0)&vdots&canonique_n&=&(0,...,0,1){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 19.22ex; " SRC="|."$dir".q|img64.svg"
 ALT="\begin{eqnarray*}
\canonique_1 &amp;=&amp; (1,0,...,0) \\\\
\canonique_2 &amp;=&amp; (0,1,0,...,0) \\\\
&amp;\vdots&amp; \\\\
\canonique_n &amp;=&amp; (0,...,0,1)
\end{eqnarray*}">|; 

1;

