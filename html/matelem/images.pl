# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img5.svg"
 ALT="\begin{eqnarray*}
\matelementaire(\alpha,u,v) \cdot \matelementaire(\beta,u,v) &amp;...
...alpha \cdot \beta \cdot (v^\dual \cdot u)] \cdot u \cdot v^\dual
\end{eqnarray*}">|; 

$key = q/(n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$(n,n)$">|; 

$key = q/1+alphacdotv^dualcdotune0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$1 + \alpha \cdot v^\dual \cdot u \ne 0$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img48.svg"
 ALT="$A$">|; 

$key = q/Ainmatrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$A \in \matrice(\corps,m,n)$">|; 

$key = q/C_i=Acdotcanonique_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img44.svg"
 ALT="$C_i = A \cdot \canonique_i$">|; 

$key = q/Delta_{ij}=canonique_i-canonique_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\Delta_{ij} = \canonique_i - \canonique_j$">|; 

$key = q/Delta_{ij}^dualcdotDelta_{ij}=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.65ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\Delta_{ij}^\dual \cdot \Delta_{ij} = 2$">|; 

$key = q/E_{yx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img16.svg"
 ALT="$E_{yx}$">|; 

$key = q/E_{yx}cdotx=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img17.svg"
 ALT="$E_{yx} \cdot x = y$">|; 

$key = q/L_i=canonique_i^dualcdotA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.35ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img49.svg"
 ALT="$L_i = \canonique_i^\dual \cdot A$">|; 

$key = q/P;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img59.svg"
 ALT="$P$">|; 

$key = q/P=matpermutation_{n,i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.62ex; vertical-align: -0.95ex; " SRC="|."$dir".q|img43.svg"
 ALT="$P = \matpermutation_{n,i,j}$">|; 

$key = q/P^dual=P^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img64.svg"
 ALT="$P^\dual = P^T$">|; 

$key = q/P^dual=P^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img63.svg"
 ALT="$P^\dual = P^{-1}$">|; 

$key = q/P_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img61.svg"
 ALT="$P_i$">|; 

$key = q/alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\alpha$">|; 

$key = q/alpha=1slash(x^dualcdotx)ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\alpha = 1 / (x^\dual \cdot x) \ne 0$">|; 

$key = q/alpha=conjaccent{alpha}=1slash(x^dualcdotx)ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\alpha = \conjaccent{\alpha} = 1 / (x^\dual \cdot x) \ne 0$">|; 

$key = q/alphaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\alpha \in \corps$">|; 

$key = q/beta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\beta$">|; 

$key = q/canonique_1,...,canonique_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\canonique_1,...,\canonique_n$">|; 

$key = q/corps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\corps^n$">|; 

$key = q/displaystyle(I+alphacdotucdotv^dual)cdotx=x+alphacdotucdot(v^dualcdotx)=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle (I + \alpha \cdot u \cdot v^\dual) \cdot x = x + \alpha \cdot u \cdot (v^\dual \cdot x) = y$">|; 

$key = q/displaystyleAcdotP=A-(C_icdotcanonique_i^dual+C_jcdotcanonique_j^dual)+(C_jcdotcanonique_i^dual+C_icdotcanonique_j^dual);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle A \cdot P = A - (C_i \cdot \canonique_i^\dual + C_j \cdot \canonique_j^\dual) + (C_j \cdot \canonique_i^\dual + C_i \cdot \canonique_j^\dual)$">|; 

$key = q/displaystyleE_{yx}=I+unsur{x^dualcdotx}cdot(y-x)cdotx^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle E_{yx} = I + \unsur{x^\dual \cdot x} \cdot (y - x) \cdot x^\dual$">|; 

$key = q/displaystyleE_{yx}=I+unsur{x^dualcdotx}cdotxcdot(y-x)^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle E_{yx} = I + \unsur{x^\dual \cdot x} \cdot x \cdot (y - x)^\dual$">|; 

$key = q/displaystyleE_{yx}^{-1}=I-unsur{x^dualcdoty}cdot(y-x)cdotx^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.29ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle E_{yx}^{-1} = I - \unsur{x^\dual \cdot y} \cdot (y - x) \cdot x^\dual$">|; 

$key = q/displaystyleE_{yx}^{-1}=I-unsur{y^dualcdotx}cdotxcdot(y-x)^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.29ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle E_{yx}^{-1} = I - \unsur{y^\dual \cdot x} \cdot x \cdot (y - x)^\dual$">|; 

$key = q/displaystyleP=P_1cdot...cdotP_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle P = P_1 \cdot ... \cdot P_n$">|; 

$key = q/displaystyleP=matpermutation_{n,i,j}=I-Delta_{ij}cdotDelta_{ij}^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.25ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle P = \matpermutation_{n,i,j} = I - \Delta_{ij} \cdot \Delta_{ij}^\dual$">|; 

$key = q/displaystylePcdotA=A-(canonique_icdotL_i+canonique_jcdotL_j)+(canonique_jcdotL_i+canonique_icdotL_j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle P \cdot A = A - (\canonique_i \cdot L_i + \canonique_j \cdot L_j) + (\canonique_j \cdot L_i + \canonique_i \cdot L_j)$">|; 

$key = q/displaystylePcdotP=I-2Delta_{ij}cdotDelta_{ij}^dual+2Delta_{ij}cdotDelta_{ij}^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle P \cdot P = I - 2 \Delta_{ij} \cdot \Delta_{ij}^\dual + 2 \Delta_{ij} \cdot \Delta_{ij}^\dual$">|; 

$key = q/displaystylePcdotP=I-2Delta_{ij}cdotDelta_{ij}^dual+Delta_{ij}cdotDelta_{ij}^dualcdotDelta_{ij}cdotDelta_{ij}^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle P \cdot P = I - 2 \Delta_{ij} \cdot \Delta_{ij}^\dual + \Delta_{ij} \cdot \Delta_{ij}^\dual \cdot \Delta_{ij} \cdot \Delta_{ij}^\dual$">|; 

$key = q/displaystylePcdotP=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle P \cdot P = I$">|; 

$key = q/displaystylealpha+beta+alphacdotbetacdot(v^dualcdotu)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \alpha + \beta + \alpha \cdot \beta \cdot (v^\dual \cdot u) = 0$">|; 

$key = q/displaystylealphacdot(v^dualcdotx)cdotu=y-x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \alpha \cdot (v^\dual \cdot x) \cdot u = y - x$">|; 

$key = q/displaystylealphacdot(x^dualcdotu)cdotv^dual=y^dual-x^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \alpha \cdot (x^\dual \cdot u) \cdot v^\dual = y^\dual - x^\dual$">|; 

$key = q/displaystylebeta=-frac{alpha}{1+alphacdot(v^dualcdotu)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.92ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \beta = - \frac{\alpha}{1 + \alpha \cdot (v^\dual \cdot u)}$">|; 

$key = q/displaystylebeta=-unsur{x^dualcdotx+(y-x)^dualcdotx}=-unsur{y^dualcdotx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \beta = - \unsur{x^\dual \cdot x + (y - x)^\dual \cdot x} = - \unsur{y^\dual \cdot x}$">|; 

$key = q/displaystylebeta=-unsur{x^dualcdotx+x^dualcdot(y-x)}=-unsur{x^dualcdoty};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \beta = - \unsur{x^\dual \cdot x + x^\dual \cdot (y - x)} = - \unsur{x^\dual \cdot y}$">|; 

$key = q/displaystyleconjaccent{alpha}cdot(u^dualcdotx)cdotv=y-x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \conjaccent{\alpha} \cdot (u^\dual \cdot x) \cdot v = y - x$">|; 

$key = q/displaystylematelementaire(alpha,u,v)=I+alphacdotuotimesv=I+alphacdotucdotv^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \matelementaire(\alpha,u,v) = I + \alpha \cdot u \otimes v = I + \alpha \cdot u \cdot v^\dual$">|; 

$key = q/displaystylematelementaire(alpha,u,v)cdotmatelementaire(beta,u,v)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \matelementaire(\alpha,u,v) \cdot \matelementaire(\beta,u,v) = I$">|; 

$key = q/displaystylematelementaire(beta,u,v)=matelementaire(alpha,u,v)^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \matelementaire(\beta,u,v) = \matelementaire(\alpha,u,v)^{-1}$">|; 

$key = q/displaystylematelementaire(beta,u,v)cdotmatelementaire(alpha,u,v)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \matelementaire(\beta,u,v) \cdot \matelementaire(\alpha,u,v) = I$">|; 

$key = q/displaystylematpermutation_{n,i,j}=I-(canonique_i-canonique_j)cdot(canonique_i-canonique_j)^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.32ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle \matpermutation_{n,i,j} = I - (\canonique_i - \canonique_j) \cdot (\canonique_i - \canonique_j)^\dual$">|; 

$key = q/displaystylematpermutation_{n,i,j}=matpermutation_{n,i,j}^T=matpermutation_{n,i,j}^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \matpermutation_{n,i,j} = \matpermutation_{n,i,j}^T = \matpermutation_{n,i,j}^\dual$">|; 

$key = q/displaystylematpermutation_{n,i,j}=matpermutation_{n,i,j}^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.69ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \matpermutation_{n,i,j} = \matpermutation_{n,i,j}^{-1}$">|; 

$key = q/displaystylex^dualcdot(I+alphacdotucdotv^dual)=x^dual+alphacdot(x^dualcdotu)cdotv^dual=y^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle x^\dual \cdot (I + \alpha \cdot u \cdot v^\dual) = x^\dual + \alpha \cdot (x^\dual \cdot u) \cdot v^\dual = y^\dual$">|; 

$key = q/i,j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img40.svg"
 ALT="$i,j$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img46.svg"
 ALT="$i$">|; 

$key = q/j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img47.svg"
 ALT="$j$">|; 

$key = q/u,vincorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$u,v \in \corps^n$">|; 

$key = q/u=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$u = x$">|; 

$key = q/u=y-x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img20.svg"
 ALT="$u = y - x$">|; 

$key = q/v=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img21.svg"
 ALT="$v = x$">|; 

$key = q/v=y-x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img31.svg"
 ALT="$v = y - x$">|; 

$key = q/x^dualcdotE_{yx}=y^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img27.svg"
 ALT="$x^\dual \cdot E_{yx} = y^\dual$">|; 

$key = q/x^dualcdotyne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img25.svg"
 ALT="$x^\dual \cdot y \ne 0$">|; 

$key = q/xinmatrice(corps,n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$x \in \matrice(\corps,n,1)$">|; 

$key = q/y^dualcdotxne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img36.svg"
 ALT="$y^\dual \cdot x \ne 0$">|; 

$key = q/yinmatrice(corps,n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$y \in \matrice(\corps,n,1)$">|; 

$key = q/{Eqts}P^dualcdotP=P_ncdot...cdotP_1cdotP_1cdot...cdotP_n=IPcdotP^dual=P_1cdot...cdotP_ncdotP_ncdot...cdotP_1=I{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img62.svg"
 ALT="\begin{Eqts}
P^\dual \cdot P = P_n \cdot ... \cdot P_1 \cdot P_1 \cdot ... \cdot...
...t P^\dual = P_1 \cdot ... \cdot P_n \cdot P_n \cdot ... \cdot P_1 = I
\end{Eqts}">|; 

1;

