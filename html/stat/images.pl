# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.09ex; " SRC="|."$dir".q|img54.svg"
 ALT="\begin{eqnarray*}
\esperof{\sum_i (X_i - M_N)^2} &amp;=&amp; \esperof{ \sum_i \left( X_i...
...f{ \left( M_N^* \right)^2 } + \esperof{ \left( M_N^* \right)^2 }
\end{eqnarray*}">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img37.svg"
 ALT="$A$">|; 

$key = q/A_0=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img72.svg"
 ALT="$A_0 = \emptyset$">|; 

$key = q/G:setR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$G : \setR^n \mapsto \setR$">|; 

$key = q/G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img44.svg"
 ALT="$G$">|; 

$key = q/M_N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img48.svg"
 ALT="$M_N$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$N$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$X$">|; 

$key = q/X_1,...,X_N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$X_1,...,X_N$">|; 

$key = q/X_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img1.svg"
 ALT="$X_1$">|; 

$key = q/X_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img2.svg"
 ALT="$X_2$">|; 

$key = q/X_N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img3.svg"
 ALT="$X_N$">|; 

$key = q/X_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img35.svg"
 ALT="$X_i$">|; 

$key = q/X_i=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img31.svg"
 ALT="$X_i = 0$">|; 

$key = q/X_i=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img29.svg"
 ALT="$X_i = 1$">|; 

$key = q/[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$[a,b]$">|; 

$key = q/a>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img22.svg"
 ALT="$a &gt; 0$">|; 

$key = q/b=esperof{X};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$b = \esperof{X}$">|; 

$key = q/displaystyleA_i=A_{i-1}cup{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle A_i = A_{i-1} \cup \{ x \}$">|; 

$key = q/displaystyleG:(X_1,...,X_N)mapstoG(X_1,...,X_N);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle G : (X_1,...,X_N) \mapsto G(X_1,...,X_N)$">|; 

$key = q/displaystyleM=sup_{xin[a,b]}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.54ex; vertical-align: -2.72ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle M = \sup_{x \in [a,b]} f(x)$">|; 

$key = q/displaystyleM_N(X_1,...,X_N)=unsur{N}sum_{i=1}^NX_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.28ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle M_N(X_1,...,X_N) = \unsur{N} \sum_{i=1}^N X_i$">|; 

$key = q/displaystyleM_N=frac{n(A)}{N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.08ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle M_N = \frac{n(A)}{N}$">|; 

$key = q/displaystyleM_N=unsur{N}sum_{i=1}^NX_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.28ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle M_N = \unsur{N} \sum_{i=1}^N X_i$">|; 

$key = q/displaystyleM_N^*=unsur{N}sum_iX_i^*;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle M_N^* = \unsur{N} \sum_i X_i^*$">|; 

$key = q/displaystyleN>frac{sigma^2}{a^2epsilon};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle N &gt; \frac{\sigma^2}{a^2 \epsilon}$">|; 

$key = q/displaystyleS^2=unsur{N-1}sum_{i=1}^{N}(X_i-M_N)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.28ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle S^2 = \unsur{N-1} \sum_{i=1}^{N} (X_i - M_N)^2$">|; 

$key = q/displaystyleV(hat{theta})=prod_iprobaof{{omega:X_i(omega)=x_i}|theta=hat{theta}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle V(\hat{\theta}) = \prod_i \probaof{ \{\omega : X_i(\omega) = x_i \} \vert \theta = \hat{\theta} }$">|; 

$key = q/displaystyleX_i-M_N=X_i-mu+mu-M_N=X_i^*-M_N^*;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle X_i - M_N = X_i - \mu + \mu - M_N = X_i^* - M_N^*$">|; 

$key = q/displaystyleYle(X-b)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle Y \le (X-b)^2$">|; 

$key = q/displaystylecov{X_i}{X_j}=var{X_i}indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \cov{X_i}{X_j} = \var{X_i}  \indicatrice_{ij}$">|; 

$key = q/displaystylederiveepartielle{phi}{theta}(hat{theta})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle \deriveepartielle{\phi}{\theta}(\hat{\theta}) = 0$">|; 

$key = q/displaystyleesperof{M_N}=mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \esperof{M_N} = \mu$">|; 

$key = q/displaystyleesperof{M_N}=unsur{N}Nmu=mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \esperof{M_N} = \unsur{N}  N  \mu = \mu$">|; 

$key = q/displaystyleesperof{S^2}=sigma^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \esperof{S^2} = \sigma^2$">|; 

$key = q/displaystyleesperof{Y}=a^2probaof{abs{X-b}gea};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \esperof{Y} = a^2  \probaof{\abs{X-b} \ge a}$">|; 

$key = q/displaystyleesperof{hat{G}}=esperof{G};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \esperof{\hat{G}} = \esperof{G}$">|; 

$key = q/displaystyleesperof{prod_iX_i}=prod_iesperof{X_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \esperof{\prod_i X_i} = \prod_i \esperof{X_i}$">|; 

$key = q/displaystyleesperof{sum_i(X_i-M_N)^2}=(N-2+1)sigma^2=(N-1)sigma^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \esperof{\sum_i (X_i - M_N)^2} = (N - 2 + 1)  \sigma^2 = (N-1)  \sigma^2$">|; 

$key = q/displaystyleesperof{sum_i(X_i-M_N)^2}=esperof{sum_ileft(X_i^*-M_N^*right)^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \esperof{\sum_i (X_i - M_N)^2} = \esperof{\sum_i \left( X_i^* - M_N^* \right)^2}$">|; 

$key = q/displaystyleint_a^bf(x)dx=1-epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \int_a^b f(x)  dx = 1 - \epsilon$">|; 

$key = q/displaystylelim_{Nto+infty}probaof{M_N=mu}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.76ex; vertical-align: -1.94ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \lim_{N \to +\infty} \probaof{M_N = \mu} = 1$">|; 

$key = q/displaystylelim_{Nto+infty}probaof{frac{n(A)}{N}=probaof{A}}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \lim_{N \to +\infty} \probaof{\frac{n(A)}{N} = \probaof{A}} = 1$">|; 

$key = q/displaystylelnprod_{i=1}^Nprobaof{{omega:X_i(omega)=x_i}|theta=hat{theta}}=sum_{i=1}^Nlnprobaof{{omega:X_i(omega)=x_i}|theta=hat{theta}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.28ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle \ln\prod_{i=1}^N \probaof{ \{\omega : X_i(\omega) = x_i \} \vert ...
...1}^N \ln\probaof{ \{\omega : X_i(\omega) = x_i \} \vert \theta = \hat{\theta} }$">|; 

$key = q/displaystylemu=esperof{indicatrice_A}=probaof{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \mu = \esperof{\indicatrice_A} = \probaof{A}$">|; 

$key = q/displaystylephi(theta)=sum_ilnf_theta(x_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \phi(\theta) = \sum_i \ln f_\theta(x_i)$">|; 

$key = q/displaystyleprobaof{abs{M_N-mu}gea}lefrac{sigma^2}{a^2N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \probaof{\abs{M_N - \mu} \ge a} \le \frac{\sigma^2}{a^2 N}$">|; 

$key = q/displaystyleprobaof{abs{M_N-mu}gea}lefrac{sigma^2}{a^2N}strictinferieurepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \probaof{\abs{M_N - \mu} \ge a} \le \frac{\sigma^2}{a^2 N} \strictinferieur \epsilon$">|; 

$key = q/displaystyleprobaof{abs{X-b}gea}leunsur{a^2}esperof{(X-b)^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \probaof{\abs{X-b} \ge a} \le \unsur{a^2}  \esperof{(X-b)^2}$">|; 

$key = q/displaystyleprobaof{abs{X-esperof{X}}gea}leunsur{a^2}var{X};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \probaof{\abs{X-\esperof{X}} \ge a} \le \unsur{a^2}  \var{X}$">|; 

$key = q/displaystylerand(a,b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \rand(a,b)$">|; 

$key = q/displaystylevar{M_N}=frac{sigma^2}{N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \var{M_N} = \frac{\sigma^2}{N}$">|; 

$key = q/displaystylevar{sum_iX_i}=sum_ivar{X_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \var{\sum_i X_i} = \sum_i \var{X_i}$">|; 

$key = q/displaystyle{x_1,...,x_N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle \{ x_1, ..., x_N \}$">|; 

$key = q/epsilon>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\epsilon &gt; 0$">|; 

$key = q/epsilonge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\epsilon \ge 0$">|; 

$key = q/esperof{Y}leesperof{(X-b)^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\esperof{Y} \le \esperof{(X-b)^2}$">|; 

$key = q/f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$f(x)$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img66.svg"
 ALT="$f$">|; 

$key = q/f_theta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img62.svg"
 ALT="$f_\theta$">|; 

$key = q/hat{G}:setR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.39ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\hat{G} : \setR^n \mapsto \setR$">|; 

$key = q/hat{theta};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\hat{\theta}$">|; 

$key = q/indicatrice_A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\indicatrice_A$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\mu$">|; 

$key = q/n(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$n(A)$">|; 

$key = q/n(A)slashN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$n(A) / N$">|; 

$key = q/omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\omega$">|; 

$key = q/omegainA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\omega \in A$">|; 

$key = q/omeganotinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\omega \notin A$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img74.svg"
 ALT="$x$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img77.svg"
 ALT="$y$">|; 

$key = q/y<f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img76.svg"
 ALT="$y &lt; f(x)$">|; 

$key = q/{Eqts}X_i^*=X_i-muM_N^*=M_N-mu{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img50.svg"
 ALT="\begin{Eqts}
X_i^* = X_i - \mu \\\\
M_N^* = M_N - \mu
\end{Eqts}">|; 

$key = q/{Eqts}Y=cases{a^2&mbox{si}abs{X-b}gea0&mbox{si}abs{X-b}<acases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img11.svg"
 ALT="\begin{Eqts}
Y =
\begin{cases}
a^2 &amp; \mbox{ si } \abs{X-b} \ge a \\\\
0 &amp; \mbox{ si } \abs{X-b} &lt; a
\end{cases} \\\\
\end{Eqts}">|; 

$key = q/{Eqts}esperof{X_i}=mucov{X_i}{X_j}=sigmaindicatrice_{ij}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img9.svg"
 ALT="\begin{Eqts}
\esperof{X_i} = \mu \\\\
\cov{X_i}{X_j} = \sigma  \indicatrice_{ij}
\end{Eqts}">|; 

$key = q/{Eqts}var{M_N^*}=esperof{left(M_N^*right)^2}=frac{sigma^2}{N}esperof{left(X_i^*right)^2}=var{X_i}=sigma^2{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.91ex; vertical-align: -3.89ex; " SRC="|."$dir".q|img55.svg"
 ALT="\begin{Eqts}
\var{M_N^*} = \esperof{ \left( M_N^* \right)^2 } = \frac{\sigma^2}{N} \\\\
\esperof{ \left( X_i^* \right)^2 } = \var{X_i} = \sigma^2
\end{Eqts}">|; 

$key = q/{Eqts}x=rand(a,b)y=rand(0,M){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img73.svg"
 ALT="\begin{Eqts}
x = \rand(a,b) \\\\
y = \rand(0,M)
\end{Eqts}">|; 

$key = q/{eqnarraystar}var{M_N}&=&unsur{N^2}var{sum_iX_i}&=&unsur{N^2}sum_ivar{X_i}&=&unsur{N^2}Nsigma^2{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 24.13ex; " SRC="|."$dir".q|img20.svg"
 ALT="\begin{eqnarray*}
\var{M_N} &amp;=&amp; \unsur{N^2} \var{\sum_i X_i} \\\\
&amp;=&amp; \unsur{N^2} \sum_i \var{X_i} \\\\
&amp;=&amp; \unsur{N^2}  N  \sigma^2
\end{eqnarray*}">|; 

1;

