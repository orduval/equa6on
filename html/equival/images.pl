# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(x,y)inR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$(x,y) \in R$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$A$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img37.svg"
 ALT="$E$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$R$">|; 

$key = q/RsubseteqA^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.34ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img18.svg"
 ALT="$R \subseteq A^2$">|; 

$key = q/displaystyleA=bigcup_{xinA}mathcal{E}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle A = \bigcup_{x \in A} \mathcal{E}(x)$">|; 

$key = q/displaystyleAslashR={mathcal{E}(x)insousens(A):xinA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle A / R = \{ \mathcal{E}(x) \in \sousens(A) : x \in A \}$">|; 

$key = q/displaystyleE=bigcup_{xinA}mathcal{E}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle E = \bigcup_{x \in A} \mathcal{E}(x)$">|; 

$key = q/displaystyleR={(x,y)inA^2:xequivy};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle R = \{ (x,y) \in A^2 : x \equiv y \}$">|; 

$key = q/displaystylemathcal{E}(x)=mathcal{E}(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \mathcal{E}(x) = \mathcal{E}(y)$">|; 

$key = q/displaystylemathcal{E}(x)={yinA:yequivx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \mathcal{E}(x) = \{ y \in A : y \equiv x \}$">|; 

$key = q/displaystylemathcal{E}(x)capmathcal{E}(y)=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \mathcal{E}(x) \cap \mathcal{E}(y) = \emptyset$">|; 

$key = q/displaystylemathcal{P}=AslashR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \mathcal{P} = A / R$">|; 

$key = q/displaystylexequivx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.27ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle x \equiv x$">|; 

$key = q/displaystylexequivy,quadyequivzquadRightarrowquadxequivz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.72ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle x \equiv y, \quad y \equiv z \quad \Rightarrow \quad x \equiv z$">|; 

$key = q/displaystylexequivyquadRightarrowquadyequivx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.72ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle x \equiv y \quad \Rightarrow \quad y \equiv x$">|; 

$key = q/displaystylezinmathcal{E}(x)capmathcal{E}(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle z \in \mathcal{E}(x) \cap \mathcal{E}(y)$">|; 

$key = q/equiv;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.27ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\equiv$">|; 

$key = q/mathcal{E}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\mathcal{E}(x)$">|; 

$key = q/mathcal{E}(x)subseteqmathcal{E}(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\mathcal{E}(x) \subseteq \mathcal{E}(y)$">|; 

$key = q/mathcal{E}(y)subseteqmathcal{E}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\mathcal{E}(y) \subseteq \mathcal{E}(x)$">|; 

$key = q/wequivy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.72ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$w \equiv y$">|; 

$key = q/winmathcal{E}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$w \in \mathcal{E}(x)$">|; 

$key = q/winmathcal{E}(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$w \in \mathcal{E}(y)$">|; 

$key = q/x,y,zinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$x,y,z \in A$">|; 

$key = q/x,yinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img20.svg"
 ALT="$x,y \in A$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$x$">|; 

$key = q/xequivy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.72ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$x \equiv y$">|; 

$key = q/xequivyequivz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.72ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$x \equiv y \equiv z$">|; 

$key = q/xequivz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.27ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$x \equiv z$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img16.svg"
 ALT="$x \in A$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$y$">|; 

$key = q/yequivx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.72ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img31.svg"
 ALT="$y \equiv x$">|; 

$key = q/yequivz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.72ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$y \equiv z$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$z$">|; 

$key = q/zequivx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.27ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img26.svg"
 ALT="$z \equiv x$">|; 

$key = q/zequivy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.72ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img23.svg"
 ALT="$z \equiv y$">|; 

$key = q/zinmathcal{E}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$z \in \mathcal{E}(x)$">|; 

$key = q/zinmathcal{E}(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$z \in \mathcal{E}(y)$">|; 

1;

