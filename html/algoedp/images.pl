# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.67ex; vertical-align: -7.28ex; " SRC="|."$dir".q|img13.svg"
 ALT="\begin{Eqts}
u_{xx} \approx \Delta_x^2 U_{ij} = \unsur{h^2}  (U_{i+1,j} - 2 U_{...
...nsur{4 h^2}  (U_{i+1,j+1} - U_{i+1,j-1} - U_{i-1,j+1} + U_{i-1,j-1})
\end{Eqts}">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$0$">|; 

$key = q/F,Hsubsetfonction(Omega,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$F,H\subset\fonction(\Omega,\setR)$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$F$">|; 

$key = q/H;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$H$">|; 

$key = q/L(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$L(u)$">|; 

$key = q/L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img30.svg"
 ALT="$L$">|; 

$key = q/U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$U$">|; 

$key = q/U_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img25.svg"
 ALT="$U_i$">|; 

$key = q/U_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img15.svg"
 ALT="$U_{ij}$">|; 

$key = q/displaystyleAU=F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle A  U = F$">|; 

$key = q/displaystyleF(x,y,u,u_x,u_y,u_{xx},u_{xy},u_{yy})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle F(x,y,u,u_x,u_y,u_{xx},u_{xy},u_{yy}) = 0$">|; 

$key = q/displaystyleL(u)=f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle L(u) = f$">|; 

$key = q/displaystyleLinlineaire(F,H);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle L \in \lineaire(F,H)$">|; 

$key = q/displaystyleOD{x}{t}=aqquadOD{y}{t}=bqquadOD{u}{t}=c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \OD{x}{t} = a \qquad \OD{y}{t} = b \qquad \OD{u}{t} = c$">|; 

$key = q/displaystyleU=A^{-1}F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle U = A^{-1}  F$">|; 

$key = q/displaystyleU_{ij}=u(ih,jh);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle U_{ij} = u(i h, j h)$">|; 

$key = q/displaystylea(x,y,u)u_x(x,y)+b(x,y,u)u_y(x,y)=c(x,y,u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\displaystyle a(x,y,u)  u_x(x,y) + b(x,y,u)  u_y(x,y) = c(x,y,u)$">|; 

$key = q/displaystyleint_OmegaL(u)(x)psi_i(x)dx=int_Omegaf(x)psi_i(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \int_\Omega L(u)(x)  \psi_i(x)  dx = \int_\Omega f(x)  \psi_i(x)  dx$">|; 

$key = q/displaystyleint_Omegaleft[L(u)(x)-f(x)]psi_i(x)dx=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \int_\Omega \left[L(u)(x)-f(x)]  \psi_i(x)  dx = 0$">|; 

$key = q/displaystyleu(x)=sum_{i=1}^nU_ivarphi_i(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle u(x) = \sum_{i=1}^n U_i  \varphi_i(x)$">|; 

$key = q/displaystyleu_{i0}=u(x_{i0},y_{i0});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle u_{i0} = u(x_{i0},y_{i0})$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img19.svg"
 ALT="$f$">|; 

$key = q/h^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$h^2$">|; 

$key = q/i=1,2,...,n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$i = 1,2,...,n$">|; 

$key = q/psi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\psi_i$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$u$">|; 

$key = q/varphi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\varphi_i$">|; 

$key = q/{Eqts}A_{ij}=int_OmegaL(varphi_i)(x)psi_i(x)dxF_i=int_Omegaf(x)psi_i(x)dx{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.44ex; vertical-align: -5.16ex; " SRC="|."$dir".q|img28.svg"
 ALT="\begin{Eqts}
A_{ij} = \int_\Omega L(\varphi_i)(x)  \psi_i(x)  dx \\\\
F_i = \int_\Omega f(x)  \psi_i(x)  dx
\end{Eqts}">|; 

$key = q/{Eqts}U=(U_i)_iA=(A_{ij})_{i,j}F=(F_i)_i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img29.svg"
 ALT="\begin{Eqts}
U = (U_i)_i \\\\
A = (A_{ij})_{i,j} \\\\
F = (F_i)_i
\end{Eqts}">|; 

$key = q/{Eqts}u_x(x,y)approxfrac{u(x+h,y)-u(x-h,y)}{2h}u_y(x,y)approxfrac{u(x,y+h)-u(x,y-h)}{2h}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.74ex; vertical-align: -4.81ex; " SRC="|."$dir".q|img7.svg"
 ALT="\begin{Eqts}
u_x(x,y) \approx \frac{u(x+h,y) - u(x-h,y)}{2 h} \\\\
u_y(x,y) \approx \frac{u(x,y+h) - u(x,y-h)}{2 h}
\end{Eqts}">|; 

$key = q/{Eqts}u_xapproxDelta_xU_{ij}=unsur{2h}(U_{i+1,j}-U_{i-1,j})u_yapproxDelta_yU_{ij}=unsur{2h}(U_{i,j+1}-U_{i,j-1}){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.25ex; vertical-align: -4.57ex; " SRC="|."$dir".q|img12.svg"
 ALT="\begin{Eqts}
u_x \approx \Delta_x U_{ij} = \unsur{2 h}  (U_{i+1,j} - U_{i-1,j}) \\\\
u_y \approx \Delta_y U_{ij} = \unsur{2 h}  (U_{i,j+1} - U_{i,j-1})
\end{Eqts}">|; 

$key = q/{Eqts}x_{i,k+1}=x_{ik}+h_ka(x_{ik},y_{ik},u_{ik})y_{i,k+1}=y_{ik}+h_kb(x_{ik},y_{ik},u_{ik})u_{i,k+1}=u_{ik}+h_kc(x_{ik},y_{ik},u_{ik}){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img5.svg"
 ALT="\begin{Eqts}
x_{i,k+1} = x_{ik} + h_k  a(x_{ik},y_{ik},u_{ik}) \\\\
y_{i,k+1} = ...
...y_{ik},u_{ik}) \\\\
u_{i,k+1} = u_{ik} + h_k  c(x_{ik},y_{ik},u_{ik})
\end{Eqts}">|; 

1;

