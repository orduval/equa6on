# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/A,BsubseteqD;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$A,B \subseteq D$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img23.svg"
 ALT="$A$">|; 

$key = q/AsubseteqD;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img8.svg"
 ALT="$A \subseteq D$">|; 

$key = q/B_1(delta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$B_1(\delta)$">|; 

$key = q/B_2(epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$B_2(\epsilon)$">|; 

$key = q/DsubseteqE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img4.svg"
 ALT="$D \subseteq E$">|; 

$key = q/E,F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$E,F$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img53.svg"
 ALT="$E$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img70.svg"
 ALT="$F$">|; 

$key = q/GinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img71.svg"
 ALT="$G \in F$">|; 

$key = q/I(G)inE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img83.svg"
 ALT="$I(G) \in E$">|; 

$key = q/I(epsilon)inE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$I(\epsilon) \in E$">|; 

$key = q/L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$L$">|; 

$key = q/LinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img10.svg"
 ALT="$L \in F$">|; 

$key = q/PinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img76.svg"
 ALT="$P \in F$">|; 

$key = q/S(G)inE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img87.svg"
 ALT="$S(G) \in E$">|; 

$key = q/S(epsilon)inE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$S(\epsilon) \in E$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img21.svg"
 ALT="$a$">|; 

$key = q/ainE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$a \in E$">|; 

$key = q/alphastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\alpha \strictsuperieur 0$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$b$">|; 

$key = q/b=c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img44.svg"
 ALT="$b = c$">|; 

$key = q/betastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\beta \strictsuperieur 0$">|; 

$key = q/c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img30.svg"
 ALT="$c$">|; 

$key = q/delta(G)>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\delta(G) &gt; 0$">|; 

$key = q/delta(P)>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\delta(P) &gt; 0$">|; 

$key = q/delta(epsilon)incorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\delta(\epsilon) \in \corps$">|; 

$key = q/delta(epsilon)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\delta(\epsilon) \strictsuperieur 0$">|; 

$key = q/delta=min{alpha,beta};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\delta = \min\{\alpha,\beta\}$">|; 

$key = q/deltastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\delta \strictsuperieur 0$">|; 

$key = q/displaystyleI(a)={xinD:xlea};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle I(a) = \{ x \in D : x \le a \}$">|; 

$key = q/displaystyleX(a)=Dsetminus{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle X(a) = D \setminus \{ a \}$">|; 

$key = q/displaystyledistance(b,c)ledistance(b,f(x))+distance(f(x),c)lefrac{epsilon}{2}+frac{epsilon}{2}=epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle \distance(b,c) \le \distance(b,f(x)) + \distance(f(x),c) \le \frac{\epsilon}{2} + \frac{\epsilon}{2} = \epsilon$">|; 

$key = q/displaystyledistance(f(x),L)leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \distance(f(x),L) \le \epsilon$">|; 

$key = q/displaystyledistance(f(x),b)lefrac{epsilon}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \distance(f(x),b) \le \frac{\epsilon}{2}$">|; 

$key = q/displaystyledistance(f(x),c)lefrac{epsilon}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \distance(f(x),c) \le \frac{\epsilon}{2}$">|; 

$key = q/displaystyledistance(x,a)ledelta(G);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \distance(x,a) \le \delta(G)$">|; 

$key = q/displaystyledistance(x,a)ledelta(P);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle \distance(x,a) \le \delta(P)$">|; 

$key = q/displaystyledistance(x,a)ledelta(epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \distance(x,a) \le \delta(\epsilon)$">|; 

$key = q/displaystylef(x)geG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle f(x) \ge G$">|; 

$key = q/displaystylef(x)leP;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle f(x) \le P$">|; 

$key = q/displaystyleg(x)=inf{f(y):yinE,ygex};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle g(x) = \inf \{ f(y) : y \in E,  y \ge x \}$">|; 

$key = q/displaystylelim_{substack{xtoaxinA}}distance(f(x),L)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.62ex; vertical-align: -2.80ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \lim_{ \substack{ x \to a \ x \in A } } \distance(f(x),L) = 0$">|; 

$key = q/displaystylelim_{substack{xtoaxinA}}f(x)=L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.62ex; vertical-align: -2.80ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \lim_{ \substack{ x \to a \ x \in A } } f(x) = L$">|; 

$key = q/displaystylelim_{substack{xtoaxinA}}f(x)nelim_{substack{xtoaxinB}}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.62ex; vertical-align: -2.80ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \lim_{ \substack{ x \to a \ x \in A } } f(x) \ne \lim_{ \substack{ x \to a \ x \in B } } f(x)$">|; 

$key = q/displaystylelim_{substack{xtoaxlea}}f(x)=lim_{substack{xtoaxinI(a)}}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.96ex; vertical-align: -3.14ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \lim_{ \substack{ x \to a \ x \le a } } f(x) = \lim_{ \substack{ x \to a \ x \in I(a) } } f(x)$">|; 

$key = q/displaystylelim_{substack{xtoaxnea}}f(x)=lim_{substack{xtoaxinX(a)}}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.96ex; vertical-align: -3.14ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \lim_{ \substack{ x \to a \ x \ne a } } f(x) = \lim_{ \substack{ x \to a \ x \in X(a) } } f(x)$">|; 

$key = q/displaystylelim_{xto+infty}f(x)=+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle \lim_{x \to +\infty} f(x) = +\infty$">|; 

$key = q/displaystylelim_{xto+infty}f(x)=-infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle \lim_{x \to +\infty} f(x) = -\infty$">|; 

$key = q/displaystylelim_{xto+infty}f(x)=L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \lim_{ x \to +\infty } f(x) = L$">|; 

$key = q/displaystylelim_{xto-infty}f(x)=+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle \lim_{x \to -\infty} f(x) = +\infty$">|; 

$key = q/displaystylelim_{xto-infty}f(x)=-infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle \lim_{x \to -\infty} f(x) = -\infty$">|; 

$key = q/displaystylelim_{xto-infty}f(x)=L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \lim_{ x \to -\infty } f(x) = L$">|; 

$key = q/displaystylelim_{xtoa}f(x)=+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \lim_{ x \to a } f(x) = +\infty$">|; 

$key = q/displaystylelim_{xtoa}f(x)=-infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle \lim_{ x \to a } f(x) = -\infty$">|; 

$key = q/displaystylelim_{xtoa}f(x)=lim_{substack{xtoaxinA}}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.62ex; vertical-align: -2.80ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \lim_{ x \to a } f(x) = \lim_{ \substack{ x \to a \ x \in A } } f(x)$">|; 

$key = q/displaystylelim_{xtoinfty}f(x)=lim_{xto+infty}f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \lim_{ x \to \infty } f(x) = \lim_{ x \to +\infty } f(x)$">|; 

$key = q/displaystyleliminf_{xto+infty}f(x)=lim_{xto+infty}inf{f(y):yinE,ygex};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \liminf_{ x \to +\infty } f(x) = \lim_{ x \to +\infty } \inf \{ f(y) : y \in E,  y \ge x \}$">|; 

$key = q/displaystylelimsup_{xto+infty}f(x)=lim_{xto+infty}sup{f(y):yinE,ygex};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.16ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \limsup_{ x \to +\infty } f(x) = \lim_{ x \to +\infty } \sup \{ f(y) : y \in E,  y \ge x \}$">|; 

$key = q/displaystyle{distance(f(x),b),distance(f(x),c)}lefrac{epsilon}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \{  \distance(f(x),b) ,  \distance(f(x),c)  \} \le \frac{\epsilon}{2}$">|; 

$key = q/distance(b,c)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\distance(b,c) = 0$">|; 

$key = q/distance(x,a)lealpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\distance(x,a) \le \alpha$">|; 

$key = q/distance(x,a)lebeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\distance(x,a) \le \beta$">|; 

$key = q/distance(x,a)ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\distance(x,a) \le \delta$">|; 

$key = q/distance;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\distance$">|; 

$key = q/distance_E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\distance_E$">|; 

$key = q/distance_F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\distance_F$">|; 

$key = q/epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\epsilon$">|; 

$key = q/epsilon>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\epsilon &gt; 0$">|; 

$key = q/epsilonincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\epsilon \in \corps$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/f(B_1(delta))subseteqB_2(epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$f(B_1(\delta)) \subseteq B_2(\epsilon)$">|; 

$key = q/f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$f(x)$">|; 

$key = q/f:DmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$f : D \mapsto F$">|; 

$key = q/f:EmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img54.svg"
 ALT="$f : E \mapsto F$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img20.svg"
 ALT="$f$">|; 

$key = q/g:EmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img65.svg"
 ALT="$g : E \mapsto F$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$x$">|; 

$key = q/xgeI(G);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img84.svg"
 ALT="$x \ge I(G)$">|; 

$key = q/xgeI(epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$x \ge I(\epsilon)$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img11.svg"
 ALT="$x \in A$">|; 

$key = q/xinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img57.svg"
 ALT="$x \in E$">|; 

$key = q/xleS(G);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img88.svg"
 ALT="$x \le S(G)$">|; 

$key = q/xleS(epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$x \le S(\epsilon)$">|; 

$key = q/{Eqts}B_1(delta)=boule(a,delta)capAB_2(epsilon)=boule(f(a),epsilon){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img45.svg"
 ALT="\begin{Eqts}
B_1(\delta) = \boule(a,\delta) \cap A \\\\
B_2(\epsilon) = \boule(f(a),\epsilon)
\end{Eqts}">|; 

$key = q/{Eqts}distance(x,y)=distance_E(x,y)Leftrightarrowx,yinEdistance(x,y)=distance_F(x,y)Leftrightarrowx,yinF{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img6.svg"
 ALT="\begin{Eqts}
\distance(x,y) = \distance_E(x,y) \Leftrightarrow x,y \in E \\\\
\distance(x,y) = \distance_F(x,y) \Leftrightarrow x,y \in F
\end{Eqts}">|; 

$key = q/{Eqts}f(a)=+inftyquadLeftrightarrowquadlim_{xtoa}f(x)=+inftyf(a)=-inftyquadLeftrightarrowquadlim_{xtoa}f(x)=-infty{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.12ex; vertical-align: -3.49ex; " SRC="|."$dir".q|img81.svg"
 ALT="\begin{Eqts}
f(a) = +\infty \quad \Leftrightarrow \quad \lim_{ x \to a } f(x) = ...
...= -\infty \quad \Leftrightarrow \quad \lim_{ x \to a } f(x) = -\infty
\end{Eqts}">|; 

$key = q/{Eqts}limsup_{xto-infty}f(x)=lim_{xto-infty}sup{f(y):yinE,ylex}liminf_{xto-infty}f(x)=lim_{xto-infty}inf{f(y):yinE,ylex}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.27ex; vertical-align: -5.58ex; " SRC="|."$dir".q|img69.svg"
 ALT="\begin{Eqts}
\limsup_{ x \to -\infty } f(x) = \lim_{ x \to -\infty } \sup \{ f(y...
... } f(x) = \lim_{ x \to -\infty } \inf \{ f(y) : y \in E,  y \le x \}
\end{Eqts}">|; 

$key = q/{eqnarraystar}f(+infty)&=&lim_{xto+infty}f(x)f(-infty)&=&lim_{xto-infty}f(x){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.15ex; " SRC="|."$dir".q|img64.svg"
 ALT="\begin{eqnarray*}
f(+\infty) &amp;=&amp; \lim_{ x \to +\infty } f(x) \ \\\\
f(-\infty) &amp;=&amp; \lim_{ x \to -\infty } f(x)
\end{eqnarray*}">|; 

1;

