# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/Ainmatrice(corps,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$A \in \matrice(\corps, n, n)$">|; 

$key = q/displaystyle(I-A)cdotG_N=I-A^{N+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle (I - A) \cdot G_N = I - A^{N + 1}$">|; 

$key = q/displaystyle(I-A)cdotsum_{k=0}^NA^k=I-A^{N+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.33ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle (I - A) \cdot \sum_{k = 0}^N A^k = I - A^{N + 1}$">|; 

$key = q/displaystyleG_N=I+(I+A+...+A^N)cdotA-A^{N+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle G_N = I + (I + A + ... + A^N) \cdot A - A^{N + 1}$">|; 

$key = q/displaystyleG_N=I+Acdot(I+A+...+A^N)-A^{N+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle G_N = I + A \cdot (I + A + ... + A^N) - A^{N + 1}$">|; 

$key = q/displaystyleG_N=I+AcdotG_N-A^{N+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.55ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle G_N = I + A \cdot G_N - A^{N + 1}$">|; 

$key = q/displaystyleG_N=I+G_NcdotA-A^{N+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.55ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle G_N = I + G_N \cdot A - A^{N + 1}$">|; 

$key = q/displaystyleG_N=sum_{k=0}^NA^k=I+A+A^2+...+A^N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.33ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle G_N = \sum_{k = 0}^N A^k = I + A + A^2 + ... + A^N$">|; 

$key = q/displaystyleG_Ncdot(I-A)=I-A^{N+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle G_N \cdot (I - A) = I - A^{N + 1}$">|; 

$key = q/displaystylesum_{k=0}^NA^k=(I-A)^{-1}cdot(I-A^{N+1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.33ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \sum_{k = 0}^N A^k = (I - A)^{-1} \cdot (I - A^{N + 1})$">|; 

$key = q/displaystylesum_{k=0}^NA^k=(I-A^{N+1})cdot(I-A)^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.33ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \sum_{k = 0}^N A^k = (I - A^{N + 1}) \cdot (I - A)^{-1}$">|; 

$key = q/displaystylesum_{k=0}^NA^kcdot(I-A)=I-A^{N+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.33ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \sum_{k = 0}^N A^k \cdot (I - A) = I - A^{N + 1}$">|; 

1;

