# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.61ex; vertical-align: -5.25ex; " SRC="|."$dir".q|img83.svg"
 ALT="\begin{Eqts}
\{ x \in A : I(x) \strictsuperieur a \} = \bigcap_n \{ x \in A : f_...
...= \bigcup_n \{ x \in A : f_n(x) \strictinferieur a \} \in \mathcal{T}
\end{Eqts}">|; 

$key = q/(AsetminusB)capB=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$(A \setminus B) \cap B = \emptyset$">|; 

$key = q/(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$(i,j)$">|; 

$key = q/-f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img78.svg"
 ALT="$-f$">|; 

$key = q/A,Binmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img13.svg"
 ALT="$A,B \in \mathcal{T}$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$A$">|; 

$key = q/AcupB=(AsetminusB)cupB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$A \cup B = (A \setminus B) \cup B$">|; 

$key = q/Ainmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img6.svg"
 ALT="$A \in \mathcal{T}$">|; 

$key = q/AsetminusBsubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$A \setminus B \subseteq A$">|; 

$key = q/AsubseteqB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img14.svg"
 ALT="$A \subseteq B$">|; 

$key = q/AsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img57.svg"
 ALT="$A \subseteq \Omega$">|; 

$key = q/C=BsetminusA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$C = B \setminus A$">|; 

$key = q/CcapA=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.06ex; vertical-align: -0.23ex; " SRC="|."$dir".q|img18.svg"
 ALT="$C \cap A = \emptyset$">|; 

$key = q/CcupA=B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$C \cup A = B$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$N$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\Omega$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img55.svg"
 ALT="$a$">|; 

$key = q/a_1,a_2,...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$a_1,a_2,...$">|; 

$key = q/ainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img73.svg"
 ALT="$a \in \setR$">|; 

$key = q/displaystyle(muotimesnu)(AtimesB)=mu(A)cdotnu(B);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle (\mu \otimes \nu)(A \times B) = \mu(A) \cdot \nu(B)$">|; 

$key = q/displaystyleA_icapA_j=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle A_i \cap A_j = \emptyset$">|; 

$key = q/displaystyleIinbig{[a,b],intervalleouvert{a}{b},intervallesemiouvertgauche{a}{b},intervallesemiouvertdroite{a}{b}big};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle I \in \big\{  [a,b], \intervalleouvert{a}{b}, \intervallesemiouvertgauche{a}{b}, \intervallesemiouvertdroite{a}{b} \big\}$">|; 

$key = q/displaystylemathcal{P}={AtimesB:Ainmathcal{T}_1,Binmathcal{T}_2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle \mathcal{P} = \{ A \times B : A \in \mathcal{T}_1,  B \in \mathcal{T}_2 \}$">|; 

$key = q/displaystylemu(A)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \mu(A) \ge 0$">|; 

$key = q/displaystylemu(A)lemu(B);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \mu(A) \le \mu(B)$">|; 

$key = q/displaystylemu(AcupB)=mu(AsetminusB)+mu(B);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \mu(A \cup B) = \mu(A \setminus B) + \mu(B)$">|; 

$key = q/displaystylemu(AcupB)lemu(A)+mu(B);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \mu(A \cup B) \le \mu(A) + \mu(B)$">|; 

$key = q/displaystylemu(B)=mu(C)+mu(A)gemu(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \mu(B) = \mu(C) + \mu(A) \ge \mu(A)$">|; 

$key = q/displaystylemu(emptyset)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \mu(\emptyset) = 0$">|; 

$key = q/displaystylemu^S(A)=supaccolades{sum_{ninN}mu_L(I_n):{I_n:ninNsubseteqsetN}inmathfrak{J},bigcup_{ninN}I_nsubseteqA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.29ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \mu^S(A) = \sup \accolades{ \sum_{n \in N} \mu_L(I_n) : \{ I_n : ...
...n N \subseteq \setN \} \in \mathfrak{J} ,  \bigcup_{n \in N} I_n \subseteq A }$">|; 

$key = q/displaystylemu_I(A)=infaccolades{sum_{ninN}mu_L(I_n):{I_n:ninNsubseteqsetN}inmathfrak{J},Asubseteqbigcup_{ninN}I_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.29ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \mu_I(A) = \inf \accolades{ \sum_{n \in N} \mu_L(I_n) : \{ I_n : n \in N \subseteq \setN \} \in \mathfrak{J} ,  A \subseteq \bigcup_{n \in N} I_n}$">|; 

$key = q/displaystylemu_I(A)=mu^S(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \mu_I(A) = \mu^S(A)$">|; 

$key = q/displaystylemu_L(A)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \mu_L(A) = 0$">|; 

$key = q/displaystylemu_L(A)=mu_I(A)=mu^S(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \mu_L(A) = \mu_I(A) = \mu^S(A)$">|; 

$key = q/displaystylemu_L(I)=b-a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \mu_L(I) = b - a$">|; 

$key = q/displaystylemu_L(N)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle \mu_L(N) = 0$">|; 

$key = q/displaystylemu_L([a,b]setminus{a_1,a_2,...})=b-a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \mu_L([a,b] \setminus \{a_1,a_2,...\}) = b - a$">|; 

$key = q/displaystylemu_L(intervalleouvert{a_1}{b_1}timesintervalleouvert{a_2}{b_2}...timesintervalleouvert{a_n}{b_n})=prod_{i=1}^n(b_i-a_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \mu_L( \intervalleouvert{a_1}{b_1} \times \intervalleouvert{a_2}{b_2} ... \times \intervalleouvert{a_n}{b_n}) = \prod_{i = 1}^n (b_i - a_i)$">|; 

$key = q/displaystylemu_L({a_1,a_2,...})=sum_imu_L({a_i})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \mu_L(\{a_1,a_2,...\}) = \sum_i \mu_L(\{a_i\}) = 0$">|; 

$key = q/displaystylemu_L({a})=a-a=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \mu_L(\{a\}) = a - a = 0$">|; 

$key = q/displaystylemu_g(A)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \mu_g(A) = 0$">|; 

$key = q/displaystylemu_g(A)=infaccolades{sum_{ninN}mu_L(I_n):{I_n:ninNsubseteqsetN}inmathfrak{J},Asubseteqbigcup_{ninN}I_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.29ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \mu_g(A) = \inf \accolades{ \sum_{n \in N} \mu_L(I_n) : \{ I_n : n \in N \subseteq \setN \} \in \mathfrak{J} ,  A \subseteq \bigcup_{n \in N} I_n}$">|; 

$key = q/displaystylemu_g(I)=g(b)-g(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \mu_g(I) = g(b) - g(a)$">|; 

$key = q/displaystylemu_g(N)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \mu_g(N) = 0$">|; 

$key = q/displaystylemuleft(bigcup_iA_iright)=sum_imu(A_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \mu\left( \bigcup_i A_i \right) = \sum_i \mu(A_i)$">|; 

$key = q/displaystylemuleft(bigcup_{i=0}^nA_iright)lesum_{i=0}^nmu(A_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \mu\left( \bigcup_{i = 0}^n A_i \right) \le \sum_{i = 0}^n \mu(A_i)$">|; 

$key = q/displaystylemuleft(bigcup_{i=0}^{+infty}A_iright)lesum_{i=0}^{+infty}mu(A_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \mu\left( \bigcup_{i = 0}^{+\infty} A_i \right) \le \sum_{i = 0}^{+\infty} \mu(A_i)$">|; 

$key = q/displaystyle{xinA:f(x)=a}={xinA:f(x)gea}cap{xinA:f(x)lea}inmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle \{ x \in A : f(x) = a \} = \{ x \in A : f(x) \ge a \} \cap \{ x \in A : f(x) \le a \} \in \mathcal{T}$">|; 

$key = q/f:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img69.svg"
 ALT="$f : A \mapsto \setR$">|; 

$key = q/f^{-1}(]-infty,a[)inmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img72.svg"
 ALT="$f^{-1}(]-\infty,a[) \in \mathcal{T}$">|; 

$key = q/f^{-1}(]a,+infty[)inmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$f^{-1}(]a,+\infty[) \in \mathcal{T}$">|; 

$key = q/f^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img70.svg"
 ALT="$f^{-1}$">|; 

$key = q/g:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img48.svg"
 ALT="$g : \setR \mapsto \setR$">|; 

$key = q/inej;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$i \ne j$">|; 

$key = q/inf_nf_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\inf_n f_n$">|; 

$key = q/mathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\mathcal{T}$">|; 

$key = q/mathcal{T}_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\mathcal{T}_1$">|; 

$key = q/mathcal{T}_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\mathcal{T}_2$">|; 

$key = q/mathcal{T}subseteqsousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\mathcal{T} \subseteq \sousens(\Omega)$">|; 

$key = q/mathfrak{J};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.39ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\mathfrak{J}$">|; 

$key = q/mu(AsetminusB)lemu(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\mu(A \setminus B) \le \mu(A)$">|; 

$key = q/mu:mathcal{T}_1mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\mu : \mathcal{T}_1 \mapsto \setR$">|; 

$key = q/mu:mathcal{T}mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\mu : \mathcal{T} \mapsto \setR$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\mu$">|; 

$key = q/mu_D^a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\mu_D^a$">|; 

$key = q/mu_L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\mu_L$">|; 

$key = q/mu_g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\mu_g$">|; 

$key = q/muotimesnu:mathcal{P}mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\mu \otimes \nu : \mathcal{P} \mapsto \setR$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img66.svg"
 ALT="$n$">|; 

$key = q/nu:mathcal{T}_2mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\nu : \mathcal{T}_2 \mapsto \setR$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\setR$">|; 

$key = q/setR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\setR^n$">|; 

$key = q/sousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\sousens(\Omega)$">|; 

$key = q/sup_nf_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.35ex; vertical-align: -0.65ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\sup_n f_n$">|; 

$key = q/{A_1,A_2,...}subseteqmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\{ A_1,A_2,... \} \subseteq \mathcal{T}$">|; 

$key = q/{Eqts}S=sup{f_n:ninsetN}I=inf{f_n:ninsetN}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img80.svg"
 ALT="\begin{Eqts}
S = \sup \{ f_n : n \in \setN \} \\\\
I = \inf \{ f_n : n \in \setN \}
\end{Eqts}">|; 

$key = q/{Eqts}mu_D^a(A)=indicatrice_A(a)=cases{1&mbox{si}ainA0&mbox{si}anotinAcases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img58.svg"
 ALT="\begin{Eqts}
\mu_D^a(A) = \indicatrice_A(a) =
\begin{cases}
1 &amp; \mbox{ si } a \in A \\\\
0 &amp; \mbox{ si } a \notin A
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}{xinA:-f(x)strictsuperieura}={xinA:f(x)strictinferieur-a}inmathcal{T}{xinA:-f(x)strictinferieura}={xinA:f(x)strictsuperieur-a}inmathcal{T}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img77.svg"
 ALT="\begin{Eqts}
\{ x \in A : -f(x) \strictsuperieur a \} = \{ x \in A : f(x) \stric...
...rieur a \} = \{ x \in A : f(x) \strictsuperieur -a \} \in \mathcal{T}
\end{Eqts}">|; 

$key = q/{Eqts}{xinA:f(x)gea}=Asetminus{xinA:f(x)strictinferieura}inmathcal{T}{xinA:f(x)lea}=Asetminus{xinA:f(x)strictsuperieura}inmathcal{T}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img75.svg"
 ALT="\begin{Eqts}
\{ x \in A : f(x) \ge a \} = A \setminus \{ x \in A : f(x) \stricti...
...= A \setminus \{ x \in A : f(x) \strictsuperieur a \} \in \mathcal{T}
\end{Eqts}">|; 

$key = q/{Eqts}{xinA:f(x)strictsuperieura}inmathcal{T}{xinA:f(x)strictinferieura}inmathcal{T}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img74.svg"
 ALT="\begin{Eqts}
\{ x \in A : f(x) \strictsuperieur a \} \in \mathcal{T} \\\\
\{ x \in A : f(x) \strictinferieur a \} \in \mathcal{T}
\end{Eqts}">|; 

$key = q/{a}=[a,a];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\{a\} = [a,a]$">|; 

$key = q/{eqnarraystar}b-a=mu_L([a,b])&=&mu_L([a,b]setminus{a_1,a_2,...})+mu_L({a_1,a_2,...})&=&mu_L([a,b]setminus{a_1,a_2,...})+0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img46.svg"
 ALT="\begin{eqnarray*}
b - a = \mu_L([a,b]) &amp;=&amp; \mu_L([a,b] \setminus \{a_1,a_2,...\}...
..._1,a_2,...\}) \\\\
&amp;=&amp; \mu_L([a,b] \setminus \{a_1,a_2,...\}) + 0
\end{eqnarray*}">|; 

$key = q/{f_n:ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\{ f_n : n \in \setN \}$">|; 

1;

