# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 30.00ex; " SRC="|."$dir".q|img39.svg"
 ALT="\begin{eqnarray*}
\lim_{m \to \infty} \sum_{k = m}^{+\infty} a_k &amp;=&amp; \lim_{m \to...
...sum_{k = 0}^{+\infty} a_k - \sum_{k = 0}^{+\infty} a_k \\\\
&amp;=&amp; 0
\end{eqnarray*}">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img104.svg"
 ALT="$0$">|; 

$key = q/AsubseteqsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img1.svg"
 ALT="$A \subseteq \setR$">|; 

$key = q/D_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img79.svg"
 ALT="$D_i$">|; 

$key = q/D_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img29.svg"
 ALT="$D_n$">|; 

$key = q/KinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img54.svg"
 ALT="$K \in \setR$">|; 

$key = q/M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img69.svg"
 ALT="$M$">|; 

$key = q/MinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img50.svg"
 ALT="$M \in \setR$">|; 

$key = q/R_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img40.svg"
 ALT="$R_m$">|; 

$key = q/S_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img30.svg"
 ALT="$S_n$">|; 

$key = q/S_n=S_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img89.svg"
 ALT="$S_n = S_m$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img101.svg"
 ALT="$a$">|; 

$key = q/a^{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img103.svg"
 ALT="$a^{n + 1}$">|; 

$key = q/a_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img36.svg"
 ALT="$a_k$">|; 

$key = q/abs{a_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\abs{a_k}$">|; 

$key = q/abs{a}strictsuperieur1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\abs{a} \strictsuperieur 1$">|; 

$key = q/displaystyle-Kle-sum_{k=0}^nabs{a_k}lesum_{k=0}^na_klesum_{k=0}^nabs{a_k}leK;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle -K \le - \sum_{k = 0}^n \abs{a_k} \le \sum_{k = 0}^n a_k \le \sum_{k = 0}^n \abs{a_k} \le K$">|; 

$key = q/displaystyle-Klesum_{k=0}^na_kleK;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle -K \le \sum_{k = 0}^n a_k \le K$">|; 

$key = q/displaystyle-abs{a_k}lea_kleabs{a_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle -\abs{a_k} \le a_k \le \abs{a_k}$">|; 

$key = q/displaystyleA={a_kinsetR:kinmathcal{Z}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle A = \{ a_k \in \setR : k \in \mathcal{Z} \}$">|; 

$key = q/displaystyleA={a_kinsetR:kinsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle A = \{ a_k \in \setR : k \in \setN \}$">|; 

$key = q/displaystyleB_m=inf{S_n:ninsetN,,ngem};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle B_m = \inf\{ S_n : n \in \setN,   n \ge m \}$">|; 

$key = q/displaystyleB_mgeS_m-frac{epsilon}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle B_m \ge S_m - \frac{\epsilon}{2}$">|; 

$key = q/displaystyleD_i=sum_{k=m+1}^iabs{a_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.46ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle D_i = \sum_{k = m + 1}^i \abs{a_k}$">|; 

$key = q/displaystyleD_n=sum_{k=m}^na_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle D_n = \sum_{k = m}^n a_k$">|; 

$key = q/displaystyleE=S_{m-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle E = S_{m - 1}$">|; 

$key = q/displaystyleH_m=sup{S_n:ninsetN,,ngem};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle H_m = \sup\{ S_n : n \in \setN,   n \ge m \}$">|; 

$key = q/displaystyleH_mleS_m+frac{epsilon}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle H_m \le S_m + \frac{\epsilon}{2}$">|; 

$key = q/displaystyleH_mleS_m+frac{epsilon}{2}=parentheses{S_m-frac{epsilon}{2}}+epsilonleB_m+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.44ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle H_m \le S_m + \frac{\epsilon}{2} = \parentheses{S_m - \frac{\epsilon}{2}} + \epsilon \le B_m + \epsilon$">|; 

$key = q/displaystyleP={abs{a_k}insetR:kinsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle P = \{ \abs{a_k} \in \setR : k \in \setN \}$">|; 

$key = q/displaystyleP={p_kinsetR:p_kge0,kinsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle P = \{ p_k \in \setR : p_k \ge 0,  k \in \setN \}$">|; 

$key = q/displaystyleR_m=sum_{k=m}^{+infty}a_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle R_m = \sum_{k = m}^{+\infty} a_k$">|; 

$key = q/displaystyleS_n-S_m=sum_{k=m+1}^na_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.10ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle S_n - S_m = \sum_{k = m + 1}^n a_k$">|; 

$key = q/displaystyleS_n=E+D_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle S_n = E + D_n$">|; 

$key = q/displaystyleS_n=S_m+sum_{k=m+1}^na_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.10ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle S_n = S_m + \sum_{k = m + 1}^n a_k$">|; 

$key = q/displaystyleS_n=sum_{k=0}^mp_k+sum_{k=m+1}^np_k=S_m+sum_{k=m+1}^np_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.10ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle S_n = \sum_{k = 0}^m p_k + \sum_{k = m + 1}^n p_k = S_m + \sum_{k = m + 1}^n p_k$">|; 

$key = q/displaystyleS_n=sum_{k=0}^na_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle S_n = \sum_{k = 0}^n a_k$">|; 

$key = q/displaystyleS_n=sum_{k=0}^np_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle S_n = \sum_{k = 0}^n p_k$">|; 

$key = q/displaystyleS_n=sum_{k=0}^{m-1}a_k+sum_{k=m}^na_k=S_{m-1}+D_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.27ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle S_n = \sum_{k = 0}^{m - 1} a_k + \sum_{k = m}^n a_k = S_{m - 1} + D_n$">|; 

$key = q/displaystyleS_ngeS_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle S_n \ge S_m$">|; 

$key = q/displaystyleS_ngeminaccolades{S_m,S_m-frac{epsilon}{2}}=S_m-frac{epsilon}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.44ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle S_n \ge \min \accolades{S_m, S_m - \frac{\epsilon}{2}} = S_m - \frac{\epsilon}{2}$">|; 

$key = q/displaystyleS_nleM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle S_n \le M$">|; 

$key = q/displaystyleS_nlemaxaccolades{S_m,S_m+frac{epsilon}{2}}=S_m+frac{epsilon}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.44ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle S_n \le \max \accolades{S_m, S_m + \frac{\epsilon}{2}} = S_m + \frac{\epsilon}{2}$">|; 

$key = q/displaystyleZ^+={kinmathcal{Z}:a_kge0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle Z^+ = \{ k \in \mathcal{Z} : a_k \ge 0 \}$">|; 

$key = q/displaystyleZ^-={kinmathcal{Z}:a_kstrictinferieur0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle Z^- = \{ k \in \mathcal{Z} : a_k \strictinferieur 0 \}$">|; 

$key = q/displaystyleabs{S_m-S_n}lefrac{epsilon}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle \abs{S_m - S_n} \le \frac{\epsilon}{2}$">|; 

$key = q/displaystyleabs{S_m-S_n}lesum_{k=m+1}^{+infty}abs{a_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.37ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle \abs{S_m - S_n} \le \sum_{k = m + 1}^{+\infty} \abs{a_k}$">|; 

$key = q/displaystyleabs{S_n-S_m}=abs{sum_{k=m+1}^na_k}lesum_{k=m+1}^nabs{a_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.37ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle \abs{S_n - S_m} = \abs{\sum_{k = m + 1}^n a_k} \le \sum_{k = m + 1}^n \abs{a_k}$">|; 

$key = q/displaystyleinf_{substack{minsetNmgeM}}H_mlesup_{substack{minsetNmgeM}}B_m+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.55ex; vertical-align: -3.86ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle \inf_{ \substack{ m \in \setN \ m \ge M } } H_m \le \sup_{ \substack{ m \in \setN \ m \ge M } } B_m + \epsilon$">|; 

$key = q/displaystylelambda=liminf_{ntoinfty}S_n=sup_{ninsetN}inf{S_k:kinsetN,kgen};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.16ex; vertical-align: -2.33ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \lambda = \liminf_{n \to \infty} S_n = \sup_{n \in \setN} \inf \{ S_k : k \in \setN,  k \ge n \}$">|; 

$key = q/displaystylelambda=sup_{minsetN}B_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.03ex; vertical-align: -2.33ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \lambda = \sup_{m \in \setN} B_m$">|; 

$key = q/displaystylelambdalesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \lambda \le \sigma$">|; 

$key = q/displaystylelim_{mtoinfty}sum_{k=m}^{+infty}abs{a_k}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle \lim_{m \to \infty} \sum_{k = m}^{+\infty} \abs{a_k} = 0$">|; 

$key = q/displaystylelim_{ntoinfty}D_n=lim_{ntoinfty}S_n-E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.42ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \lim_{n \to \infty} D_n = \lim_{n \to \infty} S_n - E$">|; 

$key = q/displaystylelim_{ntoinfty}D_n=sum_{k=m}^{+infty}a_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \lim_{n \to \infty} D_n = \sum_{k = m}^{+\infty} a_k$">|; 

$key = q/displaystylelim_{ntoinfty}S_n=E+lim_{ntoinfty}D_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.42ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \lim_{n \to \infty} S_n = E + \lim_{n \to \infty} D_n$">|; 

$key = q/displaystylelim_{ntoinfty}S_n=sum_{k=0}^{+infty}a_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \lim_{n \to \infty} S_n = \sum_{k = 0}^{+\infty} a_k$">|; 

$key = q/displaystylesigma=inf_{minsetN}H_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.58ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle \sigma = \inf_{m \in \setN} H_m$">|; 

$key = q/displaystylesigma=limsup_{ntoinfty}S_n=inf_{ninsetN}sup{S_k:kinsetN,kgen};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.00ex; vertical-align: -2.17ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle \sigma = \limsup_{n \to \infty} S_n = \inf_{n \in \setN} \sup \{ S_k : k \in \setN,  k \ge n \}$">|; 

$key = q/displaystylesigmalelambda+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle \sigma \le \lambda + \epsilon$">|; 

$key = q/displaystylesigmalelambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle \sigma \le \lambda$">|; 

$key = q/displaystylesum_{i=0}^{+infty}a^i=lim_{ntoinfty}frac{a^{n+1}-1}{a-1}=unsur{1-a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle \sum_{i=0}^{+\infty} a^i = \lim_{n \to \infty} \frac{a^{n + 1} - 1}{a - 1} = \unsur{1 - a}$">|; 

$key = q/displaystylesum_{k=0}^na_k=sum_{k=0}^ma_k+sum_{k=m+1}^na_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.10ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \sum_{k = 0}^n a_k = \sum_{k = 0}^m a_k + \sum_{k = m + 1}^n a_k$">|; 

$key = q/displaystylesum_{k=0}^nabs{a_k}leK;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle \sum_{k = 0}^n \abs{a_k} \le K$">|; 

$key = q/displaystylesum_{k=0}^{+infty}a_k=limsup_{ntoinfty}S_n=liminf_{ntoinfty}S_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\displaystyle \sum_{k = 0}^{+\infty} a_k = \limsup_{n \to \infty} S_n = \liminf_{n \to \infty} S_n$">|; 

$key = q/displaystylesum_{k=0}^{+infty}a_k=sum_{k=0}^{m-1}a_k+sum_{k=m}^{+infty}a_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.27ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \sum_{k = 0}^{+\infty} a_k = \sum_{k = 0}^{m - 1} a_k + \sum_{k = m}^{+\infty} a_k$">|; 

$key = q/displaystylesum_{k=0}^{+infty}abs{a_k}=sup_{ninsetN}sum_{k=0}^nabs{a_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \sum_{k = 0}^{+\infty} \abs{a_k} = \sup_{n \in \setN} \sum_{k = 0}^n \abs{a_k}$">|; 

$key = q/displaystylesum_{k=0}^{+infty}p_k=lim_{ntoinfty}sum_{k=0}^np_k=sup_{ninsetN}sum_{k=0}^np_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \sum_{k = 0}^{+\infty} p_k = \lim_{n \to \infty} \sum_{k = 0}^n p_k = \sup_{n \in \setN} \sum_{k = 0}^n p_k$">|; 

$key = q/displaystylesum_{k=m+1}^nabs{a_k}lelim_{itoinfty}D_i=sum_{k=m+1}^{+infty}abs{a_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.37ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle \sum_{k = m + 1}^n \abs{a_k} \le \lim_{i \to \infty} D_i = \sum_{k = m + 1}^{+\infty} \abs{a_k}$">|; 

$key = q/displaystylesum_{k=m+1}^np_kge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.10ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \sum_{k = m + 1}^n p_k \ge 0$">|; 

$key = q/displaystylesum_{k=m}^{+infty}a_k=lim_{ntoinfty}sum_{k=m}^na_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \sum_{k = m}^{+\infty} a_k = \lim_{n \to \infty} \sum_{k = m}^n a_k$">|; 

$key = q/displaystylesum_{k=m}^{+infty}a_k=sum_{k=0}^{+infty}a_k-sum_{k=0}^{m-1}a_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.27ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \sum_{k = m}^{+\infty} a_k = \sum_{k = 0}^{+\infty} a_k - \sum_{k = 0}^{m - 1} a_k$">|; 

$key = q/displaystylesum_{k=m}^{+infty}abs{a_k}lefrac{epsilon}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.21ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle \sum_{k = m}^{+\infty} \abs{a_k} \le \frac{\epsilon}{2}$">|; 

$key = q/displaystylesum_{kinZ^+}a_k=supaccolades{sum_{kinI}a_k:IsubseteqZ^+,Imathrm{fini}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.41ex; vertical-align: -3.27ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \sum_{k \in Z^+} a_k = \sup \accolades{ \sum_{k \in I} a_k : I \subseteq Z^+,  I  \mathrm{fini} }$">|; 

$key = q/displaystylesum_{kinZ^-}a_k=-supaccolades{-sum_{kinI}a_k:IsubseteqZ^-,Imathrm{fini}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.41ex; vertical-align: -3.27ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \sum_{k \in Z^-} a_k = - \sup \accolades{ - \sum_{k \in I} a_k : I \subseteq Z^-,  I  \mathrm{fini} }$">|; 

$key = q/displaystylesum_{kinmathcal{Z}}a_k=sum_{kinZ^+}a_k+sum_{kinZ^-}a_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -3.27ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \sum_{k \in \mathcal{Z}} a_k = \sum_{k \in Z^+} a_k + \sum_{k \in Z^-} a_k$">|; 

$key = q/displaystylesum_{kinsetN}a_k=sum_{k=0}^{+infty}a_k=lim_{ntoinfty}sum_{k=0}^na_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.31ex; vertical-align: -3.17ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \sum_{k \in \setN} a_k = \sum_{k = 0}^{+\infty} a_k = \lim_{n \to \infty} \sum_{k = 0}^n a_k$">|; 

$key = q/displaystylesum_{kinsetZ}a_k=sum_{k=-infty}^{+infty}a_k=lim_{ntoinfty}sum_{k=-n}^na_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.37ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \sum_{k \in \setZ} a_k = \sum_{k = -\infty}^{+\infty} a_k = \lim_{n \to \infty} \sum_{k = -n}^n a_k$">|; 

$key = q/epsilonslash2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\epsilon / 2$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img81.svg"
 ALT="$i$">|; 

$key = q/igem+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img82.svg"
 ALT="$i \ge m + 1$">|; 

$key = q/lambda=sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\lambda = \sigma$">|; 

$key = q/lambdalesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\lambda \le \sigma$">|; 

$key = q/m+1strictsuperieurmgeM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img85.svg"
 ALT="$m + 1 \strictsuperieur m \ge M$">|; 

$key = q/m,n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img72.svg"
 ALT="$m,n$">|; 

$key = q/m,ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$m,n \in \setN$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$m$">|; 

$key = q/mathcal{Z};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\mathcal{Z}$">|; 

$key = q/mathcal{Z}=setN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\mathcal{Z} = \setN$">|; 

$key = q/mathcal{Z}=setZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\mathcal{Z} = \setZ$">|; 

$key = q/mathcal{Z}subseteqsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\mathcal{Z} \subseteq \setZ$">|; 

$key = q/mge1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img20.svg"
 ALT="$m \ge 1$">|; 

$key = q/mgeM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img71.svg"
 ALT="$m \ge M$">|; 

$key = q/minsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img19.svg"
 ALT="$m \in \setN$">|; 

$key = q/mlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img45.svg"
 ALT="$m \le n$">|; 

$key = q/mtoinfty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img38.svg"
 ALT="$m \to \infty$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$n$">|; 

$key = q/n=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img88.svg"
 ALT="$n = m$">|; 

$key = q/ngem+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img73.svg"
 ALT="$n \ge m + 1$">|; 

$key = q/ngem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img24.svg"
 ALT="$n \ge m$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img18.svg"
 ALT="$n \in \setN$">|; 

$key = q/p_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img47.svg"
 ALT="$p_k$">|; 

$key = q/{D_n:ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\{ D_n : n \in \setN \}$">|; 

$key = q/{S_n:ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\{ S_n : n \in \setN \}$">|; 

1;

