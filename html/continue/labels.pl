# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate labels original text with physical files.


$key = q/chap:limite/;
$external_labels{$key} = "$URL/" . q|continue.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:continuite_uniforme/;
$external_labels{$key} = "$URL/" . q|continue.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2023 (Released January 1, 2023)
# labels from external_latex_labels array.


$key = q/chap:limite/;
$external_latex_labels{$key} = q|1 Continuité|; 
$noresave{$key} = "$nosave";

$key = q/sec:continuite_uniforme/;
$external_latex_labels{$key} = q|1.7 Continuité uniforme|; 
$noresave{$key} = "$nosave";

1;

