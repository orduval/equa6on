# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img38.svg"
 ALT="\begin{theoreme}
\par Soit $f \in \continue(I,J)$ où $I = [a,b]$ est un inter...
... \{ f(a), f(b) \} \\\\
\beta = \max \{ f(a), f(b) \}
\end{Eqts}\par\end{theoreme}">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$0$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$A$">|; 

$key = q/AsubseteqD;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img4.svg"
 ALT="$A \subseteq D$">|; 

$key = q/Delta(a,epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\Delta(a,\epsilon)$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$F$">|; 

$key = q/I(epsilon)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$I(\epsilon) \strictsuperieur 0$">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img79.svg"
 ALT="$I$">|; 

$key = q/I=[a-1,a+1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$I = [a - 1, a + 1]$">|; 

$key = q/M=(N+1)cdotepsilon+abs{f(alpha)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$M = (N + 1) \cdot \epsilon + \abs{f(\alpha)}$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img124.svg"
 ALT="$N$">|; 

$key = q/NinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img114.svg"
 ALT="$N \in \setN$">|; 

$key = q/[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$[\alpha,\beta]$">|; 

$key = q/a,bin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img112.svg"
 ALT="$a,b \in [\alpha,\beta]$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$a$">|; 

$key = q/abs{a-b}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\abs{a - b} \le \delta$">|; 

$key = q/abs{p(x)-p(a)}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\abs{p(x) - p(a)} \le \epsilon$">|; 

$key = q/abs{s-t}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\abs{s - t} \le \delta$">|; 

$key = q/abs{x-a}le1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\abs{x - a} \le 1$">|; 

$key = q/abs{x-a}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\abs{x - a} \le \delta$">|; 

$key = q/ainA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$a \in A$">|; 

$key = q/ain[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$a \in [\alpha,\beta]$">|; 

$key = q/aininterieurI;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img75.svg"
 ALT="$a \in \interieur I$">|; 

$key = q/ainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img73.svg"
 ALT="$a \in \setR$">|; 

$key = q/alpha,betaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\alpha,\beta \in \corps$">|; 

$key = q/alpha,betainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\alpha,\beta \in \setR$">|; 

$key = q/alphacdotf+betacdotg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\alpha \cdot f + \beta \cdot g$">|; 

$key = q/alphalebeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\alpha \le \beta$">|; 

$key = q/c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$c$">|; 

$key = q/cin[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$c \in [a,b]$">|; 

$key = q/cin{a,b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$c \in \{a,b\}$">|; 

$key = q/continue(A,F);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\continue(A,F)$">|; 

$key = q/continue(A,corps);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\continue(A,\corps)$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\corps$">|; 

$key = q/delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\delta$">|; 

$key = q/delta=min{delta_0,delta_1,...,delta_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\delta = \min \{ \delta_0, \delta_1, ..., \delta_n \}$">|; 

$key = q/delta_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\delta_k$">|; 

$key = q/deltainDelta(a,epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\delta \in \Delta(a,\epsilon)$">|; 

$key = q/deltainintervalleouvert{0}{1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\delta \in \intervalleouvert{0}{1}$">|; 

$key = q/deltastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\delta \strictsuperieur 0$">|; 

$key = q/displaystyle0strictinferieurgammaledeltainDelta(a,epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle 0 \strictinferieur \gamma \le \delta \in \Delta(a,\epsilon)$">|; 

$key = q/displaystyleDelta(a,epsilon)neemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle \Delta(a,\epsilon) \ne \emptyset$">|; 

$key = q/displaystyleDelta(a,epsilon)subseteqintervallesemiouvertgauche{0}{sigma(a,epsilon)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle \Delta(a,\epsilon) \subseteq \intervallesemiouvertgauche{0}{\sigma(a,\epsilon)}$">|; 

$key = q/displaystyleI(epsilon)=inf_{ain[alpha,beta]}sigma(a,epsilon)gedeltastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.09ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle I(\epsilon) = \inf_{a \in [\alpha,\beta]} \sigma(a,\epsilon) \ge \delta \strictsuperieur 0$">|; 

$key = q/displaystyleI=inf{f(x):xin[a,b]}=inff([a,b]);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle I = \inf \{ f(x) : x \in [a,b] \} = \inf f([a,b])$">|; 

$key = q/displaystyle]0,delta]subseteqDelta(a,epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle ]0,\delta] \subseteq \Delta(a,\epsilon)$">|; 

$key = q/displaystyleabs{f(b)-f(a)}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\displaystyle \abs{f(b) - f(a)} \le \epsilon$">|; 

$key = q/displaystyleabs{f(s)-f(t)}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \abs{f(s) - f(t)} \le \epsilon$">|; 

$key = q/displaystyleabs{f(x)-f(y)}lesum_{i=0}^Nabs{f(x_i)-f(x_{i-1})}le(N+1)cdotepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.28ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle \abs{f(x) - f(y)} \le \sum_{i = 0}^N \abs{f(x_i) - f(x_{i - 1})} \le (N + 1) \cdot \epsilon$">|; 

$key = q/displaystyleabs{f(x)}leabs{f(x)-f(alpha)}+abs{f(alpha)}leM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\displaystyle \abs{f(x)} \le \abs{f(x) - f(\alpha)} + \abs{f(\alpha)} \le M$">|; 

$key = q/displaystyleabs{h}=abs{frac{y-x}{N}}lefrac{beta-alpha}{N}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\displaystyle \abs{h} = \abs{\frac{y - x}{N}} \le \frac{\beta - \alpha}{N} \le \delta$">|; 

$key = q/displaystyleabs{p(s)-p(t)}leepsiloncdotfrac{sum_iabs{a_i}}{sum_jabs{a_j}}=epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.07ex; vertical-align: -2.67ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \abs{p(s) - p(t)} \le \epsilon \cdot \frac{ \sum_i \abs{a_i} }{ \sum_j \abs{a_j} } = \epsilon$">|; 

$key = q/displaystyleabs{p(s)-p(t)}lesum_{i=0}^nabs{a_i}cdotabs{s^i-t^i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \abs{p(s) - p(t)} \le \sum_{i = 0}^n \abs{a_i} \cdot \abs{s^i - t^i}$">|; 

$key = q/displaystyleabs{s-t}ledeltalefrac{epsilon}{ncdotM^{n-1}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \abs{s - t} \le \delta \le \frac{\epsilon}{ n \cdot M^{n - 1} }$">|; 

$key = q/displaystyleabs{s^k-t^k}lefrac{epsilon}{sum_jabs{a_j}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.32ex; vertical-align: -2.67ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \abs{s^k - t^k} \le \frac{\epsilon}{\sum_j \abs{a_j}}$">|; 

$key = q/displaystyleabs{s^n-t^n}leabs{s-t}cdotncdotM^{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle \abs{s^n - t^n} \le \abs{s - t} \cdot n \cdot M^{n - 1}$">|; 

$key = q/displaystyleabs{s^n-t^n}ledeltacdotncdotM^nleepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \abs{s^n - t^n} \le \delta \cdot n \cdot M^n \le \epsilon$">|; 

$key = q/displaystyleabs{sigma(a,epsilon)-delta}lepsi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle \abs{\sigma(a,\epsilon) - \delta} \le \psi$">|; 

$key = q/displaystyleabs{s},abs{t}leM=max{abs{alpha},abs{beta}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \abs{s}, \abs{t} \le M = \max \{ \abs{\alpha} , \abs{\beta} \}$">|; 

$key = q/displaystyledeltainbigcap_{ain[alpha,beta]}Delta(a,epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.06ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle \delta \in \bigcap_{a \in [\alpha,\beta]} \Delta(a,\epsilon)$">|; 

$key = q/displaystyleemptysetne]0,I(epsilon)[subseteqbigcap_{ain[alpha,beta]}(0,sigma(a,epsilon))subseteqbigcap_{ain[alpha,beta]}Delta(a,epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.06ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle \emptyset \ne  ]0,I(\epsilon)[  \subseteq \bigcap_{a \in [\alph...
...\sigma(a,\epsilon)) \subseteq \bigcap_{a \in [\alpha,\beta]} \Delta(a,\epsilon)$">|; 

$key = q/displaystylef(lambda)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle f(\lambda) = I$">|; 

$key = q/displaystylef(lambda)=inff([a,b])=minf([a,b]);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle f(\lambda) = \inf f([a,b]) = \min f([a,b])$">|; 

$key = q/displaystylef(sigma)=supf([a,b])=maxf([a,b]);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle f(\sigma) = \sup f([a,b]) = \max f([a,b])$">|; 

$key = q/displaystylef(x_n)-Ileunsur{2^n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\displaystyle f(x_n) - I \le \unsur{2^n}$">|; 

$key = q/displaystylefbig([a-gamma,a+gamma]big)subseteq[f(a)-epsilon,f(a)+epsilon];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle f\big( [a - \gamma, a + \gamma] \big) \subseteq [f(a) - \epsilon, f(a) + \epsilon]$">|; 

$key = q/displaystylefrac{beta-alpha}{N}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle \frac{\beta - \alpha}{N} \le \delta$">|; 

$key = q/displaystyleh=frac{y-x}{N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.70ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\displaystyle h = \frac{y - x}{N}$">|; 

$key = q/displaystyleintervalleouvert{0}{sigma(a,epsilon)}subseteqDelta(a,epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\displaystyle \intervalleouvert{0}{\sigma(a,\epsilon)} \subseteq \Delta(a,\epsilon)$">|; 

$key = q/displaystylelambda=lim_{ntoinfty}x_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.42ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle \lambda = \lim_{n \to \infty} x_n$">|; 

$key = q/displaystylelim_{ntoinfty}f(x_n)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle \lim_{n \to \infty} f(x_n) = I$">|; 

$key = q/displaystylelim_{ntoinfty}f(x_n)=f(lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle \lim_{n \to \infty} f(x_n) = f(\lambda)$">|; 

$key = q/displaystylelim_{stot}abs{f(s)-f(t)}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \lim_{s \to t} \abs{f(s) - f(t)} = 0$">|; 

$key = q/displaystylelim_{stot}f(s)=f(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \lim_{s \to t} f(s) = f(t)$">|; 

$key = q/displaystylelim_{substack{xtoaxinA}}f(x)=f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.62ex; vertical-align: -2.80ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \lim_{ \substack{ x \to a \ x \in A } } f(x) = f(a)$">|; 

$key = q/displaystylelim_{substack{xtoaxinD}}f(x)=f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.62ex; vertical-align: -2.80ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \lim_{ \substack{ x \to a \ x \in D } } f(x) = f(a)$">|; 

$key = q/displaystylemu:xmapstox^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.26ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \mu : x \mapsto x^n$">|; 

$key = q/displaystylenorme{e}_infty=norme{f-g}_infty=sup{norme{f(x)-g(x)}:xinA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \norme{e}_\infty = \norme{f - g}_\infty = \sup \{ \norme{f(x) - g(x)} : x \in A \}$">|; 

$key = q/displaystylenorme{f}_inftyleM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\displaystyle \norme{f}_\infty \le M$">|; 

$key = q/displaystylenorme{u}_continue=supbig{norme{u(x)}:xinAbig};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \norme{u}_\continue = \sup \big\{ \norme{u(x)} : x \in A \big\}$">|; 

$key = q/displaystylenorme{u}_infty=norme{u}_continue;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \norme{u}_\infty = \norme{u}_\continue$">|; 

$key = q/displaystylep(s)-p(t)=sum_{i=0}^na_icdot(s^i-t^i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle p(s) - p(t) = \sum_{i = 0}^n a_i \cdot (s^i - t^i)$">|; 

$key = q/displaystylep(x)=sum_{i=0}^na_icdotx^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle p(x) = \sum_{i = 0}^n a_i \cdot x^i$">|; 

$key = q/displaystylepsi=sigma(a,epsilon)-xstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle \psi = \sigma(a,\epsilon) - x \strictsuperieur 0$">|; 

$key = q/displaystyles^n-t^n=(s-t)sum_{i=0}^{n-1}s^{n-1-i}cdott^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.22ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle s^n - t^n = (s - t) \sum_{i = 0}^{n - 1} s^{n - 1 - i} \cdot t^i$">|; 

$key = q/displaystylesigma(a,epsilon)-deltalesigma(a,epsilon)-x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle \sigma(a,\epsilon) - \delta \le \sigma(a,\epsilon) - x$">|; 

$key = q/displaystylesigma(a,epsilon)geI(epsilon)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle \sigma(a,\epsilon) \ge I(\epsilon) \strictsuperieur 0$">|; 

$key = q/displaystylesigma(a,epsilon)gedeltastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\displaystyle \sigma(a,\epsilon) \ge \delta \strictsuperieur 0$">|; 

$key = q/displaystylexin]0,delta]subseteqDelta(a,epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle x \in  ]0,\delta] \subseteq \Delta(a,\epsilon)$">|; 

$key = q/displaystylexledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle x \le \delta$">|; 

$key = q/distance(a,setRsetminusI)=1strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\distance(a,\setR \setminus I) = 1 \strictsuperieur 0$">|; 

$key = q/e=f-g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img25.svg"
 ALT="$e = f - g$">|; 

$key = q/epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\epsilon$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/epsilonstrictsuperieur;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\epsilon \strictsuperieur$">|; 

$key = q/f([a,b]);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img139.svg"
 ALT="$f([a,b])$">|; 

$key = q/f(a)lef(c)lef(b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$f(a) \le f(c) \le f(b)$">|; 

$key = q/f(a)levarphilef(b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$f(a) \le \varphi \le f(b)$">|; 

$key = q/f(a)strictinferieurf(c)strictinferieurf(b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$f(a) \strictinferieur f(c) \strictinferieur f(b)$">|; 

$key = q/f(a)strictsuperieurf(b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$f(a) \strictsuperieur f(b)$">|; 

$key = q/f,g:AtoB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$f,g : A \to B$">|; 

$key = q/f,gincontinue(A,corps);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$f,g \in \continue(A,\corps)$">|; 

$key = q/f:AmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$f : A \mapsto F$">|; 

$key = q/f:DtoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f : D \to F$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$f$">|; 

$key = q/fincontinue([a,b],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img130.svg"
 ALT="$f \in \continue([a,b],\setR)$">|; 

$key = q/g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img23.svg"
 ALT="$g$">|; 

$key = q/gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\gamma$">|; 

$key = q/i=0,1,2,...,N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img120.svg"
 ALT="$i = 0,1,2,...,N$">|; 

$key = q/lambdain[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\lambda \in [a,b]$">|; 

$key = q/mu:[alpha,beta]mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\mu : [\alpha,\beta] \mapsto \setR$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\mu$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img48.svg"
 ALT="$n \in \setN$">|; 

$key = q/norme{.}_continue;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\norme{.}_\continue$">|; 

$key = q/s,tinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img43.svg"
 ALT="$s,t \in A$">|; 

$key = q/s,tin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$s,t \in [\alpha,\beta]$">|; 

$key = q/s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img61.svg"
 ALT="$s$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\setR$">|; 

$key = q/sigma(a,epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\sigma(a,\epsilon)$">|; 

$key = q/sigmain[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\sigma \in [a,b]$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img62.svg"
 ALT="$t$">|; 

$key = q/tinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img47.svg"
 ALT="$t \in A$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$u$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\varphi$">|; 

$key = q/varphiin{f(a),f(b)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\varphi \in \{f(a),f(b)\}$">|; 

$key = q/x,yin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$x,y \in [\alpha,\beta]$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img125.svg"
 ALT="$x$">|; 

$key = q/x_0=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img121.svg"
 ALT="$x_0 = x$">|; 

$key = q/x_N=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img122.svg"
 ALT="$x_N = y$">|; 

$key = q/x_i=x+icdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img119.svg"
 ALT="$x_i = x + i \cdot h$">|; 

$key = q/xinDelta(a,epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img99.svg"
 ALT="$x \in \Delta(a,\epsilon)$">|; 

$key = q/xinI;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img78.svg"
 ALT="$x \in I$">|; 

$key = q/xin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$x \in [\alpha,\beta]$">|; 

$key = q/xinintervalleouvert{0}{sigma(a,epsilon)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img93.svg"
 ALT="$x \in \intervalleouvert{0}{\sigma(a,\epsilon)}$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img126.svg"
 ALT="$y$">|; 

$key = q/{x_1,x_2,...};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\{x_1,x_2,...\}$">|; 

1;

