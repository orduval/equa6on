# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/a_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img15.svg"
 ALT="$a_0$">|; 

$key = q/a_kcdotb_{k+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img10.svg"
 ALT="$a_k \cdot b_{k + 1}$">|; 

$key = q/a_{k+1}cdotb_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img7.svg"
 ALT="$a_{k + 1} \cdot b_k$">|; 

$key = q/a_{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img16.svg"
 ALT="$a_{n + 1}$">|; 

$key = q/difference;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\difference$">|; 

$key = q/displaystyleA={a_k:kinsetN}subseteqcorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\displaystyle A = \{ a_k : k \in \setN \} \subseteq \corps$">|; 

$key = q/displaystyleB={b_k:kinsetN}subseteqcorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle B = \{ b_k : k \in \setN \} \subseteq \corps$">|; 

$key = q/displaystyledifference(a_k+b_k)=a_{k+1}+b_{k+1}-a_k-b_k=differencea_k+differenceb_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \difference (a_k + b_k) = a_{k + 1} + b_{k + 1} - a_k - b_k = \difference a_k + \difference b_k$">|; 

$key = q/displaystyledifference(a_kcdotb_k)=a_{k+1}cdotb_{k+1}-a_kcdotb_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \difference (a_k \cdot b_k) = a_{k + 1} \cdot b_{k + 1} - a_k \cdot b_k$">|; 

$key = q/displaystyledifference(a_kcdotb_k)=a_{k+1}cdotb_{k+1}-a_kcdotb_{k+1}+a_kcdotb_{k+1}-a_kcdotb_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \difference (a_k \cdot b_k) = a_{k + 1} \cdot b_{k + 1} - a_k \cdot b_{k + 1} + a_k \cdot b_{k + 1} - a_k \cdot b_k$">|; 

$key = q/displaystyledifference(a_kcdotb_k)=a_{k+1}cdotb_{k+1}-a_{k+1}cdotb_k+a_{k+1}cdotb_k-a_kcdotb_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \difference (a_k \cdot b_k) = a_{k + 1} \cdot b_{k + 1} - a_{k + 1} \cdot b_k + a_{k + 1} \cdot b_k - a_k \cdot b_k$">|; 

$key = q/displaystyledifference(a_kcdotb_k)=a_{k+1}cdotdifferenceb_k+differencea_kcdotb_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \difference (a_k \cdot b_k) = a_{k + 1} \cdot \difference b_k + \difference a_k \cdot b_k$">|; 

$key = q/displaystyledifference(a_kcdotb_k)=differencea_kcdotb_{k+1}+a_kcdotdifferenceb_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \difference (a_k \cdot b_k) = \difference a_k \cdot b_{k + 1} + a_k \cdot \difference b_k$">|; 

$key = q/displaystyledifferencea_k=a_{k+1}-a_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.29ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \difference a_k = a_{k + 1} - a_k$">|; 

$key = q/displaystyledifferencesum_{k=0}^na_k=(a_1+...+a_{n+1})-(a_0+...+a_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \difference \sum_{k = 0}^n a_k = (a_1 + ... + a_{n + 1}) - (a_0 + ... + a_n)$">|; 

$key = q/displaystyledifferencesum_{k=0}^na_k=a_{n+1}-a_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \difference \sum_{k = 0}^n a_k = a_{n + 1} - a_0$">|; 

$key = q/displaystyledifferencesum_{k=0}^na_k=sum_{k=0}^ndifferencea_k=a_{n+1}-a_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \difference \sum_{k = 0}^n a_k = \sum_{k = 0}^n \difference a_k = a_{n + 1} - a_0$">|; 

$key = q/displaystyledifferencesum_{k=0}^na_k=sum_{k=1}^{n+1}a_k-sum_{k=0}^na_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.27ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \difference \sum_{k = 0}^n a_k = \sum_{k = 1}^{n + 1} a_k - \sum_{k = 0}^n a_k$">|; 

$key = q/displaystylesum_{k=0}^na_kcdotdifferenceb_k=a_{n+1}cdotb_{n+1}-a_0cdotb_0-sum_{k=0}^ndifferencea_kcdotb_{k+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \sum_{k = 0}^n a_k \cdot \difference b_k = a_{n + 1} \cdot b_{n + 1} - a_0 \cdot b_0 - \sum_{k = 0}^n \difference a_k \cdot b_{k + 1}$">|; 

$key = q/displaystylesum_{k=0}^na_kcdotdifferenceb_k=sum_{k=0}^ndifference(a_kcdotb_k)-sum_{k=0}^ndifferencea_kcdotb_{k+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \sum_{k = 0}^n a_k \cdot \difference b_k = \sum_{k = 0}^n \difference (a_k \cdot b_k) - \sum_{k = 0}^n \difference a_k \cdot b_{k + 1}$">|; 

$key = q/displaystylesum_{k=0}^ndifference(a_kcdotb_k)=a_{n+1}cdotb_{n+1}-a_0cdotb_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \sum_{k = 0}^n \difference (a_k \cdot b_k) = a_{n + 1} \cdot b_{n + 1} - a_0 \cdot b_0$">|; 

$key = q/displaystylesum_{k=0}^ndifference(a_kcdotb_k)=sum_{k=0}^ndifferencea_kcdotb_{k+1}+sum_{k=0}^na_kcdotdifferenceb_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \sum_{k = 0}^n \difference (a_k \cdot b_k) = \sum_{k = 0}^n \difference a_k \cdot b_{k + 1} + \sum_{k = 0}^n a_k \cdot \difference b_k$">|; 

$key = q/displaystylesum_{k=0}^ndifferencea_k=(a_{n+1}-a_n)+...+(a_2-a_1)+(a_1-a_0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \sum_{k = 0}^n \difference a_k = (a_{n + 1} - a_n) + ... + (a_2 - a_1) + (a_1 - a_0)$">|; 

$key = q/displaystylesum_{k=0}^ndifferencea_k=a_{n+1}-a_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \sum_{k = 0}^n \difference a_k = a_{n + 1} - a_0$">|; 

1;

