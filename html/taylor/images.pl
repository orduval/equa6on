# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 17.26ex; " SRC="|."$dir".q|img85.svg"
 ALT="\begin{eqnarray*}
G(x) = \int_a^x (x - t)^N  dt &amp;=&amp; - \big[ (x - x)^{N + 1} - (...
...x - a)^{N + 1} \big] / (N + 1) \\\\
&amp;=&amp; (x - a)^{N + 1} / (N + 1)
\end{eqnarray*}">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$0$">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img205.svg"
 ALT="$1$">|; 

$key = q/Ccdoth^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img197.svg"
 ALT="$C \cdot h^m$">|; 

$key = q/CinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img191.svg"
 ALT="$C \in \setR$">|; 

$key = q/E:OmegasubseteqsetR^mmapstosetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img162.svg"
 ALT="$E : \Omega \subseteq \setR^m \mapsto \setR^n$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img166.svg"
 ALT="$E$">|; 

$key = q/E_a^N(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img187.svg"
 ALT="$E_a^N(f)$">|; 

$key = q/E_a^N(p)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$E_a^N(p) = 0$">|; 

$key = q/E_a^N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img33.svg"
 ALT="$E_a^N$">|; 

$key = q/Esimgrando{b(h)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img169.svg"
 ALT="$E \sim \grando{b(h)}$">|; 

$key = q/Esimgrando{h^k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img179.svg"
 ALT="$E \sim \grando{h^k}$">|; 

$key = q/Esimpetito{b(h)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img165.svg"
 ALT="$E \sim \petito{b(h)}$">|; 

$key = q/Esimpetito{h^{k-1}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img182.svg"
 ALT="$E \sim \petito{h^{k - 1}}$">|; 

$key = q/F,G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img76.svg"
 ALT="$F,G$">|; 

$key = q/F:[alpha,beta]mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img88.svg"
 ALT="$F : [\alpha,\beta] \mapsto \setR$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img93.svg"
 ALT="$F$">|; 

$key = q/G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img97.svg"
 ALT="$G$">|; 

$key = q/Gincontinue^1([alpha,beta],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img96.svg"
 ALT="$G \in \continue^1([\alpha,\beta],\setR)$">|; 

$key = q/MinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img171.svg"
 ALT="$M \in \setR$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img35.svg"
 ALT="$N$">|; 

$key = q/O(h^m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img203.svg"
 ALT="$O(h^m)$">|; 

$key = q/O(h^{m+1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img204.svg"
 ALT="$O(h^{m + 1})$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\Omega$">|; 

$key = q/OmegasubseteqsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\Omega \subseteq \setR^n$">|; 

$key = q/T_a^N(p)=p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$T_a^N(p) = p$">|; 

$key = q/T_a^N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img30.svg"
 ALT="$T_a^N$">|; 

$key = q/V-V_2approxV_2-V_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img208.svg"
 ALT="$V - V_2 \approx V_2 - V_1$">|; 

$key = q/V;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img189.svg"
 ALT="$V$">|; 

$key = q/V_1=v(h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img194.svg"
 ALT="$V_1 = v(h)$">|; 

$key = q/V_2=v(hslashk);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img195.svg"
 ALT="$V_2 = v(h/k)$">|; 

$key = q/[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$[\alpha,\beta]$">|; 

$key = q/[u,v];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$[u,v]$">|; 

$key = q/a,xin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$a,x \in [\alpha,\beta]$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$a$">|; 

$key = q/ain[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$a \in [\alpha,\beta]$">|; 

$key = q/ainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$a \in \setR$">|; 

$key = q/alpha,betainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\alpha, \beta \in \setR$">|; 

$key = q/alphastrictinferieurbeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\alpha \strictinferieur \beta$">|; 

$key = q/astrictinferieurx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img80.svg"
 ALT="$a \strictinferieur x$">|; 

$key = q/b:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img163.svg"
 ALT="$b : \setR \mapsto \setR$">|; 

$key = q/cinintervalleouvert{0}{1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img149.svg"
 ALT="$c \in \intervalleouvert{0}{1}$">|; 

$key = q/cinintervalleouvert{0}{s};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img145.svg"
 ALT="$c \in \intervalleouvert{0}{s}$">|; 

$key = q/cinintervalleouvert{a}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img79.svg"
 ALT="$c \in \intervalleouvert{a}{x}$">|; 

$key = q/cinintervalleouvert{x}{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img81.svg"
 ALT="$c \in \intervalleouvert{x}{a}$">|; 

$key = q/continue^3subseteqcontinue^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.47ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\continue^3 \subseteq \continue^2$">|; 

$key = q/displaystyle(1-r)cdotV=V_2-rcdotV_1+O(h^{m+1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img201.svg"
 ALT="$\displaystyle (1 - r) \cdot V = V_2 - r \cdot V_1 + O(h^{m+1})$">|; 

$key = q/displaystyle(x-c)^NF(x)=(x-c)^Npartial^{N+1}f(c)G(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle (x - c)^N  F(x) = (x - c)^N  \partial^{N + 1} f(c)  G(x)$">|; 

$key = q/displaystyle-E_a^N(f)(x)(N+1)(x-c)^N=-partial^{N+1}f(c)frac{(x-c)^N}{N!}(x-a)^{N+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.24ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle - E_a^N(f)(x)  (N + 1)  (x - c)^N = - \partial^{N + 1} f(c)  \frac{(x-c)^N}{N !}  (x - a)^{N + 1}$">|; 

$key = q/displaystyle-int_a^x(x-t)partial^2f(t)dt=-unsur{2}(x-a)^2partial^2f(a)-unsur{2}int_a^x(x-t)^2partial^3f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle - \int_a^x (x - t)  \partial^2 f(t)  dt = - \unsur{2}  (x - a)^2  \partial^2 f(a) - \unsur{2} \int_a^x (x - t)^2  \partial^3 f(t)  dt$">|; 

$key = q/displaystyle0lelim_{Ntoinfty}norme{E_a^N(f)}_inftylesigmalim_{Ntoinfty}frac{abs{beta-alpha}^{N+1}}{(N+1)!}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.11ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle 0 \le \lim_{N \to \infty} \norme{E_a^N(f)}_\infty \le \sigma  \lim_{N \to \infty} \frac{ \abs{\beta - \alpha}^{N + 1} }{(N + 1) !} = 0$">|; 

$key = q/displaystyle0lelim_{hto0}frac{norme{E(h)}}{norme{h}^{k-1}}lelim_{hto0}frac{Mnorme{h}^k}{norme{h}^{k-1}}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.48ex; vertical-align: -2.63ex; " SRC="|."$dir".q|img180.svg"
 ALT="$\displaystyle 0 \le \lim_{h \to 0} \frac{\norme{E(h)}}{\norme{h}^{k - 1}} \le \lim_{h \to 0} \frac{M  \norme{h}^k}{\norme{h}^{k - 1}} = 0$">|; 

$key = q/displaystyleCcdoth^m=V_1-V+O(h^{m+1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img198.svg"
 ALT="$\displaystyle C \cdot h^m = V_1 - V + O(h^{m+1})$">|; 

$key = q/displaystyleE_a^N(f)(x)=f(x)-T_a^N(f)(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle E_a^N(f)(x) = f(x) - T_a^N(f)(x)$">|; 

$key = q/displaystyleE_a^N(f)(x)=f(x)-T_a^N(f)(x)=unsur{N!}int_a^x(x-t)^Npartial^{N+1}f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle E_a^N(f)(x) = f(x) - T_a^N(f)(x) = \unsur{N !} \int_a^x (x - t)^N  \partial^{N + 1} f(t)  dt$">|; 

$key = q/displaystyleE_a^N(f)(x)=partial^{N+1}f(c)frac{(x-a)^{N+1}}{(N+1)!};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle E_a^N(f)(x) = \partial^{N + 1} f(c)  \frac{ (x - a)^{N + 1} }{(N + 1) !}$">|; 

$key = q/displaystyleE_a^N(f)(x)=partial^{N+1}f(c)frac{(x-c)^N}{N!}(x-a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.24ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle E_a^N(f)(x) = \partial^{N + 1} f(c)  \frac{(x-c)^N}{N !}  (x - a)$">|; 

$key = q/displaystyleE_a^N(f)(x)partialG(c)=partial^{N+1}f(c)frac{(x-c)^N}{N!}big[G(x)-G(a)big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.24ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\displaystyle E_a^N(f)(x)  \partial G(c) = \partial^{N + 1} f(c)  \frac{(x-c)^N}{N !}  \big[G(x) - G(a)\big]$">|; 

$key = q/displaystyleE_u^1(s,h)=h^dualcdotpartial^2f(u+ccdoth)cdothcdotfrac{(c-0)^2}{2}=h^dualcdotpartial^2f(u+ccdoth)cdothcdotfrac{c^2}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img144.svg"
 ALT="$\displaystyle E_u^1(s,h) = h^\dual \cdot \partial^2 f(u + c \cdot h) \cdot h \c...
...)^2}{2} = h^\dual \cdot \partial^2 f(u + c \cdot h) \cdot h \cdot \frac{c^2}{2}$">|; 

$key = q/displaystyleE_u^2(s,h)=partial^3f(u+ccdoth):hotimeshotimeshcdotfrac{c^3}{6};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle E_u^2(s,h) = \partial^3 f(u + c \cdot h) : h \otimes h \otimes h \cdot \frac{c^3}{6}$">|; 

$key = q/displaystyleF(a)=f(a)+partialf(a)(x-a)+partial^2f(a)frac{(x-a)^2}{2}+...=T_a^N(f)(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle F(a) = f(a) + \partial f(a)  (x - a) + \partial^2 f(a) \frac{(x - a)^2}{2} + ... = T_a^N(f)(x)$">|; 

$key = q/displaystyleF(x)-F(a)=f(x)-T_a^N(f)(x)=E_a^N(f)(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle F(x) - F(a) = f(x) - T_a^N(f)(x) = E_a^N(f)(x)$">|; 

$key = q/displaystyleF(x)=f(x)+partialf(x)(x-x)+partial^2f(x)frac{(x-x)^2}{2}+...=f(x)+0=f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle F(x) = f(x) + \partial f(x)  (x - x) + \partial^2 f(x) \frac{(x - x)^2}{2} + ... = f(x) + 0 = f(x)$">|; 

$key = q/displaystyleG(a)=(x-a)^{N+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle G(a) = (x - a)^{N + 1}$">|; 

$key = q/displaystyleG(a)=a-a=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle G(a) = a - a = 0$">|; 

$key = q/displaystyleG(x)=(x-x)^{N+1}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle G(x) = (x - x)^{N + 1} = 0$">|; 

$key = q/displaystyleG(x)=x-a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\displaystyle G(x) = x - a$">|; 

$key = q/displaystyleG:tmapsto(x-t)^{N+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle G : t \mapsto (x - t)^{N + 1}$">|; 

$key = q/displaystyleG:tmapstot-a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle G : t \mapsto t - a$">|; 

$key = q/displaystyleM^2=max_{i,j}norme{partial^2_{ij}f}_infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.10ex; vertical-align: -2.04ex; " SRC="|."$dir".q|img150.svg"
 ALT="$\displaystyle M^2 = \max_{i,j} \norme{\partial^2_{ij} f}_\infty$">|; 

$key = q/displaystyleM^3=max_{i,j,k}norme{partial^3_{ijk}f}_infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.15ex; vertical-align: -2.09ex; " SRC="|."$dir".q|img160.svg"
 ALT="$\displaystyle M^3 = \max_{i,j,k} \norme{\partial^3_{ijk} f}_\infty$">|; 

$key = q/displaystyleOD{}{t}left[unsur{2}(x-t)^2right]=(x-t)cdot(-1)=-(x-t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \OD{}{t} \left[ \unsur{2} (x - t)^2 \right] = (x - t) \cdot (-1) = - (x - t)$">|; 

$key = q/displaystyleT_a^N(f)(x)=sum_{k=0}^Nunsur{k!}cdotpartial^kf(a)cdot(x-a)^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.33ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle T_a^N(f)(x) = \sum_{k = 0}^N \unsur{k !} \cdot \partial^k f(a) \cdot (x - a)^k$">|; 

$key = q/displaystyleV=2V_2-V_1+O(h^2)=V_2+(V_2-V_1)+O(h^2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img207.svg"
 ALT="$\displaystyle V = 2 V_2 - V_1 + O(h^2) = V_2 + (V_2 - V_1) + O(h^2)$">|; 

$key = q/displaystyleV=frac{V_2-rcdotV_1}{1-r}+O(h^{m+1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.12ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img202.svg"
 ALT="$\displaystyle V = \frac{V_2 - r \cdot V_1}{1 - r} + O(h^{m+1})$">|; 

$key = q/displaystyleV_2=V+rcdotCcdoth^m+O(h^{m+1})=V+rcdot(V_1-V)+O(h^{m+1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img200.svg"
 ALT="$\displaystyle V_2 = V + r \cdot C \cdot h^m + O(h^{m+1}) = V + r \cdot (V_1 - V) + O(h^{m+1})$">|; 

$key = q/displaystyleabs{E_a^N(f)(x)}lenorme{partial^{N+1}f}_inftyfrac{abs{beta-alpha}^{N+1}}{(N+1)!};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.11ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\displaystyle \abs{E_a^N(f)(x)} \le \norme{\partial^{N + 1} f}_\infty  \frac{ \abs{\beta - \alpha}^{N + 1} }{(N + 1) !}$">|; 

$key = q/displaystyleabs{E_a^N(f)(x)}lenorme{partial^{N+1}f}_inftyfrac{abs{x-a}^{N+1}}{(N+1)!};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.11ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle \abs{E_a^N(f)(x)} \le \norme{\partial^{N + 1} f}_\infty  \frac{ \abs{x - a}^{N + 1} }{(N + 1) !}$">|; 

$key = q/displaystyleabs{mathcal{E}_u^1(h)}leunsur{2}cdotn^2cdotM^2cdotnorme{h}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle \abs{\mathcal{E}_u^1(h)} \le \unsur{2} \cdot n^2 \cdot M^2 \cdot \norme{h}^2$">|; 

$key = q/displaystyleabs{mathcal{E}_u^2(h)}leunsur{6}cdotn^3cdotM^3cdotnorme{h}^3;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img161.svg"
 ALT="$\displaystyle \abs{\mathcal{E}_u^2(h)} \le \unsur{6} \cdot n^3 \cdot M^3 \cdot \norme{h}^3$">|; 

$key = q/displaystyleabs{x-a}leabs{beta-alpha};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle \abs{x - a} \le \abs{\beta - \alpha}$">|; 

$key = q/displaystyleb_k:xmapstox^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.57ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img173.svg"
 ALT="$\displaystyle b_k : x \mapsto x^k$">|; 

$key = q/displaystylef(v)=f(u)+partialf(u)cdot(v-u)+mathcal{E}_u^1(h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\displaystyle f(v) = f(u) + \partial f(u) \cdot (v - u) + \mathcal{E}_u^1(h)$">|; 

$key = q/displaystylef(v)=f(u)+partialf(u)cdoth+h^dualcdotpartial^2f(u)cdoth+mathcal{E}_u^2(h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img158.svg"
 ALT="$\displaystyle f(v) = f(u) + \partial f(u) \cdot h + h^\dual \cdot \partial^2 f(u) \cdot h + \mathcal{E}_u^2(h)$">|; 

$key = q/displaystylef(x)-f(a)=xpartialf(x)-apartialf(a)-int_a^xtpartial^2f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle f(x) - f(a) = x  \partial f(x) - a  \partial f(a) - \int_a^x t  \partial^2 f(t)  dt$">|; 

$key = q/displaystylef(x)=f(a)+(x-a)cdotpartialf(a)+int_a^x(x-t)cdotpartial^2f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle f(x) = f(a) + (x - a) \cdot \partial f(a) + \int_a^x (x - t) \cdot \partial^2 f(t)  dt$">|; 

$key = q/displaystylef(x)=f(a)+(x-a)partialf(a)+unsur{2}(x-a)^2partial^2f(a)+unsur{2}int_a^x(x-t)^2partial^3f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle f(x) = f(a) + (x - a)  \partial f(a) + \unsur{2}  (x - a)^2  \partial^2 f(a) + \unsur{2} \int_a^x (x - t)^2  \partial^3 f(t)  dt$">|; 

$key = q/displaystylef(x)=f(a)+xpartialf(a)+int_a^xxpartial^2f(t)dt-apartialf(a)-int_a^xtpartial^2f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle f(x) = f(a) + x  \partial f(a) + \int_a^x x  \partial^2 f(t)  dt - a  \partial f(a) - \int_a^x t  \partial^2 f(t)  dt$">|; 

$key = q/displaystylef(x)=f(a)+xpartialf(x)-apartialf(a)-int_a^xtpartial^2f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle f(x) = f(a) + x  \partial f(x) - a  \partial f(a) - \int_a^x t  \partial^2 f(t)  dt$">|; 

$key = q/displaystylef(x)=sum_{k=0}^Nunsur{k!}cdotpartial^kf(a)cdot(x-a)^k+unsur{N!}int_a^x(x-t)^Npartial^{N+1}f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.33ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle f(x) = \sum_{k = 0}^N \unsur{k !} \cdot \partial^k f(a) \cdot (x - a)^k + \unsur{N !} \int_a^x (x - t)^N  \partial^{N + 1} f(t)  dt$">|; 

$key = q/displaystylegamma_k=unsur{k!}cdotpartial^kp(0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \gamma_k = \unsur{k !} \cdot \partial^k p(0)$">|; 

$key = q/displaystyleh=v-u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\displaystyle h = v - u$">|; 

$key = q/displaystyleint_a^x(x-t)^{k-1}partial^kf(t)dt=unsur{k}(x-a)^kpartial^kf(a)+unsur{k}int_a^x(x-t)^kpartial^{k+1}f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \int_a^x (x - t)^{k - 1}  \partial^k f(t)  dt = \unsur{k}  (x ...
...k  \partial^k f(a) + \unsur{k} \int_a^x (x - t)^k  \partial^{k + 1} f(t)  dt$">|; 

$key = q/displaystyleint_a^x(x-t)cdotpartial^2f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle \int_a^x (x - t) \cdot \partial^2 f(t)  dt$">|; 

$key = q/displaystyleint_a^x(x-t)partial^2f(t)dt=unsur{2}(x-a)^2partial^2f(a)+unsur{2}int_a^x(x-t)^2partial^3f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \int_a^x (x - t)  \partial^2 f(t)  dt = \unsur{2}  (x - a)^2  \partial^2 f(a) + \unsur{2} \int_a^x (x - t)^2  \partial^3 f(t)  dt$">|; 

$key = q/displaystyleint_a^xpartial^2f(t)dt=partialf(x)-partialf(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \int_a^x \partial^2 f(t)  dt = \partial f(x) - \partial f(a)$">|; 

$key = q/displaystyleint_a^xpartialf(t)dt=f(x)-f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \int_a^x \partial f(t)  dt = f(x) - f(a)$">|; 

$key = q/displaystyleint_a^xpartialf(t)dt=xpartialf(x)-apartialf(a)-int_a^xtpartial^2f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \int_a^x \partial f(t)  dt = x  \partial f(x) - a  \partial f(a) - \int_a^x t  \partial^2 f(t)  dt$">|; 

$key = q/displaystyleint_a^xpartialv(t)u(t)dt=-int_a^x(x-t)partial^2f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle \int_a^x \partial v(t)  u(t)  dt = - \int_a^x (x - t)  \partial^2 f(t)  dt$">|; 

$key = q/displaystyleint_a^xu(x)partialv(x)dx=int_a^xpartialf(t)cdot1dt=int_a^xpartialf(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \int_a^x u(x)  \partial v(x)  dx = \int_a^x \partial f(t) \cdot 1  dt = \int_a^x \partial f(t)  dt$">|; 

$key = q/displaystyleint_a^xu(x)partialv(x)dx=v(x)u(x)-v(a)u(a)-int_a^xv(t)partialu(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \int_a^x u(x)  \partial v(x)  dx = v(x)  u(x) - v(a)  u(a) - \int_a^x v(t)  \partial u(t)  dt$">|; 

$key = q/displaystyleint_a^xv(t)partialu(t)dt=unsur{2}int_a^x(x-t)^2partial^3f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle \int_a^x v(t)  \partial u(t)  dt = \unsur{2} \int_a^x (x - t)^2  \partial^3 f(t)  dt$">|; 

$key = q/displaystylelambda(s)=u+scdot(v-u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle \lambda(s) = u + s \cdot (v - u)$">|; 

$key = q/displaystylelim_{Ntoinfty}norme{E_a^N(f)}_infty=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.88ex; vertical-align: -1.77ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\displaystyle \lim_{N \to \infty} \norme{E_a^N(f)}_\infty = 0$">|; 

$key = q/displaystylelim_{hto0}frac{norme{E(h)}}{b(norme{h})}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img168.svg"
 ALT="$\displaystyle \lim_{h \to 0} \frac{ \norme{E(h)} }{b(\norme{h})} = 0$">|; 

$key = q/displaystylelim_{hto0}frac{norme{E(h)}}{norme{h}^k}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.02ex; vertical-align: -2.63ex; " SRC="|."$dir".q|img176.svg"
 ALT="$\displaystyle \lim_{h \to 0} \frac{ \norme{E(h)} }{\norme{h}^k} = 0$">|; 

$key = q/displaystylelim_{hto0}frac{norme{E(h)}}{norme{h}^{k-1}}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.02ex; vertical-align: -2.63ex; " SRC="|."$dir".q|img181.svg"
 ALT="$\displaystyle \lim_{h \to 0} \frac{\norme{E(h)}}{\norme{h}^{k - 1}} = 0$">|; 

$key = q/displaystylemathcal{E}_u^1(h)=h^dualcdotpartial^2f(u+ccdoth)cdothcdotfrac{c^2}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img148.svg"
 ALT="$\displaystyle \mathcal{E}_u^1(h) = h^\dual \cdot \partial^2 f(u + c \cdot h) \cdot h \cdot \frac{c^2}{2}$">|; 

$key = q/displaystylemathcal{E}_u^2(h)=partial^3f(u+ccdoth):hotimeshotimeshcdotfrac{c^3}{6};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img159.svg"
 ALT="$\displaystyle \mathcal{E}_u^2(h) = \partial^3 f(u + c \cdot h) : h \otimes h \otimes h \cdot \frac{c^3}{6}$">|; 

$key = q/displaystylenorme{E(h)}leMcdotb(norme{h});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img172.svg"
 ALT="$\displaystyle \norme{E(h)} \le M \cdot b(\norme{h})$">|; 

$key = q/displaystylenorme{E(h)}leMcdotnorme{h}^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img178.svg"
 ALT="$\displaystyle \norme{E(h)} \le M \cdot \norme{h}^k$">|; 

$key = q/displaystylenorme{E_a^N(f)}_inftylenorme{partial^{N+1}f}_inftyfrac{abs{beta-alpha}^{N+1}}{(N+1)!};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.11ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\displaystyle \norme{E_a^N(f)}_\infty \le \norme{\partial^{N + 1} f}_\infty  \frac{ \abs{\beta - \alpha}^{N + 1} }{(N + 1) !}$">|; 

$key = q/displaystylenorme{E_a^N(f)}_inftylesigmafrac{abs{beta-alpha}^{N+1}}{(N+1)!};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.11ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\displaystyle \norme{E_a^N(f)}_\infty \le \sigma  \frac{ \abs{\beta - \alpha}^{N + 1} }{(N + 1) !}$">|; 

$key = q/displaystylenorme{partial^nf}_inftylesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle \norme{\partial^n f}_\infty \le \sigma$">|; 

$key = q/displaystylep(x)=p(t+a)=r(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle p(x) = p(t + a) = r(t)$">|; 

$key = q/displaystylep(x)=sum_{i=0}^ngamma_icdotx^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle p(x) = \sum_{i = 0}^n \gamma_i \cdot x^i$">|; 

$key = q/displaystylep(x)=sum_{i=0}^nunsur{i!}cdotpartial^ip(0)cdotx^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle p(x) = \sum_{i = 0}^n \unsur{i !} \cdot \partial^i p(0) \cdot x^i$">|; 

$key = q/displaystylep(x)=sum_{i=0}^nunsur{i!}cdotpartial^ip(a)cdot(x-a)^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle p(x) = \sum_{i = 0}^n \unsur{i !} \cdot \partial^i p(a) \cdot (x - a)^i$">|; 

$key = q/displaystylepartialF(c)big[G(x)-G(a)big]=big[F(x)-F(a)big]partialG(c);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle \partial F(c)  \big[G(x) - G(a)\big] = \big[F(x) - F(a)\big]  \partial G(c)$">|; 

$key = q/displaystylepartialF(t)=partial^{N+1}f(t)frac{(x-t)^N}{N!};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.24ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle \partial F(t) = \partial^{N + 1} f(t)  \frac{(x-t)^N}{N !}$">|; 

$key = q/displaystylepartialG(t)=-(N+1)(x-t)^N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle \partial G(t) = - (N + 1)  (x - t)^N$">|; 

$key = q/displaystylepartialG(t)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle \partial G(t) = 1$">|; 

$key = q/displaystylepartial^2varphi(0)=h^dualcdotpartial^2f(u)cdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle \partial^2 \varphi(0) = h^\dual \cdot \partial^2 f(u) \cdot h$">|; 

$key = q/displaystylepartial^2varphi(s)=h^dualcdotpartial^2f(u+scdoth)cdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\displaystyle \partial^2 \varphi(s) = h^\dual \cdot \partial^2 f(u + s \cdot h) \cdot h$">|; 

$key = q/displaystylepartial^2varphi(s)=sum_{i,j}h_jcdotpartial^2_{ji}f(u+scdoth)cdoth_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle \partial^2 \varphi(s) = \sum_{i,j} h_j \cdot \partial^2_{ji} f(u + s \cdot h) \cdot h_i$">|; 

$key = q/displaystylepartial^3varphi(s)=partial^3f(u+scdoth):hotimeshotimesh;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img155.svg"
 ALT="$\displaystyle \partial^3 \varphi(s) = \partial^3 f(u + s \cdot h) : h \otimes h \otimes h$">|; 

$key = q/displaystylepartial^3varphi(s)=sum_{i,j,k}partial^3_{kji}f(u+scdoth)cdoth_icdoth_jcdoth_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -3.37ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\displaystyle \partial^3 \varphi(s) = \sum_{i,j,k} \partial^3_{kji} f(u + s \cdot h) \cdot h_i \cdot h_j \cdot h_k$">|; 

$key = q/displaystylepartial^ir(0)=partial^ip(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.75ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \partial^i r(0) = \partial^i p(a)$">|; 

$key = q/displaystylepartial^kp(0)=frac{k!}{0!}cdotgamma_k=k!cdotgamma_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \partial^k p(0) = \frac{k !}{0 !} \cdot \gamma_k = k ! \cdot \gamma_k$">|; 

$key = q/displaystylepartial^{N+1}f(c)G(x)=F(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle \partial^{N + 1} f(c)  G(x) = F(x)$">|; 

$key = q/displaystylepartial^{N+1}f(c)frac{(x-a)^{N+1}}{N+1}=F(x)=int_a^x(x-t)^Npartial^{N+1}f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.74ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle \partial^{N + 1} f(c)  \frac{ (x - a)^{N + 1} }{N + 1} = F(x) = \int_a^x (x - t)^N  \partial^{N + 1} f(t)  dt$">|; 

$key = q/displaystylepartialf(x)-partialf(a)=int_a^xpartial^2f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \partial f(x) - \partial f(a) = \int_a^x \partial^2 f(t)  dt$">|; 

$key = q/displaystylepartialvarphi(0)=partialf(u)cdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle \partial \varphi(0) = \partial f(u) \cdot h$">|; 

$key = q/displaystylepartialvarphi(s)=partialf(u+scdoth)cdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle \partial \varphi(s) = \partial f(u + s \cdot h) \cdot h$">|; 

$key = q/displaystylepartialvarphi(s)=sum_ipartial_if(u+scdoth)cdoth_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle \partial \varphi(s) = \sum_i \partial_i f(u + s \cdot h) \cdot h_i$">|; 

$key = q/displaystyler(t)=p(t+a)=sum_{i=0}^ngamma_icdot(t+a)^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle r(t) = p(t + a) = \sum_{i = 0}^n \gamma_i \cdot (t + a)^i$">|; 

$key = q/displaystyler(t)=sum_{i=0}^nunsur{i!}cdotpartial^ip(a)cdott^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle r(t) = \sum_{i = 0}^n \unsur{i !} \cdot \partial^i p(a) \cdot t^i$">|; 

$key = q/displaystyler(t)=sum_{i=0}^nunsur{i!}cdotpartial^ir(0)cdott^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle r(t) = \sum_{i = 0}^n \unsur{i !} \cdot \partial^i r(0) \cdot t^i$">|; 

$key = q/displaystyler=unsur{k^m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img199.svg"
 ALT="$\displaystyle r = \unsur{k^m}$">|; 

$key = q/displaystylev(h)approxV+Ccdoth^m+O(h^{m+1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img190.svg"
 ALT="$\displaystyle v(h) \approx V + C \cdot h^m + O(h^{m+1})$">|; 

$key = q/displaystylev:tmapstounsur{2}(x-t)^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle v : t \mapsto \unsur{2} (x - t)^2$">|; 

$key = q/displaystylevarphi(0)=f(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle \varphi(0) = f(u)$">|; 

$key = q/displaystylevarphi(1)=f(u+h)=f(v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\displaystyle \varphi(1) = f(u + h) = f(v)$">|; 

$key = q/displaystylevarphi(s)=(fcirclambda)(s)=f(u+scdot(v-u));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle \varphi(s) = (f \circ \lambda)(s) = f(u + s \cdot (v - u))$">|; 

$key = q/displaystylevarphi(s)=f(u)+scdotpartialf(u)cdoth+E_u^1(s,h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle \varphi(s) = f(u) + s \cdot \partial f(u) \cdot h + E_u^1(s,h)$">|; 

$key = q/displaystylevarphi(s)=f(u)+spartialf(u)cdoth+frac{s^2}{2}h^dualcdotpartial^2f(u)cdoth+E_u^2(s,h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle \varphi(s) = f(u) + s  \partial f(u) \cdot h + \frac{s^2}{2}  h^\dual \cdot \partial^2 f(u) \cdot h + E_u^2(s,h)$">|; 

$key = q/displaystylexpartialf(x)=xpartialf(a)+int_a^xxpartial^2f(t)dt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle x  \partial f(x) = x  \partial f(a) + \int_a^x x  \partial^2 f(t)  dt$">|; 

$key = q/f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$f(x)$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img58.svg"
 ALT="$f$">|; 

$key = q/fincontinue^1(Omega,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img126.svg"
 ALT="$f \in \continue^1(\Omega,\setR)$">|; 

$key = q/fincontinue^2([alpha,beta],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$f \in \continue^2([\alpha,\beta],\setR)$">|; 

$key = q/fincontinue^3(Omega,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img152.svg"
 ALT="$f \in \continue^3(\Omega,\setR)$">|; 

$key = q/fincontinue^3([alpha,beta],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$f \in \continue^3([\alpha,\beta],\setR)$">|; 

$key = q/fincontinue^N([alpha,beta],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$f \in \continue^N([\alpha,\beta],\setR)$">|; 

$key = q/fincontinue^infty([alpha,beta],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img118.svg"
 ALT="$f \in \continue^\infty([\alpha,\beta],\setR)$">|; 

$key = q/fincontinue^{N+1}(Omega,setR^n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img186.svg"
 ALT="$f \in \continue^{N + 1}(\Omega, \setR^n)$">|; 

$key = q/fincontinue^{N+1}([alpha,beta],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$f \in \continue^{N + 1}([\alpha,\beta],\setR)$">|; 

$key = q/grando{1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img183.svg"
 ALT="$\grando{1}$">|; 

$key = q/grando{b(h)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img170.svg"
 ALT="$\grando{b(h)}$">|; 

$key = q/grando{h^k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img177.svg"
 ALT="$\grando{h^k}$">|; 

$key = q/grando{h^{N+1}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img188.svg"
 ALT="$\grando{h^{N+1}}$">|; 

$key = q/hinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img164.svg"
 ALT="$h \in \Omega$">|; 

$key = q/hin[0,R]subseteqsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img192.svg"
 ALT="$h \in [0,R] \subseteq \setR$">|; 

$key = q/ige0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img18.svg"
 ALT="$i \ge 0$">|; 

$key = q/k=2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img206.svg"
 ALT="$k = 2$">|; 

$key = q/kinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img174.svg"
 ALT="$k \in \setN$">|; 

$key = q/kinsetZ[2,N];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$k \in \setZ[2,N]$">|; 

$key = q/lambda:[0,1]mapstosetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\lambda : [0,1] \mapsto \setR^n$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img193.svg"
 ALT="$m$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$n$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img121.svg"
 ALT="$n \in \setN$">|; 

$key = q/norme{.}_infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\norme{.}_\infty$">|; 

$key = q/p:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$p : \setR \mapsto \setR$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$p$">|; 

$key = q/partial^{N+1}f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\partial^{N+1} f$">|; 

$key = q/partialf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\partial f$">|; 

$key = q/petito{1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img184.svg"
 ALT="$\petito{1}$">|; 

$key = q/petito{b(h)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img167.svg"
 ALT="$\petito{b(h)}$">|; 

$key = q/petito{h^k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img175.svg"
 ALT="$\petito{h^k}$">|; 

$key = q/petito{h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img185.svg"
 ALT="$\petito{h}$">|; 

$key = q/r(0)=p(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$r(0) = p(a)$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$r$">|; 

$key = q/sigmainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\sigma \in \setR$">|; 

$key = q/sin[0,1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img132.svg"
 ALT="$s \in [0,1]$">|; 

$key = q/t=x-a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.81ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img22.svg"
 ALT="$t = x - a$">|; 

$key = q/tin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img90.svg"
 ALT="$t \in [\alpha,\beta]$">|; 

$key = q/tinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img15.svg"
 ALT="$t \in \setR$">|; 

$key = q/u,vinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img127.svg"
 ALT="$u,v \in \setR^n$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img47.svg"
 ALT="$u$">|; 

$key = q/u=partial^2f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img63.svg"
 ALT="$u = \partial^2 f$">|; 

$key = q/u=partialf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img43.svg"
 ALT="$u = \partial f$">|; 

$key = q/v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img48.svg"
 ALT="$v$">|; 

$key = q/v=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img44.svg"
 ALT="$v = \identite$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\varphi$">|; 

$key = q/varphi=fcirclambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\varphi = f \circ \lambda$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img53.svg"
 ALT="$x$">|; 

$key = q/x=t+a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img21.svg"
 ALT="$x = t + a$">|; 

$key = q/x^{k-k}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$x^{k - k} = 1$">|; 

$key = q/xin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$x \in [\alpha,\beta]$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img4.svg"
 ALT="$x \in \setR$">|; 

$key = q/xstrictinferieura;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img82.svg"
 ALT="$x \strictinferieur a$">|; 

$key = q/zin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$z \in [\alpha,\beta]$">|; 

$key = q/{Eqts}V_1=v(h)=V+Ccdoth^m+O(h^{m+1})V_2=vleft(hslashkright)=V+Ccdotleft(frac{h}{k}right)^m+O(h^{m+1}){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.37ex; vertical-align: -4.12ex; " SRC="|."$dir".q|img196.svg"
 ALT="\begin{Eqts}
V_1 = v(h) = V + C \cdot h^m + O(h^{m+1}) \\\\
V_2 = v\left(h/k\right) = V + C \cdot \left(\frac{h}{k}\right)^m
+ O(h^{m+1})
\end{Eqts}">|; 

$key = q/{eqnarraystar}F(t)&=&sum_{k=0}^Nunsur{k!}cdotpartial^kf(t)cdot(x-t)^k&=&f(t)+partialf(t)(x-t)+partial^2f(t)frac{(x-t)^2}{2}+...{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 17.92ex; " SRC="|."$dir".q|img89.svg"
 ALT="\begin{eqnarray*}
F(t) &amp;=&amp; \sum_{k = 0}^N \unsur{k !} \cdot \partial^k f(t) \cdo...
...ial f(t)  (x - t) + \partial^2 f(t)  \frac{(x - t)^2}{2} + ...
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}F(z)&=&int_a^z(x-t)^Npartial^{N+1}f(t)dtG(z)&=&int_a^z(x-t)^Ndt{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.37ex; " SRC="|."$dir".q|img77.svg"
 ALT="\begin{eqnarray*}
F(z) &amp;=&amp; \int_a^z (x - t)^N  \partial^{N + 1} f(t)  dt \\\\
G(z) &amp;=&amp; \int_a^z (x - t)^N  dt
\end{eqnarray*}">|; 

1;

