# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \differentielle{ \scalaire{f}{g} }{a}(h) = \sum_{j = 1}^n \varpi_...
... \left[ f_i(a) \cdot \partial_j g_i(a) + \partial_j f_i(a) \cdot g_i(a) \right]$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$a$">|; 

$key = q/ainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img75.svg"
 ALT="$a \in \setR$">|; 

$key = q/b=f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$b = f(a)$">|; 

$key = q/cinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img2.svg"
 ALT="$c \in F$">|; 

$key = q/da;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$da$">|; 

$key = q/db=f(a+da)-f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$db = f(a + da) - f(a)$">|; 

$key = q/displaystyle(x_1,...,x_n)mapsto(y_1,...,y_m)mapsto(z_1,...,z_p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle (x_1,...,x_n) \mapsto (y_1,...,y_m) \mapsto (z_1,...,z_p)$">|; 

$key = q/displaystyleF={f_ninsetR^setR:ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.79ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle F = \{ f_n \in \setR^\setR : n \in \setN \}$">|; 

$key = q/displaystyleOD{f}{x}=-frac{f}{g}cdotOD{g}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.40ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle \OD{f}{x} = - \frac{f}{g} \cdot \OD{g}{x}$">|; 

$key = q/displaystyleOD{f}{x}cdotg+fcdotOD{g}{x}=OD{1}{x}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle \OD{f}{x} \cdot g + f \cdot \OD{g}{x} = \OD{1}{x} = 0$">|; 

$key = q/displaystyleOD{y}{x}=left(OD{x}{y}right)^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.13ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \OD{y}{x} = \left(\OD{x}{y}\right)^{-1}$">|; 

$key = q/displaystyleOD{z}{t}=sum_kderiveepartielle{z}{x_k}cdotOD{x_k}{t};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.33ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \OD{z}{t} = \sum_k \deriveepartielle{z}{x_k} \cdot \OD{x_k}{t}$">|; 

$key = q/displaystyleOD{z}{x}=OD{z}{y}cdotOD{y}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.40ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \OD{z}{x} = \OD{z}{y} \cdot \OD{y}{x}$">|; 

$key = q/displaystyleOD{}{x}(fcdotc)(a)=f(a)cdot0+OD{f}{x}(a)cdotc=ccdotOD{f}{x}(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle \OD{}{x}(f \cdot c)(a) = f(a) \cdot 0 + \OD{f}{x}(a) \cdot c = c \cdot \OD{f}{x}(a)$">|; 

$key = q/displaystyleOD{}{x}(fcdotg)(a)=f(a)cdotOD{g}{x}(a)+OD{f}{x}(a)cdotg(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \OD{}{x}(f \cdot g)(a) = f(a) \cdot \OD{g}{x}(a) + \OD{f}{x}(a) \cdot g(a)$">|; 

$key = q/displaystyleOD{}{x}left(frac{f}{g}right)(x)=OD{}{x}left(fcdotunsur{g}right)(x)=OD{f}{x}(x)cdotg(x)-frac{f(x)}{g(x)^2}cdotOD{g}{x}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \OD{}{x}\left( \frac{f}{g} \right)(x) = \OD{}{x}\left( f \cdot \u...
...} \right)(x) = \OD{f}{x}(x) \cdot g(x) - \frac{f(x)}{g(x)^2} \cdot \OD{g}{x}(x)$">|; 

$key = q/displaystyleOD{}{x}left(frac{f}{g}right)(x)=frac{OD{f}{x}(x)cdotg(x)-f(x)cdotOD{g}{x}}{g(x)^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.18ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle \OD{}{x}\left( \frac{f}{g} \right)(x) = \frac{\OD{f}{x}(x) \cdot g(x) - f(x) \cdot \OD{g}{x}}{g(x)^2}$">|; 

$key = q/displaystyleOD{}{x}left(unsur{g}right)(x)=-unsur{g(x)^2}cdotOD{g}{x}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \OD{}{x}\left(\unsur{g}\right)(x) = - \unsur{g(x)^2} \cdot \OD{g}{x}(x)$">|; 

$key = q/displaystyled(fcdotg)=dfcdotg+fcdotdg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle d(f \cdot g) = df \cdot g + f \cdot dg$">|; 

$key = q/displaystylederiveepartielle{y}{x}=left(deriveepartielle{x}{y}right)^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.13ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \deriveepartielle{y}{x} = \left(\deriveepartielle{x}{y}\right)^{-1}$">|; 

$key = q/displaystyledifferentielle{scalaire{f}{g}}{a}(h)=scalaire{f(a)}{partialg(a)cdoth}+scalaire{partialf(a)cdoth}{g(a)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \differentielle{ \scalaire{f}{g} }{a}(h) = \scalaire{f(a)}{\partial g(a) \cdot h} + \scalaire{\partial f(a) \cdot h}{g(a)}$">|; 

$key = q/displaystylef(x)=c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle f(x) = c$">|; 

$key = q/displaystylefcdotg=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle f \cdot g = 1$">|; 

$key = q/displaystylelim_{hto0}frac{norme{E_f(h)+E_g(h)}}{norme{h}}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \lim_{h \to 0} \frac{\norme{E_f(h) + E_g(h)}}{\norme{h}} = 0$">|; 

$key = q/displaystylelim_{hto0}frac{norme{E_{fcdotg}(h)}}{norme{h}}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \lim_{h \to 0} \frac{\norme{E_{f \cdot g}(h)}}{\norme{h}} = 0$">|; 

$key = q/displaystylelim_{hto0}frac{partialg(f(a))cdotE_f(da)+E_g(df(a))}{norme{h}}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \lim_{h \to 0} \frac{\partial g(f(a)) \cdot E_f(da) + E_g(df(a))}{\norme{h}} = 0$">|; 

$key = q/displaystylelim_{ntoinfty}f_n(x)=f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \lim_{n \to \infty} f_n(x) = f(x)$">|; 

$key = q/displaystylepartial(f+g)(a)=partialf(a)+partialg(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \partial (f + g)(a) = \partial f(a) + \partial g(a)$">|; 

$key = q/displaystylepartial(f-g)(a)=partialf(a)-partialg(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \partial (f - g)(a) = \partial f(a) - \partial g(a)$">|; 

$key = q/displaystylepartial(gcircf)(a)=partialg(f(a))cdotpartialf(a)=(partialgcircf)(a)cdotpartialf(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \partial (g \circ f)(a) = \partial g(f(a)) \cdot \partial f(a) = (\partial g \circ f)(a) \cdot \partial f(a)$">|; 

$key = q/displaystylepartial_if_i(a)=lim_{lambdato0}frac{x_i+lambda-x_i}{lambda}=lim_{lambdato0}frac{lambda}{lambda}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.06ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \partial_i f_i(a) = \lim_{\lambda \to 0} \frac{x_i + \lambda - x_i}{\lambda} = \lim_{\lambda \to 0} \frac{\lambda}{\lambda} = 1$">|; 

$key = q/displaystylepartial_jf(a)=lim_{lambdato0}frac{c-c}{lambda}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.80ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \partial_j f(a) = \lim_{\lambda \to 0} \frac{c - c}{\lambda} = 0$">|; 

$key = q/displaystylepartial_jf_i(a)=lim_{lambdato0}frac{x_i-x_i}{lambda}=lim_{lambdato0}frac{0}{lambda}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.94ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \partial_j f_i(a) = \lim_{\lambda \to 0} \frac{x_i - x_i}{\lambda} = \lim_{\lambda \to 0} \frac{0}{\lambda} = 0$">|; 

$key = q/displaystylepartialf(a)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \partial f(a) = 0$">|; 

$key = q/displaystylepartialf(a)=partialidentite(a)=(indicatrice_{ij})_{i,j}=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \partial f(a) = \partial \identite(a) = ( \indicatrice_{ij} )_{i,j} = I$">|; 

$key = q/displaystylepartialf^{-1}(b)=left[partialf(a)right]^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \partial f^{-1}(b) = \left[ \partial f(a) \right]^{-1}$">|; 

$key = q/displaystylepartialf^{-1}(b)cdotpartialf(a)=partialidentite(a)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \partial f^{-1}(b) \cdot \partial f(a) = \partial \identite(a) = I$">|; 

$key = q/displaystylepartialg(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \partial g(x) = 0$">|; 

$key = q/displaystylepartialscalaire{f}{g}(a)=left[partialg(a)right]^Tcdotf(a)+left[partialf(a)right]^Tcdotg(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.95ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \partial \scalaire{f}{g}(a) = \left[ \partial g(a) \right]^T \cdot f(a) + \left[ \partial f(a) \right]^T \cdot g(a)$">|; 

$key = q/f(x)=1slashg(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$f(x) = 1/g(x)$">|; 

$key = q/f+g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img45.svg"
 ALT="$f + g$">|; 

$key = q/f,g:corpsmapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img62.svg"
 ALT="$f,g : \corps \mapsto \corps$">|; 

$key = q/f,g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img41.svg"
 ALT="$f,g$">|; 

$key = q/f:EmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$f : E \mapsto F$">|; 

$key = q/f:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img73.svg"
 ALT="$f : \setR \mapsto \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f$">|; 

$key = q/f=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$f = \identite$">|; 

$key = q/g(x)=c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$g(x) = c$">|; 

$key = q/g:FmapstoG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$g : F \mapsto G$">|; 

$key = q/g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$g$">|; 

$key = q/g=f^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img34.svg"
 ALT="$g = f^{-1}$">|; 

$key = q/gcircf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img26.svg"
 ALT="$g \circ f$">|; 

$key = q/gcircf=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img35.svg"
 ALT="$g \circ f = \identite$">|; 

$key = q/gne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img65.svg"
 ALT="$g \ne 0$">|; 

$key = q/i=j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$i = j$">|; 

$key = q/inej;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$i \ne j$">|; 

$key = q/m=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$m = n$">|; 

$key = q/m=n=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img55.svg"
 ALT="$m = n = 1$">|; 

$key = q/partialg(f(a));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\partial g(f(a))$">|; 

$key = q/x,y,zincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img32.svg"
 ALT="$x,y,z \in \corps$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img58.svg"
 ALT="$x$">|; 

$key = q/xinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img4.svg"
 ALT="$x \in \Omega$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img72.svg"
 ALT="$x \in \setR$">|; 

$key = q/y:corpsmapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img39.svg"
 ALT="$y : \corps \mapsto \corps$">|; 

$key = q/z:tmapsto(x_1(t),x_2(t),...,x_n(t));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$z : t \mapsto (x_1(t),x_2(t),...,x_n(t))$">|; 

$key = q/{Eqts}df(a)=f(a+da)-f(a)=partialf(a)cdotda+E_f(da)dg(b)=g(b+db)-g(b)=partialg(b)cdotdb+E_g(db){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img19.svg"
 ALT="\begin{Eqts}
df(a) = f(a + da) - f(a) = \partial f(a) \cdot da + E_f(da) \\\\
dg(b) = g(b + db) - g(b) = \partial g(b) \cdot db + E_g(db)
\end{Eqts}">|; 

$key = q/{Eqts}f(a+h)-f(a)=partialf(a)cdoth+E_f(h)g(a+h)-g(a)=partialg(a)cdoth+E_g(h){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img42.svg"
 ALT="\begin{Eqts}
f(a + h) - f(a) = \partial f(a) \cdot h + E_f(h) \\\\
g(a + h) - g(a) = \partial g(a) \cdot h + E_g(h)
\end{Eqts}">|; 

$key = q/{Eqts}f(a+h)=f(a)+partialf(a)cdoth+E_f(h)g(a+h)=g(a)+partialg(a)cdoth+E_g(h){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img48.svg"
 ALT="\begin{Eqts}
f(a + h) = f(a) + \partial f(a) \cdot h + E_f(h) \\\\
g(a + h) = g(a) + \partial g(a) \cdot h + E_g(h) \\\\
\end{Eqts}">|; 

$key = q/{Eqts}left[f(a+h)+g(a+h)right]-left[f(a)+g(a)right]=[partialf(a)+partialg(a)]cdothqquadqquad+E_f(h)+E_g(h){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img43.svg"
 ALT="\begin{Eqts}
\left[ f(a + h) + g(a + h) \right] - \left[ f(a) + g(a) \right]
= [...
...al f(a) + \partial g(a) ] \cdot h \\\\
\qquad \qquad + E_f(h) + E_g(h)
\end{Eqts}">|; 

$key = q/{Eqts}scalaire{f(a+h)}{g(a+h)}=scalaire{f(a)}{g(a)}+scalaire{f(a)}{partialg(a)cdoth}+scalaire{partialf(a)cdoth}{g(a)}qquadqquadqquad+E_{fcdotg}(h){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img49.svg"
 ALT="\begin{Eqts}
\scalaire{f(a + h)}{g(a + h)} = \scalaire{f(a)}{g(a)} + \scalaire{f...
...rtial f(a) \cdot h}{g(a)} \\\\
\qquad \qquad \qquad + E_{f \cdot g}(h)
\end{Eqts}">|; 

$key = q/{eqnarraystar}dg(b)&=&g(f(a+da))-g(f(a))&=&(gcircf)(a+da)-(gcircf)(a)&=&d(gcircf)(a){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img21.svg"
 ALT="\begin{eqnarray*}
dg(b) &amp;=&amp; g(f(a + da)) - g(f(a)) \\\\
&amp;=&amp; (g \circ f)(a + da) - (g \circ f)(a) \\\\
&amp;=&amp; d(g \circ f)(a)
\end{eqnarray*}">|; 

1;

