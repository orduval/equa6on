# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.94ex; vertical-align: -2.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \scalaire{y}{y} - \frac{ \scalaire{x}{y} \cdot \scalaire{y}{x} }{...
...{y}{x} \cdot \scalaire{x}{y} }{ \scalaire{x}{x}^2 } \cdot \scalaire{x}{x} \ge 0$">|; 

$key = q/(a,b)insetR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$(a,b) \in \setR^2$">|; 

$key = q/(e_1,...,e_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$(e_1,...,e_n)$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$E$">|; 

$key = q/Re(imgz)=-Im(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\Re(\img z) = - \Im(z)$">|; 

$key = q/abs{Re(scalaire{x}{y})}leabs{scalaire{x}{y}}lenorme{x}cdotnorme{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\abs{\Re(\scalaire{x}{y})} \le \abs{\scalaire{x}{y}} \le \norme{x} \cdot \norme{y}$">|; 

$key = q/abs{x_i}^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\abs{x_i}^k$">|; 

$key = q/alpha,betainsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\alpha,\beta \in \setC$">|; 

$key = q/alphainsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\alpha \in \setC$">|; 

$key = q/beta=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\beta = 1$">|; 

$key = q/corps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\corps^n$">|; 

$key = q/corpsin{setR,setC};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\corps \in \{ \setR , \setC \}$">|; 

$key = q/displaystyleabs{scalaire{x}{y}}^2lescalaire{x}{x}cdotscalaire{y}{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \abs{\scalaire{x}{y}}^2 \le \scalaire{x}{x} \cdot \scalaire{y}{y}$">|; 

$key = q/displaystyleabs{scalaire{x}{y}}lesqrt{scalaire{x}{x}cdotscalaire{y}{y}}=norme{x}cdotnorme{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \abs{\scalaire{x}{y}} \le \sqrt{\scalaire{x}{x} \cdot \scalaire{y}{y}} = \norme{x} \cdot \norme{y}$">|; 

$key = q/displaystyleabs{z}=abs{a+imgb}=sqrt{a^2+b^2}=norme{(a,b)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.98ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle \abs{z} = \abs{a + \img b} = \sqrt{a^2 + b^2} = \norme{(a,b)}$">|; 

$key = q/displaystyledistance(x,y)=norme{x-y}=sqrt{scalaire{x-y}{x-y}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \distance(x,y) = \norme{x - y} = \sqrt{\scalaire{x - y}{x - y}}$">|; 

$key = q/displaystylelambda=frac{scalaire{x}{y}}{scalaire{x}{x}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \lambda = \frac{ \scalaire{x}{y} }{ \scalaire{x}{x} }$">|; 

$key = q/displaystylelim_{kmapsto+infty}norme{x}_k=max_{i=1}^nabs{x_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.40ex; vertical-align: -1.95ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \lim_{k \mapsto +\infty} \norme{x}_k = \max_{i = 1}^n \abs{x_i}$">|; 

$key = q/displaystylenorme{alphacdotx}=sqrt{abs{alpha}^2cdotscalaire{x}{x}}=abs{alpha}cdotsqrt{scalaire{x}{x}}=abs{alpha}cdotnorme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.45ex; vertical-align: -1.20ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \norme{\alpha \cdot x} = \sqrt{ \abs{\alpha}^2 \cdot \scalaire{x}{x} } = \abs{\alpha} \cdot \sqrt{ \scalaire{x}{x} } = \abs{\alpha} \cdot \norme{x}$">|; 

$key = q/displaystylenorme{u}=sqrt{sum_iabs{u_i}^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.23ex; vertical-align: -3.60ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \norme{u} = \sqrt{ \sum_i \abs{u_i}^2 }$">|; 

$key = q/displaystylenorme{u}=sqrt{sum_{i,j}conjaccent{u}_icdotscalaire{e_i}{e_j}cdotu_j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.23ex; vertical-align: -3.76ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \norme{u} = \sqrt{ \sum_{i,j} \conjaccent{u}_i \cdot \scalaire{e_i}{e_j} \cdot u_j }$">|; 

$key = q/displaystylenorme{x+imgy}^2-norme{x-imgy}^2=-4Im(scalaire{x}{y});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \norme{x + \img y}^2 - \norme{x - \img y}^2 = - 4 \Im(\scalaire{x}{y})$">|; 

$key = q/displaystylenorme{x+y}^2+norme{x-y}^2=2(scalaire{x}{x}+scalaire{y}{y})=2(norme{x}^2+norme{y}^2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \norme{x + y}^2 + \norme{x - y}^2 = 2(\scalaire{x}{x} + \scalaire{y}{y}) = 2 (\norme{x}^2 + \norme{y}^2)$">|; 

$key = q/displaystylenorme{x+y}^2-norme{x-y}^2=4Re(scalaire{x}{y});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \norme{x + y}^2 - \norme{x - y}^2 = 4 \Re(\scalaire{x}{y})$">|; 

$key = q/displaystylenorme{x+y}^2=norme{x}^2+norme{y}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \norme{x + y}^2 = \norme{x}^2 + \norme{y}^2$">|; 

$key = q/displaystylenorme{x+y}^2lenorme{x}^2+2norme{x}cdotnorme{y}+norme{y}^2=(norme{x}+norme{y})^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \norme{x + y}^2 \le \norme{x}^2 + 2 \norme{x} \cdot \norme{y} + \norme{y}^2 = (\norme{x} + \norme{y})^2$">|; 

$key = q/displaystylenorme{x+y}lenorme{x}+norme{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \norme{x + y} \le \norme{x} + \norme{y}$">|; 

$key = q/displaystylenorme{x}=0Rightarrowscalaire{x}{x}=0Rightarrowx=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \norme{x} = 0 \Rightarrow \scalaire{x}{x} = 0 \Rightarrow x = 0$">|; 

$key = q/displaystylenorme{x}=sqrt{scalaire{x}{x}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \norme{x} = \sqrt{ \scalaire{x}{x} }$">|; 

$key = q/displaystylenorme{x}=sqrt{scalaire{x}{x}}=sqrt{conjaccent{x}^Tcdotx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.44ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \norme{x} = \sqrt{ \scalaire{x}{x} } = \sqrt{\conjaccent{x}^T \cdot x}$">|; 

$key = q/displaystylenorme{x}=sqrt{scalaire{x}{x}}=sqrt{sum_{i=1}^nabs{x_i}^2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.62ex; vertical-align: -3.62ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \norme{x} = \sqrt{ \scalaire{x}{x} } = \sqrt{\sum_{i=1}^n \abs{x_i}^2}$">|; 

$key = q/displaystylenorme{x}_infty=max_{i=1}^nabs{x_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.19ex; vertical-align: -1.74ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \norme{x}_\infty = \max_{i = 1}^n \abs{x_i}$">|; 

$key = q/displaystylenorme{x}_k=left(sum_iabs{x_i}^kright)^{1slashk};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.70ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \norme{x}_k = \left( \sum_i \abs{x_i}^k \right)^{1/k}$">|; 

$key = q/displaystylescalaire{x}{y}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \scalaire{x}{y} = 0$">|; 

$key = q/displaystylescalaire{y}{y}-frac{scalaire{x}{y}cdotscalaire{y}{x}}{scalaire{x}{x}}=scalaire{y}{y}-frac{abs{scalaire{x}{y}}^2}{scalaire{x}{x}}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.05ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \scalaire{y}{y} - \frac{ \scalaire{x}{y} \cdot \scalaire{y}{x} }{...
...} = \scalaire{y}{y} - \frac{ \abs{\scalaire{x}{y}}^2 }{ \scalaire{x}{x} } \ge 0$">|; 

$key = q/displaystyleu=sum_iu_icdote_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle u = \sum_i u_i \cdot e_i$">|; 

$key = q/i=j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img50.svg"
 ALT="$i = j$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img55.svg"
 ALT="$k$">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\lambda$">|; 

$key = q/lambdainsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\lambda \in \setC$">|; 

$key = q/mathbb{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\mathbb {C}$">|; 

$key = q/mathbb{K}^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\mathbb {K}^n$">|; 

$key = q/norme{.}:Emapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\norme{.} : E \mapsto \corps$">|; 

$key = q/norme{.};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\norme{.}$">|; 

$key = q/norme{.}=norme{.}_2=sqrt{scalaire{}{}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.81ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\norme{.} = \norme{.}_2 = \sqrt{\scalaire{}{}}$">|; 

$key = q/norme{.}=sqrt{scalaire{}{}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.81ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\norme{.} = \sqrt{\scalaire{}{}}$">|; 

$key = q/norme{.}_infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\norme{.}_\infty$">|; 

$key = q/norme{x}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\norme{x} \ge 0$">|; 

$key = q/scalaire{x}{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\scalaire{x}{x}$">|; 

$key = q/scalaire{y}{x}=conjuguescalaire{x}{y}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\scalaire{y}{x} = \conjugue \scalaire{x}{y} = 0$">|; 

$key = q/scalaire{}{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\scalaire{}{}$">|; 

$key = q/setC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\setC$">|; 

$key = q/u_i,v_iincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img48.svg"
 ALT="$u_i,v_i \in \corps$">|; 

$key = q/uinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img46.svg"
 ALT="$u \in E$">|; 

$key = q/x,yinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$x,y \in E$">|; 

$key = q/x=[x_1...x_n]^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$x = [x_1  ...  x_n]^T$">|; 

$key = q/xinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$x \in E$">|; 

$key = q/z=a+imgb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img64.svg"
 ALT="$z = a + \img b$">|; 

$key = q/{Eqts}norme{x+y}^2=scalaire{x}{x}+2Re(scalaire{x}{y})+scalaire{y}{y}norme{x-y}^2=scalaire{x}{x}-2Re(scalaire{x}{y})+scalaire{y}{y}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.88ex; vertical-align: -2.87ex; " SRC="|."$dir".q|img39.svg"
 ALT="\begin{Eqts}
\norme{x + y}^2 = \scalaire{x}{x} + 2 \Re(\scalaire{x}{y}) + \scala...
...x - y}^2 = \scalaire{x}{x} - 2 \Re(\scalaire{x}{y}) + \scalaire{y}{y}
\end{Eqts}">|; 

$key = q/{Eqts}norme{x+y}^2=scalaire{x}{x}+scalaire{x}{y}+scalaire{y}{x}+scalaire{y}{y}norme{x-y}^2=scalaire{x}{x}-scalaire{x}{y}-scalaire{y}{x}+scalaire{y}{y}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.88ex; vertical-align: -2.87ex; " SRC="|."$dir".q|img18.svg"
 ALT="\begin{Eqts}
\norme{x + y}^2 = \scalaire{x}{x} + \scalaire{x}{y} + \scalaire{y}{...
...\scalaire{x}{x} - \scalaire{x}{y} - \scalaire{y}{x} + \scalaire{y}{y}
\end{Eqts}">|; 

$key = q/{eqnarraystar}scalaire{x+y}{x+y}&=&scalaire{x}{x}+scalaire{x}{y}+scalaire{y}{x}+scalaire{y}{y}&=&scalaire{x}{x}+scalaire{y}{y}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img15.svg"
 ALT="\begin{eqnarray*}
\scalaire{x + y}{x + y} &amp;=&amp; \scalaire{x}{x} + \scalaire{x}{y} ...
...}{x} + \scalaire{y}{y} \\\\
&amp;=&amp; \scalaire{x}{x} + \scalaire{y}{y}
\end{eqnarray*}">|; 

1;

