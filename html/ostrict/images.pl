# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\Omega$">|; 

$key = q/displaystylexstrictsuperieury;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle x \strictsuperieur y$">|; 

$key = q/displaystyleystrictinferieurx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle y \strictinferieur x$">|; 

$key = q/le;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\le$">|; 

$key = q/strictinferieur;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\strictinferieur$">|; 

$key = q/x,y,zinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$x,y,z \in \Omega$">|; 

$key = q/x,y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img21.svg"
 ALT="$x,y$">|; 

$key = q/x,yinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$x, y \in \Omega$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$x$">|; 

$key = q/x=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$x = y$">|; 

$key = q/xley;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img19.svg"
 ALT="$x \le y$">|; 

$key = q/xney;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$x \ne y$">|; 

$key = q/xstrictinferieurx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img5.svg"
 ALT="$x \strictinferieur x$">|; 

$key = q/xstrictinferieury;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$x \strictinferieur y$">|; 

$key = q/xstrictinferieurystrictinferieurz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img13.svg"
 ALT="$x \strictinferieur y \strictinferieur z$">|; 

$key = q/xstrictsuperieury;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img23.svg"
 ALT="$x \strictsuperieur y$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img16.svg"
 ALT="$y$">|; 

$key = q/ylex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$y \le x$">|; 

$key = q/ynez;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$y \ne z$">|; 

$key = q/ystrictinferieurx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$y \strictinferieur x$">|; 

$key = q/ystrictinferieurz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$y \strictinferieur z$">|; 

$key = q/zstrictinferieurx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img10.svg"
 ALT="$z \strictinferieur x$">|; 

$key = q/{Eqts}xstrictinferieury,quadystrictinferieurzquadRightarrowquadxstrictinferieurz{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img12.svg"
 ALT="\begin{Eqts}
x \strictinferieur y, \quad y \strictinferieur z \quad \Rightarrow \quad x \strictinferieur z
\end{Eqts}">|; 

1;

