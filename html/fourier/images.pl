# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.16ex; vertical-align: -6.53ex; " SRC="|."$dir".q|img96.svg"
 ALT="\begin{Eqts}
\mathcal{F}\left[d_a(u)\right](y) =
\unsur{a}\int\limits_{-\infty}^...
... \\\\
\mathcal{F}\left[d_a(u)\right](y) = \unsur{a}\mathcal{F}(u)(y/a)
\end{Eqts}">|; 

$key = q/(-infty,+infty)=setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$(-\infty,+\infty) = \setR$">|; 

$key = q/(n+1)x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$(n+1) x$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img62.svg"
 ALT="$0$">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img104.svg"
 ALT="$1$">|; 

$key = q/2pi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$2\pi$">|; 

$key = q/Deltax;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\Delta x$">|; 

$key = q/Deltay;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\Delta y$">|; 

$key = q/G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img98.svg"
 ALT="$G$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img44.svg"
 ALT="$N$">|; 

$key = q/N=n-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img30.svg"
 ALT="$N = n - 1$">|; 

$key = q/Nslash2insetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$N/2 \in\setN$">|; 

$key = q/U_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img50.svg"
 ALT="$U_n$">|; 

$key = q/[-sqrt{N}slash2,sqrt{N}slash2];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.92ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$[-\sqrt{N}/2,\sqrt{N}/2]$">|; 

$key = q/a>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img92.svg"
 ALT="$a &gt; 0$">|; 

$key = q/d_a(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img93.svg"
 ALT="$d_a(u)$">|; 

$key = q/d_a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img90.svg"
 ALT="$d_a$">|; 

$key = q/displaystyleDeltax=Deltay=frac{1}{sqrt{N}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.40ex; vertical-align: -2.26ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \Delta x = \Delta y = \frac{1}{\sqrt{N}}$">|; 

$key = q/displaystyleDeltaxDeltay=frac{1}{N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \Delta x \Delta y = \frac{1}{N}$">|; 

$key = q/displaystyleG(x)=exp(-pix^2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle G(x) = \exp(-\pi x^2)$">|; 

$key = q/displaystyleU(y_n)=sum_{k=-Nslash2}^{Nslash2-1}u(x_k)exp(-2piimgx_ky_n)Deltax;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.09ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle U(y_n) = \sum_{k = -N/2}^{N/2-1} u(x_k) \exp(-2 \pi \img x_k y_n) \Delta x$">|; 

$key = q/displaystyleU_m=unsur{N}sum_{k=0}^{N-1}u_ke(k,-m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.33ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle U_m = \unsur{N} \sum_{k=0}^{N-1} u_k e(k,-m)$">|; 

$key = q/displaystyled_a(u)(x)=u(ax);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle d_a(u)(x) = u(a x)$">|; 

$key = q/displaystylee(k,l)=expleft(frac{2piimgkl}{N}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle e(k,l) = \exp \left( \frac{2 \pi \img k l}{N} \right)$">|; 

$key = q/displaystylee_k:xmapstoexpleft(frac{2piimgkx}{T}right);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle e_k : x \mapsto \exp\left(\frac{2\pi\img k x}{T}\right)$">|; 

$key = q/displaystyleforme{mathcal{F}(u)}{v}=forme{u}{mathcal{F}(v)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle \forme{\mathcal{F}(u)}{v} = \forme{u}{\mathcal{F}(v)}$">|; 

$key = q/displaystylehat{u}_k=scalaire{e_k}{u}=intlimits_{-Tslash2}^{Tslash2}u(x)expleft(-frac{2piimgkx}{T}right)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.53ex; vertical-align: -4.27ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \hat{u}_k = \scalaire{e_k}{u} = \int\limits_{-T/2}^{T/2} u(x) \exp\left(-\frac{2\pi\img k x}{T}\right)dx$">|; 

$key = q/displaystyleint_0^{2pi}exp(0)dx=int_0^{2pi}1dx=2pi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \int_0^{2\pi} \exp(0) dx = \int_0^{2\pi} 1 dx = 2 \pi$">|; 

$key = q/displaystyleint_0^{2pi}exp(imgkx)dx=unsur{imgk}[exp(2piimgk)-exp(0)]=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \int_0^{2\pi} \exp(\img k x) dx = \unsur{\img k} [\exp(2\pi\img k)-\exp(0)] = 0$">|; 

$key = q/displaystyleint_0^{2pi}exp(imgkx)dx=unsur{imgk}int_0^{2piimgk}exp(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \int_0^{2\pi} \exp(\img k x) dx = \unsur{\img k} \int_0^{2\pi\img k} \exp(s) ds$">|; 

$key = q/displaystyleintlimits_{-infty}^{+infty}exp(2piimg(x-z)y)dy=delta(x-z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.74ex; vertical-align: -3.88ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \int\limits_{-\infty}^{+\infty} \exp(2\pi\img (x-z) y) dy = \delta(x-z)$">|; 

$key = q/displaystylemathcal{F}(1)(x)=delta(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \mathcal{F}(1)(x) = \delta(x)$">|; 

$key = q/displaystylemathcal{F}(G)(y)=exp(-piy^2)intlimits_{-infty}^{+infty}exp(-pixi^2)dxi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.74ex; vertical-align: -3.88ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle \mathcal{F}(G)(y) = \exp(-\pi y^2) \int\limits_{-\infty}^{+\infty} \exp(-\pi \xi^2) d\xi$">|; 

$key = q/displaystylemathcal{F}(G)(y)=intlimits_{-infty}^{+infty}exp(-pix^2-2piimgxy)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.74ex; vertical-align: -3.88ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\displaystyle \mathcal{F}(G)(y) = \int\limits_{-\infty}^{+\infty} \exp(-\pi x^2 - 2\pi\img x y) dx$">|; 

$key = q/displaystylemathcal{F}(G)=G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle \mathcal{F}(G) = G$">|; 

$key = q/displaystylemathcal{F}(delta)(x)=intlimits_{-infty}^{+infty}delta(x)exp(-2piimgxy)dx=exp(0)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.74ex; vertical-align: -3.88ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \mathcal{F}(\delta)(x) = \int\limits_{-\infty}^{+\infty} \delta(x) \exp(-2\pi\img x y) dx = \exp(0) = 1$">|; 

$key = q/displaystylemathcal{F}(u)(y)=int_{setR}u(x)exp(2piimgxy)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle \mathcal{F}(u)(y) = \int_{\setR} u(x) \exp(2\pi \img x y) dx$">|; 

$key = q/displaystylemathcal{F}(ustarv)=mathcal{F}(u)mathcal{F}(v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle \mathcal{F}(u \star v) = \mathcal{F}(u)\mathcal{F}(v)$">|; 

$key = q/displaystylemathcal{F}left(OD{u}{x}right)(y)=2piimgymathcal{F}(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle \mathcal{F}\left(\OD{u}{x}\right)(y) = 2 \pi \img y \mathcal{F}(y)$">|; 

$key = q/displaystylemathcal{F}left[d_a(u)right]=unsur{a}mathcal{F}left[d_{1slasha}(u)right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle \mathcal{F}\left[d_a(u)\right] = \unsur{a}\mathcal{F}\left[d_{1/a}(u)\right]$">|; 

$key = q/displaystylemathcal{F}left[t_a(u)right](y)=exp(-2piimgay)mathcal{F}(u)(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle \mathcal{F}\left[t_a(u)\right](y) = \exp(-2\pi\img a y)\mathcal{F}(u)(y)$">|; 

$key = q/displaystylescalaire{e_m}{e_n}=intlimits_{-Tslash2}^{Tslash2}expleft(frac{2piimg(n-m)x}{T}right)dx=2pidelta_{mn};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.53ex; vertical-align: -4.27ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \scalaire{e_m}{e_n} = \int\limits_{-T/2}^{T/2} \exp\left(\frac{2\pi\img (n-m) x}{T}\right)dx = 2 \pi \delta_{mn}$">|; 

$key = q/displaystylescalaire{mathcal{F}(u)}{mathcal{F}(v)}=scalaire{u}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle \scalaire{\mathcal{F}(u)}{\mathcal{F}(v)} = \scalaire{u}{v}$">|; 

$key = q/displaystylescalaire{u}{v}=intlimits_{-Tslash2}^{Tslash2}bar{u}(x)v(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.53ex; vertical-align: -4.27ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \scalaire{u}{v} = \int\limits_{-T/2}^{T/2} \bar{u}(x) v(x) dx$">|; 

$key = q/displaystylescalaire{u}{v}=intlimits_{-infty}^{+infty}bar{u}(x)v(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.74ex; vertical-align: -3.88ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle \scalaire{u}{v} = \int\limits_{-\infty}^{+\infty} \bar{u}(x) v(x) dx$">|; 

$key = q/displaystylescalaire{u}{v}=sum_{k=-infty}^{+infty}conjugue(hat{u}_k)hat{v}_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.37ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \scalaire{u}{v} = \sum_{k=-\infty}^{+\infty} \conjugue(\hat{u}_k) \hat{v}_k$">|; 

$key = q/displaystylesum_{i=0}^{n}a^i=frac{1-a^{n+1}}{1-a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \sum_{i=0}^{n} a^i = \frac{1-a^{n+1}}{1-a}$">|; 

$key = q/displaystylesum_{k=0}^{N-1}u_ke(k,-m)=sum_{n=-Nslash2}^{Nslash2-1}U_nsum_{k=0}^{N-1}e(k,n-m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.09ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \sum_{k=0}^{N-1} u_k e(k,-m) = \sum_{n=-N/2}^{N/2-1} U_n \sum_{k=0}^{N-1} e(k,n-m)$">|; 

$key = q/displaystylesum_{k=0}^{n}exp(imgkx)=frac{1-exp(img(n+1)x)}{1-exp(imgx)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \sum_{k=0}^{n} \exp(\img k x) = \frac{1-\exp(\img (n+1) x)}{1-\exp(\img x)}$">|; 

$key = q/displaystylet=a+frac{T}{2pi}x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.93ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle t = a + \frac{T}{2\pi} x$">|; 

$key = q/displaystylet_a(u)(x)=u(x-a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle t_a(u)(x) = u(x-a)$">|; 

$key = q/displaystyleu(x)=sum_{k=-infty}^{+infty}hat{u}_ke_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.37ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle u(x) = \sum_{k=-\infty}^{+\infty} \hat{u}_k e_k$">|; 

$key = q/displaystyleu(x_k)=sum_{n=-Nslash2}^{Nslash2-1}U(y_n)exp(2piimgx_ky_n)Deltay;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.09ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle u(x_k) = \sum_{n=-N/2}^{N/2-1} U(y_n) \exp(2 \pi \img x_k y_n) \Delta y$">|; 

$key = q/displaystyleu_k=sum_{n=-Nslash2}^{Nslash2-1}U_ne(k,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.09ex; vertical-align: -3.54ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle u_k = \sum_{n=-N/2}^{N/2-1} U_n e(k,n)$">|; 

$key = q/displaystylev(x)=sum_{k=-infty}^{+infty}hat{v}_ke_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.37ex; vertical-align: -3.23ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle v(x) = \sum_{k=-\infty}^{+\infty} \hat{v}_k e_k$">|; 

$key = q/dx=d(x-a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img88.svg"
 ALT="$dx = d(x-a)$">|; 

$key = q/e(k,-m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$e(k,-m)$">|; 

$key = q/e_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img12.svg"
 ALT="$e_k$">|; 

$key = q/img=sqrt{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.50ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\img = \sqrt{-1}$">|; 

$key = q/k,l,m,NinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img33.svg"
 ALT="$k,l,m,N \in \setZ$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$k$">|; 

$key = q/k=-Nslash2,...,Nslash2-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$k = -N/2 , ... ,N/2 - 1$">|; 

$key = q/k=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$k=0$">|; 

$key = q/kinsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img2.svg"
 ALT="$k\in\setZ$">|; 

$key = q/kne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$k\ne 0$">|; 

$key = q/m,n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img45.svg"
 ALT="$m,n$">|; 

$key = q/m=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img47.svg"
 ALT="$m = n$">|; 

$key = q/mathcal{F}:XmapstoX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\mathcal{F} : X \mapsto X$">|; 

$key = q/min{-Nslash2,...,Nslash2-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$m \in \{-N/2,...,N/2-1\}$">|; 

$key = q/n+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img29.svg"
 ALT="$n+1$">|; 

$key = q/n-m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img43.svg"
 ALT="$n-m$">|; 

$key = q/scalaire{mathcal{F}(u)}{mathcal{F}(v)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\scalaire{\mathcal{F}(u)}{\mathcal{F}(v)}$">|; 

$key = q/u,vinXsubsetfonction(setR,setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$u,v \in X \subset \fonction(\setR,\setR)$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$u$">|; 

$key = q/u_k:k=0,...,N-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img35.svg"
 ALT="$u_k : k = 0,...,N-1$">|; 

$key = q/u_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img49.svg"
 ALT="$u_k$">|; 

$key = q/uincombilin{e_k:kinsetZ};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$u\in\combilin{e_k : k\in\setZ}$">|; 

$key = q/v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$v$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img28.svg"
 ALT="$x$">|; 

$key = q/x=2pilslashN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$x = 2\pi l/N$">|; 

$key = q/x=xi+eta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img80.svg"
 ALT="$x = \xi + \eta$">|; 

$key = q/xi=x+imgy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\xi = x + \img y$">|; 

$key = q/z=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img70.svg"
 ALT="$z=0$">|; 

$key = q/z=ax;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img95.svg"
 ALT="$z = a x$">|; 

$key = q/{-Nslash2,...,Nslash2-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\{-N/2,...,N/2-1\}$">|; 

$key = q/{Eqts}U(y)=mathcal{F}(u)(y)=intlimits_{-infty}^{+infty}u(x)exp(-2piimgxy)dxu(x)=mathcal{F}^{-1}(U)(x)=intlimits_{-infty}^{+infty}U(y)exp(2piimgxy)dy{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.07ex; vertical-align: -8.49ex; " SRC="|."$dir".q|img67.svg"
 ALT="\begin{Eqts}
U(y) = \mathcal{F}(u)(y) = \int\limits_{-\infty}^{+\infty} u(x) \ex...
...1}(U)(x) = \int\limits_{-\infty}^{+\infty} U(y) \exp(2\pi\img x y) dy
\end{Eqts}">|; 

$key = q/{Eqts}a=exp(imgx)a^k=exp(imgkx){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.63ex; vertical-align: -2.75ex; " SRC="|."$dir".q|img23.svg"
 ALT="\begin{Eqts}
a = \exp(\img x) \\\\
a^k = \exp(\img k x)
\end{Eqts}">|; 

$key = q/{Eqts}bar{e}_me_n=expleft(-frac{2piimgmx}{T}right)expleft(frac{2piimgnx}{T}right)bar{e}_me_n=expleft(frac{2piimg(n-m)x}{T}right){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.07ex; vertical-align: -5.48ex; " SRC="|."$dir".q|img14.svg"
 ALT="\begin{Eqts}
\bar{e}_m e_n =
\exp\left(-\frac{2\pi\img m x}{T}\right)\exp\left(\...
...ight) \\\\
\bar{e}_m e_n = \exp\left(\frac{2\pi\img (n-m) x}{T}\right)
\end{Eqts}">|; 

$key = q/{Eqts}expleft[-pi(x+imgy)^2right]=exp(-pix^2-2piimgxy)exp(piy^2){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.00ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img101.svg"
 ALT="\begin{Eqts}
\exp\left[ -\pi (x+\img y)^2 \right] =
\exp(-\pi x^2 - 2 \pi\img x y)\exp(\pi y^2)
\end{Eqts}">|; 

$key = q/{Eqts}int_0^{2pi}exp(imgkx)dx=cases{2pi&mbox{si}k=00&mbox{si}kne0cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img1.svg"
 ALT="\begin{Eqts}
\int_0^{2\pi} \exp(\img k x) dx =
\begin{cases}
2 \pi &amp; \mbox{si } k = 0 \\\\
0 &amp; \mbox{si } k \ne 0
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}int_a^{a+T}expleft(frac{2piimgkx}{T}right)dx=cases{T&mbox{si}k=00&mbox{si}kne0cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img10.svg"
 ALT="\begin{Eqts}
\int_a^{a+T} \exp\left(\frac{2\pi\img k x}{T}\right) dx =
\begin{cases}
T &amp; \mbox{si } k = 0 \\\\
0 &amp; \mbox{si } k \ne 0
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}int_{setR}mathcal{F}(u)(y)v(y)dy=int_{setR}u(x)v(y)exp(2piimgxy)dxdyint_{setR}mathcal{F}(u)(y)v(y)dy=int_{setR}u(x)mathcal{F}(v)(x)dx{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.44ex; vertical-align: -5.16ex; " SRC="|."$dir".q|img108.svg"
 ALT="\begin{Eqts}
\int_{\setR} \mathcal{F}(u)(y) v(y) dy = \int_{\setR} u(x) v(y) \ex...
...R} \mathcal{F}(u)(y) v(y) dy = \int_{\setR} u(x) \mathcal{F}(v)(x) dx
\end{Eqts}">|; 

$key = q/{Eqts}intlimits_{-infty}^{+infty}conjugueleft[mathcal{F}(u)(y)right]mathcal{F}(v)(y)dy=intlimits_{-infty}^{+infty}bar{u}(x)v(x)dx{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.74ex; vertical-align: -3.81ex; " SRC="|."$dir".q|img76.svg"
 ALT="\begin{Eqts}
\int\limits_{-\infty}^{+\infty} \conjugue\left[\mathcal{F}(u)(y)\ri...
...hcal{F}(v)(y) dy =
\int\limits_{-\infty}^{+\infty} \bar{u}(x) v(x) dx
\end{Eqts}">|; 

$key = q/{Eqts}mathcal{F}(ustarv)(y)=intlimits_{-infty}^{+infty}exp(-2piimgxy)dxintlimits_{-infty}^{+infty}u(x-z)v(z)dz{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.74ex; vertical-align: -3.81ex; " SRC="|."$dir".q|img78.svg"
 ALT="\begin{Eqts}
\mathcal{F}(u \star v)(y) =
\int\limits_{-\infty}^{+\infty} \exp(-2\pi\img x y) dx
\int\limits_{-\infty}^{+\infty} u(x-z) v(z) dz
\end{Eqts}">|; 

$key = q/{Eqts}mathcal{F}(ustarv)(y)=intlimits_{-infty}^{+infty}u(xi)exp(-2piimgxiy)dxiintlimits_{-infty}^{+infty}v(eta)exp(-2piimgetay)deta{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.74ex; vertical-align: -3.81ex; " SRC="|."$dir".q|img81.svg"
 ALT="\begin{Eqts}
\mathcal{F}(u \star v)(y) =
\int\limits_{-\infty}^{+\infty} u(\xi) ...
...
\int\limits_{-\infty}^{+\infty} v(\eta) \exp(-2\pi\img \eta y) d\eta
\end{Eqts}">|; 

$key = q/{Eqts}mathcal{F}left(OD{u}{x}right)(y)=-(-2piimgy)intlimits_{-infty}^{+infty}u(x)exp(-2piimgxy)dx{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.74ex; vertical-align: -3.81ex; " SRC="|."$dir".q|img84.svg"
 ALT="\begin{Eqts}
\mathcal{F}\left(\OD{u}{x}\right)(y) =
- (-2\pi\img y) \int\limits_{-\infty}^{+\infty} u(x)\exp(-2\pi\img x y) dx
\end{Eqts}">|; 

$key = q/{Eqts}mathcal{F}left(OD{u}{x}right)(y)=intlimits_{-infty}^{+infty}OD{u}{x}(x)exp(-2piimgxy)dx{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.74ex; vertical-align: -3.81ex; " SRC="|."$dir".q|img83.svg"
 ALT="\begin{Eqts}
\mathcal{F}\left(\OD{u}{x}\right)(y) =
\int\limits_{-\infty}^{+\infty} \OD{u}{x}(x)\exp(-2\pi\img x y) dx
\end{Eqts}">|; 

$key = q/{Eqts}mathcal{F}left[d_a(u)right](y)=intlimits_{-infty}^{+infty}u(ax)exp(-2piimgxy)dx{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.74ex; vertical-align: -3.81ex; " SRC="|."$dir".q|img94.svg"
 ALT="\begin{Eqts}
\mathcal{F}\left[d_a(u)\right](y) =
\int\limits_{-\infty}^{+\infty} u(a x) \exp(-2\pi\img x y) dx
\end{Eqts}">|; 

$key = q/{Eqts}s=imgkxds=imgkdx{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img6.svg"
 ALT="\begin{Eqts}
s = \img k x \\\\
ds = \img k dx
\end{Eqts}">|; 

$key = q/{Eqts}sum_{k=0}^{N-1}expleft(frac{2piimgkl}{N}right)=cases{N&mbox{si}m=lslashNinsetZ0&mbox{si}m=lslashNnotinsetZcases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.33ex; vertical-align: -3.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="\begin{Eqts}
\sum_{k=0}^{N-1} \exp \left( \frac{2 \pi \img k l}{N} \right) =
\be...
... } m = l/N \in\setZ \\\\
0 &amp; \mbox{si } m = l/N \notin\setZ
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}u(x)=intlimits_{-infty}^{+infty}U(y)exp(2piimgxy)dyU(y)=intlimits_{-infty}^{+infty}u(x)exp(-2piimgxy)dx{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.07ex; vertical-align: -8.49ex; " SRC="|."$dir".q|img65.svg"
 ALT="\begin{Eqts}
u(x) = \int\limits_{-\infty}^{+\infty} U(y) \exp(2\pi\img x y) dy \\\\
U(y) = \int\limits_{-\infty}^{+\infty} u(x) \exp(-2\pi\img x y) dx
\end{Eqts}">|; 

$key = q/{Eqts}u_k=sum_{n=-Nslash2}^{Nslash2-1}U_nexpleft(frac{2piimgkn}{N}right)U_n=unsur{N}sum_{k=0}^{N-1}u_kexpleft(-frac{2piimgkn}{N}right){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.00ex; vertical-align: -7.45ex; " SRC="|."$dir".q|img51.svg"
 ALT="\begin{Eqts}
u_k = \sum_{n=-N/2}^{N/2-1} U_n \exp\left(\frac{2 \pi \img k n}{N}\...
...ur{N} \sum_{k=0}^{N-1} u_k \exp\left(-\frac{2 \pi \img k n}{N}\right)
\end{Eqts}">|; 

$key = q/{Eqts}u_k=u(x_k)U_n=U(y_n){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img53.svg"
 ALT="\begin{Eqts}
u_k = u(x_k) \\\\
U_n = U(y_n)
\end{Eqts}">|; 

$key = q/{Eqts}u_ke(k,-m)=sum_{n=-Nslash2}^{Nslash2-1}U_ne(k,n)e(k,-m)u_ke(k,-m)=sum_{n=-Nslash2}^{Nslash2-1}U_ne(k,n-m){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 16.75ex; vertical-align: -7.83ex; " SRC="|."$dir".q|img40.svg"
 ALT="\begin{Eqts}
u_k e(k,-m) = \sum_{n=-N/2}^{N/2-1} U_n e(k,n) e(k,-m) \\\\
u_k e(k,-m) = \sum_{n=-N/2}^{N/2-1} U_n e(k,n-m)
\end{Eqts}">|; 

$key = q/{Eqts}x_k=kDeltaxy_n=nDeltay{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img52.svg"
 ALT="\begin{Eqts}
x_k = k\Delta x \\\\
y_n = n\Delta y
\end{Eqts}">|; 

$key = q/{Eqts}xi=x-zeta=z{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img79.svg"
 ALT="\begin{Eqts}
\xi = x - z \\\\
\eta = z
\end{Eqts}">|; 

1;

