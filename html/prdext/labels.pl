# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate labels original text with physical files.


$key = q/chap:prdext/;
$external_labels{$key} = "$URL/" . q|prdext.html|; 
$noresave{$key} = "$nosave";

$key = q/def:eps/;
$external_labels{$key} = "$URL/" . q|prdext.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2023 (Released January 1, 2023)
# labels from external_latex_labels array.


$key = q/chap:prdext/;
$external_latex_labels{$key} = q|1 Produit extérieur|; 
$noresave{$key} = "$nosave";

$key = q/def:eps/;
$external_latex_labels{$key} = q|1.3 Permutations en dimension $N$|; 
$noresave{$key} = "$nosave";

1;

