# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \mathcal{U} = u_1 \wedge u_2 \wedge ... \wedge u_M = \sum_{i_1,i_...
...otimes e_{ i_{N - M} } \cdot u_M^{i_N} \cdot \hdots \cdot u_1^{ i_{N - M + 1} }$">|; 

$key = q/(0,0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$(0,0)$">|; 

$key = q/(1,2,3,...,N);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$(1,2,3,...,N)$">|; 

$key = q/(e_1,...,e_N);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$(e_1,...,e_N)$">|; 

$key = q/(e_1,e_2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$(e_1,e_2)$">|; 

$key = q/(e_1,e_2,...,e_N);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$(e_1,e_2,...,e_N)$">|; 

$key = q/(e_1,e_2,e_3);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$(e_1,e_2,e_3)$">|; 

$key = q/(i,j,k,...,s);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$(i,j,k,...,s)$">|; 

$key = q/(u_1+v_1,u_2+v_2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$(u_1+v_1,u_2+v_2)$">|; 

$key = q/(u_1,u_2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$(u_1,u_2)$">|; 

$key = q/(v_1,v_2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$(v_1,v_2)$">|; 

$key = q/Ainmatrice(K,N,N);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$A \in \matrice(K,N,N)$">|; 

$key = q/Delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\Delta$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$E$">|; 

$key = q/E=combilin{e_1,e_2,e_3};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$E = \combilin{e_1,e_2,e_3}$">|; 

$key = q/E=combilin{e_1,e_2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$E = \combilin{e_1,e_2}$">|; 

$key = q/M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img56.svg"
 ALT="$M$">|; 

$key = q/M=N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img66.svg"
 ALT="$M = N$">|; 

$key = q/MleN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img52.svg"
 ALT="$M \le N$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$N$">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$S$">|; 

$key = q/alpha,betainS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\alpha,\beta \in S$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\corps$">|; 

$key = q/displaystyleA=left(a_{ij}right)_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -1.14ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle A = \left( a_{ij} \right)_{i,j}$">|; 

$key = q/displaystyleDelta=u_1wedgeu_2wedge...wedgeu_N=sum_{i,j,...,k=1}^Npermutation_{ij...k}cdotu_1^icdotu_2^jcdothdotscdotu_N^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.63ex; vertical-align: -3.37ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \Delta = u_1 \wedge u_2 \wedge ... \wedge u_N = \sum_{i,j,...,k = 1}^N \permutation_{ij...k} \cdot u_1^i \cdot u_2^j \cdot \hdots \cdot u_N^k$">|; 

$key = q/displaystyleU_{i_1,...,i_{N-M}}=sum_{i_{N-M+1},...,i_N}permutation_{i_1,i_2,...i_N}cdotu_1^{i_{N-M+1}}cdothdotscdotu_M^{i_N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.95ex; vertical-align: -3.44ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle U_{ i_1,...,i_{N - M} } = \sum_{i_{N - M + 1},...,i_N} \permutation_{i_1,i_2,...i_N} \cdot u_1^{ i_{N - M + 1} } \cdot \hdots \cdot u_M^{i_N}$">|; 

$key = q/displaystylea_i=sum_ja_{ij}e_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle a_i = \sum_j a_{ij} e_j$">|; 

$key = q/displaystyledet(u_1,...,u_N)=u_1wedgeu_2wedge...wedgeu_N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle \det(u_1,...,u_N) = u_1 \wedge u_2 \wedge ... \wedge u_N$">|; 

$key = q/displaystylemathcal{E}=sum_{i_1,i_2,...,i_N}permutation_{i_1,i_2,...,i_N}cdote_{i_1}otimese_{i_2}otimes...otimese_{i_N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \mathcal{E} = \sum_{i_1,i_2,...,i_N} \permutation_{i_1,i_2,...,i_N} \cdot e_{i_1} \otimes e_{i_2} \otimes ... \otimes e_{i_N}$">|; 

$key = q/displaystylepermutation_{i...j...j...k}=-permutation_{i...j...j...k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.19ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \permutation_{i ... j ... j ... k} = - \permutation_{i ... j ... j ... k}$">|; 

$key = q/displaystylepermutation_{i...j...j...k}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.33ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \permutation_{i ... j ... j ... k} = 0$">|; 

$key = q/displaystylepermutation_{ijk...l}=(-1)^p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \permutation_{i j k ... l} = (-1)^p$">|; 

$key = q/displaystylescalaire{e_j}{u_k}=u_k^j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.96ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \scalaire{e_j}{u_k} = u_k^j$">|; 

$key = q/displaystylescalaire{u}{vwedgew}=sum_{i,j,k}permutation_{ijk}u_iv_jw_k=uwedgevwedgew;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -3.37ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \scalaire{u}{v \wedge w} = \sum_{i,j,k} \permutation_{ijk} u_i v_j w_k = u \wedge v \wedge w$">|; 

$key = q/displaystyleu_1wedgeu_2wedge...wedgeu_M=contraction{mathcal{E}}{M}{u_Motimes...otimesu_1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle u_1 \wedge u_2 \wedge ... \wedge u_M = \contraction{ \mathcal{E} }{M}{ u_M \otimes ... \otimes u_1 }$">|; 

$key = q/displaystyleu_k=sum_{i=1}^Nu_k^ie_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.28ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle u_k = \sum_{i = 1}^N u_k^i e_i$">|; 

$key = q/displaystyleuwedgev=-vwedgeu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle u \wedge v = - v \wedge u$">|; 

$key = q/displaystyleuwedgev=sum_{i,j,k=1}^3left(permutation_{ijk}u_jv_kright)e_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.57ex; vertical-align: -3.37ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle u \wedge v = \sum_{i,j,k = 1}^3 \left( \permutation_{ijk} u_j v_k \right) e_i$">|; 

$key = q/displaystyleuwedgev=sum_{i,j=1}^2permutation_{ij}u_iv_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.52ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle u \wedge v = \sum_{i,j=1}^2 \permutation_{ij} u_i v_j$">|; 

$key = q/displaystyleuwedgevwedgew=sum_{i,j,k=1}^3permutation_{ijk}u_iv_jw_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.57ex; vertical-align: -3.37ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle u \wedge v \wedge w = \sum_{i,j,k=1}^3 \permutation_{ijk} u_i v_j w_k$">|; 

$key = q/mathcal{E}intenseur_N(E);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\mathcal{E} \in \tenseur_N(E)$">|; 

$key = q/mathcal{U}intenseur_{N-M}(E);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\mathcal{U} \in \tenseur_{N-M}(E)$">|; 

$key = q/permutation;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\permutation$">|; 

$key = q/permutation_*;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.57ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\permutation_*$">|; 

$key = q/permutation_{12}=permutation_{123}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\permutation_{12} = \permutation_{123} = 1$">|; 

$key = q/permutation_{ijk...l};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\permutation_{ijk...l}$">|; 

$key = q/permutation_{ijk}quad(i,j=1,2,3);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\permutation_{ijk} \quad (i,j=1,2,3)$">|; 

$key = q/permutation_{ij}quad(i,j=1,2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\permutation_{ij}\quad (i,j=1,2)$">|; 

$key = q/pinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$p \in \setN$">|; 

$key = q/u,v,w,...inE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img64.svg"
 ALT="$u,v,w,... \in E$">|; 

$key = q/u,v,w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img26.svg"
 ALT="$u,v,w$">|; 

$key = q/u,v,winE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$u,v,w \in E$">|; 

$key = q/u,v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$u,v$">|; 

$key = q/u,vinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$u,v \in E$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img28.svg"
 ALT="$u$">|; 

$key = q/u_1,u_2,...u_MinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img53.svg"
 ALT="$u_1,u_2,...u_M \in E$">|; 

$key = q/u_i,v_i,w_iinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$u_i,v_i,w_i \in S$">|; 

$key = q/u_i,v_iinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$u_i,v_i \in S$">|; 

$key = q/u_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img69.svg"
 ALT="$u_i$">|; 

$key = q/u_k^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img54.svg"
 ALT="$u_k^i$">|; 

$key = q/v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$v$">|; 

$key = q/w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img30.svg"
 ALT="$w$">|; 

$key = q/wedge;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.48ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\wedge$">|; 

$key = q/{Eqts}(uwedgev)_1=u_2v_3-u_3v_2(uwedgev)_2=u_3v_1-u_1v_3(uwedgev)_3=u_1v_2-u_2v_1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img35.svg"
 ALT="\begin{Eqts}
(u \wedge v)_1 = u_2 v_3 - u_3 v_2 \\\\
(u \wedge v)_2 = u_3 v_1 - u_1 v_3 \\\\
(u \wedge v)_3 = u_1 v_2 - u_2 v_1
\end{Eqts}">|; 

$key = q/{Eqts}det(A)=a_1wedge...wedgea_N=sum_{i_1,i_2,...i_N}permutation_{i_1,i_2,...i_N}a_{1,i_1}...a_{N,i_N}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.35ex; " SRC="|."$dir".q|img74.svg"
 ALT="\begin{Eqts}
\det(A) = a_1 \wedge ... \wedge a_N = \sum_{i_1,i_2,...i_N} \permutation_{i_1,i_2,...i_N} a_{1, i_1 } ...
a_{N, i_N}
\end{Eqts}">|; 

$key = q/{Eqts}permutation_{1,2,...,N}=1permutation_{i...j...k...l}=-permutation_{i...k...j...l}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img40.svg"
 ALT="\begin{Eqts}
\permutation_{1,2,...,N} = 1 \\\\
\permutation_{i ... j ... k ... l} = - \permutation_{i ... k ... j ... l}
\end{Eqts}">|; 

$key = q/{Eqts}permutation_{11}=permutation_{22}=0permutation_{12}=-1qquadpermutation_{21}=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img18.svg"
 ALT="\begin{Eqts}
\permutation_{11} = \permutation_{22} = 0 \\\\
\permutation_{12} = -1 \qquad \permutation_{21} = 1
\end{Eqts}">|; 

$key = q/{Eqts}permutation_{11}=permutation_{22}=0permutation_{12}=1qquadpermutation_{21}=-1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img17.svg"
 ALT="\begin{Eqts}
\permutation_{11} = \permutation_{22} = 0 \\\\
\permutation_{12} = 1 \qquad \permutation_{21} = -1
\end{Eqts}">|; 

$key = q/{Eqts}u=sum_{i=1}^2u_ie_iv=sum_{i=1}^2v_ie_i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.02ex; vertical-align: -6.96ex; " SRC="|."$dir".q|img6.svg"
 ALT="\begin{Eqts}
u = \sum_{i = 1}^2 u_i e_i \\\\
v = \sum_{i = 1}^2 v_i e_i
\end{Eqts}">|; 

$key = q/{Eqts}u=sum_{i=1}^3u_ie_iv=sum_{i=1}^3v_ie_iw=sum_{i=1}^3w_ie_i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 22.82ex; vertical-align: -10.87ex; " SRC="|."$dir".q|img23.svg"
 ALT="\begin{Eqts}
u = \sum_{i = 1}^3 u_i e_i \\\\
v = \sum_{i = 1}^3 v_i e_i \\\\
w = \sum_{i = 1}^3 w_i e_i
\end{Eqts}">|; 

$key = q/{Eqts}uwedgev=-vwedgeuuwedgeu=0(alphau+betav)wedgew=alphauwedgew+betavwedgewwwedge(alphau+betav)=alphawwedgeu+betawwedgev{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.12ex; vertical-align: -6.00ex; " SRC="|."$dir".q|img63.svg"
 ALT="\begin{Eqts}
u \wedge v = - v \wedge u \\\\
u \wedge u = 0 \\\\
(\alpha u + \beta ...
...
w \wedge (\alpha u + \beta v) = \alpha w \wedge u + \beta w \wedge v
\end{Eqts}">|; 

$key = q/{Eqts}uwedgevwedgew=-uwedgewwedgevuwedgevwedgew=-vwedgeuwedgew{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img31.svg"
 ALT="\begin{Eqts}
u \wedge v \wedge w = - u \wedge w \wedge v \\\\
u \wedge v \wedge w = - v \wedge u \wedge w
\end{Eqts}">|; 

1;

