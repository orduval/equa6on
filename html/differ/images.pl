# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.92ex; " SRC="|."$dir".q|img55.svg"
 ALT="\begin{eqnarray*}
f_i(a + \lambda  \varpi_j) - f_i(a) &amp;=&amp; \sum_{k = 1}^n \parti...
...e_{jk} + E_i(h) \\\\
&amp;=&amp; \lambda \cdot \partial_j f_i(a) + E_i(h)
\end{eqnarray*}">|; 

$key = q/(n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$(n,n)$">|; 

$key = q/(phi_1,...,phi_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$(\phi_1,...,\phi_m)$">|; 

$key = q/(varpi_1,...,varpi_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$(\varpi_1,...,\varpi_n)$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$0$">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img103.svg"
 ALT="$1$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img50.svg"
 ALT="$A$">|; 

$key = q/AsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img97.svg"
 ALT="$A \subseteq \Omega$">|; 

$key = q/Delta_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\Delta_{ij}$">|; 

$key = q/Delta_{ij}incorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\Delta_{ij} \in \corps$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$F$">|; 

$key = q/Omega,F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\Omega, F$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\Omega$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$a$">|; 

$key = q/abs{s-t}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\abs{s - t} \le \delta$">|; 

$key = q/ainA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img100.svg"
 ALT="$a \in A$">|; 

$key = q/ainOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img6.svg"
 ALT="$a \in \Omega$">|; 

$key = q/alpha=(alpha_1,alpha_2,...,alpha_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\alpha = (\alpha_1, \alpha_2, ..., \alpha_n)$">|; 

$key = q/b=a+lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img92.svg"
 ALT="$b = a + \lambda$">|; 

$key = q/btoa;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img95.svg"
 ALT="$b \to a$">|; 

$key = q/continue^0=continue;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\continue^0 = \continue$">|; 

$key = q/continue^1(A,F);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\continue^1(A,F)$">|; 

$key = q/continue^2(A,corps);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\continue^2(A,\corps)$">|; 

$key = q/continue^infty(A,corps);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\continue^\infty(A,\corps)$">|; 

$key = q/continue^k(A,corps);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.81ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\continue^k(A,\corps)$">|; 

$key = q/continue^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\continue^k$">|; 

$key = q/continue_{mu}^{-1}(A,B);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.03ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\continue_{\mu}^{-1}(A,B)$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\corps$">|; 

$key = q/deltastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\delta \strictsuperieur 0$">|; 

$key = q/df;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img79.svg"
 ALT="$df$">|; 

$key = q/differentielle{f}{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.68ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\differentielle{f}{a}$">|; 

$key = q/displaystyleBig[partialf(x)Big]^T=deriveepartielle{f^T}{x}=left(deriveepartielle{f}{x^T}right)^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.19ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle \Big[ \partial f(x) \Big]^T = \deriveepartielle{f^T}{x} = \left( \deriveepartielle{f}{x^T} \right)^T$">|; 

$key = q/displaystyleE(h)=f(a+h)-f(a)-differentielle{f}{a}(h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle E(h) = f(a + h) - f(a) - \differentielle{f}{a}(h)$">|; 

$key = q/displaystyleE(h)=sum_{i=1}^mE_i(h)cdotphi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle E(h) = \sum_{i = 1}^m E_i(h) \cdot \phi_i$">|; 

$key = q/displaystyleOD{f}{x}(a)=lim_{btoa}frac{f(b)-f(a)}{b-a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.28ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle \OD{f}{x}(a) = \lim_{b \to a} \frac{f(b) - f(a)}{b - a}$">|; 

$key = q/displaystyleOD{f}{x}(a)=lim_{lambdato0}frac{f(a+lambda)-f(a)}{lambda};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle \OD{f}{x}(a) = \lim_{\lambda \to 0} \frac{f(a + \lambda) - f(a)}{\lambda}$">|; 

$key = q/displaystyleOOD{f}{t}(a)=partial^2f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle \OOD{f}{t}(a) = \partial^2 f(a)$">|; 

$key = q/displaystyleabs{f(s)-f(t)-partialf(t)cdot(s-t)}leepsiloncdotabs{s-t};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle \abs{f(s) - f(t) - \partial f(t) \cdot (s - t)} \le \epsilon \cdot \abs{s - t}$">|; 

$key = q/displaystyledblederiveepartielle{f}{x}(a)=partial^2f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle \dblederiveepartielle{f}{x}(a) = \partial^2 f(a)$">|; 

$key = q/displaystylederiveepartielle{f}{x}(a)=partial_xf(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \deriveepartielle{f}{x}(a) = \partial_x f(a)$">|; 

$key = q/displaystylederiveepartielle{f}{x}(a)=partialf(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \deriveepartielle{f}{x}(a) = \partial f(a)$">|; 

$key = q/displaystylederiveepartielle{f}{y}(a)=partial_yf(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.40ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle \deriveepartielle{f}{y}(a) = \partial_y f(a)$">|; 

$key = q/displaystyledfdxdx{f}{x_i}=dfdxdy{f}{x_i}{x_i}=partial_{ii}^2f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.77ex; vertical-align: -2.29ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle \dfdxdx{f}{x_i} = \dfdxdy{f}{x_i}{x_i} = \partial_{ii}^2 f(a)$">|; 

$key = q/displaystyledfdxdy{f}{x_i}{x_j}(a)=partial_{ij}^2f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\displaystyle \dfdxdy{f}{x_i}{x_j}(a) = \partial_{ij}^2 f(a)$">|; 

$key = q/displaystyledifferentielle{f}{a}(varpi_j)=sum_{i=1}^mDelta_{ij}cdotphi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \differentielle{f}{a}(\varpi_j) = \sum_{i = 1}^m \Delta_{ij} \cdot \phi_i$">|; 

$key = q/displaystylef(a+h)-f(a)=differentielle{f}{a}(h)+E(h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle f(a + h) - f(a) = \differentielle{f}{a}(h) + E(h)$">|; 

$key = q/displaystylef(a+h)-f(a)=partialf(a)cdoth+E(h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle f(a + h) - f(a) = \partial f(a) \cdot h + E(h)$">|; 

$key = q/displaystylef(a+h)-f(a)=sum_{j=1}^ndifferentielle{f}{a}(varpi_j)cdoth_j+E(h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle f(a + h) - f(a) = \sum_{j = 1}^n \differentielle{f}{a}(\varpi_j) \cdot h_j + E(h)$">|; 

$key = q/displaystylef(a+h)-f(a)approxdifferentielle{f}{a}(h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle f(a + h) - f(a) \approx \differentielle{f}{a}(h)$">|; 

$key = q/displaystylef(a+h)=f(a)+OD{f}{x}(a)cdoth+E(h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle f(a + h) = f(a) + \OD{f}{x}(a) \cdot h + E(h)$">|; 

$key = q/displaystylef(x)=sum_{i=1}^mf_i(x)cdotphi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle f(x) = \sum_{i = 1}^m f_i(x) \cdot \phi_i$">|; 

$key = q/displaystylef:(x,y)mapstof(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle f : (x,y) \mapsto f(x,y)$">|; 

$key = q/displaystylef:xmapstof(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle f : x \mapsto f(x)$">|; 

$key = q/displaystylef_i(a+h)-f_i(a)=sum_{j=1}^nDelta_{ij}cdoth_j+E_i(h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle f_i(a + h) - f_i(a) = \sum_{j = 1}^n \Delta_{ij} \cdot h_j + E_i(h)$">|; 

$key = q/displaystylefrac{norme{E(h)}}{norme{h}}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \frac{ \norme{E(h)} }{ \norme{h} } \le \epsilon$">|; 

$key = q/displaystyleh=sum_{i=1}^nh_icdotvarpi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle h = \sum_{i = 1}^n h_i \cdot \varpi_i$">|; 

$key = q/displaystylelambdaphi_1quadLeftrightarrowquadlambdaquadLeftrightarrowquadlambdaepsilon_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle \lambda  \phi_1 \quad \Leftrightarrow \quad \lambda \quad \Leftrightarrow \quad \lambda  \epsilon_1$">|; 

$key = q/displaystylelim_{hto0}f(a+h)=f(a)+lim_{hto0}left[differentielle{f}{a}(h)+E(h)right]=f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.91ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \lim_{h \to 0} f(a + h) = f(a) + \lim_{h \to 0} \left[ \differentielle{f}{a}(h) + E(h) \right] = f(a)$">|; 

$key = q/displaystylelim_{hto0}norme{E(h)}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.62ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \lim_{h \to 0} \norme{E(h)} = 0$">|; 

$key = q/displaystylelim_{lambdato0}frac{E(lambda)}{lambda}=lim_{lambdato0}left[frac{f(a+lambda)-f(a)}{lambda}-OD{f}{x}(a)right]=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \lim_{\lambda \to 0} \frac{E(\lambda)}{\lambda} = \lim_{\lambda \to 0} \left[ \frac{f(a + \lambda) - f(a)}{\lambda} - \OD{f}{x}(a) \right] = 0$">|; 

$key = q/displaystylelim_{substack{hto0hne0}}frac{norme{E(h)}}{norme{h}}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.79ex; vertical-align: -3.41ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \lim_{ \substack{ h \to 0 \ h \ne 0 } } \frac{ \norme{E(h)} }{ \norme{h} } = 0$">|; 

$key = q/displaystylenorme{E(h)}leepsiloncdotnorme{h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \norme{E(h)} \le \epsilon \cdot \norme{h}$">|; 

$key = q/displaystylenorme{differentielle{f}{a}(0)}lenorme{differentielle{f}{a}}cdotnorme{0}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.03ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \norme{ \differentielle{f}{a}(0) } \le \norme{ \differentielle{f}{a} } \cdot \norme{0} = 0$">|; 

$key = q/displaystylenorme{differentielle{f}{a}(h)}lenorme{differentielle{f}{a}}cdotnorme{h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.03ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \norme{ \differentielle{f}{a}(h) } \le \norme{ \differentielle{f}{a} } \cdot \norme{h}$">|; 

$key = q/displaystylenorme{differentielle{f}{a}}cdotnorme{h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.03ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \norme{ \differentielle{f}{a} } \cdot \norme{h}$">|; 

$key = q/displaystylepartial^2f=partialleft(partialfright);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle \partial^2 f = \partial \left( \partial f \right)$">|; 

$key = q/displaystylepartial^kf:Amapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\displaystyle \partial^k f : A \mapsto \corps$">|; 

$key = q/displaystylepartial^kf=(partialcirc...circpartial)(f);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\displaystyle \partial^k f = (\partial \circ ... \circ \partial)(f)$">|; 

$key = q/displaystylepartial_jf(a)=Big(partial_jf_i(a)Big)_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.46ex; vertical-align: -1.70ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \partial_j f(a) = \Big( \partial_j f_i(a) \Big)_i$">|; 

$key = q/displaystylepartial_jf_i(a)=deriveepartielle{f_i}{x_j}(a)=Delta_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.60ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \partial_j f_i(a) = \deriveepartielle{f_i}{x_j}(a) = \Delta_{ij}$">|; 

$key = q/displaystylepartial_jf_i(a)=lim_{lambdato0}frac{f_i(a+lambdavarpi_j)-f_i(a)}{lambda};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \partial_j f_i(a) = \lim_{\lambda \to 0} \frac{f_i(a + \lambda  \varpi_j) - f_i(a)}{\lambda}$">|; 

$key = q/displaystylepartial_jf_i(a)=lim_{lambdato0}left[frac{f_i(a+lambdavarpi_j)-f_i(a)}{lambda}-frac{E_i(h)}{lambda}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle \partial_j f_i(a) = \lim_{\lambda \to 0} \left[ \frac{f_i(a + \lambda  \varpi_j) - f_i(a)}{\lambda} - \frac{E_i(h)}{\lambda} \right]$">|; 

$key = q/displaystylepartial_{ij}^2f(a)=partial_ileft(partial_jfright)(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.02ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle \partial_{ij}^2 f(a) = \partial_i \left( \partial_j f \right)(a)$">|; 

$key = q/displaystylepartialf(a)=Big(partial_jf_i(a)Big)_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.76ex; vertical-align: -2.00ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \partial f(a) = \Big( \partial_j f_i(a) \Big)_{i,j}$">|; 

$key = q/displaystylepartialf(a)=lim_{lambdato0}frac{f(a+lambda)-f(a)}{lambda};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.79ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\displaystyle \partial f(a) = \lim_{\lambda \to 0} \frac{f(a + \lambda) - f(a)}{\lambda}$">|; 

$key = q/displaystylepartialf:amapstopartialf(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle \partial f : a \mapsto \partial f(a)$">|; 

$key = q/displaystylepartialf_i(a)=Big(partial_jf_i(a)Big)_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.76ex; vertical-align: -2.00ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle \partial f_i(a) = \Big( \partial_j f_i(a) \Big)_j$">|; 

$key = q/displaystylesum_iphi_icdotleft[f_i(a+h)-f_i(a)-E_i(h)-sum_{j=1}^nDelta_{ij}cdoth_jright]=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.45ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle \sum_i \phi_i \cdot \left[ f_i(a + h) - f_i(a) - E_i(h) - \sum_{j = 1}^n \Delta_{ij} \cdot h_j \right] = 0$">|; 

$key = q/displaystylev(x)=int_a^xu(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\displaystyle v(x) = \int_a^x u(x) d\mu(x)$">|; 

$key = q/dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img80.svg"
 ALT="$dx$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/f(a+h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$f(a + h)$">|; 

$key = q/f,h,E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img65.svg"
 ALT="$f,h,E$">|; 

$key = q/f:Amapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img123.svg"
 ALT="$f : A \mapsto \corps$">|; 

$key = q/f:OmegamapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$f : \Omega \mapsto F$">|; 

$key = q/f:Omegamapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img102.svg"
 ALT="$f : \Omega \mapsto \corps$">|; 

$key = q/f:corpsmapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img87.svg"
 ALT="$f : \corps \mapsto \corps$">|; 

$key = q/f:setR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img83.svg"
 ALT="$f : \setR^n \mapsto \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$f$">|; 

$key = q/f^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img130.svg"
 ALT="$f^{-1}$">|; 

$key = q/f_1,...,f_m:OmegamapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img32.svg"
 ALT="$f_1,...,f_m : \Omega \mapsto F$">|; 

$key = q/f_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$f_i$">|; 

$key = q/frontiereA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\frontiere A$">|; 

$key = q/h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$h$">|; 

$key = q/h=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img26.svg"
 ALT="$h = 0$">|; 

$key = q/h=lambdavarpi_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img53.svg"
 ALT="$h = \lambda  \varpi_j$">|; 

$key = q/h_1,...,h_nincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img36.svg"
 ALT="$h_1,...,h_n \in \corps$">|; 

$key = q/h_k=lambdaindicatrice_{jk};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img54.svg"
 ALT="$h_k = \lambda  \indicatrice_{jk}$">|; 

$key = q/hinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$h \in \Omega$">|; 

$key = q/homeomorphisme^k(A,corps);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.81ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\homeomorphisme^k(A,\corps)$">|; 

$key = q/hto0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img94.svg"
 ALT="$h \to 0$">|; 

$key = q/i=j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img112.svg"
 ALT="$i = j$">|; 

$key = q/i^{ème};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img69.svg"
 ALT="$i^{ème}$">|; 

$key = q/iinsetZ(1,m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$i \in \setZ(1,m)$">|; 

$key = q/j^{ème};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img61.svg"
 ALT="$j^{ème}$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$k$">|; 

$key = q/kinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img117.svg"
 ALT="$k \in \setN$">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\lambda$">|; 

$key = q/lambda=b-a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\lambda = b - a$">|; 

$key = q/lambdaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\lambda \in \corps$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img68.svg"
 ALT="$m$">|; 

$key = q/m=n=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img84.svg"
 ALT="$m = n = 1$">|; 

$key = q/matrice(corps^n,n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\matrice(\corps^n,n,1)$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img66.svg"
 ALT="$n$">|; 

$key = q/norme{h}=lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\norme{h} = \lambda$">|; 

$key = q/partial;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\partial$">|; 

$key = q/partial^2f(a)inmatrice(corps,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\partial^2 f(a) \in \matrice(\corps,n,n)$">|; 

$key = q/partial^kf:corpsmapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.56ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\partial^k f : \corps \mapsto \corps$">|; 

$key = q/partial^kf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.56ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\partial^k f$">|; 

$key = q/partial_1f_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\partial_1 f_1$">|; 

$key = q/partial_if;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\partial_i f$">|; 

$key = q/partial_jf_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\partial_j f_i$">|; 

$key = q/partialf(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\partial f(a)$">|; 

$key = q/partialf:Amapstocorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\partial f : A \mapsto \corps^n$">|; 

$key = q/partialf:Amapstomatrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\partial f : A \mapsto \matrice(\corps,m,n)$">|; 

$key = q/partialf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\partial f$">|; 

$key = q/phi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\phi_i$">|; 

$key = q/s,tinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img139.svg"
 ALT="$s,t \in A$">|; 

$key = q/u:AmapstoB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img133.svg"
 ALT="$u : A \mapsto B$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img136.svg"
 ALT="$u$">|; 

$key = q/v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img134.svg"
 ALT="$v$">|; 

$key = q/varpi_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\varpi_j$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img81.svg"
 ALT="$x$">|; 

$key = q/x=(x_1,...,x_s);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$x = (x_1,...,x_s)$">|; 

$key = q/xinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img34.svg"
 ALT="$x \in \Omega$">|; 

$key = q/y=(y_1,...,y_t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$y = (y_1,...,y_t)$">|; 

$key = q/{Eqts}df=deriveepartielle{f}{x^T}dxdf_i=sum_jderiveepartielle{f_i}{x_j}dx_j{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.11ex; vertical-align: -5.50ex; " SRC="|."$dir".q|img78.svg"
 ALT="\begin{Eqts}
df = \deriveepartielle{f}{x^T}  dx \\\\
df_i = \sum_j \deriveepartielle{f_i}{x_j}  dx_j
\end{Eqts}">|; 

$key = q/{Eqts}lim_{hto0}=lim_{substack{hto0hne0}}lim_{btoa}=lim_{substack{btoabnea}}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.48ex; vertical-align: -5.18ex; " SRC="|."$dir".q|img20.svg"
 ALT="\begin{Eqts}
\lim_{h \to 0} = \lim_{ \substack{ h \to 0 \ h \ne 0 } } \\\\
\lim_{b \to a} = \lim_{ \substack{ b \to a \ b \ne a } }
\end{Eqts}">|; 

$key = q/{Eqts}partial^alphaf=partial^{alpha_1...alpha_n}f=partial_1^{alpha_1}...partial_n^{alpha_n}f{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img132.svg"
 ALT="\begin{Eqts}
\partial^\alpha f = \partial^{\alpha_1 ... \alpha_n} f =
\partial_1^{\alpha_1} ... \partial_n^{\alpha_n} f
\end{Eqts}">|; 

$key = q/{eqnarraystar}partial^0f&=&fpartial^kf&=&partialbig(partial^{k-1}fbig){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.80ex; " SRC="|."$dir".q|img119.svg"
 ALT="\begin{eqnarray*}
\partial^0 f &amp;=&amp; f \\\\
\partial^k f &amp;=&amp; \partial \big( \partial^{k-1} f \big)
\end{eqnarray*}">|; 

1;

