# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 24.62ex; " SRC="|."$dir".q|img31.svg"
 ALT="\begin{eqnarray*}
U \cdot x &amp;=&amp; x - \frac{2 u^\dual \cdot x}{u^\dual \cdot u} \c...
...) \cdot x + 2 (u^\dual \cdot x) \cdot y }{u^\dual \cdot u^\dual}
\end{eqnarray*}">|; 

$key = q/(m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$(m,n)$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img61.svg"
 ALT="$A$">|; 

$key = q/A^{(n-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img67.svg"
 ALT="$A^{(n-1)}$">|; 

$key = q/H^{(n-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img68.svg"
 ALT="$H^{(n-1)}$">|; 

$key = q/H_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img63.svg"
 ALT="$H_1$">|; 

$key = q/Q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img84.svg"
 ALT="$Q$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img81.svg"
 ALT="$R$">|; 

$key = q/U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$U$">|; 

$key = q/U=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$U = I$">|; 

$key = q/U=matunitaire(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$U = \matunitaire(u)$">|; 

$key = q/U^dualcdotU=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img24.svg"
 ALT="$U^\dual \cdot U = I$">|; 

$key = q/U^{-1}=U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$U^{-1} = U$">|; 

$key = q/U^{-1}=U^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$U^{-1} = U^\dual$">|; 

$key = q/Ucdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$U \cdot x$">|; 

$key = q/Ucdotxapproxy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$U \cdot x \approx y$">|; 

$key = q/alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\alpha$">|; 

$key = q/alpha=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\alpha = 0$">|; 

$key = q/alpha=lambdacdotx_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\alpha = \lambda \cdot x_i$">|; 

$key = q/alphainsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\alpha \in \setC$">|; 

$key = q/alphainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\alpha \in \setR$">|; 

$key = q/alphane0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\alpha \ne 0$">|; 

$key = q/displaystyle(Q,R)=householder(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle (Q,R) = \householder(A)$">|; 

$key = q/displaystyle2+alphacdot(u^dualcdotu)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle 2 + \alpha \cdot (u^\dual \cdot u) = 0$">|; 

$key = q/displaystyle2cdotalpha+alpha^2cdot(u^dualcdotu)=alphacdot[2+alphacdot(u^dualcdotu)]=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle 2 \cdot \alpha + \alpha^2 \cdot (u^\dual \cdot u) = \alpha \cdot [2 + \alpha \cdot (u^\dual \cdot u)] = 0$">|; 

$key = q/displaystyleA=QcdotR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle A = Q \cdot R$">|; 

$key = q/displaystyleA=[xx_2...x_n];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle A = [x  x_2  ...  x_n]$">|; 

$key = q/displaystyleH=H_pcdot...cdotH_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle H = H_p \cdot ... \cdot H_1$">|; 

$key = q/displaystyleQ=H^{-1}=H_1cdot...cdotH_p=H^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.79ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle Q = H^{-1} = H_1 \cdot ... \cdot H_p = H^\dual$">|; 

$key = q/displaystyleU=I-frac{2}{u^dualcdotu}cdotucdotu^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle U = I - \frac{2}{u^\dual \cdot u} \cdot u \cdot u^\dual$">|; 

$key = q/displaystyleU=matelementaire(alpha,u,u)=I+alphacdotucdotu^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle U = \matelementaire(\alpha,u,u) = I + \alpha \cdot u \cdot u^\dual$">|; 

$key = q/displaystyleU^dual=I-frac{2}{u^dualcdotu}cdotucdotu^dual=U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle U^\dual = I - \frac{2}{u^\dual \cdot u} \cdot u \cdot u^\dual = U$">|; 

$key = q/displaystyleU^dualcdotU=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle U^\dual \cdot U = I$">|; 

$key = q/displaystyleUcdotx=gammacdotcanonique_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle U \cdot x = \gamma \cdot \canonique_i$">|; 

$key = q/displaystyleUcdotx=gammacdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle U \cdot x = \gamma \cdot y$">|; 

$key = q/displaystylealpha=-frac{2}{u^dualcdotu};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \alpha = - \frac{2}{u^\dual \cdot u}$">|; 

$key = q/displaystylealpha=lambdacdotx_i=sqrt{frac{x^dualcdotx}{abs{x_i}^2}}cdotx_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.23ex; vertical-align: -3.11ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle \alpha = \lambda \cdot x_i = \sqrt{ \frac{ x^\dual \cdot x }{ \abs{x_i}^2 } } \cdot x_i$">|; 

$key = q/displaystylealpha=sqrt{x^dualcdotx}insetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.34ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \alpha = \sqrt{x^\dual \cdot x} \in \setR$">|; 

$key = q/displaystylecomposante_{ij}U=canonique_i^dualcdotU^dualcdotUcdotcanonique_j=canonique_i^dualcdotcanonique_j=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.25ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \composante_{ij} U = \canonique_i^\dual \cdot U^\dual \cdot U \cdot \canonique_j = \canonique_i^\dual \cdot \canonique_j = \indicatrice_{ij}$">|; 

$key = q/displaystylegamma=frac{2u^dualcdotx}{u^dualcdotu^dual}insetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.90ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \gamma = \frac{2 u^\dual \cdot x}{u^\dual \cdot u^\dual} \in \setC$">|; 

$key = q/displaystylematunitaire(u)=I-frac{2}{u^dualcdotu}cdotucdotu^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \matunitaire(u) = I - \frac{2}{u^\dual \cdot u} \cdot u \cdot u^\dual$">|; 

$key = q/displaystylescalaire{Ucdotx}{Ucdoty}=x^dualcdotU^dualcdotUcdoty=scalaire{x}{y}=x^dualcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\displaystyle \scalaire{U \cdot x}{U \cdot y} = x^\dual \cdot U^\dual \cdot U \cdot y = \scalaire{x}{y} = x^\dual \cdot y$">|; 

$key = q/displaystyleu^dualcdotu^dual-2u^dualcdotx=(2x^dualcdotx-2y^dualcdotx)-(2x^dualcdotx-2y^dualcdotx)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle u^\dual \cdot u^\dual - 2 u^\dual \cdot x = (2 x^\dual \cdot x - 2 y^\dual \cdot x) - (2 x^\dual \cdot x - 2 y^\dual \cdot x) = 0$">|; 

$key = q/displaystyleu^dualcdotu^dual-2u^dualcdotx=(x^dualcdotx-x^dualcdoty-y^dualcdotx+y^dualcdoty)-(2x^dualcdotx-2y^dualcdotx);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle u^\dual \cdot u^\dual - 2 u^\dual \cdot x = (x^\dual \cdot x - x^...
... - y^\dual \cdot x + y^\dual \cdot y) - (2 x^\dual \cdot x - 2 y^\dual \cdot x)$">|; 

$key = q/displaystylex^dualcdoty=conjugue(x^dualcdoty)=y^dualcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle x^\dual \cdot y = \conjugue(x^\dual \cdot y) = y^\dual \cdot x$">|; 

$key = q/displaystyley=alphacdotcanonique_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle y = \alpha \cdot \canonique_i$">|; 

$key = q/displaystyley^dualcdotx=conjaccent{alpha}cdotx_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.32ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle y^\dual \cdot x = \conjaccent{\alpha} \cdot x_i$">|; 

$key = q/displaystyley^dualcdotx=lambdacdotconjaccent{x}_icdotx_i=lambdacdotabs{x_i}^2insetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle y^\dual \cdot x = \lambda \cdot \conjaccent{x}_i \cdot x_i = \lambda \cdot \abs{x_i}^2 \in \setR$">|; 

$key = q/displaystyley^dualcdoty=abs{alpha}^2=x^dualcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle y^\dual \cdot y = \abs{\alpha}^2 = x^\dual \cdot x$">|; 

$key = q/displaystyley^dualcdoty=lambda^2cdotabs{x_i}^2=x^dualcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle y^\dual \cdot y = \lambda^2 \cdot \abs{x_i}^2 = x^\dual \cdot x$">|; 

$key = q/gamma_1insetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\gamma_1 \in \setC$">|; 

$key = q/gammacdotcanonique_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\gamma \cdot \canonique_1$">|; 

$key = q/gammainsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\gamma \in \setC$">|; 

$key = q/i^{eme};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img42.svg"
 ALT="$i^{eme}$">|; 

$key = q/k+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img72.svg"
 ALT="$k + 1$">|; 

$key = q/lambda^2=x^dualcdotxslashabs{x_i}^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\lambda^2 = x^\dual \cdot x / \abs{x_i}^2$">|; 

$key = q/lambdainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\lambda \in \setR$">|; 

$key = q/m=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img77.svg"
 ALT="$m = n$">|; 

$key = q/mstrictinferieurn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img75.svg"
 ALT="$m \strictinferieur n$">|; 

$key = q/mstrictsuperieurn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img79.svg"
 ALT="$m \strictsuperieur n$">|; 

$key = q/norme{Ucdotx}=norme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\norme{U \cdot x} = \norme{x}$">|; 

$key = q/norme{x}=sqrt{scalaire{x}{x}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.81ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\norme{x} = \sqrt{\scalaire{x}{x} }$">|; 

$key = q/p=min{m,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$p = \min\{m,n\}$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$u$">|; 

$key = q/u=x-alphacdotcanonique_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img58.svg"
 ALT="$u = x - \alpha \cdot \canonique_i$">|; 

$key = q/u=x-y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$u = x - y$">|; 

$key = q/u^dualcdotustrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.81ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img20.svg"
 ALT="$u^\dual \cdot u \strictsuperieur 0$">|; 

$key = q/x,y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$x,y$">|; 

$key = q/x,yincorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$x,y \in \corps^n$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$x$">|; 

$key = q/x^dualcdotx=y^dualcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.20ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img34.svg"
 ALT="$x^\dual \cdot x = y^\dual \cdot y$">|; 

$key = q/x^dualcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.20ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img35.svg"
 ALT="$x^\dual \cdot y$">|; 

$key = q/x_i=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img47.svg"
 ALT="$x_i = 0$">|; 

$key = q/x_i=composante_ix;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.65ex; " SRC="|."$dir".q|img45.svg"
 ALT="$x_i = \composante_i x$">|; 

$key = q/x_ine0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$x_i \ne 0$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img41.svg"
 ALT="$y$">|; 

$key = q/y^dualcdotx=0insetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img48.svg"
 ALT="$y^\dual \cdot x = 0 \in \setR$">|; 

$key = q/{Eqts}H_1cdotA=Matrix{{cc}gamma_1&hdots0&A^{(n-1)}Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img66.svg"
 ALT="\begin{Eqts}
H_1 \cdot A =
\begin{Matrix}{cc}
\gamma_1 &amp; \hdots \\\\
0 &amp; A^{(n - 1)}
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}H_2=Matrix{{cc}1&00&H^{(n-1)}Matrix{=Matrix{{cc}I_1&00&H^{(n-1)}Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img69.svg"
 ALT="\begin{Eqts}
H_2 =
\begin{Matrix}{cc}
1 &amp; 0 \\\\
0 &amp; H^{(n-1)}
\end{Matrix}=
\begin{Matrix}{cc}
I_1 &amp; 0 \\\\
0 &amp; H^{(n-1)}
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}H_2cdotH_1cdotA=Matrix{{ccc}gamma_1&hdots&hdots0&gamma_2&hdots0&0&H^{(n-2)}Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.62ex; vertical-align: -3.75ex; " SRC="|."$dir".q|img70.svg"
 ALT="\begin{Eqts}
H_2 \cdot H_1 \cdot A =
\begin{Matrix}{ccc}
\gamma_1 &amp; \hdots &amp; \hdots \\\\
0 &amp; \gamma_2 &amp; \hdots \\\\
0 &amp; 0 &amp; H^{(n-2)}
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}H_{k+1}=Matrix{{cc}I_k&00&H^{(n-k)}Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img73.svg"
 ALT="\begin{Eqts}
H_{k + 1} =
\begin{Matrix}{cc}
I_k &amp; 0 \\\\
0 &amp; H^{(n-k)}
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}HcdotA=R=Matrix{{cccc}gamma_1&hdots&hdots&hdots0&ddots&hdots&hdots0&0&gamma_m&hdotsMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.91ex; vertical-align: -4.39ex; " SRC="|."$dir".q|img76.svg"
 ALT="\begin{Eqts}
H \cdot A = R =
\begin{Matrix}{cccc}
\gamma_1 &amp; \hdots &amp; \hdots &amp; \...
... &amp; \ddots &amp; \hdots &amp; \hdots \\\\
0 &amp; 0 &amp; \gamma_m &amp; \hdots
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}HcdotA=R=Matrix{{ccc}gamma_1&hdots&hdots0&ddots&hdots0&0&gamma_m0&0&0Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.69ex; vertical-align: -5.79ex; " SRC="|."$dir".q|img80.svg"
 ALT="\begin{Eqts}
H \cdot A = R =
\begin{Matrix}{ccc}
\gamma_1 &amp; \hdots &amp; \hdots \\\\
0 &amp; \ddots &amp; \hdots \\\\
0 &amp; 0 &amp; \gamma_m \\\\
0 &amp; 0 &amp; 0
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}HcdotA=R=Matrix{{ccc}gamma_1&hdots&hdots0&ddots&hdots0&0&gamma_mMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.91ex; vertical-align: -4.39ex; " SRC="|."$dir".q|img78.svg"
 ALT="\begin{Eqts}
H \cdot A = R =
\begin{Matrix}{ccc}
\gamma_1 &amp; \hdots &amp; \hdots \\\\
0 &amp; \ddots &amp; \hdots \\\\
0 &amp; 0 &amp; \gamma_m
\end{Matrix}\end{Eqts}">|; 

1;

