# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.52ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \sup_{\le^\dual} \big\{ a, \sup_{\le^\dual} \{b, c\} \big\} = \su...
..._{\le^\dual} \big\{ \sup_{\le^\dual} \{a, b\}, \sup_{\le^\dual} \{b, c\} \big\}$">|; 

$key = q/(mcdotn,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img150.svg"
 ALT="$(m \cdot n, 1)$">|; 

$key = q/(mr,ns);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img155.svg"
 ALT="$(m r,n s)$">|; 

$key = q/(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img173.svg"
 ALT="$(x,y)$">|; 

$key = q/-infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img141.svg"
 ALT="$-\infty$">|; 

$key = q/0(3);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img181.svg"
 ALT="$0(3)$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img93.svg"
 ALT="$0$">|; 

$key = q/A(m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img153.svg"
 ALT="$A(m,n)$">|; 

$key = q/A(n,n),B(m,m),C(m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img161.svg"
 ALT="$A(n,n), B(m,m), C(m,n)$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img47.svg"
 ALT="$A$">|; 

$key = q/Ainmatrice(K,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img147.svg"
 ALT="$A \in \matrice(K,m,n)$">|; 

$key = q/AsubseteqF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img46.svg"
 ALT="$A \subseteq F$">|; 

$key = q/AsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img37.svg"
 ALT="$A \subseteq \Omega$">|; 

$key = q/B(r,s);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img154.svg"
 ALT="$B(r,s)$">|; 

$key = q/Deltax,Deltay;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img180.svg"
 ALT="$\Delta x, \Delta y$">|; 

$key = q/Deltax;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img165.svg"
 ALT="$\Delta x$">|; 

$key = q/Deltax^mDeltay^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img169.svg"
 ALT="$\Delta x^m\Delta y^n$">|; 

$key = q/Deltay;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img166.svg"
 ALT="$\Delta y$">|; 

$key = q/E_0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img114.svg"
 ALT="$E_0 = 0$">|; 

$key = q/E_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img110.svg"
 ALT="$E_i$">|; 

$key = q/E_i=hat{y}_i-y_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img98.svg"
 ALT="$E_i = \hat{y}_i - y_i$">|; 

$key = q/E_{i-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img111.svg"
 ALT="$E_{i-1}$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img48.svg"
 ALT="$F$">|; 

$key = q/F=OmegasetminusU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$F = \Omega \setminus U$">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img62.svg"
 ALT="$I$">|; 

$key = q/I=[0,x];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$I = [0,x]$">|; 

$key = q/I=[x,0];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$I = [x,0]$">|; 

$key = q/I_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img133.svg"
 ALT="$I_2$">|; 

$key = q/I_2=I^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img143.svg"
 ALT="$I_2 = I^2$">|; 

$key = q/M=max{abs{e_1},...,abs{e_N}}leepsiloncdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img79.svg"
 ALT="$M = \max\{\abs{e_1},...,\abs{e_N}\} \le \epsilon \cdot h$">|; 

$key = q/NinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img68.svg"
 ALT="$N \in \setN$">|; 

$key = q/O(p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img167.svg"
 ALT="$O(p)$">|; 

$key = q/P;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img189.svg"
 ALT="$P$">|; 

$key = q/P=[x,x+Deltax]times[y,y+Deltay]subsetsetR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img164.svg"
 ALT="$P = [x,x+\Delta x]\times[y,y+\Delta y]\subset\setR^2$">|; 

$key = q/P_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img184.svg"
 ALT="$P_i$">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img183.svg"
 ALT="$S$">|; 

$key = q/SsubsetsetR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img172.svg"
 ALT="$S\subset\setR^2$">|; 

$key = q/T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img116.svg"
 ALT="$T$">|; 

$key = q/TinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img89.svg"
 ALT="$T \in \setR$">|; 

$key = q/U=boule(x,epsilon);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$U = \boule(x,\epsilon)$">|; 

$key = q/UsubseteqOmegasetminusA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$U \subseteq \Omega \setminus A$">|; 

$key = q/X(m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img159.svg"
 ALT="$X(m,n)$">|; 

$key = q/a,b,cinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$a, b, c \in \mathcal{T}$">|; 

$key = q/a_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img149.svg"
 ALT="$a_i$">|; 

$key = q/a_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img157.svg"
 ALT="$a_{ij}$">|; 

$key = q/abs{e_k}leepsiloncdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\abs{e_k} \le \epsilon \cdot h$">|; 

$key = q/abs{h}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\abs{h} \le \delta$">|; 

$key = q/abs{h}strictinferieurmin{delta,1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\abs{h} \strictinferieur \min\{\delta,1\}$">|; 

$key = q/ainOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img53.svg"
 ALT="$a \in \Omega$">|; 

$key = q/ainadhA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img50.svg"
 ALT="$a \in \adh A$">|; 

$key = q/bigotimes;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\bigotimes$">|; 

$key = q/binome{N}{0}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\binome{N}{0} = 1$">|; 

$key = q/boule(a,epsilon)capAneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\boule(a,\epsilon) \cap A \ne \emptyset$">|; 

$key = q/boule(x,epsilon)capA=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\boule(x,\epsilon) \cap A = \emptyset$">|; 

$key = q/deltastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\delta \strictsuperieur 0$">|; 

$key = q/dexp(x)=exp(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img140.svg"
 ALT="$d\exp(x) = \exp(x) dx$">|; 

$key = q/displaystyle(x,y)to(x+Deltax,y)to(x+Deltax,y+Deltay)to(x,y+Deltay)to(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img176.svg"
 ALT="$\displaystyle (x,y)\to (x+\Delta x,y)\to (x+\Delta x,y+\Delta y)
\to (x,y+\Delta y)\to(x,y)$">|; 

$key = q/displaystyleg=pgcd(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle g = \pgcd(i,j)$">|; 

$key = q/displaystylegamma=varkappa=sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \gamma = \varkappa = \sigma$">|; 

$key = q/displaystylegammagemuge{b,c};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \gamma \ge \mu \ge \{b,c\}$">|; 

$key = q/displaystylegammagesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \gamma \ge \sigma$">|; 

$key = q/displaystylegammage{lambda,mu};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle \gamma \ge \{\lambda,\mu\}$">|; 

$key = q/displaystylegmoduloi=gmoduloj=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle g \modulo i = g \modulo j = 0$">|; 

$key = q/displaystyleimodulop=jmodulop=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle i \modulo p = j \modulo p = 0$">|; 

$key = q/displaystyleinfbig{a,inf{b,c}big}=infbig{inf{a,b},cbig}=infbig{inf{a,b},inf{b,c}big};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \inf \big\{ a, \inf \{b, c\} \big\} = \inf \big\{ \inf \{a, b\}, c \big\} = \inf \big\{ \inf \{a, b\}, \inf \{b, c\} \big\}$">|; 

$key = q/displaystylep=pgcd(i,j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle p = \pgcd(i,j)$">|; 

$key = q/displaystylesigma=gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \sigma = \gamma$">|; 

$key = q/displaystylesigma=varkappa;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \sigma = \varkappa$">|; 

$key = q/displaystylesigmagegamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \sigma \ge \gamma$">|; 

$key = q/displaystylesigmagelambdage{a,b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \sigma \ge \lambda \ge \{a,b\}$">|; 

$key = q/displaystylesigmagemuge{b,c};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \sigma \ge \mu \ge \{b,c\}$">|; 

$key = q/displaystylesigmagevarkappa;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \sigma \ge \varkappa$">|; 

$key = q/displaystylesupbig{a,sup{b,c}big}=supbig{sup{a,b},cbig}=supbig{sup{a,b},sup{b,c}big};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \sup \big\{ a, \sup \{b, c\} \big\} = \sup \big\{ \sup \{a, b\}, c \big\} = \sup \big\{ \sup \{a, b\}, \sup \{b, c\} \big\}$">|; 

$key = q/displaystylevarkappagelambdage{a,b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \varkappa \ge \lambda \ge \{a,b\}$">|; 

$key = q/displaystylevarkappagesigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \varkappa \ge \sigma$">|; 

$key = q/displaystylevarkappage{lambda,mu};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \varkappa \ge \{\lambda,\mu\}$">|; 

$key = q/distance(a,A)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\distance(a,A) = 0$">|; 

$key = q/dxdy=rdrdtheta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img132.svg"
 ALT="$dx dy = r dr d\theta$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/exp(h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\exp(h)$">|; 

$key = q/exp;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\exp$">|; 

$key = q/f(t,y)=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img84.svg"
 ALT="$f(t,y) = y$">|; 

$key = q/f(x_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$f(x_k)$">|; 

$key = q/f(x_{k-1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img76.svg"
 ALT="$f(x_{k - 1})$">|; 

$key = q/f,g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img171.svg"
 ALT="$f,g$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img61.svg"
 ALT="$f$">|; 

$key = q/f=exp;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img54.svg"
 ALT="$f = \exp$">|; 

$key = q/gammagea;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\gamma \ge a$">|; 

$key = q/gammage{a,b,c};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\gamma \ge \{a,b,c\}$">|; 

$key = q/gammage{a,mu};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\gamma \ge \{a,\mu\}$">|; 

$key = q/h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img107.svg"
 ALT="$h$">|; 

$key = q/h=TslashN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img90.svg"
 ALT="$h = T / N$">|; 

$key = q/hat{y}_N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\hat{y}_N$">|; 

$key = q/hat{y}_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\hat{y}_i$">|; 

$key = q/i,jinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img32.svg"
 ALT="$i,j \in \setN$">|; 

$key = q/i=N,N-1,...,0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img112.svg"
 ALT="$i = N, N-1, ... , 0$">|; 

$key = q/kin{0,...,N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img72.svg"
 ALT="$k \in \{0,...,N$">|; 

$key = q/m+n=p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img170.svg"
 ALT="$m+n = p$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\mu$">|; 

$key = q/mu=abs{1+h-exp(h)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\mu = \abs{1 + h - \exp(h)}$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img188.svg"
 ALT="$n$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img168.svg"
 ALT="$p$">|; 

$key = q/partialP;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img175.svg"
 ALT="$\partial P$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img108.svg"
 ALT="$r$">|; 

$key = q/r=1+h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img106.svg"
 ALT="$r = 1+h$">|; 

$key = q/r=abs{r};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$r = \abs{r}$">|; 

$key = q/rexp(-r^2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img135.svg"
 ALT="$r\exp(-r^2)$">|; 

$key = q/s,h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img65.svg"
 ALT="$s,h$">|; 

$key = q/s,s+hinI;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img66.svg"
 ALT="$s, s + h \in I$">|; 

$key = q/setR^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\setR^2$">|; 

$key = q/sigmagegamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\sigma \ge \gamma$">|; 

$key = q/sigmagevarkappa;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\sigma \ge \varkappa$">|; 

$key = q/sigmage{a,mu};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\sigma \ge \{a,\mu\}$">|; 

$key = q/sigmage{lambda,c};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\sigma \ge \{\lambda,c\}$">|; 

$key = q/sigmage{lambda,mu};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\sigma \ge \{\lambda,\mu\}$">|; 

$key = q/sigmalegamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\sigma \le \gamma$">|; 

$key = q/sigmalevarkappa;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\sigma \le \varkappa$">|; 

$key = q/sinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img55.svg"
 ALT="$s \in \setR$">|; 

$key = q/theta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\theta$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img92.svg"
 ALT="$u$">|; 

$key = q/varkappagec;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\varkappa \ge c$">|; 

$key = q/varkappage{a,b,c};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\varkappa \ge \{a,b,c\}$">|; 

$key = q/varkappage{lambda,c};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\varkappa \ge \{\lambda,c\}$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img125.svg"
 ALT="$x$">|; 

$key = q/x=sqrt{pi}xi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.65ex; " SRC="|."$dir".q|img146.svg"
 ALT="$x = \sqrt{\pi}\xi$">|; 

$key = q/x_k=kcdoth;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img71.svg"
 ALT="$x_k = k \cdot h$">|; 

$key = q/xinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img38.svg"
 ALT="$x \in \Omega$">|; 

$key = q/xinU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img43.svg"
 ALT="$x \in U$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img57.svg"
 ALT="$x \in \setR$">|; 

$key = q/xnotinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img44.svg"
 ALT="$x \notin F$">|; 

$key = q/xnotinadhA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img49.svg"
 ALT="$x \notin \adh A$">|; 

$key = q/xstrictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img60.svg"
 ALT="$x \strictinferieur 0$">|; 

$key = q/y(t)=exp(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img87.svg"
 ALT="$y(t) = \exp(t)$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img85.svg"
 ALT="$y$">|; 

$key = q/y_N=exp(T);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img119.svg"
 ALT="$y_N = \exp(T)$">|; 

$key = q/y_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img101.svg"
 ALT="$y_i$">|; 

$key = q/{Eqts}(AbigotimesB)(CbigotimesD)=(AC)bigotimes(BD)(AbigotimesB)^H=A^HbigotimesB^Hmt{vec}(BAC)=(C^TbigotimesB)mt{vec}(A){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.85ex; vertical-align: -5.87ex; " SRC="|."$dir".q|img158.svg"
 ALT="\begin{Eqts}
(A \bigotimes B) (C \bigotimes D) = (A C) \bigotimes (B D) \\\\
(A \...
...H \bigotimes B^H \\\\
\mt{vec}(B A C) = (C^T \bigotimes B) \mt{vec}(A)
\end{Eqts}">|; 

$key = q/{Eqts}(x,y)=phi(r,theta)=left(rcos(theta),rsin(theta)right){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img127.svg"
 ALT="\begin{Eqts}
(x,y) = \phi(r,\theta) = \left(r\cos(\theta),r\sin(\theta)\right)
\end{Eqts}">|; 

$key = q/{Eqts}A=[a_1hdotsa_n]{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img148.svg"
 ALT="\begin{Eqts}
A = [a_1  \hdots  a_n]
\end{Eqts}">|; 

$key = q/{Eqts}AbigotimesB=Matrix{{ccc}a_{11}B&ldots&a_{1n}Bvdots&ddots&vdotsa_{m1}B&ldots&a_{mn}BMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.91ex; vertical-align: -4.39ex; " SRC="|."$dir".q|img156.svg"
 ALT="\begin{Eqts}
A \bigotimes B =
\begin{Matrix}{ccc}
a_{11} B &amp; \ldots &amp; a_{1n} B \...
...ts &amp; \ddots &amp; \vdots \\\\
a_{m1} B &amp; \ldots &amp; a_{mn} B \\\\
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}E=sum_{k=0}^{N-1}e_{N-k}cdot(1+h)^k{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.33ex; vertical-align: -3.10ex; " SRC="|."$dir".q|img78.svg"
 ALT="\begin{Eqts}
E = \sum_{k = 0}^{N - 1} e_{N - k} \cdot (1 + h)^k
\end{Eqts}">|; 

$key = q/{Eqts}E_i=left(1+h-exp(h)right)y_{i-1}+(1+h)E_{i-1}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img102.svg"
 ALT="\begin{Eqts}
E_i = \left( 1 + h - \exp(h) \right) y_{i-1} + (1 + h) E_{i-1}
\end{Eqts}">|; 

$key = q/{Eqts}I=int_setRexp(-x^2)dx{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img122.svg"
 ALT="\begin{Eqts}
I = \int_\setR \exp(-x^2) dx
\end{Eqts}">|; 

$key = q/{Eqts}I_2=-pileft[lim_{rto+infty}exp(-r^2)-exp(0)right]{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img139.svg"
 ALT="\begin{Eqts}
I_2 = - \pi \left[ \lim_{r \to +\infty} \exp(-r^2) - \exp(0) \right]
\end{Eqts}">|; 

$key = q/{Eqts}I_2=2piint_0^{+infty}rexp(-r^2){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.31ex; " SRC="|."$dir".q|img137.svg"
 ALT="\begin{Eqts}
I_2 = 2\pi \int_0^{+\infty} r \exp(-r^2)
\end{Eqts}">|; 

$key = q/{Eqts}I_2=I^2{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img126.svg"
 ALT="\begin{Eqts}
I_2 = I^2
\end{Eqts}">|; 

$key = q/{Eqts}I_2=int_0^{+infty}drint_0^{2pi}exp(-r^2)rdtheta{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.82ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img134.svg"
 ALT="\begin{Eqts}
I_2 = \int_0^{+\infty} dr \int_0^{2\pi} \exp(-r^2) r d\theta
\end{Eqts}">|; 

$key = q/{Eqts}I_2=int_setRexp(-x^2)dxint_setRexp(-y^2)dy{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img124.svg"
 ALT="\begin{Eqts}
I_2 = \int_\setR \exp(-x^2) dx \int_\setR \exp(-y^2) dy
\end{Eqts}">|; 

$key = q/{Eqts}I_2=int_{setR^2}exp(-x^2-y^2)dxdy{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img123.svg"
 ALT="\begin{Eqts}
I_2 = \int_{\setR^2} \exp(-x^2-y^2) dx dy
\end{Eqts}">|; 

$key = q/{Eqts}I_2=pi{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img142.svg"
 ALT="\begin{Eqts}
I_2 = \pi
\end{Eqts}">|; 

$key = q/{Eqts}K=A^TbigotimesI_m-I_nbigotimesB{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.90ex; vertical-align: -1.37ex; " SRC="|."$dir".q|img162.svg"
 ALT="\begin{Eqts}
K = A^T \bigotimes I_m - I_n \bigotimes B \\\\
\end{Eqts}">|; 

$key = q/{Eqts}Kcdotmt{vec}(X)=mt{vec}(C){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img163.svg"
 ALT="\begin{Eqts}
K \cdot \mt{vec}(X) = \mt{vec}(C)
\end{Eqts}">|; 

$key = q/{Eqts}OD{y}{t}(t)=y(t)y(0)=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.52ex; vertical-align: -3.70ex; " SRC="|."$dir".q|img86.svg"
 ALT="\begin{Eqts}
\OD{y}{t}(t) = y(t) \\\\
y(0) = 1
\end{Eqts}">|; 

$key = q/{Eqts}OD{}{r}exp(-r^2)=-2rexp(-r^2){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.91ex; " SRC="|."$dir".q|img138.svg"
 ALT="\begin{Eqts}
\OD{}{r}\exp(-r^2) = -2 r \exp(-r^2)
\end{Eqts}">|; 

$key = q/{Eqts}XA-BX=C{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img160.svg"
 ALT="\begin{Eqts}
X A - B X = C
\end{Eqts}">|; 

$key = q/{Eqts}abs{E}leMabs{sum_{k=0}^{N-1}(1+h)^k}leMcdotabs{frac{1-(1+h)^N}{1-(1+h)}}leepsiloncdothcdotfrac{1-abs{1+h}^N}{1-abs{1+h}}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.33ex; vertical-align: -3.10ex; " SRC="|."$dir".q|img83.svg"
 ALT="\begin{Eqts}
\abs{E} \le M \abs{\sum_{k = 0}^{N - 1} (1 + h)^k} \le M \cdot \abs...
... \le \epsilon \cdot h \cdot \frac{1 - \abs{1 + h}^N}{1 - \abs{1 + h}}
\end{Eqts}">|; 

$key = q/{Eqts}abs{detfrac{partial(x,y)}{partial(r,theta)}}=abs{rleft[cos(theta)^2+sin(theta)^2right]}=r{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img131.svg"
 ALT="\begin{Eqts}
\abs{ \det\frac{\partial (x,y)}{\partial (r,\theta)} } =
\abs{ r \left[\cos(\theta)^2 + \sin(\theta)^2\right] } = r
\end{Eqts}">|; 

$key = q/{Eqts}abs{f(s+h)-f(s)cdot(1+h)}leepsiloncdoth{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img64.svg"
 ALT="\begin{Eqts}
\abs{f(s + h) - f(s) \cdot (1 + h)} \le \epsilon \cdot h
\end{Eqts}">|; 

$key = q/{Eqts}exp(x)=sum_{k=0}^{+infty}frac{x^k}{k!}exp(x)=1+x+frac{x^2}{2}+frac{x^3}{6}+...{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.97ex; vertical-align: -5.93ex; " SRC="|."$dir".q|img94.svg"
 ALT="\begin{Eqts}
\exp(x) = \sum_{k=0}^{+\infty} \frac{x^k}{k !} \\\\
\exp(x) = 1 + x + \frac{x^2}{2} + \frac{x^3}{6} + ...
\end{Eqts}">|; 

$key = q/{Eqts}f(s+h)=f(s)+partialf(s)cdoth+o(h)=f(s)+f(s)cdoth+o(h)=f(s)cdot(1+h)+o(h){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img56.svg"
 ALT="\begin{Eqts}
f(s + h) = f(s) + \partial f(s) \cdot h + o(h) = f(s) + f(s) \cdot h + o(h) = f(s) \cdot (1 + h) + o(h)
\end{Eqts}">|; 

$key = q/{Eqts}f(x+epsilon,y+eta)=f+PD{f}{x}epsilon+PD{f}{y}eta+O(2)g(x+epsilon,y+eta)=f+PD{g}{x}epsilon+PD{g}{y}eta+O(2){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img174.svg"
 ALT="\begin{Eqts}
f(x+\epsilon,y+\eta) = f + \PD{f}{x}\epsilon + \PD{f}{y}\eta + O(2) \\\\
g(x+\epsilon,y+\eta) = f + \PD{g}{x}\epsilon + \PD{g}{y}\eta + O(2)
\end{Eqts}">|; 

$key = q/{Eqts}f(x_k)=f(x_{k-1})cdot(1+h)+e_k{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img73.svg"
 ALT="\begin{Eqts}
f(x_k) = f(x_{k - 1}) \cdot (1 + h) + e_k
\end{Eqts}">|; 

$key = q/{Eqts}frac{partial(x,y)}{partial(r,theta)}=Matrix{{cc}cos(theta)&-rsin(theta)sin(theta)&rcos(theta)Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img130.svg"
 ALT="\begin{Eqts}
\frac{\partial (x,y)}{\partial (r,\theta)} =
\begin{Matrix}{cc}
\cos(\theta) &amp; -r\sin(\theta) \\\\
\sin(\theta) &amp; r\cos(\theta)
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}h=frac{x}{N}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.60ex; " SRC="|."$dir".q|img69.svg"
 ALT="\begin{Eqts}
h = \frac{x}{N}
\end{Eqts}">|; 

$key = q/{Eqts}hat{y}_0=1hat{y}_i=hat{y}_{i-1}+hf(t,y_{i-1})hat{y}_i=hat{y}_{i-1}+hy_{i-1}hat{y}_i=(1+h)hat{y}_{i-1}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.12ex; vertical-align: -6.00ex; " SRC="|."$dir".q|img97.svg"
 ALT="\begin{Eqts}
\hat{y}_0 = 1 \\\\
\hat{y}_i = \hat{y}_{i-1} + h f(t,y_{i-1}) \\\\
\hat{y}_i = \hat{y}_{i-1} + h y_{i-1} \\\\
\hat{y}_i = (1+h) \hat{y}_{i-1}
\end{Eqts}">|; 

$key = q/{Eqts}hat{y}_N=(1+h)hat{y}_{N-1}hat{y}_N=(1+h)^2hat{y}_{N-2}vdotshat{y}_N=(1+h)^N{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.52ex; vertical-align: -6.71ex; " SRC="|."$dir".q|img120.svg"
 ALT="\begin{Eqts}
\hat{y}_N = (1+h) \hat{y}_{N-1} \\\\
\hat{y}_N = (1+h)^2 \hat{y}_{N-2} \\\\
\vdots \\\\
\hat{y}_N = (1+h)^N \\\\
\end{Eqts}">|; 

$key = q/{Eqts}hat{y}_i=(1+h)y_{i-1}+(1+h)E_{i-1}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img99.svg"
 ALT="\begin{Eqts}
\hat{y}_i = (1+h) y_{i-1} + (1+h) E_{i-1}
\end{Eqts}">|; 

$key = q/{Eqts}int_setRexp(-pixi^2)dxi=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img145.svg"
 ALT="\begin{Eqts}
\int_\setR \exp(-\pi\xi^2) d\xi = 1
\end{Eqts}">|; 

$key = q/{Eqts}int_setRexp(-x^2)dx=sqrt{pi}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img144.svg"
 ALT="\begin{Eqts}
\int_\setR \exp(-x^2) dx = \sqrt{\pi}
\end{Eqts}">|; 

$key = q/{Eqts}int_{partialP}(fn_x+gn_y)dpartialP=I+II+III+IV{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img187.svg"
 ALT="\begin{Eqts}
\int_{\partial P} (f n_x + g n_y) d\partial P = I + II + III + IV
\end{Eqts}">|; 

$key = q/{Eqts}int_{partialP}(fn_x+gn_y)dpartialP=int_Pleft(PD{f}{x}+PD{g}{y}right)dxdy{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img192.svg"
 ALT="\begin{Eqts}
\int_{\partial P} (f n_x + g n_y) d\partial P = \int_P \left( \PD{f}{x} + \PD{g}{y} \right) dx dy
\end{Eqts}">|; 

$key = q/{Eqts}int_{partialP}(fn_x+gn_y)dpartialP=left(PD{f}{x}+PD{g}{y}right)DeltaxDeltay+O(3){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img191.svg"
 ALT="\begin{Eqts}
\int_{\partial P} (f n_x + g n_y) d\partial P = \left( \PD{f}{x} + \PD{g}{y} \right) \Delta x \Delta y + O(3)
\end{Eqts}">|; 

$key = q/{Eqts}int_{partialP}fdx+gdy=I+II+III+IV{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img177.svg"
 ALT="\begin{Eqts}
\int_{\partial P} f dx + g dy = I + II + III + IV
\end{Eqts}">|; 

$key = q/{Eqts}int_{partialP}fdx+gdy=int_Pleft(PD{g}{x}-PD{f}{y}right)dxdy{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img182.svg"
 ALT="\begin{Eqts}
\int_{\partial P} f dx + g dy = \int_P \left( \PD{g}{x} - \PD{f}{y} \right) dx dy
\end{Eqts}">|; 

$key = q/{Eqts}int_{partialP}fdx+gdy=left(PD{g}{x}-PD{f}{y}right)DeltaxDeltay+O(3){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img179.svg"
 ALT="\begin{Eqts}
\int_{\partial P} f dx + g dy = \left( \PD{g}{x} - \PD{f}{y} \right) \Delta x \Delta y + O(3)
\end{Eqts}">|; 

$key = q/{Eqts}int_{partialS}(fn_x+gn_y)dpartialS=int_Sleft(PD{f}{x}+PD{g}{y}right)dS{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img193.svg"
 ALT="\begin{Eqts}
\int_{\partial S} (f n_x + g n_y) d\partial S = \int_S \left( \PD{f}{x} + \PD{g}{y} \right) dS
\end{Eqts}">|; 

$key = q/{Eqts}int_{partialS}fdx+gdy=int_Sleft(PD{g}{x}-PD{f}{y}right)dxdy{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.15ex; " SRC="|."$dir".q|img186.svg"
 ALT="\begin{Eqts}
\int_{\partial S} f dx + g dy = \int_S \left( \PD{g}{x} - \PD{f}{y} \right) dx dy
\end{Eqts}">|; 

$key = q/{Eqts}int_{partialS}fdx+gdy=sum_iint_{partialP_i}fdx+gdyint_{partialS}fdx+gdy=sum_iint_{P_i}left(PD{g}{x}-PD{f}{y}right)dxdy{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.09ex; vertical-align: -5.99ex; " SRC="|."$dir".q|img185.svg"
 ALT="\begin{Eqts}
\int_{\partial S} f dx + g dy = \sum_i\int_{\partial P_i} f dx + g ...
... + g dy = \sum_i\int_{P_i} \left( \PD{g}{x} - \PD{f}{y} \right) dx dy
\end{Eqts}">|; 

$key = q/{Eqts}lim_{Nrightarrow+infty}E_N=lim_{hrightarrow0}E_N=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.98ex; vertical-align: -1.42ex; " SRC="|."$dir".q|img117.svg"
 ALT="\begin{Eqts}
\lim_{N \rightarrow +\infty} E_N = \lim_{h \rightarrow 0} E_N = 0
\end{Eqts}">|; 

$key = q/{Eqts}lim_{Nrightarrow+infty}left(1+frac{T}{N}right)^N=exp(T){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.19ex; vertical-align: -2.53ex; " SRC="|."$dir".q|img121.svg"
 ALT="\begin{Eqts}
\lim_{N \rightarrow +\infty} \left( 1 + \frac{T}{N} \right)^N = \exp(T)
\end{Eqts}">|; 

$key = q/{Eqts}mu=frac{h^2}{2}+frac{h^3}{6}+...{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -2.02ex; " SRC="|."$dir".q|img105.svg"
 ALT="\begin{Eqts}
\mu = \frac{h^2}{2} + \frac{h^3}{6} + ...
\end{Eqts}">|; 

$key = q/{Eqts}partial^kexp(0)=partial^{k-1}exp(0)=...=exp(0)=1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.06ex; vertical-align: -0.95ex; " SRC="|."$dir".q|img95.svg"
 ALT="\begin{Eqts}
\partial^k \exp(0) = \partial^{k-1} \exp(0) = ... = \exp(0) = 1
\end{Eqts}">|; 

$key = q/{Eqts}setR^2={(x,y):x,yinsetR}={(r,theta):rin[0,+infty),quadthetain[0,2pi)}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img129.svg"
 ALT="\begin{Eqts}
\setR^2 = \{ (x,y) : x,y \in \setR \} =
\{ (r,\theta) : r \in [0,+\infty),\quad \theta \in [0,2\pi) \}
\end{Eqts}">|; 

$key = q/{Eqts}sum_{k=0}^{N-1}(1+h)^k=frac{(1+h)^N-1}{(1+h)-1}=unsur{h}cdot(sum_{k=0}^Nbinome{N}{k}cdoth^k-1){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.33ex; vertical-align: -3.10ex; " SRC="|."$dir".q|img80.svg"
 ALT="\begin{Eqts}
\sum_{k = 0}^{N - 1} (1 + h)^k = \frac{(1 + h)^N - 1}{(1 + h) - 1} = \unsur{h} \cdot (\sum_{k = 0}^N \binome{N}{k} \cdot h^k - 1)
\end{Eqts}">|; 

$key = q/{Eqts}sum_{k=0}^{N-1}(1+h)^k=unsur{h}cdot(1+sum_{k=1}^Nbinome{N}{k}cdoth^k-1)=sum_{k=1}^Nbinome{N}{k}cdoth^k{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.33ex; vertical-align: -3.10ex; " SRC="|."$dir".q|img82.svg"
 ALT="\begin{Eqts}
\sum_{k = 0}^{N - 1} (1 + h)^k = \unsur{h} \cdot (1 + \sum_{k = 1}^N \binome{N}{k} \cdot h^k - 1) = \sum_{k = 1}^N \binome{N}{k} \cdot h^k
\end{Eqts}">|; 

$key = q/{Eqts}u_i=u(ih){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.92ex; " SRC="|."$dir".q|img91.svg"
 ALT="\begin{Eqts}
u_i = u(i h)
\end{Eqts}">|; 

$key = q/{Eqts}vectorisationA=Matrix{{c}a_1vdotsa_nMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.91ex; vertical-align: -4.39ex; " SRC="|."$dir".q|img151.svg"
 ALT="\begin{Eqts}
\vectorisation A =
\begin{Matrix}{c}
a_1 \ \vdots \ a_n
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}y_{i}=expleft[ihright]y_{i}=exp(h)cdotexpleft[(i-1)hright]y_{i}=exp(h)y_{i-1}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img96.svg"
 ALT="\begin{Eqts}
y_{i} = \exp\left[ i h\right] \\\\
y_{i} = \exp(h) \cdot \exp\left[ (i-1) h\right] \\\\
y_{i} = \exp(h) y_{i-1} \\\\
\end{Eqts}">|; 

$key = q/{eqnarraystar}f(x)=f(x_N)&=&f(x_{N-1})cdot(1+h)+e_N&=&f(x_{N-2})cdot(1+h)^2+e_{N-1}cdot(1+h)+e_Nvdots&=&f(0)cdot(1+h)^N+E{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 19.22ex; " SRC="|."$dir".q|img77.svg"
 ALT="\begin{eqnarray*}
f(x) = f(x_N) &amp;=&amp; f(x_{N - 1}) \cdot (1 + h) + e_N \\\\
&amp;=&amp; f(x...
... \cdot (1 + h) + e_N \\\\
\vdots \\\\
&amp;=&amp; f(0) \cdot (1 + h)^N + E
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}lambda&=&sup{a,b}mu&=&sup{b,c}gamma&=&sup{a,mu}varkappa&=&sup{lambda,c}sigma&=&sup{lambda,mu}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 21.70ex; " SRC="|."$dir".q|img2.svg"
 ALT="\begin{eqnarray*}
\lambda &amp;=&amp; \sup\{a,b\} \\\\
\mu &amp;=&amp; \sup\{b,c\} \\\\
\gamma &amp;=&amp;...
...arkappa &amp;=&amp; \sup\{\lambda,c\} \\\\
\sigma &amp;=&amp; \sup\{\lambda,\mu\}
\end{eqnarray*}">|; 

1;

