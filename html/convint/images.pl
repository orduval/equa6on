# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img49.svg"
 ALT="\begin{eqnarray*}
W_n^i &amp;=&amp; C_n \cap \Psi_i = \{ x \in \Psi_i : \alpha \cdot w(x...
...si_i = \{ x \in \Psi_i : u_n(x) \strictsuperieur u_{n + 1}(x) \}
\end{eqnarray*}">|; 

$key = q/0leu_n(x)les(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$0 \le u_n(x) \le s(x) = 0$">|; 

$key = q/0strictinferieuralphastrictinferieur1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img47.svg"
 ALT="$0 \strictinferieur \alpha \strictinferieur 1$">|; 

$key = q/0strictsuperieuralphastrictsuperieur1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img97.svg"
 ALT="$0 \strictsuperieur \alpha \strictsuperieur 1$">|; 

$key = q/1slashalpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img92.svg"
 ALT="$1/\alpha$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$A$">|; 

$key = q/C_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img86.svg"
 ALT="$C_n$">|; 

$key = q/C_nsubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img84.svg"
 ALT="$C_n \subseteq A$">|; 

$key = q/I_mleI_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img21.svg"
 ALT="$I_m \le I_n$">|; 

$key = q/I_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img26.svg"
 ALT="$I_n$">|; 

$key = q/I_nleS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img25.svg"
 ALT="$I_n \le S$">|; 

$key = q/KinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img74.svg"
 ALT="$K \in \setN$">|; 

$key = q/LleS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img99.svg"
 ALT="$L \le S$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\Omega$">|; 

$key = q/Phi=AsetminusZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\Phi = A \setminus Z$">|; 

$key = q/Psi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\Psi$">|; 

$key = q/Psi=PhisetminusN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\Psi = \Phi \setminus N$">|; 

$key = q/Psi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\Psi_i$">|; 

$key = q/Psi_i=A_icapPsi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\Psi_i = A_i \cap \Psi$">|; 

$key = q/Psi_isubseteqPsi=PhisetminusN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\Psi_i \subseteq \Psi = \Phi \setminus N$">|; 

$key = q/Psi_isubseteqPsisubseteqPhi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\Psi_i \subseteq \Psi \subseteq \Phi$">|; 

$key = q/PsisubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\Psi \subseteq A$">|; 

$key = q/S=L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img100.svg"
 ALT="$S = L$">|; 

$key = q/SleL;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img98.svg"
 ALT="$S \le L$">|; 

$key = q/U_n=int_Au_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.86ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img111.svg"
 ALT="$U_n = \int_A u_n$">|; 

$key = q/U_nleF_n=int_Af_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.86ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img115.svg"
 ALT="$U_n \le F_n = \int_A f_n$">|; 

$key = q/W_n^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img81.svg"
 ALT="$W_n^i$">|; 

$key = q/W_n^i=C_ncapPsi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img85.svg"
 ALT="$W_n^i = C_n \cap \Psi_i$">|; 

$key = q/W_n^isetminusX_n^isubseteqW_{n+1}^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.79ex; vertical-align: -0.83ex; " SRC="|."$dir".q|img55.svg"
 ALT="$W_n^i \setminus X_n^i \subseteq W_{n + 1}^i$">|; 

$key = q/W_n^isubseteqPsi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img57.svg"
 ALT="$W_n^i \subseteq \Psi_i$">|; 

$key = q/X_n^isubseteqZ_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img50.svg"
 ALT="$X_n^i \subseteq Z_n$">|; 

$key = q/abs{f(x)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\abs{f(x)}$">|; 

$key = q/abs{f(x)}levarphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\abs{f(x)} \le \varphi(x)$">|; 

$key = q/abs{f_n}levarphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\abs{f_n} \le \varphi$">|; 

$key = q/abs{f}levarphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\abs{f} \le \varphi$">|; 

$key = q/abs{u_n(x)-s(x)}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\abs{u_n(x) - s(x)} \le \delta$">|; 

$key = q/alpha;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\alpha$">|; 

$key = q/alphacdotw(x)le0=u_n(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\alpha \cdot w(x) \le 0 = u_n(x)$">|; 

$key = q/alphacdotw(x)lealphacdots(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\alpha \cdot w(x) \le \alpha \cdot s(x)$">|; 

$key = q/alphacdotwleu_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.01ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\alpha \cdot w \le u_n$">|; 

$key = q/alphastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\alpha \strictsuperieur 0$">|; 

$key = q/displaystyleC_n={xinPsi:alphacdotw(x)leu_n(x)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle C_n = \{ x \in \Psi : \alpha \cdot w(x) \le u_n(x) \}$">|; 

$key = q/displaystyleI_n=int_Au_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle I_n = \int_A u_n(x)  d\mu(x)$">|; 

$key = q/displaystyleL=lim_{ntoinfty}I_nleS=int_As(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle L = \lim_{n \to \infty} I_n \le S = \int_A s(x)  d\mu(x)$">|; 

$key = q/displaystyleN={xinA:w(x)strictsuperieurs(x)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle N = \{ x \in A : w(x) \strictsuperieur s(x) \}$">|; 

$key = q/displaystyleS-epsilon=int_As(x)dmu(x)-epsilonlefrac{L}{alpha};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle S - \epsilon = \int_A s(x)  d\mu(x) - \epsilon \le \frac{L}{\alpha}$">|; 

$key = q/displaystyleS=int_As(x)dmu(x)strictinferieur+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle S = \int_A s(x)  d\mu(x) \strictinferieur +\infty$">|; 

$key = q/displaystyleZ=bigcup_nZ_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.52ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle Z = \bigcup_n Z_n$">|; 

$key = q/displaystyleZ_n={xinA:u_n(x)strictsuperieuru_{n+1}(x)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle Z_n = \{ x \in A : u_n(x) \strictsuperieur u_{n + 1}(x) \}$">|; 

$key = q/displaystylealphacdotw(x)leu_n(x)leu_{n+1}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \alpha \cdot w(x) \le u_n(x) \le u_{n + 1}(x)$">|; 

$key = q/displaystylebigcup_nW_n^i=Psi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.52ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle \bigcup_n W_n^i = \Psi_i$">|; 

$key = q/displaystyledelta=(1-alpha)cdots(x)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \delta = (1 - \alpha) \cdot s(x) \strictsuperieur 0$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=lim_{ntoinfty}int_Af_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) = \lim_{n \to \infty} \int_A f_n(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)gelimsup_{ntoinfty}int_Af_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img144.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) \ge \limsup_{n \to \infty} \int_A f_n(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Af(x)dmu(x)leliminf_{ntoinfty}int_Af_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\displaystyle \int_A f(x)  d\mu(x) \le \liminf_{n \to \infty} \int_A f_n(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Alim_{ntoinfty}f_n(x)dmu(x)=lim_{ntoinfty}int_Af_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\displaystyle \int_A \lim_{n \to \infty} f_n(x)  d\mu(x) = \lim_{n \to \infty} \int_A f_n(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Alim_{ntoinfty}u_n(x)dmu(x)=lim_{ntoinfty}int_Au_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle \int_A \lim_{n \to \infty} u_n(x)  d\mu(x) = \lim_{n \to \infty} \int_A u_n(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Aliminf_n[varphi-f_n]=int_Avarphi-int_Af;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\displaystyle \int_A \liminf_n [\varphi - f_n] = \int_A \varphi - \int_A f$">|; 

$key = q/displaystyleint_Aliminf_{ntoinfty}[varphi(x)+f_n(x)]dmu(x)leliminf_{ntoinfty}int_A[varphi(x)+f_n(x)]dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img146.svg"
 ALT="$\displaystyle \int_A \liminf_{n \to \infty} [\varphi(x) + f_n(x)]  d\mu(x) \le \liminf_{n \to \infty} \int_A [\varphi(x) + f_n(x)]  d\mu(x)$">|; 

$key = q/displaystyleint_Aliminf_{ntoinfty}[varphi(x)-f_n(x)]dmu(x)leliminf_{ntoinfty}int_A[varphi(x)-f_n(x)]dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle \int_A \liminf_{n \to \infty} [\varphi(x) - f_n(x)]  d\mu(x) \le \liminf_{n \to \infty} \int_A [\varphi(x) - f_n(x)]  d\mu(x)$">|; 

$key = q/displaystyleint_Aliminf_{ntoinfty}f_n(x)dmu(x)=lim_{ntoinfty}int_Au_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle \int_A \liminf_{n \to \infty} f_n(x)  d\mu(x) = \lim_{n \to \infty} \int_A u_n(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Aliminf_{ntoinfty}f_n(x)dmu(x)=liminf_{ntoinfty}int_Au_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle \int_A \liminf_{n \to \infty} f_n(x)  d\mu(x) = \liminf_{n \to \infty} \int_A u_n(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Aliminf_{ntoinfty}f_n(x)dmu(x)leliminf_{ntoinfty}int_Af_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\displaystyle \int_A \liminf_{n \to \infty} f_n(x)  d\mu(x) \le \liminf_{n \to \infty} \int_A f_n(x)  d\mu(x)$">|; 

$key = q/displaystyleint_As(x)dmu(x)-epsilonleint_Aw(x)dmu(x)lefrac{L}{alpha};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle \int_A s(x)  d\mu(x) - \epsilon \le \int_A w(x)  d\mu(x) \le \frac{L}{\alpha}$">|; 

$key = q/displaystyleint_As(x)dmu(x)leint_Aw(x)dmu(x)+epsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \int_A s(x)  d\mu(x) \le \int_A w(x)  d\mu(x) + \epsilon$">|; 

$key = q/displaystyleint_Asum_{i=0}^{+infty}f_i(x)dmu(x)=lim_{ntoinfty}int_Asum_{i=0}^nf_i(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle \int_A \sum_{i = 0}^{+\infty} f_i(x)  d\mu(x) = \lim_{n \to \infty} \int_A \sum_{i = 0}^n f_i(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Avarphi-int_Afleint_Avarphi-limsup_nint_Af_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle \int_A \varphi - \int_A f \le \int_A \varphi - \limsup_n \int_A f_n$">|; 

$key = q/displaystyleint_Aw(x)dmu(x)=sum_iw_icdotmu(Psi_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.25ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \int_A w(x)  d\mu(x) = \sum_i w_i \cdot \mu(\Psi_i)$">|; 

$key = q/displaystyleint_Aw(x)dmu(x)leunsur{alpha}lim_{ntoinfty}I_n=frac{L}{alpha};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle \int_A w(x)  d\mu(x) \le \unsur{\alpha} \lim_{n \to \infty} I_n = \frac{L}{\alpha}$">|; 

$key = q/displaystyleint_Omegaf(x)dmu(x)=lim_{ntoinfty}int_Omegaf_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img155.svg"
 ALT="$\displaystyle \int_\Omega f(x)  d\mu(x) = \lim_{n \to \infty} \int_\Omega f_n(x)  d\mu(x)$">|; 

$key = q/displaystyleint_Psiw(x)dmu(x)=int_Aw(x)cdotindicatrice_Psi(x)dmu(x)=sum_iw_icdotmu(Psi_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.25ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \int_\Psi w(x)  d\mu(x) = \int_A w(x) \cdot \indicatrice_\Psi(x)  d\mu(x) = \sum_i w_i \cdot \mu(\Psi_i)$">|; 

$key = q/displaystyleint_Psiw(x)dmu(x)=int_Phiw(x)dmu(x)=int_Aw(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \int_\Psi w(x)  d\mu(x) = \int_\Phi w(x)  d\mu(x) = \int_A w(x)  d\mu(x)$">|; 

$key = q/displaystyleint_{C_n}alphacdotw(x)dmu(x)=alphaint_{C_n}w(x)dmu(x)leint_Au_n(x)dmu(x)=I_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.63ex; vertical-align: -2.39ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \int_{C_n} \alpha \cdot w(x)  d\mu(x) = \alpha \int_{C_n} w(x)  d\mu(x) \le \int_A u_n(x)  d\mu(x) = I_n$">|; 

$key = q/displaystyleint_{C_n}w(x)dmu(x)=int_Aw(x)cdotindicatrice_{C_n}(x)dmu(x)=sum_iw_icdotmu(W_n^i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.25ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\displaystyle \int_{C_n} w(x)  d\mu(x) = \int_A w(x) \cdot \indicatrice_{C_n}(x)  d\mu(x) = \sum_i w_i \cdot \mu(W_n^i)$">|; 

$key = q/displaystylelim_{ntoinfty}f_n(x)=f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\displaystyle \lim_{n \to \infty} f_n(x) = f(x)$">|; 

$key = q/displaystylelim_{ntoinfty}int_Af_n(x)dmu(x)=limsup_{ntoinfty}int_Af_n(x)dmu(x)=liminf_{ntoinfty}int_Af_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle \lim_{n \to \infty} \int_A f_n(x)  d\mu(x) = \limsup_{n \to \infty} \int_A f_n(x)  d\mu(x) = \liminf_{n \to \infty} \int_A f_n(x)  d\mu(x)$">|; 

$key = q/displaystylelim_{ntoinfty}int_Af_n(x)dmu(x)leint_Af(x)dmu(x)lelim_{ntoinfty}int_Af_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\displaystyle \lim_{n \to \infty} \int_A f_n(x)  d\mu(x) \le \int_A f(x)  d\mu(x) \le \lim_{n \to \infty} \int_A f_n(x)  d\mu(x)$">|; 

$key = q/displaystylelim_{ntoinfty}mu(W_n^i)=mu(Psi_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.79ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle \lim_{n \to \infty} \mu(W_n^i) = \mu(\Psi_i)$">|; 

$key = q/displaystylelim_{ntoinfty}mu(W_n^i)=muleft(bigcup_nW_n^iright);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \lim_{n \to \infty} \mu(W_n^i) = \mu\left( \bigcup_n W_n^i \right)$">|; 

$key = q/displaystylelim_{ntoinfty}u_n(x)=s(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \lim_{n \to \infty} u_n(x) = s(x)$">|; 

$key = q/displaystylelim_{ntoinfty}u_n(x)=sum_{i=0}^{+infty}f_i(x)=s(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle \lim_{n \to \infty} u_n(x) = \sum_{i = 0}^{+\infty} f_i(x) = s(x)$">|; 

$key = q/displaystyleliminf_n(-f_n)=-limsup_nf_n=-lim_nf_n=-f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.00ex; vertical-align: -2.17ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle \liminf_n (-f_n) = - \limsup_n f_n = - \lim_n f_n = -f$">|; 

$key = q/displaystyleliminf_nint_A[varphi-f_n]=int_Avarphi+liminf_nleft[-int_Af_nright]=int_Avarphi-limsup_nint_Af_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle \liminf_n \int_A [\varphi - f_n] = \int_A \varphi + \liminf_n \left[ - \int_A f_n \right] = \int_A \varphi - \limsup_n \int_A f_n$">|; 

$key = q/displaystyleliminf_{ntoinfty}int_Au_n(x)dmu(x)leliminf_{ntoinfty}int_Af_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\displaystyle \liminf_{n \to \infty} \int_A u_n(x)  d\mu(x) \le \liminf_{n \to \infty} \int_A f_n(x)  d\mu(x)$">|; 

$key = q/displaystylelimsup_{ntoinfty}int_Af_n(x)dmu(x)=liminf_{ntoinfty}int_Af_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img150.svg"
 ALT="$\displaystyle \limsup_{n \to \infty} \int_A f_n(x)  d\mu(x) = \liminf_{n \to \infty} \int_A f_n(x)  d\mu(x)$">|; 

$key = q/displaystylelimsup_{ntoinfty}int_Af_n(x)dmu(x)leliminf_{ntoinfty}int_Af_n(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img148.svg"
 ALT="$\displaystyle \limsup_{n \to \infty} \int_A f_n(x)  d\mu(x) \le \liminf_{n \to \infty} \int_A f_n(x)  d\mu(x)$">|; 

$key = q/displaystyles=sup{u_n:ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle s = \sup \{ u_n : n \in \setN\}$">|; 

$key = q/displaystyleu_0(x)leu_1(x)leu_2(x)le...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle u_0(x) \le u_1(x) \le u_2(x) \le ...$">|; 

$key = q/displaystyleu_0essinferieuru_1essinferieuru_2essinferieuru_3essinferieur...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle u_0 \essinferieur u_1 \essinferieur u_2 \essinferieur u_3 \essinferieur ...$">|; 

$key = q/displaystyleu_n(x)ges(x)-delta=s(x)-(1-alpha)cdots(x)=alphacdots(x)gealphacdotw(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle u_n(x) \ge s(x) - \delta = s(x) - (1 - \alpha) \cdot s(x) = \alpha \cdot s(x) \ge \alpha \cdot w(x)$">|; 

$key = q/displaystyleu_n=inf{f_m:minsetN,mgen};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\displaystyle u_n = \inf \{ f_m : m \in \setN,  m \ge n \}$">|; 

$key = q/displaystyleu_n=sum_{i=0}^nf_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle u_n = \sum_{i = 0}^n f_i$">|; 

$key = q/displaystylew=sum_iw_icdotindicatrice[A_i];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle w = \sum_i w_i \cdot \indicatrice[A_i]$">|; 

$key = q/displaystylewcdotindicatrice[C_n]=sum_iw_icdotindicatrice[C_n]cdotindicatrice[Psi_i]=sum_iw_icdotindicatrice[W_n^i];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle w \cdot \indicatrice[C_n] = \sum_i w_i \cdot \indicatrice[C_n] \cdot \indicatrice[\Psi_i] = \sum_i w_i \cdot \indicatrice[W_n^i]$">|; 

$key = q/displaystylewcdotindicatrice[Psi]=sum_iw_icdotindicatrice[A_i]cdotindicatrice[Psi]=sum_iw_icdotindicatrice[Psi_i];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle w \cdot \indicatrice[\Psi] = \sum_i w_i \cdot \indicatrice[A_i] \cdot \indicatrice[\Psi] = \sum_i w_i \cdot \indicatrice[\Psi_i]$">|; 

$key = q/displaystyle{u_ninsetR^A:ninsetR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\displaystyle \{ u_n \in \setR^A : n \in \setR \}$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/f:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img121.svg"
 ALT="$f : A \mapsto \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img131.svg"
 ALT="$f$">|; 

$key = q/f_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img137.svg"
 ALT="$f_n$">|; 

$key = q/inf(-X)=-sup(X);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\inf(-X) = -\sup(X)$">|; 

$key = q/int_Aabs{f}leint_Avarphistrictinferieur+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.86ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\int_A \abs{f} \le \int_A \varphi \strictinferieur +\infty$">|; 

$key = q/int_Alim_nu_n=lim_nint_Au_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.86ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\int_A \lim_n u_n = \lim_n \int_A u_n$">|; 

$key = q/liminf(X)lelimsup(X);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img149.svg"
 ALT="$\liminf(X) \le \limsup(X)$">|; 

$key = q/liminf_nU_n=lim_nU_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\liminf_n U_n = \lim_n U_n$">|; 

$key = q/liminf_nU_nleliminf_nF_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\liminf_n U_n \le \liminf_n F_n$">|; 

$key = q/liminf_nf_n=limsup_nf_n=limf_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.35ex; vertical-align: -0.65ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\liminf_n f_n = \limsup_n f_n = \lim f_n$">|; 

$key = q/m,ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$m,n \in \setN$">|; 

$key = q/mlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img23.svg"
 ALT="$m \le n$">|; 

$key = q/mu(X_n^i)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\mu(X_n^i) = 0$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$n$">|; 

$key = q/ngeK;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img76.svg"
 ALT="$n \ge K$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img3.svg"
 ALT="$n \in \setN$">|; 

$key = q/omega_n=varphi+f_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img145.svg"
 ALT="$\omega_n = \varphi + f_n$">|; 

$key = q/psi_n=varphi-f_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\psi_n = \varphi - f_n$">|; 

$key = q/s(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$s(x)$">|; 

$key = q/s(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$s(x) = 0$">|; 

$key = q/s(x)=sup_nu_n(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$s(x) = \sup_n u_n(x)$">|; 

$key = q/s(x)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$s(x) \strictsuperieur 0$">|; 

$key = q/s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img94.svg"
 ALT="$s$">|; 

$key = q/s=sup_nu_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.65ex; " SRC="|."$dir".q|img108.svg"
 ALT="$s = \sup_n u_n$">|; 

$key = q/u_n(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$u_n(x) = 0$">|; 

$key = q/u_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img19.svg"
 ALT="$u_n$">|; 

$key = q/u_nessinferieuru_{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img7.svg"
 ALT="$u_n \essinferieur u_{n + 1}$">|; 

$key = q/u_nesssuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img2.svg"
 ALT="$u_n \esssuperieur 0$">|; 

$key = q/u_nlef_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img114.svg"
 ALT="$u_n \le f_n$">|; 

$key = q/u_nles;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.01ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img24.svg"
 ALT="$u_n \le s$">|; 

$key = q/varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\varphi(x)$">|; 

$key = q/varphi-max{f_n,-f_n}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\varphi - \max \{ f_n , -f_n \} \ge 0$">|; 

$key = q/varphi:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\varphi : A \mapsto \setR$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\varphi$">|; 

$key = q/w(x)les(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$w(x) \le s(x)$">|; 

$key = q/w(x)les(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$w(x) \le s(x) = 0$">|; 

$key = q/w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$w$">|; 

$key = q/w_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img36.svg"
 ALT="$w_i$">|; 

$key = q/wessinferieurs;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img31.svg"
 ALT="$w \essinferieur s$">|; 

$key = q/winmathcal{E}_A(s);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$w \in \mathcal{E}_A(s)$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img80.svg"
 ALT="$x$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img105.svg"
 ALT="$x \in A$">|; 

$key = q/xinC_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img78.svg"
 ALT="$x \in C_n$">|; 

$key = q/xinC_ncapPsi_i=W_n^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img79.svg"
 ALT="$x \in C_n \cap \Psi_i = W_n^i$">|; 

$key = q/xinPhi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$x \in \Phi$">|; 

$key = q/xinPsi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img58.svg"
 ALT="$x \in \Psi_i$">|; 

$key = q/xinW_n^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img64.svg"
 ALT="$x \in W_n^i$">|; 

$key = q/xinW_n^isetminusX_n^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$x \in W_n^i \setminus X_n^i$">|; 

$key = q/xinW_{n+1}^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.79ex; vertical-align: -0.83ex; " SRC="|."$dir".q|img54.svg"
 ALT="$x \in W_{n + 1}^i$">|; 

$key = q/xnotinN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img67.svg"
 ALT="$x \notin N$">|; 

$key = q/{A_1,...,A_N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\{A_1,...,A_N\}$">|; 

$key = q/{Eqts}varphi-f_nge0varphi+f_nge0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img134.svg"
 ALT="\begin{Eqts}
\varphi - f_n \ge 0 \\\\
\varphi + f_n \ge 0
\end{Eqts}">|; 

$key = q/{I_n:ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\{ I_n : n \in \setN \}$">|; 

$key = q/{abs{f_n(x)}:ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\{ \abs{f_n(x)} : n \in \setN \}$">|; 

$key = q/{eqnarraystar}lim_{ntoinfty}int_{C_n}w(x)dmu(x)&=&sum_iw_icdotlim_{ntoinfty}mu(W_n^i)&=&sum_iw_icdotmu(Psi_i)&=&int_Aw(x)dmu(x){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 23.20ex; " SRC="|."$dir".q|img89.svg"
 ALT="\begin{eqnarray*}
\lim_{n \to \infty} \int_{C_n} w(x)  d\mu(x) &amp;=&amp; \sum_i w_i \...
...
&amp;=&amp; \sum_i w_i \cdot \mu(\Psi_i) \\\\
&amp;=&amp; \int_A w(x)  d\mu(x)
\end{eqnarray*}">|; 

$key = q/{f_n:ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\{ f_n : n \in \setN \}$">|; 

$key = q/{f_ninsetR^A:ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\{ f_n \in \setR^A : n \in \setN \}$">|; 

$key = q/{u_n(x):ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\{ u_n(x) : n \in \setN \}$">|; 

1;

