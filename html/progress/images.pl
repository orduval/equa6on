# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img28.svg"
 ALT="$-1$">|; 

$key = q/0lejlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$0 \le j \le n$">|; 

$key = q/2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img14.svg"
 ALT="$2$">|; 

$key = q/G_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img21.svg"
 ALT="$G_n$">|; 

$key = q/a,bincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$a,b \in \corps$">|; 

$key = q/a=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img38.svg"
 ALT="$a = 0$">|; 

$key = q/a=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$a = 1$">|; 

$key = q/a^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img36.svg"
 ALT="$a^n$">|; 

$key = q/a^{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$a^{n + 1}$">|; 

$key = q/acdotG_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img25.svg"
 ALT="$a \cdot G_n$">|; 

$key = q/aincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img16.svg"
 ALT="$a \in \corps$">|; 

$key = q/ane0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img31.svg"
 ALT="$a \ne 0$">|; 

$key = q/ane1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img20.svg"
 ALT="$a \ne 1$">|; 

$key = q/displaystyle(1-a)cdotG_n=1-a^{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle (1 - a) \cdot G_n = 1 - a^{n + 1}$">|; 

$key = q/displaystyle(1-r)cdotsum_{i=0}^nr^i=1-r^{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle (1 - r) \cdot \sum_{i = 0}^n r^i = 1 - r^{n + 1}$">|; 

$key = q/displaystyle(a-b)cdota^ncdotsum_{i=0}^nfrac{b^i}{a^i}=a^{n+1}-b^{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle (a - b) \cdot a^n \cdot \sum_{i = 0}^n \frac{b^i}{a^i} = a^{n + 1} - b^{n + 1}$">|; 

$key = q/displaystyle(n-0)+(n-1)+...+(n-n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle (n - 0) + (n - 1) + ... + (n - n)$">|; 

$key = q/displaystyle1+acdot(1+a+a^2+...+a^n)-a^{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle 1 + a \cdot (1 + a + a^2 + ... + a^n) - a^{n+1}$">|; 

$key = q/displaystyle2S_n=ncdot(n+1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle 2 S_n = n \cdot (n + 1)$">|; 

$key = q/displaystyleC_n=sum_{i=0}^ni^2=1+4+16+...+n^2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle C_n = \sum_{i = 0}^n i^2 = 1 + 4 + 16 + ... + n^2$">|; 

$key = q/displaystyleG_n=1+acdotG_n-a^{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.49ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle G_n = 1 + a \cdot G_n - a^{n + 1}$">|; 

$key = q/displaystyleG_n=sum_{i=0}^na^i=1+...+1=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle G_n = \sum_{i=0}^n a^i = 1 + ... + 1 = n$">|; 

$key = q/displaystyleG_n=sum_{i=0}^na^i=1+a+a^2+...+a^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle G_n = \sum_{i=0}^n a^i = 1 + a + a^2 + ... + a^n$">|; 

$key = q/displaystyleS_n=ncdot(n+1)-S_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle S_n = n \cdot (n + 1) - S_n$">|; 

$key = q/displaystyleS_n=sum_{i=0}^ni=0+1+2+...+n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle S_n = \sum_{i = 0}^n i = 0 + 1 + 2 + ... + n$">|; 

$key = q/displaystyleS_n=sum_{j=0}^n(n-j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle S_n = \sum_{j = 0}^n (n-j)$">|; 

$key = q/displaystylea^{n+1}-b^{n+1}=(a-b)cdotsum_{i=0}^na^icdotb^{n-i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle a^{n + 1} - b^{n + 1} = (a - b) \cdot \sum_{i = 0}^n a^i \cdot b^{n - i}$">|; 

$key = q/displaystylea^{n+1}-b^{n+1}=(a-b)cdotsum_{i=0}^na^{n-i}cdotb^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle a^{n + 1} - b^{n + 1} = (a - b) \cdot \sum_{i = 0}^n a^{n - i} \cdot b^i$">|; 

$key = q/displaystylea^{n+1}-b^{n+1}=-b^{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.34ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle a^{n + 1} - b^{n + 1} = - b^{n + 1}$">|; 

$key = q/displaystylefrac{3}{2}cdotC_n=unsur{4}cdotBig[2cdotncdot(n+1)^2-ncdot(n+1)Big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \frac{3}{2} \cdot C_n = \unsur{4} \cdot \Big[ 2 \cdot n \cdot (n + 1)^2 - n \cdot (n + 1) \Big]$">|; 

$key = q/displaystyler=frac{b}{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle r = \frac{b}{a}$">|; 

$key = q/displaystylesum_{i=0}^na^i=frac{1-a^{n+1}}{1-a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \sum_{i = 0}^n a^i = \frac{ 1 - a^{n + 1} }{ 1 - a }$">|; 

$key = q/displaystylesum_{i=0}^na^i=frac{a^{n+1}-1}{a-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \sum_{i = 0}^n a^i = \frac{a^{n + 1} - 1}{a - 1}$">|; 

$key = q/displaystylesum_{i=0}^na^{n-i}cdotb^i=a^n+a^{n-1}cdotb+...+acdotb^{n-1}+b^n=sum_{i=0}^na^icdotb^{n-i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \sum_{i = 0}^n a^{n - i} \cdot b^i = a^n + a^{n - 1} \cdot b + ... + a \cdot b^{n - 1} + b^n = \sum_{i = 0}^n a^i \cdot b^{n - i}$">|; 

$key = q/displaystylesum_{i=0}^ni^2=frac{(2cdotn^2+n)cdot(n+1)}{6};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \sum_{i = 0}^n i^2 = \frac{ (2 \cdot n^2 + n) \cdot (n + 1) }{ 6 }$">|; 

$key = q/displaystylesum_{i=0}^nsum_{j=0}^ij=sum_{j=0}^nsum_{i=j}^nj;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.55ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \sum_{i = 0}^n \sum_{j = 0}^i j = \sum_{j = 0}^n \sum_{i = j}^n j$">|; 

$key = q/displaystylesum_{i=0}^nunsur{2}cdoticdot(i+1)=sum_{i=0}^nsum_{j=0}^ij;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.55ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \sum_{i = 0}^n \unsur{2} \cdot i \cdot (i+1) = \sum_{i = 0}^n \sum_{j = 0}^i j$">|; 

$key = q/displaystylesum_{i=0}^nunsur{2}cdoticdot(i+1)=unsur{2}sum_{i=0}^n(i^2+i)=unsur{2}cdotC_n+unsur{4}cdotncdot(n+1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \sum_{i = 0}^n \unsur{2} \cdot i \cdot (i+1) = \unsur{2} \sum_{i = 0}^n (i^2 + i) = \unsur{2} \cdot C_n + \unsur{4} \cdot n \cdot (n + 1)$">|; 

$key = q/displaystylesum_{i=0}^{n}i=frac{ncdot(n+1)}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \sum_{i = 0}^{n} i = \frac{ n \cdot (n + 1) }{ 2 }$">|; 

$key = q/displaystylesum_{j=0}^n(n-j)=sum_{j=0}^{n}n-sum_{j=0}^{n}j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \sum_{j = 0}^n (n-j) = \sum_{j = 0}^{n} n - \sum_{j = 0}^{n} j$">|; 

$key = q/displaystylesum_{j=0}^nj=S_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \sum_{j = 0}^n j = S_n$">|; 

$key = q/displaystylesum_{j=0}^nn=ncdotsum_{j=0}^{n}1=ncdot(n+1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \sum_{j = 0}^n n = n \cdot \sum_{j = 0}^{n} 1 = n \cdot (n + 1)$">|; 

$key = q/displaystylesum_{j=0}^nsum_{i=j}^nj=sum_{j=0}^njsum_{i=j}^n1=sum_{j=0}^njcdot(n-j+1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \sum_{j = 0}^n \sum_{i = j}^n j = \sum_{j = 0}^n j \sum_{i = j}^n 1 = \sum_{j = 0}^n j \cdot (n - j + 1)$">|; 

$key = q/displaystyleunsur{2}cdoticdot(i+1)=sum_{j=0}^ij;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.55ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \unsur{2} \cdot i \cdot (i+1) = \sum_{j=0}^i j$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img48.svg"
 ALT="$i$">|; 

$key = q/i=n-j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$i = n - j$">|; 

$key = q/j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$j$">|; 

$key = q/j=n-i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$j = n -i$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img1.svg"
 ALT="$n \in \setN$">|; 

$key = q/{eqnarraystar}(a-b)cdotsum_{i=0}^na^{n-i}cdotb^i&=&-bcdot(0^n+0^{n-1}cdotb+...+0cdotb^{n-1}+b^n)&=&-bcdotb^n=-b^{n+1}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.63ex; " SRC="|."$dir".q|img40.svg"
 ALT="\begin{eqnarray*}
(a - b) \cdot \sum_{i = 0}^n a^{n - i} \cdot b^i &amp;=&amp; -b \cdot ...
..... + 0 \cdot b^{n - 1} + b^n) \\\\
&amp;=&amp; -b \cdot b^n = - b^{n + 1}
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}6cdotC_n&=&(2cdotncdot(n+1)-n)cdot(n+1)&=&(2cdotn^2+n)cdot(n+1){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img53.svg"
 ALT="\begin{eqnarray*}
6 \cdot C_n &amp;=&amp; ( 2 \cdot n \cdot (n + 1) - n ) \cdot (n + 1) \\\\
&amp;=&amp; ( 2 \cdot n^2 + n ) \cdot (n + 1)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}G_n&=&1+sum_{i=1}^na^i&=&1+acdotsum_{i=0}^{n-1}a^i&=&1+acdotsum_{i=0}^na^i-a^{n+1}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 26.99ex; " SRC="|."$dir".q|img23.svg"
 ALT="\begin{eqnarray*}
G_n &amp;=&amp; 1 + \sum_{i = 1}^n a^i \\\\
&amp;=&amp; 1 + a \cdot \sum_{i = 0}^{n - 1} a^i \\\\
&amp;=&amp; 1 + a \cdot \sum_{i = 0}^n a^i - a^{n + 1}
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}a^{2}-b^{2}&=&(a-b)cdot(a+b)a^{3}-b^{3}&=&(a-b)cdot(a^2+acdotb+b^2){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img41.svg"
 ALT="\begin{eqnarray*}
a^{2} - b^{2} &amp;=&amp; (a - b) \cdot (a + b) \\\\
a^{3} - b^{3} &amp;=&amp; (a - b) \cdot (a^2 + a \cdot b + b^2)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}sum_{j=0}^njcdot(n-j+1)&=&(n+1)cdotsum_{j=0}^nj-sum_{j=0}^nj^2&=&unsur{2}cdotncdot(n+1)^2-C_n{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 17.43ex; " SRC="|."$dir".q|img50.svg"
 ALT="\begin{eqnarray*}
\sum_{j = 0}^n j \cdot (n - j + 1) &amp;=&amp; (n + 1) \cdot \sum_{j =...
...um_{j = 0}^n j^2 \\\\
&amp;=&amp; \unsur{2} \cdot n \cdot (n + 1)^2 - C_n
\end{eqnarray*}">|; 

1;

