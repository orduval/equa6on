# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 21.74ex; " SRC="|."$dir".q|img22.svg"
 ALT="\begin{eqnarray*}
\OD{y}{x} &amp;=&amp; f \\\\
\frac{d^2 y}{dx^2} &amp;=&amp; \deriveepartielle{f...
...e{f}{x}+\deriveepartielle{f}{y} f \\\\
\frac{d^3 y}{dx^3} &amp;=&amp; ...
\end{eqnarray*}">|; 

$key = q/[x_i,x_{i+1}];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$[x_i,x_{i+1}]$">|; 

$key = q/displaystylefleft(x_i+tcdoth_i,y(x_i+th_i)right)approxf_i+tcdot(f^*_{i+1}-f_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.83ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle f\left(x_i + t \cdot h_i,y(x_i + t h_i)\right) \approx f_i + t \cdot (f^*_{i+1} - f_i)$">|; 

$key = q/displaystyleh_i=x_{i+1}-x_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle h_i = x_{i+1} - x_i$">|; 

$key = q/displaystyley_{i+1}=y_i+f(x_i,h_i)h_i+OD{f}{x}(x_i,y_i)frac{h_i^2}{2}+...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle y_{i+1} = y_i + f(x_i,h_i)  h_i + \OD{f}{x}(x_i,y_i)  \frac{h_i^2}{2} + ...$">|; 

$key = q/displaystyley_{i+1}=y_i+frac{h_i}{2}cdot[f(x_i,y_i)+f(x_{i+1},y_{i+1}^*)];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle y_{i+1} = y_i + \frac{h_i}{2} \cdot [f(x_i,y_i) + f(x_{i+1},y_{i+1}^*)]$">|; 

$key = q/displaystyley_{i+1}=y_i+h_icdotf(x_i,y_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle y_{i+1} = y_i + h_i \cdot f(x_i,y_i)$">|; 

$key = q/displaystyley_{i+1}=y_i+int_{x_i}^{x_{i+1}}f(x,y(x))dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.72ex; vertical-align: -2.43ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle y_{i+1} = y_i + \int_{x_i}^{x_{i+1}} f(x,y(x))  dx$">|; 

$key = q/displaystyley_{i+1}^*=y_i+h_icdotf(x_i,y_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.83ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle y_{i+1}^* = y_i + h_i \cdot f(x_i,y_i)$">|; 

$key = q/h_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img10.svg"
 ALT="$h_i$">|; 

$key = q/i=0,1,2,...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$i=0,1,2,...$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$n$">|; 

$key = q/tin[0,1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$t\in [0,1]$">|; 

$key = q/x_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img4.svg"
 ALT="$x_i$">|; 

$key = q/y(x)=f(x,y(x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$y(x) = f(x,y(x))$">|; 

$key = q/y(x_{i+1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$y(x_{i+1})$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$y$">|; 

$key = q/y_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$y_i$">|; 

$key = q/y_{i+1}^*;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.83ex; " SRC="|."$dir".q|img15.svg"
 ALT="$y_{i+1}^*$">|; 

$key = q/y_{i+1}approxy(x_{i+1});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$y_{i+1} \approx y(x_{i+1})$">|; 

$key = q/{Eqts}OD{y}{x}(x)=f(x,y(x))y(0)=y_0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.52ex; vertical-align: -3.70ex; " SRC="|."$dir".q|img2.svg"
 ALT="\begin{Eqts}
\OD{y}{x}(x) = f(x,y(x)) \\\\
y(0) = y_0
\end{Eqts}">|; 

$key = q/{Eqts}f_i=f(x_i,y_i)f^*_{i+1}=f(x_{i+1},y^*_{i+1}){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img18.svg"
 ALT="\begin{Eqts}
f_i = f(x_i,y_i) \\\\
f^*_{i+1} = f(x_{i+1},y^*_{i+1})
\end{Eqts}">|; 

1;

