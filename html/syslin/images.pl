# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 25.04ex; vertical-align: -11.99ex; " SRC="|."$dir".q|img41.svg"
 ALT="\begin{Eqts}
P_k =
\begin{Matrix}{cc}
I_k &amp; 0 \\\\
0 &amp; P^{(n - k)}
\end{Matrix} \...
... \\\\
F_k =
\begin{Matrix}{cc}
I_k &amp; 0 \\\\
0 &amp; F^{(n - k)}
\end{Matrix}\end{Eqts}">|; 

$key = q/(canonique_1-u)^dual=[0-z^dual];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$(\canonique_1 - u)^\dual = [0  -z^\dual]$">|; 

$key = q/(e_1,...,e_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img184.svg"
 ALT="$(e_1,...,e_m)$">|; 

$key = q/(e_1,e_2,...,e_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img161.svg"
 ALT="$(e_1,e_2,...,e_n)$">|; 

$key = q/(f_1,f_2,...,f_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img162.svg"
 ALT="$(f_1,f_2,...,f_m)$">|; 

$key = q/(m,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$(m,1)$">|; 

$key = q/(m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img200.svg"
 ALT="$(m,n)$">|; 

$key = q/(m-n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img86.svg"
 ALT="$(m - n, 1)$">|; 

$key = q/(m-r,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img114.svg"
 ALT="$(m - r,1)$">|; 

$key = q/(n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$(n,1)$">|; 

$key = q/(n,m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img137.svg"
 ALT="$(n,m)$">|; 

$key = q/(n-m,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img102.svg"
 ALT="$(n - m,1)$">|; 

$key = q/(n-r,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img110.svg"
 ALT="$(n - r, 1)$">|; 

$key = q/(r,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$(r,1)$">|; 

$key = q/(u_1,...,u_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img183.svg"
 ALT="$(u_1,...,u_n)$">|; 

$key = q/(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img158.svg"
 ALT="$(x,y)$">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img12.svg"
 ALT="$1$">|; 

$key = q/A,B,C,D,F,G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img149.svg"
 ALT="$A,B,C,D,F,G$">|; 

$key = q/A,Binmatrice(corps,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img139.svg"
 ALT="$A,B \in \matrice(\corps,n,n)$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$A$">|; 

$key = q/A=(a_{ki})_{k,i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img199.svg"
 ALT="$A = (a_{ki})_{k,i}$">|; 

$key = q/A^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img136.svg"
 ALT="$A^\dual$">|; 

$key = q/A^{(n-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$A^{(n - 1)}$">|; 

$key = q/A^{(n-r)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img42.svg"
 ALT="$A^{(n - r)}$">|; 

$key = q/A^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img144.svg"
 ALT="$A^{-1}$">|; 

$key = q/AcdotB=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img128.svg"
 ALT="$A \cdot B = I$">|; 

$key = q/AcdotB=I_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img175.svg"
 ALT="$A \cdot B = I_m$">|; 

$key = q/Acdotx=AcdotBcdoty=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img141.svg"
 ALT="$A \cdot x = A \cdot B \cdot y = y$">|; 

$key = q/Acdotx=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img78.svg"
 ALT="$A \cdot x = y$">|; 

$key = q/Ainmatrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img1.svg"
 ALT="$A \in \matrice(\corps,m,n)$">|; 

$key = q/B-A^{-1}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.22ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img145.svg"
 ALT="$B - A^{-1} = 0$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img132.svg"
 ALT="$B$">|; 

$key = q/B=A^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img146.svg"
 ALT="$B = A^{-1}$">|; 

$key = q/B^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img135.svg"
 ALT="$B^\dual$">|; 

$key = q/BcdotA=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img133.svg"
 ALT="$B \cdot A = I$">|; 

$key = q/BcdotA=I_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img178.svg"
 ALT="$B \cdot A = I_n$">|; 

$key = q/Binmatrice(corps,n,m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img172.svg"
 ALT="$B \in \matrice(\corps,n,m)$">|; 

$key = q/Ccdotz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img115.svg"
 ALT="$C \cdot z$">|; 

$key = q/Ccdotz=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img117.svg"
 ALT="$C \cdot z = b$">|; 

$key = q/D-CcdotA^{-1}cdotB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.22ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img157.svg"
 ALT="$D - C \cdot A^{-1} \cdot B$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img159.svg"
 ALT="$E$">|; 

$key = q/E^{(n-1)},F^{(n-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img32.svg"
 ALT="$E^{(n-1)},F^{(n-1)}$">|; 

$key = q/E_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img54.svg"
 ALT="$E_0$">|; 

$key = q/EcdotAcdotF=C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img61.svg"
 ALT="$E \cdot A \cdot F = C$">|; 

$key = q/F_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img27.svg"
 ALT="$F_0$">|; 

$key = q/L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img62.svg"
 ALT="$L$">|; 

$key = q/L=E^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img59.svg"
 ALT="$L = E^{-1}$">|; 

$key = q/L^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img71.svg"
 ALT="$L^{-1}$">|; 

$key = q/P^{(n-1)},Q^{(n-1)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img31.svg"
 ALT="$P^{(n-1)},Q^{(n-1)}$">|; 

$key = q/P_0=matpermutation_{n,i,1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.62ex; vertical-align: -0.95ex; " SRC="|."$dir".q|img9.svg"
 ALT="$P_0 = \matpermutation_{n,i,1}$">|; 

$key = q/P_0cdotAcdotQ_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$P_0 \cdot A \cdot Q_0$">|; 

$key = q/P_i^{-1}=P_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img51.svg"
 ALT="$P_i^{-1} = P_i$">|; 

$key = q/Q_0=matpermutation_{n,j,1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.62ex; vertical-align: -0.95ex; " SRC="|."$dir".q|img10.svg"
 ALT="$Q_0 = \matpermutation_{n,j,1}$">|; 

$key = q/Q_i^{-1}=Q_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img52.svg"
 ALT="$Q_i^{-1} = Q_i$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img63.svg"
 ALT="$R$">|; 

$key = q/R=F^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img60.svg"
 ALT="$R = F^{-1}$">|; 

$key = q/S(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img89.svg"
 ALT="$S(y)$">|; 

$key = q/a_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img173.svg"
 ALT="$a_{ij}$">|; 

$key = q/a_{ij}incorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img165.svg"
 ALT="$a_{ij} \in \corps$">|; 

$key = q/a_{ki}incorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img185.svg"
 ALT="$a_{ki} \in \corps$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img111.svg"
 ALT="$b$">|; 

$key = q/b=L^{-1}cdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img83.svg"
 ALT="$b = L^{-1} \cdot y$">|; 

$key = q/b_1,b_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img84.svg"
 ALT="$b_1,b_2$">|; 

$key = q/b_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img112.svg"
 ALT="$b_1$">|; 

$key = q/b_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img113.svg"
 ALT="$b_2$">|; 

$key = q/b_2=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img92.svg"
 ALT="$b_2 = 0$">|; 

$key = q/b_2ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img91.svg"
 ALT="$b_2 \ne 0$">|; 

$key = q/b_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img174.svg"
 ALT="$b_{ij}$">|; 

$key = q/b_{kj}incorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img168.svg"
 ALT="$b_{kj} \in \corps$">|; 

$key = q/canonique_1^dualcdotu=1ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.37ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\canonique_1^\dual \cdot u = 1 \ne 0$">|; 

$key = q/canonique_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\canonique_i$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img160.svg"
 ALT="$\corps$">|; 

$key = q/corps^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\corps^m$">|; 

$key = q/corps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\corps^n$">|; 

$key = q/displaystyle(BcdotA)^dual=A^dualcdotB^dual=I^dual=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle (B \cdot A)^\dual = A^\dual \cdot B^\dual = I^\dual = I$">|; 

$key = q/displaystyle(D-CcdotA^{-1}cdotB)cdoty=G-CcdotA^{-1}cdotF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\displaystyle (D - C \cdot A^{-1} \cdot B) \cdot y = G - C \cdot A^{-1} \cdot F$">|; 

$key = q/displaystyle(L,C,R)=gaussjordan(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle (L,C,R) = \gaussjordan(A)$">|; 

$key = q/displaystyleA=LcdotCcdotR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle A = L \cdot C \cdot R$">|; 

$key = q/displaystyleA=LcdotIcdotR=LcdotR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle A = L \cdot I \cdot R = L \cdot R$">|; 

$key = q/displaystyleA^{-1}=R^{-1}cdotL^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle A^{-1} = R^{-1} \cdot L^{-1}$">|; 

$key = q/displaystyleAcdot(B-A^{-1})=I-I=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle A \cdot (B - A^{-1}) = I - I = 0$">|; 

$key = q/displaystyleAcdotB=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle A \cdot B = I$">|; 

$key = q/displaystyleAcdotx=AcdotBcdoty=Icdoty=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\displaystyle A \cdot x = A \cdot B \cdot y = I \cdot y = y$">|; 

$key = q/displaystyleAcdotx=LcdotCcdotRcdotx=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle A \cdot x = L \cdot C \cdot R \cdot x = y$">|; 

$key = q/displaystyleBcdotA=A^{-1}cdotA=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\displaystyle B \cdot A = A^{-1} \cdot A = I$">|; 

$key = q/displaystyleCcdotA^{-1}cdotF-CcdotA^{-1}cdotBcdoty+Dcdoty=G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle C \cdot A^{-1} \cdot F - C \cdot A^{-1} \cdot B \cdot y + D \cdot y = G$">|; 

$key = q/displaystyleCcdotRcdotx=L^{-1}cdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle C \cdot R \cdot x = L^{-1} \cdot y$">|; 

$key = q/displaystyleCcdotz=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle C \cdot z = b$">|; 

$key = q/displaystyleE_0=I+unsur{x^dualcdotx}cdot(canonique_1-x)cdotx^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle E_0 = I + \unsur{x^\dual \cdot x} \cdot (\canonique_1 - x) \cdot x^\dual$">|; 

$key = q/displaystyleE_0^{-1}=I-unsur{a}cdot(canonique_1-x)cdotx^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle E_0^{-1} = I - \unsur{a} \cdot (\canonique_1 - x) \cdot x^\dual$">|; 

$key = q/displaystyleEcdotAcdotF=C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle E \cdot A \cdot F = C$">|; 

$key = q/displaystyleF_0=I+unsur{u^dualcdotu}cdotucdot(canonique_1-u)^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle F_0 = I + \unsur{u^\dual \cdot u} \cdot u \cdot (\canonique_1 - u)^\dual$">|; 

$key = q/displaystyleF_0^{-1}=I-ucdot(canonique_1-u)^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle F_0^{-1} = I - u \cdot (\canonique_1 - u)^\dual$">|; 

$key = q/displaystyleQ_0cdotAcdotP_0=[xc_2...c_n];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle Q_0 \cdot A \cdot P_0 = [x  c_2  ...  c_n ]$">|; 

$key = q/displaystyleS(y)={xincorps^n:Acdotx=y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle S(y) = \{ x \in \corps^n : A \cdot x = y \}$">|; 

$key = q/displaystylea=composante_{ij}Ane0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.19ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle a = \composante_{ij} A \ne 0$">|; 

$key = q/displaystyledimE=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img181.svg"
 ALT="$\displaystyle \dim E = n$">|; 

$key = q/displaystylee_k=sum_{j=1}^mb_{kj}cdotf_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img167.svg"
 ALT="$\displaystyle e_k = \sum_{j = 1}^m b_{kj} \cdot f_j$">|; 

$key = q/displaystylee_k=sum_{k=1}^nsum_{j=1}^mb_{kj}cdota_{ji}cdote_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img177.svg"
 ALT="$\displaystyle e_k = \sum_{k = 1}^n \sum_{j = 1}^m b_{kj} \cdot a_{ji} \cdot e_i$">|; 

$key = q/displaystylef_i=sum_{j=1}^msum_{k=1}^na_{ik}cdotb_{kj}cdotf_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img169.svg"
 ALT="$\displaystyle f_i = \sum_{j = 1}^m \sum_{k = 1}^n a_{ik} \cdot b_{kj} \cdot f_j$">|; 

$key = q/displaystylef_i=sum_{k=1}^na_{ik}cdote_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img164.svg"
 ALT="$\displaystyle f_i = \sum_{k = 1}^n a_{ik} \cdot e_k$">|; 

$key = q/displaystyler=rangA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle r = \rang A$">|; 

$key = q/displaystylesum_{k=1}^ma_{ik}cdotb_{kj}=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img171.svg"
 ALT="$\displaystyle \sum_{k = 1}^m a_{ik} \cdot b_{kj} = \indicatrice_{ij}$">|; 

$key = q/displaystylesum_{k=1}^my_kcdote_k=w=sum_{k=1}^msum_{i=1}^na_{ki}cdotx_icdote_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img194.svg"
 ALT="$\displaystyle \sum_{k = 1}^m y_k \cdot e_k = w = \sum_{k = 1}^m \sum_{i = 1}^n a_{ki} \cdot x_i \cdot e_k$">|; 

$key = q/displaystyleu_i=sum_{k=1}^ma_{ki}cdote_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img186.svg"
 ALT="$\displaystyle u_i = \sum_{k = 1}^m a_{ki} \cdot e_k$">|; 

$key = q/displaystylew=sum_{i=1}^nx_icdotu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img193.svg"
 ALT="$\displaystyle w = \sum_{i = 1}^n x_i \cdot u_i$">|; 

$key = q/displaystylew=sum_{k=1}^my_kcdote_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img189.svg"
 ALT="$\displaystyle w = \sum_{k = 1}^m y_k \cdot e_k$">|; 

$key = q/displaystylex=A^{-1}cdotF-A^{-1}cdotBcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\displaystyle x = A^{-1} \cdot F - A^{-1} \cdot B \cdot y$">|; 

$key = q/displaystylex=A^{-1}cdot[F-Bcdot(D-CcdotA^{-1}cdotB)^{-1}cdot(G-CcdotA^{-1}cdotF)];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle x = A^{-1} \cdot [ F - B \cdot (D - C \cdot A^{-1} \cdot B)^{-1} \cdot (G - C \cdot A^{-1} \cdot F) ]$">|; 

$key = q/displaystylex=A^{-1}cdoty=R^{-1}cdotL^{-1}cdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle x = A^{-1} \cdot y = R^{-1} \cdot L^{-1} \cdot y$">|; 

$key = q/displaystylex=R^{-1}cdotz=R^{-1}cdotb_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.49ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle x = R^{-1} \cdot z = R^{-1} \cdot b_1$">|; 

$key = q/displaystyley=(D-CcdotA^{-1}cdotB)^{-1}cdot(G-CcdotA^{-1}cdotF);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img155.svg"
 ALT="$\displaystyle y = (D - C \cdot A^{-1} \cdot B)^{-1} \cdot (G - C \cdot A^{-1} \cdot F)$">|; 

$key = q/displaystyley=Acdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img201.svg"
 ALT="$\displaystyle y = A \cdot x$">|; 

$key = q/displaystyley_k=sum_{i=1}^na_{ki}cdotx_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img196.svg"
 ALT="$\displaystyle y_k = \sum_{i = 1}^n a_{ki} \cdot x_i$">|; 

$key = q/displaystylez_1=b=L^{-1}cdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle z_1 = b = L^{-1} \cdot y$">|; 

$key = q/e_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img34.svg"
 ALT="$e_1$">|; 

$key = q/e_1^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.32ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img35.svg"
 ALT="$e_1^\dual$">|; 

$key = q/e_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img195.svg"
 ALT="$e_k$">|; 

$key = q/e_kinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img166.svg"
 ALT="$e_k \in E$">|; 

$key = q/f_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img170.svg"
 ALT="$f_i$">|; 

$key = q/f_iinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img163.svg"
 ALT="$f_i \in E$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$i$">|; 

$key = q/j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$j$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$k$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img182.svg"
 ALT="$m$">|; 

$key = q/m=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img138.svg"
 ALT="$m = n$">|; 

$key = q/mlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img176.svg"
 ALT="$m \le n$">|; 

$key = q/mstrictsuperieurn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img127.svg"
 ALT="$m \strictsuperieur n$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img180.svg"
 ALT="$n$">|; 

$key = q/nlem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img179.svg"
 ALT="$n \le m$">|; 

$key = q/nstrictsuperieurm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img131.svg"
 ALT="$n \strictsuperieur m$">|; 

$key = q/p=min{m,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$p = \min\{m, n\}$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$r$">|; 

$key = q/r=m=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img75.svg"
 ALT="$r = m = n$">|; 

$key = q/r=m=p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img123.svg"
 ALT="$r = m = p$">|; 

$key = q/r=mlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img142.svg"
 ALT="$r = m \le n$">|; 

$key = q/r=mstrictinferieurn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img97.svg"
 ALT="$r = m \strictinferieur n$">|; 

$key = q/r=n=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img204.svg"
 ALT="$r = n = m$">|; 

$key = q/r=n=p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img126.svg"
 ALT="$r = n = p$">|; 

$key = q/r=nlem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img203.svg"
 ALT="$r = n \le m$">|; 

$key = q/r=nstrictinferieurm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img81.svg"
 ALT="$r = n \strictinferieur m$">|; 

$key = q/r=p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img43.svg"
 ALT="$r = p$">|; 

$key = q/r=p=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img49.svg"
 ALT="$r = p = m$">|; 

$key = q/r=p=m=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img47.svg"
 ALT="$r = p = m = n$">|; 

$key = q/r=p=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img48.svg"
 ALT="$r = p = n$">|; 

$key = q/rlemin{m,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img202.svg"
 ALT="$r \le \min \{m,n\}$">|; 

$key = q/rlep;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img67.svg"
 ALT="$r \le p$">|; 

$key = q/rstrictinferieurm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img122.svg"
 ALT="$r \strictinferieur m$">|; 

$key = q/rstrictinferieurn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img124.svg"
 ALT="$r \strictinferieur n$">|; 

$key = q/rstrictinferieurp;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img50.svg"
 ALT="$r \strictinferieur p$">|; 

$key = q/u^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$u^\dual$">|; 

$key = q/u^dual=[1z^dual];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$u^\dual = [1  z^\dual]$">|; 

$key = q/u^dualcdotu=1+z^dualcdotz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.93ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img26.svg"
 ALT="$u^\dual \cdot u = 1 + z^\dual \cdot z$">|; 

$key = q/u_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img192.svg"
 ALT="$u_i$">|; 

$key = q/v^dual=canonique_1^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.32ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img23.svg"
 ALT="$v^\dual = \canonique_1^\dual$">|; 

$key = q/v^dualcdotu=canonique_1^dualcdotu=1ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.37ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img56.svg"
 ALT="$v^\dual \cdot u = \canonique_1^\dual \cdot u = 1 \ne 0$">|; 

$key = q/w;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img191.svg"
 ALT="$w$">|; 

$key = q/winE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img187.svg"
 ALT="$w \in E$">|; 

$key = q/x,y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img150.svg"
 ALT="$x,y$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img14.svg"
 ALT="$x$">|; 

$key = q/x=Bcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img129.svg"
 ALT="$x = B \cdot y$">|; 

$key = q/x=R^{-1}cdotz;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img88.svg"
 ALT="$x = R^{-1} \cdot z$">|; 

$key = q/x=[x_1...x_n]^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img197.svg"
 ALT="$x = [x_1  ...  x_n]^\dual$">|; 

$key = q/x^dualcdotcanonique_1=ane0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$x^\dual \cdot \canonique_1 = a \ne 0$">|; 

$key = q/x^dualcdoty=x^dualcdotcanonique_1=ane0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img53.svg"
 ALT="$x^\dual \cdot y = x^\dual \cdot \canonique_1 = a \ne 0$">|; 

$key = q/x_iincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img190.svg"
 ALT="$x_i \in \corps$">|; 

$key = q/xinS(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img79.svg"
 ALT="$x \in S(y)$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img94.svg"
 ALT="$y$">|; 

$key = q/y=[y_1...y_m]^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img198.svg"
 ALT="$y = [y_1  ...  y_m]^\dual$">|; 

$key = q/y=b=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img121.svg"
 ALT="$y = b = 0$">|; 

$key = q/y=canonique_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$y = \canonique_1$">|; 

$key = q/y_kincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img188.svg"
 ALT="$y_k \in \corps$">|; 

$key = q/yincorps^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img69.svg"
 ALT="$y \in \corps^m$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img99.svg"
 ALT="$z$">|; 

$key = q/z_1,z_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img100.svg"
 ALT="$z_1,z_2$">|; 

$key = q/z_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img107.svg"
 ALT="$z_1$">|; 

$key = q/z_1=b_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img119.svg"
 ALT="$z_1 = b_1$">|; 

$key = q/z_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img108.svg"
 ALT="$z_2$">|; 

$key = q/z_2inmatrice(corps,n-m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img105.svg"
 ALT="$z_2 \in \matrice(\corps, n - m)$">|; 

$key = q/z_2ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img125.svg"
 ALT="$z_2 \ne 0$">|; 

$key = q/{Eqts}Acdotx+Bcdoty=FCcdotx+Dcdoty=G{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img151.svg"
 ALT="\begin{Eqts}
A \cdot x + B \cdot y = F \\\\
C \cdot x + D \cdot y = G
\end{Eqts}">|; 

$key = q/{Eqts}C=Matrix{{cc}I_m&0Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.01ex; vertical-align: -0.93ex; " SRC="|."$dir".q|img98.svg"
 ALT="\begin{Eqts}
C =
\begin{Matrix}{cc}
I_m &amp; 0
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}C=Matrix{{c}I_n0Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img82.svg"
 ALT="\begin{Eqts}
C =
\begin{Matrix}{c}
I_n \ 0
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}Ccdotz=Matrix{{cc}I_m&0Matrix{cdotMatrix{{c}z_1z_2Matrix{=I_mcdotz_1+0cdotz_2=z_1{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img103.svg"
 ALT="\begin{Eqts}
C \cdot z =
\begin{Matrix}{cc}
I_m &amp; 0
\end{Matrix}\cdot
\begin{Matrix}{c}
z_1 \ z_2
\end{Matrix}=
I_m \cdot z_1 + 0 \cdot z_2 = z_1
\end{Eqts}">|; 

$key = q/{Eqts}Ccdotz=Matrix{{cc}I_r&00&0Matrix{cdotMatrix{{c}z_1z_2Matrix{=Matrix{{c}z_10Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img116.svg"
 ALT="\begin{Eqts}
C \cdot z =
\begin{Matrix}{cc}
I_r &amp; 0 \\\\
0 &amp; 0
\end{Matrix}\cdot
...
...x}{c}
z_1 \ z_2
\end{Matrix}=
\begin{Matrix}{c}
z_1 \ 0
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}Cinleft{I_p,Matrix{{c}I_n0Matrix{,Matrix{{cc}I_m&0Matrix{,Matrix{{cc}I_r&00&0Matrix{right}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img46.svg"
 ALT="\begin{Eqts}
C \in \left\{
I_p ,
\begin{Matrix}{c}
I_n \ 0
\end{Matrix} ,
\begi...
...d{Matrix} ,
\begin{Matrix}{cc}
I_r &amp; 0 \\\\
0 &amp; 0
\end{Matrix}\right\}
\end{Eqts}">|; 

$key = q/{Eqts}E_0cdotP_0cdotAcdotQ_0=Matrix{{cc}1&z^dual0&ddotsMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img18.svg"
 ALT="\begin{Eqts}
E_0 \cdot P_0 \cdot A \cdot Q_0 =
\begin{Matrix}{cc}
1 &amp; z^\dual \\\\
0 &amp; \ddots
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}E_0cdotP_0cdotAcdotQ_0cdotF_0=Matrix{{cc}1&00&A^{(n-1)}Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img30.svg"
 ALT="\begin{Eqts}
E_0 \cdot P_0 \cdot A \cdot Q_0 \cdot F_0 =
\begin{Matrix}{cc}
1 &amp; 0 \\\\
0 &amp; A^{(n - 1)}
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}E_1cdotP_1cdotE_0cdotP_0cdotAcdotQ_0cdotF_0cdotQ_1cdotF_1=Matrix{{ccc}1&0&00&1&00&0&A^{(n-2)}Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 8.62ex; vertical-align: -3.75ex; " SRC="|."$dir".q|img37.svg"
 ALT="\begin{Eqts}
E_1 \cdot P_1 \cdot E_0 \cdot P_0 \cdot A \cdot Q_0 \cdot F_0 \cdot...
...rix}{ccc}
1 &amp; 0 &amp; 0 \\\\
0 &amp; 1 &amp; 0 \\\\
0 &amp; 0 &amp; A^{(n - 2)}
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}F_0=I+unsur{1+z^dualcdotz}cdotMatrix{{cc}0&-z^dual0&-zcdotz^dualMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img28.svg"
 ALT="\begin{Eqts}
F_0 = I + \unsur{1 + z^\dual \cdot z} \cdot
\begin{Matrix}{cc}
0 &amp; -z^\dual \\\\
0 &amp; -z \cdot z^\dual
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}Matrix{{cc}A&BC&DMatrix{cdotMatrix{{c}x&yMatrix{=Matrix{{c}FGMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img148.svg"
 ALT="\begin{Eqts}
\begin{Matrix}{cc}
A &amp; B \ C &amp; D
\end{Matrix}\cdot
\begin{Matrix}{c}
x &amp; y
\end{Matrix}=
\begin{Matrix}{c}
F \ G
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}Matrix{{c}I_n0Matrix{cdotz=Matrix{{c}z0Matrix{=Matrix{{c}b_1b_2Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img87.svg"
 ALT="\begin{Eqts}
\begin{Matrix}{c}
I_n \ 0
\end{Matrix}\cdot
z
=
\begin{Matrix}{c}
z \ 0
\end{Matrix}=
\begin{Matrix}{c}
b_1 \ b_2
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}Matrix{{c}z_10Matrix{=Matrix{{c}b_1b_2Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img118.svg"
 ALT="\begin{Eqts}
\begin{Matrix}{c}
z_1 \ 0
\end{Matrix}=
\begin{Matrix}{c}
b_1 \ b_2
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}ucdot(canonique_1-u)^dual=Matrix{{c}1zMatrix{cdotMatrix{{cc}0&-z^dualMatrix{=Matrix{{cc}0&-z^dual0&-zcdotz^dualMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img25.svg"
 ALT="\begin{Eqts}
u \cdot (\canonique_1 - u)^\dual =
\begin{Matrix}{c}
1 \ z
\end{Ma...
...
\begin{Matrix}{cc}
0 &amp; -z^\dual \\\\
0 &amp; -z \cdot z^\dual
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}x=R^{-1}cdotMatrix{{c}b_1z_2Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img120.svg"
 ALT="\begin{Eqts}
x = R^{-1} \cdot
\begin{Matrix}{c}
b_1 \ z_2
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}x=R^{-1}cdotz=R^{-1}cdotMatrix{{c}L^{-1}cdotyz_2Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img106.svg"
 ALT="\begin{Eqts}
x =R^{-1} \cdot z = R^{-1} \cdot
\begin{Matrix}{c}
L^{-1} \cdot y \ z_2
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}y=LcdotMatrix{{c}b_10Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img95.svg"
 ALT="\begin{Eqts}
y = L \cdot
\begin{Matrix}{c}
b_1 \ 0
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}y=LcdotMatrix{{c}b_1b_2Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img96.svg"
 ALT="\begin{Eqts}
y = L \cdot
\begin{Matrix}{c}
b_1 \ b_2
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}z=Rcdotxb=L^{-1}cdoty{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img73.svg"
 ALT="\begin{Eqts}
z = R \cdot x \\\\
b = L^{-1} \cdot y
\end{Eqts}">|; 

$key = q/{Eqts}z=b_10=b_2{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img90.svg"
 ALT="\begin{Eqts}
z = b_1 \\\\
0 = b_2
\end{Eqts}">|; 

$key = q/{eqnarraystar}E&=&E_rcdotP_rcdot...cdotE_0cdotP_0F&=&Q_0cdotF_0cdot...cdotQ_rcdotF_r{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.44ex; " SRC="|."$dir".q|img44.svg"
 ALT="\begin{eqnarray*}
E &amp;=&amp; E_r \cdot P_r \cdot ... \cdot E_0 \cdot P_0 \\\\
F &amp;=&amp; Q_0 \cdot F_0 \cdot ... \cdot Q_r \cdot F_r
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}E^{-1}&=&P_0cdotE_0^{-1}cdot...cdotP_rcdotE_r^{-1}F^{-1}&=&F_r^{-1}cdotQ_rcdot...cdotF_0^{-1}cdotQ_0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img58.svg"
 ALT="\begin{eqnarray*}
E^{-1} &amp;=&amp; P_0 \cdot E_0^{-1} \cdot ... \cdot P_r \cdot E_r^{-...
...F^{-1} &amp;=&amp; F_r^{-1} \cdot Q_r \cdot ... \cdot F_0^{-1} \cdot Q_0
\end{eqnarray*}">|; 

1;

