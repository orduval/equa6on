# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.53ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\displaystyle R^i_{m,kj} = \deriveepartielle{}{x^k}\christoffel{i}{jm} - \deriv...
...offel{i}{kl}\christoffel{l}{jm} - \sum_l \christoffel{i}{jl}\christoffel{l}{km}$">|; 

$key = q/(e^1,...,e^n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$(e^1, ..., e^n)$">|; 

$key = q/(e_1,...,e_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$(e_1, ..., e_n)$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$E$">|; 

$key = q/E=setR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$E = \setR^n$">|; 

$key = q/G=(g^i_m)_{i,m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.70ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img58.svg"
 ALT="$G = (g^i_m)_{i,m}$">|; 

$key = q/OOD{r}{t}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.19ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\OOD{r}{t} = 0$">|; 

$key = q/R_{...}^{...};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.35ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img116.svg"
 ALT="$R_{...}^{...}$">|; 

$key = q/a,binE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img35.svg"
 ALT="$a,b\in E$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img63.svg"
 ALT="$a$">|; 

$key = q/a^i=OD{x^i}{t};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.21ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img64.svg"
 ALT="$a^i = \OD{x^i}{t}$">|; 

$key = q/a^iinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img44.svg"
 ALT="$a^i\in\setR$">|; 

$key = q/a_iinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img70.svg"
 ALT="$a_i\in\setR$">|; 

$key = q/ainE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img42.svg"
 ALT="$a\in E$">|; 

$key = q/b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img76.svg"
 ALT="$b$">|; 

$key = q/christoffel{i}{kj};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\christoffel{i}{kj}$">|; 

$key = q/da^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.05ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img47.svg"
 ALT="$da^i$">|; 

$key = q/de_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img49.svg"
 ALT="$de_i$">|; 

$key = q/deriveepartielle{e_k}{x^j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.05ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\deriveepartielle{e_k}{x^j}$">|; 

$key = q/deriveepartielle{x^i}{y_j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.67ex; vertical-align: -1.36ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\deriveepartielle{x^i}{y_j}$">|; 

$key = q/deriveepartielle{y_i}{x^j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.08ex; vertical-align: -0.90ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\deriveepartielle{y_i}{x^j}$">|; 

$key = q/displaystyle(ds)^2=scalaire{dr}{dr}=sum_{i,j}g_{ij}dx^idx^j=sum_{i,j}g^{ij}dy_idy_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle (ds)^2 = \scalaire{dr}{dr} = \sum_{i,j} g_{ij}  dx^i  dx^j = \sum_{i,j} g^{ij}  dy_i  dy_j$">|; 

$key = q/displaystyleOD{r}{t}=sum_ie_iOD{x^i}{t}=sum_ie^iOD{y_i}{t};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \OD{r}{t} = \sum_i e_i  \OD{x^i}{t} = \sum_i e^i  \OD{y_i}{t}$">|; 

$key = q/displaystyleOOD{r}{t}=OD{}{t}OD{r}{t}=OD{a}{t};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle \OOD{r}{t} = \OD{}{t}\OD{r}{t} = \OD{a}{t}$">|; 

$key = q/displaystyleOOD{r}{t}=sum_{i}e_ileft[OOD{x^i}{t}+sum_{j,k}christoffel{i}{kj}OD{x^k}{t}OD{x^j}{t}right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.51ex; vertical-align: -3.37ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle \OOD{r}{t} = \sum_{i} e_i  \left[ \OOD{x^i}{t} + \sum_{j,k} \christoffel{i}{kj}  \OD{x^k}{t} \OD{x^j}{t} \right]$">|; 

$key = q/displaystyleT=sum_{i,j,k,l}T_{i...j}^{k...l}e^iotimes...otimese^jotimese_kotimes...otimese_l;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -3.37ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle T = \sum_{i,j,k,l} T_{i...j}^{k...l} e^i \otimes ... \otimes e^j \otimes e_k \otimes ... \otimes e_l$">|; 

$key = q/displaystyleT=sum_{i,j}T^i_je_iotimese^j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle T = \sum_{i,j} T^i_j  e_i \otimes e^j$">|; 

$key = q/displaystyleT^i_j=gradient_ja^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.04ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle T^i_j = \gradient_j a^i$">|; 

$key = q/displaystyleT_{i...j}^{k...l}=dblecont{e_jotimes...otimese_i}{m}{T}{n}{e^lotimes...otimese^k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.43ex; vertical-align: -1.31ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle T_{i...j}^{k...l} = \dblecont{e_j \otimes ... \otimes e_i}{m}{T}{n}{e^l \otimes ... \otimes e^k}$">|; 

$key = q/displaystylea=OD{r}{t}=sum_ie_iOD{x^i}{t};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle a = \OD{r}{t} = \sum_i e_i  \OD{x^i}{t}$">|; 

$key = q/displaystylea=sum_ia^ie_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle a = \sum_i a^i  e_i$">|; 

$key = q/displaystylea=sum_ia_ie^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle a = \sum_i a_i  e^i$">|; 

$key = q/displaystylechristoffel{i}{kj}=christoffel{i}{jk};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle \christoffel{i}{kj} = \christoffel{i}{jk}$">|; 

$key = q/displaystylechristoffel{kj}{i}=christoffel{jk}{i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \christoffel{kj}{i} = \christoffel{jk}{i}$">|; 

$key = q/displaystyled(aotimesb)=daotimesb+aotimesdb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle d(a \otimes b) = da \otimes b + a \otimes db$">|; 

$key = q/displaystyledT=sum_{i,j,k}e_iotimese^jdx^kleft[deriveepartielle{T^i_j}{x^k}+sum_mchristoffel{i}{mk}T^m_j-sum_mchristoffel{m}{jk}T^i_mright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.51ex; vertical-align: -3.37ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle dT = \sum_{i,j,k} e_i \otimes e^j dx^k \left[ \deriveepartielle{T...
...} + \sum_m \christoffel{i}{mk} T^m_j - \sum_m \christoffel{m}{jk} T^i_m \right]$">|; 

$key = q/displaystyledT=sum_{i,j}e_iotimese^jleft[dT^i_j+sum_{k,m}christoffel{i}{mk}T^m_jdx^k+sum_{k,m}christoffel{mk}{j}T^i_mdy_kright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.51ex; vertical-align: -3.37ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle dT = \sum_{i,j} e_i \otimes e^j \left[ dT^i_j + \sum_{k,m} \chris...
...l{i}{mk}  T^m_j  dx^k + \sum_{k,m} \christoffel{mk}{j}  T^i_m  dy_k \right]$">|; 

$key = q/displaystyledT=sum_{i,j}left[dT^i_je_iotimese^j+T^i_jde_iotimese^j+T^i_je_iotimesde^jright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle dT = \sum_{i,j} \left[ dT^i_j  e_i \otimes e^j + T^i_j  de_i \otimes e^j + T^i_j  e_i \otimes de^j \right]$">|; 

$key = q/displaystyleda=scalaire{gradienta}{dr}=gradientacdotdr;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\displaystyle da = \scalaire{\gradient a}{dr} = \gradient a \cdot dr$">|; 

$key = q/displaystyleda=sum_ida^ie_i+sum_ka^kde_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle da = \sum_i da^i  e_i + \sum_k a^k  de_k$">|; 

$key = q/displaystyleda=sum_{i,j}e^idx^jleft[deriveepartielle{a_i}{x^j}-sum_kchristoffel{k}{ij}a_kright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.45ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle da = \sum_{i,j} e^i dx^j \left[ \deriveepartielle{a_i}{x^j} - \sum_k \christoffel{k}{ij} a_k \right]$">|; 

$key = q/displaystyleda=sum_{i,j}e^idy_jleft[deriveepartielle{a_i}{y_j}+sum_kchristoffel{kj}{i}a_kright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.45ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle da = \sum_{i,j} e^i  dy_j \left[ \deriveepartielle{a_i}{y_j} + \sum_k \christoffel{kj}{i}  a_k \right]$">|; 

$key = q/displaystyleda=sum_{i,j}e_idx^jleft[deriveepartielle{a^i}{x^j}+sum_kchristoffel{i}{kj}a^kright];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.45ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle da = \sum_{i,j} e_i  dx^j  \left[ \deriveepartielle{a^i}{x^j} + \sum_k \christoffel{i}{kj}  a^k \right]$">|; 

$key = q/displaystyleda^i=sum_jderiveepartielle{a^i}{x^j}dx^j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle da^i = \sum_j \deriveepartielle{a^i}{x^j}  dx^j$">|; 

$key = q/displaystyleda_i=deriveepartielle{a_i}{y_j}dy_j=deriveepartielle{a_i}{x^j}dx^j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.60ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle da_i = \deriveepartielle{a_i}{y_j} dy_j = \deriveepartielle{a_i}{x^j} dx^j$">|; 

$key = q/displaystylede_k=sum_jderiveepartielle{e_k}{x^j}dx^j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.58ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle de_k = \sum_j \deriveepartielle{e_k}{x^j}  dx^j$">|; 

$key = q/displaystylederiveepartielle{e^k}{y_j}=sum_ichristoffel{kj}{i}e^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.58ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle \deriveepartielle{e^k}{y_j} = \sum_i \christoffel{kj}{i}  e^i$">|; 

$key = q/displaystylederiveepartielle{e_k}{x^j}=dfdxdy{r}{x^j}{x^k}=dfdxdy{r}{x^k}{x^j}=deriveepartielle{e_j}{x^k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \deriveepartielle{e_k}{x^j} = \dfdxdy{r}{x^j}{x^k} = \dfdxdy{r}{x^k}{x^j} = \deriveepartielle{e_j}{x^k}$">|; 

$key = q/displaystylederiveepartielle{e_k}{x^j}=sum_ichristoffel{i}{kj}e_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.48ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \deriveepartielle{e_k}{x^j} = \sum_i \christoffel{i}{kj}  e_i$">|; 

$key = q/displaystylederiveepartielle{g_{ij}}{x^k}=gamma_{ijk}+gamma_{jik};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle \deriveepartielle{g_{ij}}{x^k} = \gamma_{ijk} + \gamma_{jik}$">|; 

$key = q/displaystylederiveepartielle{g_{ij}}{x^l}=sum_kchristoffel{k}{il}g_{kj}+sum_kchristoffel{k}{jl}g_{ik};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.53ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle \deriveepartielle{g_{ij}}{x^l} = \sum_k \christoffel{k}{il}  g_{kj} + \sum_k \christoffel{k}{jl}  g_{ik}$">|; 

$key = q/displaystyledr=sum_ie_idx^i=sum_ie^idy_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle dr = \sum_i e_i  dx^i = \sum_i e^i  dy_i$">|; 

$key = q/displaystyledscalaire{e^i}{e_j}=dindicatrice_i^j=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.12ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle d\scalaire{e^i}{e_j} = d\indicatrice_i^j = 0$">|; 

$key = q/displaystyleg^{ik}g_{kj}=indicatrice_j^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.09ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle g^{ik} g_{kj} = \indicatrice_j^i$">|; 

$key = q/displaystyleg_i^j=indicatrice_i^j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.90ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle g_i^j = \indicatrice_i^j$">|; 

$key = q/displaystylegamma_{ijl}=gamma_{ilj};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle \gamma_{ijl} = \gamma_{ilj}$">|; 

$key = q/displaystylegamma_{ijl}=sum_kchristoffel{k}{il}g_{kj};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.53ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle \gamma_{ijl} = \sum_k \christoffel{k}{il}  g_{kj}$">|; 

$key = q/displaystylegradient_ja^i=deriveepartielle{a^i}{x^j}+sum_kchristoffel{i}{kj}a^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.58ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle \gradient_j a^i = \deriveepartielle{a^i}{x^j} + \sum_k \christoffel{i}{kj}  a^k$">|; 

$key = q/displaystylegradient_kT^i_j=deriveepartielle{T^i_j}{x^k}+sum_mchristoffel{i}{mk}T^m_j-sum_mchristoffel{m}{jk}T^i_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.72ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle \gradient_k T^i_j = \deriveepartielle{T^i_j}{x^k} + \sum_m \christoffel{i}{mk} T^m_j - \sum_m \christoffel{m}{jk} T^i_m$">|; 

$key = q/displaystylegradient_kgradient_ja^i-gradient_jgradient_ka^i=sum_mR^i_{m,kj}a^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.52ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle \gradient_k \gradient_j a^i - \gradient_j \gradient_k a^i = \sum_m R^i_{m,kj} a^m$">|; 

$key = q/displaystylegradienta=sum_{i,j}gradient_ja^ie_iotimese^j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle \gradient a = \sum_{i,j} \gradient_j a^i e_i \otimes e^j$">|; 

$key = q/displaystyler=rho(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle r = \rho(x)$">|; 

$key = q/displaystyler=sigma(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle r = \sigma(y)$">|; 

$key = q/dr;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$dr$">|; 

$key = q/ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img38.svg"
 ALT="$ds$">|; 

$key = q/e^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.05ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$e^i$">|; 

$key = q/e^j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.05ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$e^j$">|; 

$key = q/e_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img10.svg"
 ALT="$e_i$">|; 

$key = q/g^{ij},g_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.70ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img90.svg"
 ALT="$g^{ij},g_{ij}$">|; 

$key = q/g_i^j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.90ex; vertical-align: -0.69ex; " SRC="|."$dir".q|img91.svg"
 ALT="$g_i^j$">|; 

$key = q/g_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img80.svg"
 ALT="$g_{ij}$">|; 

$key = q/gradient_jgradient_ka^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.70ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\gradient_j \gradient_k a^i$">|; 

$key = q/gradient_kT^i_j=gradient_kgradient_ja^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.93ex; vertical-align: -0.97ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\gradient_k T^i_j = \gradient_k \gradient_j a^i$">|; 

$key = q/i...j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img96.svg"
 ALT="$i...j$">|; 

$key = q/j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img113.svg"
 ALT="$j$">|; 

$key = q/k,j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img57.svg"
 ALT="$k,j$">|; 

$key = q/k...l;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img98.svg"
 ALT="$k...l$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img86.svg"
 ALT="$k$">|; 

$key = q/l;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img85.svg"
 ALT="$l$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img95.svg"
 ALT="$m$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img97.svg"
 ALT="$n$">|; 

$key = q/phi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\phi$">|; 

$key = q/phi=rho^{-1}circsigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\phi = \rho^{-1} \circ \sigma$">|; 

$key = q/psi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\psi$">|; 

$key = q/psi=sigma^{-1}circrho;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\psi = \sigma^{-1} \circ \rho$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$r$">|; 

$key = q/rho:setR^ntosetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\rho : \setR^n \to \setR^n$">|; 

$key = q/rho;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\rho$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\setR$">|; 

$key = q/sigma:setR^ntosetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\sigma : \setR^n \to \setR^n$">|; 

$key = q/sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\sigma$">|; 

$key = q/tinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img19.svg"
 ALT="$t \in \setR$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$x$">|; 

$key = q/x^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.05ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img45.svg"
 ALT="$x^i$">|; 

$key = q/x^i=x^i(t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.63ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$x^i = x^i(t)$">|; 

$key = q/xinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img4.svg"
 ALT="$x \in \setR^n$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img18.svg"
 ALT="$y$">|; 

$key = q/y_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img71.svg"
 ALT="$y_i$">|; 

$key = q/yinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$y \in \setR^n$">|; 

$key = q/{Eqts}a=sum_ia^ie_i=sum_ia_ie^ib=sum_ib^ie_i=sum_ib_ie^i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.65ex; vertical-align: -5.26ex; " SRC="|."$dir".q|img36.svg"
 ALT="\begin{Eqts}
a = \sum_i a^i  e_i = \sum_i a_i  e^i \\\\
b = \sum_i b^i  e_i = \sum_i b_i  e^i
\end{Eqts}">|; 

$key = q/{Eqts}deriveepartielle{phi}{y_j}=sum_ideriveepartielle{x^i}{y_j}e_ideriveepartielle{psi}{x^j}=sum_ideriveepartielle{y_i}{x^j}e^i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.38ex; vertical-align: -6.14ex; " SRC="|."$dir".q|img30.svg"
 ALT="\begin{Eqts}
\deriveepartielle{\phi}{y_j} = \sum_i \deriveepartielle{x^i}{y_j} \...
...eriveepartielle{\psi}{x^j} = \sum_i \deriveepartielle{y_i}{x^j}  e^i
\end{Eqts}">|; 

$key = q/{Eqts}deriveepartielle{x^i}{y_j}=scalaire{e_i}{deriveepartielle{phi}{y_j}}deriveepartielle{y_i}{x^j}=scalaire{e^i}{deriveepartielle{psi}{x^j}}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.18ex; vertical-align: -5.53ex; " SRC="|."$dir".q|img100.svg"
 ALT="\begin{Eqts}
\deriveepartielle{x^i}{y_j} = \scalaire{e_i}{ \deriveepartielle{\ph...
...epartielle{y_i}{x^j} = \scalaire{e^i}{ \deriveepartielle{\psi}{x^j} }
\end{Eqts}">|; 

$key = q/{Eqts}e_i(x)=deriveepartielle{rho}{x^i}(x)e^i(y)=deriveepartielle{sigma}{y_i}(y){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.94ex; vertical-align: -4.91ex; " SRC="|."$dir".q|img12.svg"
 ALT="\begin{Eqts}
e_i(x) = \deriveepartielle{\rho}{x^i}(x) \\\\
e^i(y) = \deriveepartielle{\sigma}{y_i}(y)
\end{Eqts}">|; 

$key = q/{Eqts}g_{ij}=g_{ji}g^{ij}=g^{ji}g_i^j=g_j^i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.34ex; vertical-align: -4.61ex; " SRC="|."$dir".q|img34.svg"
 ALT="\begin{Eqts}
g_{ij} = g_{ji} \\\\
g^{ij} = g^{ji} \\\\
g_i^j = g_j^i
\end{Eqts}">|; 

$key = q/{Eqts}g_{ij}=scalaire{e_i}{e_j}g_i^j=scalaire{e_i}{e^j}g^{ij}=scalaire{e^i}{e^j}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.30ex; vertical-align: -4.59ex; " SRC="|."$dir".q|img33.svg"
 ALT="\begin{Eqts}
g_{ij} = \scalaire{e_i}{e_j} \\\\
g_i^j = \scalaire{e_i}{e^j} \\\\
g^{ij} = \scalaire{e^i}{e^j}
\end{Eqts}">|; 

$key = q/{Eqts}scalaire{a}{b}=sum_{i,j}g_{ij}a^ib^jscalaire{a}{b}=sum_{i,j}g^{ij}a_ib_jscalaire{a}{b}=sum_{i,j}g_j^ia_ib^jscalaire{a}{b}=sum_{i,j}g_i^ja^ib_j{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 25.07ex; vertical-align: -12.00ex; " SRC="|."$dir".q|img37.svg"
 ALT="\begin{Eqts}
\scalaire{a}{b} = \sum_{i,j} g_{ij}  a^i  b^j \\\\
\scalaire{a}{b}...
... g_j^i  a_i  b^j \\\\
\scalaire{a}{b} = \sum_{i,j} g_i^j  a^i  b_j
\end{Eqts}">|; 

$key = q/{Eqts}x=rho^{-1}(r)=(rho^{-1}circsigma)(y)=phi(y)y=sigma^{-1}(r)=(sigma^{-1}circrho)(x)=psi(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img23.svg"
 ALT="\begin{Eqts}
x = \rho^{-1}(r) = (\rho^{-1} \circ \sigma)(y) = \phi(y) \\\\
y = \sigma^{-1}(r) = (\sigma^{-1} \circ \rho)(x) = \psi(x)
\end{Eqts}">|; 

1;

