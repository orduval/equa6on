# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(x,y)inAtimesB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$(x,y) \in A \times B$">|; 

$key = q/(x,y)inR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$(x,y) \in R$">|; 

$key = q/(x,y)inidentite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$(x,y) \in \identite$">|; 

$key = q/(x,z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$(x,z)$">|; 

$key = q/(y,z)inS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$(y,z)\in S$">|; 

$key = q/A,B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$A,B$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$A$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$B$">|; 

$key = q/C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img30.svg"
 ALT="$C$">|; 

$key = q/R(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$R(x)$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$R$">|; 

$key = q/R^1=R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$R^1 = R$">|; 

$key = q/R^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img12.svg"
 ALT="$R^{-1}$">|; 

$key = q/Rinrelation(A,A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$R \in \relation(A,A)$">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$S$">|; 

$key = q/S^{-1}(z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$S^{-1}(z)$">|; 

$key = q/ScircR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img31.svg"
 ALT="$S \circ R$">|; 

$key = q/XsubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img22.svg"
 ALT="$X \subseteq A$">|; 

$key = q/YsubseteqB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img25.svg"
 ALT="$Y \subseteq B$">|; 

$key = q/displaystyleR(X)={yinB:xinX,(x,y)inR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle R(X) = \{ y \in B : x \in X,  (x,y) \in R \}$">|; 

$key = q/displaystyleR(x)={yinB:(x,y)inR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle R(x) = \{ y \in B : (x,y) \in R \}$">|; 

$key = q/displaystyleR^n=Rcirc...circR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.81ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle R^n = R \circ ... \circ R$">|; 

$key = q/displaystyleR^{-1}(Y)={xinA:yinY,(x,y)inR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle R^{-1}(Y) = \{ x \in A : y \in Y,  (x,y) \in R \}$">|; 

$key = q/displaystyleR^{-1}(y)={xinA:(x,y)inR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle R^{-1}(y) = \{ x \in A : (x,y) \in R \}$">|; 

$key = q/displaystyleR^{-1}={(y,x)inBtimesA:(x,y)inR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle R^{-1} = \{ (y,x) \in B \times A : (x,y) \in R \}$">|; 

$key = q/displaystyleScircR={(x,z)inAtimesC:R(x)capS^{-1}(z)neemptyset};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle S \circ R = \{ (x,z) \in A \times C : R(x) \cap S^{-1}(z) \ne \emptyset \}$">|; 

$key = q/displaystyleScircR={(x,z)inAtimesC:existsyinB:(x,y)inR,(y,z)inS};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle S \circ R = \{ (x,z) \in A \times C : \exists y \in B : (x,y) \in R,  (y,z) \in S \}$">|; 

$key = q/displaystyledomaineR=R^{-1}(B)={xinA:yinB,(x,y)inR};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \domaine R = R^{-1}(B) = \{ x \in A : y \in B,  (x,y) \in R \}$">|; 

$key = q/displaystyleidentite={(x,x):xinX};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \identite = \{ (x,x) : x \in X \}$">|; 

$key = q/displaystyleidentite^{-1}=identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.18ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \identite^{-1} = \identite$">|; 

$key = q/displaystyleimageR=R(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \image R = R(A)$">|; 

$key = q/displaystylerelation(A,B)={R:RsubseteqAtimesB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \relation(A,B) = \{ R : R \subseteq A \times B \}$">|; 

$key = q/displaystylexRy;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle x  R  y$">|; 

$key = q/identitesubseteqXtimesX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\identite \subseteq X \times X$">|; 

$key = q/relation(A,B);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\relation(A,B)$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$x$">|; 

$key = q/x=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img19.svg"
 ALT="$x = y$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img8.svg"
 ALT="$x \in A$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$y$">|; 

$key = q/yinB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$y \in B$">|; 

$key = q/{eqnarraystar}R^0&=&identiteR^n&=&RcircR^{n-1}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.99ex; " SRC="|."$dir".q|img39.svg"
 ALT="\begin{eqnarray*}
R^0 &amp;=&amp; \identite \\\\
R^n &amp;=&amp; R \circ R^{n-1}
\end{eqnarray*}">|; 

1;

