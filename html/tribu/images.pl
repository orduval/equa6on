# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/A,Binmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img16.svg"
 ALT="$A,B \in \mathcal{T}$">|; 

$key = q/A_1,A_2,...inmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$A_1,A_2,... \in \mathcal{T}$">|; 

$key = q/A_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img14.svg"
 ALT="$A_i$">|; 

$key = q/A_iinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img11.svg"
 ALT="$A_i \in \mathcal{T}$">|; 

$key = q/Ainmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img5.svg"
 ALT="$A \in \mathcal{T}$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\Omega$">|; 

$key = q/OmegasetminusAinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\Omega \setminus A \in \mathcal{T}$">|; 

$key = q/displaystyleA_i=OmegasetminusB_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle A_i = \Omega \setminus B_i$">|; 

$key = q/displaystyleAsetminusB=Acap(OmegasetminusB)inmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle A \setminus B = A \cap (\Omega \setminus B) \in \mathcal{T}$">|; 

$key = q/displaystyleB_i=OmegasetminusA_iinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle B_i = \Omega \setminus A_i \in \mathcal{T}$">|; 

$key = q/displaystyleOmega=Omegasetminusemptysetinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \Omega = \Omega \setminus \emptyset \in \mathcal{T}$">|; 

$key = q/displaystylebigcap_iA_i=Omegasetminuscrochets{bigcup_iB_i}inmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \bigcap_i A_i = \Omega \setminus \crochets{\bigcup_i B_i} \in \mathcal{T}$">|; 

$key = q/displaystylebigcup_iA_iinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \bigcup_i A_i \in \mathcal{T}$">|; 

$key = q/displaystyletribu(mathcal{C},Omega)=inf_subseteq{mathcal{T}intribu(Omega):mathcal{C}subseteqmathcal{T}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.84ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \tribu(\mathcal{C},\Omega) = \inf_\subseteq \{ \mathcal{T} \in \tribu(\Omega) : \mathcal{C} \subseteq \mathcal{T} \}$">|; 

$key = q/emptysetinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.06ex; vertical-align: -0.23ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\emptyset \in \mathcal{T}$">|; 

$key = q/mathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\mathcal{C}$">|; 

$key = q/mathcal{C}subseteqsousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\mathcal{C} \subseteq \sousens(\Omega)$">|; 

$key = q/mathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\mathcal{T}$">|; 

$key = q/mathcal{T}subseteqsousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\mathcal{T} \subseteq \sousens(\Omega)$">|; 

$key = q/tribu(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\tribu(\Omega)$">|; 

1;

