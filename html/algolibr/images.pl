# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$(n,1)$">|; 

$key = q/(n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$(n,n)$">|; 

$key = q/D_k^dualcdotD_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img66.svg"
 ALT="$D_k^\dual \cdot D_k$">|; 

$key = q/H;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img45.svg"
 ALT="$H$">|; 

$key = q/H=H^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$H = H^\dual$">|; 

$key = q/J;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img49.svg"
 ALT="$J$">|; 

$key = q/J_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img40.svg"
 ALT="$J_k$">|; 

$key = q/J_k^dualcdotdelta_k=scalaire{J_k}{delta_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.53ex; vertical-align: -0.70ex; " SRC="|."$dir".q|img25.svg"
 ALT="$J_k^\dual \cdot
\delta_k = \scalaire{J_k}{\delta_k}$">|; 

$key = q/N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$N$">|; 

$key = q/Phi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\Phi$">|; 

$key = q/Phiin{varphi,J,H};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\Phi \in \{\varphi,J,H\}$">|; 

$key = q/S=noyaufneemptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img60.svg"
 ALT="$S = \noyau f \ne \emptyset$">|; 

$key = q/alpha_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\alpha_k$">|; 

$key = q/alpha_kinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\alpha_k \in \setR$">|; 

$key = q/beta_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\beta_k$">|; 

$key = q/beta_kinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\beta_k \in \setR$">|; 

$key = q/boule(0,norme{J_k});MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\boule(0,\norme{J_k})$">|; 

$key = q/delta_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\delta_k$">|; 

$key = q/displaystyle-J_k^dualcdotp_k+alpha_kcdotp_k^dualcdotH_kcdotp_k=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle - J_k^\dual \cdot p_k + \alpha_k \cdot p_k^\dual \cdot H_k \cdot p_k = 0$">|; 

$key = q/displaystyle0leminunsur{2}f(x)^dualcdotf(x)leunsur{2}f(gamma)^dualcdotf(gamma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle 0 \le \min \unsur{2} f(x)^\dual \cdot f(x) \le \unsur{2} f(\gamma)^\dual \cdot f(\gamma) = 0$">|; 

$key = q/displaystyleD=partialf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle D = \partial f$">|; 

$key = q/displaystyleH=partial^2varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle H = \partial^2 \varphi$">|; 

$key = q/displaystyleHapproxD^dualcdotD;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle H \approx D^\dual \cdot D$">|; 

$key = q/displaystyleJ=(partialvarphi)^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle J = (\partial \varphi)^\dual$">|; 

$key = q/displaystyleJ=D^dualcdotf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.32ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle J = D^\dual \cdot f$">|; 

$key = q/displaystyleJ_k+H_kcdots_kapprox0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle J_k + H_k \cdot s_k \approx 0$">|; 

$key = q/displaystyleJ_k-J_{k-1}approxH_kcdot(x_k-x_{k-1})=-alpha_kcdotH_kcdotp_{k-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle J_k - J_{k - 1} \approx H_k \cdot (x_k - x_{k - 1}) = - \alpha_k \cdot H_k \cdot p_{k - 1}$">|; 

$key = q/displaystylePhi_k=Phi(x_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \Phi_k = \Phi(x_k)$">|; 

$key = q/displaystylealpha_k=frac{J_k^dualcdotJ_k}{J_k^dualcdotH_kcdotJ_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.57ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \alpha_k = \frac{J_k^\dual \cdot J_k}{J_k^\dual \cdot H_k \cdot J_k}$">|; 

$key = q/displaystylealpha_k=frac{J_k^dualcdotp_k}{p_k^dualcdotH_kcdotp_k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.57ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \alpha_k = \frac{J_k^\dual \cdot p_k}{p_k^\dual \cdot H_k \cdot p_k}$">|; 

$key = q/displaystyleargmin_xunsur{2}f(x)^dualcdotf(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.87ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \arg\min_x \unsur{2} f(x)^\dual \cdot f(x)$">|; 

$key = q/displaystylebeta_k=frac{J_k^dualcdotH_kcdotp_{k-1}}{p_{k-1}^dualcdotH_kcdotp_{k-1}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.73ex; vertical-align: -2.50ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \beta_k = \frac{J_k^\dual \cdot H_k \cdot p_{k - 1}}{p_{k - 1}^\dual \cdot H_k \cdot p_{k - 1}}$">|; 

$key = q/displaystylebeta_kapproxfrac{J_k^dualcdot(J_k-J_{k-1})}{p_{k-1}^dualcdot(J_k-J_{k-1})};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.50ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \beta_k \approx \frac{J_k^\dual \cdot (J_k - J_{k - 1})}{p_{k - 1}^\dual \cdot (J_k - J_{k - 1})}$">|; 

$key = q/displaystyledelta_k=-J_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \delta_k = - J_k$">|; 

$key = q/displaystylep_0=J_0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle p_0 = J_0$">|; 

$key = q/displaystylep_k=J_k-beta_kcdotp_{k-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle p_k = J_k - \beta_k \cdot p_{k-1}$">|; 

$key = q/displaystylep_k^dualcdotH_kcdotp_{k-1}=J_k^dualcdotH_kcdotp_{k-1}-beta_kcdotp_{k-1}^dualcdotH_kcdotp_{k-1}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.83ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle p_k^\dual \cdot H_k \cdot p_{k - 1} = J_k^\dual \cdot H_k \cdot p_{k - 1} - \beta_k \cdot p_{k - 1}^\dual \cdot H_k \cdot p_{k - 1} = 0$">|; 

$key = q/displaystyles_k=-H_k^{-1}cdotJ_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.79ex; vertical-align: -0.74ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle s_k = - H_k^{-1} \cdot J_k$">|; 

$key = q/displaystyles_k=-[D_k^dualcdotD_k+lambda_kcdotI]^{-1}cdotD_k^dualcdotf_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle s_k = -[D_k^\dual \cdot D_k+ \lambda_k \cdot I]^{-1} \cdot D_k^\dual \cdot f_k$">|; 

$key = q/displaystyles_k=-[D_k^dualcdotD_k]^{-1}cdotD_k^dualcdotf_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle s_k = -[D_k^\dual \cdot D_k]^{-1} \cdot D_k^\dual \cdot f_k$">|; 

$key = q/displaystylevarphi_{k+1}approxvarphi_k+J_k^dualcdots_k+unsur{2}cdots_k^dualcdotH_kcdots_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \varphi_{k+1} \approx \varphi_k + J_k^\dual \cdot s_k + \unsur{2} \cdot s_k^\dual \cdot H_k \cdot s_k$">|; 

$key = q/displaystylevarphi_{k+1}approxvarphi_k+alpha_kcdotJ_k^dualcdotdelta_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \varphi_{k+1} \approx \varphi_k + \alpha_k \cdot J_k^\dual \cdot \delta_k$">|; 

$key = q/displaystylevarphi_{k+1}approxvarphi_k-alpha_kcdotJ_k^dualcdotp_k+frac{alpha_k^2}{2}cdotp_k^dualcdotH_kcdotp_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \varphi_{k + 1} \approx \varphi_k - \alpha_k \cdot J_k^\dual \cdot p_k + \frac{\alpha_k^2}{2} \cdot p_k^\dual \cdot H_k \cdot p_k$">|; 

$key = q/displaystylex_Napproxlim_{ktoinfty}x_k=argmin_{xinOmega}varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle x_N \approx \lim_{k \to \infty} x_k = \arg\min_{x \in \Omega} \varphi(x)$">|; 

$key = q/displaystylex_{k+1}=I(x_k)=x_k+p_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle x_{k + 1} = I(x_k) = x_k + p_k$">|; 

$key = q/displaystylex_{k+1}=x_k+alpha_kcdotdelta_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle x_{k + 1} = x_k + \alpha_k \cdot \delta_k$">|; 

$key = q/displaystylex_{k+1}=x_k+s_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.04ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle x_{k + 1} = x_k + s_k$">|; 

$key = q/displaystylex_{k+1}=x_k-alpha_kcdotJ_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle x_{k + 1} = x_k - \alpha_k \cdot J_k$">|; 

$key = q/displaystylex_{k+1}=x_k-alpha_kcdotp_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.05ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle x_{k+1} = x_k - \alpha_k \cdot p_k$">|; 

$key = q/f(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$f(x) = 0$">|; 

$key = q/f:setR^nmapstosetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img52.svg"
 ALT="$f : \setR^n \mapsto \setR^m$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img57.svg"
 ALT="$f$">|; 

$key = q/gammainS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\gamma \in S$">|; 

$key = q/k=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img36.svg"
 ALT="$k = 0$">|; 

$key = q/kge1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img38.svg"
 ALT="$k \ge 1$">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\lambda$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img44.svg"
 ALT="$n$">|; 

$key = q/p_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img39.svg"
 ALT="$p_k$">|; 

$key = q/p_kinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$p_k \in \setR^n$">|; 

$key = q/p_{k-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img41.svg"
 ALT="$p_{k - 1}$">|; 

$key = q/s_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img30.svg"
 ALT="$s_k$">|; 

$key = q/setR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\setR^n$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\varphi$">|; 

$key = q/varphi=f^dualcdotfslash2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\varphi = f^\dual \cdot f / 2$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img65.svg"
 ALT="$x$">|; 

$key = q/x^{(k)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img21.svg"
 ALT="$x^{(k)}$">|; 

$key = q/x_0inOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img3.svg"
 ALT="$x_0 \in \Omega$">|; 

$key = q/xinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img63.svg"
 ALT="$x \in \setR^n$">|; 

1;

