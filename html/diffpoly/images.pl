# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 33.14ex; " SRC="|."$dir".q|img28.svg"
 ALT="\begin{eqnarray*}
\abs{e(s,t)} &amp;\le&amp; \abs{s - t} \sum_{i = 0}^{n - 1} M^{n - 1 -...
...1} i \\\\
&amp;\le&amp; \unsur{2}  \abs{s - t}  M^{n - 2}  (n - 1)  n
\end{eqnarray*}">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$1$">|; 

$key = q/M=max{abs{alpha},abs{beta}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$M = \max \{ \abs{\alpha} , \abs{\beta} \}$">|; 

$key = q/[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$[\alpha,\beta]$">|; 

$key = q/abs{s},abs{t}leM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\abs{s}, \abs{t} \le M$">|; 

$key = q/alpha,betainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\alpha,\beta \in \setR$">|; 

$key = q/alphalebeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\alpha \le \beta$">|; 

$key = q/delta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\delta$">|; 

$key = q/delta=min{delta_0,delta_1,...,delta_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\delta = \min \{ \delta_0, \delta_1, ..., \delta_n \}$">|; 

$key = q/delta_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\delta_k$">|; 

$key = q/displaystyleOD{}{t}(t^n)=lim_{stot}frac{s^n-t^n}{s-t}=ncdott^{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.15ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \OD{}{t} (t^n) = \lim_{ s \to t} \frac{s^n - t^n}{s - t} = n \cdot t^{n - 1}$">|; 

$key = q/displaystyleOD{}{t}(t^n)=sum_{i=0}^{n-1}t^icdott^{n-1-i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.22ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \OD{}{t} (t^n) = \sum_{i = 0}^{n - 1} t^i \cdot t^{n - 1 - i}$">|; 

$key = q/displaystyleabs{e(s,t)}lefrac{M^{n-2}cdot(n-1)cdotncdotdelta}{2}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.18ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \abs{e(s,t)} \le \frac{ M^{n - 2} \cdot (n - 1) \cdot n \cdot \delta }{2} \le \epsilon$">|; 

$key = q/displaystyleabs{e_i(s,t)}lefrac{epsilon}{sum_jabs{a_j}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.32ex; vertical-align: -2.67ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \abs{e_i(s,t)} \le \frac{\epsilon}{\sum_j \abs{a_j}}$">|; 

$key = q/displaystyleabs{mu(s)-mu(t)-partialmu(t)}leabs{e(s,t)}cdotabs{s-t}leepsiloncdotabs{s-t};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \abs{\mu(s) - \mu(t) - \partial \mu(t)} \le \abs{e(s,t)} \cdot \abs{s - t} \le \epsilon \cdot \abs{s - t}$">|; 

$key = q/displaystyleabs{p(s)-p(t)-partialp(t)cdot(s-t)}leabs{s-t}cdotepsiloncdotfrac{sum_iabs{a_i}}{sum_jabs{a_j}}=abs{s-t}cdotepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.07ex; vertical-align: -2.67ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \abs{p(s) - p(t) - \partial p(t) \cdot (s - t)} \le \abs{s - t} \...
...cdot \frac{ \sum_i \abs{a_i} }{ \sum_j \abs{a_j} } = \abs{s - t} \cdot \epsilon$">|; 

$key = q/displaystyleabs{p(s)-p(t)-partialp(t)cdot(s-t)}leabs{s-t}sum_{i=0}^nabs{a_i}cdotabs{e_i(s,t)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \abs{p(s) - p(t) - \partial p(t) \cdot (s - t)} \le \abs{s - t} \sum_{i = 0}^n \abs{a_i} \cdot \abs{e_i(s,t)}$">|; 

$key = q/displaystyleabs{s-t}ledeltalefrac{2epsilon}{M^{n-2}cdot(n-1)cdotn};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \abs{s - t} \le \delta \le \frac{ 2 \epsilon}{ M^{n - 2} \cdot (n - 1) \cdot n }$">|; 

$key = q/displaystylee(s,t)=(s-t)sum_{i=0}^{n-1}t^{n-1-i}sum_{k=0}^{i-1}s^kt^{i-1-k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.30ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle e(s,t) = (s - t) \sum_{i = 0}^{n - 1} t^{n - 1 - i}  \sum_{k = 0}^{i - 1} s^k  t^{i - 1 - k}$">|; 

$key = q/displaystylee(s,t)=frac{s-t}{s-t}-1=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.96ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle e(s,t) = \frac{s - t}{s - t} - 1 = 0$">|; 

$key = q/displaystylee(s,t)=frac{s^n-t^n}{s-t}-OD{}{t}(t^n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.15ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle e(s,t) = \frac{s^n - t^n}{s - t} - \OD{}{t}(t^n)$">|; 

$key = q/displaystylee(s,t)=sum_{i=0}^{n-1}t^{n-1-i}(s-t)sum_{k=0}^{i-1}s^kt^{i-1-k};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.30ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle e(s,t) = \sum_{i = 0}^{n - 1} t^{n - 1 - i}  (s - t)  \sum_{k = 0}^{i - 1} s^k  t^{i - 1 - k}$">|; 

$key = q/displaystylefrac{s^n-t^n}{s-t}=sum_{i=0}^{n-1}s^icdott^{n-1-i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.22ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \frac{s^n - t^n}{s - t} = \sum_{i = 0}^{n - 1} s^i \cdot t^{n - 1 - i}$">|; 

$key = q/displaystylelim_{stot}frac{s^n-t^n}{s-t}=sum_{i=0}^{n-1}t^icdott^{n-1-i}=sum_{i=0}^{n-1}t^{n-1}=ncdott^{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.22ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \lim_{ s \to t} \frac{s^n - t^n}{s - t} = \sum_{i = 0}^{n - 1} t^i \cdot t^{n - 1 - i} = \sum_{i = 0}^{n - 1} t^{n - 1} = n \cdot t^{n - 1}$">|; 

$key = q/displaystylemu(s)-mu(t)-partialmu(t)=s^n-t^n-ncdott^{n-1}=e(s,t)cdot(s-t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \mu(s) - \mu(t) - \partial \mu(t) = s^n - t^n - n \cdot t^{n - 1} = e(s,t) \cdot (s - t)$">|; 

$key = q/displaystylemu:tmapstot^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.26ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \mu : t \mapsto t^n$">|; 

$key = q/displaystylep(x)=sum_{i=0}^na_icdotx^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle p(x) = \sum_{i = 0}^n a_i \cdot x^i$">|; 

$key = q/displaystyles^n-t^n=(s-t)sum_{i=0}^{n-1}s^icdott^{n-1-i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.22ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle s^n - t^n = (s - t) \sum_{i = 0}^{n - 1} s^i \cdot t^{n - 1 - i}$">|; 

$key = q/e_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img40.svg"
 ALT="$e_i$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img24.svg"
 ALT="$i$">|; 

$key = q/mu:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\mu : \setR \mapsto \setR$">|; 

$key = q/mu;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\mu$">|; 

$key = q/n=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$n = 1$">|; 

$key = q/nge2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img18.svg"
 ALT="$n \ge 2$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img1.svg"
 ALT="$n \in \setN$">|; 

$key = q/s-t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.81ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img23.svg"
 ALT="$s - t$">|; 

$key = q/s;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img35.svg"
 ALT="$s$">|; 

$key = q/s^i-t^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img21.svg"
 ALT="$s^i - t^i$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\setR$">|; 

$key = q/stot;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$s \to t$">|; 

$key = q/t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img36.svg"
 ALT="$t$">|; 

$key = q/tinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img4.svg"
 ALT="$t \in \setR$">|; 

$key = q/{eqnarraystar}e(s,t)&=&sum_{i=0}^{n-1}s^it^{n-1-i}-sum_{i=0}^{n-1}t^it^{n-1-i}&=&sum_{i=0}^{n-1}(s^i-t^i)t^{n-1-i}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 19.85ex; " SRC="|."$dir".q|img20.svg"
 ALT="\begin{eqnarray*}
e(s,t) &amp;=&amp; \sum_{i = 0}^{n - 1} s^i  t^{n - 1 - i} - \sum_{i ...
... 1 - i} \\\\
&amp;=&amp; \sum_{i = 0}^{n - 1} (s^i - t^i)  t^{n - 1 - i}
\end{eqnarray*}">|; 

1;

