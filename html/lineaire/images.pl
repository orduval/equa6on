# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \frac{ \norme{f(x)} }{ \norme{x} } = \frac{ \norme{f(x)} }{ N \cd...
...rme{ f\left( \frac{x}{ \lambda } \right) } = \frac{ \norme{f(u)} }{ \norme{u} }$">|; 

$key = q/(AcdotB)cdotD=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img175.svg"
 ALT="$(A \cdot B) \cdot D = I$">|; 

$key = q/(N,N);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img185.svg"
 ALT="$(N,N)$">|; 

$key = q/(canonique_1,...canonique_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img163.svg"
 ALT="$(\canonique_1,...\canonique_n)$">|; 

$key = q/(e_1,...,e_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$(e_1,...,e_n)$">|; 

$key = q/(f_1,...,f_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img86.svg"
 ALT="$(f_1,...,f_m)$">|; 

$key = q/(m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img139.svg"
 ALT="$(m,n)$">|; 

$key = q/(m,p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img141.svg"
 ALT="$(m,p)$">|; 

$key = q/(n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img171.svg"
 ALT="$(n,n)$">|; 

$key = q/(n,p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img140.svg"
 ALT="$(n,p)$">|; 

$key = q/(u,v)inE_1timesE_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$(u,v) \in E_1 \times E_2$">|; 

$key = q/(x_1,...,x_n)inE_1times...timesE_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$(x_1,...,x_n) \in E_1 \times ... \times E_n$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img63.svg"
 ALT="$0$">|; 

$key = q/2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img78.svg"
 ALT="$2$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img113.svg"
 ALT="$A$">|; 

$key = q/AB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img138.svg"
 ALT="$A B$">|; 

$key = q/A^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img172.svg"
 ALT="$A^{-1}$">|; 

$key = q/AcdotB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img132.svg"
 ALT="$A \cdot B$">|; 

$key = q/AcdotR=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img181.svg"
 ALT="$A \cdot R = I$">|; 

$key = q/Ainmatrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img99.svg"
 ALT="$A \in \matrice(\corps,m,n)$">|; 

$key = q/B,Cinmatrice(corps,n,p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img151.svg"
 ALT="$B,C \in \matrice(\corps,n,p)$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img135.svg"
 ALT="$B$">|; 

$key = q/B={xinA:norme{x}=delta};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$B = \{ x \in A : \norme{x} = \delta \}$">|; 

$key = q/Binmatrice(corps,n,p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$B \in \matrice(\corps,n,p)$">|; 

$key = q/Ccdot(AcdotB)=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img174.svg"
 ALT="$C \cdot (A \cdot B) = I$">|; 

$key = q/Cinmatrice(corps,m,p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img130.svg"
 ALT="$C \in \matrice(\corps,m,p)$">|; 

$key = q/Cinmatrice(corps,p,q);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img145.svg"
 ALT="$C \in \matrice(\corps,p,q)$">|; 

$key = q/Dinmatrice(corps,p,q);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img152.svg"
 ALT="$D \in \matrice(\corps,p,q)$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$E$">|; 

$key = q/E_1times...timesE_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img74.svg"
 ALT="$E_1 \times ... \times E_n$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$F$">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img170.svg"
 ALT="$I$">|; 

$key = q/I_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img169.svg"
 ALT="$I_n$">|; 

$key = q/Iinmatrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img167.svg"
 ALT="$I \in \matrice(\corps,m,n)$">|; 

$key = q/Iinmatrice(corps,n,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img159.svg"
 ALT="$I \in \matrice(\corps,n,n)$">|; 

$key = q/L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img178.svg"
 ALT="$L$">|; 

$key = q/LcdotA=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img179.svg"
 ALT="$L \cdot A = I$">|; 

$key = q/N=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img52.svg"
 ALT="$N = 1$">|; 

$key = q/Nincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img37.svg"
 ALT="$N \in \corps$">|; 

$key = q/Nstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img38.svg"
 ALT="$N \strictsuperieur 0$">|; 

$key = q/R;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img180.svg"
 ALT="$R$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img184.svg"
 ALT="$X$">|; 

$key = q/Xinmatrice(corps,N,N);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img189.svg"
 ALT="$X \in \matrice(\corps,N,N)$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img64.svg"
 ALT="$a$">|; 

$key = q/a_0,...,a_nincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img187.svg"
 ALT="$a_0,...,a_n \in \corps$">|; 

$key = q/a_{ij},b_{ij},c_{ij}inK;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img147.svg"
 ALT="$a_{ij},b_{ij},c_{ij} \in K$">|; 

$key = q/a_{ij},b_{ij}inK;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img118.svg"
 ALT="$a_{ij},b_{ij} \in K$">|; 

$key = q/a_{ij}incorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img91.svg"
 ALT="$a_{ij} \in \corps$">|; 

$key = q/alpha,betaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\alpha, \beta \in \corps$">|; 

$key = q/alphaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\alpha \in \corps$">|; 

$key = q/c_i=colonne_iA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img110.svg"
 ALT="$c_i = \colonne_i A$">|; 

$key = q/cdot;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\cdot$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\corps$">|; 

$key = q/corps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\corps^n$">|; 

$key = q/deltastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\delta \strictsuperieur 0$">|; 

$key = q/displaystyle(AcdotB)^T=B^TcdotA^T;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.78ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle (A \cdot B)^T = B^T \cdot A^T$">|; 

$key = q/displaystyle(AcdotB)^{-1}=B^{-1}cdotA^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img177.svg"
 ALT="$\displaystyle (A \cdot B)^{-1} = B^{-1} \cdot A^{-1}$">|; 

$key = q/displaystyle(AcdotB)cdotx=big(mathcal{A}circmathcal{B}big)(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle (A \cdot B) \cdot x = \big( \mathcal{A} \circ \mathcal{B} \big)(x)$">|; 

$key = q/displaystyleA=(a_{ij})_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\displaystyle A = ( a_{ij} )_{i,j}$">|; 

$key = q/displaystyleA=[c_1c_2...c_n];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\displaystyle A = [ c_1  c_2  ...  c_n ]$">|; 

$key = q/displaystyleA^{-1}cdotA=AcdotA^{-1}=I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img173.svg"
 ALT="$\displaystyle A^{-1} \cdot A = A \cdot A^{-1} = I$">|; 

$key = q/displaystyleA^{-k}=(A^{-1})^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img183.svg"
 ALT="$\displaystyle A^{-k} = (A^{-1})^k$">|; 

$key = q/displaystyleAcdot(BcdotC)=left(sum_{k,l}a_{ik}cdotb_{kl}cdotc_{lj}right)_{i,j}=(AcdotB)cdotC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.90ex; vertical-align: -3.77ex; " SRC="|."$dir".q|img148.svg"
 ALT="$\displaystyle A \cdot (B \cdot C) = \left( \sum_{k,l} a_{ik} \cdot b_{kl} \cdot c_{lj} \right)_{i,j} = (A \cdot B) \cdot C$">|; 

$key = q/displaystyleAcdotB=left(sum_{k=1}^na_{ik}cdotb_{kj}right)_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.60ex; vertical-align: -3.47ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\displaystyle A \cdot B = \left(\sum_{k=1}^n a_{ik} \cdot b_{kj}\right)_{i,j}$">|; 

$key = q/displaystyleAcdotBcdotC=Acdot(BcdotC)=(AcdotB)cdotC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img149.svg"
 ALT="$\displaystyle A \cdot B \cdot C = A \cdot (B \cdot C) = (A \cdot B) \cdot C$">|; 

$key = q/displaystyleAcdotBneBcdotA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle A \cdot B \ne B \cdot A$">|; 

$key = q/displaystyleAcdotx=left(sum_ja_{ij}cdotx_jright)_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.55ex; vertical-align: -3.42ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle A \cdot x = \left( \sum_j a_{ij} \cdot x_j \right)_i$">|; 

$key = q/displaystyleAcdotx=mathcal{A}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle A \cdot x = \mathcal{A}(x)$">|; 

$key = q/displaystyleAcdotx=sum_ic_icdotx_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle A \cdot x = \sum_i c_i \cdot x_i$">|; 

$key = q/displaystyleB={uinE:norme{u}=N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle B = \{ u \in E : \norme{u} = N \}$">|; 

$key = q/displaystyleC=D=B^{-1}cdotA^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img176.svg"
 ALT="$\displaystyle C = D = B^{-1} \cdot A^{-1}$">|; 

$key = q/displaystyleI=(indicatrice_{ij})_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img165.svg"
 ALT="$\displaystyle I = ( \indicatrice_{ij} )_{i,j}$">|; 

$key = q/displaystyleI=[canonique_1hdotscanonique_n];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img166.svg"
 ALT="$\displaystyle I = [\canonique_1  \hdots  \canonique_n]$">|; 

$key = q/displaystyleIcdotcanonique_i=canonique_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img164.svg"
 ALT="$\displaystyle I \cdot \canonique_i = \canonique_i$">|; 

$key = q/displaystyleIcdotx=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img161.svg"
 ALT="$\displaystyle I \cdot x = x$">|; 

$key = q/displaystyleM=max_{i,j}abs{composante_{ij}A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.94ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle M = \max_{i,j} \abs{\composante_{ij} A}$">|; 

$key = q/displaystyle[A,B]=AcdotB-BcdotA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img155.svg"
 ALT="$\displaystyle [A,B] = A \cdot B - B \cdot A$">|; 

$key = q/displaystyle[mathcal{A},mathcal{B}]=mathcal{A}circmathcal{B}-mathcal{B}circmathcal{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\displaystyle [\mathcal{A},\mathcal{B}] = \mathcal{A} \circ \mathcal{B} - \mathcal{B} \circ \mathcal{A}$">|; 

$key = q/displaystylealphacdotf^{-1}(u)+betacdotf^{-1}(v)=f^{-1}(alphacdotu+betacdotv);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \alpha \cdot f^{-1}(u) + \beta \cdot f^{-1}(v) = f^{-1}(\alpha \cdot u + \beta \cdot v)$">|; 

$key = q/displaystylecomposante_{ij}(AcdotB)=x_i^Tcdoty_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.59ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img144.svg"
 ALT="$\displaystyle \composante_{ij} (A \cdot B) = x_i^T \cdot y_j$">|; 

$key = q/displaystylecomposante_{ij}C=c_{ij}=sum_ka_{ik}cdotb_{kj};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.59ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle \composante_{ij} C = c_{ij} = \sum_k a_{ik} \cdot b_{kj}$">|; 

$key = q/displaystylef(...,alphax+betay,...)=alphacdotf(...,x,...)+betacdotf(...,y,...);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle f(...,\alpha x + \beta y,...) = \alpha \cdot f(...,x,...) + \beta \cdot f(...,y,...)$">|; 

$key = q/displaystylef(0)=f(0cdotx)=0cdotf(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle f(0) = f(0 \cdot x) = 0 \cdot f(x) = 0$">|; 

$key = q/displaystylef(alphacdotx+betacdoty)=alphacdotf(x)+betacdotf(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle f(\alpha \cdot x + \beta \cdot y) = \alpha \cdot f(x) + \beta \cdot f(y)$">|; 

$key = q/displaystylefrac{norme{alphacdotf(x)}}{norme{x}}=abs{alpha}cdotfrac{norme{f(x)}}{norme{x}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \frac{ \norme{\alpha \cdot f(x)} }{ \norme{x} } = \abs{\alpha} \cdot \frac{ \norme{f(x)} }{ \norme{x} }$">|; 

$key = q/displaystylefrac{norme{f(u)}}{norme{u}}lenorme{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \frac{ \norme{f(u)} }{ \norme{u} } \le \norme{f}$">|; 

$key = q/displaystylefrac{norme{f(x)}}{norme{x}}=frac{norme{f(u)}}{norme{u}}lesupBig{frac{norme{f(v)}}{norme{v}}:vinBBig};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \frac{ \norme{f(x)} }{ \norme{x} } = \frac{ \norme{f(u)} }{ \norme{u} } \le \sup \Big\{ \frac{ \norme{f(v)} }{ \norme{v} } : v \in B \Big\}$">|; 

$key = q/displaystylefrac{norme{gcircf(x)}}{norme{x}}lenorme{g}cdotnorme{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \frac{ \norme{g \circ f(x)} }{ \norme{x} } \le \norme{g} \cdot \norme{f}$">|; 

$key = q/displaystyleimageA=combilin{c_1,c_2,...,c_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle \image A = \combilin{c_1,c_2,...,c_n}$">|; 

$key = q/displaystyleimageA={Acdotx:xincorps^n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle \image A = \{ A \cdot x : x \in \corps^n \}$">|; 

$key = q/displaystylelambda=frac{norme{x}}{N};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.08ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \lambda = \frac{ \norme{x} }{N}$">|; 

$key = q/displaystylemathcal{A}(e_j)=sum_{i=1}^ma_{ij}cdotf_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle \mathcal{A}(e_j) = \sum_{i = 1}^m a_{ij} \cdot f_i$">|; 

$key = q/displaystylenorme{Acdotx}leMcdotmcdotncdotmax_ix_ileMcdotmcdotncdotnorme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.57ex; vertical-align: -1.74ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle \norme{A \cdot x} \le M \cdot m \cdot n \cdot \max_i x_i \le M \cdot m \cdot n \cdot \norme{x}$">|; 

$key = q/displaystylenorme{A}_2=supleft{frac{norme{Acdotx}}{norme{x}}:xincorps^n,xne0right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\displaystyle \norme{A}_2 = \sup \left\{ \frac{ \norme{A \cdot x} }{ \norme{x} } : x \in \corps^n,  x \ne 0 \right\}$">|; 

$key = q/displaystylenorme{A}leMcdotmcdotnstrictinferieurinfty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\displaystyle \norme{A} \le M \cdot m \cdot n \strictinferieur \infty$">|; 

$key = q/displaystylenorme{alphacdotf}=abs{alpha}cdotnorme{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \norme{\alpha \cdot f} = \abs{\alpha} \cdot \norme{f}$">|; 

$key = q/displaystylenorme{f(u,v)}lenorme{f}cdotnorme{u}cdotnorme{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle \norme{f(u,v)} \le \norme{f} \cdot \norme{u} \cdot \norme{v}$">|; 

$key = q/displaystylenorme{f(x)+g(x)}lenorme{f(x)}+norme{g(x)}lenorme{f}cdotnorme{x}+norme{g}cdotnorme{x}=(norme{f}+norme{g})cdotnorme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \norme{f(x) + g(x)} \le \norme{f(x)} + \norme{g(x)} \le \norme{f}...
...\norme{x} + \norme{g} \cdot \norme{x} = (\norme{f} + \norme{g}) \cdot \norme{x}$">|; 

$key = q/displaystylenorme{f(x)-f(0)}le1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle \norme{f(x) - f(0)} \le 1$">|; 

$key = q/displaystylenorme{f(x)-f(a)}=norme{f(x-a)}lenorme{f}cdotnorme{x-a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \norme{f(x) - f(a)} = \norme{f(x - a)} \le \norme{f} \cdot \norme{x-a}$">|; 

$key = q/displaystylenorme{f(x)}lenorme{f}cdotnorme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \norme{f(x)} \le \norme{f} \cdot \norme{x}$">|; 

$key = q/displaystylenorme{f(x_1,...,x_n)}lenorme{f}cdotprod_{i=1}^nnorme{x_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle \norme{f(x_1,...,x_n)} \le \norme{f} \cdot \prod_{i = 1}^n \norme{x_i}$">|; 

$key = q/displaystylenorme{f+g}lenorme{f}+norme{g};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \norme{f + g} \le \norme{f} + \norme{g}$">|; 

$key = q/displaystylenorme{f^n}=norme{fcirc...circf}lenorme{f}^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.56ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle \norme{f^n} = \norme{f \circ ... \circ f} \le \norme{f}^n$">|; 

$key = q/displaystylenorme{f}=sup_{xinB}frac{norme{f(x)}}{norme{x}}leunsur{delta}strictinferieur+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.71ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle \norme{f} = \sup_{x \in B} \frac{\norme{f(x)}}{\norme{x}} \le \unsur{\delta} \strictinferieur +\infty$">|; 

$key = q/displaystylenorme{f}=supleft{frac{norme{f(u,v)}}{norme{u}cdotnorme{v}}:(u,v)inE_1timesE_2right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle \norme{f} = \sup \left\{ \frac{ \norme{f(u,v)} }{ \norme{u} \cdot \norme{v} } : (u,v) \in E_1 \times E_2 \right\}$">|; 

$key = q/displaystylenorme{f}=supleft{frac{norme{f(x)}}{norme{x}}:xinE,xne0right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle \norme{f} = \sup \left\{ \frac{ \norme{f(x)} }{ \norme{x} } : x \in E,  x \ne 0 \right\}$">|; 

$key = q/displaystylenorme{f}=supleft{frac{norme{f(x_1,...,x_n)}}{prod_{i=1}^nnorme{x_i}}:(x_1,...,x_n)inE_1times...timesE_nright};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.81ex; vertical-align: -2.37ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \norme{f} = \sup \left\{ \frac{ \norme{f(x_1,...,x_n)} }{ \prod_{i = 1}^n \norme{x_i} } : (x_1,...,x_n) \in E_1 \times ... \times E_n \right\}$">|; 

$key = q/displaystylenorme{f}=supleft{norme{f(v)}:vinE,norme{v}=1right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \norme{f} = \sup \left\{ \norme{f(v)} : v \in E,  \norme{v} = 1 \right\}$">|; 

$key = q/displaystylenorme{f}_lineaire=norme{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.60ex; vertical-align: -0.77ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \norme{f}_\lineaire = \norme{f}$">|; 

$key = q/displaystylenorme{f}lesupBig{frac{norme{f(v)}}{norme{v}}:vinBBig};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \norme{f} \le \sup \Big\{ \frac{ \norme{f(v)} }{ \norme{v} } : v \in B \Big\}$">|; 

$key = q/displaystylenorme{gcircf(x)}lenorme{g}cdotnorme{f(x)}lenorme{g}cdotnorme{f}cdotnorme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \norme{g \circ f(x)} \le \norme{g} \cdot \norme{f(x)} \le \norme{g} \cdot \norme{f} \cdot \norme{x}$">|; 

$key = q/displaystylenorme{gcircf}lenorme{g}cdotnorme{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \norme{g \circ f} \le \norme{g} \cdot \norme{f}$">|; 

$key = q/displaystylenorme{u}=norme{unsur{lambda}cdotx}=unsur{lambda}cdotnorme{x}=frac{N}{norme{x}}cdotnorme{x}=N;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \norme{u} = \norme{\unsur{\lambda} \cdot x} = \unsur{\lambda} \cdot \norme{x} = \frac{N}{ \norme{x} } \cdot \norme{x} = N$">|; 

$key = q/displaystylenoyauA={xincorps^n:Acdotx=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle \noyau A = \{ x \in \corps^n : A \cdot x = 0 \}$">|; 

$key = q/displaystylep(X)=sum_{i=0}^na_icdotX^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img188.svg"
 ALT="$\displaystyle p(X) = \sum_{i = 0}^n a_i \cdot X^i$">|; 

$key = q/displaystylesupBig{frac{norme{f(v)}}{norme{v}}:vinBBig}lenorme{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \sup \Big\{ \frac{ \norme{f(v)} }{ \norme{v} } : v \in B \Big\} \le \norme{f}$">|; 

$key = q/displaystylesup_{xinB}frac{norme{f(x)}}{norme{x}}=unsur{delta}sup_{xinB}norme{f(x)-f(0)}leunsur{delta};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.71ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \sup_{x \in B} \frac{\norme{f(x)}}{\norme{x}} = \unsur{\delta} \sup_{x \in B} \norme{f(x) - f(0)} \le \unsur{\delta}$">|; 

$key = q/displaystylesupleft{frac{norme{f(v)}}{norme{v}}:vinBright}=norme{f};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle \sup \left\{ \frac{ \norme{f(v)} }{ \norme{v} } : v \in B \right\} = \norme{f}$">|; 

$key = q/displaystyleu=frac{x}{lambda};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle u = \frac{x}{\lambda}$">|; 

$key = q/displaystyley=mathcal{A}(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle y = \mathcal{A}(x)$">|; 

$key = q/displaystyley=sum_{i=1}^mf_isum_{j=1}^na_{ij}cdotx_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle y = \sum_{i = 1}^m f_i \sum_{j = 1}^n a_{ij} \cdot x_j$">|; 

$key = q/displaystyley=sum_{j=1}^nmathcal{A}(e_j)cdotx_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle y = \sum_{j = 1}^n \mathcal{A}(e_j) \cdot x_j$">|; 

$key = q/displaystyley_i=sum_{j=1}^na_{ij}cdotx_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.19ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle y_i = \sum_{j = 1}^n a_{ij} \cdot x_j$">|; 

$key = q/displaystylez_i=sum_ka_{ik}cdoty_k=sum_ka_{ik}sum_jb_{kj}cdotx_j=sum_{k,j}a_{ik}cdotb_{kj}cdotx_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -3.37ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\displaystyle z_i = \sum_k a_{ik} \cdot y_k = \sum_k a_{ik} \sum_j b_{kj} \cdot x_j = \sum_{k,j} a_{ik} \cdot b_{kj} \cdot x_j$">|; 

$key = q/distance(x,0)=norme{x}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\distance(x,0) = \norme{x} \le \delta$">|; 

$key = q/f(0)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$f(0) = 0$">|; 

$key = q/f(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$f(x) = 0$">|; 

$key = q/f(x)inF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$f(x) \in F$">|; 

$key = q/f,g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$f,g$">|; 

$key = q/f:E_1times...timesE_nmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img71.svg"
 ALT="$f : E_1 \times ... \times E_n \mapsto F$">|; 

$key = q/f:E_1timesE_2mapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img79.svg"
 ALT="$f : E_1 \times E_2 \mapsto F$">|; 

$key = q/f:EmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$f : E \mapsto F$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$f$">|; 

$key = q/f=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$f = 0$">|; 

$key = q/f^{-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.48ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$f^{-1}$">|; 

$key = q/f_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img93.svg"
 ALT="$f_i$">|; 

$key = q/finlineaire(A,B);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$f \in \lineaire(A,B)$">|; 

$key = q/g:FmapstoG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img54.svg"
 ALT="$g : F \mapsto G$">|; 

$key = q/i^{ème};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img96.svg"
 ALT="$i^{ème}$">|; 

$key = q/identite;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img160.svg"
 ALT="$\identite$">|; 

$key = q/lineaire(E,F);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\lineaire(E,F)$">|; 

$key = q/lineaire_n(E_1,...,E_n,F);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\lineaire_n(E_1,...,E_n,F)$">|; 

$key = q/m,nstrictinferieurinfty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img108.svg"
 ALT="$m,n \strictinferieur \infty$">|; 

$key = q/mathcal{A}(e_j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\mathcal{A}(e_j)$">|; 

$key = q/mathcal{A}:EtoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\mathcal{A} : E \to F$">|; 

$key = q/mathcal{A}:corps^ntocorps^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\mathcal{A} : \corps^n \to \corps^m$">|; 

$key = q/mathcal{A};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\mathcal{A}$">|; 

$key = q/mathcal{B}:corps^ptocorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\mathcal{B} : \corps^p \to \corps^n$">|; 

$key = q/mathcal{C}=mathcal{A}circmathcal{B};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\mathcal{C} = \mathcal{A} \circ \mathcal{B}$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$n$">|; 

$key = q/norme{f(x)}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\norme{f(x)} = 0$">|; 

$key = q/norme{f}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\norme{f} = 0$">|; 

$key = q/norme{f}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\norme{f} \ge 0$">|; 

$key = q/norme{x};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\norme{x}$">|; 

$key = q/norme{x}ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\norme{x} \ne 0$">|; 

$key = q/p:matrice(corps,N,N)mapstomatrice(corps,N,N);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img186.svg"
 ALT="$p : \matrice(\corps,N,N) \mapsto \matrice(\corps,N,N)$">|; 

$key = q/p=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img136.svg"
 ALT="$p = 1$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img49.svg"
 ALT="$u$">|; 

$key = q/u=f(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$u = f(x)$">|; 

$key = q/uinB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img43.svg"
 ALT="$u \in B$">|; 

$key = q/v=f(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$v = f(y)$">|; 

$key = q/x,y,z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img125.svg"
 ALT="$x,y,z$">|; 

$key = q/x,yinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$x,y \in E$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img47.svg"
 ALT="$x$">|; 

$key = q/x=f^{-1}(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$x = f^{-1}(u)$">|; 

$key = q/x_i,y_iincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img88.svg"
 ALT="$x_i,y_i \in \corps$">|; 

$key = q/x_i^T=ligne_i(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img142.svg"
 ALT="$x_i^T = \ligne_i(A)$">|; 

$key = q/xinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img17.svg"
 ALT="$x \in E$">|; 

$key = q/xinEsetminus{0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$x \in E \setminus \{ 0 \}$">|; 

$key = q/xincorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img162.svg"
 ALT="$x \in \corps^n$">|; 

$key = q/xincorps^p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img122.svg"
 ALT="$x \in \corps^p$">|; 

$key = q/xne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img26.svg"
 ALT="$x \ne 0$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img97.svg"
 ALT="$y$">|; 

$key = q/y=f^{-1}(v);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$y = f^{-1}(v)$">|; 

$key = q/y_j=colonne_j(B);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img143.svg"
 ALT="$y_j = \colonne_j(B)$">|; 

$key = q/yincorps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img123.svg"
 ALT="$y \in \corps^n$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img127.svg"
 ALT="$z$">|; 

$key = q/zincorps^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img124.svg"
 ALT="$z \in \corps^m$">|; 

$key = q/{Eqts}A=(a_{ij})_{i,j}B=(b_{ij})_{i,j}C=(c_{ij})_{i,j}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img146.svg"
 ALT="\begin{Eqts}
A = ( a_{ij} )_{i,j} \\\\
B = ( b_{ij} )_{i,j} \\\\
C = ( c_{ij} )_{i,j}
\end{Eqts}">|; 

$key = q/{Eqts}A=(a_{ij})_{i,j}B=(b_{ij})_{i,j}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img117.svg"
 ALT="\begin{Eqts}
A = ( a_{ij} )_{i,j} \\\\
B = ( b_{ij} )_{i,j}
\end{Eqts}">|; 

$key = q/{Eqts}A^0=IA^k=AcdotA^{k-1}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.63ex; vertical-align: -2.75ex; " SRC="|."$dir".q|img182.svg"
 ALT="\begin{Eqts}
A^0 = I \\\\
A^k = A \cdot A^{k-1}
\end{Eqts}">|; 

$key = q/{Eqts}Acdot(B+C)=AcdotB+AcdotC(B+C)cdotD=BcdotD+CcdotD{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img150.svg"
 ALT="\begin{Eqts}
A \cdot (B + C) = A \cdot B + A \cdot C \\\\
(B + C) \cdot D = B \cdot D + C \cdot D
\end{Eqts}">|; 

$key = q/{Eqts}AcdotI=AIcdotB=B{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img168.svg"
 ALT="\begin{Eqts}
A \cdot I = A \\\\
I \cdot B = B
\end{Eqts}">|; 

$key = q/{Eqts}Matrix{{cc}A_1&00&A_2Matrix{cdotMatrix{{cc}B_1&00&B_2Matrix{=Matrix{{cc}A_1cdotB_1&00&A_2cdotB_2Matrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.79ex; vertical-align: -2.32ex; " SRC="|."$dir".q|img158.svg"
 ALT="\begin{Eqts}
\begin{Matrix}{cc}
A_1 &amp; 0 \ 0 &amp; A_2
\end{Matrix}\cdot
\begin{Matr...
...begin{Matrix}{cc}
A_1 \cdot B_1 &amp; 0 \\\\
0 &amp; A_2 \cdot B_2
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}mathcal{B}(x)=Bcdotxmathcal{A}(y)=Acdoty{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img121.svg"
 ALT="\begin{Eqts}
\mathcal{B}(x) = B \cdot x \\\\
\mathcal{A}(y) = A \cdot y
\end{Eqts}">|; 

$key = q/{Eqts}x=sum_{i=1}^nx_icdote_iy=sum_{i=1}^my_icdotf_i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.36ex; vertical-align: -6.63ex; " SRC="|."$dir".q|img87.svg"
 ALT="\begin{Eqts}
x = \sum_{i = 1}^n x_i \cdot e_i \\\\
y = \sum_{i = 1}^m y_i \cdot f_i
\end{Eqts}">|; 

$key = q/{Eqts}y=mathcal{B}(x)z=mathcal{A}(y)=big(mathcal{A}circmathcal{B}big)(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.57ex; vertical-align: -2.72ex; " SRC="|."$dir".q|img126.svg"
 ALT="\begin{Eqts}
y = \mathcal{B}(x) \\\\
z = \mathcal{A}(y) = \big( \mathcal{A} \circ \mathcal{B} \big)(x)
\end{Eqts}">|; 

1;

