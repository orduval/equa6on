# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 21.70ex; " SRC="|."$dir".q|img73.svg"
 ALT="\begin{eqnarray*}
h(a) &amp;=&amp; f(b) \cdot g(a) - f(a) \cdot g(a) - f(a)\cdot g(b) + ...
... g(b) + f(b)\cdot g(a) \\\\
&amp;=&amp; f(b) \cdot g(a) - f(a) \cdot g(b)
\end{eqnarray*}">|; 

$key = q/-hinboule(0,delta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$-h \in \boule(0,\delta)$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img96.svg"
 ALT="$0$">|; 

$key = q/AsubseteqsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img2.svg"
 ALT="$A \subseteq \setR^n$">|; 

$key = q/F,G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img80.svg"
 ALT="$F,G$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img88.svg"
 ALT="$F$">|; 

$key = q/G;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img89.svg"
 ALT="$G$">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img94.svg"
 ALT="$I$">|; 

$key = q/I=[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img81.svg"
 ALT="$I=[\alpha,\beta]$">|; 

$key = q/Isetminus{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$I\setminus \{a\}$">|; 

$key = q/N={hinsetR^n:norme{h}=gamma};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$N = \{ h \in \setR^n : \norme{h} = \gamma \}$">|; 

$key = q/[0,1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$[0,1]$">|; 

$key = q/[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$[\alpha,\beta]$">|; 

$key = q/[u,v];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$[u,v]$">|; 

$key = q/]alpha,beta[;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$]\alpha,\beta[$">|; 

$key = q/a,binsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img70.svg"
 ALT="$a,b \in \setR$">|; 

$key = q/a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img84.svg"
 ALT="$a$">|; 

$key = q/abs{gamma-s}leabs{t-s}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\abs{\gamma - s} \le \abs{t - s} \le \delta$">|; 

$key = q/abs{partialf(gamma)-partialf(s)}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\abs{\partial f(\gamma) - \partial f(s)} \le \epsilon$">|; 

$key = q/abs{s-t}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img104.svg"
 ALT="$\abs{s - t} \le \delta$">|; 

$key = q/aininterieurA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img4.svg"
 ALT="$a \in \interieur A$">|; 

$key = q/aininterieurI;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img83.svg"
 ALT="$a \in \interieur I$">|; 

$key = q/b=a+hinIsetminus{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img87.svg"
 ALT="$b = a+h\in I\setminus \{a\}$">|; 

$key = q/boule(0,delta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\boule(0,\delta)$">|; 

$key = q/cinintervalleouvert{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$c \in \intervalleouvert{a}{b}$">|; 

$key = q/delta=min{delta_1,delta_2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\delta = \min \{ \delta_1,\delta_2 \}$">|; 

$key = q/delta_1strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\delta_1 \strictsuperieur 0$">|; 

$key = q/delta_2strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\delta_2 \strictsuperieur 0$">|; 

$key = q/deltastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\delta \strictsuperieur 0$">|; 

$key = q/displaystyle0=partialg(c)=partialf(c)-frac{f(b)-f(a)}{b-a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.28ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle 0 = \partial g(c) = \partial f(c) - \frac{f(b) - f(a)}{b - a}$">|; 

$key = q/displaystyle0=partialh(c)=[f(b)-f(a)]cdotpartialg(c)-partialf(c)cdot[g(b)-g(a)];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle 0 = \partial h(c) = [f(b) - f(a)] \cdot \partial g(c) - \partial f(c) \cdot [ g(b) - g(a) ]$">|; 

$key = q/displaystyleF(a)=G(a)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle F(a) = G(a) = 0$">|; 

$key = q/displaystyleF(b)cdotpartialG(a+tcdoth)=G(b)cdotpartialF(a+tcdoth);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle F(b) \cdot \partial G(a + t \cdot h) = G(b) \cdot \partial F(a + t \cdot h)$">|; 

$key = q/displaystyleOD{f}{x}=frac{differencef}{differencex};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \OD{f}{x} = \frac{\difference f}{\difference x}$">|; 

$key = q/displaystyle[F(b)-F(a)]cdotpartialG(a+tcdoth)=[G(b)-G(a)]cdotpartialF(a+tcdoth);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle [F(b) - F(a)] \cdot \partial G(a + t \cdot h) = [G(b) - G(a)] \cdot \partial F(a + t \cdot h)$">|; 

$key = q/displaystyleabs{E(h)}leepsiloncdotabs{h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \abs{E(h)} \le \epsilon \cdot \abs{h}$">|; 

$key = q/displaystyleabs{differentielle{f}{a}(h)}leepsiloncdotnorme{h};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.03ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \abs{\differentielle{f}{a}(h)} \le \epsilon \cdot \norme{h}$">|; 

$key = q/displaystyleabs{f(t)-f(s)-partialf(s)cdot(t-s)}leepsiloncdotabs{t-s};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle \abs{f(t) - f(s) - \partial f(s) \cdot (t - s)} \le \epsilon \cdot \abs{t - s}$">|; 

$key = q/displaystyleabs{f(t)-f(t)-partialf(t)cdot(t-t)}=0leepsiloncdot(t-t)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle \abs{f(t) - f(t) - \partial f(t) \cdot (t - t)} = 0 \le \epsilon \cdot (t - t) = 0$">|; 

$key = q/displaystyleabs{frac{f(t)-f(s)}{t-s}-partialf(s)}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle \abs{\frac{f(t) - f(s)}{t - s} - \partial f(s)} \le \epsilon$">|; 

$key = q/displaystyleabs{partialf(s)-partialf(t)}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle \abs{\partial f(s) - \partial f(t)} \le \epsilon$">|; 

$key = q/displaystyledifferentielle{f}{a}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.79ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \differentielle{f}{a} = 0$">|; 

$key = q/displaystylef(a)lef(a+h);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle f(a) \le f(a + h)$">|; 

$key = q/displaystylef(v)-f(u)=partialf(u+scdot(v-u))cdot(v-u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle f(v) - f(u) = \partial f(u + s \cdot (v - u)) \cdot (v - u)$">|; 

$key = q/displaystylef(v)-f(u)=partialf(w)cdot(v-u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle f(v) - f(u) = \partial f(w) \cdot (v - u)$">|; 

$key = q/displaystylef(x)=lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle f(x) = \lambda$">|; 

$key = q/displaystylefrac{df}{dg}=frac{differencef}{differenceg};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.40ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \frac{df}{dg} = \frac{\difference f}{\difference g}$">|; 

$key = q/displaystylefrac{f(t)-f(s)}{t-s}-partialf(s)=partialf(gamma)-partialf(s);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.28ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\displaystyle \frac{f(t) - f(s)}{t - s} - \partial f(s) = \partial f(\gamma) - \partial f(s)$">|; 

$key = q/displaystylefrac{partialF(a+tcdoth)}{partialG(a+tcdoth)}=frac{F(b)}{F(a)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle \frac{ \partial F(a + t \cdot h) }{ \partial G(a + t \cdot h) } = \frac{F(b)}{F(a)}$">|; 

$key = q/displaystylefrac{partialg(c)}{partialf(c)}=frac{g(b)-g(a)}{f(b)-f(a)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle \frac{\partial g(c)}{\partial f(c)} = \frac{g(b) - g(a)}{f(b) - f(a)}$">|; 

$key = q/displaystyleg(t)=(fcircphi)(t)=f(u+tcdot(v-u));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle g(t) = (f \circ \phi)(t) = f( u + t \cdot (v - u))$">|; 

$key = q/displaystyleg(x)=f(x)-frac{f(b)-f(a)}{b-a}cdot(x-a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.28ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle g(x) = f(x) - \frac{f(b) - f(a)}{b - a} \cdot (x - a)$">|; 

$key = q/displaystyleh(a)=h(b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle h(a) = h(b)$">|; 

$key = q/displaystyleh(x)=[f(b)-f(a)]cdotg(x)-f(x)cdot[g(b)-g(a)];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle h(x) = [f(b) - f(a)] \cdot g(x) - f(x) \cdot [ g(b) - g(a) ]$">|; 

$key = q/displaystylelambdainintervalleouvert{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \lambda \in \intervalleouvert{a}{b}$">|; 

$key = q/displaystylelambdalef(x)lesigma=lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \lambda \le f(x) \le \sigma = \lambda$">|; 

$key = q/displaystyleleft[f(b)-f(a)right]cdotpartialg(c)=partialf(c)cdotleft[g(b)-g(a)right];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle \left[ f(b) - f(a) \right] \cdot \partial g(c) = \partial f(c) \cdot \left[ g(b) - g(a) \right]$">|; 

$key = q/displaystylelim_{xtoa}frac{F(x)}{G(x)}=lim_{xtoa}frac{partialF(a)}{partialG(a)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.66ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle \lim_{x \to a} \frac{F(x)}{G(x)} = \lim_{x \to a} \frac{\partial F(a)}{\partial G(a)}$">|; 

$key = q/displaystylenorme{differentielle{f}{a}}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.03ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle \norme{\differentielle{f}{a}} = 0$">|; 

$key = q/displaystylenorme{differentielle{f}{a}}=supleft{unsur{gamma}norme{differentielle{f}{a}(h)}:hinNright};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \norme{\differentielle{f}{a}} = \sup \left\{ \unsur{\gamma} \norme{\differentielle{f}{a}(h)} : h \in N \right\}$">|; 

$key = q/displaystylepartialf(c)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \partial f(c) = 0$">|; 

$key = q/displaystylepartialf(c)=frac{f(b)-f(a)}{b-a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.28ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \partial f(c) = \frac{f(b)-f(a)}{b-a}$">|; 

$key = q/displaystylepartialf(gamma)=frac{f(t)-f(s)}{t-s};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.28ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle \partial f(\gamma) = \frac{f(t) - f(s)}{t - s}$">|; 

$key = q/displaystylepartialf(lambda)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \partial f(\lambda) = 0$">|; 

$key = q/displaystylepartialf(lambda)=partialf(sigma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \partial f(\lambda) = \partial f(\sigma) = 0$">|; 

$key = q/displaystylepartialf(sigma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \partial f(\sigma) = 0$">|; 

$key = q/displaystylepartialg_i(s)=frac{g_i(1)-g_i(0)}{1-0}=g_i(1)-g_i(0)=f_i(v)-f_i(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.28ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \partial g_i(s) = \frac{g_i(1) - g_i(0)}{1 - 0} = g_i(1) - g_i(0) = f_i(v) - f_i(u)$">|; 

$key = q/displaystylepartialg_i(s)=sum_jpartial_jf_i(u+scdot(v-u))cdot(v_j-u_j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \partial g_i(s) = \sum_j \partial_j f_i(u + s \cdot (v - u)) \cdot (v_j - u_j)$">|; 

$key = q/displaystylephi(t)=u+tcdot(v-u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \phi(t) = u + t \cdot (v - u)$">|; 

$key = q/displaystylesigmainintervalleouvert{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle \sigma \in \intervalleouvert{a}{b}$">|; 

$key = q/displaystyleunsur{gamma}abs{differentielle{f}{a}(h)}leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.29ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle \unsur{\gamma} \abs{\differentielle{f}{a}(h)} \le \epsilon$">|; 

$key = q/displaystyle{lambda,sigma}subseteqintervalleouvert{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \{\lambda,\sigma\} \subseteq \intervalleouvert{a}{b}$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/f(a)=f(b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$f(a) = f(b)$">|; 

$key = q/f(b)nef(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$f(b) \ne f(a)$">|; 

$key = q/f,gincontinue^1([a,b],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img69.svg"
 ALT="$f,g \in \continue^1([a,b],\setR)$">|; 

$key = q/f:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f : A \mapsto \setR$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$f$">|; 

$key = q/fincontinue^1([a,b],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$f \in \continue^1([a,b],\setR)$">|; 

$key = q/fincontinue^1([alpha,beta],setR);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img99.svg"
 ALT="$f \in \continue^1([\alpha,\beta],\setR)$">|; 

$key = q/fincontinue^1(setR^m,setR^n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.74ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$f \in \continue^1(\setR^m,\setR^n)$">|; 

$key = q/g(a)=g(b)=f(a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$g(a) = g(b) = f(a)$">|; 

$key = q/g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$g$">|; 

$key = q/g_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img59.svg"
 ALT="$g_i$">|; 

$key = q/gamma=deltaslash2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\gamma = \delta / 2$">|; 

$key = q/gammain]s,t[;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\gamma \in ]s,t[$">|; 

$key = q/h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img71.svg"
 ALT="$h$">|; 

$key = q/hinN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img23.svg"
 ALT="$h \in N$">|; 

$key = q/hinboule(0,delta);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$h \in \boule(0,\delta)$">|; 

$key = q/hinboule(0,delta_1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$h \in \boule(0,\delta_1)$">|; 

$key = q/hinboule(0,delta_2);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$h \in \boule(0,\delta_2)$">|; 

$key = q/hne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img86.svg"
 ALT="$h \ne 0$">|; 

$key = q/lambda,sigmain[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\lambda,\sigma \in [a,b]$">|; 

$key = q/lambda=f(a)=f(b)=sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\lambda = f(a) = f(b) = \sigma$">|; 

$key = q/lambda=f(a)=f(b)strictinferieursigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\lambda = f(a) = f(b) \strictinferieur \sigma$">|; 

$key = q/lambdastrictinferieurf(a)=f(b)=sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\lambda \strictinferieur f(a) = f(b) = \sigma$">|; 

$key = q/lambdastrictinferieurf(a)=f(b)strictinferieursigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\lambda \strictinferieur f(a) = f(b) \strictinferieur \sigma$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img51.svg"
 ALT="$n$">|; 

$key = q/partialG;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\partial G$">|; 

$key = q/partialf(c)ne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\partial f(c) \ne 0$">|; 

$key = q/partialf;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\partial f$">|; 

$key = q/partialf=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\partial f = 0$">|; 

$key = q/phi:[0,1]mapstosetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\phi : [0,1] \mapsto \setR^m$">|; 

$key = q/s,tin[alpha,beta];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$s,t \in [\alpha,\beta]$">|; 

$key = q/s=t;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.62ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img105.svg"
 ALT="$s = t$">|; 

$key = q/sinintervalleouvert{0}{1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$s \in \intervalleouvert{0}{1}$">|; 

$key = q/snet;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img107.svg"
 ALT="$s \ne t$">|; 

$key = q/sstrictinferieurt;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.68ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img108.svg"
 ALT="$s \strictinferieur t$">|; 

$key = q/tin[0,1];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$t \in [0,1]$">|; 

$key = q/tinintervalleouvert{0}{1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img90.svg"
 ALT="$t \in \intervalleouvert{0}{1}$">|; 

$key = q/u,vinsetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img53.svg"
 ALT="$u,v \in \setR^m$">|; 

$key = q/win[u,v]subseteqsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$w \in [u,v] \subseteq \setR^n$">|; 

$key = q/xin[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$x \in [a,b]$">|; 

$key = q/{Eqts}differentielle{f}{a}(h)=f(a+h)-f(a)-E(h)ge-E(h)ge-epsiloncdotnorme{-h}differentielle{f}{a}(h)=f(a)-f(a-h)+E(-h)leE(-h)leepsiloncdotnorme{h}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.70ex; vertical-align: -2.78ex; " SRC="|."$dir".q|img16.svg"
 ALT="\begin{Eqts}
\differentielle{f}{a}(h) = f(a + h) - f(a) - E(h) \ge - E(h) \ge - ...
...}(h) = f(a) - f(a - h) + E(-h) \le E(-h) \le \epsilon \cdot \norme{h}
\end{Eqts}">|; 

$key = q/{Eqts}f(a+h)-f(a)=differentielle{f}{a}(h)+E(h)f(a-h)-f(a)=-differentielle{f}{a}(h)+E(-h){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.70ex; vertical-align: -2.78ex; " SRC="|."$dir".q|img5.svg"
 ALT="\begin{Eqts}
f(a + h) - f(a) = \differentielle{f}{a}(h) + E(h) \\\\
f(a - h) - f(a) = - \differentielle{f}{a}(h) + E(-h)
\end{Eqts}">|; 

$key = q/{Eqts}f(lambda)=minf([a,b])f(sigma)=maxf([a,b]){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img29.svg"
 ALT="\begin{Eqts}
f(\lambda) = \min f([a,b]) \\\\
f(\sigma) = \max f([a,b])
\end{Eqts}">|; 

1;

