# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(m,n)inDelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$(m,n) \in \Delta$">|; 

$key = q/(setN,+);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img237.svg"
 ALT="$(\setN,+)$">|; 

$key = q/(setN,cdot);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img238.svg"
 ALT="$(\setN,\cdot)$">|; 

$key = q/+...+-...-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img28.svg"
 ALT="$+...+-...-$">|; 

$key = q/+...+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img8.svg"
 ALT="$+...+$">|; 

$key = q/+.....+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img21.svg"
 ALT="$+.....+$">|; 

$key = q/+:setNtimessetNmapstosetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img4.svg"
 ALT="$+ : \setN \times \setN \mapsto \setN$">|; 

$key = q/-...-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img74.svg"
 ALT="$-...-$">|; 

$key = q/0+m=m+0=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img18.svg"
 ALT="$0 + m = m + 0 = m$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$0$">|; 

$key = q/0^{+...+}=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$0^{+...+} = m$">|; 

$key = q/0cdotm=mcdot0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img104.svg"
 ALT="$0 \cdot m = m \cdot 0 = 0$">|; 

$key = q/1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img109.svg"
 ALT="$1$">|; 

$key = q/1cdotm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img116.svg"
 ALT="$1 \cdot m$">|; 

$key = q/1cdotm=mcdot1=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img118.svg"
 ALT="$1 \cdot m = m \cdot 1 = m$">|; 

$key = q/A_{mn};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img168.svg"
 ALT="$A_{mn}$">|; 

$key = q/a,b,c,dinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img57.svg"
 ALT="$a,b,c,d \in \setN$">|; 

$key = q/a_1,a_2insetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img139.svg"
 ALT="$a_1, a_2 \in \setN$">|; 

$key = q/a_1=a_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img146.svg"
 ALT="$a_1 = a_2$">|; 

$key = q/cdot:setNtimessetNmapstosetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\cdot : \setN \times \setN \mapsto \setN$">|; 

$key = q/displaystyle(a_1-a_2)cdot1=a_1-a_2=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img145.svg"
 ALT="$\displaystyle (a_1 - a_2) \cdot 1 = a_1 - a_2 = 0$">|; 

$key = q/displaystyle(a_1-a_2)cdotn=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle (a_1 - a_2) \cdot n = 0$">|; 

$key = q/displaystyle(i_0,i_1,i_2,...i_{n-1},i_n)in{0,1,2,3,4,5,6,7,8,9}^{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img158.svg"
 ALT="$\displaystyle (i_0,i_1,i_2,...i_{n - 1},i_n) \in \{ 0,1,2,3,4,5,6,7,8,9 \}^{n + 1}$">|; 

$key = q/displaystyle(k+1)cdotn=kcdotn+nstrictsuperieurkcdotn+r=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img202.svg"
 ALT="$\displaystyle (k + 1) \cdot n = k \cdot n + n \strictsuperieur k \cdot n + r = m$">|; 

$key = q/displaystyle(m+1)cdot1=m+1strictsuperieurm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img178.svg"
 ALT="$\displaystyle (m + 1) \cdot 1 = m + 1 \strictsuperieur m$">|; 

$key = q/displaystyle(m+n)-p=m+(n-p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle (m + n) - p = m + (n - p)$">|; 

$key = q/displaystyle(m-n)-p=m-(n+p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\displaystyle (m - n) - p = m - (n + p)$">|; 

$key = q/displaystyle(mcdoti)diventiere(ncdotj)=(mdiventieren)cdot(idiventierej);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img214.svg"
 ALT="$\displaystyle (m \cdot i) \diventiere (n \cdot j) = (m \diventiere n) \cdot (i \diventiere j)$">|; 

$key = q/displaystyle(mcdoti)diventiere(ncdotj)=qcdotk;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img213.svg"
 ALT="$\displaystyle (m \cdot i) \diventiere (n \cdot j) = q \cdot k$">|; 

$key = q/displaystyle(mdiventieren)cdotnlem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img182.svg"
 ALT="$\displaystyle (m \diventiere n) \cdot n \le m$">|; 

$key = q/displaystyle(n+p)cdotm=ncdotm+pcdotm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\displaystyle (n + p) \cdot m = n \cdot m + p \cdot m$">|; 

$key = q/displaystyle(n-p)cdotm=ncdotm-pcdotm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\displaystyle (n - p) \cdot m = n \cdot m - p \cdot m$">|; 

$key = q/displaystyle0+m=0^{+...+};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.26ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle 0 + m = 0^{+...+}$">|; 

$key = q/displaystyle0+m=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle 0 + m = m$">|; 

$key = q/displaystyle0=n+...+ngenstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle 0 = n + ... + n \ge n \strictsuperieur 0$">|; 

$key = q/displaystyle0diventieren=sup{0}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img181.svg"
 ALT="$\displaystyle 0 \diventiere n = \sup \{ 0 \} = 0$">|; 

$key = q/displaystyle0lemmodulonlen-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img195.svg"
 ALT="$\displaystyle 0 \le m \modulo n \le n - 1$">|; 

$key = q/displaystyle1+...+1=0^{+...+}=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.26ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\displaystyle 1 + ... + 1 = 0^{+...+} = n$">|; 

$key = q/displaystyle1cdotm=0cdotm+m=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\displaystyle 1 \cdot m = 0 \cdot m + m = m$">|; 

$key = q/displaystyle7512=2+1cdot10+5cdot10^2+7cdot10^3;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.32ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img160.svg"
 ALT="$\displaystyle 7512 = 2 + 1 \cdot 10 + 5 \cdot 10^2 + 7 \cdot 10^3$">|; 

$key = q/displaystyleA_{mn}={kinsetN:kcdotnlem};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img164.svg"
 ALT="$\displaystyle A_{mn} = \{ k \in \setN : k \cdot n \le m \}$">|; 

$key = q/displaystyleA_{mn}subseteq{0,1,...,m-1,m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img167.svg"
 ALT="$\displaystyle A_{mn} \subseteq \{ 0, 1, ..., m - 1, m \}$">|; 

$key = q/displaystyleDelta={(m,n)insetN^2:nlem};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.72ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle \Delta = \{ (m,n) \in \setN^2 : n \le m \}$">|; 

$key = q/displaystylea+cleb+d;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle a + c \le b + d$">|; 

$key = q/displaystylea-cleb-d;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle a - c \le b - d$">|; 

$key = q/displaystylea_1cdotn-a_2cdotn=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\displaystyle a_1 \cdot n - a_2 \cdot n = 0$">|; 

$key = q/displaystylea_1cdotn=a_2cdotn=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle a_1 \cdot n = a_2 \cdot n = 0$">|; 

$key = q/displaystylealeb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle a \le b$">|; 

$key = q/displaystyleb+d=a+p+c+q=(a+c)+(p+q);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle b + d = a + p + c + q = (a + c) + (p + q)$">|; 

$key = q/displaystyleb-d-q=a+p-c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle b - d - q = a + p - c$">|; 

$key = q/displaystyleb-d=a-c+p+q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\displaystyle b - d = a - c + p + q$">|; 

$key = q/displaystyleb=a+p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle b = a + p$">|; 

$key = q/displaystylecged;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\displaystyle c \ge d$">|; 

$key = q/displaystylecled;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle c \le d$">|; 

$key = q/displaystyled+q=c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle d + q = c$">|; 

$key = q/displaystylei+j=i^++j^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.53ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle i + j = i^+ + j^-$">|; 

$key = q/displaystylei=kcdotj;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img209.svg"
 ALT="$\displaystyle i = k \cdot j$">|; 

$key = q/displaystylei_ni_{n-1}...i_2i_1i_0=i_0+i_1cdot10+i_2cdot10^2+...+i_{n-1}cdot10^{n-1}+i_ncdot10^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.65ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img159.svg"
 ALT="$\displaystyle i_n i_{n - 1} ... i_2 i_1 i_0 = i_0 + i_1 \cdot 10 + i_2 \cdot 10^2 + ... + i_{n - 1} \cdot 10^{n - 1} + i_n \cdot 10^n$">|; 

$key = q/displaystyleicdot0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img97.svg"
 ALT="$\displaystyle i \cdot 0 = 0$">|; 

$key = q/displaystyleicdotj+k=(icdotj)+k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle i \cdot j + k = ( i \cdot j ) + k$">|; 

$key = q/displaystyleicdotj=(icdotj^-)+i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle i \cdot j = ( i \cdot j^- ) + i$">|; 

$key = q/displaystyleidiventierej=k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img211.svg"
 ALT="$\displaystyle i \diventiere j = k$">|; 

$key = q/displaystyleinfsetN=minsetN=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \inf \setN = \min \setN = 0$">|; 

$key = q/displaystylekcdot0=0lem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img173.svg"
 ALT="$\displaystyle k \cdot 0 = 0 \le m$">|; 

$key = q/displaystylekcdotnlem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img199.svg"
 ALT="$\displaystyle k \cdot n \le m$">|; 

$key = q/displaystyleleft(m^nright)^p=m^{ncdotp};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.56ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img233.svg"
 ALT="$\displaystyle \left(m^n\right)^p = m^{n \cdot p}$">|; 

$key = q/displaystyleleft(mcdotnright)^p=m^pcdotn^p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.56ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img231.svg"
 ALT="$\displaystyle \left(m \cdot n\right)^p = m^p \cdot n^p$">|; 

$key = q/displaystyleleft(mcdotnright)^p=mcdot...cdotmcdotncdot...cdotn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.56ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img230.svg"
 ALT="$\displaystyle \left(m \cdot n\right)^p = m \cdot ... \cdot m \cdot n \cdot ... \cdot n$">|; 

$key = q/displaystyleleft(mcdotnright)^p=mcdotncdot...cdotmcdotn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.56ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img228.svg"
 ALT="$\displaystyle \left(m \cdot n\right)^p = m \cdot n \cdot ... \cdot m \cdot n$">|; 

$key = q/displaystylem+(n+p)=(m+n)+p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle m + (n + p) = (m + n) + p$">|; 

$key = q/displaystylem+(p+n)=(m+n)+p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle m + (p + n) = (m + n) + p$">|; 

$key = q/displaystylem+(p+n)=m+p^{+...+};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle m + (p + n) = m + p^{+...+}$">|; 

$key = q/displaystylem+(p+n)=m^{+...+}+p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle m + (p + n) = m^{+...+} + p$">|; 

$key = q/displaystylem+n+p=m+(n+p)=(m+n)+p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle m + n + p = m + (n + p) = (m + n) + p$">|; 

$key = q/displaystylem+n-p=(m+n)-p=m+(n-p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle m + n - p = (m + n) - p = m + (n - p)$">|; 

$key = q/displaystylem+n=m^{+...+}=0^{+.....+};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.26ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle m + n = m^{+...+} = 0^{+.....+}$">|; 

$key = q/displaystylem+n=n+m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle m + n = n + m$">|; 

$key = q/displaystylem+n^p=m+(n^p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img223.svg"
 ALT="$\displaystyle m + n^p = m + (n^p)$">|; 

$key = q/displaystylem-n-p=(m-n)-p=m-(n+p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle m - n - p = (m - n) - p = m - (n + p)$">|; 

$key = q/displaystylem-n-p=m-(n+p)=m-(p+n)=m-p-n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle m - n - p = m - (n + p) = m - (p + n) = m - p - n$">|; 

$key = q/displaystylem=(mdiventieren)cdotn+mmodulon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img186.svg"
 ALT="$\displaystyle m = (m \diventiere n) \cdot n + m \modulo n$">|; 

$key = q/displaystylem=(mdiventieren)cdotn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img189.svg"
 ALT="$\displaystyle m = (m \diventiere n) \cdot n$">|; 

$key = q/displaystylem=big[mdiventierepgcd(m,n)big]cdotpgcd(m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img216.svg"
 ALT="$\displaystyle m = \big[ m \diventiere \pgcd(m,n) \big] \cdot \pgcd(m,n)$">|; 

$key = q/displaystylem=kcdotn+r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img197.svg"
 ALT="$\displaystyle m = k \cdot n + r$">|; 

$key = q/displaystylem=mcdot1lem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img177.svg"
 ALT="$\displaystyle m = m \cdot 1 \le m$">|; 

$key = q/displaystylem=n+p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle m = n + p$">|; 

$key = q/displaystylem=qcdotn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.67ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img208.svg"
 ALT="$\displaystyle m = q \cdot n$">|; 

$key = q/displaystylem^{n+p}=(mcdot...cdotm)cdot(mcdot...cdotm);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img226.svg"
 ALT="$\displaystyle m^{n+p} = (m \cdot ... \cdot m) \cdot (m \cdot ... \cdot m)$">|; 

$key = q/displaystylem^{n+p}=m^ncdotm^p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.08ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img227.svg"
 ALT="$\displaystyle m^{n+p} = m^n \cdot m^p$">|; 

$key = q/displaystylem^{n+p}=mcdot...cdotm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.08ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img225.svg"
 ALT="$\displaystyle m^{n+p} = m \cdot ... \cdot m$">|; 

$key = q/displaystylemaxA_{mn}lemax{0,1,...,m-1,m}=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img170.svg"
 ALT="$\displaystyle \max A_{mn} \le \max \{ 0, 1, ..., m - 1, m \} = m$">|; 

$key = q/displaystylemcdot(n+p)=(m+...+m)+(m+...+m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\displaystyle m \cdot (n + p) = (m + ... + m) + (m + ... + m)$">|; 

$key = q/displaystylemcdot(n+p)=m+...+m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\displaystyle m \cdot (n + p) = m + ... + m$">|; 

$key = q/displaystylemcdot(n+p)=mcdotn+mcdotp;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle m \cdot (n + p) = m \cdot n + m \cdot p$">|; 

$key = q/displaystylemcdot(n-p)=mcdotn-mcdotp;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\displaystyle m \cdot (n - p) = m \cdot n - m \cdot p$">|; 

$key = q/displaystylemcdot(ncdotp)=(m+...+m)+...+(m+...+m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img149.svg"
 ALT="$\displaystyle m \cdot (n \cdot p) = (m + ... + m) + ... + (m + ... + m)$">|; 

$key = q/displaystylemcdot(ncdotp)=(mcdotn)+...+(mcdotn);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img150.svg"
 ALT="$\displaystyle m \cdot (n \cdot p) = (m \cdot n) + ... + (m \cdot n)$">|; 

$key = q/displaystylemcdot(ncdotp)=(mcdotn)cdotp;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle m \cdot (n \cdot p) = (m \cdot n) \cdot p$">|; 

$key = q/displaystylemcdot(ncdotp)=m+...+m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\displaystyle m \cdot (n \cdot p) = m + ... + m$">|; 

$key = q/displaystylemcdot(p+q)=mcdotp+mcdotq;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle m \cdot (p + q) = m \cdot p + m \cdot q$">|; 

$key = q/displaystylemcdot0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle m \cdot 0 = 0$">|; 

$key = q/displaystylemcdot1=mcdot0+m=0+m=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle m \cdot 1 = m \cdot 0 + m = 0 + m = m$">|; 

$key = q/displaystylemcdoti=qcdotkcdotncdotj;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img212.svg"
 ALT="$\displaystyle m \cdot i = q \cdot k \cdot n \cdot j$">|; 

$key = q/displaystylemcdotn=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle m \cdot n = 0$">|; 

$key = q/displaystylemcdotn=0quadRightarrowquadm=0quadmathrm{ou}quadn=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle m \cdot n = 0 \quad \Rightarrow \quad m = 0 \quad \mathrm{ou} \quad n = 0$">|; 

$key = q/displaystylemcdotn=m^-+...+m^-+1+...+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.26ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\displaystyle m \cdot n = m^- + ... + m^- + 1 + ... + 1$">|; 

$key = q/displaystylemcdotn=m^-+...+m^-+n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.26ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle m \cdot n = m^- + ... + m^- + n$">|; 

$key = q/displaystylemcdotn=m^-+1+...+m^-+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.26ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle m \cdot n = m^- + 1 + ... + m^- + 1$">|; 

$key = q/displaystylemcdotn=m^-cdotn+n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.26ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\displaystyle m \cdot n = m^- \cdot n + n$">|; 

$key = q/displaystylemcdotn=mcdotn^-+m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.26ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle m \cdot n = m \cdot n^- + m$">|; 

$key = q/displaystylemcdotn=mcdotp+mcdot(n-p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle m \cdot n = m \cdot p + m \cdot (n - p)$">|; 

$key = q/displaystylemcdotn=n+...+n=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img155.svg"
 ALT="$\displaystyle m \cdot n = n + ... + n = 0$">|; 

$key = q/displaystylemcdotn=ncdotm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle m \cdot n = n \cdot m$">|; 

$key = q/displaystylemcdotn^+=(mcdotn)+m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle m \cdot n^+ = ( m \cdot n ) + m$">|; 

$key = q/displaystylemcdotn^+=mcdotn+m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.26ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle m \cdot n^+ = m \cdot n + m$">|; 

$key = q/displaystylemcdotn^p=mcdot(n^p);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img224.svg"
 ALT="$\displaystyle m \cdot n^p = m \cdot (n^p)$">|; 

$key = q/displaystylemcdotncdotp=mcdot(ncdotp)=(mcdotn)cdotp;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\displaystyle m \cdot n \cdot p = m \cdot (n \cdot p) = (m \cdot n) \cdot p$">|; 

$key = q/displaystylemdiventiere0=supsetN=+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img176.svg"
 ALT="$\displaystyle m \diventiere 0 = \sup \setN = +\infty$">|; 

$key = q/displaystylemdiventiere1=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img179.svg"
 ALT="$\displaystyle m \diventiere 1 = m$">|; 

$key = q/displaystylemdiventieren=k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img204.svg"
 ALT="$\displaystyle m \diventiere n = k$">|; 

$key = q/displaystylemdiventieren=maxA_{mn}=supA_{mn};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img169.svg"
 ALT="$\displaystyle m \diventiere n = \max A_{mn} = \sup A_{mn}$">|; 

$key = q/displaystylemdiventieren=q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img210.svg"
 ALT="$\displaystyle m \diventiere n = q$">|; 

$key = q/displaystylemdiventieren=sup{kinsetN:kcdotnlem};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img162.svg"
 ALT="$\displaystyle m \diventiere n = \sup \{ k \in \setN : k \cdot n \le m \}$">|; 

$key = q/displaystylemdiventierenlem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img171.svg"
 ALT="$\displaystyle m \diventiere n \le m$">|; 

$key = q/displaystylemgen+kcdotn=(k+1)cdotn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img193.svg"
 ALT="$\displaystyle m \ge n + k \cdot n = (k + 1) \cdot n$">|; 

$key = q/displaystylemgen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle m \ge n$">|; 

$key = q/displaystyleminorsetN={0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \minor \setN = \{ 0 \}$">|; 

$key = q/displaystylemlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle m \le n$">|; 

$key = q/displaystylemmodulon=m-(mdiventieren)cdotn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img185.svg"
 ALT="$\displaystyle m \modulo n = m - (m \diventiere n) \cdot n$">|; 

$key = q/displaystylemmodulon=m-kcdotngen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img192.svg"
 ALT="$\displaystyle m \modulo n = m - k \cdot n \ge n$">|; 

$key = q/displaystylemmodulon=r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img205.svg"
 ALT="$\displaystyle m \modulo n = r$">|; 

$key = q/displaystylemmodulonge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img191.svg"
 ALT="$\displaystyle m \modulo n \ge 0$">|; 

$key = q/displaystylemn=mcdotn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle m  n = m \cdot n$">|; 

$key = q/displaystylemstrictsuperieurn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle m \strictsuperieur n$">|; 

$key = q/displaystylen!=ncdot(n-1)cdot(n-2)cdot...cdot1=1cdot2cdot3cdot...cdotn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img235.svg"
 ALT="$\displaystyle n ! = n \cdot (n - 1) \cdot (n - 2) \cdot ... \cdot 1 = 1 \cdot 2 \cdot 3 \cdot ... \cdot n$">|; 

$key = q/displaystylen+m=n^{+...+}=0^{+.....+};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.26ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle n + m = n^{+...+} = 0^{+.....+}$">|; 

$key = q/displaystylen+p=p+n=p^{+...+}=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.53ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle n + p = p + n = p^{+...+} = m$">|; 

$key = q/displaystylen=0+n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle n = 0 + n$">|; 

$key = q/displaystylen=big[ndiventierepgcd(m,n)big]cdotpgcd(m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img217.svg"
 ALT="$\displaystyle n = \big[ n \diventiere \pgcd(m,n) \big] \cdot \pgcd(m,n)$">|; 

$key = q/displaystylen^-cdotm=mcdotn^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.08ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\displaystyle n^- \cdot m = m \cdot n^-$">|; 

$key = q/displaystylen^-lenlen^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.40ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle n^- \le n \le n^+$">|; 

$key = q/displaystylencdotm=n^-cdotm+m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.26ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\displaystyle n \cdot m = n^- \cdot m + m$">|; 

$key = q/displaystylenge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle n \ge 0$">|; 

$key = q/displaystylenlem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle n \le m$">|; 

$key = q/displaystylenstrictinferieurm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle n \strictinferieur m$">|; 

$key = q/displaystylenstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle n \strictsuperieur 0$">|; 

$key = q/displaystylep=m-n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle p = m - n$">|; 

$key = q/displaystylep=m-n=m^{-...-}=0^{+...+-...-};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.53ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle p = m - n = m^{-...-} = 0^{+...+-...-}$">|; 

$key = q/displaystylep=m^{-...-};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.53ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle p = m^{-...-}$">|; 

$key = q/displaystylep^{+...+-...-}=p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.53ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle p^{+...+-...-} = p$">|; 

$key = q/displaystylepcdotn=p+...+pstrictsuperieurpstrictsuperieurm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img166.svg"
 ALT="$\displaystyle p \cdot n = p + ... + p \strictsuperieur p \strictsuperieur m$">|; 

$key = q/displaystylepgcd(m,n)=sup{kinsetN:mmodulok=nmodulok=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img215.svg"
 ALT="$\displaystyle \pgcd(m,n) = \sup \{ k \in \setN : m \modulo k = n \modulo k = 0 \}$">|; 

$key = q/displaystyleppcm(m,n)=big[ppcm(m,n)diventierembig]cdotm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img219.svg"
 ALT="$\displaystyle \ppcm(m,n) = \big[ \ppcm(m,n) \diventiere m \big] \cdot m$">|; 

$key = q/displaystyleppcm(m,n)=big[ppcm(m,n)diventierenbig]cdotn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img220.svg"
 ALT="$\displaystyle \ppcm(m,n) = \big[ \ppcm(m,n) \diventiere n \big] \cdot n$">|; 

$key = q/displaystyleppcm(m,n)=inf{kinsetN:kmodulom=kmodulon=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img218.svg"
 ALT="$\displaystyle \ppcm(m,n) = \inf \{ k \in \setN : k \modulo m = k \modulo n = 0 \}$">|; 

$key = q/displaystyleq=n-p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\displaystyle q = n - p$">|; 

$key = q/displaystyler=m-(mdiventieren)cdotn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img183.svg"
 ALT="$\displaystyle r = m - (m \diventiere n) \cdot n$">|; 

$key = q/displaystylerlen-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img201.svg"
 ALT="$\displaystyle r \le n - 1$">|; 

$key = q/displaystylesetN={0,1,2,3,4,5,6,7,8,9,10,...};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle \setN = \{ 0,1,2,3,4,5,6,7,8,9,10,... \}$">|; 

$key = q/displaystylesupsetN=+infty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \sup \setN = +\infty$">|; 

$key = q/displaystylexcdoty!=xcdot(y!);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img236.svg"
 ALT="$\displaystyle x \cdot y ! = x \cdot (y !)$">|; 

$key = q/displaystyle{kinsetN:kcdot0lem}=setN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img175.svg"
 ALT="$\displaystyle \{ k \in \setN : k \cdot 0 \le m \} = \setN$">|; 

$key = q/displaystyle{kinsetN:kcdotnle0}={0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img180.svg"
 ALT="$\displaystyle \{ k \in \setN : k \cdot n \le 0 \} = \{ 0 \}$">|; 

$key = q/diventiere:setNtimessetNtosetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img161.svg"
 ALT="$\diventiere : \setN \times \setN \to \setN$">|; 

$key = q/i,j,kinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img100.svg"
 ALT="$i,j,k \in \setN$">|; 

$key = q/i=m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$i = m$">|; 

$key = q/j=n^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$j = n^+$">|; 

$key = q/k+1strictsuperieurmdiventieren;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img203.svg"
 ALT="$k + 1 \strictsuperieur m \diventiere n$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img194.svg"
 ALT="$k$">|; 

$key = q/k=mdiventieren;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img190.svg"
 ALT="$k = m \diventiere n$">|; 

$key = q/kinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img174.svg"
 ALT="$k \in \setN$">|; 

$key = q/klemdiventieren;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img200.svg"
 ALT="$k \le m \diventiere n$">|; 

$key = q/m+(p+n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$m + (p + n)$">|; 

$key = q/m+n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.70ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img33.svg"
 ALT="$m + n$">|; 

$key = q/m,n,k,rinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img196.svg"
 ALT="$m,n,k,r \in \setN$">|; 

$key = q/m,n,pinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$m,n,p \in \setN$">|; 

$key = q/m,n,q,i,j,kinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img206.svg"
 ALT="$m,n,q,i,j,k \in \setN$">|; 

$key = q/m,ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$m,n \in \setN$">|; 

$key = q/m,nne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img154.svg"
 ALT="$m, n \ne 0$">|; 

$key = q/m,plen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img86.svg"
 ALT="$m,p \le n$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img15.svg"
 ALT="$m$">|; 

$key = q/m=m^-+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img107.svg"
 ALT="$m = m^- + 1$">|; 

$key = q/m^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img113.svg"
 ALT="$m^-$">|; 

$key = q/m^{+...+};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$m^{+...+}$">|; 

$key = q/mcdot0=0cdotm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img119.svg"
 ALT="$m \cdot 0 = 0 \cdot m$">|; 

$key = q/mcdotn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img229.svg"
 ALT="$m \cdot n$">|; 

$key = q/mdiventiere0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img172.svg"
 ALT="$m \diventiere 0$">|; 

$key = q/mdiventieren;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img163.svg"
 ALT="$m \diventiere n$">|; 

$key = q/mge1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img105.svg"
 ALT="$m \ge 1$">|; 

$key = q/minsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img13.svg"
 ALT="$m \in \setN$">|; 

$key = q/mlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img1.svg"
 ALT="$m \le n$">|; 

$key = q/mmodulon=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img188.svg"
 ALT="$m \modulo n = 0$">|; 

$key = q/mnen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img51.svg"
 ALT="$m \ne n$">|; 

$key = q/mstrictinferieurn;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.47ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img76.svg"
 ALT="$m \strictinferieur n$">|; 

$key = q/n+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img41.svg"
 ALT="$n+1$">|; 

$key = q/n+p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img129.svg"
 ALT="$n+p$">|; 

$key = q/n+plem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img79.svg"
 ALT="$n + p \le m$">|; 

$key = q/n,jne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img207.svg"
 ALT="$n,j \ne 0$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img9.svg"
 ALT="$n$">|; 

$key = q/n=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img121.svg"
 ALT="$n = 0$">|; 

$key = q/n=0=1^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img125.svg"
 ALT="$n = 0 = 1^-$">|; 

$key = q/n=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img144.svg"
 ALT="$n = 1$">|; 

$key = q/n=1=2^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img126.svg"
 ALT="$n = 1 = 2^-$">|; 

$key = q/n=2=3^-;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img127.svg"
 ALT="$n = 2 = 3^-$">|; 

$key = q/n^-+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img42.svg"
 ALT="$n^- + 1$">|; 

$key = q/ncdotp;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.67ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img148.svg"
 ALT="$n \cdot p$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img65.svg"
 ALT="$n \in \setN$">|; 

$key = q/ninsetZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img143.svg"
 ALT="$n \in \setZ$">|; 

$key = q/nlem;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img47.svg"
 ALT="$n \le m$">|; 

$key = q/nne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img69.svg"
 ALT="$n \ne 0$">|; 

$key = q/p+q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img63.svg"
 ALT="$p + q$">|; 

$key = q/p+q=n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img135.svg"
 ALT="$p + q = n$">|; 

$key = q/p,qinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img60.svg"
 ALT="$p,q \in \setN$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img35.svg"
 ALT="$p$">|; 

$key = q/p=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$p = 1$">|; 

$key = q/p^{+-}=p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.42ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img29.svg"
 ALT="$p^{+-} = p$">|; 

$key = q/pinsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img39.svg"
 ALT="$p \in \setN$">|; 

$key = q/plen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img83.svg"
 ALT="$p \le n$">|; 

$key = q/pstrictsuperieurm;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img165.svg"
 ALT="$p \strictsuperieur m$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img184.svg"
 ALT="$r$">|; 

$key = q/r=mmodulon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img187.svg"
 ALT="$r = m \modulo n$">|; 

$key = q/rge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img198.svg"
 ALT="$r \ge 0$">|; 

$key = q/setN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\setN$">|; 

$key = q/sinmajorsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img54.svg"
 ALT="$s \in \major \setN$">|; 

$key = q/sles^+insetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img55.svg"
 ALT="$s \le s^+ \in \setN$">|; 

$key = q/{Eqts}m+(p+n)=m^++p^{+...+-}vdotsm+(p+n)=m^{+...+}+p^{+...+-...-}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.90ex; vertical-align: -4.89ex; " SRC="|."$dir".q|img27.svg"
 ALT="\begin{Eqts}
m + (p + n) = m^+ + p^{+...+-} \\\\
\vdots \\\\
m + (p + n) = m^{+...+} + p^{+...+-...-}
\end{Eqts}">|; 

$key = q/{Eqts}m=n=n+0Longleftrightarrowm-n=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img78.svg"
 ALT="\begin{Eqts}
m = n = n + 0 \\\\
\Longleftrightarrow \\\\
m - n = 0
\end{Eqts}">|; 

$key = q/{eqnarraystar}0!&=&1n!&=&ncdot(n-1)!{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img234.svg"
 ALT="\begin{eqnarray*}
0 ! &amp;=&amp; 1 \\\\
n ! &amp;=&amp; n \cdot (n-1) !
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}0cdotm&=&0cdotm^-+0vdots&&0cdotm&=&0+...+0=0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 15.04ex; " SRC="|."$dir".q|img103.svg"
 ALT="\begin{eqnarray*}
0 \cdot m &amp;=&amp; 0 \cdot m^- + 0 \\\\
\vdots &amp;&amp; \\\\
0 \cdot m &amp;=&amp; 0 + ... + 0 = 0
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}b&=&a+pd&=&c+q{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.44ex; " SRC="|."$dir".q|img61.svg"
 ALT="\begin{eqnarray*}
b &amp;=&amp; a + p \\\\
d &amp;=&amp; c + q
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}left(m^nright)^p&=&left(mcdot...cdotmright)^p&=&mcdot...cdotm{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.99ex; " SRC="|."$dir".q|img232.svg"
 ALT="\begin{eqnarray*}
\left(m^n\right)^p &amp;=&amp; \left(m \cdot ... \cdot m\right)^p \\\\
&amp;=&amp; m \cdot ... \cdot m
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}m+0&=&0m+n^+&=&m^++n{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.17ex; " SRC="|."$dir".q|img5.svg"
 ALT="\begin{eqnarray*}
m + 0 &amp;=&amp; 0 \\\\
m + n^+ &amp;=&amp; m^+ + n
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}m+1&=&m+0^+=m^++0=m^+m+2&=&m+0^{++}=m^++0^+=m^{++}+0=m^{++}vdots&&m+n&=&...=m^{+...+}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.42ex; " SRC="|."$dir".q|img7.svg"
 ALT="\begin{eqnarray*}
m + 1 &amp;=&amp; m + 0^+ = m^+ + 0 = m^+ \\\\
m + 2 &amp;=&amp; m + 0^{++} = m...
... m^{++} + 0 = m^{++} \\\\
\vdots &amp;&amp; \\\\
m + n &amp;=&amp; ... = m^{+...+}
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}m+n-p=(m+n)-p&=&(n+m)-p&=&n+(m-p)&=&(m-p)+n&=&m-p+n{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.20ex; " SRC="|."$dir".q|img87.svg"
 ALT="\begin{eqnarray*}
m + n - p = (m + n) - p &amp;=&amp; (n + m) - p \\\\
&amp;=&amp; n + (m - p) \\\\
&amp;=&amp; (m - p) + n \\\\
&amp;=&amp; m - p + n
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}m^0&=&1m^n&=&mcdotm^{n-1}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.99ex; " SRC="|."$dir".q|img221.svg"
 ALT="\begin{eqnarray*}
m^0 &amp;=&amp; 1 \\\\
m^n &amp;=&amp; m \cdot m^{n-1}
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}m^n&=&mcdotm^{n-1}&vdots&&=&mcdot...cdotm{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.69ex; " SRC="|."$dir".q|img222.svg"
 ALT="\begin{eqnarray*}
m^n &amp;=&amp; m \cdot m^{n-1} \\\\
&amp;\vdots&amp; \\\\
&amp;=&amp; m \cdot ... \cdot m
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}mcdotn&=&mcdotn^-+m&vdots&&=&mcdot0+m+...+m&=&0+m+...+m&=&m+...+m{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 21.80ex; " SRC="|."$dir".q|img106.svg"
 ALT="\begin{eqnarray*}
m \cdot n &amp;=&amp; m \cdot n^- + m \\\\
&amp;\vdots&amp; \\\\
&amp;=&amp; m \cdot 0 + m + ... + m \\\\
&amp;=&amp; 0 + m + ... + m \\\\
&amp;=&amp; m + ... + m
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}n^-+1&=&nn+1&=&n^+{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.17ex; " SRC="|."$dir".q|img43.svg"
 ALT="\begin{eqnarray*}
n^- + 1 &amp;=&amp; n \\\\
n + 1 &amp;=&amp; n^+
\end{eqnarray*}">|; 

1;

