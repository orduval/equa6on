# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate labels original text with physical files.


$key = q/chap:naturels/;
$external_labels{$key} = "$URL/" . q|naturel.html|; 
$noresave{$key} = "$nosave";

$key = q/sec:positivite_des_naturels/;
$external_labels{$key} = "$URL/" . q|naturel.html|; 
$noresave{$key} = "$nosave";

1;


# LaTeX2HTML 2023 (Released January 1, 2023)
# labels from external_latex_labels array.


$key = q/chap:naturels/;
$external_latex_labels{$key} = q|1 Naturels|; 
$noresave{$key} = "$nosave";

$key = q/sec:positivite_des_naturels/;
$external_latex_labels{$key} = q|1.5 Positivité|; 
$noresave{$key} = "$nosave";

1;

