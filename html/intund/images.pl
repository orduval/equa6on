# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/I(a,b)=I(a,c)+I(c,b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$I(a,b) = I(a,c) + I(c,b)$">|; 

$key = q/[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$[a,b]$">|; 

$key = q/a,b,c,alpha,betainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$a,b,c,\alpha,\beta \in \setR$">|; 

$key = q/a,b,c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$a,b,c$">|; 

$key = q/a,binsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img2.svg"
 ALT="$a,b \in \setR$">|; 

$key = q/aleb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img3.svg"
 ALT="$a \le b$">|; 

$key = q/aleblec;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img16.svg"
 ALT="$a \le b \le c$">|; 

$key = q/alecleb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img19.svg"
 ALT="$a \le c \le b$">|; 

$key = q/alphalea,b,clebeta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\alpha \le a,b,c \le \beta$">|; 

$key = q/c;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img31.svg"
 ALT="$c$">|; 

$key = q/displaystyleI(a,b)=I(a,c)+I(c,b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle I(a,b) = I(a,c) + I(c,b)$">|; 

$key = q/displaystyleI(a,c)=I(a,b)+I(b,c);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle I(a,c) = I(a,b) + I(b,c)$">|; 

$key = q/displaystyleI(a,c)=I(a,b)-I(c,b)=I(a,b)+I(b,c);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle I(a,c) = I(a,b) - I(c,b) = I(a,b) + I(b,c)$">|; 

$key = q/displaystyleI(x,y)=int_x^yf(xi)dxi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.49ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle I(x,y) = \int_x^y f(\xi)  d\xi$">|; 

$key = q/displaystyleI(y,x)=-I(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle I(y,x) = - I(x,y)$">|; 

$key = q/displaystylef(c)=unsur{b-a}int_a^bf(s)ds;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle f(c) = \unsur{b - a} \int_a^b f(s) ds$">|; 

$key = q/displaystylef(lambda)leunsur{b-a}int_a^bf(s)dslef(sigma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle f(\lambda) \le \unsur{b - a} \int_a^b f(s) ds \le f(\sigma)$">|; 

$key = q/displaystyleint_a^bf(x)dmu(x)=int_{[a,b]}f(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.28ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \int_a^b f(x)  d\mu(x) = \int_{[a,b]} f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_a^cf(x)dx=int_a^bf(x)dx+int_b^cf(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \int_a^c f(x)  dx = \int_a^b f(x)  dx + \int_b^c f(x)  dx$">|; 

$key = q/displaystyleint_b^af(x)dmu(x)=-int_a^bf(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \int_b^a f(x)  d\mu(x) = - \int_a^b f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_{-infty}^{+infty}f(x)dmu(x)=int_setRf(x)dmu(x)=lim_{ato-infty}int_{-a}^af(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.92ex; vertical-align: -2.36ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \int_{-\infty}^{+\infty} f(x)  d\mu(x) = \int_\setR f(x)  d\mu(x) = \lim_{a \to -\infty} \int_{-a}^a f(x)  d\mu(x)$">|; 

$key = q/displaystyleint_{]a,b[}f(x)dmu(x)=lim_{(s,t)to(a,b)}int_s^tf(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.16ex; vertical-align: -2.58ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \int_{]a,b[} f(x)  d\mu(x) = \lim_{(s,t) \to (a,b)} \int_s^t f(x)  d\mu(x)$">|; 

$key = q/displaystylemu_L([a,b]cap[b,c])=mu_L({b})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \mu_L( [a,b] \cap [b,c] ) = \mu_L( \{ b \} ) = 0$">|; 

$key = q/displaystylemu_L([a,c]cap[c,b])=mu_L({c})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \mu_L( [a,c] \cap [c, b] ) = \mu_L( \{ c \} ) = 0$">|; 

$key = q/dxi=dmu_L(xi);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$d\xi = d\mu_L(\xi)$">|; 

$key = q/f:[a,b]mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$f : [a,b] \mapsto \setR$">|; 

$key = q/f:[alpha,beta]mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$f : [\alpha,\beta] \mapsto \setR$">|; 

$key = q/f:setRmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$f : \setR \mapsto \setR$">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\lambda$">|; 

$key = q/setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\setR$">|; 

$key = q/sigma,lambdain[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\sigma,\lambda \in [a,b]$">|; 

$key = q/sigma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\sigma$">|; 

$key = q/{Eqts}f(sigma)=maxf([a,b])f(lambda)=minf([a,b]){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img29.svg"
 ALT="\begin{Eqts}
f(\sigma) = \max f([a,b]) \\\\
f(\lambda) = \min f([a,b])
\end{Eqts}">|; 

$key = q/{Eqts}int_a^{+infty}f(x)dmu(x)=lim_{bto+infty}int_a^bf(x)dmu(x)int_{-infty}^bf(x)dmu(x)=lim_{ato-infty}int_a^bf(x)dmu(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.53ex; vertical-align: -5.71ex; " SRC="|."$dir".q|img7.svg"
 ALT="\begin{Eqts}
\int_a^{+\infty} f(x)  d\mu(x) = \lim_{b \to +\infty} \int_a^b f(x...
...nfty}^b f(x)  d\mu(x) = \lim_{a \to -\infty} \int_a^b f(x)  d\mu(x)
\end{Eqts}">|; 

$key = q/{Eqts}supessentiel{f(x):xin[a,b]}=maxf([a,b])infessentiel{f(x):xin[a,b]}=minf([a,b]){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img27.svg"
 ALT="\begin{Eqts}
\supessentiel \{ f(x) : x \in [a,b] \} = \max f([a,b]) \\\\
\infessentiel \{ f(x) : x \in [a,b] \} = \min f([a,b])
\end{Eqts}">|; 

1;

