# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img37.svg"
 ALT="\begin{eqnarray*}
\max [a,b] &amp;=&amp; \max \intervallesemiouvertgauche{a}{b} = \max \...
...roite{a}{b} = \min \intervallesemiouvertdroite{a}{+\infty}  = a
\end{eqnarray*}">|; 

$key = q/-(x-c)=c-xler;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$-(x - c) = c - x \le r$">|; 

$key = q/I;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img38.svg"
 ALT="$I$">|; 

$key = q/[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$[a,b]$">|; 

$key = q/a,binsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$a,b \in \setR$">|; 

$key = q/a=c-r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img19.svg"
 ALT="$a = c - r$">|; 

$key = q/abs{x-c}ler;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\abs{x - c} \le r$">|; 

$key = q/ainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img9.svg"
 ALT="$a \in \setR$">|; 

$key = q/aleb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img2.svg"
 ALT="$a \le b$">|; 

$key = q/b=c+r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img20.svg"
 ALT="$b = c + r$">|; 

$key = q/c,r,xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$c,r,x \in \setR$">|; 

$key = q/displaystyle[a,b]={xinsetR:alexleb};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle [a,b] = \{ x \in \setR : a \le x \le b \}$">|; 

$key = q/displaystyleinfintervalleouvert{a}{b}=maxminorintervalleouvert{a}{b}=max]-infty,a]=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \inf \intervalleouvert{a}{b} = \max \minor \intervalleouvert{a}{b} = \max  ]-\infty, a] = a$">|; 

$key = q/displaystyleintervalleouvert{a}{b}={xinsetR:astrictinferieurxstrictinferieurb};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \intervalleouvert{a}{b}  = \{ x \in \setR : a \strictinferieur x \strictinferieur b \}$">|; 

$key = q/displaystyleintervallesemiouvertdroite{a}{b}={xinsetR:alexstrictinferieurb};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \intervallesemiouvertdroite{a}{b}  = \{ x \in \setR : a \le x \strictinferieur b \}$">|; 

$key = q/displaystyleintervallesemiouvertgauche{a}{b}={xinsetR:astrictinferieurxleb};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \intervallesemiouvertgauche{a}{b} = \{ x \in \setR : a \strictinferieur x \le b \}$">|; 

$key = q/displaystylemajor[a,b]=intervallesemiouvertgauche{b}{+infty};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \major [a,b] = \intervallesemiouvertgauche{b}{+\infty}$">|; 

$key = q/displaystylemajorintervalleouvert{a}{b}=intervallesemiouvertgauche{b}{+infty};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \major \intervalleouvert{a}{b} = \intervallesemiouvertgauche{b}{+\infty}$">|; 

$key = q/displaystyleminor[a,b]=intervallesemiouvertdroite{-infty}{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \minor [a,b] = \intervallesemiouvertdroite{-\infty}{a}$">|; 

$key = q/displaystyleminorintervalleouvert{a}{b}=intervallesemiouvertdroite{-infty}{a};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \minor \intervalleouvert{a}{b} = \intervallesemiouvertdroite{-\infty}{a}$">|; 

$key = q/displaystylepartial[a,b]=partialintervalleouvert{a}{b}={a,b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \partial [a,b] = \partial \intervalleouvert{a}{b} = \{a,b\}$">|; 

$key = q/displaystylesupintervalleouvert{a}{b}=minmajorintervalleouvert{a}{b}=min[b,+infty[=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \sup \intervalleouvert{a}{b} = \min \major \intervalleouvert{a}{b} = \min [b, +\infty[  = b$">|; 

$key = q/infI=minI;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\inf I = \min I$">|; 

$key = q/intervalleouvert{a}{b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\intervalleouvert{a}{b}$">|; 

$key = q/rge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img12.svg"
 ALT="$r \ge 0$">|; 

$key = q/supI=maxI;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\sup I = \max I$">|; 

$key = q/x-cler;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img14.svg"
 ALT="$x - c \le r$">|; 

$key = q/xge[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$x \ge [a,b]$">|; 

$key = q/xgeb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img25.svg"
 ALT="$x \ge b$">|; 

$key = q/xgebgey;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img27.svg"
 ALT="$x \ge b \ge y$">|; 

$key = q/xgey;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$x \ge y$">|; 

$key = q/xin[c-r,c+r];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$x \in [c - r, c + r]$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img23.svg"
 ALT="$x \in \setR$">|; 

$key = q/xle[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$x \le [a,b]$">|; 

$key = q/xlea;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img32.svg"
 ALT="$x \le a$">|; 

$key = q/xlealey;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img33.svg"
 ALT="$x \le a \le y$">|; 

$key = q/xley;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img34.svg"
 ALT="$x \le y$">|; 

$key = q/yin[a,b];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$y \in [a,b]$">|; 

$key = q/{Eqts}a+b=c-r+c+r=2cb-a=c+r-c+r=2r{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img21.svg"
 ALT="\begin{Eqts}
a + b = c - r + c + r = 2 c \\\\
b - a = c + r - c + r = 2 r
\end{Eqts}">|; 

$key = q/{Eqts}xlec+rxgec-r{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img16.svg"
 ALT="\begin{Eqts}
x \le c + r \\\\
x \ge c - r
\end{Eqts}">|; 

$key = q/{eqnarraystar}adh[a,b]&=&adhintervalleouvert{a}{b}=[a,b]interieur[a,b]&=&interieurintervalleouvert{a}{b}=]a,b[{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img43.svg"
 ALT="\begin{eqnarray*}
\adh [a,b] &amp;=&amp; \adh \intervalleouvert{a}{b} = [a,b] \\\\
\interieur [a,b] &amp;=&amp; \interieur \intervalleouvert{a}{b} =  ]a,b[
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}boule[c,r]&=&[c-r,c+r]boule(c,r)&=&intervalleouvert{c-r}{c+r}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img18.svg"
 ALT="\begin{eqnarray*}
\boule[c,r] &amp;=&amp; [c - r, c + r] \\\\
\boule(c,r) &amp;=&amp;  \intervalleouvert{c - r}{c + r}
\end{eqnarray*}">|; 

1;

