# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.12ex; vertical-align: -6.00ex; " SRC="|."$dir".q|img5.svg"
 ALT="\begin{Eqts}
\setZ^n = \{ (z_1,z_2,...,z_n) : z_1,z_2,...,z_n \in \setN \} \\\\
\...
... \} \\\\
\setC^n = \{ (c_1,c_2,...,c_n) : c_1,c_2,...,c_n \in \setC \}
\end{Eqts}">|; 

$key = q/0inA^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img34.svg"
 ALT="$0 \in A^n$">|; 

$key = q/A^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img23.svg"
 ALT="$A^n$">|; 

$key = q/A^ninmathcal{D};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img8.svg"
 ALT="$A^n \in \mathcal{D}$">|; 

$key = q/alphainA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\alpha \in A$">|; 

$key = q/betainA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\beta \in A$">|; 

$key = q/cdot:AtimesA^ntoA^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\cdot : A \times A^n \to A^n$">|; 

$key = q/displaystyle0=(0,0,...,0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle 0 = (0,0,...,0)$">|; 

$key = q/displaystylealphacdotbetacdotx=(alphacdotbeta)cdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \alpha \cdot \beta \cdot x = (\alpha \cdot \beta) \cdot x$">|; 

$key = q/displaystylealphacdotx=(alphacdotx_1,alphacdotx_2,...,alphacdotx_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \alpha \cdot x = (\alpha \cdot x_1 , \alpha \cdot x_2 , ... , \alpha \cdot x_n)$">|; 

$key = q/displaystylealphax=alphacdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \alpha  x = \alpha \cdot x$">|; 

$key = q/displaystylem!=m_1!cdotm_2!cdot...cdotm_n!;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle m ! = m_1 ! \cdot m_2 ! \cdot ... \cdot m_n !$">|; 

$key = q/displaystylemathcal{D}={setN^n,setZ^n,setQ^n,setR^n,setC^n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \mathcal{D} = \{ \setN^n , \setZ^n , \setQ^n , \setR^n , \setC^n \}$">|; 

$key = q/displaystylesetN^n={(m_1,m_2,...,m_n):m_1,m_2,...,m_ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \setN^n = \{ (m_1,m_2,...,m_n) : m_1,m_2,...,m_n \in \setN \}$">|; 

$key = q/displaystylex+y=(x_1+y_1,x_2+y_2,...,x_n+y_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$\displaystyle x + y = (x_1 + y_1 , x_2 + y_2 , ... , x_n + y_n)$">|; 

$key = q/displaystylex-y=(x_1-y_1,x_2-y_2,...,x_n-y_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle x - y = (x_1 - y_1 , x_2 - y_2 , ... , x_n - y_n)$">|; 

$key = q/displaystylexcdotalpha=alphacdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.22ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle x \cdot \alpha = \alpha \cdot x$">|; 

$key = q/displaystyley^x=y_1^{x_1}cdoty_2^{x_2}cdot...cdoty_n^{x_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.67ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\displaystyle y^x = y_1^{x_1} \cdot y_2^{x_2} \cdot ... \cdot y_n^{x_n}$">|; 

$key = q/displaystyle{1,2,...,n}mapsto{f(1),f(2),...,f(n)}equiv(f_1,f_2,...,f_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \{1,2,...,n\} \mapsto \{f(1),f(2),...,f(n)\} \equiv (f_1,f_2,...,f_n)$">|; 

$key = q/iin{1,2,...,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$i \in \{1,2,...,n\}$">|; 

$key = q/m=(m_1,m_2,...,m_n)insetN^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$m = (m_1,m_2,...,m_n) \in \setN^n$">|; 

$key = q/m_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img4.svg"
 ALT="$m_i$">|; 

$key = q/mathcal{D};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\mathcal{D}$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$n$">|; 

$key = q/setN^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\setN^n$">|; 

$key = q/x,yinA^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$x,y \in A^n$">|; 

$key = q/x=(x_1,...x_n),y=(y_1,...,y_n)inA^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$x = (x_1,...x_n) , y = (y_1,...,y_n) \in A^n$">|; 

$key = q/x=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img14.svg"
 ALT="$x = 0$">|; 

$key = q/x=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$x = y$">|; 

$key = q/x_i,y_iinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$x_i,y_i \in A$">|; 

$key = q/x_i=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img15.svg"
 ALT="$x_i = 0$">|; 

$key = q/x_i=y_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$x_i = y_i$">|; 

$key = q/x_iley_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$x_i \le y_i$">|; 

$key = q/x_istrictinferieury_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img20.svg"
 ALT="$x_i \strictinferieur y_i$">|; 

$key = q/xley;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img16.svg"
 ALT="$x \le y$">|; 

$key = q/xstrictinferieury;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img19.svg"
 ALT="$x \strictinferieur y$">|; 

$key = q/ygex;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img18.svg"
 ALT="$y \ge x$">|; 

$key = q/ystrictsuperieurx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img21.svg"
 ALT="$y \strictsuperieur x$">|; 

$key = q/{Eqts}x=(x_1,x_2,...,x_n)y=(y_1,y_2,...,y_n){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img10.svg"
 ALT="\begin{Eqts}
x = (x_1,x_2,...,x_n) \\\\
y = (y_1,y_2,...,y_n)
\end{Eqts}">|; 

1;

