# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.12ex; " SRC="|."$dir".q|img61.svg"
 ALT="\begin{eqnarray*}
\distance(f(x,y),L) &amp;\le&amp; \distance(f(x,y),\lambda(y)) + \dist...
...,L) \\\\
&amp;\le&amp; \frac{\epsilon}{2} + \frac{\epsilon}{2} = \epsilon
\end{eqnarray*}">|; 

$key = q/(I,J)inAtimesB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$(I, J) \in A \times B$">|; 

$key = q/(a,b)inAtimesB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$(a,b) \in A \times B$">|; 

$key = q/(x,y),(a,b),(c,d)inAtimesB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$(x,y),(a,b),(c,d) \in A \times B$">|; 

$key = q/(x,y),(a,b)inAtimesB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$(x,y),(a,b) \in A \times B$">|; 

$key = q/(x,y)inAtimesB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$(x,y) \in A \times B$">|; 

$key = q/(x,y)inU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$(x,y) \in U$">|; 

$key = q/A,B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img3.svg"
 ALT="$A, B$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img17.svg"
 ALT="$A$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$B$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$F$">|; 

$key = q/L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$L$">|; 

$key = q/LinF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img40.svg"
 ALT="$L \in F$">|; 

$key = q/UsubseteqAtimesB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img24.svg"
 ALT="$U \subseteq A \times B$">|; 

$key = q/delta=min{delta_1,delta_2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\delta = \min \{ \delta_1, \delta_2 \}$">|; 

$key = q/delta_1,delta_2strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\delta_1, \delta_2 \strictsuperieur 0$">|; 

$key = q/delta_1strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\delta_1 \strictsuperieur 0$">|; 

$key = q/delta_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\delta_2$">|; 

$key = q/deltastrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\delta \strictsuperieur 0$">|; 

$key = q/displaystyle(x,y)=(a,b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle (x,y) = (a,b)$">|; 

$key = q/displaystyleL=lim_{ytob}lambda(y)=lim_{ytob}lim_{xtoa}f(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.92ex; vertical-align: -2.09ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\displaystyle L = \lim_{y \to b} \lambda(y) = \lim_{y \to b} \lim_{x \to a} f(x,y)$">|; 

$key = q/displaystyleM=lim_{xtoa}mu(y)=lim_{xtoa}lim_{ytob}f(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.92ex; vertical-align: -2.09ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle M = \lim_{x \to a} \mu(y) = \lim_{x \to a} \lim_{y \to b} f(x,y)$">|; 

$key = q/displaystyled=distance^2big[(a,b),(c,d)big]=maxbig{distance(a,c),distance(b,d)big};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle d = \distance^2\big[(a,b), (c,d)\big] = \max \big\{ \distance(a,c), \distance(b,d) \big\}$">|; 

$key = q/displaystyledelta=min{delta_1,delta_2}strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle \delta = \min \{ \delta_1, \delta_2 \} \strictsuperieur 0$">|; 

$key = q/displaystyledistance(a,c)ledistance(a,x)+distance(x,c);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \distance(a,c) \le \distance(a,x) + \distance(x,c)$">|; 

$key = q/displaystyledistance(b,d)ledistance(b,y)+distance(y,d);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \distance(b,d) \le \distance(b,y) + \distance(y,d)$">|; 

$key = q/displaystyledistance(f(x,y),L)leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \distance(f(x,y), L) \le \epsilon$">|; 

$key = q/displaystyledistance(f(x,y),lambda(y))leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\displaystyle \distance(f(x,y),\lambda(y)) \le \epsilon$">|; 

$key = q/displaystyledistance(f(x,y),lambda(y))lefrac{epsilon}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \distance(f(x,y),\lambda(y)) \le \frac{\epsilon}{2}$">|; 

$key = q/displaystyledistance(f(x,y),mu(x))leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle \distance(f(x,y),\mu(x)) \le \epsilon$">|; 

$key = q/displaystyledistance(f(x,y),mu(x))lefrac{epsilon}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \distance(f(x,y),\mu(x)) \le \frac{\epsilon}{2}$">|; 

$key = q/displaystyledistance(lambda(y),L)lefrac{epsilon}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\displaystyle \distance(\lambda(y), L) \le \frac{\epsilon}{2}$">|; 

$key = q/displaystyledistance(mu(x),M)lefrac{epsilon}{2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.34ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle \distance(\mu(x), M) \le \frac{\epsilon}{2}$">|; 

$key = q/displaystyledistance(x,a)=distance(y,b)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \distance(x,a) = \distance(y,b) = 0$">|; 

$key = q/displaystyledistance(x,a)ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \distance(x,a) \le \delta$">|; 

$key = q/displaystyledistance(x,a)ledelta_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \distance(x,a) \le \delta_1$">|; 

$key = q/displaystyledistance(x,a)ledelta_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \distance(x,a) \le \delta_2$">|; 

$key = q/displaystyledistance(y,b)ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \distance(y,b) \le \delta$">|; 

$key = q/displaystyledistance(y,b)ledelta_1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\displaystyle \distance(y,b) \le \delta_1$">|; 

$key = q/displaystyledistance(y,b)ledelta_2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle \distance(y,b) \le \delta_2$">|; 

$key = q/displaystyledistance^2:(AtimesB)times(AtimesB)mapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.76ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\displaystyle \distance^2 : (A \times B) \times (A \times B) \mapsto \corps$">|; 

$key = q/displaystyledistance^2big[(x,y),(a,b)big]=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle \distance^2\big[(x,y), (a,b)\big] = 0$">|; 

$key = q/displaystyledistance^2big[(x,y),(a,b)big]=distance^2big[(a,b),(x,y)big];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \distance^2\big[(x,y), (a,b)\big] = \distance^2\big[(a,b), (x,y)\big]$">|; 

$key = q/displaystyledistance^2big[(x,y),(a,b)big]=maxbig{distance(x,a),distance(y,b)big};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \distance^2\big[(x,y), (a,b)\big] = \max \big\{ \distance(x,a), \distance(y,b) \big\}$">|; 

$key = q/displaystyledistance^2big[(x,y),(a,b)big]=max{distance(x,a),distance(y,b)}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \distance^2\big[ (x,y), (a,b) \big] = \max \{ \distance(x,a), \distance(y,b) \} \le \delta$">|; 

$key = q/displaystyledistance^2big[(x,y),(x,y)big]=maxbig{distance(x,x),distance(y,y)big}=max{0,0}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.99ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \distance^2\big[(x,y), (x,y)\big] = \max \big\{ \distance(x,x), \distance(y,y) \big\} = \max \{ 0, 0 \} = 0$">|; 

$key = q/displaystyledistancebig[f(x,y),Lbig]leepsilon;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \distance\big[f(x,y), L\big] \le \epsilon$">|; 

$key = q/displaystylelambda(y)=lim_{xtoa}f(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \lambda(y) = \lim_{x \to a} f(x,y)$">|; 

$key = q/displaystylelim_{(x,y)to(a,b)}f(x,y)=L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.09ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \lim_{(x,y) \to (a,b)} f(x,y) = L$">|; 

$key = q/displaystylelim_{(x,y)to(a,b)}f(x,y)=M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.09ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \lim_{(x,y) \to (a,b)} f(x,y) = M$">|; 

$key = q/displaystylelim_{(x,y)to(a,b)}f(x,y)=lim_{xtoa}lim_{ytob}f(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.09ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle \lim_{(x,y) \to (a,b)} f(x,y) = \lim_{x \to a} \lim_{y \to b} f(x,y)$">|; 

$key = q/displaystylelim_{(x,y)to(a,b)}f(x,y)=lim_{ytob}lim_{xtoa}f(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.09ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \lim_{(x,y) \to (a,b)} f(x,y) = \lim_{y \to b} \lim_{x \to a} f(x,y)$">|; 

$key = q/displaystylelim_{(x,y)to+infty}f(x,y)=L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.09ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle \lim_{(x,y) \to +\infty} f(x,y) = L$">|; 

$key = q/displaystylelim_{(x,y)to-infty}f(x,y)=L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.09ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle \lim_{(x,y) \to -\infty} f(x,y) = L$">|; 

$key = q/displaystylelim_{substack{(x,y)to(a,b)(x,y)inU}}f(x,y)=L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.84ex; vertical-align: -4.02ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle \lim_{ \substack{ (x,y) \to (a,b) \ (x,y) \in U } } f(x,y) = L$">|; 

$key = q/displaystylemax{distance(x,a),distance(y,b)}ledelta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \max \{ \distance(x,a), \distance(y,b) \} \le \delta$">|; 

$key = q/displaystylemu(x)=lim_{ytob}f(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.92ex; vertical-align: -2.09ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle \mu(x) = \lim_{y \to b} f(x,y)$">|; 

$key = q/distance^2ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.49ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\distance^2 \ge 0$">|; 

$key = q/epsilonstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\epsilon \strictsuperieur 0$">|; 

$key = q/f:AtimesBmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img23.svg"
 ALT="$f : A \times B \mapsto F$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img26.svg"
 ALT="$f$">|; 

$key = q/lambda(y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\lambda(y)$">|; 

$key = q/mu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\mu(x)$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$x$">|; 

$key = q/x=a;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img12.svg"
 ALT="$x = a$">|; 

$key = q/xinA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img66.svg"
 ALT="$x \in A$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img2.svg"
 ALT="$y$">|; 

$key = q/y=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img13.svg"
 ALT="$y = b$">|; 

$key = q/yinB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img50.svg"
 ALT="$y \in B$">|; 

$key = q/{Eqts}distance(x,a)ledelta_1distance(y,b)ledelta_2{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img37.svg"
 ALT="\begin{Eqts}
\distance(x,a) \le \delta_1 \\\\
\distance(y,b) \le \delta_2
\end{Eqts}">|; 

$key = q/{Eqts}distance(x,a)ledeltadistance(y,b)ledelta{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img35.svg"
 ALT="\begin{Eqts}
\distance(x,a) \le \delta \\\\
\distance(y,b) \le \delta
\end{Eqts}">|; 

$key = q/{Eqts}distance(x,a)ledeltaledelta_1distance(y,b)ledeltaledelta_2{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img39.svg"
 ALT="\begin{Eqts}
\distance(x,a) \le \delta \le \delta_1 \\\\
\distance(y,b) \le \delta \le \delta_2
\end{Eqts}">|; 

$key = q/{Eqts}xgeIygeJ{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img45.svg"
 ALT="\begin{Eqts}
x \ge I \\\\
y \ge J
\end{Eqts}">|; 

$key = q/{Eqts}xleIyleJ{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img47.svg"
 ALT="\begin{Eqts}
x \le I \\\\
y \le J
\end{Eqts}">|; 

$key = q/{eqnarraystar}distance(f(x,y),M)&le&distance(f(x,y),mu(x))+distance(mu(x),M)&le&frac{epsilon}{2}+frac{epsilon}{2}=epsilon{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 13.12ex; " SRC="|."$dir".q|img74.svg"
 ALT="\begin{eqnarray*}
\distance(f(x,y),M) &amp;\le&amp; \distance(f(x,y),\mu(x)) + \distance...
...,M) \\\\
&amp;\le&amp; \frac{\epsilon}{2} + \frac{\epsilon}{2} = \epsilon
\end{eqnarray*}">|; 

1;

