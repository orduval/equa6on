# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/A(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$A(x)$">|; 

$key = q/A(x)subseteqM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$A(x) \subseteq M$">|; 

$key = q/Ainsousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$A \in \sousens(\Omega)$">|; 

$key = q/Binsousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$B \in \sousens(\Omega)$">|; 

$key = q/L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$L$">|; 

$key = q/Linminor_subseteqmathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.71ex; " SRC="|."$dir".q|img31.svg"
 ALT="$L \in \minor_\subseteq \mathcal{C}$">|; 

$key = q/LsubseteqA(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$L \subseteq A(x)$">|; 

$key = q/LsubseteqI;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img30.svg"
 ALT="$L \subseteq I$">|; 

$key = q/LsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img23.svg"
 ALT="$L \subseteq \Omega$">|; 

$key = q/M;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img14.svg"
 ALT="$M$">|; 

$key = q/Minmajor_subseteqmathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.91ex; " SRC="|."$dir".q|img21.svg"
 ALT="$M \in \major_\subseteq \mathcal{C}$">|; 

$key = q/MsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img10.svg"
 ALT="$M \subseteq \Omega$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\Omega$">|; 

$key = q/SsubseteqM;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img20.svg"
 ALT="$S \subseteq M$">|; 

$key = q/X;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$X$">|; 

$key = q/displaystyleAsubseteqB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle A \subseteq B$">|; 

$key = q/displaystyleI=bigcap_{xinX}A(x)insousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle I = \bigcap_{x \in X} A(x) \in \sousens(\Omega)$">|; 

$key = q/displaystyleI=max_subseteqminor_subseteqmathcal{C}=inf_subseteqmathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle I = \max_\subseteq \minor_\subseteq \mathcal{C} = \inf_\subseteq \mathcal{C}$">|; 

$key = q/displaystyleIinminor_subseteqmathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.68ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle I \in \minor_\subseteq \mathcal{C}$">|; 

$key = q/displaystyleIsubseteqbigcap_{xinX}A(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle I \subseteq \bigcap_{x \in X} A(x)$">|; 

$key = q/displaystyleS=bigcup_{xinX}A(x)insousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle S = \bigcup_{x \in X} A(x) \in \sousens(\Omega)$">|; 

$key = q/displaystyleS=min_subseteqmajor_subseteqmathcal{C}=sup_subseteqmathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.13ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle S = \min_\subseteq \major_\subseteq \mathcal{C} = \sup_\subseteq \mathcal{C}$">|; 

$key = q/displaystyleSinmajor_subseteqmathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.13ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle S \in \major_\subseteq \mathcal{C}$">|; 

$key = q/displaystylebigcup_{xinX}A(x)subseteqS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \bigcup_{x \in X} A(x) \subseteq S$">|; 

$key = q/displaystyleinf_subseteq{A(x)insousens(Omega):xinX}=bigcap_{xinX}A(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \inf_\subseteq \{ A(x) \in \sousens(\Omega) : x \in X \} = \bigcap_{x \in X} A(x)$">|; 

$key = q/displaystylemajor_subseteqmathcal{C}=left{Minsousens(Omega):bigcup_{xinX}A(x)subseteqMright};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.29ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \major_\subseteq \mathcal{C} = \left\{ M \in \sousens(\Omega) : \bigcup_{x \in X} A(x) \subseteq M \right\}$">|; 

$key = q/displaystylemathcal{C}={A(x)insousens(Omega):xinX};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle \mathcal{C} = \{ A(x) \in \sousens(\Omega) : x \in X \}$">|; 

$key = q/displaystyleminor_subseteqmathcal{C}=left{Linsousens(Omega):Lsubseteqbigcap_{xinX}A(x)right};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.29ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \minor_\subseteq \mathcal{C} = \left\{ L \in \sousens(\Omega) : L \subseteq \bigcap_{x \in X} A(x) \right\}$">|; 

$key = q/displaystylesousens(Omega)={A:AsubseteqOmega};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\displaystyle \sousens(\Omega) = \{ A : A \subseteq \Omega \}$">|; 

$key = q/displaystylesup_subseteq{A(x)insousens(Omega):xinX}=bigcup_{xinX}A(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \sup_\subseteq \{ A(x) \in \sousens(\Omega) : x \in X \} = \bigcup_{x \in X} A(x)$">|; 

$key = q/ensinferieur;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\ensinferieur$">|; 

$key = q/mathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\mathcal{C}$">|; 

$key = q/sousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\sousens(\Omega)$">|; 

$key = q/subseteq;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\subseteq$">|; 

$key = q/xinX;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img13.svg"
 ALT="$x \in X$">|; 

1;

