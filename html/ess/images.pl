# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/0leklen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img45.svg"
 ALT="$0 \le k \le n$">|; 

$key = q/A,Binmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$A,B \in \mathcal{T}$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$A$">|; 

$key = q/A_msubseteqA_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img29.svg"
 ALT="$A_m \subseteq A_n$">|; 

$key = q/A_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img43.svg"
 ALT="$A_n$">|; 

$key = q/A_n=A_{n-1}cupD_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img37.svg"
 ALT="$A_n = A_{n - 1} \cup D_n$">|; 

$key = q/A_nsubseteqA_{m-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img58.svg"
 ALT="$A_n \subseteq A_{m - 1}$">|; 

$key = q/A_{n-1},D_nsubseteqA_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img36.svg"
 ALT="$A_{n - 1}, D_n \subseteq A_n$">|; 

$key = q/Ainmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img2.svg"
 ALT="$A \in \mathcal{T}$">|; 

$key = q/Asetminus(NcupZ);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img111.svg"
 ALT="$A \setminus (N \cup Z)$">|; 

$key = q/Asetminus(ZcupN);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img120.svg"
 ALT="$A \setminus (Z \cup N)$">|; 

$key = q/AsetminusN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img7.svg"
 ALT="$A \setminus N$">|; 

$key = q/AsubseteqB;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img10.svg"
 ALT="$A \subseteq B$">|; 

$key = q/C;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img75.svg"
 ALT="$C$">|; 

$key = q/C_msubseteqC_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img85.svg"
 ALT="$C_m \subseteq C_n$">|; 

$key = q/C_n=A_nsetminusZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$C_n = A_n \setminus Z$">|; 

$key = q/C_nsubseteqC_{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img83.svg"
 ALT="$C_n \subseteq C_{n + 1}$">|; 

$key = q/D_i=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img51.svg"
 ALT="$D_i = \emptyset$">|; 

$key = q/D_icapD_j=D_mcapD_n=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img61.svg"
 ALT="$D_i \cap D_j = D_m \cap D_n = \emptyset$">|; 

$key = q/D_icapD_j=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img53.svg"
 ALT="$D_i \cap D_j = \emptyset$">|; 

$key = q/D_j=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img52.svg"
 ALT="$D_j = \emptyset$">|; 

$key = q/D_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img44.svg"
 ALT="$D_k$">|; 

$key = q/D_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img60.svg"
 ALT="$D_n$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img24.svg"
 ALT="$E$">|; 

$key = q/E=AsetminusZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$E = A \setminus Z$">|; 

$key = q/E_n=A_nsetminusZ_nsubseteqA_{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img72.svg"
 ALT="$E_n = A_n \setminus Z_n \subseteq A_{n + 1}$">|; 

$key = q/E_nsubseteqA_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img68.svg"
 ALT="$E_n \subseteq A_n$">|; 

$key = q/E_nsubseteqA_{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img69.svg"
 ALT="$E_n \subseteq A_{n + 1}$">|; 

$key = q/Einessentiel(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$E \in \essentiel(A)$">|; 

$key = q/NcupZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img115.svg"
 ALT="$N \cup Z$">|; 

$key = q/Ninmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img5.svg"
 ALT="$N \in \mathcal{T}$">|; 

$key = q/NsubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img4.svg"
 ALT="$N \subseteq A$">|; 

$key = q/S;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img95.svg"
 ALT="$S$">|; 

$key = q/Z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$Z$">|; 

$key = q/Z_nsubseteqA_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img70.svg"
 ALT="$Z_n \subseteq A_n$">|; 

$key = q/Zinmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img20.svg"
 ALT="$Z \in \mathcal{T}$">|; 

$key = q/ZsubseteqA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img21.svg"
 ALT="$Z \subseteq A$">|; 

$key = q/displaystyle0lemu(A)lemu(B)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle 0 \le \mu(A) \le \mu(B) = 0$">|; 

$key = q/displaystyle0lemu(AcupB)lemu(A)+mu(B)=0+0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle 0 \le \mu(A \cup B) \le \mu(A) + \mu(B) = 0 + 0 = 0$">|; 

$key = q/displaystyleA=bigcup_{ninsetN}A_n=bigcup_{kinsetN}D_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.68ex; vertical-align: -3.17ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle A = \bigcup_{n \in \setN} A_n = \bigcup_{k \in \setN} D_k$">|; 

$key = q/displaystyleA=bigcup_{ninsetN}A_ninmathcal{T};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.16ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle A = \bigcup_{n \in \setN} A_n \in \mathcal{T}$">|; 

$key = q/displaystyleA_n=D_ncupA_{n-1}=D_ncupbigcup_{k=0}^{n-1}D_k=bigcup_{k=0}^nD_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.27ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\displaystyle A_n = D_n \cup A_{n - 1} = D_n \cup \bigcup_{k = 0}^{n - 1} D_k = \bigcup_{k = 0}^n D_k$">|; 

$key = q/displaystyleA_n=bigcup_{k=0}^nD_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\displaystyle A_n = \bigcup_{k = 0}^n D_k$">|; 

$key = q/displaystyleC=bigcup_{ninsetN}C_n=bigcup_{ninsetN}A_nsetminusZ=AsetminusZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.16ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle C = \bigcup_{n \in \setN} C_n = \bigcup_{n \in \setN} A_n \setminus Z = A \setminus Z$">|; 

$key = q/displaystyleC_0subseteqC_1subseteqC_2subseteqC_3subseteq...;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\displaystyle C_0 \subseteq C_1 \subseteq C_2 \subseteq C_3 \subseteq ...$">|; 

$key = q/displaystyleD={xinA:f(x)neg(x)}=ZcupN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img136.svg"
 ALT="$\displaystyle D = \{ x \in A : f(x) \ne g(x)\} = Z \cup N$">|; 

$key = q/displaystyleZ=bigcup_{ninsetN}Z_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.16ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle Z = \bigcup_{n \in \setN} Z_n$">|; 

$key = q/displaystyleessentiel(A)={AsetminusN:NsubseteqA,Ninmathcal{T},mu(N)=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \essentiel(A) = \{ A \setminus N : N \subseteq A,  N \in \mathcal{T},  \mu(N) = 0 \}$">|; 

$key = q/displaystylef+uessinferieurg+v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\displaystyle f + u \essinferieur g + v$">|; 

$key = q/displaystylef-uessinferieurg-v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\displaystyle f - u \essinferieur g - v$">|; 

$key = q/displaystylefessegalg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img134.svg"
 ALT="$\displaystyle f \essegal g$">|; 

$key = q/displaystylefessinferieurg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle f \essinferieur g$">|; 

$key = q/displaystylefessinferieurh;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\displaystyle f \essinferieur h$">|; 

$key = q/displaystylefesssuperieurg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle f \esssuperieur g$">|; 

$key = q/displaystylelim_{ntoinfty}mu(A_n)=mu(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\displaystyle \lim_{n \to \infty} \mu(A_n) = \mu(A)$">|; 

$key = q/displaystylelim_{ntoinfty}mu(C_n)=mu(C)=mu(AsetminusZ);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\displaystyle \lim_{n \to \infty} \mu(C_n) = \mu(C) = \mu(A \setminus Z)$">|; 

$key = q/displaystylemax{f,g}essinferieurh;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\displaystyle \max\{f,g\} \essinferieur h$">|; 

$key = q/displaystylemin{f,g}esssuperieurh;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\displaystyle \min\{f,g\} \esssuperieur h$">|; 

$key = q/displaystylemu(A)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \mu(A) = 0$">|; 

$key = q/displaystylemu(A)=mu(E)+mu(Z)=mu(E)+0=mu(E);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \mu(A) = \mu(E) + \mu(Z) = \mu(E) + 0 = \mu(E)$">|; 

$key = q/displaystylemu(A)=sum_{ninsetN}mu(D_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.16ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \mu(A) = \sum_{n \in \setN} \mu(D_n)$">|; 

$key = q/displaystylemu(A_n)=sum_{k=0}^nmu(D_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img62.svg"
 ALT="$\displaystyle \mu(A_n) = \sum_{k = 0}^n \mu(D_k)$">|; 

$key = q/displaystylemu(AcupB)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \mu(A \cup B) = 0$">|; 

$key = q/displaystylemu(E)=mu(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \mu(E) = \mu(A)$">|; 

$key = q/displaystylemu({xinA:f(x)-g(x)strictinferieur0})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle \mu(\{ x \in A : f(x) - g(x) \strictinferieur 0 \}) = 0$">|; 

$key = q/displaystylemu({xinA:f(x)-g(x)strictsuperieur0})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle \mu(\{ x \in A : f(x) - g(x) \strictsuperieur 0 \}) = 0$">|; 

$key = q/displaystylemu({xinA:f(x)strictinferieurg(x)})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle \mu(\{ x \in A : f(x) \strictinferieur g(x) \}) = 0$">|; 

$key = q/displaystylemu({xinA:f(x)strictsuperieurg(x)})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle \mu(\{ x \in A : f(x) \strictsuperieur g(x) \}) = 0$">|; 

$key = q/displaystylemuleft(bigcup_{i=1}^nA_iright)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.15ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \mu\left( \bigcup_{i = 1}^n A_i \right) = 0$">|; 

$key = q/displaystylemuleft(bigcup_{i=1}^{+infty}A_iright)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.16ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle \mu\left( \bigcup_{i = 1}^{+\infty} A_i \right) = 0$">|; 

$key = q/displaystylesum_{ninsetN}mu(D_n)=lim_{ntoinfty}sum_{k=0}^nmu(D_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.03ex; vertical-align: -3.16ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle \sum_{n \in \setN} \mu(D_n) = \lim_{n \to \infty} \sum_{k = 0}^n \mu(D_k)$">|; 

$key = q/displaystylexnotinA_n=bigcup_{k=0}^nD_ksubseteqA_{m-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.94ex; vertical-align: -3.07ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle x \notin A_n = \bigcup_{k = 0}^n D_k \subseteq A_{m - 1}$">|; 

$key = q/f(x)+u(x)leg(x)+v(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img119.svg"
 ALT="$f(x) + u(x) \le g(x) + v(x)$">|; 

$key = q/f(x)-u(x)leg(x)-v(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img124.svg"
 ALT="$f(x) - u(x) \le g(x) - v(x)$">|; 

$key = q/f(x)geg(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img100.svg"
 ALT="$f(x) \ge g(x)$">|; 

$key = q/f(x)leg(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img96.svg"
 ALT="$f(x) \le g(x)$">|; 

$key = q/f(x)leh(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$f(x) \le h(x)$">|; 

$key = q/f,g,h:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img106.svg"
 ALT="$f,g,h : A \mapsto \setR$">|; 

$key = q/f,g:AmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img90.svg"
 ALT="$f,g : A \mapsto \setR$">|; 

$key = q/f,gessinferieurh;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img126.svg"
 ALT="$f,g \essinferieur h$">|; 

$key = q/f,gesssuperieurh;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img130.svg"
 ALT="$f,g \esssuperieur h$">|; 

$key = q/f-g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img91.svg"
 ALT="$f - g$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img92.svg"
 ALT="$f$">|; 

$key = q/fessinferieurg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img102.svg"
 ALT="$f \essinferieur g$">|; 

$key = q/fesssuperieurg;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img104.svg"
 ALT="$f \esssuperieur g$">|; 

$key = q/g(x)leh(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img112.svg"
 ALT="$g(x) \le h(x)$">|; 

$key = q/g;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img93.svg"
 ALT="$g$">|; 

$key = q/h;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img114.svg"
 ALT="$h$">|; 

$key = q/i,j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img47.svg"
 ALT="$i,j$">|; 

$key = q/inej;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img48.svg"
 ALT="$i \ne j$">|; 

$key = q/m,n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img30.svg"
 ALT="$m,n$">|; 

$key = q/m,ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img86.svg"
 ALT="$m,n \in \setN$">|; 

$key = q/m-1gen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img57.svg"
 ALT="$m - 1 \ge n$">|; 

$key = q/m=max{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$m = \max \{ i, j \}$">|; 

$key = q/max{f(x),g(x)}leh(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\max\{f(x) , g(x)\} \le h(x)$">|; 

$key = q/min{f(x),g(x)}geh(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\min\{f(x) , g(x)\} \ge h(x)$">|; 

$key = q/mlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img31.svg"
 ALT="$m \le n$">|; 

$key = q/mu(A)=mu(B)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\mu(A) = \mu(B) = 0$">|; 

$key = q/mu(B)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\mu(B) = 0$">|; 

$key = q/mu(C)=mu(A);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\mu(C) = \mu(A)$">|; 

$key = q/mu(C_n)=mu(A_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\mu(C_n) = \mu(A_n)$">|; 

$key = q/mu(N)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\mu(N) = 0$">|; 

$key = q/mu(NcupZ)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img110.svg"
 ALT="$\mu(N \cup Z) = 0$">|; 

$key = q/mu(Z)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\mu(Z) = 0$">|; 

$key = q/mu(Z)=mu(N)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\mu(Z) = \mu(N) = 0$">|; 

$key = q/mu(Z_n)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\mu(Z_n) = 0$">|; 

$key = q/mu:mathcal{T}mapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\mu : \mathcal{T} \mapsto \setR$">|; 

$key = q/n-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img40.svg"
 ALT="$n - 1$">|; 

$key = q/n=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img39.svg"
 ALT="$n = 0$">|; 

$key = q/n=min{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$n = \min \{ i,j \}$">|; 

$key = q/nge1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img35.svg"
 ALT="$n \ge 1$">|; 

$key = q/ninsetN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img67.svg"
 ALT="$n \in \setN$">|; 

$key = q/setN;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\setN$">|; 

$key = q/uessinferieurv;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img117.svg"
 ALT="$u \essinferieur v$">|; 

$key = q/uesssuperieurv;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.41ex; vertical-align: -0.63ex; " SRC="|."$dir".q|img122.svg"
 ALT="$u \esssuperieur v$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img42.svg"
 ALT="$x$">|; 

$key = q/xinA_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img55.svg"
 ALT="$x \in A_m$">|; 

$key = q/xinA_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img78.svg"
 ALT="$x \in A_n$">|; 

$key = q/xinA_{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img80.svg"
 ALT="$x \in A_{n + 1}$">|; 

$key = q/xinA_{n+1}setminusZ=C_{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$x \in A_{n + 1} \setminus Z = C_{n + 1}$">|; 

$key = q/xinC_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img77.svg"
 ALT="$x \in C_n$">|; 

$key = q/xinD_m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img54.svg"
 ALT="$x \in D_m$">|; 

$key = q/xinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img97.svg"
 ALT="$x \in S$">|; 

$key = q/xnotinA_{m-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img56.svg"
 ALT="$x \notin A_{m - 1}$">|; 

$key = q/xnotinZ;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img81.svg"
 ALT="$x \notin Z$">|; 

$key = q/xnotinZ_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img79.svg"
 ALT="$x \notin Z_n$">|; 

$key = q/{A_ninmathcal{T}:ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\{ A_n \in \mathcal{T} : n \in \setN \}$">|; 

$key = q/{D_nsubseteqA:ninsetN};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\{D_n \subseteq A : n \in \setN \}$">|; 

$key = q/{Eqts}Z={xinA:f(x)strictinferieurg(x)}N={xinA:f(x)strictsuperieurg(x)}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img135.svg"
 ALT="\begin{Eqts}
Z = \{ x \in A : f(x) \strictinferieur g(x)\} \\\\
N = \{ x \in A : f(x) \strictsuperieur g(x)\}
\end{Eqts}">|; 

$key = q/{Eqts}Z={xinA:f(x)strictinferieurh(x)}N={xinA:g(x)strictinferieurh(x)}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img131.svg"
 ALT="\begin{Eqts}
Z = \{ x \in A : f(x) \strictinferieur h(x)\} \\\\
N = \{ x \in A : g(x) \strictinferieur h(x)\}
\end{Eqts}">|; 

$key = q/{Eqts}Z={xinA:f(x)strictsuperieurg(x)}N={xinA:g(x)strictsuperieurh(x)}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img108.svg"
 ALT="\begin{Eqts}
Z = \{ x \in A : f(x) \strictsuperieur g(x)\} \\\\
N = \{ x \in A : g(x) \strictsuperieur h(x)\}
\end{Eqts}">|; 

$key = q/{Eqts}Z={xinA:f(x)strictsuperieurg(x)}N={xinA:u(x)strictinferieurv(x)}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img123.svg"
 ALT="\begin{Eqts}
Z = \{ x \in A : f(x) \strictsuperieur g(x)\} \\\\
N = \{ x \in A : u(x) \strictinferieur v(x)\}
\end{Eqts}">|; 

$key = q/{Eqts}Z={xinA:f(x)strictsuperieurg(x)}N={xinA:u(x)strictsuperieurv(x)}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img118.svg"
 ALT="\begin{Eqts}
Z = \{ x \in A : f(x) \strictsuperieur g(x)\} \\\\
N = \{ x \in A : u(x) \strictsuperieur v(x)\}
\end{Eqts}">|; 

$key = q/{Eqts}Z={xinA:f(x)strictsuperieurh(x)}N={xinA:g(x)strictsuperieurh(x)}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img127.svg"
 ALT="\begin{Eqts}
Z = \{ x \in A : f(x) \strictsuperieur h(x)\} \\\\
N = \{ x \in A : g(x) \strictsuperieur h(x)\}
\end{Eqts}">|; 

$key = q/{Eqts}fessinferieurggessinferieurh{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img107.svg"
 ALT="\begin{Eqts}
f \essinferieur g \ \\\\
g \essinferieur h
\end{Eqts}">|; 

$key = q/{eqnarraystar}D_0&=&A_0D_n&=&A_nsetminusA_{n-1}{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img34.svg"
 ALT="\begin{eqnarray*}
D_0 &amp;=&amp; A_0 \\\\
D_n &amp;=&amp; A_n \setminus A_{n - 1}
\end{eqnarray*}">|; 

1;

