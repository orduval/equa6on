# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(Omega,topologie);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$(\Omega,\topologie)$">|; 

$key = q/A,Bintopologie;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$A,B \in \topologie$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$A$">|; 

$key = q/AsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img27.svg"
 ALT="$A \subseteq \Omega$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img28.svg"
 ALT="$F$">|; 

$key = q/F=OmegasetminusU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$F = \Omega \setminus U$">|; 

$key = q/FsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img13.svg"
 ALT="$F \subseteq \Omega$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\Omega$">|; 

$key = q/OmegasetminusA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\Omega \setminus A$">|; 

$key = q/U,Vintopologie;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img19.svg"
 ALT="$U,V \in \topologie$">|; 

$key = q/U;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$U$">|; 

$key = q/U=OmegasetminusF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$U = \Omega \setminus F$">|; 

$key = q/UcapV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$U \cap V$">|; 

$key = q/Uintopologie;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img12.svg"
 ALT="$U \in \topologie$">|; 

$key = q/VsubseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img24.svg"
 ALT="$V \subseteq \Omega$">|; 

$key = q/adhAinferme;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.39ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\adh A \in \ferme$">|; 

$key = q/bigcapF=OmegasetminusbigcupU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\bigcap F = \Omega \setminus \bigcup U$">|; 

$key = q/bigcupU=OmegasetminusbigcapF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\bigcup U = \Omega \setminus \bigcap F$">|; 

$key = q/displaystyleAcapBintopologie;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle A \cap B \in \topologie$">|; 

$key = q/displaystyleF=OmegasetminusU;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle F = \Omega \setminus U$">|; 

$key = q/displaystyleFcupG=Omegasetminus(UcapV);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle F \cup G = \Omega \setminus (U \cap V)$">|; 

$key = q/displaystyleU=bigcupmathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.90ex; vertical-align: -1.37ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle U = \bigcup \mathcal{C}$">|; 

$key = q/displaystyleUcapV=emptyset;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.06ex; vertical-align: -0.23ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle U \cap V = \emptyset$">|; 

$key = q/displaystyleadhA=Omegasetminusinterieur(OmegasetminusA);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \adh A = \Omega \setminus \interieur (\Omega \setminus A)$">|; 

$key = q/displaystyleadhA=inf_subseteq{Finferme:AsubseteqF}=bigcap{Finferme:AsubseteqF};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.53ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \adh A = \inf_\subseteq \{ F \in \ferme : A \subseteq F \} = \bigcap \{ F \in \ferme : A \subseteq F \}$">|; 

$key = q/displaystylebigcapmathcal{F}=Omegasetminusbigcupmathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.90ex; vertical-align: -1.37ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle \bigcap \mathcal{F} = \Omega \setminus \bigcup \mathcal{C}$">|; 

$key = q/displaystylebigcupmathcal{C}intopologie;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.90ex; vertical-align: -1.37ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \bigcup \mathcal{C} \in \topologie$">|; 

$key = q/displaystyleferme={OmegasetminusU:Uintopologie};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle \ferme = \{ \Omega \setminus U : U \in \topologie \}$">|; 

$key = q/displaystylefrontiereA=(adhA)setminus(interieurA);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \frontiere A = (\adh A) \setminus (\interieur A)$">|; 

$key = q/displaystyleinterieurA=Omegasetminusadh(OmegasetminusA);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img43.svg"
 ALT="$\displaystyle \interieur A = \Omega \setminus \adh (\Omega \setminus A)$">|; 

$key = q/displaystyleinterieurA=sup_subseteq{Uintopologie:UsubseteqA}=bigcup{Uintopologie:UsubseteqA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.98ex; vertical-align: -2.46ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\displaystyle \interieur A = \sup_\subseteq \{ U \in \topologie : U \subseteq A \} = \bigcup \{ U \in \topologie : U \subseteq A \}$">|; 

$key = q/displaystyleinterieurAsubseteqAsubseteqadhA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \interieur A \subseteq A \subseteq \adh A$">|; 

$key = q/displaystylemathcal{F}={OmegasetminusU:Uinmathcal{C}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle \mathcal{F} = \{ \Omega \setminus U : U \in \mathcal{C} \}$">|; 

$key = q/displaystyletopologies(mathcal{C},Omega)=inf_subseteq{topologieintopologies(Omega):mathcal{C}subseteqtopologie};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.84ex; vertical-align: -2.01ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\displaystyle \topologies(\mathcal{C},\Omega) = \inf_\subseteq \{ \topologie \in \topologies(\Omega) : \mathcal{C} \subseteq \topologie \}$">|; 

$key = q/displaystylexinU,yinV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img46.svg"
 ALT="$\displaystyle x \in U,  y \in V$">|; 

$key = q/emptyset,Omegaintopologie;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img4.svg"
 ALT="$\emptyset, \Omega \in \topologie$">|; 

$key = q/interieurAintopologie;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\interieur A \in \topologie$">|; 

$key = q/mathcal{C};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\mathcal{C}$">|; 

$key = q/mathcal{C}subseteqsousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\mathcal{C} \subseteq \sousens(\Omega)$">|; 

$key = q/mathcal{C}subseteqtopologie;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.11ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\mathcal{C} \subseteq \topologie$">|; 

$key = q/topologie;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\topologie$">|; 

$key = q/topologies(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\topologies(\Omega)$">|; 

$key = q/topologiesubseteqsousens(Omega);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\topologie \subseteq \sousens(\Omega)$">|; 

$key = q/x,yinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$x,y \in \Omega$">|; 

$key = q/xinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img25.svg"
 ALT="$x \in \Omega$">|; 

$key = q/xinUsubseteqV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img26.svg"
 ALT="$x \in U \subseteq V$">|; 

$key = q/xney;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img45.svg"
 ALT="$x \ne y$">|; 

$key = q/{Eqts}F=OmegasetminusUG=OmegasetminusV{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img20.svg"
 ALT="\begin{Eqts}
F = \Omega \setminus U \\\\
G = \Omega \setminus V
\end{Eqts}">|; 

$key = q/{emptyset,Omega}subseteqtopologiecapferme;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\{\emptyset,\Omega\} \subseteq \topologie \cap \ferme$">|; 

1;

