# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img35.svg"
 ALT="\begin{eqnarray*}
\scalaire{\alpha \cdot u + \beta \cdot v}{w} &amp;=&amp; \conjaccent{\...
...e{w}{u}} + \conjaccent{\beta} \cdot \conjaccent{\scalaire{w}{v}}
\end{eqnarray*}">|; 

$key = q/(e_1,..,e_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img112.svg"
 ALT="$(e_1,..,e_n)$">|; 

$key = q/(e_1,...,e_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img66.svg"
 ALT="$(e_1,...,e_n)$">|; 

$key = q/(f_1,...,f_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$(f_1,...,f_m)$">|; 

$key = q/0inV^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.20ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img55.svg"
 ALT="$0 \in V^\orthogonal$">|; 

$key = q/A(u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img88.svg"
 ALT="$A(u)$">|; 

$key = q/A:EmapstoE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img87.svg"
 ALT="$A : E \mapsto E$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img90.svg"
 ALT="$A$">|; 

$key = q/A=conjaccent{U^T}cdotUinmatrice(corps,m,m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.96ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img143.svg"
 ALT="$A = \conjaccent{U^T} \cdot U \in \matrice(\corps,m,m)$">|; 

$key = q/Ainmatrice(corps,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img122.svg"
 ALT="$A \in \matrice(\corps,m,n)$">|; 

$key = q/E,F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img110.svg"
 ALT="$E,F$">|; 

$key = q/E;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img4.svg"
 ALT="$E$">|; 

$key = q/F;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img114.svg"
 ALT="$F$">|; 

$key = q/Ucdotxne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img146.svg"
 ALT="$U \cdot x \ne 0$">|; 

$key = q/Uinmatrice(corps,n,m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img138.svg"
 ALT="$U \in \matrice(\corps,n,m)$">|; 

$key = q/V;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img47.svg"
 ALT="$V$">|; 

$key = q/V^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img52.svg"
 ALT="$V^\orthogonal$">|; 

$key = q/VsubseteqE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.09ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img46.svg"
 ALT="$V \subseteq E$">|; 

$key = q/a_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img77.svg"
 ALT="$a_i$">|; 

$key = q/alpha,betaincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\alpha,\beta \in \corps$">|; 

$key = q/alpha,betainsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\alpha,\beta \in \setC$">|; 

$key = q/alpha,betainsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\alpha,\beta \in \setR$">|; 

$key = q/alpha=scalaire{u}{v}insetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\alpha = \scalaire{u}{v} \in \setC$">|; 

$key = q/alphacdotx+betacdotyinV^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.56ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\alpha \cdot x + \beta \cdot y \in V^\orthogonal$">|; 

$key = q/conjaccent{A^T}=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.38ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\conjaccent{A^T} = A$">|; 

$key = q/conjaccent{l}_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\conjaccent{l}_i$">|; 

$key = q/conjaccent{u}_i=u_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img84.svg"
 ALT="$\conjaccent{u}_i = u_i$">|; 

$key = q/conjaccent{x^T}cdotAcdotx>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.44ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img128.svg"
 ALT="$\conjaccent{x^T} \cdot A \cdot x &gt; 0$">|; 

$key = q/conjaccent{x^T}cdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.83ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img107.svg"
 ALT="$\conjaccent{x^T} \cdot y$">|; 

$key = q/conjaccent{x}=x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.65ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\conjaccent{x} = x$">|; 

$key = q/conjaccent{x}^TcdotAcdotxstrictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.16ex; " SRC="|."$dir".q|img147.svg"
 ALT="$\conjaccent{x}^T \cdot A \cdot x \strictsuperieur 0$">|; 

$key = q/corps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\corps$">|; 

$key = q/corps=setC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\corps = \setC$">|; 

$key = q/corps=setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\corps = \setR$">|; 

$key = q/corps^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\corps^n$">|; 

$key = q/corpsin{setR,setC};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\corps \in \{ \setR , \setC \}$">|; 

$key = q/displaystyle0=composante_i(Acdotx)=l_icdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.02ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img151.svg"
 ALT="$\displaystyle 0 = \composante_i (A \cdot x) = l_i \cdot x$">|; 

$key = q/displaystyleU=[u_1u_2...u_m];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle U = [u_1  u_2  ...  u_m]$">|; 

$key = q/displaystyleV^orthogonal=bigcap_{xinV}x^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.67ex; vertical-align: -3.15ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle V^\orthogonal = \bigcap_{x \in V} x^\orthogonal$">|; 

$key = q/displaystylea_i=scalaire{e_i}{0}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img79.svg"
 ALT="$\displaystyle a_i = \scalaire{e_i}{0} = 0$">|; 

$key = q/displaystylecomposante_{ij}A=scalaire{e_i}{e_j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.32ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle \composante_{ij} A = \scalaire{e_i}{e_j}$">|; 

$key = q/displaystylecomposante_{ij}A=scalaire{f_i}{mathcal{A}(e_j)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.32ex; vertical-align: -2.49ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\displaystyle \composante_{ij} A = \scalaire{f_i}{\mathcal{A}(e_j)}$">|; 

$key = q/displaystyleconjaccent{A^T}=conjaccent{(conjaccent{U^T}cdotU)^T}=conjaccent{U^T}cdotU=A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.42ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img148.svg"
 ALT="$\displaystyle \conjaccent{A^T} = \conjaccent{(\conjaccent{U^T} \cdot U)^T} = \conjaccent{U^T} \cdot U = A$">|; 

$key = q/displaystyleconjaccent{x^T}cdotAcdotx=conjaccent{x^T}cdotconjaccent{U^T}cdotUcdotx=conjugue(Ucdotx)^Tcdot(Ucdotx)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.96ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img145.svg"
 ALT="$\displaystyle \conjaccent{x^T} \cdot A \cdot x = \conjaccent{x^T} \cdot \conjaccent{U^T} \cdot U \cdot x = \conjugue(U \cdot x)^T \cdot (U \cdot x) \ge 0$">|; 

$key = q/displaystylee_i=(indicatrice_{ij})_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img102.svg"
 ALT="$\displaystyle e_i = ( \indicatrice_{ij} )_{i,j}$">|; 

$key = q/displaystylee_i^Tcdote_j=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.85ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle e_i^T \cdot e_j = \indicatrice_{ij}$">|; 

$key = q/displaystyleg_{ij}=scalaire{e_i}{e_j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle g_{ij} = \scalaire{e_i}{e_j}$">|; 

$key = q/displaystylenoyauA=combilin{conjaccent{l}_1,...,conjaccent{l}_m}^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.84ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\displaystyle \noyau A = \combilin{\conjaccent{l}_1,...,\conjaccent{l}_m}^\orthogonal$">|; 

$key = q/displaystylephi_u(v)=forme{phi_u}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\displaystyle \phi_u(v) = \forme{\phi_u}{v}$">|; 

$key = q/displaystylescalaire{X}{Y}=conjaccent{X^T}cdotY=conjaccent{x^T}cdotconjaccent{U^T}cdotUcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.96ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\displaystyle \scalaire{X}{Y} = \conjaccent{X^T} \cdot Y = \conjaccent{x^T} \cdot \conjaccent{U^T} \cdot U \cdot y$">|; 

$key = q/displaystylescalaire{alphacdotu+betacdotv}{w}=alphacdotscalaire{u}{w}+betacdotscalaire{v}{w};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle \scalaire{\alpha \cdot u + \beta \cdot v}{w} = \alpha \cdot \scalaire{u}{w} + \beta \cdot \scalaire{v}{w}$">|; 

$key = q/displaystylescalaire{alphacdotu+betacdotv}{w}=conjaccent{alpha}cdotscalaire{u}{w}+conjaccent{beta}cdotscalaire{v}{w};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.84ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$\displaystyle \scalaire{\alpha \cdot u + \beta \cdot v}{w} = \conjaccent{\alpha} \cdot \scalaire{u}{w} + \conjaccent{\beta} \cdot \scalaire{v}{w}$">|; 

$key = q/displaystylescalaire{e_i}{e_j}=indicatrice_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \scalaire{e_i}{e_j} = \indicatrice_{ij}$">|; 

$key = q/displaystylescalaire{e_k}{u}=u_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \scalaire{e_k}{u} = u_k$">|; 

$key = q/displaystylescalaire{hat{x}}{hat{y}}=conjaccent{x^T}cdotAcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.96ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\displaystyle \scalaire{\hat{x}}{\hat{y}} = \conjaccent{x^T} \cdot A \cdot y$">|; 

$key = q/displaystylescalaire{hat{x}}{hat{y}}=sum_{i,j}conjaccent{x}_iscalaire{e_i}{e_j}y_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\displaystyle \scalaire{\hat{x}}{\hat{y}} = \sum_{i,j} \conjaccent{x}_i \scalaire{e_i}{e_j} y_j$">|; 

$key = q/displaystylescalaire{u-v}{u-v}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \scalaire{u - v}{u - v} = 0$">|; 

$key = q/displaystylescalaire{u-v}{w}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle \scalaire{u - v}{w} = 0$">|; 

$key = q/displaystylescalaire{u}{A(u)}=scalaire{A(u)}{u}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\displaystyle \scalaire{u}{A(u)} = \scalaire{A(u)}{u} \ge 0$">|; 

$key = q/displaystylescalaire{u}{alphacdotv+betacdotw}=alphacdotscalaire{u}{v}+betacdotscalaire{u}{w};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle \scalaire{u}{\alpha \cdot v + \beta \cdot w} = \alpha \cdot
\scalaire{u}{v} + \beta \cdot \scalaire{u}{w}$">|; 

$key = q/displaystylescalaire{u}{u}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \scalaire{u}{u} = 0$">|; 

$key = q/displaystylescalaire{u}{u}=conjaccent{scalaire{u}{u}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$\displaystyle \scalaire{u}{u} = \conjaccent{\scalaire{u}{u}}$">|; 

$key = q/displaystylescalaire{u}{u}>0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle \scalaire{u}{u} &gt; 0$">|; 

$key = q/displaystylescalaire{u}{u}ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \scalaire{u}{u} \ge 0$">|; 

$key = q/displaystylescalaire{u}{v}=conjaccent{scalaire{v}{u}};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.97ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\displaystyle \scalaire{u}{v} = \conjaccent{\scalaire{v}{u}}$">|; 

$key = q/displaystylescalaire{u}{v}=forme{phi_u}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\displaystyle \scalaire{u}{v} = \forme{\phi_u}{v}$">|; 

$key = q/displaystylescalaire{u}{v}=scalaire{v}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img25.svg"
 ALT="$\displaystyle \scalaire{u}{v} = \scalaire{v}{u}$">|; 

$key = q/displaystylescalaire{u}{v}=sum_iconjaccent{u}_icdotv_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle \scalaire{u}{v} = \sum_i \conjaccent{u}_i \cdot v_i$">|; 

$key = q/displaystylescalaire{u}{v}=sum_{i,j}conjaccent{u}_icdotg_{ij}cdotv_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img83.svg"
 ALT="$\displaystyle \scalaire{u}{v} = \sum_{i,j} \conjaccent{u}_i \cdot g_{ij} \cdot v_j$">|; 

$key = q/displaystylescalaire{u}{v}=sum_{i,j}u_icdotg_{ij}cdotv_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.83ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img85.svg"
 ALT="$\displaystyle \scalaire{u}{v} = \sum_{i,j} u_i \cdot g_{ij} \cdot v_j$">|; 

$key = q/displaystylescalaire{u}{w}=scalaire{v}{w};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img59.svg"
 ALT="$\displaystyle \scalaire{u}{w} = \scalaire{v}{w}$">|; 

$key = q/displaystylescalaire{x}{y}=conjaccent{x^T}cdotAcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.96ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle \scalaire{x}{y} = \conjaccent{x^T} \cdot A \cdot y$">|; 

$key = q/displaystylescalaire{x}{y}=conjaccent{x^T}cdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.96ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle \scalaire{x}{y} = \conjaccent{x^T} \cdot y$">|; 

$key = q/displaystylescalaire{x}{y}=sum_iconjaccent{x}_icdoty_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle \scalaire{x}{y} = \sum_i \conjaccent{x}_i \cdot y_i$">|; 

$key = q/displaystylescalaire{x}{y}=sum_{i=1}^nconjaccent{x}_iy_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle \scalaire{x}{y} = \sum_{i=1}^n \conjaccent{x}_i y_i$">|; 

$key = q/displaystylescalaire{x}{y}=sum_{i=1}^nx_iy_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\displaystyle \scalaire{x}{y} = \sum_{i = 1}^n x_i y_i$">|; 

$key = q/displaystylescalaire{z}{alphacdotx+betacdoty}=alphacdotscalaire{z}{x}+betacdotscalaire{z}{y}=0+0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\displaystyle \scalaire{z}{\alpha \cdot x + \beta \cdot y} = \alpha \cdot \scalaire{z}{x} + \beta \cdot \scalaire{z}{y} = 0 + 0 = 0$">|; 

$key = q/displaystylesum_{i=1}^na_icdote_i=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle \sum_{i=1}^n a_i \cdot e_i = 0$">|; 

$key = q/displaystyleu=sum_{i=1}^nu_icdote_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle u = \sum_{i = 1}^n u_i \cdot e_i$">|; 

$key = q/displaystylex^orthogonal={zinE:scalaire{x}{z}=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.80ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle x^\orthogonal = \{ z \in E : \scalaire{x}{z} = 0 \}$">|; 

$key = q/displaystyley=sum_{i=1}^nscalaire{e_i}{u}cdote_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\displaystyle y = \sum_{i = 1}^n \scalaire{e_i}{u} \cdot e_i$">|; 

$key = q/e_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img68.svg"
 ALT="$e_i$">|; 

$key = q/e_inee_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.45ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img67.svg"
 ALT="$e_i \ne e_j$">|; 

$key = q/e_k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img72.svg"
 ALT="$e_k$">|; 

$key = q/f_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img116.svg"
 ALT="$f_i$">|; 

$key = q/hat{x},hat{y};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\hat{x},\hat{y}$">|; 

$key = q/hat{x},hat{y}inE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\hat{x},\hat{y} \in E$">|; 

$key = q/i=k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img74.svg"
 ALT="$i = k$">|; 

$key = q/l_i=ligne_iA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.35ex; vertical-align: -0.65ex; " SRC="|."$dir".q|img149.svg"
 ALT="$l_i = \ligne_i A$">|; 

$key = q/mathbb{C}^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\mathbb {C}^n$">|; 

$key = q/mathbb{K}^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\mathbb {K}^n$">|; 

$key = q/mathbb{R}^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\mathbb {R}^n$">|; 

$key = q/mathcal{A}(e_j);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\mathcal{A}(e_j)$">|; 

$key = q/mathcal{A}:EmapstoF;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\mathcal{A} : E \mapsto F$">|; 

$key = q/matrice(setC,n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\matrice(\setC,n,1)$">|; 

$key = q/noyauU={0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img144.svg"
 ALT="$\noyau U = \{0\}$">|; 

$key = q/phi_uinE^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\phi_u \in E^\dual$">|; 

$key = q/scalaire{u}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\scalaire{u}{u}$">|; 

$key = q/scalaire{x}{z}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\scalaire{x}{z} = 0$">|; 

$key = q/scalaire{z}{0}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\scalaire{z}{0} = 0$">|; 

$key = q/scalaire{}{}:EtimesEmapstocorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\scalaire{}{} : E \times E \mapsto \corps$">|; 

$key = q/scalaire{}{}:EtimesEmapstosetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\scalaire{}{} : E \times E \mapsto \setC$">|; 

$key = q/scalaire{}{}:EtimesEmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\scalaire{}{} : E \times E \mapsto \setR$">|; 

$key = q/scalaire{}{};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\scalaire{}{}$">|; 

$key = q/setC^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\setC^n$">|; 

$key = q/setR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\setR^n$">|; 

$key = q/u,v,w,x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img37.svg"
 ALT="$u,v,w,x$">|; 

$key = q/u,v,winE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img13.svg"
 ALT="$u,v,w \in E$">|; 

$key = q/u,vinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$u,v \in E$">|; 

$key = q/u-v=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img64.svg"
 ALT="$u - v = 0$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img19.svg"
 ALT="$u$">|; 

$key = q/u=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img22.svg"
 ALT="$u = 0$">|; 

$key = q/u=v;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img65.svg"
 ALT="$u = v$">|; 

$key = q/u_1,u_2,...,u_minmatrice(corps,n,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img133.svg"
 ALT="$u_1,u_2,...,u_m \in \matrice(\corps,n,1)$">|; 

$key = q/u_i,v_iincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img81.svg"
 ALT="$u_i,v_i \in \corps$">|; 

$key = q/u_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img139.svg"
 ALT="$u_i$">|; 

$key = q/u_iincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img70.svg"
 ALT="$u_i \in \corps$">|; 

$key = q/ucdotv=scalaire{u}{v};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$u \cdot v = \scalaire{u}{v}$">|; 

$key = q/uinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$u \in E$">|; 

$key = q/uinEsetminus{0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img24.svg"
 ALT="$u \in E \setminus \{ 0 \}$">|; 

$key = q/w=u-vinE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img62.svg"
 ALT="$w = u - v \in E$">|; 

$key = q/winE;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img60.svg"
 ALT="$w \in E$">|; 

$key = q/x,y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img125.svg"
 ALT="$x,y$">|; 

$key = q/x,yinV^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.56ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img56.svg"
 ALT="$x,y \in V^\orthogonal$">|; 

$key = q/x,yinmatrice(corps,m,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img136.svg"
 ALT="$x,y \in \matrice(\corps,m,1)$">|; 

$key = q/x,yinsetC^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img97.svg"
 ALT="$x,y \in \setC^n$">|; 

$key = q/x,yinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img92.svg"
 ALT="$x,y \in \setR^n$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img45.svg"
 ALT="$x$">|; 

$key = q/x=(x_1,..,x_n),y=(y_1,...,y_n)insetC^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$x = (x_1,..,x_n) , y = (y_1,...,y_n) \in \setC^n$">|; 

$key = q/x^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img43.svg"
 ALT="$x^\orthogonal$">|; 

$key = q/x_i,y_iinS;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img120.svg"
 ALT="$x_i,y_i \in S$">|; 

$key = q/x_i,y_iincorps;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img134.svg"
 ALT="$x_i,y_i \in \corps$">|; 

$key = q/x_i,y_iinsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img98.svg"
 ALT="$x_i,y_i \in \setC$">|; 

$key = q/x_i,y_iinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img94.svg"
 ALT="$x_i,y_i \in \setR$">|; 

$key = q/xinH;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img42.svg"
 ALT="$x \in H$">|; 

$key = q/xinV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img51.svg"
 ALT="$x \in V$">|; 

$key = q/xinnoyauA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img150.svg"
 ALT="$x \in \noyau A$">|; 

$key = q/xinsetRsubseteqsetC;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.10ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img41.svg"
 ALT="$x \in \setR \subseteq \setC$">|; 

$key = q/xne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img129.svg"
 ALT="$x \ne 0$">|; 

$key = q/zinV;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img53.svg"
 ALT="$z \in V$">|; 

$key = q/zinV^orthogonal;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.20ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img49.svg"
 ALT="$z \in V^\orthogonal$">|; 

$key = q/{Eqts}X=UcdotxY=Ucdoty{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img141.svg"
 ALT="\begin{Eqts}
X = U \cdot x \\\\
Y = U \cdot y
\end{Eqts}">|; 

$key = q/{Eqts}X=sum_{i=1}^mx_icdotu_iY=sum_{i=1}^my_icdotu_i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.36ex; vertical-align: -6.63ex; " SRC="|."$dir".q|img135.svg"
 ALT="\begin{Eqts}
X = \sum_{i = 1}^m x_i \cdot u_i \\\\
Y = \sum_{i = 1}^m y_i \cdot u_i
\end{Eqts}">|; 

$key = q/{Eqts}hat{x}=sum_ix_icdote_ihat{y}=sum_iy_icdote_i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.65ex; vertical-align: -5.26ex; " SRC="|."$dir".q|img119.svg"
 ALT="\begin{Eqts}
\hat{x} = \sum_i x_i \cdot e_i \\\\
\hat{y} = \sum_i y_i \cdot e_i
\end{Eqts}">|; 

$key = q/{Eqts}scalaire{e_k}{u}=sum_{i=1}^nu_icdotscalaire{e_k}{e_i}scalaire{e_k}{u}=sum_{i=1}^nu_icdotindicatrice_{ik}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.36ex; vertical-align: -6.63ex; " SRC="|."$dir".q|img73.svg"
 ALT="\begin{Eqts}
\scalaire{e_k}{u} = \sum_{i = 1}^n u_i \cdot \scalaire{e_k}{e_i} \\\\
\scalaire{e_k}{u} = \sum_{i = 1}^n u_i \cdot \indicatrice_{ik}
\end{Eqts}">|; 

$key = q/{Eqts}scalaire{u}{u}insetRscalaire{u}{u}ge0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img31.svg"
 ALT="\begin{Eqts}
\scalaire{u}{u} \in \setR \\\\
\scalaire{u}{u} \ge 0
\end{Eqts}">|; 

$key = q/{Eqts}u=sum_iu_icdote_iv=sum_iv_icdote_i{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.65ex; vertical-align: -5.26ex; " SRC="|."$dir".q|img80.svg"
 ALT="\begin{Eqts}
u = \sum_i u_i \cdot e_i \\\\
v = \sum_i v_i \cdot e_i
\end{Eqts}">|; 

$key = q/{Eqts}x=(x_1,x_2,...,x_n)y=(y_1,y_2,...,y_n){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img93.svg"
 ALT="\begin{Eqts}
x = (x_1,x_2,...,x_n) \\\\
y = (y_1,y_2,...,y_n)
\end{Eqts}">|; 

$key = q/{Eqts}x=Matrix{{c}x_1x_2vdotsx_nMatrix{qquadqquady=Matrix{{c}y_1y_2vdotsy_nMatrix{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 12.69ex; vertical-align: -5.79ex; " SRC="|."$dir".q|img104.svg"
 ALT="\begin{Eqts}
x =
\begin{Matrix}{c}
x_1 \\\\
x_2 \\\\
\vdots \\\\
x_n
\end{Matrix}\q...
...quad
y =
\begin{Matrix}{c}
y_1 \\\\
y_2 \\\\
\vdots \\\\
y_n
\end{Matrix}\end{Eqts}">|; 

$key = q/{Eqts}x=[x_1x_2...x_m]^Ty=[y_1y_2...y_m]^T{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.66ex; vertical-align: -2.76ex; " SRC="|."$dir".q|img137.svg"
 ALT="\begin{Eqts}
x = [x_1  x_2  ...  x_m]^T \\\\
y = [y_1  y_2  ...  y_m]^T
\end{Eqts}">|; 

$key = q/{Eqts}x=[x_1x_2...x_n]^Ty=[y_1y_2...y_n]^T{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.66ex; vertical-align: -2.76ex; " SRC="|."$dir".q|img127.svg"
 ALT="\begin{Eqts}
x = [x_1  x_2  ...  x_n]^T \\\\
y = [y_1  y_2  ...  y_n]^T
\end{Eqts}">|; 

1;

