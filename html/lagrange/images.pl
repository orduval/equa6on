# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img232.svg"
 ALT="\begin{Eqts}
\lagrangien(\gamma,\lambda) = c^\dual \cdot \gamma + \lambda^\dual ...
...gamma^\dual \cdot (c - A^\dual \cdot \lambda) = b^\dual \cdot \lambda
\end{Eqts}">|; 

$key = q/(canonique_1,...,canonique_m);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img160.svg"
 ALT="$(\canonique_1,...,\canonique_m)$">|; 

$key = q/(gamma,lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img97.svg"
 ALT="$(\gamma,\lambda)$">|; 

$key = q/(gamma,lambda)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img200.svg"
 ALT="$(\gamma,\lambda) \ge 0$">|; 

$key = q/(gamma,lambda)inOmegatimesmathcal{P};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img85.svg"
 ALT="$(\gamma,\lambda) \in \Omega \times \mathcal{P}$">|; 

$key = q/(gamma,lambda)insetR^ntimesmathcal{P};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$(\gamma,\lambda) \in \setR^n \times \mathcal{P}$">|; 

$key = q/(gamma,lambda,mu,nu);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img174.svg"
 ALT="$(\gamma,\lambda,\mu,\nu)$">|; 

$key = q/(lambda,gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img226.svg"
 ALT="$(\lambda,\gamma)$">|; 

$key = q/(m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img210.svg"
 ALT="$(m,n)$">|; 

$key = q/(x,s,t);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img184.svg"
 ALT="$(x,s,t)$">|; 

$key = q/(x,s,t)inGamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img188.svg"
 ALT="$(x,s,t) \in \Gamma$">|; 

$key = q/(x,u)insetR^ntimessetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img151.svg"
 ALT="$(x,u) \in \setR^n \times \setR^m$">|; 

$key = q/(x,y)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img201.svg"
 ALT="$(x,y) \ge 0$">|; 

$key = q/(x,y)insetR^ntimesmathcal{P};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img102.svg"
 ALT="$(x,y) \in \setR^n \times \mathcal{P}$">|; 

$key = q/(x,y,z)insetR^ntimesmathcal{P}timesmathcal{P};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img146.svg"
 ALT="$(x,y,z) \in \setR^n \times \mathcal{P} \times \mathcal{P}$">|; 

$key = q/(x,y,z,u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img172.svg"
 ALT="$(x,y,z,u)$">|; 

$key = q/(y,x)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img220.svg"
 ALT="$(y,x) \ge 0$">|; 

$key = q/(y-lambda)^dualcdotomega(gamma)=-lambda_icdotomega_i(gamma)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img105.svg"
 ALT="$(y - \lambda)^\dual \cdot \omega(\gamma) = -\lambda_i \cdot \omega_i(\gamma) \le 0$">|; 

$key = q/-psi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img127.svg"
 ALT="$-\psi$">|; 

$key = q/-vartheta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img128.svg"
 ALT="$-\vartheta$">|; 

$key = q/-xle0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img191.svg"
 ALT="$-x \le 0$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img209.svg"
 ALT="$A$">|; 

$key = q/AcdotA^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img254.svg"
 ALT="$A \cdot A^\dual$">|; 

$key = q/Acdotxleb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img214.svg"
 ALT="$A \cdot x \le b$">|; 

$key = q/Ainmatrice(setR,m,n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img243.svg"
 ALT="$A \in \matrice(\setR,m,n)$">|; 

$key = q/Gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img183.svg"
 ALT="$\Gamma$">|; 

$key = q/L;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img197.svg"
 ALT="$L$">|; 

$key = q/Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\Omega$">|; 

$key = q/Omegatimesmathcal{P};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.97ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\Omega \times \mathcal{P}$">|; 

$key = q/Phi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img142.svg"
 ALT="$\Phi$">|; 

$key = q/Theta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\Theta$">|; 

$key = q/b^dualcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img241.svg"
 ALT="$b^\dual \cdot y$">|; 

$key = q/binmatrice(setR,m,1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img244.svg"
 ALT="$b \in \matrice(\setR,m,1)$">|; 

$key = q/c-A^dualcdotyle0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img227.svg"
 ALT="$c - A^\dual \cdot y \le 0$">|; 

$key = q/c^dualcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img240.svg"
 ALT="$c^\dual \cdot x$">|; 

$key = q/cinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img212.svg"
 ALT="$c \in \setR^n$">|; 

$key = q/convexe(Omega)=Omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\convexe(\Omega) = \Omega$">|; 

$key = q/displaystyle(u-lambda)^dualcdotvarrho(gamma)=-varrho_i(gamma)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img165.svg"
 ALT="$\displaystyle (u - \lambda)^\dual \cdot \varrho(\gamma) = -\varrho_i(\gamma) \le 0$">|; 

$key = q/displaystyle(u-lambda)^dualcdotvarrho(gamma)=varrho_i(gamma)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img162.svg"
 ALT="$\displaystyle (u - \lambda)^\dual \cdot \varrho(\gamma) = \varrho_i(\gamma) \le 0$">|; 

$key = q/displaystyle(u-lambda)^dualcdotvarrho(gamma)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img159.svg"
 ALT="$\displaystyle (u - \lambda)^\dual \cdot \varrho(\gamma) \le 0$">|; 

$key = q/displaystyle(y-lambda)^dualcdotomega(gamma)=1cdotomega_i(gamma)=omega_i(gamma)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\displaystyle (y - \lambda)^\dual \cdot \omega(\gamma) = 1 \cdot \omega_i(\gamma) = \omega_i(\gamma) \le 0$">|; 

$key = q/displaystyle(y-lambda)^dualcdotomega(gamma)=lambda_icdotomega_i(gamma)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img108.svg"
 ALT="$\displaystyle (y - \lambda)^\dual \cdot \omega(\gamma) = \lambda_i \cdot \omega_i(\gamma) \le 0$">|; 

$key = q/displaystyle-z^dualcdotxle0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.18ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img194.svg"
 ALT="$\displaystyle -z^\dual \cdot x \le 0$">|; 

$key = q/displaystyleA^dualcdotygec;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.32ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img228.svg"
 ALT="$\displaystyle A^\dual \cdot y \ge c$">|; 

$key = q/displaystyleAcdotA^dualcdotlambda=b;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img253.svg"
 ALT="$\displaystyle A \cdot A^\dual \cdot \lambda = b$">|; 

$key = q/displaystyleL(gamma,y,z)leL(gamma,lambda,mu)leL(x,lambda,mu);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img193.svg"
 ALT="$\displaystyle L(\gamma,y,z) \le L(\gamma,\lambda,\mu) \le L(x,\lambda,\mu)$">|; 

$key = q/displaystyleL(x,lambda,mu)leL(gamma,lambda,mu)leL(gamma,y,z);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img204.svg"
 ALT="$\displaystyle L(x,\lambda,\mu) \le L(\gamma,\lambda,\mu) \le L(\gamma,y,z)$">|; 

$key = q/displaystyleL(x,y,z)=psi(x)+y^dualcdotomega(x)+z^dualcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img203.svg"
 ALT="$\displaystyle L(x,y,z) = \psi(x) + y^\dual \cdot \omega(x) + z^\dual \cdot x$">|; 

$key = q/displaystyleL(x,y,z)=varphi(x)+(y-z)^dualcdotvarrho(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img148.svg"
 ALT="$\displaystyle L(x,y,z) = \varphi(x) + (y - z)^\dual \cdot \varrho(x)$">|; 

$key = q/displaystyleL(x,y,z)=varphi(x)+y^dualcdotomega(x)-z^dualcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img192.svg"
 ALT="$\displaystyle L(x,y,z) = \varphi(x) + y^\dual \cdot \omega(x) - z^\dual \cdot x$">|; 

$key = q/displaystyleL(x,y,z)=varphi(x)+y^dualcdotomega(x)-z^dualcdotxlevarphi(x)+y^dualcdotomega(x)=L(x,y,0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img196.svg"
 ALT="$\displaystyle L(x,y,z) = \varphi(x) + y^\dual \cdot \omega(x) - z^\dual \cdot x \le \varphi(x) + y^\dual \cdot \omega(x) = L(x,y,0)$">|; 

$key = q/displaystyleL(x,y,z)=varphi(x)+y^dualcdotvarrho(x)+z^dualcdot(-varrho(x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img145.svg"
 ALT="$\displaystyle L(x,y,z) = \varphi(x) + y^\dual \cdot \varrho(x) + z^\dual \cdot (-\varrho(x))$">|; 

$key = q/displaystyleL(x,y,z)geL(x,y,0);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img206.svg"
 ALT="$\displaystyle L(x,y,z) \ge L(x,y,0)$">|; 

$key = q/displaystyleOmega={xinsetR^n:-vartheta(x)le0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\displaystyle \Omega = \{ x \in \setR^n : -\vartheta(x) \le 0 \}$">|; 

$key = q/displaystyleOmega={xinsetR^n:omega(x)le0,vartheta(x)ge0,varrho(x)=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img170.svg"
 ALT="$\displaystyle \Omega = \{ x \in \setR^n : \omega(x) \le 0,  \vartheta(x) \ge 0,  \varrho(x) = 0 \}$">|; 

$key = q/displaystyleOmega={xinsetR^n:omega(x)le0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle \Omega = \{ x \in \setR^n : \omega(x) \le 0 \}$">|; 

$key = q/displaystyleOmega={yinsetR^m:A^dualcdotygec,yge0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img230.svg"
 ALT="$\displaystyle \Omega = \{ y \in \setR^m : A^\dual \cdot y \ge c,  y \ge 0 \}$">|; 

$key = q/displaystyleOmega^+={xincorps^n:omega(x)le0,xge0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.66ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img189.svg"
 ALT="$\displaystyle \Omega^+ = \{ x \in \corps^n : \omega(x) \le 0,  x \ge 0 \}$">|; 

$key = q/displaystylePhi={xinmatrice(setR,n,1):Acdotx=b};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img245.svg"
 ALT="$\displaystyle \Phi = \{ x \in \matrice(\setR,n,1) : A \cdot x = b \}$">|; 

$key = q/displaystylePhi={xinsetR^n:varrho(x)=0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img140.svg"
 ALT="$\displaystyle \Phi = \{ x \in \setR^n : \varrho(x) = 0 \}$">|; 

$key = q/displaystylePhi={xinsetR^n:varrho(x)le0,varrho(x)ge0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img144.svg"
 ALT="$\displaystyle \Phi = \{ x \in \setR^n : \varrho(x) \le 0,  \varrho(x) \ge 0 \}$">|; 

$key = q/displaystyleTheta={xincorps^n:vartheta(x)ge0,xge0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img202.svg"
 ALT="$\displaystyle \Theta = \{ x \in \corps^n : \vartheta(x) \ge 0,  x \ge 0 \}$">|; 

$key = q/displaystyleTheta={xinsetR^n:Acdotxleb,xge0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img211.svg"
 ALT="$\displaystyle \Theta = \{ x \in \setR^n : A \cdot x \le b,  x \ge 0 \}$">|; 

$key = q/displaystyleTheta={xinsetR^n:vartheta(x)ge0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle \Theta = \{ x \in \setR^n : \vartheta(x) \ge 0 \}$">|; 

$key = q/displaystyleV=max_{xinTheta}(c^dualcdotx)=min_{yinOmega}(b^dualcdoty);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.90ex; vertical-align: -2.08ex; " SRC="|."$dir".q|img236.svg"
 ALT="$\displaystyle V = \max_{x \in \Theta} (c^\dual \cdot x) = \min_{y \in \Omega} (b^\dual \cdot y)$">|; 

$key = q/displaystyleargmin_{xinsetR^n}psi(x)subseteqOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \arg\min_{x \in \setR^n} \psi(x) \subseteq \Omega$">|; 

$key = q/displaystyleb-Acdotxge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img215.svg"
 ALT="$\displaystyle b - A \cdot x \ge 0$">|; 

$key = q/displaystylec^dualcdotgamma=b^dualcdotlambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.32ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img234.svg"
 ALT="$\displaystyle c^\dual \cdot \gamma = b^\dual \cdot \lambda$">|; 

$key = q/displaystylec^dualcdotxleVleb^dualcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.32ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img237.svg"
 ALT="$\displaystyle c^\dual \cdot x \le V \le b^\dual \cdot y$">|; 

$key = q/displaystylechiinargmin_{xinsetR^nsetminusOmega}psi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.09ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \chi \in \arg\min_{x \in \setR^n \setminus \Omega} \psi(x)$">|; 

$key = q/displaystylederiveepartielle{lagrangien}{x}(gamma,lambda)=partialvarphi(gamma)+lambda^dualcdotpartialomega(gamma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.95ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\displaystyle \deriveepartielle{\lagrangien}{x}(\gamma,\lambda) = \partial \varphi(\gamma) + \lambda^\dual \cdot \partial \omega(\gamma) = 0$">|; 

$key = q/displaystylegamma=A^dualcdotleft(AcdotA^dualright)^{-1}cdotb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img256.svg"
 ALT="$\displaystyle \gamma = A^\dual \cdot \left( A \cdot A^\dual \right)^{-1} \cdot b$">|; 

$key = q/displaystylegamma=argmin_{xinPhi}left(unsur{2}cdotx^dualcdotxright);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.75ex; vertical-align: -2.30ex; " SRC="|."$dir".q|img249.svg"
 ALT="$\displaystyle \gamma = \arg\min_{x \in \Phi} \left( \unsur{2} \cdot x^\dual \cdot x \right)$">|; 

$key = q/displaystylegammainargmax_{xinTheta}big[c^dualcdotxbig];MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.93ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img213.svg"
 ALT="$\displaystyle \gamma \in \arg\max_{x \in \Theta} \big[ c^\dual \cdot x \big]$">|; 

$key = q/displaystylegammainargmax_{xinTheta}psi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img124.svg"
 ALT="$\displaystyle \gamma \in \arg\max_{x \in \Theta} \psi(x)$">|; 

$key = q/displaystylegammainargmin_{xinOmega}(-psi(x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img125.svg"
 ALT="$\displaystyle \gamma \in \arg\min_{x \in \Omega} (-\psi(x))$">|; 

$key = q/displaystylegammainargmin_{xinOmega}varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle \gamma \in \arg\min_{x \in \Omega} \varphi(x)$">|; 

$key = q/displaystylegammainargmin_{xinPhi}varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img143.svg"
 ALT="$\displaystyle \gamma \in \arg\min_{x \in \Phi} \varphi(x)$">|; 

$key = q/displaystylegammainargmin_{xinsetR^n}lagrangien(x,lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle \gamma \in \arg\min_{x \in \setR^n} \lagrangien(x,\lambda)$">|; 

$key = q/displaystylegammainargmin_{xinsetR^n}psi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img54.svg"
 ALT="$\displaystyle \gamma \in \arg\min_{x \in \setR^n} \psi(x)$">|; 

$key = q/displaystylelagrangien(gamma,lambda)=lagrangien^dual(lambda,gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img233.svg"
 ALT="$\displaystyle \lagrangien(\gamma,\lambda) = \lagrangien^\dual(\lambda,\gamma)$">|; 

$key = q/displaystylelagrangien(gamma,lambda)=min_{xinsetR^n}lagrangien(x,lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img70.svg"
 ALT="$\displaystyle \lagrangien(\gamma,\lambda) = \min_{x \in \setR^n} \lagrangien(x,\lambda)$">|; 

$key = q/displaystylelagrangien(gamma,lambda)=varphi(gamma)+lambda^dualcdotomega(gamma)=varphi(gamma)+0=varphi(gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img74.svg"
 ALT="$\displaystyle \lagrangien(\gamma,\lambda) = \varphi(\gamma) + \lambda^\dual \cdot \omega(\gamma) = \varphi(\gamma) + 0 = \varphi(\gamma)$">|; 

$key = q/displaystylelagrangien(gamma,lambda)lelagrangien(x,lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\displaystyle \lagrangien(\gamma,\lambda) \le \lagrangien(x,\lambda)$">|; 

$key = q/displaystylelagrangien(gamma,u)-lagrangien(gamma,lambda)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img157.svg"
 ALT="$\displaystyle \lagrangien(\gamma,u) - \lagrangien(\gamma,\lambda) \le 0$">|; 

$key = q/displaystylelagrangien(gamma,u)lelagrangien(gamma,lambda)lelagrangien(x,lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img156.svg"
 ALT="$\displaystyle \lagrangien(\gamma,u) \le \lagrangien(\gamma,\lambda) \le \lagrangien(x,\lambda)$">|; 

$key = q/displaystylelagrangien(gamma,y)-lagrangien(gamma,lambda)=(y-lambda)^dualcdotomega(gamma)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\displaystyle \lagrangien(\gamma,y) - \lagrangien(\gamma,\lambda) = (y - \lambda)^\dual \cdot \omega(\gamma) \le 0$">|; 

$key = q/displaystylelagrangien(gamma,y)=varphi(gamma)+y^dualcdotomega(gamma)levarphi(gamma)=lagrangien(gamma,lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle \lagrangien(\gamma,y) = \varphi(\gamma) + y^\dual \cdot \omega(\gamma) \le \varphi(\gamma) = \lagrangien(\gamma,\lambda)$">|; 

$key = q/displaystylelagrangien(gamma,y)lelagrangien(gamma,lambda)lelagrangien(x,lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img98.svg"
 ALT="$\displaystyle \lagrangien(\gamma,y) \le \lagrangien(\gamma,\lambda) \le \lagrangien(x,\lambda)$">|; 

$key = q/displaystylelagrangien(gamma,y,z,u)lelagrangien(gamma,lambda,mu,nu)lelagrangien(x,lambda,mu,nu);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img175.svg"
 ALT="$\displaystyle \lagrangien(\gamma,y,z,u) \le \lagrangien(\gamma,\lambda,\mu,\nu) \le \lagrangien(x,\lambda,\mu,\nu)$">|; 

$key = q/displaystylelagrangien(x,lambda)=varphi(x)+lambda^dualcdotomega(x)levarphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img115.svg"
 ALT="$\displaystyle \lagrangien(x,\lambda) = \varphi(x) + \lambda^\dual \cdot \omega(x) \le \varphi(x)$">|; 

$key = q/displaystylelagrangien(x,lambda)lelagrangien(gamma,lambda)lelagrangien(gamma,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img208.svg"
 ALT="$\displaystyle \lagrangien(x,\lambda) \le \lagrangien(\gamma,\lambda) \le \lagrangien(\gamma,y)$">|; 

$key = q/displaystylelagrangien(x,lambda,mu,nu)lelagrangien(gamma,lambda,mu,nu)lelagrangien(gamma,y,z,u);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img178.svg"
 ALT="$\displaystyle \lagrangien(x,\lambda,\mu,\nu) \le \lagrangien(\gamma,\lambda,\mu,\nu) \le \lagrangien(\gamma,y,z,u)$">|; 

$key = q/displaystylelagrangien(x,u)=unsur{2}cdotx^dualcdotx+u^dualcdot(b-Acdotx);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.84ex; vertical-align: -1.69ex; " SRC="|."$dir".q|img250.svg"
 ALT="$\displaystyle \lagrangien(x,u) = \unsur{2} \cdot x^\dual \cdot x + u^\dual \cdot (b - A \cdot x)$">|; 

$key = q/displaystylelagrangien(x,u)=varphi(x)+u^dualcdotvarrho(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img150.svg"
 ALT="$\displaystyle \lagrangien(x,u) = \varphi(x) + u^\dual \cdot \varrho(x)$">|; 

$key = q/displaystylelagrangien(x,y)=L(x,y,0)=psi(x)+y^dualcdotvartheta(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img207.svg"
 ALT="$\displaystyle \lagrangien(x,y) = L(x,y,0) = \psi(x) + y^\dual \cdot \vartheta(x)$">|; 

$key = q/displaystylelagrangien(x,y)=L(x,y,0)=varphi(x)+y^dualcdotomega(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img199.svg"
 ALT="$\displaystyle \lagrangien(x,y) = L(x,y,0) = \varphi(x) + y^\dual \cdot \omega(x)$">|; 

$key = q/displaystylelagrangien(x,y)=varphi(x)+sum_{i=1}^my_icdotomega_i(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle \lagrangien(x,y) = \varphi(x) + \sum_{i = 1}^m y_i \cdot \omega_i(x)$">|; 

$key = q/displaystylelagrangien(x,y)=varphi(x)+y^dualcdotomega(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img67.svg"
 ALT="$\displaystyle \lagrangien(x,y) = \varphi(x) + y^\dual \cdot \omega(x)$">|; 

$key = q/displaystylelagrangien(x,y,z,u)=psi(x)-y^dualcdotomega(x)+z^dualcdotvartheta(x)+u^dualcdotvarrho(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img177.svg"
 ALT="$\displaystyle \lagrangien(x,y,z,u) = \psi(x) - y^\dual \cdot \omega(x) + z^\dual \cdot \vartheta(x) + u^\dual \cdot \varrho(x)$">|; 

$key = q/displaystylelagrangien(x,y,z,u)=varphi(x)+y^dualcdotomega(x)-z^dualcdotvartheta(x)+u^dualcdotvarrho(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img171.svg"
 ALT="$\displaystyle \lagrangien(x,y,z,u) = \varphi(x) + y^\dual \cdot \omega(x) - z^\dual \cdot \vartheta(x) + u^\dual \cdot \varrho(x)$">|; 

$key = q/displaystylelagrangien^dual(lambda,x)lelagrangien^dual(lambda,gamma)lelagrangien^dual(y,gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img225.svg"
 ALT="$\displaystyle \lagrangien^\dual(\lambda,x) \le \lagrangien^\dual(\lambda,\gamma) \le \lagrangien^\dual(y,\gamma)$">|; 

$key = q/displaystylelagrangien^dual(y,x)=lagrangien(x,y)=b^dualcdoty+x^dualcdot(c-A^dualcdoty);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img219.svg"
 ALT="$\displaystyle \lagrangien^\dual(y,x) = \lagrangien(x,y) = b^\dual \cdot y + x^\dual \cdot (c - A^\dual \cdot y)$">|; 

$key = q/displaystylelagrangien_{max}(x,lambda)lelagrangien_{max}(gamma,lambda)lelagrangien_{max}(gamma,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\displaystyle \lagrangien_{\max}(x,\lambda) \le \lagrangien_{\max}(\gamma,\lambda) \le \lagrangien_{\max}(\gamma,y)$">|; 

$key = q/displaystylelagrangien_{max}(x,y)=-lagrangien_{min}(x,y)=psi(x)+y^dualcdotvartheta(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img131.svg"
 ALT="$\displaystyle \lagrangien_{\max}(x,y) = - \lagrangien_{\min}(x,y) = \psi(x) + y^\dual \cdot \vartheta(x)$">|; 

$key = q/displaystylelagrangien_{min}(gamma,y)lelagrangien_{min}(gamma,lambda)lelagrangien_{min}(x,lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\displaystyle \lagrangien_{\min}(\gamma,y) \le \lagrangien_{\min}(\gamma,\lambda) \le \lagrangien_{\min}(x,\lambda)$">|; 

$key = q/displaystylelagrangien_{min}(x,y)=Big[-psi(x)Big]+y^dualcdotBig[-vartheta(x)Big]=-psi(x)-y^dualcdotvartheta(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.36ex; vertical-align: -1.61ex; " SRC="|."$dir".q|img129.svg"
 ALT="$\displaystyle \lagrangien_{\min}(x,y) = \Big[-\psi(x)\Big] + y^\dual \cdot \Big[-\vartheta(x)\Big] = -\psi(x) - y^\dual \cdot \vartheta(x)$">|; 

$key = q/displaystylelambda=left(AcdotA^dualright)^{-1}cdotb;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img255.svg"
 ALT="$\displaystyle \lambda = \left( A \cdot A^\dual \right)^{-1} \cdot b$">|; 

$key = q/displaystylelambda^dualcdotomega(gamma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img73.svg"
 ALT="$\displaystyle \lambda^\dual \cdot \omega(\gamma) = 0$">|; 

$key = q/displaystylelambda_i^dualcdotomega_i(gamma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle \lambda_i^\dual \cdot \omega_i(\gamma) = 0$">|; 

$key = q/displaystylelambda_icdotomega_i(gamma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle \lambda_i \cdot \omega_i(\gamma) = 0$">|; 

$key = q/displaystylelambda_icdotomega_i(gamma)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle \lambda_i \cdot \omega_i(\gamma) \ge 0$">|; 

$key = q/displaystylelambdainargmin_{yinOmega}(b^dualcdoty);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.90ex; vertical-align: -2.08ex; " SRC="|."$dir".q|img229.svg"
 ALT="$\displaystyle \lambda \in \arg\min_{y \in \Omega} (b^\dual \cdot y)$">|; 

$key = q/displaystylemathcal{P}={yinsetR^m:yge0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle \mathcal{P} = \{ y \in \setR^m : y \ge 0 \}$">|; 

$key = q/displaystylemin_{xinOmega}psi(x)=min_{xinOmega}varphi(x)=varphi(gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.70ex; vertical-align: -1.87ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle \min_{x \in \Omega} \psi(x) = \min_{x \in \Omega} \varphi(x) = \varphi(\gamma)$">|; 

$key = q/displaystylemin_{xinsetR^n}psi(x)=min{psi(chi),varphi(gamma)}=varphi(gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \min_{x \in \setR^n} \psi(x) = \min \{ \psi(\chi) , \varphi(\gamma) \} = \varphi(\gamma)$">|; 

$key = q/displaystyleomega(scdotu+tcdotv)lescdotomega(u)+tcdotomega(v)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$\displaystyle \omega(s \cdot u + t \cdot v) \le s \cdot \omega(u) + t \cdot \omega(v) \le 0$">|; 

$key = q/displaystyleomega(x)=(omega_1(x),omega_2(x),...,omega_m(x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \omega(x) = (\omega_1(x), \omega_2(x), ..., \omega_m(x))$">|; 

$key = q/displaystyleomega_i(x)=lim_{ntoinfty}omega_i(u_n)lelimsup_{ntoinfty}omega_i(u_n)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 4.00ex; vertical-align: -2.17ex; " SRC="|."$dir".q|img34.svg"
 ALT="$\displaystyle \omega_i(x) = \lim_{n \to \infty} \omega_i(u_n) \le \limsup_{n \to \infty} \omega_i(u_n) \le 0$">|; 

$key = q/displaystyleomega_i(x)=lim_{ntoinfty}omega_i(v_n)geliminf_{ntoinfty}omega_i(v_n)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.55ex; vertical-align: -1.72ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\displaystyle \omega_i(x) = \lim_{n \to \infty} \omega_i(v_n) \ge \liminf_{n \to \infty} \omega_i(v_n) \ge 0$">|; 

$key = q/displaystylepartialvarphi(gamma)+sum_{i=1}^mlambda_icdotpartialomega_i(gamma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \partial \varphi(\gamma) + \sum_{i = 1}^m \lambda_i \cdot \partial \omega_i(\gamma) = 0$">|; 

$key = q/displaystylepartialvarphi(xi)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle \partial \varphi(\xi) = 0$">|; 

$key = q/displaystylepsi(x)=varphi(x)+p(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img45.svg"
 ALT="$\displaystyle \psi(x) = \varphi(x) + p(x)$">|; 

$key = q/displaystylesum_ilambda_icdotomega_i(gamma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \sum_i \lambda_i \cdot \omega_i(\gamma) = 0$">|; 

$key = q/displaystylesum_ilambda_icdotomega_i(gamma)lelambda_kcdotomega_k(gamma)strictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.53ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle \sum_i \lambda_i \cdot \omega_i(\gamma) \le \lambda_k \cdot \omega_k(\gamma) \strictinferieur 0$">|; 

$key = q/displaystylevarphi(gamma)=lagrangien(gamma,lambda)lelagrangien(x,lambda)=varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img169.svg"
 ALT="$\displaystyle \varphi(\gamma) = \lagrangien(\gamma,\lambda) \le \lagrangien(x,\lambda) = \varphi(x)$">|; 

$key = q/displaystylevarphi(gamma)=lagrangien(gamma,lambda)lelagrangien(x,lambda)levarphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\displaystyle \varphi(\gamma) = \lagrangien(\gamma,\lambda) \le \lagrangien(x,\lambda) \le \varphi(x)$">|; 

$key = q/displaystylevarrho(x)=(varrho_1(x),varrho_2(x),...,varrho_m(x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img139.svg"
 ALT="$\displaystyle \varrho(x) = (\varrho_1(x), \varrho_2(x), ..., \varrho_m(x))$">|; 

$key = q/displaystylevartheta(x)=(vartheta_1(x),vartheta_2(x),...,vartheta_m(x));MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\displaystyle \vartheta(x) = (\vartheta_1(x), \vartheta_2(x), ..., \vartheta_m(x))$">|; 

$key = q/displaystylex=(x_1,...,x_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle x = (x_1,...,x_n)$">|; 

$key = q/displaystylexiinargmin_{xinsetR^n}varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 3.71ex; vertical-align: -1.88ex; " SRC="|."$dir".q|img21.svg"
 ALT="$\displaystyle \xi \in \arg\min_{x \in \setR^n} \varphi(x)$">|; 

$key = q/displaystylez^dualcdotxge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.18ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img205.svg"
 ALT="$\displaystyle z^\dual \cdot x \ge 0$">|; 

$key = q/distance(x,Omega)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$\distance(x,\Omega) = 0$">|; 

$key = q/distance(x,setR^nsetminusOmega)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$\distance(x,\setR^n \setminus \Omega) = 0$">|; 

$key = q/gamma;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img55.svg"
 ALT="$\gamma$">|; 

$key = q/gamma=A^dualcdotlambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img252.svg"
 ALT="$\gamma = A^\dual \cdot \lambda$">|; 

$key = q/gammainOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\gamma \in \Omega$">|; 

$key = q/gammainPhi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img141.svg"
 ALT="$\gamma \in \Phi$">|; 

$key = q/gammainTheta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\gamma \in \Theta$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img40.svg"
 ALT="$i$">|; 

$key = q/iin{1,...,m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img35.svg"
 ALT="$i \in \{1,...,m\}$">|; 

$key = q/iin{1,2,...,m};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img83.svg"
 ALT="$i \in \{1,2,...,m\}$">|; 

$key = q/k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img79.svg"
 ALT="$k$">|; 

$key = q/lagrangien(gamma,lambda)=varphi(gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\lagrangien(\gamma,\lambda) = \varphi(\gamma)$">|; 

$key = q/lagrangien(x,lambda);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\lagrangien(x,\lambda)$">|; 

$key = q/lagrangien;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img66.svg"
 ALT="$\lagrangien$">|; 

$key = q/lagrangien^dual;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img224.svg"
 ALT="$\lagrangien^\dual$">|; 

$key = q/lagrangien_{max};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img135.svg"
 ALT="$\lagrangien_{\max}$">|; 

$key = q/lagrangien_{min};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img133.svg"
 ALT="$\lagrangien_{\min}$">|; 

$key = q/lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img95.svg"
 ALT="$\lambda$">|; 

$key = q/lambda^dualcdotomega(gamma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img89.svg"
 ALT="$\lambda^\dual \cdot \omega(\gamma) = 0$">|; 

$key = q/lambda^dualcdotomega(x)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img114.svg"
 ALT="$\lambda^\dual \cdot \omega(x) \le 0$">|; 

$key = q/lambda_icdotomega_i(gamma)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\lambda_i \cdot \omega_i(\gamma) \le 0$">|; 

$key = q/lambda_ige0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\lambda_i \ge 0$">|; 

$key = q/lambda_kcdotomega_k(gamma)strictinferieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img80.svg"
 ALT="$\lambda_k \cdot \omega_k(\gamma) \strictinferieur 0$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img8.svg"
 ALT="$m$">|; 

$key = q/max-min;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.92ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img242.svg"
 ALT="$\max - \min$">|; 

$key = q/mu=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img198.svg"
 ALT="$\mu = 0$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img14.svg"
 ALT="$n$">|; 

$key = q/norme{x}=sqrt{x^dualcdotx};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.82ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img246.svg"
 ALT="$\norme{x} = \sqrt{x^\dual \cdot x}$">|; 

$key = q/norme{x}insetR^+;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.54ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img248.svg"
 ALT="$\norme{x} \in \setR^+$">|; 

$key = q/norme{x}mapstonorme{x}^2slash2;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.89ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img247.svg"
 ALT="$\norme{x} \mapsto \norme{x}^2 /2 $">|; 

$key = q/omega(gamma)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img92.svg"
 ALT="$\omega(\gamma) \le 0$">|; 

$key = q/omega(x)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\omega(x) \le 0$">|; 

$key = q/omega:setR^nmapstosetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$\omega : \setR^n \mapsto \setR^m$">|; 

$key = q/omega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\omega$">|; 

$key = q/omega_1,omega_2,...,omega_m:setR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\omega_1,\omega_2,...,\omega_m : \setR^n \mapsto \setR$">|; 

$key = q/omega_i(gamma)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\omega_i(\gamma) \le 0$">|; 

$key = q/omega_i(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$\omega_i(x) = 0$">|; 

$key = q/omega_i(x)strictsuperieur0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$\omega_i(x) \strictsuperieur 0$">|; 

$key = q/omega_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\omega_i$">|; 

$key = q/p(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img46.svg"
 ALT="$p(x)$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img42.svg"
 ALT="$p$">|; 

$key = q/partial_ulagrangien(x,u)=varrho(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img154.svg"
 ALT="$\partial_u \lagrangien(x,u) = \varrho(x)$">|; 

$key = q/partial_xlagrangien(gamma,lambda)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img87.svg"
 ALT="$\partial_x \lagrangien(\gamma,\lambda) = 0$">|; 

$key = q/partialpsi(gamma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img56.svg"
 ALT="$\partial \psi(\gamma) = 0$">|; 

$key = q/psi(chi)gevarphi(gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img49.svg"
 ALT="$\psi(\chi) \ge \varphi(\gamma)$">|; 

$key = q/psi(chi)strictsuperieurvarphi(gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img51.svg"
 ALT="$\psi(\chi) \strictsuperieur \varphi(\gamma)$">|; 

$key = q/psi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img187.svg"
 ALT="$\psi(x)$">|; 

$key = q/psi:setR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\psi : \setR^n \mapsto \setR$">|; 

$key = q/psi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\psi$">|; 

$key = q/s+t=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img25.svg"
 ALT="$s + t = 1$">|; 

$key = q/s,tge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$s,t \ge 0$">|; 

$key = q/s,tinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img23.svg"
 ALT="$s,t \in \setR$">|; 

$key = q/s=-omega(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img180.svg"
 ALT="$s = - \omega(x)$">|; 

$key = q/setR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\setR^m$">|; 

$key = q/setR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img152.svg"
 ALT="$\setR^n$">|; 

$key = q/setR^ntimesmathcal{P};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img100.svg"
 ALT="$\setR^n \times \mathcal{P}$">|; 

$key = q/sge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img181.svg"
 ALT="$s \ge 0$">|; 

$key = q/t=vartheta(x)ge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img182.svg"
 ALT="$t = \vartheta(x) \ge 0$">|; 

$key = q/u,vinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img22.svg"
 ALT="$u,v \in \Omega$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img163.svg"
 ALT="$u$">|; 

$key = q/u=lambda+canonique_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img161.svg"
 ALT="$u = \lambda + \canonique_i$">|; 

$key = q/u=lambda-canonique_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.15ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img164.svg"
 ALT="$u = \lambda - \canonique_i$">|; 

$key = q/u=y-z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img149.svg"
 ALT="$u = y - z$">|; 

$key = q/u_ninOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img32.svg"
 ALT="$u_n \in \Omega$">|; 

$key = q/uinsetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img158.svg"
 ALT="$u \in \setR^m$">|; 

$key = q/v_ninsetR^nsetminusOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img38.svg"
 ALT="$v_n \in \setR^n \setminus \Omega$">|; 

$key = q/varphi(gamma);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img52.svg"
 ALT="$\varphi(\gamma)$">|; 

$key = q/varphi(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img186.svg"
 ALT="$\varphi(x)$">|; 

$key = q/varphi:setR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$\varphi : \setR^n \mapsto \setR$">|; 

$key = q/varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\varphi$">|; 

$key = q/varrho(gamma)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img166.svg"
 ALT="$\varrho(\gamma) = 0$">|; 

$key = q/varrho(gamma)=varrho(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img168.svg"
 ALT="$\varrho(\gamma) = \varrho(x) = 0$">|; 

$key = q/varrho(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img153.svg"
 ALT="$\varrho(x) = 0$">|; 

$key = q/varrho:setR^nmapstosetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img138.svg"
 ALT="$\varrho : \setR^n \mapsto \setR^m$">|; 

$key = q/varrho_1,varrho_2,...,varrho_m:setR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img137.svg"
 ALT="$\varrho_1,\varrho_2,...,\varrho_m : \setR^n \mapsto \setR$">|; 

$key = q/vartheta:setR^nmapstosetR^m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.80ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\vartheta : \setR^n \mapsto \setR^m$">|; 

$key = q/vartheta_1,vartheta_2,...,vartheta_m:setR^nmapstosetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img117.svg"
 ALT="$\vartheta_1,\vartheta_2,...,\vartheta_m : \setR^n \mapsto \setR$">|; 

$key = q/w=scdotu+tcdotvinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.95ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img27.svg"
 ALT="$w = s \cdot u + t \cdot v \in \Omega$">|; 

$key = q/x,yge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img223.svg"
 ALT="$x,y \ge 0$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img33.svg"
 ALT="$x$">|; 

$key = q/xge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img190.svg"
 ALT="$x \ge 0$">|; 

$key = q/xi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\xi$">|; 

$key = q/xinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img7.svg"
 ALT="$x \in \Omega$">|; 

$key = q/xinPhi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img167.svg"
 ALT="$x \in \Phi$">|; 

$key = q/xinTheta;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.87ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img238.svg"
 ALT="$x \in \Theta$">|; 

$key = q/xinadhOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img30.svg"
 ALT="$x \in \adh \Omega$">|; 

$key = q/xinfrontiereOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img29.svg"
 ALT="$x \in \frontiere \Omega$">|; 

$key = q/xinsetR^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img4.svg"
 ALT="$x \in \setR^n$">|; 

$key = q/xmapstoc^dualcdotx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.75ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img235.svg"
 ALT="$x \mapsto c^\dual \cdot x$">|; 

$key = q/xnotinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img58.svg"
 ALT="$x \notin \Omega$">|; 

$key = q/xnotininterieurOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.02ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img36.svg"
 ALT="$x \notin \interieur \Omega$">|; 

$key = q/y,zge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img173.svg"
 ALT="$y,z \ge 0$">|; 

$key = q/y-z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.99ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img147.svg"
 ALT="$y - z$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img96.svg"
 ALT="$y$">|; 

$key = q/y=(y_1,...,y_m)inmathcal{P};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img65.svg"
 ALT="$y = (y_1,...,y_m) \in \mathcal{P}$">|; 

$key = q/y=lambda;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img68.svg"
 ALT="$y = \lambda$">|; 

$key = q/y^dualcdotomega(gamma)le0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img93.svg"
 ALT="$y^\dual \cdot \omega(\gamma) \le 0$">|; 

$key = q/y_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img62.svg"
 ALT="$y_i$">|; 

$key = q/y_icdotomega_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.67ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img59.svg"
 ALT="$y_i \cdot \omega_i$">|; 

$key = q/y_ige0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img61.svg"
 ALT="$y_i \ge 0$">|; 

$key = q/y_iinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.24ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img60.svg"
 ALT="$y_i \in \setR$">|; 

$key = q/yge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img91.svg"
 ALT="$y \ge 0$">|; 

$key = q/yinOmega;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img239.svg"
 ALT="$y \in \Omega$">|; 

$key = q/yinmathcal{P};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img86.svg"
 ALT="$y \in \mathcal{P}$">|; 

$key = q/ymapstob^dualcdoty;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img221.svg"
 ALT="$y \mapsto b^\dual \cdot y$">|; 

$key = q/z;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img195.svg"
 ALT="$z$">|; 

$key = q/{Eqts}-partialpsi(gamma)-sum_{i=1}^mlambda_icdotpartialvartheta_i(gamma)=0-lambda_i^dualcdotvartheta_i(gamma)=0-vartheta(gamma)le0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 20.59ex; vertical-align: -9.75ex; " SRC="|."$dir".q|img134.svg"
 ALT="\begin{Eqts}
- \partial \psi(\gamma) - \sum_{i = 1}^m \lambda_i \cdot \partial \...
...^\dual \cdot \vartheta_i(\gamma) = 0 \ \\\\
- \vartheta(\gamma) \le 0
\end{Eqts}">|; 

$key = q/{Eqts}?cases{c-A^dualcdotyle0c-A^dualcdotyge0cases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img222.svg"
 ALT="\begin{Eqts}
?
\begin{cases}
c - A^\dual \cdot y \le 0 \\\\
c - A^\dual \cdot y \ge 0
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}lambda^dualcdot(b-Acdotgamma)=0gamma^dualcdot(c-A^dualcdotlambda)=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img231.svg"
 ALT="\begin{Eqts}
\lambda^\dual \cdot (b - A \cdot \gamma) = 0 \ \\\\
\gamma^\dual \cdot (c - A^\dual \cdot \lambda) = 0
\end{Eqts}">|; 

$key = q/{Eqts}partial_xlagrangien(gamma,lambda)=0partial_ulagrangien(gamma,lambda)=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img155.svg"
 ALT="\begin{Eqts}
\partial_x \lagrangien(\gamma,\lambda) = 0 \ \\\\
\partial_u \lagrangien(\gamma,\lambda) = 0
\end{Eqts}">|; 

$key = q/{Eqts}partial_xlagrangien(gamma,lambda)=gamma-A^dualcdotlambda=0partial_ulagrangien(gamma,lambda)=b-Acdotgamma=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img251.svg"
 ALT="\begin{Eqts}
\partial_x \lagrangien(\gamma,\lambda) = \gamma - A^\dual \cdot \la...
... \\\\
\partial_u \lagrangien(\gamma,\lambda) = b - A \cdot \gamma = 0
\end{Eqts}">|; 

$key = q/{Eqts}partialpsi(gamma)+sum_{i=1}^mlambda_icdotpartialvartheta_i(gamma)=0lambda_i^dualcdotvartheta_i(gamma)=0vartheta(gamma)ge0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 20.59ex; vertical-align: -9.75ex; " SRC="|."$dir".q|img136.svg"
 ALT="\begin{Eqts}
\partial \psi(\gamma) + \sum_{i = 1}^m \lambda_i \cdot \partial \va...
..._i^\dual \cdot \vartheta_i(\gamma) = 0 \ \\\\
\vartheta(\gamma) \ge 0
\end{Eqts}">|; 

$key = q/{Eqts}partialvarphi(gamma)+sum_{i=1}^mlambda_icdotpartialomega_i(gamma)=0lambda_i^dualcdotomega_i(gamma)=0omega(gamma)le0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 20.59ex; vertical-align: -9.75ex; " SRC="|."$dir".q|img84.svg"
 ALT="\begin{Eqts}
\partial \varphi(\gamma) + \sum_{i = 1}^m \lambda_i \cdot \partial ...
...lambda_i^\dual \cdot \omega_i(\gamma) = 0 \ \\\\
\omega(\gamma) \le 0
\end{Eqts}">|; 

$key = q/{Eqts}y_k=cases{lambda_k&text{si}knei0&text{si}k=icases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img104.svg"
 ALT="\begin{Eqts}
y_k =
\begin{cases}
\lambda_k &amp; \text{ si } k \ne i \\\\
0 &amp; \text{ si } k = i
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}y_k=cases{lambda_k&text{si}knei2lambda_i&text{si}k=icases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img107.svg"
 ALT="\begin{Eqts}
y_k =
\begin{cases}
\lambda_k &amp; \text{ si } k \ne i \\\\
2 \lambda_i &amp; \text{ si } k = i
\end{cases}\end{Eqts}">|; 

$key = q/{Eqts}y_k=cases{lambda_k&text{si}kneilambda_i+1&text{si}k=icases{{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.14ex; vertical-align: -3.00ex; " SRC="|."$dir".q|img110.svg"
 ALT="\begin{Eqts}
y_k =
\begin{cases}
\lambda_k &amp; \text{ si } k \ne i \\\\
\lambda_i + 1 &amp; \text{ si } k = i
\end{cases}\end{Eqts}">|; 

$key = q/{eqnarraystar}c^dualcdotx&=&x^dualcdotcy^dualcdotb&=&b^dualcdotyy^dualcdotAcdotx&=&(Acdotx)^dualcdoty=x^dualcdotA^dualcdoty{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.95ex; " SRC="|."$dir".q|img217.svg"
 ALT="\begin{eqnarray*}
c^\dual \cdot x &amp;=&amp; x^\dual \cdot c \\\\
y^\dual \cdot b &amp;=&amp; b^...
... x &amp;=&amp; (A \cdot x)^\dual \cdot y = x^\dual \cdot A^\dual \cdot y
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}lagrangien(x,y)&=&c^dualcdotx+y^dualcdot(b-Acdotx)&=&c^dualcdotx+y^dualcdotb-y^dualcdotAcdotx{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.44ex; " SRC="|."$dir".q|img216.svg"
 ALT="\begin{eqnarray*}
\lagrangien(x,y) &amp;=&amp; c^\dual \cdot x + y^\dual \cdot (b - A \c...
...
&amp;=&amp; c^\dual \cdot x + y^\dual \cdot b - y^\dual \cdot A \cdot x
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}lagrangien(x,y)&=&x^dualcdotc+b^dualcdoty-x^dualcdotA^dualcdoty&=&b^dualcdoty+x^dualcdot(c-A^dualcdoty){eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img218.svg"
 ALT="\begin{eqnarray*}
\lagrangien(x,y) &amp;=&amp; x^\dual \cdot c + b^\dual \cdot y - x^\du...
... y \\\\
&amp;=&amp; b^\dual \cdot y + x^\dual \cdot (c - A^\dual \cdot y)
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}omega_1(x)&le&0omega_2(x)&le&0dots&&omega_m(x)&le&0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.32ex; " SRC="|."$dir".q|img9.svg"
 ALT="\begin{eqnarray*}
\omega_1(x) &amp;\le&amp; 0 \\\\
\omega_2(x) &amp;\le&amp; 0 \\\\
\dots &amp; &amp; \\\\
\omega_m(x) &amp;\le&amp; 0
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}p(x)&=&0text{pourtout}xinOmegap(x)&strictsuperieur&0text{pourtout}xinsetR^nsetminusOmega{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 11.57ex; " SRC="|."$dir".q|img43.svg"
 ALT="\begin{eqnarray*}
p(x) &amp;=&amp; 0 \text{ pour tout } x \in \Omega \\\\
p(x) &amp;\strictsuperieur&amp; 0 \text{ pour tout } x \in \setR^n \setminus \Omega
\end{eqnarray*}">|; 

$key = q/{eqnarraystar}s,t&ge&0s+omega(x)&=&0t-vartheta(x)&=&0varrho(x)&=&0{eqnarraystar};MSF=1.6;TAGS=R;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 18.32ex; " SRC="|."$dir".q|img185.svg"
 ALT="\begin{eqnarray*}
s,t &amp;\ge&amp; 0 \\\\
s + \omega(x) &amp;=&amp; 0 \\\\
t - \vartheta(x) &amp;=&amp; 0 \\\\
\varrho(x) &amp;=&amp; 0
\end{eqnarray*}">|; 

1;

