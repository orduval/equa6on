# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q/(x,y);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$(x,y)$">|; 

$key = q/(x_1,x_2,...,x_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img8.svg"
 ALT="$(x_1,x_2,...,x_n)$">|; 

$key = q/A;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$A$">|; 

$key = q/A^n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img20.svg"
 ALT="$A^n$">|; 

$key = q/A_1,A_2,...,A_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img9.svg"
 ALT="$A_1, A_2, ..., A_n$">|; 

$key = q/B;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.78ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img3.svg"
 ALT="$B$">|; 

$key = q/displaystyle(a,a,b)ne(a,b);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img19.svg"
 ALT="$\displaystyle (a,a,b) \ne (a,b)$">|; 

$key = q/displaystyle(a,b)ne(b,a);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle (a,b) \ne (b,a)$">|; 

$key = q/displaystyle(x_1,x_2,...,x_n)=(y_1,y_2,...,y_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$\displaystyle (x_1,x_2,...,x_n) = (y_1,y_2,...,y_n)$">|; 

$key = q/displaystyleA^2=AtimesA;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.34ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle A^2 = A \times A$">|; 

$key = q/displaystyleA^n={(x_1,x_2,...,x_n):x_1,x_2,...,x_ninA};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$\displaystyle A^n = \{ (x_1,x_2,...,x_n) : x_1,x_2,...,x_n \in A \}$">|; 

$key = q/displaystyleA_1timesA_2times...timesA_n={(x_1,x_2,...,x_n):x_1inA_1,x_2inA_2,...,x_ninA_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img10.svg"
 ALT="$\displaystyle A_1 \times A_2 \times ... \times A_n = \{ (x_1,x_2,...,x_n) : x_1 \in A_1, x_2 \in A_2, ..., x_n \in A_n \}$">|; 

$key = q/displaystyleAtimesB={(x,y):xinAtext{et}yinB};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\displaystyle A \times B = \{ (x,y) : x \in A  \text{ et }  y \in B \}$">|; 

$key = q/displaystylex=(x_1,x_2,...,x_n)inA_1timesA_2times...timesA_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img12.svg"
 ALT="$\displaystyle x = (x_1,x_2,...,x_n) \in A_1 \times A_2 \times ... \times A_n$">|; 

$key = q/displaystylex=y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img15.svg"
 ALT="$\displaystyle x = y$">|; 

$key = q/displaystylex_i=y_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img16.svg"
 ALT="$\displaystyle x_i = y_i$">|; 

$key = q/displaystyley=(y_1,y_2,...,y_n)inA_1timesA_2times...timesA_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle y = (y_1,y_2,...,y_n) \in A_1 \times A_2 \times ... \times A_n$">|; 

$key = q/iin{1,2,...,n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img17.svg"
 ALT="$i \in \{1,2,...,n\}$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img21.svg"
 ALT="$n$">|; 

$key = q/times;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.74ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\times$">|; 

$key = q/x;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img6.svg"
 ALT="$x$">|; 

$key = q/x_1,x_2,...,x_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img11.svg"
 ALT="$x_1,x_2,...,x_n$">|; 

$key = q/y;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img7.svg"
 ALT="$y$">|; 

1;

