# LaTeX2HTML 2023 (Released January 1, 2023)
# Associate images original text with physical files.


$key = q//;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.52ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img42.svg"
 ALT="$\displaystyle \scalaire{\phi_i}{\phi_{n+1}} = a_n \scalaire{\phi_i}{x\phi_n} + ...
...alaire{\phi_i}{\phi_{n-1}} + \sum_{j=0}^{n-2} d_j \scalaire{\phi_i}{\phi_j} = 0$">|; 

$key = q/(1,x,x^2,...,x^n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.61ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img31.svg"
 ALT="$(1,x,x^2,...,x^n)$">|; 

$key = q/(mu_0,mu_1,...,mu_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img4.svg"
 ALT="$(\mu_0,\mu_1,...,\mu_n)$">|; 

$key = q/(phi_0,...,phi_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img36.svg"
 ALT="$(\phi_0,...,\phi_n)$">|; 

$key = q/(phi_0,...,phi_n,xphi_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img37.svg"
 ALT="$(\phi_0,...,\phi_n,x\phi_n)$">|; 

$key = q/(phi_0,...phi_n);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img57.svg"
 ALT="$(\phi_0,...\phi_n)$">|; 

$key = q/(phi_n)_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img32.svg"
 ALT="$(\phi_n)_n$">|; 

$key = q/0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img25.svg"
 ALT="$0$">|; 

$key = q/E_i={1,2,...,n}setminus{i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img117.svg"
 ALT="$E_i = \{ 1,2,...,n \} \setminus \{i\}$">|; 

$key = q/Lambda_i(x_i)=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img118.svg"
 ALT="$\Lambda_i(x_i) = 1$">|; 

$key = q/Lambda_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img111.svg"
 ALT="$\Lambda_i$">|; 

$key = q/Phi(n+1,n+1);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img76.svg"
 ALT="$\Phi(n+1,n+1)$">|; 

$key = q/S=setR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.79ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img2.svg"
 ALT="$S=\setR$">|; 

$key = q/a_iinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img98.svg"
 ALT="$a_i\in\setR$">|; 

$key = q/a_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img54.svg"
 ALT="$a_n$">|; 

$key = q/displaystyle2int_Aphi_i(x)[u(x)-w(x)]dmu(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img63.svg"
 ALT="$\displaystyle 2 \int_A \phi_i(x) [u(x)-w(x)] d\mu(x) = 0$">|; 

$key = q/displaystyleA_i=unsur{P_i(x_i)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.42ex; vertical-align: -2.27ex; " SRC="|."$dir".q|img119.svg"
 ALT="$\displaystyle A_i = \unsur{P_i(x_i)}$">|; 

$key = q/displaystyleI(f)=sum_{i=0}^nw_if(x_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img68.svg"
 ALT="$\displaystyle I(f) = \sum_{i=0}^n w_i f(x_i)$">|; 

$key = q/displaystyleI(p)=I(r);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img93.svg"
 ALT="$\displaystyle I(p) = I(r)$">|; 

$key = q/displaystyleLambda_i(x)=A_iprod_{jinE_i}(x-x_j)=A_iP_i(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.87ex; vertical-align: -3.35ex; " SRC="|."$dir".q|img116.svg"
 ALT="$\displaystyle \Lambda_i(x) = A_i \prod_{j \in E_i} (x-x_j) = A_i P_i(x)$">|; 

$key = q/displaystyleLambda_i(x)=prod_{jinE_i}frac{(x-x_j)}{(x_i-x_j)};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.74ex; vertical-align: -3.35ex; " SRC="|."$dir".q|img120.svg"
 ALT="$\displaystyle \Lambda_i(x) = \prod_{j \in E_i} \frac{(x-x_j)}{(x_i - x_j)}$">|; 

$key = q/displaystylePhiW=varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img75.svg"
 ALT="$\displaystyle \Phi W = \varphi$">|; 

$key = q/displaystyleW=Phi^{-1}varphi;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.59ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img77.svg"
 ALT="$\displaystyle W = \Phi^{-1} \varphi$">|; 

$key = q/displaystylea_01=0quadRightarrowquada_0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img9.svg"
 ALT="$\displaystyle a_0 1 = 0 \quad\Rightarrow\quad a_0 = 0$">|; 

$key = q/displaystylea_0=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img17.svg"
 ALT="$\displaystyle a_0 = 0$">|; 

$key = q/displaystylea_0=a_1=...=a_n=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img28.svg"
 ALT="$\displaystyle a_0 = a_1 = ... = a_n = 0$">|; 

$key = q/displaystylea_1=a_2=...=a_n=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.03ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img27.svg"
 ALT="$\displaystyle a_1 = a_2 = ... = a_n = 0$">|; 

$key = q/displaystylea_i=forme{phi_i}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img106.svg"
 ALT="$\displaystyle a_i = \forme{\phi_i}{u}$">|; 

$key = q/displaystyleforme{phi_i}{u_j}=delta_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img105.svg"
 ALT="$\displaystyle \forme{\phi_i}{u_j} = \delta_{ij}$">|; 

$key = q/displaystyleforme{phi_i}{u}=u(x_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img112.svg"
 ALT="$\displaystyle \forme{\phi_i}{u} = u(x_i)$">|; 

$key = q/displaystyleforme{phi_i}{u}=y_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img101.svg"
 ALT="$\displaystyle \forme{\phi_i}{u} = y_i$">|; 

$key = q/displaystyleforme{phi_j}{Lambda_i}=Lambda_i(x_j)=delta_{ij};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img113.svg"
 ALT="$\displaystyle \forme{\phi_j}{\Lambda_i} = \Lambda_i(x_j) = \delta_{ij}$">|; 

$key = q/displaystyleint_Af(x)dmu(x)=sum_{i=0}^nw_if(x_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img94.svg"
 ALT="$\displaystyle \int_A f(x) d\mu(x) = \sum_{i=0}^n w_i f(x_i)$">|; 

$key = q/displaystyleint_{-1}^1P_n(x)P_m(x)dx=frac{2}{2n+1}delta_{mn};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.98ex; vertical-align: -2.36ex; " SRC="|."$dir".q|img96.svg"
 ALT="$\displaystyle \int_{-1}^1 P_n(x) P_m(x) dx = \frac{2}{2 n + 1} \delta_{mn}$">|; 

$key = q/displaystylelanglefrangle=scalaire{f}{1}=int_Af(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img69.svg"
 ALT="$\displaystyle \langle f \rangle = \scalaire{f}{1} = \int_A f(x) d\mu(x)$">|; 

$key = q/displaystylelanglephi_krangle=sum_{i=0}^nw_iphi_k(x_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img72.svg"
 ALT="$\displaystyle \langle \phi_k \rangle = \sum_{i=0}^n w_i \phi_k(x_i)$">|; 

$key = q/displaystylelanglephi_krangle=unsur{phi_0}scalaire{phi_k}{phi_0};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.29ex; vertical-align: -2.14ex; " SRC="|."$dir".q|img78.svg"
 ALT="$\displaystyle \langle \phi_k \rangle = \unsur{\phi_0} \scalaire{\phi_k}{\phi_0}$">|; 

$key = q/displaystylelanglephi_{n+1}rangle=I(phi_{n+1})=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img82.svg"
 ALT="$\displaystyle \langle \phi_{n+1} \rangle = I(\phi_{n+1}) = 0$">|; 

$key = q/displaystylelangleprangle=langlerrangle=I(r);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img91.svg"
 ALT="$\displaystyle \langle p \rangle = \langle r \rangle = I(r)$">|; 

$key = q/displaystylelangleqphi_{n+1}rangle=sum_{i=0}^nq_iscalaire{phi_i}{phi_{n+1}}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img90.svg"
 ALT="$\displaystyle \langle q \phi_{n+1} \rangle = \sum_{i=0}^n q_i \scalaire{\phi_i}{\phi_{n+1}} = 0$">|; 

$key = q/displaystylemathcal{P}_n=combilin{mu_0,mu_1,...,mu_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img3.svg"
 ALT="$\displaystyle \mathcal{P}_n = \combilin{\mu_0,\mu_1,...,\mu_n}$">|; 

$key = q/displaystylep(x)=q(x)phi_{n+1}(x)+r(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img86.svg"
 ALT="$\displaystyle p(x) = q(x) \phi_{n+1}(x) + r(x)$">|; 

$key = q/displaystylep(x)=sum_{i=0}^na_ix^i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img13.svg"
 ALT="$\displaystyle p(x) = \sum_{i=0}^n a_i x^i$">|; 

$key = q/displaystylep(x)=sum_{i=1}^na_ix^i=xq(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img18.svg"
 ALT="$\displaystyle p(x) = \sum_{i=1}^n a_i x^i = x q(x) = 0$">|; 

$key = q/displaystylep_{ii}=f(x_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img130.svg"
 ALT="$\displaystyle p_{ii} = f(x_i)$">|; 

$key = q/displaystylep_{ij}(x)=frac{(x-x_i)p_{i+1,j}(x)-(x-x_j)p_{i,j-1}(x)}{x_j-x_i};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.73ex; vertical-align: -2.34ex; " SRC="|."$dir".q|img132.svg"
 ALT="$\displaystyle p_{ij}(x) = \frac{(x-x_i)p_{i+1,j}(x)-(x-x_j)p_{i,j-1}(x)}{x_j-x_i}$">|; 

$key = q/displaystylep_{ij}(x)=sum_{j=0}^{j-i}a_kx^k;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.64ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img126.svg"
 ALT="$\displaystyle p_{ij}(x) = \sum_{j=0}^{j-i} a_k x^k$">|; 

$key = q/displaystylep_{ij}(x_k)=f(x_k);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.58ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img127.svg"
 ALT="$\displaystyle p_{ij}(x_k) = f(x_k)$">|; 

$key = q/displaystylephi_{n+1}(x)=a_nxphi_n(x)+b_nphi_n(x)+c_nphi_{n-1}(x)+sum_{i=0}^{n-2}d_iphi_i(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.22ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img40.svg"
 ALT="$\displaystyle \phi_{n+1}(x) = a_n x\phi_n(x) + b_n \phi_n(x) + c_n \phi_{n-1}(x) + \sum_{i=0}^{n-2} d_i \phi_i(x)$">|; 

$key = q/displaystylephi_{n+1}(x_i)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img81.svg"
 ALT="$\displaystyle \phi_{n+1}(x_i) = 0$">|; 

$key = q/displaystyleq(0)=lim_{substack{xrightarrow0xne0}}q(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.15ex; vertical-align: -3.33ex; " SRC="|."$dir".q|img23.svg"
 ALT="$\displaystyle q(0) = \lim_{ \substack{ x \rightarrow 0 \ x \ne 0 } } q(x) = 0$">|; 

$key = q/displaystyleq(x)=sum_{i=1}^na_ix^{i-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img20.svg"
 ALT="$\displaystyle q(x) = \sum_{i=1}^n a_i x^{i-1}$">|; 

$key = q/displaystyleq=sum_{i=0}^mq_iphi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img88.svg"
 ALT="$\displaystyle q = \sum_{i=0}^m q_i \phi_i$">|; 

$key = q/displaystylescalaire{phi_i}{phi_{n+1}}=sum_{j=0}^{n-2}d_jdelta_{ij}=d_i=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.52ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img50.svg"
 ALT="$\displaystyle \scalaire{\phi_i}{\phi_{n+1}} = \sum_{j=0}^{n-2} d_j \delta_{ij} = d_i = 0$">|; 

$key = q/displaystylescalaire{phi_i}{xphi_n}=int_Axphi_i(x)phi_n(x)dmu(x)=scalaire{xphi_i}{phi_n};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img44.svg"
 ALT="$\displaystyle \scalaire{\phi_i}{x\phi_n} = \int_A x \phi_i(x) \phi_n(x) d\mu(x) = \scalaire{x\phi_i}{\phi_n}$">|; 

$key = q/displaystylescalaire{phi_i}{xphi_n}=sum_{j=0}^{i+1}alpha_iscalaire{phi_j}{phi_n}=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.55ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img48.svg"
 ALT="$\displaystyle \scalaire{\phi_i}{x\phi_n} = \sum_{j=0}^{i+1} \alpha_i \scalaire{\phi_j}{\phi_n} = 0$">|; 

$key = q/displaystylescalaire{phi_m}{phi_n}=int_Aphi_m(x)phi_n(x)dmu(x)=delta_{mn};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img35.svg"
 ALT="$\displaystyle \scalaire{\phi_m}{\phi_n} = \int_A \phi_m(x) \phi_n(x) d\mu(x) = \delta_{mn}$">|; 

$key = q/displaystylescalaire{phi_{n+1}}{phi_{n+1}}=a_nscalaire{xphi_n}{phi_{n+1}}=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img53.svg"
 ALT="$\displaystyle \scalaire{\phi_{n+1}}{\phi_{n+1}} = a_n \scalaire{x\phi_n}{\phi_{n+1}} = 1$">|; 

$key = q/displaystylescalaire{p}{q}=int_a^bp(x)q(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img29.svg"
 ALT="$\displaystyle \scalaire{p}{q} = \int_a^b p(x) q(x) d\mu(x)$">|; 

$key = q/displaystylescalaire{p}{q}=int_a^bp(x)q(x)w(x)dx;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.89ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img30.svg"
 ALT="$\displaystyle \scalaire{p}{q} = \int_a^b p(x) q(x) w(x) dx$">|; 

$key = q/displaystylescalaire{u-w}{u-w}=int_A[u(x)-w(x)]^2dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img61.svg"
 ALT="$\displaystyle \scalaire{u-w}{u-w} = \int_A [u(x)-w(x)]^2 d\mu(x)$">|; 

$key = q/displaystylescalaire{u}{v}=int_Au(x)v(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img58.svg"
 ALT="$\displaystyle \scalaire{u}{v} = \int_A u(x) v(x) d\mu(x)$">|; 

$key = q/displaystylesum_{i=0}^na_imu_i=0quadRightarrowquada_0=a_1=...=a_n=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img6.svg"
 ALT="$\displaystyle \sum_{i=0}^n a_i \mu_i = 0 \quad\Rightarrow\quad a_0 = a_1 = ... = a_n = 0$">|; 

$key = q/displaystylesum_{i=0}^na_ix^i=0quadforallxinsetRquadRightarrowquada_0=a_1=...=a_n=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img7.svg"
 ALT="$\displaystyle \sum_{i=0}^n a_i x^i = 0 \quad\forall x \in\setR \quad\Rightarrow\quad a_0 = a_1 = ... = a_n = 0$">|; 

$key = q/displaystyleu(x_i)=f(x_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img109.svg"
 ALT="$\displaystyle u(x_i) = f(x_i)$">|; 

$key = q/displaystyleu(x_i)=w(x_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img122.svg"
 ALT="$\displaystyle u(x_i) = w(x_i)$">|; 

$key = q/displaystyleu=sum_{i=1}^na_iu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img99.svg"
 ALT="$\displaystyle u = \sum_{i=1}^n a_i u_i$">|; 

$key = q/displaystyleu_i=forme{phi_i}{u}=u(x_i);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img123.svg"
 ALT="$\displaystyle u_i = \forme{\phi_i}{u} = u(x_i)$">|; 

$key = q/displaystylew(x)=sum_{i=0}^nw_iphi_i(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img60.svg"
 ALT="$\displaystyle w(x) = \sum_{i=0}^n w_i \phi_i(x)$">|; 

$key = q/displaystylew(x)=sum_{i=1}^{n}u_iLambda_i(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.89ex; vertical-align: -3.02ex; " SRC="|."$dir".q|img121.svg"
 ALT="$\displaystyle w(x) = \sum_{i=1}^{n} u_i \Lambda_i(x)$">|; 

$key = q/displaystylew_i=int_Aphi_i(x)u(x)dmu(x)=scalaire{phi_i}{u};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img65.svg"
 ALT="$\displaystyle w_i = \int_A \phi_i(x) u(x) d\mu(x) = \scalaire{\phi_i}{u}$">|; 

$key = q/displaystylew_i=int_Aphi_i(x)w(x)dmu(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 5.43ex; vertical-align: -2.19ex; " SRC="|."$dir".q|img64.svg"
 ALT="$\displaystyle w_i = \int_A \phi_i(x) w(x) d\mu(x)$">|; 

$key = q/displaystylexphi_i=sum_{j=0}^{i+1}alpha_iphi_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 7.55ex; vertical-align: -3.32ex; " SRC="|."$dir".q|img47.svg"
 ALT="$\displaystyle x \phi_i = \sum_{j=0}^{i+1} \alpha_i \phi_j$">|; 

$key = q/f;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img108.svg"
 ALT="$f$">|; 

$key = q/finmathcal{P}_{2n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img95.svg"
 ALT="$f\in\mathcal{P}_{2n+1}$">|; 

$key = q/i+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.89ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img46.svg"
 ALT="$i+1$">|; 

$key = q/i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.71ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img34.svg"
 ALT="$i$">|; 

$key = q/i<j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img131.svg"
 ALT="$i &lt; j$">|; 

$key = q/i=1,2,...,n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img102.svg"
 ALT="$i=1,2,...,n$">|; 

$key = q/i=j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img129.svg"
 ALT="$i=j$">|; 

$key = q/iin{0,...,n-2};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img41.svg"
 ALT="$i \in \{0, ..., n-2\}$">|; 

$key = q/j-i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img125.svg"
 ALT="$j-i$">|; 

$key = q/jlei+1<n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.16ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img49.svg"
 ALT="$j \le i+1 &lt; n$">|; 

$key = q/jnei;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img115.svg"
 ALT="$j \ne i$">|; 

$key = q/k=0,1,...,n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img73.svg"
 ALT="$k = 0,1,...,n$">|; 

$key = q/kin{i,i+1,...,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img128.svg"
 ALT="$k\in\{i,i+1,...,j\}$">|; 

$key = q/kne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img79.svg"
 ALT="$k\ne 0$">|; 

$key = q/m;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img87.svg"
 ALT="$m$">|; 

$key = q/mathcal{P}_N^D;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.67ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img103.svg"
 ALT="$\mathcal{P}_N^D$">|; 

$key = q/mathcal{P}_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.12ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img1.svg"
 ALT="$\mathcal{P}_n$">|; 

$key = q/mathcal{P}_{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img38.svg"
 ALT="$\mathcal{P}_{n+1}$">|; 

$key = q/mge0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.00ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img85.svg"
 ALT="$m \ge 0$">|; 

$key = q/mlen;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.98ex; vertical-align: -0.41ex; " SRC="|."$dir".q|img89.svg"
 ALT="$m \le n$">|; 

$key = q/mu_0=1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.14ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img8.svg"
 ALT="$\mu_0=1$">|; 

$key = q/mu_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img5.svg"
 ALT="$\mu_i$">|; 

$key = q/n+m+1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.86ex; vertical-align: -0.27ex; " SRC="|."$dir".q|img84.svg"
 ALT="$n+m+1$">|; 

$key = q/n-1;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.29ex; " SRC="|."$dir".q|img11.svg"
 ALT="$n-1$">|; 

$key = q/n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img67.svg"
 ALT="$n$">|; 

$key = q/n=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.69ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img10.svg"
 ALT="$n=0$">|; 

$key = q/p(0)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img16.svg"
 ALT="$p(0)=0$">|; 

$key = q/p(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img14.svg"
 ALT="$p(x) = 0$">|; 

$key = q/p;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img83.svg"
 ALT="$p$">|; 

$key = q/p_{i,j};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img124.svg"
 ALT="$p_{i,j}$">|; 

$key = q/phi_0,...,phi_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img71.svg"
 ALT="$\phi_0,...,\phi_n$">|; 

$key = q/phi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img33.svg"
 ALT="$\phi_i$">|; 

$key = q/phi_{n+1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.31ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img39.svg"
 ALT="$\phi_{n+1}$">|; 

$key = q/pinmathcal{P}_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.23ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img12.svg"
 ALT="$p\in\mathcal{P}_n$">|; 

$key = q/q(x);MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img26.svg"
 ALT="$q(x)$">|; 

$key = q/q(x)=0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.51ex; vertical-align: -0.68ex; " SRC="|."$dir".q|img22.svg"
 ALT="$q(x) = 0$">|; 

$key = q/q;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img24.svg"
 ALT="$q$">|; 

$key = q/qinmathcal{P}_{n-1};MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.28ex; vertical-align: -0.61ex; " SRC="|."$dir".q|img19.svg"
 ALT="$q\in\mathcal{P}_{n-1}$">|; 

$key = q/r;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img92.svg"
 ALT="$r$">|; 

$key = q/u;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.19ex; vertical-align: -0.10ex; " SRC="|."$dir".q|img59.svg"
 ALT="$u$">|; 

$key = q/u_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img100.svg"
 ALT="$u_i$">|; 

$key = q/w_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img62.svg"
 ALT="$w_i$">|; 

$key = q/x_0<x_1<...<x_n;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.76ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img70.svg"
 ALT="$x_0 &lt; x_1 &lt; ... &lt; x_n$">|; 

$key = q/x_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.54ex; vertical-align: -0.45ex; " SRC="|."$dir".q|img80.svg"
 ALT="$x_i$">|; 

$key = q/x_j;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.84ex; vertical-align: -0.75ex; " SRC="|."$dir".q|img114.svg"
 ALT="$x_j$">|; 

$key = q/xinsetR;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.88ex; vertical-align: -0.19ex; " SRC="|."$dir".q|img15.svg"
 ALT="$x\in\setR$">|; 

$key = q/xne0;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img21.svg"
 ALT="$x\ne 0$">|; 

$key = q/xphi_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 2.25ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img45.svg"
 ALT="$x\phi_i$">|; 

$key = q/y_i;MSF=1.6;AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 1.64ex; vertical-align: -0.55ex; " SRC="|."$dir".q|img104.svg"
 ALT="$y_i$">|; 

$key = q/{Eqts}P_0(x)=1P_1(x)=x(n+1)P_{n+1}(x)=(2n+1)xP_n(x)-nP_{n-1}(x){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.74ex; vertical-align: -4.31ex; " SRC="|."$dir".q|img97.svg"
 ALT="\begin{Eqts}
P_0(x) = 1 \\\\
P_1(x) = x \\\\
(n+1) P_{n+1}(x) = (2 n + 1) x P_n(x) - n P_{n-1}(x)
\end{Eqts}">|; 

$key = q/{Eqts}b_n=-a_nscalaire{phi_n}{xphi_n}c_n=-a_nscalaire{phi_{n-1}}{xphi_n}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img52.svg"
 ALT="\begin{Eqts}
b_n = -a_n\scalaire{\phi_n}{x\phi_n} \\\\
c_n = -a_n\scalaire{\phi_{n-1}}{x\phi_n}
\end{Eqts}">|; 

$key = q/{Eqts}forme{phi_i}{u}=OD{u}{x}(x_i)y_i=OD{f}{x}(x_i){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 10.49ex; vertical-align: -4.68ex; " SRC="|."$dir".q|img110.svg"
 ALT="\begin{Eqts}
\forme{\phi_i}{u} = \OD{u}{x}(x_i) \\\\
y_i = \OD{f}{x}(x_i)
\end{Eqts}">|; 

$key = q/{Eqts}forme{phi_i}{u}=u(x_i)y_i=f(x_i){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img107.svg"
 ALT="\begin{Eqts}
\forme{\phi_i}{u} = u(x_i) \\\\
y_i = f(x_i)
\end{Eqts}">|; 

$key = q/{Eqts}phi_0=unsur{sqrt{scalaire{1}{1}}}phi_1=a_1(x-scalaire{phi_0}{x}phi_0){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.43ex; vertical-align: -4.15ex; " SRC="|."$dir".q|img56.svg"
 ALT="\begin{Eqts}
\phi_0 = \unsur{\sqrt{\scalaire{1}{1}}} \\\\
\phi_1 = a_1 (x - \scalaire{\phi_0}{x} \phi_0)
\end{Eqts}">|; 

$key = q/{Eqts}scalaire{phi_i}{phi_n}=scalaire{phi_i}{phi_{n-1}}=0scalaire{phi_i}{phi_j}=delta_{ij}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img43.svg"
 ALT="\begin{Eqts}
\scalaire{\phi_i}{\phi_n} = \scalaire{\phi_i}{\phi_{n-1}} = 0 \\\\
\scalaire{\phi_i}{\phi_j} = \delta_{ij}
\end{Eqts}">|; 

$key = q/{Eqts}scalaire{phi_{n+1}}{phi_n}=0scalaire{phi_{n+1}}{phi_{n-1}}=0{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 6.37ex; vertical-align: -2.61ex; " SRC="|."$dir".q|img51.svg"
 ALT="\begin{Eqts}
\scalaire{\phi_{n+1}}{\phi_n} = 0 \\\\
\scalaire{\phi_{n+1}}{\phi_{n-1}} = 0
\end{Eqts}">|; 

$key = q/{Eqts}varphi=(langlephi_krangle)_kW=(w_i)_iPhi=left(phi_i(x_j)right)_{i,j}{Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 9.95ex; vertical-align: -4.41ex; " SRC="|."$dir".q|img74.svg"
 ALT="\begin{Eqts}
\varphi = (\langle \phi_k \rangle)_k \\\\
W = (w_i)_i \\\\
\Phi = \left(\phi_i(x_j)\right)_{i,j}
\end{Eqts}">|; 

$key = q/{Eqts}w(x)=sum_{i=0}^nphi_i(x)int_Aphi_i(y)u(y)dmu(y)w(x)=sum_{i=0}^nint_Aphi_i(x)phi_i(y)u(y)dmu(y){Eqts};AAT/;
$cached_env_img{$key} = q|<IMG
 STYLE="height: 14.36ex; vertical-align: -6.63ex; " SRC="|."$dir".q|img66.svg"
 ALT="\begin{Eqts}
w(x) = \sum_{i=0}^n \phi_i(x) \int_A \phi_i(y) u(y) d\mu(y) \\\\
w(x) = \sum_{i=0}^n \int_A \phi_i(x) \phi_i(y) u(y) d\mu(y)
\end{Eqts}">|; 

1;

